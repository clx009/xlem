package ws.pojo.request;

import java.util.List;

import ws.pojo.WsPojo;

public class BaseRequest {
	private String userName;
	private String batchNo;
	private String activePW;
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getActivePW() {
		return activePW;
	}

	public void setActivePW(String activePW) {
		this.activePW = activePW;
	}


	public String getBatchNo() {
		return batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	
}
