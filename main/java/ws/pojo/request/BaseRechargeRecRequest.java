package ws.pojo.request;

import ws.pojo.WsPojo;

public class BaseRechargeRecRequest extends WsPojo {
	private String searchDate;

	public String getSearchDate() {
		return searchDate;
	}

	public void setSearchDate(String searchDate) {
		this.searchDate = searchDate;
	}
	
}
