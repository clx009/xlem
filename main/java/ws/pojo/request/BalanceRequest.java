package ws.pojo.request;

public class BalanceRequest extends BaseRequest {
	private String payType; // 支付方式01为奖励款支付 02为现金余额支付
	private String useDate;// 使用时间

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getUseDate() {
		return useDate;
	}

	public void setUseDate(String useDate) {
		this.useDate = useDate;
	}

}
