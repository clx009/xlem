package ws.pojo.request;

import java.util.ArrayList;
import java.util.List;

import com.xlem.dao.AvailableRoomInfo;


public class AvailableRoomInfoRequest extends BaseRequest{
	private List<AvailableRoomInfo> details =new ArrayList<AvailableRoomInfo>();

	public void setBaseRequest(BaseRequest br) {
		this.setUserName(br.getUserName());
		this.setBatchNo(br.getBatchNo());
		this.setActivePW(br.getActivePW());
	}
	
	public List<AvailableRoomInfo> getDetails() {
		return details;
	}

	public void setDetails(List<AvailableRoomInfo> details) {
		this.details = details;
	}
	
	
}
