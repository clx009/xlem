package ws.pojo.request;

public class FinanceRequest extends BaseRequest {
	private String useDate;
	
	public void setUseDate(String useDate) {
		this.useDate = useDate;
	}
	
	public String getUseDate() {
		return this.useDate;
	}

}
