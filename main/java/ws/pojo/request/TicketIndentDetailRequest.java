package ws.pojo.request;

public class TicketIndentDetailRequest {
	private String ticketPriceId;
	private String ticketId;
	private String unitPrice;
	private String printPrice;
	private String amount;

	public String getTicketPriceId() {
		return ticketPriceId;
	}

	public void setTicketPriceId(String ticketPriceId) {
		this.ticketPriceId = ticketPriceId;
	}


	public String getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getPrintPrice() {
		return printPrice;
	}

	public void setPrintPrice(String printPrice) {
		this.printPrice = printPrice;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}
}
