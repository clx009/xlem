package ws.pojo.request;

public class SmsRequest extends BaseRequest{
	private String phones;
	private String content;
	
	public String getPhones() {
		return phones;
	}
	public void setPhones(String phones) {
		this.phones = phones;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
}
