package ws.pojo.request;

import java.util.ArrayList;
import java.util.List;

import com.xlem.dao.TicketIndent;

import common.CommonMethod;


public class TicketIndentRequest extends BaseRequest{
	private String useDate;
	private String linkman;
	private String phoneNumber;
	private String idCardNumber;
	private String totalAmount;
	private String totalPrice;
	private String outNo;
	private String cashBalance;
	private String payType;
	private List<TicketIndentDetailRequest> details =new ArrayList<TicketIndentDetailRequest>();
	
	public TicketIndent reqToIndent(){
		TicketIndent ti = new TicketIndent();
		ti.setIdCardNumber(this.getIdCardNumber());
		ti.setLinkman(this.getLinkman());
		ti.setFaxNumber(this.getOutNo());
		ti.setPhoneNumber(this.getPhoneNumber());
		ti.setTotalAmount(Integer.parseInt(this.getTotalAmount()));
		ti.setTotalPrice(Double.parseDouble(this.getTotalPrice()));
		ti.setUseDate(CommonMethod.string2Time1(this.getUseDate()));
		ti.setCashBalance(this.getCashBalance()==null?null:Double.parseDouble(this.getCashBalance()));
		ti.setPayType(payType);
		return ti;
	}
	

	public String getUseDate() {
		return useDate;
	}

	public void setUseDate(String useDate) {
		this.useDate = useDate;
	}

	public String getLinkman() {
		return linkman;
	}

	public void setLinkman(String linkman) {
		this.linkman = linkman;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getIdCardNumber() {
		return idCardNumber;
	}

	public void setIdCardNumber(String idCardNumber) {
		this.idCardNumber = idCardNumber;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}

	public List<TicketIndentDetailRequest> getDetails() {
		return details;
	}

	public void setDetails(List<TicketIndentDetailRequest> details) {
		this.details = details;
	}

	public String getOutNo() {
		return outNo;
	}

	public void setOutNo(String outNo) {
		this.outNo = outNo;
	}


	public String getCashBalance() {
		return cashBalance;
	}


	public void setCashBalance(String cashBalance) {
		this.cashBalance = cashBalance;
	}


	public String getPayType() {
		return payType;
	}


	public void setPayType(String payType) {
		this.payType = payType;
	}
	
}
