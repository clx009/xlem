package ws.pojo.request;

public class TicketPriceRequest extends BaseRequest{
	private String useDate;

	public String getUseDate() {
		return useDate;
	}

	public void setUseDate(String useDate) {
		this.useDate = useDate;
	}
}
