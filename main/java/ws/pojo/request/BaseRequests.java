package ws.pojo.request;

import java.util.List;

import ws.pojo.WsPojo;

public class BaseRequests extends BaseRequest {
	private List<? extends WsPojo> wsPojos;

	public List<? extends WsPojo> getWsPojos() {
		return wsPojos;
	}

	public void setWsPojos(List<? extends WsPojo> wsPojos) {
		this.wsPojos = wsPojos;
	}

}
