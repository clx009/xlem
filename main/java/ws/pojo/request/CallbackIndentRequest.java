package ws.pojo.request;

import com.xlem.dao.TicketIndent;

import ws.pojo.WsPojo;

public class CallbackIndentRequest extends WsPojo {
	private String indentCode;
	private String totalAmount;
	private String totalPrice;
	private String perFee;
	private String payFee;
	private String status;
	private String synchroState;

	public String getIndentCode() {
		return indentCode;
	}

	public void setIndentCode(String indentCode) {
		this.indentCode = indentCode;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getPerFee() {
		return perFee;
	}

	public void setPerFee(String perFee) {
		this.perFee = perFee;
	}

	public String getPayFee() {
		return payFee;
	}

	public void setPayFee(String payFee) {
		this.payFee = payFee;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSynchroState() {
		return synchroState;
	}

	public void setSynchroState(String synchroState) {
		this.synchroState = synchroState;
	}

	public CallbackIndentRequest indentToRequest(TicketIndent ti) {
		this.setIndentCode(ti.getIndentCode());
		this.setPayFee(ti.getPayFee()==null?null:ti.getPayFee().toString());
		this.setPerFee(ti.getPerFee()==null?null:ti.getPerFee().toString());
		this.setStatus(ti.getStatus());
		this.setSynchroState(ti.getSynchroState());
		this.setTotalAmount(ti.getTotalAmount().toString());
		this.setTotalPrice(ti.getTotalPrice().toString());
		return this;
	}
}
