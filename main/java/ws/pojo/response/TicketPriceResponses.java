package ws.pojo.response;

import java.util.List;

public class TicketPriceResponses extends BaseResponse {
	private List<TicketPriceResponse> wsPojos;
	
	public List<TicketPriceResponse> getWsPojos() {
		return this.wsPojos;
	}
	
	public void setWsPojos(List<TicketPriceResponse> wsPojos) {
		this.wsPojos = wsPojos;
	}

}
