package ws.pojo.response;

import com.xlem.dao.TicketIndent;

public class ReturnTicketResponse extends BaseResponses {
	private String indentId;
	private String indentCode;
	private String outNo;
	private String totalPrice;
	private String perFee;
	private String payFee;
	private String status;
	private String synchroState;

	public ReturnTicketResponse indentToRes(TicketIndent ti) {
		this.setIndentId(ti.getIndentId());
		this.setIndentCode(ti.getIndentCode());
		this.setOutNo(ti.getTelNumber());
		this.setTotalPrice(ti.getTotalPrice().toString());
		this.setStatus(ti.getStatus());
		this.setSynchroState(ti.getSynchroState());
		this.setPayFee(ti.getPayFee()==null?null:ti.getPayFee().toString());
		this.setPerFee(ti.getPerFee()==null?null:ti.getPerFee().toString());
		return this;
	}

	public String getIndentId() {
		return indentId;
	}

	public void setIndentId(String indentId) {
		this.indentId = indentId;
	}

	public String getIndentCode() {
		return indentCode;
	}

	public void setIndentCode(String indentCode) {
		this.indentCode = indentCode;
	}

	public String getOutNo() {
		return outNo;
	}

	public void setOutNo(String outNo) {
		this.outNo = outNo;
	}

	public String getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSynchroState() {
		return synchroState;
	}

	public void setSynchroState(String synchroState) {
		this.synchroState = synchroState;
	}

	public String getPerFee() {
		return perFee;
	}

	public void setPerFee(String perFee) {
		this.perFee = perFee;
	}

	public String getPayFee() {
		return payFee;
	}

	public void setPayFee(String payFee) {
		this.payFee = payFee;
	}

}
