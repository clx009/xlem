package ws.pojo.response;

public class BalanceResponse extends BaseResponse {
	private String cashBalance;
	private String rewardBalance;

	public String getCashBalance() {
		return cashBalance;
	}

	public void setCashBalance(String cashBalance) {
		this.cashBalance = cashBalance;
	}

	public String getRewardBalance() {
		return rewardBalance;
	}

	public void setRewardBalance(String rewardBalance) {
		this.rewardBalance = rewardBalance;
	}

}
