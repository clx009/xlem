package ws.pojo.response;

import java.util.List;

import ws.pojo.WsPojo;

public class BaseResponses extends BaseResponse{
	private List<? extends WsPojo> wsPojos; 
	public List<? extends WsPojo> getWsPojos() {
		return wsPojos;
	}

	public void setWsPojos(List<? extends WsPojo> wsPojos) {
		this.wsPojos = wsPojos;
	}

}
