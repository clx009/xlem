package ws.pojo.response;


public class TicketPriceForMResponse extends TicketPriceResponse{
	private String residue;

	public String getResidue() {
		return residue;
	}

	public void setResidue(String residue) {
		this.residue = residue;
	}
	
}
