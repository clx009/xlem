package ws.pojo.response;

import java.sql.Timestamp;

import com.xlem.dao.HotelRoomType;

import ws.pojo.WsPojo;

public class HotelBookingLimitResponse extends WsPojo{

	// Fields

	private Long limitId;
	private HotelRoomType hotelRoomType;
	private Integer amount;
	private Timestamp startDate;
	private Timestamp endDate;
	private String status;
	private String remark;
	private Long inputer;
	private Timestamp inputTime;
	private Long updater;
	private Timestamp updateTime;
	private String synchroState;

	// Constructors

	/** default constructor */
	public HotelBookingLimitResponse() {
	}

	/** minimal constructor */
	public HotelBookingLimitResponse(Long limitId) {
		this.limitId = limitId;
	}

	/** full constructor */
	public HotelBookingLimitResponse(Long limitId, HotelRoomType hotelRoomType,
			Integer amount, Timestamp startDate, Timestamp endDate,
			String status, String remark, Long inputer, Timestamp inputTime,
			Long updater, Timestamp updateTime, String synchroState) {
		this.limitId = limitId;
		this.hotelRoomType = hotelRoomType;
		this.amount = amount;
		this.startDate = startDate;
		this.endDate = endDate;
		this.status = status;
		this.remark = remark;
		this.inputer = inputer;
		this.inputTime = inputTime;
		this.updater = updater;
		this.updateTime = updateTime;
		this.synchroState = synchroState;
	}

	public Long getLimitId() {
		return limitId;
	}

	public void setLimitId(Long limitId) {
		this.limitId = limitId;
	}

	public HotelRoomType getHotelRoomType() {
		return hotelRoomType;
	}

	public void setHotelRoomType(HotelRoomType hotelRoomType) {
		this.hotelRoomType = hotelRoomType;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getInputer() {
		return inputer;
	}

	public void setInputer(Long inputer) {
		this.inputer = inputer;
	}

	public Timestamp getInputTime() {
		return inputTime;
	}

	public void setInputTime(Timestamp inputTime) {
		this.inputTime = inputTime;
	}

	public Long getUpdater() {
		return updater;
	}

	public void setUpdater(Long updater) {
		this.updater = updater;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public String getSynchroState() {
		return synchroState;
	}

	public void setSynchroState(String synchroState) {
		this.synchroState = synchroState;
	}

	

}