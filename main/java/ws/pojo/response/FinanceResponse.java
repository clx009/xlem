package ws.pojo.response;

import java.sql.Timestamp;

import com.xlem.dao.BaseRechargeRec;

import common.CommonMethod;

import ws.pojo.WsPojo;


public class FinanceResponse extends WsPojo{
	
	private String indentCode;
	private String paymentTypeText;
	private String cashMoney;
	private String cashBalance;
	private String inputTime;
	private String outNo;
	
	public void rechargeToResponse(BaseRechargeRec brr) {
		this.setIndentCode(brr.getTicketIndent().getIndentCode());
		this.setPaymentTypeText(brr.getPaymentTypeText());
		this.setCashMoney(brr.getCashMoney()==null?null:brr.getCashMoney().toString());
		this.setCashBalance(brr.getCashBalance()==null?null:brr.getCashBalance().toString());
		this.setInputTime(CommonMethod.timeToString2(Timestamp.valueOf(brr.getInputTime().toString())));
		this.setOutNo(brr.getTicketIndent().getFaxNumber());		
	}
	
	public String getIndentCode() {
		return indentCode;
	}
	public void setIndentCode(String indentCode) {
		this.indentCode = indentCode;
	}
	public String getPaymentTypeText() {
		return paymentTypeText;
	}
	public void setPaymentTypeText(String paymentTypeText) {
		this.paymentTypeText = paymentTypeText;
	}
	public String getCashMoney() {
		return cashMoney;
	}
	public void setCashMoney(String cashMoney) {
		this.cashMoney = cashMoney;
	}
	public String getCashBalance() {
		return cashBalance;
	}
	public void setCashBalance(String cashBalance) {
		this.cashBalance = cashBalance;
	}
	public String getInputTime() {
		return inputTime;
	}
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	public String getOutNo() {
		return outNo;
	}
	public void setOutNo(String outNo) {
		this.outNo = outNo;
	}
	
}
