package ws.pojo.response;

import java.util.List;

public class FinanceResponses extends BaseResponse {
	private List<FinanceResponse> wsPojos;
	
	public List<FinanceResponse> getWsPojos() {
		return this.wsPojos;
	}
	
	public void setWsPojos(List<FinanceResponse> wsPojos) {
		this.wsPojos = wsPojos;
	}

}
