package ws.pojo.response;

public class TicketResponse extends BaseResponses {
	private String residue;

	public String getResidue() {
		return residue;
	}

	public void setResidue(String residue) {
		this.residue = residue;
	}

}
