package ws.pojo.response;

import ws.pojo.WsPojo;

public class TicketPriceResponse extends WsPojo {
	private String ticketPriceId;
	private String ticketId;
	private String ticketName;
	private String unitPrice;
	private String printPrice;
	private String startDate;
	private String endDate;
	private String intro;

	public String getTicketPriceId() {
		return ticketPriceId;
	}

	public void setTicketPriceId(String ticketPriceId) {
		this.ticketPriceId = ticketPriceId;
	}

	public String getTicketName() {
		return ticketName;
	}

	public void setTicketName(String ticketName) {
		this.ticketName = ticketName;
	}

	public String getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getPrintPrice() {
		return printPrice;
	}

	public void setPrintPrice(String printPrice) {
		this.printPrice = printPrice;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getTicketId() {
		return ticketId;
	}
	
}
