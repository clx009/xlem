package ws.pojo.response;

import ws.ReturnInfo;
import ws.pojo.WsPojo;

import java.sql.Timestamp;

import com.thoughtworks.xstream.XStream;
import com.xlem.dao.TicketIndent;

import common.CommonMethod;

public class TicketIndentResponse extends BaseResponse{
	private String indentId;
	private String indentCode;
	private String totalAmount;
	private String totalPrice;
	private String useDate;
	private String perFee;
	private String payFee;
	private String linkman;
	private String idCardNumber;
	private String phoneNumber;
	private String checkCode;
	private String status;
	private String inputTime;
	private String synchroState;
	private String cashBalance;

	public TicketIndentResponse indentToRes(TicketIndent ti){
		this.setCheckCode(ti.getCheckCode());
		this.setIdCardNumber(ti.getIdCardNumber());
		this.setIndentCode(ti.getIndentCode());
		this.setIndentId(ti.getIndentId());
		this.setInputTime(CommonMethod.timeToString2(Timestamp.valueOf(ti.getInputTime().toString())));
		this.setLinkman(ti.getLinkman());
		this.setPayFee(ti.getPayFee()==null?null:ti.getPayFee().toString());
		this.setPerFee(ti.getPerFee()==null?null:ti.getPerFee().toString());
		this.setPhoneNumber(ti.getPhoneNumber());
		this.setStatus(ti.getStatus());
		this.setSynchroState(ti.getSynchroState());
		this.setTotalAmount(ti.getTotalAmount().toString());
		this.setTotalPrice(ti.getTotalPrice().toString());
		this.setUseDate(CommonMethod.timeToString(Timestamp.valueOf(ti.getUseDate().toString())));
		return this;
	}
	
	public String getIndentId() {
		return indentId;
	}

	public void setIndentId(String indentId) {
		this.indentId = indentId;
	}

	public String getIndentCode() {
		return indentCode;
	}

	public void setIndentCode(String indentCode) {
		this.indentCode = indentCode;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getUseDate() {
		return useDate;
	}

	public void setUseDate(String useDate) {
		this.useDate = useDate;
	}

	public String getPerFee() {
		return perFee;
	}

	public void setPerFee(String perFee) {
		this.perFee = perFee;
	}

	public String getPayFee() {
		return payFee;
	}

	public void setPayFee(String payFee) {
		this.payFee = payFee;
	}

	public String getLinkman() {
		return linkman;
	}

	public void setLinkman(String linkman) {
		this.linkman = linkman;
	}

	public String getIdCardNumber() {
		return idCardNumber;
	}

	public void setIdCardNumber(String idCardNumber) {
		this.idCardNumber = idCardNumber;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCheckCode() {
		return checkCode;
	}

	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getInputTime() {
		return inputTime;
	}

	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}

	public String getSynchroState() {
		return synchroState;
	}

	public void setSynchroState(String synchroState) {
		this.synchroState = synchroState;
	}

	public String getCashBalance() {
		return cashBalance;
	}

	public void setCashBalance(String cashBalance) {
		this.cashBalance = cashBalance;
	}
	
}
