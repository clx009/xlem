package ws.hotel.comm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IpMap {
	
	/**
	 * 获取可通过IP
	 * 
	 * @return yyyy-MM-dd HH:mm:ss
	 */
	public static Map<String,String> getIps() {
		Map<String,String> map = new HashMap<String,String>();
		
		//正式IP
		map.put("192.168.2.171", "11");
		map.put("192.168.4.160", "11");
		//测试IP
		map.put("192.168.5.200", "00");
		map.put("192.168.5.20", "00");
		
		return map;
	}
	
	public static List<String> findHotelForIp(String ip) {
		Map<String,List<String>> map = new HashMap<String,List<String>>();
		
		//正式IP
		List<String> list171 = new ArrayList<String>();
		list171.add("SD");
		map.put("192.168.2.171", list171);
		List<String> list160 = new ArrayList<String>();
		list160.add("YX");
		list160.add("SCDN");
		map.put("192.168.4.160", list160);
		//测试IP
		List<String> list200 = new ArrayList<String>();
		list200.add("YX");
		map.put("192.168.5.200", list200);
		List<String> list20 = new ArrayList<String>();
		list20.add("YX");
		map.put("192.168.5.20", list20);
		
		return map.get(ip);
	}
}
