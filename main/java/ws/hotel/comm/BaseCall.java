package ws.hotel.comm;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;

import common.BookingConfig;
import common.BusinessException;
import common.CommonMethod;

public abstract class BaseCall {
	private static String URL_SFBY = BookingConfig.getInstance().getValue("xrws_sfby_url");
	private static String URL_SCDN = BookingConfig.getInstance().getValue("xrws_scdn_url");
	private static String URL_YX = BookingConfig.getInstance().getValue("xrws_yx_url");
	private static String URL_SD =BookingConfig.getInstance().getValue("xrws_sd_url");;

	protected String call(String input, String operationName,
			HotelCode hotelCode) {
		try {
			Service service = new Service();
			Call call = (Call) service.createCall();
			System.out.println(CommonMethod.getDate());
			System.out.println(getUrlByHotel(hotelCode));
			call.setTargetEndpointAddress(getUrlByHotel(hotelCode));
			call.setOperationName(operationName);// WSDL里面描述的接口名称
			call.addParameter("xml",
					org.apache.axis.encoding.XMLType.XSD_STRING,
					javax.xml.rpc.ParameterMode.IN);// 接口的参数
			call.setReturnType(org.apache.axis.encoding.XMLType.SOAP_STRING);// 设置返回类型
			String result = (String) call.invoke(new Object[] { input });
			return result;
		} catch (ServiceException e) {
			e.printStackTrace();
			throw new BusinessException("接口出错", e);
		} catch (RemoteException e) {
			e.printStackTrace();
			throw new BusinessException("接口出错", e);
		}
	}

	private String getUrlByHotel(HotelCode hotelCode) {
		switch (hotelCode) {
		case SFBY:
			return URL_SFBY;
		case SCDN:
			return URL_SCDN;
		case YX:
			return URL_YX;
		case SD:
			return URL_SD;
		default :
			return null;
		}
	}
}
