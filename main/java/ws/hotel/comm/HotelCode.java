package ws.hotel.comm;

public enum HotelCode {
	/**映雪酒店首峰别苑*/
	SFBY,
	/**斯堪的纳度假酒店*/
	SCDN,
	/**映雪酒店*/
	YX,
	/**山地度假酒店*/
	SD,
}
