package ws.hotel.comm;

public enum CallMethod {
	/**订单写入 */
	writeHotelIndent,
	/**订单修改*/
	updateHotelIndent,
	/**退订*/
	cancelHotelIndent,
	/**房型上限设置*/
	bookingLimit,
	/**实时房态查询*/
	realTimeQuery,
	/**房型*/
	synRoomType,
	/**同步消费记录*/
	synDetails,
	HelloWorld,
	ReadXmlToDaset,
	ReadXmlToDaset111,
	printBack
}
