package ws.hotel.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import ws.ReturnInfo;
import ws.hotel.comm.BaseCall;
import ws.hotel.comm.CallMethod;
import ws.hotel.comm.HotelCode;

import com.thoughtworks.xstream.XStream;
import com.xlem.dao.HotelBookingLimit;
import com.xlem.dao.HotelIndent;
import com.xlem.dao.HotelIndentDetail;
import com.xlem.dao.HotelRoomType;
import com.xlem.dao.RoomInfoQueryParam;

import common.BusinessException;

public class HotelIndentClient extends BaseCall {
	
	/**
	 * @param list1 订单数据
	 * @param hotelCode 
	 * @return
	 */
	public ReturnInfo writeHotelIndent(List<HotelIndent> list, HotelCode hotelCode){
		for (HotelIndent hotelIndent : list) {
			if(hotelIndent != null){
				Set<HotelIndentDetail> hid = hotelIndent.getHotelIndentDetails();
				for (HotelIndentDetail hotelIndentDetail : hid) {
					if(hotelIndentDetail != null){
						HotelRoomType hrt = hotelIndentDetail.getHotelRoomType();
						if(hrt != null){
							hrt.setHotelPriceLimitDetails(null);
						}
					}
				}
				hotelIndent.setHotelCutNoteses(null);
			}
		}
		XStream xsStream = new XStream();
		xsStream.alias("obj", HotelIndent.class);
		xsStream.alias("root", List.class);
		//System.out.println(xsStream.toXML(list));
		String rstr = call(xsStream.toXML(list), CallMethod.writeHotelIndent.name(), hotelCode);
		//System.out.println(rstr);
		XStream xstream = new XStream();
		xstream.alias("ReturnInfo", ReturnInfo.class);
		ReturnInfo ri = (ReturnInfo) xstream.fromXML(rstr);
		return ri;
	}

	/**
	 * 以行为单位读取文件，常用于读面向行的格式化文件
	 * 
	 * @param fileName
	 *            文件名
	 */
	public static String readFileByLines(String fileName) {
		File file = new File(fileName);
		BufferedReader reader = null;
		StringBuffer xml = new StringBuffer();
		try {
			reader = new BufferedReader(new FileReader(file));
			String tempString = null;
			int line = 1;
			// 一次读入一行，直到读入null为文件结束
			while ((tempString = reader.readLine()) != null) {
				// 显示行号
				xml.append(tempString+"\n");
				line++;
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e1) {
				}
			}
		}
		return xml.toString();
	}

	public static void main(String[] args) {
		String xml = readFileByLines("d:\\test.xml");
		System.out.println(xml);
		HotelIndentClient hic = new HotelIndentClient();
		hic.call(xml, CallMethod.writeHotelIndent.name(), HotelCode.YX);
		
	}
	/**
	 * 修改订单
	 * @param hotelIndent
	 * @return
	 */
	public ReturnInfo updateIndent(HotelIndent hotelIndent){
		if(hotelIndent != null){
			Set<HotelIndentDetail> hid = hotelIndent.getHotelIndentDetails();
			for (HotelIndentDetail hotelIndentDetail : hid) {
				if(hotelIndentDetail != null){
					HotelRoomType hrt = hotelIndentDetail.getHotelRoomType();
					if(hrt != null){
						hrt.setHotelPriceLimitDetails(null);
					}
				}
			}
			hotelIndent.setHotelCutNoteses(null);
		}
		XStream xsStream = new XStream();
		xsStream.alias("obj", HotelIndent.class);
		//System.out.println(xsStream.toXML(hotelIndent));
		String rstr = call(xsStream.toXML(hotelIndent), CallMethod.updateHotelIndent.name(), HotelCode.valueOf(hotelIndent.getHotel().getHotelCode()));
		XStream xstream = new XStream();
		xstream.alias("ReturnInfo", ReturnInfo.class);
		ReturnInfo ri = (ReturnInfo) xstream.fromXML(rstr);
		return ri;
	}
	
	
	/**
	 * 同步订房上限
	 * @param hotelIndent
	 * @return
	 */
	public ReturnInfo bookingLimit(List<HotelBookingLimit> bookingLimits,HotelCode hotelCode){
		for (HotelBookingLimit hbl : bookingLimits) {
			if(hbl != null){
				HotelRoomType hrt = hbl.getHotelRoomType();
				if(hrt != null){
					hrt.setHotelPriceLimitDetails(null);
				}
			}
		}
		XStream xsStream = new XStream();
		xsStream.alias("obj", HotelBookingLimit.class);
		xsStream.alias("root", List.class);
		//System.out.println(xsStream.toXML(bookingLimits));
		String rstr = call(xsStream.toXML(bookingLimits), CallMethod.bookingLimit.name(), hotelCode);
		XStream xstream = new XStream();
		xstream.alias("ReturnInfo", ReturnInfo.class);
		ReturnInfo ri = (ReturnInfo) xstream.fromXML(rstr);
		return ri;
	}
	
	
	/**
	 * 取消订单
	 * @param hotelIndent
	 * @return
	 */
	public ReturnInfo CancelHotelIndent(List<HotelIndent> list,HotelCode hotelCode) {
		for (HotelIndent hotelIndent : list) {
			if(hotelIndent != null){
//				Set<HotelIndentDetail> hid = hotelIndent.getHotelIndentDetails();
//				for (HotelIndentDetail hotelIndentDetail : hid) {
//					if(hotelIndentDetail != null){
//						HotelRoomType hrt = hotelIndentDetail.getHotelRoomType();
//						if(hrt != null){
//							hrt.setHotelPriceLimitDetails(null);
//						}
//					}
//				}
				hotelIndent.setHotelCutNoteses(null);
			}
		}
		XStream xsStream = new XStream();
		xsStream.alias("obj", HotelIndent.class);
		xsStream.alias("root", List.class);
		//System.out.println(xsStream.toXML(list));
		String rstr = call(xsStream.toXML(list), CallMethod.cancelHotelIndent.name(), hotelCode);
		//System.out.println(rstr);
		XStream xstream = new XStream();
		xstream.alias("ReturnInfo", ReturnInfo.class);
		ReturnInfo ri = (ReturnInfo) xstream.fromXML(rstr);
		return ri;
	}
	
	/**
	 * 
	 * @param param 封装查询条件
	 * @param hotelCode 对应的酒店类型，调用不同的Webservice
	 * @return 
	 */
	public  ReturnInfo getRealTimeRoomInfo(RoomInfoQueryParam param,HotelCode hotelCode){
		
			if(param != null){
				List<HotelRoomType> hrt = param.getHotelRoomTypes();
				for (HotelRoomType hotelRoomType : hrt) {
					hotelRoomType.setHotelPriceLimitDetails(null);
				}
			}
			XStream paramXS=new XStream();
			paramXS.alias("param",RoomInfoQueryParam.class);
			paramXS.alias("collection",List.class);
			paramXS.alias("room_type",HotelRoomType.class);
			String inputParam=paramXS.toXML(param);
			String retInfoXML=this.call(inputParam, CallMethod.realTimeQuery.name(),hotelCode);
			XStream retInfoXStream=new XStream();
			retInfoXStream.alias("ReturnInfo",ReturnInfo.class);
			ReturnInfo retInfo=(ReturnInfo)retInfoXStream.fromXML(retInfoXML);
			if("-1".equals(retInfo.getResult().trim())){
				throw new BusinessException(retInfo.getError());
			}
			return retInfo;
	}
	
	/**
	 * 同步房型
	 * @param hotelIndent
	 * @return
	 */
	public ReturnInfo synRoomType(HotelCode hotelCode) {
		XStream xsStream = new XStream();
		String rstr = call(hotelCode.name(), CallMethod.synRoomType.name(), hotelCode);
		//System.out.println(rstr);
		xsStream.alias("ReturnInfo", ReturnInfo.class);
		ReturnInfo ri = (ReturnInfo) xsStream.fromXML(rstr);
		return ri;
	}
	
	/**
	 * 同步房型
	 * @param hotelIndent
	 * @return
	 */
	public ReturnInfo synDetails(HotelCode hotelCode) {
		XStream xsStream = new XStream();
		String rstr = call(hotelCode.name(), CallMethod.synDetails.name(), hotelCode);
		//System.out.println(rstr);
		xsStream.alias("ReturnInfo", ReturnInfo.class);
		ReturnInfo ri = (ReturnInfo) xsStream.fromXML(rstr);
		return ri;
	}
}
