package ws.hotel.pojo;

import java.io.UnsupportedEncodingException;

public class RoomType {
	private String type;
	private String descript;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescript() {
		try {
			return new String(descript.getBytes("cp850"),"gbk");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return descript;
		}
	}
	public void setDescript(String descript) {
		this.descript = descript;
	}
}
