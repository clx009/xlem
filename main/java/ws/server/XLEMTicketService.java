package ws.server;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.rpc.ServiceException;

import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.jaxrpc.ServletEndpointSupport;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import service.comm.baseCashAccount.BaseCashAccountService;
import service.comm.baseRechargeRec.BaseRechargeRecService;
import service.comm.baseRewardAccount.BaseRewardAccountService;
import service.comm.baseTranRec.BaseTranRecService;
import service.comm.reg.baseTravelAgency.BaseTravelAgencyService;
import service.sys.user.UserService;
import service.ticket.ticketAddPay.TicketAddPayService;
import service.ticket.ticketBarcodeRec.TicketBarcodeRecService;
import service.ticket.ticketIndent.TicketIndentService;
import service.ticket.ticketIndentDetail.TicketIndentDetailService;
import service.ticket.ticketMemberList.TicketMemberListService;
import service.ticket.ticketPrice.TicketPriceService;
import service.ticket.ticketType.TicketTypeService;
import ws.ReturnInfo;
import ws.pojo.request.BalanceRequest;
import ws.pojo.request.FinanceRequest;
import ws.pojo.request.QueryTicketRequest;
import ws.pojo.request.ReturnTicketRequest;
import ws.pojo.request.TicketIndentDetailRequest;
import ws.pojo.request.TicketIndentRequest;
import ws.pojo.request.TicketPriceRequest;
import ws.pojo.response.BalanceResponse;
import ws.pojo.response.BaseResponses;
import ws.pojo.response.FinanceResponse;
import ws.pojo.response.FinanceResponses;
import ws.pojo.response.ReturnTicketResponse;
import ws.pojo.response.TicketIndentResponse;
import ws.pojo.response.TicketPriceResponse;
import ws.pojo.response.TicketPriceResponses;

import com.thoughtworks.xstream.XStream;
import com.xlem.dao.BaseCashAccount;
import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.BaseRewardAccount;
import com.xlem.dao.BaseTranRec;
import com.xlem.dao.TicketIndent;
import com.xlem.dao.TicketIndentDetail;
import com.xlem.dao.TicketPrice;
import com.xlem.dao.TicketType;
import com.xlem.dao.Travservice;
import com.xlem.dao.User;
import com.xlem.utils.RESTFulTicket;
import com.xlem.utils.RESTFulTicketIndent;
import com.xlem.utils.RESTFulTicketIndents;
import com.xlem.utils.RESTFulTickets;
import com.xlem.utils.SeriesNumber;
import com.xlem.utils.SystemSetting;
import com.xlem.utils.TicketPoolService;
import com.xlem.web.JSONTicket;

import common.CommonMethod;
import dao.ticket.ticketIndent.TicketIndentDao;
import dao.ticket.ticketIndentDetail.TicketIndentDetailDao;

@Service("wsXLEMTicketService")
public class XLEMTicketService extends ServletEndpointSupport{
	private static Logger logger = Logger.getLogger("xlemTicketService");
	
	@Autowired
	private SystemSetting systemSetting;

	@Autowired
	private UserService userService;
	@Autowired
	private BaseTravelAgencyService baseTravelAgencyService;
	@Autowired
	private TicketPriceService ticketPriceService;
	@Autowired
	private TicketTypeService ticketTypeService;
	@Autowired
	private TicketIndentDao ticketIndentDao;
	@Autowired
	private TicketIndentDetailDao ticketIndentDetailDao;
	@Autowired
	private TicketIndentService ticketIndentService;
	@Autowired
	private TicketIndentDetailService ticketIndentDetailService;
	@Autowired
	private BaseCashAccountService baseCashAccountService;
	@Autowired
	private BaseRewardAccountService baseRewardAccountService;
	@Autowired
	private BaseTranRecService baseTranRecService;
	@Autowired
	private BaseRechargeRecService baseRechargeRecService;
	@Autowired
	private TicketPoolService ticketPoolService;
	
	private RESTFulTickets restTickets = null;
	private List<RESTFulTicket> tickets = null;
	
	@Override
	protected void onInit() throws ServiceException {
		initTicketList();
	}
	
	private void initTicketList() {
		// call xlpw web service here
		final String uri = systemSetting.getXlpwUrl() + "/rest/ticket/list";
		
	    RestTemplate restTemplate = new RestTemplate();
	     
	    restTickets = restTemplate.getForObject(uri, RESTFulTickets.class);
	    tickets = restTickets.getRESTFulTickets();
	    if (tickets == null) {
	    	return;
	    }
	    
		for (RESTFulTicket ticket : tickets) {
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(TicketType.class);
			detachedCriteria.add(Property.forName("ticketName").eq(ticket.getName()));
			List<TicketType> ticketTypes = ticketTypeService.findByCriteria(detachedCriteria);
			
			if (ticketTypes.size() > 0) {
				DetachedCriteria dc = DetachedCriteria.forClass(TicketPrice.class);
				dc.add(Property.forName("ticketType").eq(ticketTypes.get(0)));
				List<TicketPrice> ticketPrices = ticketPriceService.findByCriteria(dc);
				TicketPrice ticketPrice = ticketPrices.get(0);
				
				if (ticket.getInfo() != null) {
					if (ticketTypes.get(0).getIntro() == null 
							|| !ticketTypes.get(0).getIntro().equals(ticket.getInfo())) {
						TicketType ticketType = ticketTypes.get(0);
						ticketType.setIntro(ticket.getInfo());
						ticketTypeService.update(ticketType);
					}
				}
				
				if (!ticket.getPrice().equals(ticketPrice.getPrintPrice())
						|| !ticket.getStart().equals(ticketPrice.getStartDate())
						|| !ticket.getEnd().equals(ticketPrice.getEndDate())) {
					ticketPrice.setPrintPrice(ticket.getPrice());
					ticketPrice.setStartDate(ticket.getStart());
					ticketPrice.setEndDate(ticket.getEnd());
					ticketPriceService.update(ticketPrice);
				}			
			}
			else {
				TicketType ticketType = new TicketType(ticket.getName(), ticket.getInfo());
				ticketTypeService.save(ticketType);
				TicketPrice ticketPrice = new TicketPrice(ticketType, ticket.getPrice(), ticket.getPrice(), ticket.getStart(), ticket.getEnd());
				ticketPrice.setToplimit(5000);
				ticketPrice.setRemainToplimit(5000);
				ticketPriceService.save(ticketPrice);
			}
		}	    
	}	
	
	/**
	 * 票种查询
	 * queryTicket
	 * @param xml
	 * @return
	 */
	public String queryTicket(String xml){
		System.out.println(xml);
		ReturnInfo ri = new ReturnInfo();
		XStream xsRi = new XStream();
		TicketPriceRequest request = new TicketPriceRequest();
		TicketPriceResponses response = new TicketPriceResponses();

		try {
			xsRi.alias("request", TicketPriceRequest.class);
			request = (TicketPriceRequest) xsRi.fromXML(xml);
			
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(User.class);
			detachedCriteria.add(Property.forName("name").eq(request.getUserName()));
			List<User> users = userService.findByCriteria(detachedCriteria);
			
			if (!users.isEmpty()) {
				User user = users.get(0);
				if (user.getPwd().equals(request.getActivePW())) {
					detachedCriteria = DetachedCriteria.forClass(Travservice.class);
					detachedCriteria.add(Property.forName("user").eq(user));
					List<Travservice> travelAgencies = baseTravelAgencyService.findByCriteria(detachedCriteria);
					if (!travelAgencies.isEmpty()) {
						response.setResult("0");
						
						DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
						Date useDate = fmt.parse(request.getUseDate());
						DetachedCriteria criteria = DetachedCriteria.forClass(TicketPrice.class);
						criteria.add(Property.forName("startDate").le(useDate));
						criteria.add(Property.forName("endDate").ge(useDate));
						List<TicketPrice> ticketPrices = ticketPriceService.findByCriteria(criteria);
						
						List<TicketPriceResponse> wsPojos = new ArrayList<TicketPriceResponse>();
						for (TicketPrice ticketPrice : ticketPrices) {
							TicketPriceResponse wsPojo = new TicketPriceResponse();
							wsPojo.setTicketPriceId(ticketPrice.getTicketPriceId().toString());
							wsPojo.setTicketId(ticketPrice.getTicketType().getTicketId().toString());
							wsPojo.setTicketName(ticketPrice.getTicketType().getTicketName());
							wsPojo.setUnitPrice(ticketPrice.getUnitPrice().toString());
							wsPojo.setPrintPrice(ticketPrice.getPrintPrice().toString());
							wsPojo.setStartDate(ticketPrice.getStartDate().toString());
							wsPojo.setEndDate(ticketPrice.getEndDate().toString());
							wsPojo.setIntro(ticketPrice.getTicketType().getIntro());
							
							wsPojos.add(wsPojo);
						}
						
						response.setWsPojos(wsPojos);
					}
					else {
						response.setResult("-1");
						response.setError("此接口只向旅行社开放");
					}
				}
				else {
					response.setResult("-1");
					response.setError("密码错误");
				}
			}
			else {
				response.setResult("-1");
				response.setError("用户不存在");
			}
		
			XStream xsStream = new XStream();
			xsStream.alias("response", TicketPriceResponses.class);
			return xsStream.toXML(response);
		} catch (Exception e) {
			logger.error("queryTicket接口出错，错误原因："+e.getMessage());
			e.printStackTrace();
			ri.setResult("-1");
			ri.setError(e.getMessage());
		}
		
		return xsRi.toXML(ri);
	}
	
	/**
	 * 单笔购票
	 * buyTIcketByType
	 * @param xml
	 * @return
	 */
	public String buyTicketByType(String xml){
		System.out.println(xml);
		ReturnInfo ri = new ReturnInfo();
		XStream xsRi = new XStream();
		TicketIndentRequest request = new TicketIndentRequest();
		TicketIndentResponse response = new TicketIndentResponse();
		
		DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");

		try {
			xsRi.alias("request", TicketIndentRequest.class);
			request = (TicketIndentRequest) xsRi.fromXML(xml);
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(User.class);
			detachedCriteria.add(Property.forName("name").eq(request.getUserName()));
			List<User> users = userService.findByCriteria(detachedCriteria);
			
			if (!users.isEmpty()) {
				User user = users.get(0);
				if (user.getPwd().equals(request.getActivePW())) {
					detachedCriteria = DetachedCriteria.forClass(Travservice.class);
					detachedCriteria.add(Property.forName("user").eq(user));
					List<Travservice> travelAgencies = baseTravelAgencyService.findByCriteria(detachedCriteria);
					if (!travelAgencies.isEmpty()) {
						response.setResult("0");
			
						TicketIndent ticketIndent = new TicketIndent(
								ticketIndentDao.getNextCode("TicketIndent", "indentId", "00000000000000000000000000000000", null));
						ticketIndent.setOutNo(request.getOutNo());
			
						List<TicketIndentDetail> ticketIndentDetails = new ArrayList<TicketIndentDetail>();
						Set<TicketIndentDetail> indentDetails = new HashSet<TicketIndentDetail>();
						for (TicketIndentDetailRequest detailRequest: request.getDetails()) {
							DetachedCriteria dc = DetachedCriteria.forClass(TicketType.class);
							dc.add(Property.forName("ticketId").eq(Long.valueOf(detailRequest.getTicketId())));
							List<TicketType> ticketTypes = ticketTypeService.findByCriteria(dc);
							TicketType ticketType = ticketTypes.get(0);
				
							dc = DetachedCriteria.forClass(TicketPrice.class);
							dc.add(Property.forName("ticketType").eq(ticketType));
							dc.add(Property.forName("ticketPriceId").eq(Long.valueOf(detailRequest.getTicketPriceId())));
							List<TicketPrice> ticketPrices = ticketPriceService.findByCriteria(dc);
							TicketPrice ticketPrice = ticketPrices.get(0);
					
							TicketIndentDetail ticketIndentDetail = new TicketIndentDetail(
												ticketIndentDetailDao.getNextIndentCode("00000000000000000000000000000000", "000000"));
							
							
							ticketIndentDetail.setTicketIndent(ticketIndent);
							ticketIndentDetail.setTicketPrice(ticketPrice);
							ticketIndentDetail.setAmount(Integer.valueOf(detailRequest.getAmount()));
							ticketIndentDetail.setUseDate(fmt.parse(request.getUseDate()));
							ticketIndentDetail.setSubtotal(ticketPrice.getUnitPrice() * Integer.valueOf(detailRequest.getAmount()));
					
							indentDetails.add(ticketIndentDetail);
							ticketIndentDetails.add(ticketIndentDetail);
						}
						ticketIndent.setDetails(ticketIndentDetails);
						ticketIndent.setTicketIndentDetails(indentDetails);			
						
						for (TicketIndentDetail indentDetail : ticketIndentDetails) {
							if (indentDetail.getTicketPrice().getRemainToplimit() < indentDetail.getAmount()) {
								ri.setResult("-1");
								ri.setError("预定日票额不足");
								xsRi.alias("response", ReturnInfo.class);
								return xsRi.toXML(ri);
							}
								
							detachedCriteria = DetachedCriteria.forClass(TicketIndentDetail.class);
							detachedCriteria.add(Property.forName("status").eq("01"));
							detachedCriteria.add(Property.forName("useDate").eq(indentDetail.getUseDate()));
							List<TicketIndentDetail> effectiveDetails = ticketIndentDetailService.findByCriteria(detachedCriteria);
							
							Integer effectiveAmount = 0;
							for (TicketIndentDetail effect : effectiveDetails) {
								effectiveAmount += effect.getAmount();
							}
								
							if (effectiveAmount + indentDetail.getAmount() > indentDetail.getTicketPrice().getToplimit()) {
								ri.setResult("-1");
								ri.setError("游玩日票额不足");
								xsRi.alias("response", ReturnInfo.class);
								return xsRi.toXML(ri);
							}
						}
							
						for (TicketIndentDetail indentDetail : ticketIndentDetails) {
							TicketPrice ticketPrice = indentDetail.getTicketPrice();
							ticketPrice.setRemainToplimit(ticketPrice.getRemainToplimit() - indentDetail.getAmount());
							ticketPriceService.update(ticketPrice);
						}
						
						Double totalPrice = 0.00;
						for (TicketIndentDetail indentDetail : ticketIndentDetails) {
							// ticketIndentDetailDao.save(indentDetail);
							indentDetail.setInputer(user.getUserId());
							indentDetail.setInputTime(new Date());
							indentDetail.setStatus("00");
							indentDetails.add(indentDetail);
							totalPrice += indentDetail.getSubtotal();
						}
						
						ticketIndent.setDetails(ticketIndentDetails);
						ticketIndent.setTicketIndentDetails(indentDetails);
						ticketIndent.setLinkman(request.getLinkman());
						ticketIndent.setIdCardNumber(request.getIdCardNumber());
						ticketIndent.setPhoneNumber(request.getPhoneNumber());
						ticketIndent.setBookDate(new Date());
						ticketIndent.setStatus("00");
						ticketIndent.setTotalPrice(totalPrice);
						ticketIndent.setIndentCode(ticketIndent.getIndentId().substring(12));
						ticketIndent.setInputer(user.getUserId());
						ticketIndent.setInputTime(new Date());
							
						ticketIndent.setUser(user);
						
						Travservice travservice = travelAgencies.get(0);
									
						String userType = "travelAgency";	
									
						DetachedCriteria criteria = DetachedCriteria.forClass(BaseCashAccount.class);
						criteria.add(Property.forName("baseTravelAgency").eq(travservice));
						List<BaseCashAccount> cashAccounts = baseCashAccountService.findByCriteria(criteria);
									
						Double cashBalance = cashAccounts.get(0).getCashBalance();
									
						criteria = DetachedCriteria.forClass(BaseRewardAccount.class);
						criteria.add(Property.forName("baseTravelAgency").eq(travservice));
						List<BaseRewardAccount> rewardAccounts = baseRewardAccountService.findByCriteria(criteria);
									
						Double rewardBalance = rewardAccounts.get(0).getRewardBalance();
									
						if (cashBalance >= totalPrice) {
							for (TicketIndentDetail indentDetail : ticketIndent.getTicketIndentDetails()) {
								BaseTranRec baseTranRec = new BaseTranRec();
								baseTranRec.setTicketIndent(ticketIndent);
								baseTranRec.setTicketIndentDetail(indentDetail);
								baseTranRec.setTrxId(indentDetail.getIndentDetailId());
								baseTranRec.setMoney(indentDetail.getSubtotal());
								baseTranRec.setPayFee(indentDetail.getSubtotal());
								baseTranRec.setPerFee(indentDetail.getPerFee());
								baseTranRec.setCardNumber(String.valueOf(travservice.getTravelAgencyId()));
								baseTranRec.setBank("intralAccount");
								baseTranRec.setAccountName(String.valueOf(travservice.getTravelAgencyId()));
								baseTranRec.setPaymentType("00"); //支付
								Date paymentTime = new Date();
								baseTranRec.setPaymentTime(paymentTime);
								baseTranRec.setPayType("01"); //intral account payment
						    			
								baseTranRecService.save(baseTranRec);
							}
										
							BaseCashAccount newBalance = cashAccounts.get(0);
							newBalance.setCashBalance(newBalance.getCashBalance() - totalPrice);
							baseCashAccountService.update(newBalance);
										
							travservice.setCashBalance(travservice.getCashBalance() - totalPrice);
							baseTravelAgencyService.update(travservice);
							
							ticketIndent.setStatus("01");
							ticketIndent.setCustomerType("01");
							ticketIndentService.save(ticketIndent);
							
							for (TicketIndentDetail indentDetail : ticketIndent.getTicketIndentDetails()) {
								indentDetail.setStatus("01");
								ticketIndentDetailService.save(indentDetail);
							}
						
				    		String smsCode = SeriesNumber.RandomString(6);
				    		String smsContent = "您所预订的订单已成功预订，取票验证码为：" + smsCode; 
				    		CommonMethod.sendSms(ticketIndent.getPhoneNumber(), smsContent, "true");
										
							response.setIndentId(ticketIndent.getIndentId());
							response.setIndentCode(ticketIndent.getIndentCode());
							response.setTotalAmount(ticketIndent.getTotalAmount().toString());
							response.setTotalPrice(ticketIndent.getTotalPrice().toString());
							response.setUseDate(ticketIndent.getUseDate().toString());
							response.setPerFee(ticketIndent.getPerFee().toString());
							response.setPayFee(ticketIndent.getPayFee().toString());
							response.setLinkman(ticketIndent.getLinkman());
							response.setIdCardNumber(ticketIndent.getIdCardNumber());
							response.setPhoneNumber(ticketIndent.getPhoneNumber());
							response.setCheckCode(smsCode);
							response.setInputTime(ticketIndent.getInputTime().toString());
							response.setStatus("03");
							response.setCashBalance(newBalance.toString());
							
							XStream xsStream = new XStream();
							xsStream.alias("response", TicketIndentResponse.class);
							return xsStream.toXML(response);
						}
						else if (rewardBalance >= totalPrice) {
							for (TicketIndentDetail indentDetail : ticketIndent.getTicketIndentDetails()) {
								BaseTranRec baseTranRec = new BaseTranRec();
								baseTranRec.setTicketIndent(ticketIndent);
								baseTranRec.setTicketIndentDetail(indentDetail);
								baseTranRec.setTrxId(indentDetail.getIndentDetailId());
								baseTranRec.setMoney(indentDetail.getSubtotal());
								baseTranRec.setPayFee(indentDetail.getSubtotal());
								baseTranRec.setPerFee(indentDetail.getPerFee());
								baseTranRec.setCardNumber(String.valueOf(travservice.getTravelAgencyId()));
								baseTranRec.setBank("intralAccount");
								baseTranRec.setAccountName(String.valueOf(travservice.getTravelAgencyId()));
								baseTranRec.setPaymentType("00"); //支付
								Date paymentTime = new Date();
								baseTranRec.setPaymentTime(paymentTime);
								baseTranRec.setPayType("02"); //reward account payment
						    	
								baseTranRecService.save(baseTranRec);
							}
										
							BaseRewardAccount newRewardBalance = rewardAccounts.get(0);
							newRewardBalance.setRewardBalance(newRewardBalance.getRewardBalance() - totalPrice);
							baseRewardAccountService.update(newRewardBalance);
										
							ticketIndent.setStatus("01");
							ticketIndent.setCustomerType("01");
							ticketIndentService.save(ticketIndent);
							
							for (TicketIndentDetail indentDetail : ticketIndent.getTicketIndentDetails()) {
								indentDetail.setStatus("01");
								ticketIndentDetailService.save(indentDetail);
							}
						
				    		String smsCode = SeriesNumber.RandomString(6);
				    		String smsContent = "您所预订的订单已成功预订，取票验证码为：" + smsCode; 
				    		CommonMethod.sendSms(ticketIndent.getPhoneNumber(), smsContent, "true");
										
							response.setIndentId(ticketIndent.getIndentId());
							response.setIndentCode(ticketIndent.getIndentCode());
							response.setTotalAmount(ticketIndent.getTotalAmount().toString());
							response.setTotalPrice(ticketIndent.getTotalPrice().toString());
							response.setUseDate(ticketIndent.getUseDate().toString());
							response.setPerFee(ticketIndent.getPerFee().toString());
							response.setPayFee(ticketIndent.getPayFee().toString());
							response.setLinkman(ticketIndent.getLinkman());
							response.setIdCardNumber(ticketIndent.getIdCardNumber());
							response.setPhoneNumber(ticketIndent.getPhoneNumber());
							response.setCheckCode(smsCode);
							response.setInputTime(ticketIndent.getInputTime().toString());
							response.setStatus("03");
							response.setCashBalance(newRewardBalance.toString());
							
							XStream xsStream = new XStream();
							xsStream.alias("response", TicketIndentResponse.class);
							return xsStream.toXML(response);
						}
					}
					else {
						ri.setResult("-1");
						ri.setError("此接口只向旅行社开放");
					}
				}
				else {
					ri.setResult("-1");
					ri.setError("密码错误");
				}
			}
			else {
				ri.setResult("-1");
				ri.setError("用户不存在");
			}
			
			xsRi.alias("response", ReturnInfo.class);
			return xsRi.toXML(ri);
		} catch (Exception e) {
			logger.error("queryTicket接口出错，错误原因："+e.getMessage());
			e.printStackTrace();
			ri.setResult("-1");
			ri.setError(e.getMessage());
		}
		
		return xsRi.toXML(ri);
	}
	
	/**
	 * 单笔订单退款
	 * returnTicket
	 * @param xml
	 * @return
	 */
	public String returnTicket(String xml){
		System.out.println(xml);
		ReturnInfo ri = new ReturnInfo();
		XStream xsRi = new XStream();
		ReturnTicketRequest request = new ReturnTicketRequest();
		ReturnTicketResponse response = new ReturnTicketResponse();
		
		DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");

		try {
			xsRi.alias("request", ReturnTicketRequest.class);
			request = (ReturnTicketRequest) xsRi.fromXML(xml);
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(User.class);
			detachedCriteria.add(Property.forName("name").eq(request.getUserName()));
			List<User> users = userService.findByCriteria(detachedCriteria);
			
			if (!users.isEmpty()) {
				User user = users.get(0);
				if (user.getPwd().equals(request.getActivePW())) {
					detachedCriteria = DetachedCriteria.forClass(Travservice.class);
					detachedCriteria.add(Property.forName("user").eq(user));
					List<Travservice> travelAgencies = baseTravelAgencyService.findByCriteria(detachedCriteria);
					if (!travelAgencies.isEmpty()) {
						DetachedCriteria criteria = DetachedCriteria.forClass(TicketIndent.class);
						criteria.add(Property.forName("indentCode").eq(request.getIndentCode()));
						List<TicketIndent> ticketIndents = ticketIndentService.findByCriteria(criteria);
						if (!ticketIndents.isEmpty()) {
							TicketIndent ticketIndent = ticketIndents.get(0);
							for (TicketIndentDetail indentDetail : ticketIndent.getTicketIndentDetails()) {
								if (!ticketIndent.getStatus().equals("01")) {
									ri.setResult("-1");
									ri.setError("订单未支付，不能退票");
									xsRi.alias("response", ReturnInfo.class);
									return xsRi.toXML(ri);
								}
									
								Date now = new Date();
								Long diff = indentDetail.getUseDate().getTime() - now.getTime();
								Long hours = diff / (1000 * 60 * 60);
								if (hours < 24) {
									ri.setResult("-1");
									ri.setError("距离游玩时间小于24小时不能退票");
									xsRi.alias("response", ReturnInfo.class);
									return xsRi.toXML(ri);
								}
							}
							
							for (TicketIndentDetail indentDetail : ticketIndent.getTicketIndentDetails()) {
								boolean refunded = ticketIndentDetailService.saveReturn(indentDetail);
									
								if (refunded) {
									User tuser = indentDetail.getTicketIndent().getUser();
									if (tuser.getAccumulate() != null) {
										Integer point = tuser.getAccumulate();
										point -= indentDetail.getSubtotal().intValue();
										tuser.setAccumulate(point);
									}
								}
									
								if (refunded) {
							    	List<RESTFulTicketIndent> restTicketIndents = new ArrayList<RESTFulTicketIndent>();
							    	Set<TicketIndentDetail> indentDetails = ticketIndent.getTicketIndentDetails();
							    	RESTFulTicketIndent restTicketIndent = new RESTFulTicketIndent();
							    	String orderid = ticketIndent.getIndentId();
							    	orderid = orderid.substring(12) + indentDetail.getIndentDetailId();
							    	restTicketIndent.setOrderid(orderid);
							    	restTicketIndent.setUname(ticketIndent.getLinkman());
							    	restTicketIndent.setUid(ticketIndent.getIdCardNumber());
							    	restTicketIndent.setMphone(ticketIndent.getPhoneNumber());
							    	restTicketIndent.setVcode(null);
							    	for (RESTFulTicket ticket : tickets) {
							    		if (ticket.getName().equals(indentDetail.getTicketPrice().getTicketType().getTicketName())) {
							    			restTicketIndent.setPid(ticket.getPid());
							    			if (ticket.getIsSuite()) {
							    				restTicketIndent.setSname(ticket.getName());
							    			}
							    			break;
							    		}
							    	}
							    	restTicketIndent.setCount(indentDetail.getAmount());
							    	if (ticketIndent.getDiscount() != null) {
							    		restTicketIndent.setDiscount(ticketIndent.getDiscount());
							    	}
							    	else {
							    		restTicketIndent.setDiscount("1.0");
							    	}
							    	restTicketIndent.setPlaydate(indentDetail.getUseDate());
							    	restTicketIndent.setPaytype("alipay");
							    			
							    	restTicketIndents.add(restTicketIndent);
							    		
							    	// call xlpw web service here
							    	final String uri = systemSetting.getXlpwUrl() + "/rest/ticket/cancelBook";
							    		
							    	RestTemplate restTemplate = new RestTemplate();
							    	    
							    	RESTFulTicketIndents indents = new RESTFulTicketIndents(restTicketIndents); 
							    	RESTFulTicketIndents result = restTemplate.postForObject(uri, indents, RESTFulTicketIndents.class);
							    	
							        System.out.println(">>>>>成功完成票务订单退票：" + indentDetail.getIndentDetailId());  
								}
								else {
									ri.setResult("-1");
									ri.setError("发生内部错误");
									xsRi.alias("response", ReturnInfo.class);
									return xsRi.toXML(ri);
								}
							}
								
							response.setResult("0");
							response.setIndentId(ticketIndent.getIndentId());
							response.setIndentCode(ticketIndent.getIndentCode());
							response.setOutNo(request.getOutNo());
							response.setTotalPrice(ticketIndent.getTotalPrice().toString());
							response.setPerFee(ticketIndent.getPerFee().toString());
							response.setPayFee(ticketIndent.getPayFee().toString());
							response.setStatus("08");
							response.setSynchroState("");
							
							XStream xsStream = new XStream();
							xsStream.alias("response", ReturnTicketResponse.class);
							return xsStream.toXML(response);
						}
					}
					else {
						ri.setResult("-1");
						ri.setError("此接口只向旅行社开放");
					}
				}
				else {
					ri.setResult("-1");
					ri.setError("密码错误");
				}
			}
			else {
				ri.setResult("-1");
				ri.setError("用户不存在");
			}
		} catch (Exception e) {
			logger.error("queryTicket接口出错，错误原因："+e.getMessage());
			e.printStackTrace();
			ri.setResult("-1");
			ri.setError(e.getMessage());
		}
		
		return xsRi.toXML(ri);
	}	
	
	/**
	 * 账务查询接口
	 * queryFinance
	 * @param xml
	 * @return
	 */
	public String queryFinance(String xml){
		System.out.println(xml);
		ReturnInfo ri = new ReturnInfo();
		XStream xsRi = new XStream();
		FinanceRequest request = new FinanceRequest();
		FinanceResponses responses = new FinanceResponses();
		
		DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");

		try {
			xsRi.alias("request", FinanceRequest.class);
			request = (FinanceRequest) xsRi.fromXML(xml);
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(User.class);
			detachedCriteria.add(Property.forName("name").eq(request.getUserName()));
			List<User> users = userService.findByCriteria(detachedCriteria);
			
			if (!users.isEmpty()) {
				User user = users.get(0);
				if (user.getPwd().equals(request.getActivePW())) {
					detachedCriteria = DetachedCriteria.forClass(Travservice.class);
					detachedCriteria.add(Property.forName("user").eq(user));
					List<Travservice> travelAgencies = baseTravelAgencyService.findByCriteria(detachedCriteria);
					if (!travelAgencies.isEmpty()) {
						// first query ticket indent data
						DetachedCriteria criteria = DetachedCriteria.forClass(TicketIndent.class);
						criteria.add(Property.forName("user").eq(user));
						Date useDate = fmt.parse(request.getUseDate());
						criteria.add(Property.forName("useDate").eq(useDate));
						List<TicketIndent> ticketIndents = ticketIndentService.findByCriteria(criteria);
					
						List<FinanceResponse> wsPojos = new ArrayList<FinanceResponse>();
						for (TicketIndent indent : ticketIndents) {
							DetachedCriteria c = DetachedCriteria.forClass(BaseTranRec.class);
							c.add(Property.forName("ticketIndent").eq(indent));
							List<BaseTranRec> trans = baseTranRecService.findByCriteria(c);
							
							for (BaseTranRec t : trans) {
								FinanceResponse pojo = new FinanceResponse();
								pojo.setIndentCode(indent.getIndentCode());
								pojo.setOutNo(indent.getOutNo());
							
								switch (t.getPaymentType()) {
								case "00":
									pojo.setPaymentTypeText("支付");
									break;
									
								case "08":
									pojo.setPaymentTypeText("退款");
								}
								
								pojo.setCashMoney(String.valueOf(t.getPayFee()));
								pojo.setCashBalance("");
								pojo.setInputTime(t.getPaymentTime().toString());
								
								wsPojos.add(pojo);
							}
						}
						
						// second deal with fund charge data
						criteria = DetachedCriteria.forClass(BaseRechargeRec.class);
						criteria.add(Property.forName("baseTravelAgency").eq(travelAgencies.get(0)));
						criteria.add(Property.forName("endDate").eq(useDate));
						List<BaseRechargeRec> recharges = baseRechargeRecService.findByCriteria(criteria);
						
						for (BaseRechargeRec recharge : recharges) {
							FinanceResponse pojo = new FinanceResponse();
							pojo.setIndentCode(recharge.getRechargeRecId().toString());
							pojo.setOutNo("");
							pojo.setPaymentTypeText("充值");
							pojo.setCashMoney(recharge.getCashMoney().toString());
							pojo.setCashBalance(recharge.getCashBalance().toString());
							pojo.setInputTime(recharge.getEndDate().toString());
							
							wsPojos.add(pojo);
						}
					
						responses.setResult("0");
						responses.setWsPojos(wsPojos);
						
						XStream xsStream = new XStream();
						xsStream.alias("response", FinanceResponses.class);
						return xsStream.toXML(responses);	
					}
					else {
						ri.setResult("-1");
						ri.setError("此接口只向旅行社开放");
					}
				}
				else {
					ri.setResult("-1");
					ri.setError("密码错误");
				}
			}
			else {
				ri.setResult("-1");
				ri.setError("用户不存在");
			}
		} catch (Exception e) {
			logger.error("queryTicket接口出错，错误原因："+e.getMessage());
			e.printStackTrace();
			ri.setResult("-1");
			ri.setError(e.getMessage());
		}
		
		return xsRi.toXML(ri);
	}	
	
	/**
	 * 订单状态查询接口
	 * queryIndent
	 * @param xml
	 * @return
	 */	
	public String queryIndent(String xml) {
		System.out.println(xml);
		ReturnInfo ri = new ReturnInfo();
		XStream xsRi = new XStream();
		QueryTicketRequest request = new QueryTicketRequest();
		TicketIndentResponse response = new TicketIndentResponse();
		try {
			xsRi.alias("request", FinanceRequest.class);
			request = (QueryTicketRequest) xsRi.fromXML(xml);
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(User.class);
			detachedCriteria.add(Property.forName("name").eq(request.getUserName()));
			List<User> users = userService.findByCriteria(detachedCriteria);
			
			if (!users.isEmpty()) {
				User user = users.get(0);
				if (user.getPwd().equals(request.getActivePW())) {
					detachedCriteria = DetachedCriteria.forClass(Travservice.class);
					detachedCriteria.add(Property.forName("user").eq(user));
					List<Travservice> travelAgencies = baseTravelAgencyService.findByCriteria(detachedCriteria);
					if (!travelAgencies.isEmpty()) {
						// first query ticket indent data
						DetachedCriteria criteria = DetachedCriteria.forClass(TicketIndent.class);
						criteria.add(Property.forName("user").eq(user));
						criteria.add(Property.forName("indentCode").eq(request.getIndentCode()));
						List<TicketIndent> ticketIndents = ticketIndentService.findByCriteria(criteria);
					
						if (ticketIndents.size() > 0) {
							TicketIndent indent = ticketIndents.get(0); 
							response.setIndentCode(indent.getIndentCode());
							Integer totalAmount = 0;
							for (TicketIndentDetail indentDetail : indent.getDetails()) {
								totalAmount += indentDetail.getAmount();
							}
							response.setTotalAmount(totalAmount.toString());
							response.setTotalPrice(indent.getTotalPrice().toString());
							response.setPerFee(indent.getPerFee().toString());
							response.setPayFee(indent.getPayFee().toString());
							switch (indent.getStatus()) {
							// 已支付待审核
							case "00":
								response.setStatus("02 已支付待审核");
								break;
							// 已审核待消费
							case "01":
								response.setStatus("03 已审核待消费");
								break;
							// 已退订	
							case "08":
								response.setStatus("06 已退订");
								break;
							}
												
							response.setResult("0");
							XStream xsStream = new XStream();
							xsStream.alias("response", TicketIndentResponse.class);
							return xsStream.toXML(response);	
						}
						else {
							ri.setResult("-1");
							ri.setError("查询订单号不存在");
						}
					}
					else {
						ri.setResult("-1");
						ri.setError("此接口只向旅行社开放");
					}
				}
				else {
					ri.setResult("-1");
					ri.setError("密码错误");
				}
			}
			else {
				ri.setResult("-1");
				ri.setError("用户不存在");
			}
		} catch (Exception e) {
			logger.error("queryTicket接口出错，错误原因："+e.getMessage());
			e.printStackTrace();
			ri.setResult("-1");
			ri.setError(e.getMessage());
		}
		
		return xsRi.toXML(ri);
		
	}
	
	/**
	 * 账户余额查询接口
	 * queryBalance
	 * @param xml
	 * @return
	 */	
	public String queryBalance(String xml) {
		System.out.println(xml);
		ReturnInfo ri = new ReturnInfo();
		XStream xsRi = new XStream();
		BalanceRequest request = new BalanceRequest();
		BalanceResponse response = new BalanceResponse();
		try {
			xsRi.alias("request", FinanceRequest.class);
			request = (BalanceRequest) xsRi.fromXML(xml);
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(User.class);
			detachedCriteria.add(Property.forName("name").eq(request.getUserName()));
			List<User> users = userService.findByCriteria(detachedCriteria);
			
			if (!users.isEmpty()) {
				User user = users.get(0);
				if (user.getPwd().equals(request.getActivePW())) {
					detachedCriteria = DetachedCriteria.forClass(Travservice.class);
					detachedCriteria.add(Property.forName("user").eq(user));
					List<Travservice> travelAgencies = baseTravelAgencyService.findByCriteria(detachedCriteria);
					if (!travelAgencies.isEmpty()) {
						switch (request.getPayType()) {
						// 奖励款
						case "01":
							detachedCriteria = DetachedCriteria.forClass(BaseRewardAccount.class);
							detachedCriteria.add(Property.forName("baseTravelAgency").eq(travelAgencies.get(0)));
							List<BaseRewardAccount> rewardAccounts = baseRewardAccountService.findByCriteria(detachedCriteria);
						
							if (rewardAccounts.size() > 0) {
								BaseRewardAccount account = rewardAccounts.get(0);
								response.setRewardBalance(account.getRewardBalance().toString());
								
								response.setResult("0");
								XStream xsStream = new XStream();
								xsStream.alias("response", TicketIndentResponse.class);
								return xsStream.toXML(response);	
							}
							else {
								ri.setResult("-1");
								ri.setError("未开通现金账户");
							}
							break;
							
						// 预付款
						case "04":	
							detachedCriteria = DetachedCriteria.forClass(BaseCashAccount.class);
							detachedCriteria.add(Property.forName("baseTravelAgency").eq(travelAgencies.get(0)));
							List<BaseCashAccount> cashAccounts = baseCashAccountService.findByCriteria(detachedCriteria);
						
							if (cashAccounts.size() > 0) {
								BaseCashAccount account = cashAccounts.get(0); 
								response.setCashBalance(account.getCashBalance().toString());
								
								response.setResult("0");
								XStream xsStream = new XStream();
								xsStream.alias("response", TicketIndentResponse.class);
								return xsStream.toXML(response);	
							}
							else {
								ri.setResult("-1");
								ri.setError("未开通现金账户");
							}
							break;
							
						default:
							ri.setResult("-1");
							ri.setError("未定义的账户类型");
						}
					}
					else {
						ri.setResult("-1");
						ri.setError("此接口只向旅行社开放");
					}
				}
				else {
					ri.setResult("-1");
					ri.setError("密码错误");
				}
			}
			else {
				ri.setResult("-1");
				ri.setError("用户不存在");
			}
		} catch (Exception e) {
			logger.error("queryTicket接口出错，错误原因："+e.getMessage());
			e.printStackTrace();
			ri.setResult("-1");
			ri.setError(e.getMessage());
		}
		
		return xsRi.toXML(ri);
		
	}
	
	
	/**
	 * 打印回传(花水湾打印回传)
	 * @param xml
	 * @return
	 */
//	public String printHswBack(String xml){
//		System.out.println(xml);
//		ReturnInfo ri = new ReturnInfo();
//		XStream xsRi = new XStream();
//		try {
//			xsRi.alias("ReturnInfo", ReturnInfo.class);
//			XStream xsStream = new XStream();
//			xsStream.alias("obj", TicketThirdBarcodeRec.class);
//			xsStream.alias("root", List.class);
//			xsStream.aliasField("ticketIndentDetail", TicketThirdBarcodeRec.class, "ticketThirdIndentDetail");
//			xsStream.aliasField("ticketBarcodeRecs", TicketThirdIndentDetail.class, "ticketThirdBarcodeRecs");
//			xsStream.aliasField("ticketIndent", TicketThirdIndentDetail.class, "ticketThirdIndent");
//			xsStream.aliasField("ticketIndentDetails", TicketThirdIndent.class, "ticketThirdIndentDetails");
//			
//			List<TicketThirdBarcodeRec> tbr = (List<TicketThirdBarcodeRec>)xsStream.fromXML(xml);
//			hswTicketBarcodeRecService.saveHswPrintBack(tbr);
//			ri.setResult("0");
//			ri.setError("");
//		} catch (Exception e) {
//			logger.error("接收打印回传数据出错，错误原因："+e.getMessage());
//			e.printStackTrace();
//			ri.setResult("-1");
//			ri.setError(e.getMessage());
//		}
//		return xsRi.toXML(ri);
//	}
	
	/**
	 * 验票回传
	 * @param xml
	 * @return
	 */
//	public String checkBack(String xml){
//		System.out.println(xml);
//		ReturnInfo ri = new ReturnInfo();
//		XStream xsRi = new XStream();
//		try {
//			xsRi.alias("ReturnInfo", ReturnInfo.class);
//			XStream xsStream = new XStream();
//			xsStream.alias("obj", TicketBarcodeRec.class);
//			xsStream.alias("root", List.class);
//			List<TicketBarcodeRec> tbr = (List<TicketBarcodeRec>)xsStream.fromXML(xml);
//			ticketBarcodeRecService.saveCheckBack(tbr);
//			ri.setResult("0");
//			ri.setError("");
//		} catch (Exception e) {
//			e.printStackTrace();
//			ri.setResult("-1");
//			ri.setError(e.getMessage());
//		}
//		return xsRi.toXML(ri);
//	}
	
	/**
	 * 交易回传
	 * @param xml
	 * @return
	 */
//	public String tranBack(String xml){
//		System.out.println(xml);
//		ReturnInfo ri = new ReturnInfo();
//		XStream xsRi = new XStream();
//		try {
//			xsRi.alias("ReturnInfo", ReturnInfo.class);
//			XStream xsStream = new XStream();
//			xsStream.alias("obj", BaseTranRec.class);
//			xsStream.alias("root", List.class);
//			List<BaseTranRec> btrs = (List<BaseTranRec>)xsStream.fromXML(xml);
//			baseTranRecService.saveTranBack(btrs);
//			ri.setResult("0");
//			ri.setError("");
//		} catch (Exception e) {
//			e.printStackTrace();
//			ri.setResult("-1");
//			ri.setError(e.getMessage());
//		}
//		return xsRi.toXML(ri);
//	}
	/**
	 * 回传补款记录
	 * @param xml
	 * @return
	 */
//	public String addPayBack(String xml){
//		System.out.println(xml);
//		ReturnInfo ri = new ReturnInfo();
//		XStream xsRi = new XStream();
//		try {
//			xsRi.alias("ReturnInfo", ReturnInfo.class);
//			XStream xsStream = new XStream();
//			xsStream.alias("obj", TicketAddPay.class);
//			xsStream.alias("root", List.class);
//			List<TicketAddPay> tap = (List<TicketAddPay>)xsStream.fromXML(xml);
//			ticketAddPayService.saveList(tap);
//			ri.setResult("0");
//			ri.setError("");
//		} catch (Exception e) {
//			e.printStackTrace();
//			ri.setResult("-1");
//			ri.setError(e.getMessage());
//		}
//		return xsRi.toXML(ri);
//	}
	
	/**
	 * 回传吉联现场记录
	 * @param xml
	 * @return
	 */
//	public String jlDataBack(String xml){
//		System.out.println(xml);
//		ReturnInfo ri = new ReturnInfo();
//		TicketPriceResponse rData = new TicketPriceResponse();
//		XStream xsRi = new XStream();
//		try {
//			xsRi.alias("request", TicketPriceRequest.class);
//			TicketPriceRequest request = (TicketPriceRequest) xsRi.fromXML(xml);
//			
//			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(User.class);
//			detachedCriteria.add(Property.forName("TicketPriceRequest").eq(request.getUserName()));
//			list<User> users = user
//			
//			xsRi.alias("ReturnInfo", ReturnInfo.class);
//			XStream xsStream = new XStream();
//			xsStream.alias("obj", JlTicket.class);
//			xsStream.alias("root", List.class);
//			List<JlTicket> jlTickets = (List<JlTicket>)xsStream.fromXML(xml);
//			jlTicketService.saves(jlTickets);
//			ri.setResult("0");
//			ri.setError("");
//		} catch (Exception e) {
//			e.printStackTrace();
//			ri.setResult("-1");
//			ri.setError(e.getMessage());
//		}
//		return xsRi.toXML(ri);
//	}
	
	/**
	 * 回传旅行团成员名单
	 * @param xml
	 * @return
	 */
//	public String memberUpdate(String xml){
//		System.out.println(xml);
//		ReturnInfo ri = new ReturnInfo();
//		XStream xsRi = new XStream();
//		try {
//			xsRi.alias("ReturnInfo", ReturnInfo.class);
//			XStream xsStream = new XStream();
//			xsStream.alias("obj", TicketMemberList.class);
//			xsStream.alias("root", List.class);
//			List<TicketMemberList> ticketMemberLists = (List<TicketMemberList>)xsStream.fromXML(xml);
//			ticketMemberListService.updates(ticketMemberLists);
//			ri.setResult("0");
//			ri.setError("");
//		} catch (Exception e) {
//			e.printStackTrace();
//			ri.setResult("-1");
//			ri.setError(e.getMessage());
//		}
//		return xsRi.toXML(ri);
//	}
	
}
