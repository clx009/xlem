package ws.client;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.log4j.Logger;

import ws.ReturnInfo;

import com.thoughtworks.xstream.XStream;
import com.xlem.dao.TicketIndent;

import common.BookingConfig;
import common.BusinessException;

enum CallMethod{
	writeTicketIndent,//写订单信息
	returnTicketIndent,//退订单
	writeTicketType,//写票种
	writeTicketPrice,//写票价
	updateUsers,//同步用户
	updateTravelAgency,//同步旅行社
	findRealPrint//按时间段查询出票数量
}

public  class CallService {
	
	private static String endpoint = BookingConfig.getInstance().getValue("bookingCache_ws_url");
//	private String endpoint = "http://133.37.135.202:8080/itsm/service/WorkFlowHttpPort?wsdl";
//	private String endpoint = "http://133.37.94.201/itsm/service/WorkFlowHttpPort?wsdl";
//	private String endpoint = "http://116.58.219.223:8080/sdk/SDKService?wsdl";
	private static Logger logger = Logger.getLogger("bookingService");
	
	public ReturnInfo writeTicketIndent(String xml){
		logger.info("writeTicketIndent_xml"+xml);
		return call(xml,CallMethod.writeTicketIndent.name());
	}
	
	private ReturnInfo call(String input,String operationName){
		try {
			Service service = new Service();
			Call call = (Call) service.createCall();
			call.setTargetEndpointAddress(endpoint);
			call.setOperationName(operationName);// WSDL里面描述的接口名称
			call.addParameter("xml",
					org.apache.axis.encoding.XMLType.XSD_STRING,
					javax.xml.rpc.ParameterMode.IN);// 接口的参数
			call.setReturnType(org.apache.axis.encoding.XMLType.SOAP_STRING);// 设置返回类型
			logger.info(input);
//			System.out.println(input);
			String result = (String) call.invoke(new Object[] { input });
			// 给方法传递参数，并且调用方法
			System.out.println("result is " + result);
			XStream xstream = new XStream();
			xstream.alias("ReturnInfo", ReturnInfo.class);
			ReturnInfo ri = (ReturnInfo) xstream.fromXML(result);
			if (ri.getResult().trim().equals("-1")) {
				throw new BusinessException(ri.getError());
			}
			return ri;
		} catch (ServiceException e) {
			e.printStackTrace();
			throw new BusinessException("接口出错", e);
		} catch (RemoteException e) {
			e.printStackTrace();
			throw new BusinessException("接口出错", e);
		}
	}
	
	public static void main(String[] args) {
				TicketIndent indent = new TicketIndent();
				indent.setIndentId("1");
				indent.setTicketIndentDetails(null);
				indent.setBaseRechargeRecs(null);
				indent.setTicketDetailHises(null);
				indent.setBaseTranRecs(null);
			XStream xsStream = new XStream();
			xsStream.alias("indent", TicketIndent.class);
			StringBuffer xml = new StringBuffer(xsStream.toXML(indent));
			System.out.println(xml);

	}

	public ReturnInfo returnTicketIndent(String xml) {
		return call(xml,CallMethod.returnTicketIndent.name());
	}
	
	
	public ReturnInfo writeTicketType(String xml) {
		return call(xml,CallMethod.writeTicketType.name());
	}
	
	public ReturnInfo writeTicketPrice(String xml){
		return call(xml,CallMethod.writeTicketPrice.name());
	}

	public ReturnInfo updateUsers(String xml) {
		return call(xml,CallMethod.updateUsers.name());
	}
	
	public ReturnInfo updateTravelAgency(String xml) {
		return call(xml,CallMethod.updateTravelAgency.name());
	}
	
	public ReturnInfo findRealPrint(String xml) {
		return call(xml,CallMethod.findRealPrint.name());
	}
	
}
