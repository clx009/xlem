package ws.client;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;

import common.BusinessException;

public class XilingCallBackClient {
	public  String call(String input,String operationName,String endpoint){
		try {
			Service service = new Service();
			Call call = (Call) service.createCall();
			call.setTargetEndpointAddress(endpoint);
			call.setOperationName(operationName);// WSDL里面描述的接口名称
			call.addParameter("xml",
					org.apache.axis.encoding.XMLType.XSD_STRING,
					javax.xml.rpc.ParameterMode.IN);// 接口的参数
			call.setReturnType(org.apache.axis.encoding.XMLType.SOAP_STRING);// 设置返回类型
			System.out.println(input);
			String result = (String) call.invoke(new Object[] { input });
			// 给方法传递参数，并且调用方法
			System.out.println("result is " + result);
			return result;
		} catch (ServiceException e) {
			e.printStackTrace();
			throw new BusinessException("接口出错", e);
		} catch (RemoteException e) {
			e.printStackTrace();
			throw new BusinessException("接口出错", e);
		}
	}
	
	public  String call1(String input,String operationName,String endpoint){
		try {
			URL url = new URL(endpoint+"&xml="+input);
			BufferedReader in = null;
			in = new BufferedReader(new InputStreamReader(url.openStream()));
			String line = in.readLine();
			if(line!=null && line.trim().equals("success")){
				return "success";
			}else{
				return "fail";
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException("接口出错", e);
		} 
	}
}
