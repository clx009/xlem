package ws.client.ticketIndent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ws.ReturnInfo;
import ws.client.CallService;

import com.thoughtworks.xstream.XStream;
import com.xlem.dao.SysUser;
import com.xlem.dao.TicketIndent;
import com.xlem.dao.TicketPrice;
import com.xlem.dao.TicketType;

import common.BusinessException;
import common.CommonMethod;

public class TicketIndentClient {
	
	/**
	 * 向缓存写入订单数据
	 * @param indents
	 * @return
	 */
	public ReturnInfo writeTicketIndents(List<TicketIndent> indents){
		for (TicketIndent indent : indents) {
			indent.setBaseRechargeRecs(null);
			indent.setTicketDetailHises(null);
			indent.setBaseTranRecs(null);
			indent.setTicketAddPaies(null);
			indent.setTicketMemberLists(null);
			indent.setTicketIndentCheckLogs(null);
			indent.setTranRecs(null);
		}
		XStream xsStream = new XStream();
		xsStream.alias("obj", TicketIndent.class);
		xsStream.alias("root", List.class);
		CallService call = new CallService();
		return call.writeTicketIndent(xsStream.toXML(indents));
	}
	
	/**
	 * 发起退订同步请求
	 * @param indents
	 * @return
	 */
	public ReturnInfo returnTickets(List<TicketIndent> indents){
		for (TicketIndent indent : indents) {
			indent.setBaseRechargeRecs(null);
			indent.setTicketDetailHises(null);
			indent.setBaseTranRecs(null);
			indent.setTicketIndentCheckLogs(null);
		}
		XStream xsStream = new XStream();
		xsStream.alias("obj", TicketIndent.class);
		xsStream.alias("root", List.class);
		CallService call = new CallService();
		return call.returnTicketIndent(xsStream.toXML(indents));
	}
	
	/**
	 * 发起票种同步
	 * @param ticketTypes
	 * @return
	 */
	public ReturnInfo writeTicketType(List<TicketType> ticketTypes){
		XStream xsStream = new XStream();
		xsStream.alias("obj", TicketType.class);
		xsStream.alias("root", List.class);
		CallService call = new CallService();
		return call.writeTicketType(xsStream.toXML(ticketTypes));
	}
	
	/**
	 * 发起票价同步
	 * @param ticketPrices
	 * @return
	 */
	public ReturnInfo writeTicketPrice(List<TicketPrice> ticketPrices){
		for (TicketPrice ticketPrice : ticketPrices) {
			ticketPrice.setTicketType(new TicketType(ticketPrice.getTicketType().getTicketId()));
			ticketPrice.setTicketIndentDetails(null);
			ticketPrice.setTicketDetailHises(null);
		}
		XStream xsStream = new XStream();
		xsStream.alias("obj", TicketPrice.class);
		xsStream.alias("root", List.class);
		
		CallService call = new CallService();
		return call.writeTicketPrice(xsStream.toXML(ticketPrices));
	}
	
	/**
	 * 用户信息同步（用于同步员工信息）
	 * @return
	 */
	public ReturnInfo updateUsers(List<SysUser> sysUsers){
		XStream xsStream = new XStream();
		xsStream.alias("obj", SysUser.class);
		xsStream.alias("root", List.class);
		CallService call = new CallService();
		return call.updateUsers(xsStream.toXML(sysUsers));
	}
	
	/**
	 * 用户信息同步（用于同步旅行社信息）
	 * @return
	 */
	public ReturnInfo updateTravelAgency(SysUser sysUser){
		XStream xsStream = new XStream();
		sysUser.setSysUserRoles(null);
		if(sysUser.getBaseTravelAgency()!=null){
//			sysUser.getBaseTravelAgency().setBaseRechargeRecs(null);
//			sysUser.getBaseTravelAgency().setBaseTourGuides(null);
//			sysUser.getBaseTravelAgency().setSysUsers(null);
//			sysUser.getBaseTravelAgency().setHotelRechargeRecs(null);
//			sysUser.getBaseTravelAgency().setBaseRewardAccounts(null);
		}
		xsStream.alias("obj", SysUser.class);
		CallService call = new CallService();
		return call.updateTravelAgency(xsStream.toXML(sysUser));
	}
	
	public ReturnInfo findRealPrint(String start,String end){
		if(start!=null && CommonMethod.string2Time1(start)==null){
			throw new BusinessException("日期格式错误","");
		}
		if(end!=null && CommonMethod.string2Time1(end)==null){
			throw new BusinessException("日期格式错误","");
		}
		Map<String,String> con= new HashMap<String,String>();
		con.put("start", start);
		con.put("end", end);
		XStream xsStream = new XStream();
		xsStream.alias("obj", Map.class);
		CallService call = new CallService();
		return call.findRealPrint(xsStream.toXML(con));
	}
}
