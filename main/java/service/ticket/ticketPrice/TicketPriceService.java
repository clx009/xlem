package service.ticket.ticketPrice;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.xlem.dao.TicketPrice;

import common.FullCalBean;
import common.action.JsonPager;
import common.service.BaseService;
public interface TicketPriceService extends BaseService<TicketPrice> {

	void save(Collection<TicketPrice> coll,String ticketId);

	JsonPager<TicketPrice> findJsonPageByCriteriaForGroup(JsonPager<TicketPrice> jp, TicketPrice ticketPrice);
	
	JsonPager<TicketPrice> findJsonPageByCriteriaForCheck(JsonPager<TicketPrice> jp, TicketPrice ticketPrice);
	
	/**
	 * 查询所有有效票种
	 * @param jp 翻页对象
	 * @param ticketPrice 
	 * @return
	 */
	JsonPager<TicketPrice> findJsonPageByCriteriaForValid(
			JsonPager<TicketPrice> jp, TicketPrice ticketPrice);
	/**
	 * 为散客购票页面获取票种及票价
	 * @param jp 翻页对象
	 * @param ticketPrice 
	 * @return
	 */
	JsonPager<TicketPrice> findJsonPageByCriteriaForBuyTicket(
			JsonPager<TicketPrice> jp, TicketPrice ticketPrice);
	/**
	 * 为散客购票页面获取票种及票价 特殊票
	 * @param jp 翻页对象
	 * @param ticketPrice 
	 * @return
	 */
	JsonPager<TicketPrice> findJsonPageByCriteriaForBuyTicket2(
			JsonPager<TicketPrice> jp, TicketPrice ticketPrice);
	
	/**
	 * 为日历页面查询票价
	 * @param start
	 * @param end
	 * @param ticketPrice
	 * @return
	 */
	List<FullCalBean> findByFull(Date start, Date end,
			TicketPrice ticketPrice);
	
	/**
	 * 保存设置的价格
	 * @param fullCalBean
	 * @param ticketPrice
	 */
	void saveFull(FullCalBean fullCalBean, TicketPrice ticketPrice);
	
	/**
	 * 保存价格审核信息
	 * @param fullCalBean
	 * @param ticketPrice
	 */
	void savePriceAudit(FullCalBean fullCalBean, TicketPrice ticketPrice);

	/**
	 * 为日历页面查询上限
	 * @param start
	 * @param end
	 * @param ticketPrice
	 * @return
	 */
	List<FullCalBean> findAmountByFull(Timestamp start, Timestamp end,
			TicketPrice ticketPrice);

	/**
	 * 保存设置的上限
	 * @param fullCalBean
	 * @param ticketPrice
	 */
	void saveAmountFull(FullCalBean fullCalBean, TicketPrice ticketPrice);

	String findParam(String t);
}