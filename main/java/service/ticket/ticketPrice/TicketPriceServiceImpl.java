package service.ticket.ticketPrice;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.SysRole;
import com.xlem.dao.SysUserRole;
import com.xlem.dao.TicketModule;
import com.xlem.dao.TicketPrice;
import com.xlem.dao.TicketType;

import ws.ReturnInfo;
import ws.client.ticketIndent.TicketIndentClient;

import common.BusinessException;
import common.CommonMethod;
import common.FullCalBean;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.sys.sysRole.SysRoleDao;
import dao.sys.sysUser.SysUserDao;
import dao.sys.sysUserRole.SysUserRoleDao;
import dao.ticket.ticketModule.TicketModuleDao;
import dao.ticket.ticketPrice.TicketPriceDao;
import dao.ticket.ticketType.TicketTypeDao;

@Service("ticketPriceService")
public class TicketPriceServiceImpl extends BaseServiceImpl<TicketPrice>
		implements TicketPriceService {
	@Autowired
	private TicketPriceDao ticketPriceDao;
	@Autowired
	private TicketTypeDao ticketTypeDao;
	@Autowired
	private TicketModuleDao ticketModuleDao;
	@Autowired
	private SysUserDao sysUserDao;
	@Autowired
	private SysUserRoleDao sysUserRoleDao;
	@Autowired
	private SysRoleDao sysRoleDao;

	private static Logger logger = Logger.getLogger("bookingService");

	@Override
	@Resource(name = "ticketPriceDao")
	protected void initBaseDAO(BaseDao<TicketPrice> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<TicketPrice> findPageByCriteria(
			PaginationSupport<TicketPrice> ps, TicketPrice t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketPrice.class);
		return ticketPriceDao.findPageByCriteria(ps,
				Order.desc("ticketPriceId"), dc);
	}

	@Override
	public void save(TicketPrice t) {
		t.setTicketPriceId(getNextKey("TicketPrice".toUpperCase(), 1));
		ticketPriceDao.save(t);
	}

	@Override
	public String findParam(String t) {
		return findSysParam(t);
	}

	@Override
	public JsonPager<TicketPrice> findJsonPageByCriteria(
			JsonPager<TicketPrice> jp, TicketPrice t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketPrice.class);
		dc.createAlias("ticketType", "ticketType");
		dc.add(Property.forName("ticketType").eq(t.getTicketType()));
		dc.addOrder(Order.desc("startDate"));
		return ticketPriceDao.findJsonPageByCriteria(jp, dc);
	}

	@Override
	public void save(Collection<TicketPrice> coll, String ticketId) {

		// 查询已设置过的时间段用于判断时间交叉
		Map map = ticketPriceDao.findTicketPriceTimeList(ticketId);
		List otpList = (List) map.get("timeBList");

		List<TicketPrice> list = new ArrayList<TicketPrice>();
		for (TicketPrice ticketPrice : coll) {

			List oldTimeBT = new ArrayList();
			String tpId = "";
			if (ticketPrice.getTicketPriceId() != null) {
				tpId = ticketPrice.getTicketPriceId().toString();
			}

			for (Object o : otpList) {
				Map otBT = (Map) o;
				String otpId = otBT.get("id").toString();
				if (!tpId.equals(otpId)) {// 排除掉本条记录
					oldTimeBT.add(o);
				}
			}

			// 判断时间是否有交叉
			Map crossTime = CommonMethod
					.CrossValidationPeriod(CommonMethod.toStartTime(Timestamp.valueOf(ticketPrice
							.getStartDate().toString())), CommonMethod
							.toEndTime(Timestamp.valueOf(ticketPrice.getEndDate().toString())), oldTimeBT);
			if (crossTime != null) {// 有交叉时间
				String exception = "你设置的时间段"
						+ CommonMethod.timeToString(Timestamp.valueOf(ticketPrice.getStartDate().toString()))
						+ "至"
						+ CommonMethod.timeToString(Timestamp.valueOf(ticketPrice.getEndDate().toString()))
						+ "与已有的时间段"
						+ crossTime.get("startDate").toString()
								.substring(0, 10) + "至"
						+ crossTime.get("endDate").toString().substring(0, 10)
						+ "交叉，请重新设置！";
				throw new BusinessException(exception, "");
			}

			double eUnitP = ticketPrice.getEntranceUnitPrice() == null ? 0
					: ticketPrice.getEntranceUnitPrice();
			double rUnitP = ticketPrice.getRopewayUnitPrice() == null ? 0
					: ticketPrice.getRopewayUnitPrice();
			double eP = ticketPrice.getEntrancePrice() == null ? 0
					: ticketPrice.getEntrancePrice();
			double rP = ticketPrice.getRopewayPrice() == null ? 0 : ticketPrice
					.getRopewayPrice();

			if (ticketPrice.getTicketPriceId() == null) {
				ticketPrice.setTicketPriceId(getNextKey("Ticket_Price", 1));

				ticketPrice.setUnitPrice(eUnitP + rUnitP);
				ticketPrice.setEntrancePrice(eP);
				ticketPrice.setEntranceUnitPrice(eUnitP);
				ticketPrice.setEntranceCompany("01");
				ticketPrice.setRopewayPrice(rP);
				ticketPrice.setRopewayUnitPrice(rUnitP);
				ticketPrice.setRopewayCompany("02");

				ticketPrice.setInputer(getCurrentUserId());
				ticketPrice.setInputTime(CommonMethod.getTimeStamp());
				ticketPriceDao.save(ticketPrice);
			} else {
				ticketPrice.setUnitPrice(eUnitP + rUnitP);
				ticketPrice.setEntrancePrice(eP);
				ticketPrice.setEntranceUnitPrice(eUnitP);
				ticketPrice.setEntranceCompany("01");
				ticketPrice.setRopewayPrice(rP);
				ticketPrice.setRopewayUnitPrice(rUnitP);
				ticketPrice.setRopewayCompany("02");

				ticketPrice.setUpdater(getCurrentUserId());
				ticketPrice.setUpdateTime(CommonMethod.getTimeStamp());
				ticketPrice.setEndDate(CommonMethod.toEndTime(Timestamp.valueOf(ticketPrice
						.getEndDate().toString())));
				ticketPriceDao.update(ticketPrice);

				TicketType ticketType = ticketTypeDao.findById(ticketPrice
						.getTicketType().getTicketId());

				DetachedCriteria dc = DetachedCriteria
						.forClass(TicketPrice.class);
				dc.createAlias("ticketType", "ticketType");
				dc.add(Property.forName("ticketType.ticketId").ne(
						ticketPrice.getTicketType().getTicketId()));
				dc.add(Property.forName("ticketType.ticketModuleId").eq(
						ticketType.getTicketModuleId()));
				dc.add(Property.forName("ticketType.customerType").eq(
						ticketType.getCustomerType()));
				// dc.add(Property.forName("startDate").le(CommonMethod.toEndTime(ticketPrice.getStartDate())));
				// dc.add(Property.forName("startDate").ge(CommonMethod.toStartTime(ticketPrice.getStartDate())));
				// dc.add(Property.forName("endDate").le(CommonMethod.toEndTime(ticketPrice.getEndDate())));
				// dc.add(Property.forName("endDate").ge(CommonMethod.toStartTime(ticketPrice.getEndDate())));
				dc.add(Property.forName("startDate").eq(
						CommonMethod.toStartTime(Timestamp.valueOf(ticketPrice.getStartDate().toString()))));
				dc.add(Property.forName("endDate").eq(
						CommonMethod.toEndTime(Timestamp.valueOf(ticketPrice.getEndDate().toString()))));
				List<TicketPrice> tpList = ticketPriceDao.findByCriteria(dc);

				for (TicketPrice tp : tpList) {
					tp.setStatus(ticketPrice.getStatus());
					tp.setUpdater(getCurrentUserId());
					tp.setUpdateTime(CommonMethod.getTimeStamp());
					ticketPrice.setEndDate(CommonMethod.toEndTime(Timestamp.valueOf(ticketPrice
							.getEndDate().toString())));
					ticketPriceDao.update(tp);
					tp.setTicketDetailHises(null);
					list.add(tp);
				}

			}
			ticketPrice.setTicketDetailHises(null);
			list.add(ticketPrice);
		}
		TicketIndentClient tic = new TicketIndentClient();
		try {
			ReturnInfo ri = tic.writeTicketPrice(list);
			if (ri.getResult().trim().equals("0")) {// 接口调用成功
			} else {
				logger.error("同步票价出错:" + ri.getError());
				throw new BusinessException("网络中断,请稍后再试!");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("同步票价出错:" + e.getMessage());
			throw new BusinessException(e);
		}
	}

	@Override
	public JsonPager<TicketPrice> findJsonPageByCriteriaForGroup(
			JsonPager<TicketPrice> jp, TicketPrice ticketPrice) {
		Timestamp useDate = null;
		if (ticketPrice != null && ticketPrice.getTicketIndent() != null
				&& ticketPrice.getTicketIndent().getUseDate() != null) {
			useDate = Timestamp.valueOf(ticketPrice.getTicketIndent().getUseDate().toString());
		} else {
			useDate = CommonMethod.getTimeStamp();
		}
		Long userId = getCurrentUserId();
		Long roleId = 0L;
		if (userId == null) {
			userId = 0L;
			DetachedCriteria dcSU = DetachedCriteria.forClass(SysRole.class);
			dcSU.add(Property.forName("roleCode").eq("003"));
			List<SysRole> listSR = sysRoleDao.findByCriteria(dcSU);
			for (SysRole sr : listSR) {
				roleId = sr.getRoleId();
			}

		} else {
			DetachedCriteria dcSU = DetachedCriteria
					.forClass(SysUserRole.class);
			dcSU.add(Property.forName("sysUser.userId").eq(userId));
			List<SysUserRole> listSU = sysUserRoleDao.findByCriteria(dcSU);
			for (SysUserRole sr : listSU) {
				roleId = sr.getSysRole().getRoleId();
			}
		}

		boolean isAgr = sysUserDao.findIsAgreementByUseId(userId);

		String usePage = "02";

		if ("outBound".equals(ticketPrice.getRemark())) {// 境外团
			usePage = "04";
		} else if ("behalf".equals(ticketPrice.getRemark())) {// 代散客
			usePage = "05";
		} else if ("webService".equals(ticketPrice.getRemark())) {// 接口
			usePage = "08";
		} else if (ticketPrice.getTicketType() != null
				&& "02".equals(ticketPrice.getTicketType().getTicketBigType())) {// 项目票购买
			usePage = "07";
		} else if (isAgr) {// 协议
			usePage = "03";
		}

		List<TicketPrice> tpLi = ticketPriceDao.findTicketList(ticketPrice,
				useDate, usePage, userId, roleId, "Group");

		// DetachedCriteria dc = DetachedCriteria.forClass(TicketPrice.class);
		// dc.createAlias("ticketType", "ticketType");
		// dc.add(Property.forName("startDate").le(CommonMethod.toEndTime(useDate)));//开始时间<=当前时间
		// dc.add(Property.forName("endDate").ge(CommonMethod.toStartTime(useDate)));//结束时间>=当前时间
		// dc.add(Property.forName("unitPrice").gt(Double.valueOf(0)));
		// dc.add(Property.forName("status").eq("11"));
		// dc.add(Property.forName("ticketType.customerType").eq(findSysParam("CUSTOMER_TYPE_GROUP")));
		// dc.add(Property.forName("ticketType.specialType").in(new
		// String[]{"01","04"}));
		// dc.add(Property.forName("ticketType.status").eq("11"));
		//
		// if(ticketPrice.getTicketType()!=null &&
		// ticketPrice.getTicketType().getTicketBigType()!=null &&
		// !"".equals(ticketPrice.getTicketType().getTicketBigType())){
		// dc.add(Property.forName("ticketType.ticketBigType").eq(ticketPrice.getTicketType().getTicketBigType()));
		// if(ticketPrice.getTicketType().getTicketBigType().equals("02")){//查项目票时判断剩余数量
		// dc.add(Property.forName("remainToplimit").gt(Integer.valueOf(0)));
		// }
		// }
		//
		// dc.addOrder(Order.asc("ticketType.ticketBigType"));
		// dc.addOrder(Order.asc("ticketType.ticketName"));
		// jp = ticketPriceDao.findJsonPageByCriteria(jp, dc);

		jp.setRoot(tpLi);

		// 境外团购票和协议旅行社查询价格为打印价格
		if ("outBound".equals(ticketPrice.getRemark()) || isAgr) {
			List<TicketPrice> tpList = jp.getRoot();
			for (TicketPrice tp : tpList) {
				tp.setUnitPrice(tp.getPrintPrice());
				tp.setEntranceUnitPrice(tp.getEntrancePrice());
				tp.setRopewayUnitPrice(tp.getRopewayPrice());
			}
		}
		return jp;
	}

	@Override
	public JsonPager<TicketPrice> findJsonPageByCriteriaForCheck(
			JsonPager<TicketPrice> jp, TicketPrice ticketPrice) {
		Timestamp useDate = null;
		if (ticketPrice != null && ticketPrice.getTicketIndent() != null
				&& ticketPrice.getTicketIndent().getUseDate() != null) {
			useDate = Timestamp.valueOf(ticketPrice.getTicketIndent().getUseDate().toString());
		} else {
			useDate = CommonMethod.getTimeStamp();
		}

		DetachedCriteria dc = DetachedCriteria.forClass(TicketPrice.class);
		dc.createAlias("ticketType", "ticketType");
		dc.add(Property.forName("startDate")
				.le(CommonMethod.toEndTime(useDate)));// 开始时间<=当前时间
		dc.add(Property.forName("endDate")
				.ge(CommonMethod.toStartTime(useDate)));// 结束时间>=当前时间
		dc.add(Property.forName("ticketType.customerType").eq(
				findSysParam("CUSTOMER_TYPE_GROUP")));
		dc.add(Property.forName("status").eq("11"));
		dc.add(Property.forName("ticketType.status").eq("11"));
		dc.addOrder(Order.asc("ticketType.ticketName"));
		return ticketPriceDao.findJsonPageByCriteria(jp, dc);
	}

	@Override
	public JsonPager<TicketPrice> findJsonPageByCriteriaForValid(
			JsonPager<TicketPrice> jp, TicketPrice ticketPrice) {
		Timestamp useDate = null;
		if (ticketPrice != null && ticketPrice.getTicketIndent() != null
				&& ticketPrice.getTicketIndent().getUseDate() != null) {
			useDate = Timestamp.valueOf(ticketPrice.getTicketIndent().getUseDate().toString());
		} else {
			useDate = CommonMethod.getTimeStamp();
		}

		DetachedCriteria dc = DetachedCriteria.forClass(TicketPrice.class);
		dc.createAlias("ticketType", "ticketType");
		dc.add(Property.forName("startDate")
				.le(CommonMethod.toEndTime(useDate)));// 开始时间<=当前时间
		dc.add(Property.forName("endDate")
				.ge(CommonMethod.toStartTime(useDate)));// 结束时间>=当前时间
		// dc.add(Property.forName("status").eq("11"));
		// dc.add(Property.forName("ticketType.status").eq("11"));
		return ticketPriceDao.findJsonPageByCriteria(jp, dc);
	}

	@Override
	public JsonPager<TicketPrice> findJsonPageByCriteriaForBuyTicket(
			JsonPager<TicketPrice> jp, TicketPrice ticketPrice) {
		Timestamp useDate = null;
		if (ticketPrice != null && ticketPrice.getTicketIndent() != null
				&& ticketPrice.getTicketIndent().getUseDate() != null) {
			useDate = Timestamp.valueOf(ticketPrice.getTicketIndent().getUseDate().toString());
		} else {
			useDate = CommonMethod.getTimeStamp();
		}

		Long userId = getCurrentUserId();
		Long roleId = 0L;
		if (userId == null) {
			userId = 0L;
			DetachedCriteria dcSU = DetachedCriteria.forClass(SysRole.class);
			dcSU.add(Property.forName("roleCode").eq("003"));
			List<SysRole> listSR = sysRoleDao.findByCriteria(dcSU);
			for (SysRole sr : listSR) {
				roleId = sr.getRoleId();
			}

		} else {
			DetachedCriteria dcSU = DetachedCriteria
					.forClass(SysUserRole.class);
			dcSU.add(Property.forName("sysUser.userId").eq(userId));
			List<SysUserRole> listSU = sysUserRoleDao.findByCriteria(dcSU);
			for (SysUserRole sr : listSU) {
				roleId = sr.getSysRole().getRoleId();
			}
		}

		String usePage = "02";

		if ("act".equals(ticketPrice.getRemark())) {// 抢购活动
			usePage = "06";
		} else if ("home".equals(ticketPrice.getRemark())
				|| "home".equals(ticketPrice.getIsUse())
				|| "gamesHome".equals(ticketPrice.getRemark())) {
			usePage = "01";
		}else if("fProject".equals(ticketPrice.getRemark())
				|| "fgames".equals(ticketPrice.getRemark())){
			usePage = "07";
		}
		try {
			List<TicketPrice> tpLi = null;
			if (ticketPrice.getTicketType() != null
					&& !ticketPrice.getTicketType().equals(null)) {
				if (ticketPrice.getTicketType().getTicketBigType() == "04"
						|| "04".equals(ticketPrice.getTicketType()
								.getTicketBigType())) {
					tpLi = ticketPriceDao.findTicketListByHsw(ticketPrice,
							useDate, usePage, userId, roleId, "BuyTicket");
				}else {
					tpLi = ticketPriceDao.findTicketList(ticketPrice, useDate,
							usePage, userId, roleId, "BuyTicket");
				}

			} else{
				tpLi = ticketPriceDao.findTicketList(ticketPrice, useDate,
						usePage, userId, roleId, "BuyTicket");
			} 
			jp.setRoot(tpLi);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// DetachedCriteria dc = DetachedCriteria.forClass(TicketPrice.class);
		// dc.createAlias("ticketType", "ticketType");
		// if(!"act".equals(ticketPrice.getRemark())){//查询活动票必须设置唯一启用时间段
		// dc.add(Property.forName("startDate").le(CommonMethod.toEndTime(useDate)));//开始时间<=当前时间
		// dc.add(Property.forName("endDate").ge(CommonMethod.toStartTime(useDate)));//结束时间>=当前时间
		// }
		// dc.add(Property.forName("unitPrice").gt(Double.valueOf(0)));
		// dc.add(Property.forName("status").eq("11"));
		// dc.add(Property.forName("ticketType.customerType").eq(findSysParam("CUSTOMER_TYPE_TOURIST")));
		// dc.add(Property.forName("ticketType.status").eq("11"));
		//
		// if("act".equals(ticketPrice.getRemark())){//查询活动票
		// dc.add(Property.forName("ticketType.specialType").eq("05"));
		// dc.add(Property.forName("ticketType.ticketBigType").eq("03"));
		// }else if(!"allTic".equals(ticketPrice.getRemark())){//不查全部票种
		// dc.add(Property.forName("ticketType.ticketBigType").ne("03"));//排除活动票
		// if(ticketPrice.getTicketType()!=null &&
		// ticketPrice.getTicketType().getSpecialType()!=null &&
		// ticketPrice.getTicketType().getSpecialType().equals("02_03")){
		// dc.add(Property.forName("ticketType.specialType").in(new
		// String[]{"02","03"}));
		// }else{
		// dc.add(Property.forName("ticketType.specialType").in(new
		// String[]{"01","04"}));
		// }
		// }
		//
		//
		// if(ticketPrice.getTicketType()!=null &&
		// ticketPrice.getTicketType().getTicketBigType()!=null &&
		// !"".equals(ticketPrice.getTicketType().getTicketBigType())){
		// dc.add(Property.forName("ticketType.ticketBigType").eq(ticketPrice.getTicketType().getTicketBigType()));
		// if(ticketPrice.getTicketType().getTicketBigType().equals("02")){//查项目票时判断剩余数量
		// dc.add(Property.forName("remainToplimit").gt(Integer.valueOf(0)));
		// }
		// }
		// dc.addOrder(Order.asc("ticketType.ticketBigType"));
		// dc.addOrder(Order.asc("ticketType.ticketName"));
		// return ticketPriceDao.findJsonPageByCriteria(jp, dc);

		return jp;
	}

	@Override
	public JsonPager<TicketPrice> findJsonPageByCriteriaForBuyTicket2(
			JsonPager<TicketPrice> jp, TicketPrice ticketPrice) {
		Timestamp useDate = null;
		if (ticketPrice != null && ticketPrice.getTicketIndent() != null
				&& ticketPrice.getTicketIndent().getUseDate() != null) {
			useDate = Timestamp.valueOf(ticketPrice.getTicketIndent().getUseDate().toString());
		} else {
			useDate = CommonMethod.getTimeStamp();
		}

		DetachedCriteria dc = DetachedCriteria.forClass(TicketPrice.class);
		dc.createAlias("ticketType", "ticketType");
		dc.add(Property.forName("startDate")
				.le(CommonMethod.toEndTime(useDate)));// 开始时间<=当前时间
		dc.add(Property.forName("endDate")
				.ge(CommonMethod.toStartTime(useDate)));// 结束时间>=当前时间
		dc.add(Property.forName("unitPrice").gt(Double.valueOf(0)));
		dc.add(Property.forName("status").eq("11"));
		dc.add(Property.forName("ticketType.customerType").eq(
				findSysParam("CUSTOMER_TYPE_TOURIST")));
		dc.add(Property.forName("ticketType.status").eq("11"));
		dc.add(Property.forName("ticketType.specialType").ne("01"));
		dc.add(Property.forName("ticketType.ticketBigType").ne("03"));// 排除活动票
		dc.addOrder(Order.desc("unitPrice"));
		return ticketPriceDao.findJsonPageByCriteria(jp, dc);
	}

	@Override
	public List<FullCalBean> findByFull(Date start, Date end, TicketPrice t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketPrice.class);
		dc.add(Property.forName("ticketType.ticketId").eq(
				t.getTicketType().getTicketId()));
		dc.add(Property.forName("startDate").ge(start));
		dc.add(Property.forName("startDate").le(end));
		dc.addOrder(Order.desc("startDate"));
		List<TicketPrice> ticketPrices = ticketPriceDao.findByCriteria(dc);

		Map<Timestamp, TicketPrice> tpMap = new HashMap<Timestamp, TicketPrice>();
		if (!ticketPrices.isEmpty()) {
			for (TicketPrice tp : ticketPrices) {
				tpMap.put(Timestamp.valueOf(tp.getStartDate().toString()), tp);
			}
		}

		List<Timestamp> timeList = CommonMethod.getTimeStampList(
				CommonMethod.string2Time1(CommonMethod.dateToString(start)),
				CommonMethod.string2Time1(CommonMethod.dateToString(end)));

		List<FullCalBean> calList = new ArrayList<FullCalBean>();
		for (Timestamp ti : timeList) {
			TicketPrice ticketPrice = tpMap.get(ti);
			FullCalBean cal = new FullCalBean();
			if (ticketPrice != null) {
				cal.setId(ticketPrice.getTicketPriceId().toString());
				cal.setTitle(ticketPrice.getUnitPrice() == null ? "-"
						: ticketPrice.getUnitPrice().toString());
				cal.setTitle1(ticketPrice.getPrintPrice() == null ? "-"
						: ticketPrice.getPrintPrice().toString());

				cal.setTitle2(ticketPrice.getStatusText());
				cal.setStart(CommonMethod.toStartTime(Timestamp.valueOf(ticketPrice
						.getStartDate().toString())));
			} else {
				cal.setId("");
				cal.setTitle("-");
				cal.setTitle1("-");
				cal.setStart(CommonMethod.toStartTime(ti));
			}
			calList.add(cal);
		}

		return calList;

	}

	@Override
	public void saveFull(FullCalBean fullCalBean, TicketPrice ticketPrice) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketPrice.class);
		dc.add(Property.forName("startDate").ge(
				CommonMethod.toStartTime(fullCalBean.getStart())));
		dc.add(Property.forName("startDate").le(
				CommonMethod.toEndTime(fullCalBean.getEnd())));
		dc.add(Property.forName("ticketType.ticketId").eq(
				ticketPrice.getTicketType().getTicketId()));
		dc.addOrder(Order.asc("startDate"));
		List<TicketPrice> tpList = ticketPriceDao.findByCriteria(dc);

		TicketType ticketType = ticketTypeDao.findById(ticketPrice
				.getTicketType().getTicketId());

		TicketModule ticketModule = ticketModuleDao.findById(Long
				.valueOf(ticketType.getTicketModuleId()));

		String company = ticketModule.getCompany();

		Map<Timestamp, TicketPrice> tpMap = new HashMap<Timestamp, TicketPrice>();
		if (!tpList.isEmpty()) {
			for (TicketPrice tp : tpList) {
				tpMap.put(Timestamp.valueOf(tp.getStartDate().toString()), tp);
			}
		}

		List<Timestamp> tList = CommonMethod.getTimeStampList(
				fullCalBean.getStart(), fullCalBean.getEnd());

		Timestamp nowtime = CommonMethod.getTimeStamp();
		// 同步票价start
		List<TicketPrice> list = new ArrayList<TicketPrice>();
		// 同步票价end
		for (Timestamp t : tList) {
			TicketPrice tempTp = tpMap.get(t);
			if (tempTp != null) {
				tempTp.setUnitPrice(Double.parseDouble(fullCalBean.getTitle()));
				tempTp.setPrintPrice(Double.parseDouble(fullCalBean.getTitle1()));

				if ("01".equals(company)) {
					tempTp.setEntranceCompany(company);
					tempTp.setEntrancePrice(tempTp.getPrintPrice());
					tempTp.setEntranceUnitPrice(tempTp.getUnitPrice());

					tempTp.setRopewayCompany(null);
					tempTp.setRopewayPrice(0d);
					tempTp.setRopewayUnitPrice(0d);
				} else if ("02".equals(company)) {
					tempTp.setRopewayCompany(company);
					tempTp.setRopewayPrice(tempTp.getPrintPrice());
					tempTp.setRopewayUnitPrice(tempTp.getUnitPrice());

					tempTp.setEntranceCompany(null);
					tempTp.setEntrancePrice(0d);
					tempTp.setEntranceUnitPrice(0d);
				}

				tempTp.setStatus("00");// 改为未审核
				tempTp.setUpdater(getCurrentUserId());
				tempTp.setUpdateTime(nowtime);
				ticketPriceDao.update(tempTp);
				// 同步票价start
				tempTp.setTicketDetailHises(null);
				list.add(tempTp);
				// 同步票价end
			} else {
				TicketPrice tempTicketPrice = new TicketPrice();
				tempTicketPrice.setTicketPriceId(getNextKey("Ticket_Price", 1));
				tempTicketPrice.setTicketType(ticketPrice.getTicketType());
				tempTicketPrice.setUnitPrice(Double.parseDouble(fullCalBean
						.getTitle()));
				tempTicketPrice.setPrintPrice(Double.parseDouble(fullCalBean
						.getTitle1()));

				if ("01".equals(company)) {
					tempTicketPrice.setEntranceCompany(company);
					tempTicketPrice.setEntrancePrice(tempTicketPrice
							.getPrintPrice());
					tempTicketPrice.setEntranceUnitPrice(tempTicketPrice
							.getUnitPrice());

					tempTicketPrice.setRopewayCompany(null);
					tempTicketPrice.setRopewayPrice(0d);
					tempTicketPrice.setRopewayUnitPrice(0d);
				} else if ("02".equals(company)) {
					tempTicketPrice.setRopewayCompany(company);
					tempTicketPrice.setRopewayPrice(tempTicketPrice
							.getPrintPrice());
					tempTicketPrice.setRopewayUnitPrice(tempTicketPrice
							.getUnitPrice());

					tempTicketPrice.setEntranceCompany(null);
					tempTicketPrice.setEntrancePrice(0d);
					tempTicketPrice.setEntranceUnitPrice(0d);
				}

				tempTicketPrice.setStartDate(t);
				Timestamp ts = new Timestamp(t.getTime());
				tempTicketPrice.setEndDate(CommonMethod.toEndTime(ts));
				tempTicketPrice.setStatus("00");// 改为未审核
				tempTicketPrice.setInputer(getCurrentUserId());
				tempTicketPrice.setInputTime(nowtime);
				ticketPriceDao.save(tempTicketPrice);
				// 同步票价start
				tempTicketPrice.setTicketDetailHises(null);
				list.add(tempTicketPrice);
				// 同步票价end
			}
		}

		// 同步票价start
		TicketIndentClient tic = new TicketIndentClient();
		try {
			ReturnInfo ri = tic.writeTicketPrice(list);
			if (ri.getResult().trim().equals("0")) {// 接口调用成功
			} else {
				logger.error("同步票价出错:" + ri.getError());
				throw new BusinessException("网络中断,请稍后再试!");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("同步票价出错:" + e.getMessage());
			throw new BusinessException(e);
		}
		// 同步票价end
	}

	@Override
	public void savePriceAudit(FullCalBean fullCalBean, TicketPrice ticketPrice) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketPrice.class);
		dc.add(Property.forName("startDate").ge(
				CommonMethod.toStartTime(fullCalBean.getStart())));
		dc.add(Property.forName("startDate").le(
				CommonMethod.toEndTime(fullCalBean.getEnd())));
		dc.add(Property.forName("ticketType.ticketId").eq(
				ticketPrice.getTicketType().getTicketId()));
		dc.addOrder(Order.asc("startDate"));
		List<TicketPrice> tpList = ticketPriceDao.findByCriteria(dc);

		Map<Timestamp, TicketPrice> tpMap = new HashMap<Timestamp, TicketPrice>();
		if (!tpList.isEmpty()) {
			for (TicketPrice tp : tpList) {
				tpMap.put(Timestamp.valueOf(tp.getStartDate().toString()), tp);
			}
		}

		List<Timestamp> tList = CommonMethod.getTimeStampList(
				fullCalBean.getStart(), fullCalBean.getEnd());

		Timestamp nowtime = CommonMethod.getTimeStamp();
		// 同步票价start
		List<TicketPrice> list = new ArrayList<TicketPrice>();
		// 同步票价end
		for (Timestamp t : tList) {
			TicketPrice tempTp = tpMap.get(t);
			if (tempTp != null) {
				tempTp.setStatus(ticketPrice.getStatus());// 改为未审核
				tempTp.setUpdater(getCurrentUserId());
				tempTp.setUpdateTime(nowtime);
				ticketPriceDao.update(tempTp);
				// 同步票价start
				tempTp.setTicketDetailHises(null);
				list.add(tempTp);
				// 同步票价end
			}
		}

		// 同步票价start
		TicketIndentClient tic = new TicketIndentClient();
		try {
			ReturnInfo ri = tic.writeTicketPrice(list);
			if (ri.getResult().trim().equals("0")) {// 接口调用成功
			} else {
				logger.error("同步票价出错:" + ri.getError());
				throw new BusinessException("网络中断,请稍后再试!");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("同步票价出错:" + e.getMessage());
			throw new BusinessException(e);
		}
		// 同步票价end
	}

	@Override
	public List<FullCalBean> findAmountByFull(Timestamp start, Timestamp end,
			TicketPrice t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketPrice.class);
		dc.add(Property.forName("ticketType.ticketId").eq(
				t.getTicketType().getTicketId()));
		dc.add(Property.forName("startDate").ge(start));
		dc.add(Property.forName("startDate").le(end));
		dc.addOrder(Order.desc("startDate"));
		List<TicketPrice> ticketPrices = ticketPriceDao.findByCriteria(dc);

		Map<Timestamp, TicketPrice> tpMap = new HashMap<Timestamp, TicketPrice>();
		if (!ticketPrices.isEmpty()) {
			for (TicketPrice tp : ticketPrices) {
				tpMap.put(Timestamp.valueOf(tp.getStartDate().toString()), tp);
			}
		}

		List<Timestamp> timeList = CommonMethod.getTimeStampList(
				CommonMethod.string2Time1(CommonMethod.dateToString(start)),
				CommonMethod.string2Time1(CommonMethod.dateToString(end)));

		List<FullCalBean> calList = new ArrayList<FullCalBean>();
		for (Timestamp ti : timeList) {
			TicketPrice ticketPrice = tpMap.get(ti);
			FullCalBean cal = new FullCalBean();
			if (ticketPrice != null) {
				cal.setId(ticketPrice.getTicketPriceId().toString());
				cal.setTitle(ticketPrice.getToplimit() == null ? ""
						: ticketPrice.getToplimit().toString());
				cal.setStart(CommonMethod.toStartTime(Timestamp.valueOf(ticketPrice
						.getStartDate().toString())));
			} else {
				cal.setId("");
				cal.setTitle("");
				cal.setStart(CommonMethod.toStartTime(ti));
			}
			calList.add(cal);
		}
		return calList;
	}

	@Override
	public void saveAmountFull(FullCalBean fullCalBean, TicketPrice ticketPrice) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketPrice.class);
		dc.add(Property.forName("startDate").ge(
				CommonMethod.toStartTime(fullCalBean.getStart())));
		dc.add(Property.forName("startDate").le(
				CommonMethod.toEndTime(fullCalBean.getEnd())));
		dc.add(Property.forName("ticketType.ticketId").eq(
				ticketPrice.getTicketType().getTicketId()));
		dc.addOrder(Order.asc("startDate"));
		List<TicketPrice> tpList = ticketPriceDao.findByCriteria(dc);

		Map<Timestamp, TicketPrice> tpMap = new HashMap<Timestamp, TicketPrice>();
		if (!tpList.isEmpty()) {
			for (TicketPrice tp : tpList) {
				tpMap.put(Timestamp.valueOf(tp.getStartDate().toString()), tp);
			}
		}

		List<Timestamp> tList = CommonMethod.getTimeStampList(
				fullCalBean.getStart(), fullCalBean.getEnd());

		Timestamp nowtime = CommonMethod.getTimeStamp();

		// 同步票价start
		List<TicketPrice> list = new ArrayList<TicketPrice>();
		// 同步票价end

		for (Timestamp t : tList) {

			TicketPrice tempTp = tpMap.get(t);
			if (tempTp != null) {
				int remainToplimit = Integer.parseInt(fullCalBean.getTitle())
						- ticketPriceDao.findTopLimit(t, tempTp);
				if (remainToplimit < 0) {
					remainToplimit = 0;
				}

				tempTp.setToplimit(Integer.valueOf(fullCalBean.getTitle()));
				tempTp.setRemainToplimit(remainToplimit);
				tempTp.setUpdater(getCurrentUserId());
				tempTp.setUpdateTime(nowtime);
				ticketPriceDao.update(tempTp);
				// 同步票价start
				tempTp.setTicketDetailHises(null);
				list.add(tempTp);
				// 同步票价end
			} else {
				TicketPrice tempTicketPrice = new TicketPrice();
				tempTicketPrice.setTicketPriceId(getNextKey("Ticket_Price", 1));
				tempTicketPrice.setTicketType(ticketPrice.getTicketType());
				tempTicketPrice.setToplimit(Integer.valueOf(fullCalBean
						.getTitle()));
				tempTicketPrice.setRemainToplimit(Integer.valueOf(fullCalBean
						.getTitle()));
				tempTicketPrice.setStartDate(t);
				Timestamp ts = new Timestamp(t.getTime());
				tempTicketPrice.setEndDate(CommonMethod.toEndTime(ts));
				tempTicketPrice.setStatus("00");// 设置为未审核
				tempTicketPrice.setInputer(getCurrentUserId());
				tempTicketPrice.setInputTime(nowtime);
				ticketPriceDao.save(tempTicketPrice);
				// 同步票价start
				tempTicketPrice.setTicketDetailHises(null);
				list.add(tempTicketPrice);
				// 同步票价end
			}
		}

		// 同步票价start
		TicketIndentClient tic = new TicketIndentClient();
		try {
			ReturnInfo ri = tic.writeTicketPrice(list);
			if (ri.getResult().trim().equals("0")) {// 接口调用成功
			} else {
				logger.error("同步票价出错:" + ri.getError());
				throw new BusinessException("网络中断,请稍后再试!");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("同步票价出错:" + e.getMessage());
			throw new BusinessException(e);
		}
		// 同步票价end
	}
}