package service.ticket.ticketCheckRec;
import java.util.Map;

import com.xlem.dao.TicketAccounts;
import com.xlem.dao.TicketCheckRec;

import common.action.JsonPager;
import common.service.BaseService;

public interface TicketCheckRecService extends BaseService<TicketCheckRec> {

	public Map<String,Double> updateAccounts();
	
	public JsonPager<TicketCheckRec> findGrid(JsonPager<TicketCheckRec> jp,TicketCheckRec t);
	
	/**
	 * 查询结帐明细
	 * @param checkRecId
	 * @return
	 */
	public JsonPager<TicketCheckRec> findGridDetail(Long checkRecId);
	
	/**
	 * 查询结帐明细 (分账)
	 * @param checkRecId
	 * @return
	 */
	public JsonPager<TicketAccounts> findGridDetailSub(Long checkRecId);
	
}