package service.ticket.ticketCheckRec;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.TicketAccounts;
import com.xlem.dao.TicketCheckRec;

import common.CodeListener;
import common.CommonMethod;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.ticket.ticketBarcodeRec.TicketBarcodeRecDao;
import dao.ticket.ticketCheckRec.TicketCheckRecDao;

@Service("ticketCheckRecService")
public class TicketCheckRecServiceImpl extends
		BaseServiceImpl<TicketCheckRec> implements TicketCheckRecService {
	@Autowired
	private TicketCheckRecDao ticketCheckRecDao;
	@Autowired
	private TicketBarcodeRecDao ticketBarcodeRecDao;

	@Override
	@Resource(name = "ticketCheckRecDao")
	protected void initBaseDAO(BaseDao<TicketCheckRec> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<TicketCheckRec> findPageByCriteria(
			PaginationSupport<TicketCheckRec> ps, TicketCheckRec t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketCheckRec.class);
		return ticketCheckRecDao.findPageByCriteria(ps,
				Order.desc("ticketCheckRecId"), dc);
	}

	@Override
	public void save(TicketCheckRec t) {
		t.setCheckRecId(getNextKey("Ticket_Check_Rec".toUpperCase(), 1));
		ticketCheckRecDao.save(t);
	}

	@Override
	public JsonPager<TicketCheckRec> findJsonPageByCriteria(
			JsonPager<TicketCheckRec> jp, TicketCheckRec t) {
		return null;
	}

	@Override
	public Map<String,Double> updateAccounts() {
		
		long checkRecId = getNextKey("Ticket_Check_Rec".toUpperCase(), 1);
		
		//产生结账记录  修改消费记录,将结账记录Id保存进消费记录，同时修改消费记录状态为已结账
		ticketBarcodeRecDao.update(checkRecId,getCurrentUserId());
		
		//查询结账金额并修改结账记录金额
		return ticketBarcodeRecDao.updateTotal(checkRecId);
			
		
	}
	
	@Override 
	public JsonPager<TicketCheckRec> findGrid(JsonPager<TicketCheckRec> jp,TicketCheckRec t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketCheckRec.class);
		if (t.getStartTime() != null) {
			dc.add(Property.forName("checkTime").ge(t.getStartTime()));
		}

		if (t.getEndTime() != null) {
			dc.add(Property.forName("checkTime").le(CommonMethod.toEndTime(Timestamp.valueOf(t.getEndTime().toString()))));
		}
		dc.addOrder(Order.desc("checkTime"));
		return ticketCheckRecDao.findJsonPageByCriteria(jp, dc);
	}

	@Override
	public JsonPager<TicketCheckRec> findGridDetail(Long checkRecId) {
		return ticketCheckRecDao.findGridDetail(checkRecId);
	}
	
	@Override
	public JsonPager<TicketAccounts> findGridDetailSub(Long checkRecId) {
		JsonPager<TicketAccounts> jp = new JsonPager<TicketAccounts>();
		List<TicketAccounts> taList = ticketCheckRecDao.findGridDetailSub(checkRecId);
		
		List<TicketAccounts> taListSub = new ArrayList<TicketAccounts>();
		
		String eCompany= CodeListener.getNameByCode("TICKET_MODULE", "company", "01");
		String rCompany= CodeListener.getNameByCode("TICKET_MODULE", "company", "02");
		
		for(TicketAccounts ta : taList){
			boolean setAmount = true;
			if((ta.getSubtotalGroupE()!=null && ta.getSubtotalGroupE()>0d ) || 
					(ta.getSubtotalGroupDE()!=null && ta.getSubtotalGroupDE()>0d ) ||
					(ta.getSubtotalE()!=null && ta.getSubtotalE()>0d ) ||
					(ta.getSubtotalLocaleE()!=null && ta.getSubtotalLocaleE()>0d )){
				TicketAccounts taE = new TicketAccounts(ta,"E",eCompany,true);
				taListSub.add(taE);
				setAmount = false;
			}
			
			if((ta.getSubtotalGroupR()!=null && ta.getSubtotalGroupR()>0d ) || 
					(ta.getSubtotalGroupDR()!=null && ta.getSubtotalGroupDR()>0d ) ||
					(ta.getSubtotalR()!=null && ta.getSubtotalR()>0d ) ||
					(ta.getSubtotalLocaleR()!=null && ta.getSubtotalLocaleR()>0d )){
				TicketAccounts taR = new TicketAccounts(ta,"R",rCompany,setAmount);
				taListSub.add(taR);
			}
			
			
		}
		
		jp.setRoot(taListSub);
		jp.setTotal(taListSub.size());
		return jp;
	}
	
}