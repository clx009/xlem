package service.ticket.ticketMemberList;
import java.util.List;

import com.xlem.dao.TicketMemberList;

import common.service.BaseService;

public interface TicketMemberListService extends BaseService<TicketMemberList> {

	public List<TicketMemberList> findByIndentId(String indentId);
	
	public void updates(List<TicketMemberList> tbr);
}