package service.ticket.ticketMemberList;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.TicketMemberList;

import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.ticket.ticketMemberList.TicketMemberListDao;

@Service("ticketMemberListService")
public class TicketMemberListServiceImpl extends BaseServiceImpl<TicketMemberList>
		implements TicketMemberListService {
	@Autowired
	private TicketMemberListDao ticketMemberListDao;

	@Override
	@Resource(name = "ticketMemberListDao")
	protected void initBaseDAO(BaseDao<TicketMemberList> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<TicketMemberList> findPageByCriteria(
			PaginationSupport<TicketMemberList> ps, TicketMemberList t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketMemberList.class);
		return ticketMemberListDao.findPageByCriteria(ps,
				Order.desc("ticketMemberListId"), dc);
	}

	@Override
	public void save(TicketMemberList t) {
		ticketMemberListDao.save(t);
	}
	
	@Override
	public void updates(List<TicketMemberList> tbr) {
		for (TicketMemberList ticketMemberList : tbr) {
			
			DetachedCriteria dcDetail = DetachedCriteria.forClass(TicketMemberList.class);
			dcDetail.add(Property.forName("ticketIndent.indentId").eq(ticketMemberList.getTicketIndent().getIndentId()));
			dcDetail.add(Property.forName("idCardNumber").eq(ticketMemberList.getIdCardNumber()));
			dcDetail.add(Property.forName("memberName").eq(ticketMemberList.getMemberName()));
			
			List<TicketMemberList> tmList = ticketMemberListDao.findByCriteria(dcDetail);
			if(tmList!=null && tmList.size()>0){
				TicketMemberList tm = tmList.get(0);
				tm.setStatus(ticketMemberList.getStatus());
				tm.setSynchroState(ticketMemberList.getSynchroState());
				tm.setRemark(ticketMemberList.getRemark());
				ticketMemberListDao.update(tm);
			}
			
		}
	}
	
	@Override
	public List<TicketMemberList> findByIndentId(String indentId) {
		if(indentId==null || indentId.trim().isEmpty()){
			return new ArrayList<TicketMemberList>();
		}
		DetachedCriteria dcDetail = DetachedCriteria.forClass(TicketMemberList.class);
		dcDetail.add(Property.forName("ticketIndent.indentId").eq(indentId));
		List<TicketMemberList> tm = ticketMemberListDao.findByCriteria(dcDetail);
		Map<String, Integer> teMap = ticketMemberListDao.findFrequency(indentId);
		for(TicketMemberList tml : tm){
			int frequency = teMap.get("IC_"+tml.getIdCardNumber()+"_"+tml.getMemberName())==null?0:Integer.valueOf(teMap.get("IC_"+tml.getIdCardNumber()+"_"+tml.getMemberName()).toString());
			tml.setFrequency(frequency);
		}
		return tm;
	}

	@Override
	public JsonPager<TicketMemberList> findJsonPageByCriteria(
			JsonPager<TicketMemberList> jp, TicketMemberList t) {
		// TODO Auto-generated method stub
		return null;
	}
}