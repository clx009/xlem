package service.ticket.ticketType;

import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.TicketType;

import ws.ReturnInfo;
import ws.client.ticketIndent.TicketIndentClient;

import common.BusinessException;
import common.CommonMethod;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.ticket.ticketType.TicketTypeDao; 

@Service("ticketTypeService")
public class TicketTypeServiceImpl extends BaseServiceImpl<TicketType>
		implements TicketTypeService {
	@Autowired
	private TicketTypeDao ticketTypeDao;
	
	private static Logger logger = Logger.getLogger("bookingService");
	
	@Override
	@Resource(name = "ticketTypeDao")
	protected void initBaseDAO(BaseDao<TicketType> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<TicketType> findPageByCriteria(
			PaginationSupport<TicketType> ps, TicketType t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketType.class);
		return ticketTypeDao.findPageByCriteria(ps, Order.desc("ticketId"), dc);
	}

	@Override
	public void save(TicketType t) {
		t.setTicketId(getNextKey("Ticket_Type".toUpperCase(), 1));
		ticketTypeDao.save(t);
	}

	@Override
	public JsonPager<TicketType> findJsonPageByCriteria(
			JsonPager<TicketType> jp, TicketType t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketType.class);
		if(t.getCustomerType()!=null && !t.getCustomerType().equals("")){
			dc.add(Property.forName("customerType").eq(t.getCustomerType()));
		}
		
		if(t.getIntro()!=null && t.getIntro().equals("01_04")){
			dc.add(Property.forName("specialType").in(new String[]{"05","01","04","06"}));
		}else if(t.getIntro()!=null && t.getIntro().equals("01_04_07")){
			dc.add(Property.forName("specialType").in(new String[]{"05","01","04","06","07"}));
		}else if(t.getSpecialType()!=null && t.getSpecialType().equals("01_04")){
			dc.add(Property.forName("specialType").in(new String[]{"01","04"}));
		}else if(t.getIntro()!=null && t.getIntro().equals("redLetter")){
			dc.add(Property.forName("specialType").in(new String[]{"05","04","06"}));
		}else if(t.getSpecialType()!=null && t.getSpecialType().equals("02_03")){
			dc.add(Property.forName("specialType").in(new String[]{"02","03"}));
		}else if(t.getSpecialType()!=null && !t.getSpecialType().equals("")){
			dc.add(Property.forName("specialType").eq(t.getSpecialType()));
		}
		
		if(t.getTicketBigType()!=null && !t.getTicketBigType().trim().equals("")){
			if(t.getIntro()!=null && t.getIntro().equals("01_04") && !"02".equals(t.getTicketBigType().trim())){//票审核页面
				dc.add(Property.forName("ticketBigType").in(new String[]{"01","03"}));
			}else{
				dc.add(Property.forName("ticketBigType").eq(t.getTicketBigType().trim()));
			}
		}
		if("notNull".equals(t.getImgAddress())){//首页购票
			dc.add(Property.forName("imgAddress").isNotNull());
			dc.add(Property.forName("imgAddress").ne("''"));
		}
		//区分首页购买观景索道票和项目票  20150527  tt
		if(t.getTicketCode()!=null && !t.getTicketCode().trim().equals("")){
			dc.add(Property.forName("ticketCode").eq(t.getTicketCode().trim()));
		}
		dc.add(Property.forName("status").eq("11"));
		dc.addOrder(Order.asc("customerType"));
		dc.addOrder(Order.asc("specialType"));
		dc.addOrder(Order.asc("ticketName"));
		return ticketTypeDao.findJsonPageByCriteria(jp, dc);
	}

	@Override
	public void save(Collection<TicketType> coll) {
		for (TicketType ticketType : coll) {
			if (ticketType.getTicketId() == null) {
				ticketType.setTicketId(getNextKey("Ticket_Type", 1));
				ticketType.setInputer(getCurrentUserId());
				ticketType.setTicketBigType("01");
				ticketType.setInputTime(CommonMethod.getTimeStamp());
				ticketTypeDao.save(ticketType);
			} else {
				ticketType.setUpdater(getCurrentUserId());
				ticketType.setUpdateTime(CommonMethod.getTimeStamp());
				if("05".equals(ticketType.getSpecialType()) || "06".equals(ticketType.getSpecialType())){//活动票、特惠票大类改为03
					ticketType.setTicketBigType("03");
				}
				ticketTypeDao.update(ticketType);
			}
		}
		List<TicketType> list = (List<TicketType>) coll;
		TicketIndentClient tic = new TicketIndentClient();
		try{
			ReturnInfo ri = tic.writeTicketType(list);
			if(ri.getResult().trim().equals("0")){//接口调用成功
			}
			else{
				logger.error("同步票种出错:"+ri.getError());
				throw new BusinessException("网络中断,请稍后再试!");
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			logger.error("同步票种出错:"+e.getMessage());
			throw new BusinessException(e.getMessage());
		}
	}
	
	@Override
	public void saveEditGame(TicketType t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketType.class);
		dc.add(Property.forName("ticketModuleId").eq(t.getTicketModuleId()));
		
		List<TicketType> listT = ticketTypeDao.findByCriteria(dc);
		
		for(TicketType tt : listT ){
			
			tt.setImgAddress(t.getImgAddress());
			tt.setImgFolder(t.getImgFolder());
			tt.setIntro(t.getIntro());
			tt.setDescride(t.getDescride());
			tt.setRemark(t.getRemark());
			tt.setUpdater(getCurrentUserId());
			tt.setUpdateTime(CommonMethod.getTimeStamp());
			ticketTypeDao.update(tt);
		}
		
	}
	
}