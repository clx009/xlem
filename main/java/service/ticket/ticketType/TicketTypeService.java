package service.ticket.ticketType;
import java.util.Collection;

import com.xlem.dao.TicketType;

import common.action.JsonPager;
import common.service.BaseService;
public interface TicketTypeService extends BaseService<TicketType> {

	void save(Collection<TicketType> coll);

	public void saveEditGame(TicketType t);
}