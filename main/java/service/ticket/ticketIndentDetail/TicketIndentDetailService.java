package service.ticket.ticketIndentDetail;
import java.util.Collection;
import java.util.List;

import com.xlem.dao.TicketIndent;
import com.xlem.dao.TicketIndentDetail;
import com.xlem.dao.TicketPrice;

import common.action.JsonPager;
import common.service.BaseService;

public interface TicketIndentDetailService extends BaseService<TicketIndentDetail> {

	void saveForeign(Collection<TicketIndentDetail> coll,TicketIndentDetail ticketIndentDetail);
	
	void save(Collection<TicketIndentDetail> coll, TicketIndentDetail ticketIndentDetail);
	
	void saveHomeIndemt(Collection<TicketIndentDetail> coll, TicketIndentDetail ticketIndentDetail);
	
	void saveCheck(Collection<TicketIndentDetail> coll, TicketIndentDetail ticketIndentDetail);
	void saveGames(Collection<TicketIndentDetail> coll, TicketIndentDetail ticketIndentDetail);
	
	void saveHomeGames(Collection<TicketIndentDetail> coll, TicketIndentDetail ticketIndentDetail);
	boolean saveReturn(TicketIndentDetail ticketIndentDetail);
	void saveReturnByMobile(TicketIndentDetail ticketIndentDetail);

	JsonPager<TicketIndentDetail> findJsonPageByCriteriaForConfirm(
			JsonPager<TicketIndentDetail> jp,
			TicketIndentDetail ticketIndentDetail);
	
	/**
	 * 获取订单编号
	 * @param string
	 * @param currentYMD
	 * @return
	 */
	String getNextIndentCode(String string, String currentYMD);
	
	/**
	 * 验证购票权限及购票上限,同时修改剩余数量
	 * @param ticketIndent 
	 * @param t
	 * @param customerType 客户类型：01团，02散
	 * @param isOutbound 是否境外团：11是
	 * @param isAgr 是否协议
	 * @param userId 
	 * @param roleId 
	 * @param updateRemain 是否修改剩余数量
	 * @return
	 */
	public TicketPrice checkTopLimit(TicketIndent ticketIndent,TicketIndentDetail t, String customerType,String isOutbound, boolean isAgr,long userId,long roleId,boolean updateRemain);
	
	/**
	 * 退现信息确认页面查询明细
	 * @param t
	 * @return
	 */
	public List<TicketIndentDetail> findIndentDetailList(TicketIndent t);
	
	public String findCheckAmount(String useDateT);

	void saveAcProject(Collection<TicketIndentDetail> coll, TicketIndentDetail t);
}