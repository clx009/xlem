package service.ticket.ticketIndentDetail;
import com.xlem.dao.TicketDetailHis;

import common.action.JsonPager;
import common.service.BaseService;

public interface TicketDetailHisService extends BaseService<TicketDetailHis> {

	JsonPager<TicketDetailHis> findJsonPageByCriteriaForConfirm(
			JsonPager<TicketDetailHis> jp,
			TicketDetailHis ticketDetailHis);
	
}