package service.ticket.ticketIndentDetail;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.TicketDetailHis;

import common.action.JsonPager;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.ticket.ticketIndentDetail.TicketDetailHisDao;

@Service("ticketDetailHisService")
public class TicketDetailHisServiceImpl extends
		BaseServiceImpl<TicketDetailHis> implements
		TicketDetailHisService {
	
	@Autowired
	private TicketDetailHisDao ticketDetailHisDao;

	@Override
	public JsonPager<TicketDetailHis> findJsonPageByCriteria(
			JsonPager<TicketDetailHis> jp, TicketDetailHis t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void initBaseDAO(BaseDao<TicketDetailHis> baseDao) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public JsonPager<TicketDetailHis> findJsonPageByCriteriaForConfirm(
			JsonPager<TicketDetailHis> jp,
			TicketDetailHis ticketDetailHis) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketDetailHis.class);
		dc.createAlias("ticketPrice", "ticketPrice");
		dc.createAlias("ticketPrice.ticketType", "ticketType");
		dc.add(Property.forName("ticketIndent.indentId").eq(ticketDetailHis.getTicketIndent().getIndentId()));
		return ticketDetailHisDao.findJsonPageByCriteria(jp, dc);
	}
	
}