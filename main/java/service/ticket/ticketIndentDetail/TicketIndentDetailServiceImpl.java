package service.ticket.ticketIndentDetail;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import service.comm.baseCashAccount.BaseCashAccountService;
import service.comm.baseRechargeRec.BaseRechargeRecService;
import service.comm.baseRewardAccount.BaseRewardAccountService;
import service.comm.baseTranRec.BaseTranRecService;
import service.comm.reg.baseTravelAgency.BaseTravelAgencyService;
import service.sys.sysParam.SysParamService;
import service.ticket.financeTicketAccounts.FinanceTicketAccountsService;
import service.ticket.ticketRoleType.TicketRoleTypeService;
import service.ticket.ticketUserType.TicketUserTypeService;
import ws.ReturnInfo;
import ws.client.ticketIndent.TicketIndentClient;

import com.thoughtworks.xstream.XStream;
import com.xlem.dao.BaseCashAccount;
import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.BaseRewardAccount;
import com.xlem.dao.BaseRewardType;
import com.xlem.dao.BaseTourGuide;
import com.xlem.dao.BaseTourist;
import com.xlem.dao.BaseTranRec;
import com.xlem.dao.CommActivity;
import com.xlem.dao.SysRole;
import com.xlem.dao.SysUser;
import com.xlem.dao.SysUserRole;
import com.xlem.dao.TicketDetailHis;
import com.xlem.dao.TicketIndent;
import com.xlem.dao.TicketIndentDetail;
import com.xlem.dao.TicketPrice;
import com.xlem.dao.TicketType;
import com.xlem.dao.Travservice;
import com.xlem.dao.TravserviceHome;
import com.xlem.dao.User;

import common.BusinessException;
import common.CommonMethod;
import common.MD5;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.pay.PayFactoryService;
import common.pay.RefundParams;
import common.service.BaseServiceImpl;
import dao.comm.baseRewardType.BaseRewardTypeDao;
import dao.comm.baseTranRec.BaseTranRecDao;
import dao.comm.commActivity.CommActivityDao;
import dao.comm.commReserve.CommReserveDao;
import dao.comm.information.baseTourGuide.BaseTourGuideDao;
import dao.comm.reg.baseTourist.BaseTouristDao;
import dao.comm.reg.baseTravelAgency.BaseTravelAgencyDao;
import dao.sys.sysRole.SysRoleDao;
import dao.sys.sysUser.SysUserDao;
import dao.sys.sysUserRole.SysUserRoleDao;
import dao.ticket.ticketIndent.TicketIndentDao;
import dao.ticket.ticketIndentDetail.TicketDetailHisDao;
import dao.ticket.ticketIndentDetail.TicketIndentDetailDao;
import dao.ticket.ticketPrice.TicketPriceDao;
import dao.ticket.ticketType.TicketTypeDao;

@Service("ticketIndentDetailService")
public class TicketIndentDetailServiceImpl extends
		BaseServiceImpl<TicketIndentDetail> implements TicketIndentDetailService {
	@Autowired
	private TicketIndentDetailDao ticketIndentDetailDao;
	@Autowired
	private TicketIndentDao ticketIndentDao;
	@Autowired
	private TicketPriceDao ticketPriceDao;
	@Autowired
	private TicketDetailHisDao ticketDetailHisDao;
	@Autowired
	private TicketTypeDao ticketTypeDao;
	@Autowired
	private SysUserDao sysUserDao;
	@Autowired
	private BaseTravelAgencyDao baseTravelAgencyDao;
	@Autowired
	private CommActivityDao commActivityDao;
	@Autowired
	private CommReserveDao commReserveDao;
	@Autowired
	private BaseTranRecService baseTranRecService;
	@Autowired
	private BaseRechargeRecService baseRechargeRecService;
	@Autowired
	private PayFactoryService payFactoryService;
	@Autowired
	private BaseTranRecDao baseTranRecDao;
	@Autowired
	private BaseTourGuideDao baseTourGuideDao;
	@Autowired
	private BaseTouristDao baseTouristDao;
	@Autowired
	private SysUserRoleDao sysUserRoleDao;
	@Autowired
	private SysRoleDao sysRoleDao;
	@Autowired
	private SysParamService sysParamService;
	@Autowired
	private TicketUserTypeService ticketUserTypeService;
	@Autowired
	private TicketRoleTypeService ticketRoleTypeService;
	@Autowired
	private FinanceTicketAccountsService financeTicketAccountsService;
	@Autowired
	private BaseTravelAgencyService baseTravelAgencyService;
	@Autowired
	private BaseCashAccountService baseCashAccountService;
	@Autowired
	private BaseRewardTypeDao baseRewardTypeDao;
	@Autowired
	private BaseRewardAccountService baseRewardAccountService;
	
	
	private static Logger logger = Logger.getLogger("bookingService");
	
	@Override
	@Resource(name = "ticketIndentDetailDao")
	protected void initBaseDAO(BaseDao<TicketIndentDetail> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<TicketIndentDetail> findPageByCriteria(
			PaginationSupport<TicketIndentDetail> ps, TicketIndentDetail t) {
		DetachedCriteria dc = DetachedCriteria
				.forClass(TicketIndentDetail.class);
		return ticketIndentDetailDao.findPageByCriteria(ps,
				Order.desc("ticketIndentDetailId"), dc);
	}

	@Override
	public void save(TicketIndentDetail t) {
		// t.setTicketIndentDetailId(getNextKey("TicketIndentDetail".toUpperCase(),
		// 1));
		ticketIndentDetailDao.save(t);
	}

	@Override
	public JsonPager<TicketIndentDetail> findJsonPageByCriteria(
			JsonPager<TicketIndentDetail> jp, TicketIndentDetail t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketIndentDetail.class);
		dc.createAlias("ticketType", "ticketType");
		return ticketIndentDetailDao.findJsonPageByCriteria(jp, dc);
	}
	/**
	 * 省外旅行社订单和订单明细保存
	 */
	@Override
	public void saveForeign(Collection<TicketIndentDetail> coll,
			TicketIndentDetail t) {
		t.getTicketIndent().setStatus("21");
		save(coll,t);
	}
	/**
	 * 省内旅行社团购订单和订单明细保存
	 */
	@Override
	public void save(Collection<TicketIndentDetail> coll,TicketIndentDetail t) {
		TicketIndent ticketIndent = t.getTicketIndent();
		int totalAmount = 0;
		int gateAmount = 0;
		double totalPrice = 0;
		//增加门票+索道票+观景索道票套票  20150914 tt start
		List<String> normalTicketList = new ArrayList<String>();//普通门票List
		List<String> packageTickeList = new ArrayList<String>();//门票+索道票+观景索道票套票List
		List<String> fPackageTickeList = new ArrayList<String>();//门票+索道票+观景索道票套票散客List
		//增加门票+索道票+观景索道票套票  20150914 tt end
		double perFee = Double.parseDouble(findSysParam("PER_FEE"));
		
		Timestamp ctime = CommonMethod.getTimeStamp();
		if(ticketIndent.getUseDate().before(CommonMethod.toStartTime(CommonMethod.getTimeStamp()))){
			throw new BusinessException("游玩日期已过，无法购票!","");
		}
		//身份证和手机验证
		if((ticketIndent.getIdCardNumber()==null || ticketIndent.getIdCardNumber().isEmpty())
			&&(ticketIndent.getPhoneNumber()==null || ticketIndent.getPhoneNumber().isEmpty())){
			throw new BusinessException("取票人身份证和取票人手机号请选填一个，否则无法取票！","");
		}
		
		String BUY_TIME = sysParamService.findSysParam("BUY_TIME");
		String timeStr = CommonMethod.timeToString(Timestamp.valueOf(ticketIndent.getUseDate().toString()))+" "+BUY_TIME+":00";
		
		Timestamp endTime = CommonMethod.string2Time1(timeStr);
		
		if(endTime.before(ctime)){
			String BUY_TIME_CHECK = sysParamService.findSysParam("BUY_TIME_CHECK");
			if("true".equals(BUY_TIME_CHECK)){
				throw new BusinessException("超过"+BUY_TIME+"，交通索道已关闭，无法购票!","");
			}
		}
		
		if("01".equals(ticketIndent.getCustomerType()) && !"1".equals(t.getBuyType())){//团客订单判断是否需要新增导游
			ticketIndent.setLinkman(ticketIndent.getLinkman().trim());
			boolean isHave = false;
			BaseTourGuide btg = null;
			if(CommonMethod.isNumber(ticketIndent.getLinkman())){//是不是整数或文字
				btg = baseTourGuideDao.findById(Long.parseLong(ticketIndent.getLinkman()));
			}else{
				DetachedCriteria dcBTU = DetachedCriteria.forClass(BaseTourGuide.class);
				dcBTU.add(Property.forName("name").eq(ticketIndent.getLinkman()));
				dcBTU.add(Property.forName("phoneNumber").eq(ticketIndent.getPhoneNumber()));
				dcBTU.add(Property.forName("idCardNumber").eq(ticketIndent.getIdCardNumber()));
				dcBTU.add(Property.forName("guideNumber").eq(ticketIndent.getGuideNumber()));
				
				List<BaseTourGuide> listSU = baseTourGuideDao.findByCriteria(dcBTU);;
				for(BaseTourGuide sr : listSU){
					btg = sr;
				}
			}
			
			if(btg!=null){
				isHave = true;
			}
			
			
			if(isHave){
				ticketIndent.setLinkman(btg.getName());
			}
			else{//新增导游
				btg = new BaseTourGuide();
				SysUser sysUser = getSysUser();
//				BaseTravelAgency baseTravelAgency = baseTravelAgencyDao.findById(sysUser.getBaseTravelAgency().getTravelAgencyId());
				
				btg.setTourGuideId(getNextKey("Base_Tour_Guide", 1));
				btg.setName(ticketIndent.getLinkman());
				btg.setPhoneNumber(ticketIndent.getPhoneNumber());
				btg.setIdCardNumber(ticketIndent.getIdCardNumber());
				btg.setGuideNumber(ticketIndent.getGuideNumber());
				btg.setBaseTravelAgency(sysUser.getBaseTravelAgency());
				baseTourGuideDao.save(btg);
			}
		}
		
		boolean updateRemain = true;
		Long userId=null;
		if(ticketIndent.getIndentId()==null || ticketIndent.getIndentId().trim().isEmpty()){
			userId = getCurrentUserId();
		}else{
			userId = ticketIndent.getInputer();
			updateRemain = false;
		}
		Long roleId = 0L;
		if(userId==null){
			userId = 0L;
			DetachedCriteria dcSU = DetachedCriteria.forClass(SysRole.class);
			dcSU.add(Property.forName("roleCode").eq("003"));
			List<SysRole> listSR = sysRoleDao.findByCriteria(dcSU);
			for(SysRole sr : listSR){
				roleId = sr.getRoleId();
			}
			
		}else{
			DetachedCriteria dcSU = DetachedCriteria.forClass(SysUserRole.class);
			dcSU.add(Property.forName("sysUser.userId").eq(userId));
			List<SysUserRole> listSU = sysUserRoleDao.findByCriteria(dcSU);
			for(SysUserRole sr : listSU){
				roleId = sr.getSysRole().getRoleId();
			}
		}
		
		String isOutbound = t.getTicketIndent().getIsOutbound();//'11'是境外团
		
		
		
		boolean isAgr = sysUserDao.findIsAgreementByUseId(userId);
		
		Map<String,Integer> gameNums = new HashMap<String,Integer>();
		Map<String,String> gameGate = new HashMap<String,String>();
		Map<String,TicketPrice> tpMap = new HashMap<String,TicketPrice>();
		if("02".equals(ticketIndent.getCustomerType())){//散客需判断购买数量
			for (TicketIndentDetail ticketIndentDetail : coll) {
				if(ticketIndentDetail.getAmount()!=null && ticketIndentDetail.getAmount()>0){
					ticketIndentDetail.setRemark(t.getRemark());
					ticketIndentDetail.setBuyType(t.getBuyType());
					TicketPrice ticketPrice = checkTopLimit(ticketIndent, ticketIndentDetail, ticketIndent.getCustomerType(), isOutbound, isAgr, userId, roleId, updateRemain);
					tpMap.put("tp_"+ticketPrice.getTicketPriceId(), ticketPrice);
					TicketType ticketType = ticketPrice.getTicketType();
					if("02".equals(ticketType.getTicketBigType())){//项目票
						
						String gate =  ticketType.getGate();
						int amount = ticketIndentDetail.getAmount();
						String gateName = ticketType.getGateText().replaceAll("闸口","票");
						
						int gaNum = gameNums.get("gate"+gate)==null?0:gameNums.get("gate"+gate);
						
						gaNum =  gaNum + amount;
						gameNums.put("gate"+gate, gaNum);
						gameGate.put("gate"+gate, gateName);
					}else{
						gateAmount = gateAmount +ticketIndentDetail.getAmount();
					}
					//散客索道套票List
					if(ticketType.getActivity()!=null && "01".equals(ticketType.getActivity())){
						fPackageTickeList.add(ticketType.getTicketName());
					}
					//
					long typeId = ticketType.getTicketId()==null || "".equals(ticketType.getTicketId()) ? 0L : ticketType.getTicketId();
					if(3062==typeId || 3063==typeId){//观景索道抢购活动票，每个身份证、手机号每种票只能一张
						
						String cNum = ticketIndent.getIdCardNumber();
						if(cNum==null || cNum.isEmpty()){//没有身份证
							throw new BusinessException("对应票种："+ticketType.getTicketName()+"，购买时必须填写身份证！","");
						}
						String pNum = ticketIndent.getPhoneNumber();
						//重复购买检测，每个身份证一个月限定二张
						DetachedCriteria dcI = DetachedCriteria.forClass(TicketIndentDetail.class);
						dcI.createAlias("ticketIndent", "ticketIndent");
						dcI.createAlias("ticketPrice", "ticketPrice");
						dcI.createAlias("ticketPrice.ticketType", "ticketType");
						dcI.add(Property.forName("ticketIndent.idCardNumber").eq(cNum));
						dcI.add(Property.forName("ticketIndent.status").in(new String[]{"01","02","03","04","05","08"}));
						dcI.add(Property.forName("ticketType.ticketId").in(new Long[]{3062L,3063L}));
						List<TicketIndentDetail> tisI = this.findByCriteria(dcI);
						if(tisI!=null && tisI.size()>0){
							throw new BusinessException("身份证："+cNum+"已购买过活动票，不能重复购买！","");
						}
						
						//重复购买检测，每个身份证一个月限定二张
						DetachedCriteria dcP = DetachedCriteria.forClass(TicketIndentDetail.class);
						dcP.createAlias("ticketIndent", "ticketIndent");
						dcP.createAlias("ticketPrice", "ticketPrice");
						dcP.createAlias("ticketPrice.ticketType", "ticketType");
						dcP.add(Property.forName("ticketIndent.phoneNumber").eq(pNum));
						dcP.add(Property.forName("ticketIndent.status").in(new String[]{"01","02","03","04","05","08"}));
						dcP.add(Property.forName("ticketType.ticketId").in(new Long[]{3062L,3063L}));
						List<TicketIndentDetail> tisP = this.findByCriteria(dcP);
						if(tisP!=null && tisP.size()>0){
							throw new BusinessException("手机："+pNum+"已购买过活动票，不能重复购买！","");
						}
						
					}
				}
			}
			
			String iTop = findSysParam("INDENT_TOPLIMIT");
			int inTop = 0;
			if(iTop!=null && iTop!=""){
				inTop = Integer.valueOf(iTop);
			}
			
			for(String key : gameNums.keySet()){
				int gaNum = gameNums.get(key);
				if(inTop>0 && gaNum > inTop){
					throw new BusinessException("您购买的"+gameGate.get(key)+"数量："+gaNum+"已大于购买单种项目票的上限："+inTop,"");
				}
			}
			
			//20141203 限制散客每日购票不超过5张 start
			if(!"05".equals(ticketIndent.getPayType()) && !"06".equals(ticketIndent.getPayType())
					&& (fPackageTickeList==null || fPackageTickeList.isEmpty())){
//				DetachedCriteria tiDc = DetachedCriteria.forClass(TicketIndent.class);
//				tiDc.add(Property.forName("inputer").eq(getCurrentUserId()));
//				tiDc.add(Property.forName("useDate").eq(ticketIndent.getUseDate()));
//				tiDc.add(Property.forName("status").in(new String[]{"02","03","04","05"}));
//				//20150514 start
//				tiDc.add(Property.forName("indentType").eq("01"));
//				//20150514 end
//				List<TicketIndent> tiList = ticketIndentDao.findByCriteria(tiDc);
//				for(TicketIndent ti:tiList){
//					gateAmount = gateAmount +ti.getTotalAmount();
//				}
				DetachedCriteria tiDc = DetachedCriteria.forClass(TicketIndentDetail.class);
				tiDc.createAlias("ticketIndent", "ticketIndent");
				tiDc.createAlias("ticketPrice", "ticketPrice");
				tiDc.createAlias("ticketPrice.ticketType", "ticketType");
				tiDc.add(Property.forName("ticketIndent.inputer").eq(getCurrentUserId()));
				tiDc.add(Property.forName("ticketIndent.useDate").eq(ticketIndent.getUseDate()));
				tiDc.add(Property.forName("ticketIndent.indentType").eq("01"));
				tiDc.add(Property.forName("ticketIndent.status").in(new String[]{"02","03","04","05"}));
				tiDc.add(Property.forName("ticketType.activity").eq("00"));
				List<TicketIndentDetail> tidList = this.findByCriteria(tiDc);
				if(tidList!=null && tidList.size()>0){
					for(TicketIndentDetail tid:tidList){
						gateAmount = gateAmount +tid.getAmount();
					}
				}
				if(gateAmount>5){
					throw new BusinessException("您购买的"+CommonMethod.timeToString(Timestamp.valueOf(ticketIndent.getUseDate().toString()))+"日的门票总数量已大于5张，不能购买！","");
				}
			}
			//20141203 限制散客每日购票不超过5张 end
		}
		
		for (TicketIndentDetail ticketIndentDetail : coll) {//保存定单明细计算总数量总金额
			if(ticketIndentDetail.getAmount()!=null && ticketIndentDetail.getAmount()>0){
				ticketIndentDetail.setRemark(t.getRemark());
				ticketIndentDetail.setBuyType(t.getBuyType());
				TicketPrice ticketPrice=null;
				
				if(tpMap.size()>0){
					ticketPrice = tpMap.get("tp_"+ticketIndentDetail.getTicketPrice().getTicketPriceId());
				}else{
					ticketPrice = checkTopLimit(ticketIndent, ticketIndentDetail, ticketIndent.getCustomerType(), isOutbound, isAgr, userId, roleId, updateRemain);
				}
				
				ticketIndentDetail.setTicketPrice(ticketPrice);
				TicketType ticketType = ticketPrice.getTicketType();
				
				checkActivity(ticketType,ticketIndentDetail,t, ticketIndent);
				ticketIndentDetail.setIndentDetailId(""+getNextKey("Ticket_Indent_Detail", 1));
				ticketIndentDetail.setTicketIndent(ticketIndent);
				//境外团购票和协议旅行社计算价格为打印价格
				if("11".equals(isOutbound) || sysUserDao.findIsAgreementByUseId(userId)){//lzp 2013-11-10 合并购票核心部分
					ticketIndentDetail.setUnitPrice(ticketPrice.getPrintPrice());
					ticketIndentDetail.setEntranceUnitPrice(ticketPrice.getEntrancePrice());
					ticketIndentDetail.setRopewayUnitPrice(ticketPrice.getRopewayPrice());
				}else{
					ticketIndentDetail.setUnitPrice(ticketPrice.getUnitPrice());
					ticketIndentDetail.setEntranceUnitPrice(ticketPrice.getEntranceUnitPrice());
					ticketIndentDetail.setRopewayUnitPrice(ticketPrice.getRopewayUnitPrice());
				}
				
				ticketIndentDetail.setEntranceCompany(ticketPrice.getEntranceCompany());
				ticketIndentDetail.setRopewayCompany(ticketPrice.getRopewayCompany());
				
				ticketIndentDetail.setSubtotal(ticketIndentDetail.getAmount()*ticketIndentDetail.getUnitPrice());
				ticketIndentDetail.setInputer(getCurrentUserId());
				ticketIndentDetail.setInputTime(ctime);
				ticketIndentDetail.setPerFee(perFee);
				ticketIndentDetail.setPayFee(ticketIndentDetail.getAmount()*ticketIndentDetail.getUnitPrice()*ticketIndentDetail.getPerFee());
				
				if("02".equals(ticketType.getTicketBigType())){//项目票
					
					int remainToplimit = ticketPrice.getRemainToplimit() == null ? 0 : ticketPrice.getRemainToplimit();
					remainToplimit = remainToplimit - ticketIndentDetail.getAmount();
					if(remainToplimit<0){
						throw new BusinessException("您选择的项目剩余数量不足！","");
					}
					ticketPrice.setRemainToplimit(remainToplimit);
					
				}
				
				//增加门票+索道票+观景索道票套票  20150914 tt start
				if(ticketType.getActivity()!=null && "01".equals(ticketType.getActivity())){
					packageTickeList.add(ticketType.getTicketName());
				}else{
					normalTicketList.add(ticketType.getTicketName());
				}
				//增加门票+索道票+观景索道票套票  20150914 tt end
				
				totalAmount += ticketIndentDetail.getAmount();
				totalPrice += ticketIndentDetail.getAmount()*ticketIndentDetail.getUnitPrice();
				ticketPriceDao.merge(ticketPrice);
			}
		}
		
		//增加门票+索道票+观景索道票套票限制  20150914 tt start
		if(packageTickeList!=null && !packageTickeList.isEmpty()
				&& normalTicketList!=null && !normalTicketList.isEmpty()){
				throw new BusinessException("'"+normalTicketList.get(0)+"'"+"与"+"'"+packageTickeList.get(0)+"'"+"不能同时购买！","");
		}
		//增加门票+索道票+观景索道票套票限制  20150914 tt end
		//增加门票+索道票+观景索道票套票身份证购买限制  20151026 tt start
//		if(packageTickeList!=null && !packageTickeList.isEmpty()){
//			//单次购买张数不能大于3张
//			if(totalAmount>3){
//				throw new BusinessException("您购买的"+CommonMethod.timeToString(ticketIndent.getUseDate())+"日的索道套票已大于3张，不能购买！","");
//			}
//			//重复购买检测，每个身份证一个月内的票限定购买一次
//			DetachedCriteria dcP = DetachedCriteria.forClass(TicketIndentDetail.class);
//			dcP.createAlias("ticketIndent", "ticketIndent");
//			dcP.createAlias("ticketPrice", "ticketPrice");
//			dcP.createAlias("ticketPrice.ticketType", "ticketType");
//			dcP.add(Property.forName("ticketIndent.idCardNumber").eq(ticketIndent.getIdCardNumber()));
//			dcP.add(Property.forName("ticketIndent.useDate").le(CommonMethod.string2Time1(CommonMethod.getLastDayOfMonth(CommonMethod.timeToString(ticketIndent.getUseDate())))));
//			dcP.add(Property.forName("ticketIndent.useDate").ge(CommonMethod.string2Time1(CommonMethod.getFirstDayOfMonth(CommonMethod.timeToString(ticketIndent.getUseDate())))));
//			dcP.add(Property.forName("ticketIndent.status").in(new String[]{"01","02","03","04","05","08"}));
//			dcP.add(Property.forName("ticketType.activity").eq("01"));
//			List<TicketIndentDetail> tisP = this.findByCriteria(dcP);
//			if(tisP!=null && tisP.size()>0){
//				throw new BusinessException("身份证："+ticketIndent.getIdCardNumber()+"已购买过一个月内的索道套票，不能重复购买！","");
//			}
//		}
		//增加门票+索道票+观景索道票套票身份证购买限制  20151026 tt end
		//购买票时，设定订单类型   2013-12-10 lzp
		setIndentType(ticketIndent,coll);
		
		if("05".equals(ticketIndent.getPayType()) || "06".equals(ticketIndent.getPayType())){//微信或微博订单
			ticketIndent.setStatus("03");
		}else if(!"21".equals(ticketIndent.getStatus())){//协议旅行社保存订单
			ticketIndent.setStatus("01");//未支付
		}
		ticketIndent.setTotalAmount(totalAmount);
		ticketIndent.setTotalPrice(totalPrice);
		ticketIndent.setPerFee(perFee);
		ticketIndent.setPayFee(totalPrice*perFee);
		ticketIndent.setSynchroState("00");//初始未同步状态
		ticketIndent.setIsOutbound(isOutbound);//非境外订单
		if("1".equals(t.getBuyType())){
			ticketIndent.setTelNumber("1");
		}
		//游玩当日门票上限check 2014-01-26 tt
		checkAmount(ticketIndent);
		
		if(t.getTicketIndent().getIsOutbound()!=null && t.getTicketIndent().getIsOutbound().trim().equals("11")){
			ticketIndent.setStatus("21");//未支付
		}
		if(ticketIndent.getIndentId()==null || ticketIndent.getIndentId().trim().isEmpty()){//无id时新增订单
			ticketIndent.setCheckCode(findCheckCode(ticketIndent));//验证码
			ticketIndent.setIndentCode(getNextIndentCode(CommonMethod.getCurrentYMD()+"000001",CommonMethod.getCurrentYMD()));
			ticketIndent.setIndentId(getNextKey("ticket_Indent", 1).toString());
			ticketIndent.setInputer(getCurrentUserId());
			ticketIndent.setInputTime(ctime);
			ticketIndentDao.save(ticketIndent);
		}
		else{//修改订单,首先删除所有订单明细
			ticketIndentDetailDao.deleteByIndentId(ticketIndent.getIndentId());
			ticketIndent.setUpdater(getCurrentUserId());
			ticketIndent.setUpdateTime(ctime);
			ticketIndentDao.update(ticketIndent);
		}
		for (TicketIndentDetail ticketIndentDetail : coll) {//保存完订单信息，再保存定单明细信息
			if(ticketIndentDetail.getIndentDetailId()!=null && !ticketIndentDetail.getIndentDetailId().isEmpty()){
				ticketIndentDetailDao.save(ticketIndentDetail);
			}
		}
	}
	/**
	 * 设置项目类型
	 * @param coll 
	 * @param ticketIndent订单类型：01只有门票；02只有项目票；03门票+项目票；04补项目票
	 */
	private void setIndentType(TicketIndent ticketIndent, Collection<TicketIndentDetail> coll) {
		//补买项目票
		if(ticketIndent.getParentsIndentId()!=null && !ticketIndent.getParentsIndentId().isEmpty()){
			ticketIndent.setIndentType("04");
		}else if(ticketIndent.getIndentType() != null){//接口购买项目票
			
		}else{
			ticketIndent.setIndentType("01");
		}
	}
	/**
	 * 验证购票权限及购票上限,同时修改剩余数量
	 * @param ticketIndent 
	 * @param t
	 * @param customerType 客户类型：01团，02散
	 * @param isOutbound 是否境外团：11是
	 * @param isAgr 是否协议
	 * @param userId 
	 * @param roleId 
	 * @param updateRemain 是否修改剩余数量
	 * @return
	 */
	@Override
	public TicketPrice checkTopLimit(TicketIndent ticketIndent,TicketIndentDetail t, String customerType,String isOutbound, boolean isAgr,long userId,long roleId,boolean updateRemain){
		
		String fun = "Group";
		if("02".equals(ticketIndent.getCustomerType()) && !"05".equals(ticketIndent.getPayType()) && !"06".equals(ticketIndent.getPayType())){
			fun = "BuyTicket";
		}
		
		String usePage = "02";
		if("11".equals(isOutbound)){//境外团
			usePage = "04";
		}else if("webService".equals(t.getRemark())){//接口
			usePage = "08";
		}else if("fProject".equals(t.getRemark())//散客购观景索道票
				|| "fgames".equals(t.getRemark())//散客购项目票
				|| "acProject".equals(t.getRemark())){//团客购买观景索道活动票
			usePage = "07";
		}else if("1".equals(t.getBuyType())){//代散客
			usePage = "05";
		}else if(ticketIndent.getParentsIndentId()!=null && !ticketIndent.getParentsIndentId().isEmpty()){//团客购项目票
			usePage = "07";
		}else if(isAgr){//协议
			usePage = "03";
		}else if("home".equals(t.getRemark())){
			usePage = "01";
		}else if("act".equals(t.getRemark())){//抢购活动
			usePage = "06";
		}
		TicketPrice tPrice = new TicketPrice();
		if("act".equals(t.getRemark())){
			tPrice = t.getTicketPrice();
			tPrice.setRemark("act");
		}
		if("webService".equals(t.getRemark())){
			tPrice = t.getTicketPrice();
			tPrice.setRemark("webService");
		}
		if("acProject".equals(t.getRemark())){
			tPrice = t.getTicketPrice();
		}
		if("buyTicketFlag".equals(t.getRemark())){
			tPrice.setRemark("buyTicketFlag");
		}
		//画面购买观景索道活动票时，设置标识acPayProject
		if(ticketIndent.getAuditor()!=null &&1==ticketIndent.getAuditor()){
			tPrice = t.getTicketPrice();
			tPrice.setRemark("acPayProject");
		}
		
		TicketPrice ticketPrice = ticketPriceDao.findTicketByPriceId(tPrice, Timestamp.valueOf(ticketIndent.getUseDate().toString()), usePage, userId, roleId, fun, t.getTicketPrice().getTicketPriceId());
		
		if(ticketPrice==null){
			throw new BusinessException("您的订单中有未开放购买的票种，订单保存失败！","");
		}
		
		t.setTicketPrice(ticketPrice);
		TicketType ticketType = ticketPrice.getTicketType();
		Integer indTop = ticketPrice.getIndentToplimit();
		if(indTop!=null && indTop>0){
			if(indTop<t.getAmount()){
				throw new BusinessException("您的订单中"+ticketType.getTicketName()+"不能大于"+indTop+"张，订单保存失败！","");
			}
		}
		Integer useIndTop = ticketPrice.getUserIndentToplimit();
		int thisIndOldAm = 0;
		
		DetachedCriteria dc = DetachedCriteria.forClass(TicketIndentDetail.class);
//		dc.createAlias("ticketPrice", "ticketPrice");
//		dc.createAlias("ticketPrice.ticketType", "ticketType");
		
		dc.add(Property.forName("inputer").eq(userId));
//		dc.add(Property.forName("ticketType.ticketId").eq(ticketType.getTicketId()));
		dc.add(Property.forName("ticketPrice.ticketPriceId").eq(t.getTicketPrice().getTicketPriceId()));
		
		List<TicketIndentDetail> tidList = ticketIndentDetailDao.findByCriteria(dc);
		for(TicketIndentDetail tid : tidList){
			if(ticketIndent.getIndentId()!=null && ticketIndent.getIndentId().equals(tid.getTicketIndent().getIndentId())){
				thisIndOldAm = tid.getAmount();
			}
		}
		
		
		if(useIndTop!=null && useIndTop>0){
			int oldAm = 0;
			for(TicketIndentDetail tid : tidList){
				if(ticketIndent.getIndentId()!=null && !ticketIndent.getIndentId().equals(tid.getTicketIndent().getIndentId())){
					TicketIndent ti = ticketIndentDao.findById(tid.getTicketIndent().getIndentId());//排除取消、退订、拒绝的订单
					if(!"06".equals(ti.getStatus()) && !"07".equals(ti.getStatus()) && !"08".equals(ti.getStatus()) && !"24".equals(ti.getStatus())){
						oldAm += tid.getAmount();
					}
				}
			}
			if(useIndTop<(oldAm+t.getAmount())){
				throw new BusinessException("您的订单中"+ticketType.getTicketName()+"您只能购买"+useIndTop+"张，您已购买"+oldAm+"张，订单保存失败！","");
			}
		}
		
		Integer topLimit = ticketPrice.getToplimitT();
		Integer rTopLimit = ticketPrice.getRemainToplimitT();
		if(topLimit!=null && topLimit>0){//被设置上限
			int amount = t.getAmount();
			//计算下订单后票的剩余数量
			if(ticketIndent.getIndentId()==null || ticketIndent.getIndentId().trim().isEmpty()){//新增订单
			}else{
				rTopLimit = rTopLimit + thisIndOldAm;
			}
			
			rTopLimit = rTopLimit - amount;
			if(rTopLimit<0){
				throw new BusinessException("您选择的"+ticketType.getTicketName()+"剩余数量不足！","");
			}
			if(thisIndOldAm>0 && amount!=thisIndOldAm){//修改订单
				updateRemain = true;
			}
			if(updateRemain){
				int updateNums = amount - thisIndOldAm;
				ticketUserTypeService.updateReminTopLimit(userId, ticketType.getTicketId(), updateNums);
				ticketRoleTypeService.updateReminTopLimit(roleId, ticketType.getTicketId(), updateNums);
			}
		}
		
		return ticketPrice;
	}
	/**
	 * 验证活动票
	 * @param ticketType
	 * @param ticketIndentDetail
	 * @param t
	 * @param ticketIndent
	 */
	private void checkActivity(TicketType ticketType,TicketIndentDetail ticketIndentDetail,TicketIndentDetail t,TicketIndent ticketIndent){
		if("05".equals(ticketType.getSpecialType()) && "03".equals(ticketType.getTicketBigType())){//特殊活动票
			//如果是活动票则需要修改活动上限
			if(t.getRemark()!=null&&!t.getRemark().isEmpty()&&!CommonMethod.isNull(t.getRemark())){
				if("act".equals(t.getRemark())){
					long activityId = Long.valueOf(ticketIndent.getActivityId());
					CommActivity commActivity = commActivityDao.findById(activityId);
					//验证是否已参与活动
					DetachedCriteria dc = DetachedCriteria.forClass(TicketIndent.class);
					dc.add(Property.forName("inputer").eq(getCurrentUserId()));
					dc.add(Restrictions.or(Restrictions.or(Property.forName("activityId").eq(Long.valueOf(3)),
							Property.forName("activityId").eq(Long.valueOf(4))),Property.forName("activityId").eq(Long.valueOf(5))));
					List<TicketIndent> tList = ticketIndentDao.findByCriteria(dc);
					if(tList!=null&&tList.size()>0){//已参加活动
						throw new BusinessException("感谢您参与"+commActivity.getActivityName()+"!您已成功抢购本次活动票不能再次抢购!","");
					}
	//				else{//没参加活动，验证是否预订
	//					DetachedCriteria dcCR = DetachedCriteria.forClass(CommReserve.class);
	//					dcCR.add(Property.forName("userId").eq(getCurrentUserId()));
	//					dcCR.add(Property.forName("commActivity.activityId").eq(activityId));
	//					List<CommReserve> commReserve = commReserveDao.findByCriteria(dcCR);
	//					if(commReserve!=null && commReserve.size()>0){//有预约
	//					}else{
	//						throw new BusinessException("感谢您参与"+commActivity.getActivityName()+"!您没有进行预约，不能参与购买!","");
	//					}
	//				}
					
					if(ticketIndentDetail.getAmount()>commActivity.getActivityAmount()){
						throw new BusinessException("感谢您参与"+commActivity.getActivityName()+"!本次活动票每人限购"+commActivity.getActivityAmount()+"张！","");
					}
					
					int limit = commActivity.getActivityLimit()-ticketIndentDetail.getAmount();
					
					if("5".equals(activityId)){
						if(limit<0){
							throw new BusinessException("感谢您参与"+commActivity.getActivityName()+"!本次活动提供的门票已售罄!","");
						}
					}else{
						if(limit<0){
							throw new BusinessException("该种雪季特价门票已经售罄，请到活动首页购买其他雪季特价门票!","");
						}
					}
					
					commActivity.setActivityLimit(limit);
					if(limit<1){
						Timestamp aet = CommonMethod.getTimeStamp();
						commActivity.setActivityEndTime(CommonMethod.addHour(aet, 2));
					}
					commActivityDao.update(commActivity);
					ticketIndent.setActivityId(activityId);
				}else if ("webService".equals(t.getRemark())){
					long activityId = Long.valueOf(ticketIndent.getActivityId());
					CommActivity commActivity = commActivityDao.findById(activityId);
					
					int limit = commActivity.getActivityLimit()-ticketIndentDetail.getAmount();
					
					if("8".equals(activityId) || "11".equals(activityId) || "14".equals(activityId)){
						if(limit<0){
							throw new BusinessException("感谢您参与"+commActivity.getActivityName()+"!本次活动提供的门票已售罄!","");
						}
					}else{
						if(limit<0){
							throw new BusinessException("剩余票数不足,"+ticketType.getTicketName()
									+"只剩余"+commActivity.getActivityLimit()+"张票!","");
						}
					}
					
					commActivity.setActivityLimit(limit);
					if(limit<1){
						commActivity.setActivityEndTime(CommonMethod.getTimeStamp());
					}
					commActivityDao.update(commActivity);
				}
			}else{//不是活动页面来的
				throw new BusinessException("你所选择的票种已停止销售！","");
			}
		}
	}
	@Override
	public void saveHomeIndemt(Collection<TicketIndentDetail> coll,TicketIndentDetail t) {
		TicketIndent ticketIndent = t.getTicketIndent();
		int totalAmount = 0;
		int gateAmount = 0;
		double totalPrice = 0;
		double perFee = Double.parseDouble(findSysParam("PER_FEE"));
		Timestamp ctime = CommonMethod.getTimeStamp();
		
		if(ticketIndent.getUseDate().before(CommonMethod.toStartTime(CommonMethod.getTimeStamp()))){
			throw new BusinessException("游玩日期已过，无法购票!","");
		}
		
		//身份证和手机验证
		if((ticketIndent.getIdCardNumber()==null || ticketIndent.getIdCardNumber().isEmpty())
			&&(ticketIndent.getPhoneNumber()==null || ticketIndent.getPhoneNumber().isEmpty())){
			throw new BusinessException("取票人身份证和取票人手机号请选填一个，否则无法取票！","");
		}
		
		String BUY_TIME = sysParamService.findSysParam("BUY_TIME");
		String timeStr = CommonMethod.timeToString(Timestamp.valueOf(ticketIndent.getUseDate().toString()))+" "+BUY_TIME+":00";
		
		Timestamp endTime = CommonMethod.string2Time1(timeStr);
		
		if(endTime.before(ctime)){
			String BUY_TIME_CHECK = sysParamService.findSysParam("BUY_TIME_CHECK");
			if("true".equals(BUY_TIME_CHECK)){
				throw new BusinessException("超过"+BUY_TIME+"，交通索道已关闭，无法购票!","");
			}
		}
//		if("01".equals(ticketIndent.getCustomerType())){//团客订单判断是否需要新增导游
//			boolean isHave = false;
//			BaseTourGuide btg = null;
//			if(CommonMethod.isNumber(ticketIndent.getLinkman())){//是不是整数或文字
//				btg = baseTourGuideDao.findById(Long.parseLong(ticketIndent.getLinkman()));
//				if(btg!=null){
//					isHave = true;
//				}
//			}
//			if(isHave){
//				ticketIndent.setLinkman(btg.getName());
//			}
//			else{//新增导游
//				btg = new BaseTourGuide();
//				SysUser sysUser = getSysUser();
////				BaseTravelAgency baseTravelAgency = baseTravelAgencyDao.findById(sysUser.getBaseTravelAgency().getTravelAgencyId());
//				
//				btg.setTourGuideId(getNextKey("Base_Tour_Guide", 1));
//				btg.setName(ticketIndent.getLinkman());
//				btg.setPhoneNumber(ticketIndent.getPhoneNumber());
//				btg.setIdCardNumber(ticketIndent.getIdCardNumber());
//				btg.setGuideNumber(ticketIndent.getGuideNumber());
//				btg.setBaseTravelAgency(sysUser.getBaseTravelAgency());
//				baseTourGuideDao.save(btg);
//			}
//		}
		
		String msg = "";
		
		String phoneNumber = ticketIndent.getPhoneNumber();
		
		DetachedCriteria dc = DetachedCriteria.forClass(SysUser.class);
		dc.add(Property.forName("phoneNumber").eq(phoneNumber.trim()));
		List<SysUser> suList = sysUserDao.findByCriteria(dc);
		SysUser sysUser;
		if(suList!=null && suList.size()>0){
			sysUser = suList.get(0); 
			
			if("01".equals(sysUser.getUserType())){//员工
				throw new BusinessException("您的手机号已注册成为员工，不能在首页购票!","");
			}else if("03".equals(sysUser.getUserType())){//旅行社
				throw new BusinessException("您的手机号已注册成为旅行社账号，不能在首页购票!","");
			}
			
			msg = "感谢您使用西岭雪山订票系统，您已有西岭雪山电子商务平台账户！登录后可自行退订、取消订单，账户号为："+
				sysUser.getUserCode()+"，登录的网址为：http://www.xilingxueshan.cn/booking。";
		}else{//添加新账户
			sysUser = new SysUser();
			
			String uCode = ticketIndent.getPhoneNumber();
			if(ticketIndent.getPhoneNumber()==null || ticketIndent.getPhoneNumber().isEmpty()){
				uCode = baseTouristDao.getNextUserCode();
			}
			sysUser.setUserCode(uCode);
			long captcha =  Math.round(Math.random()*(999999-100000)+100000);
			
			sysUser.setPassword(MD5.getMD5(String.valueOf(captcha)));
			sysUser.setUserType("02");
			sysUser.setStatus("11");
			sysUser.setUserName(ticketIndent.getLinkman());
			sysUser.setPhoneNumber(ticketIndent.getPhoneNumber());
			sysUser.setUserId(getNextKey("SYS_USER".toUpperCase(), 1));
			SysUserRole sysUserRole = new SysUserRole();
			sysUserRole.setSysUser(sysUser);
			
			
			List<SysRole> srList = sysRoleDao.findByProperty("roleCode", findSysParam("ROLE_TOURIST_CODE"));
			SysRole sysRole = srList.get(0);
			sysUserRole.setSysRole(sysRole);
			sysUserRole.setUserRoleId(getNextKey("SYS_USER_ROLE".toUpperCase(), 1));
			
			BaseTourist bt = new BaseTourist();
			
			bt.setTouristId(getNextKey("BASE_TOURIST".toUpperCase(), 1));
			bt.setTouristName(ticketIndent.getLinkman());
			bt.setPhoneNumber(ticketIndent.getPhoneNumber());
			bt.setIdCardNumber(ticketIndent.getIdCardNumber());
			bt.setInputTime(CommonMethod.getTimeStamp());
			sysUser.setBaseTourist(bt);
			
			baseTouristDao.save(bt);
			sysUserDao.save(sysUser);
			sysUserRoleDao.save(sysUserRole);
			
			msg = "感谢您使用西岭雪山订票系统，我们为您注册了西岭雪山电子商务平台账户！登录后可自行退订、取消订单，<font color='red' style='font: 14px/35px \"宋体\";font-weight: bold;'>账户号为："+
				sysUser.getUserCode()+"，初始密码为："+
				captcha+"，建议尽快修改密码</font>，登录的网址为：<font color='red' style='font: 14px/35px \"宋体\";font-weight: bold;'>http://www.xilingxueshan.cn/booking</font>。";
			
			if(!(ticketIndent.getPhoneNumber()==null || ticketIndent.getPhoneNumber().isEmpty())){
				CommonMethod.sendSms(sysUser.getPhoneNumber(), "感谢您使用西岭雪山订票系统，我们为您注册了西岭雪山电子商务平台账户！登录后可自行退订、取消订单，账户号为："+
						sysUser.getUserCode()+"，初始密码为："+
						captcha+"，建议尽快修改密码，登录的网址为：http://www.xilingxueshan.cn/booking。",sysParamService.findSysParam("USE_NEW_PLATFROM"));
			}
			
		}
		ticketIndent.setMsg(msg);
		//ActionContext.getContext().getSession().put("userId", sysUser.getUserId());
		
		Long userId = sysUser.getUserId();
		
		Long roleId = 0L;
		if(userId==null){
			userId = 0L;
			DetachedCriteria dcSU = DetachedCriteria.forClass(SysRole.class);
			dcSU.add(Property.forName("roleCode").eq("003"));
			List<SysRole> listSR = sysRoleDao.findByCriteria(dcSU);
			for(SysRole sr : listSR){
				roleId = sr.getRoleId();
			}
			
		}else{
			DetachedCriteria dcSU = DetachedCriteria.forClass(SysUserRole.class);
			dcSU.add(Property.forName("sysUser.userId").eq(userId));
			List<SysUserRole> listSU = sysUserRoleDao.findByCriteria(dcSU);
			for(SysUserRole sr : listSU){
				roleId = sr.getSysRole().getRoleId();
			}
		}
		
		Map<String,Integer> gameNums = new HashMap<String,Integer>();
		Map<String,String> gameGate = new HashMap<String,String>();
		Map<String,TicketPrice> tpMap = new HashMap<String,TicketPrice>();
		//散客需判断购买数量
		for (TicketIndentDetail ticketIndentDetail : coll) {
			if(ticketIndentDetail.getAmount()!=null && ticketIndentDetail.getAmount()>0){
				ticketIndentDetail.setRemark(t.getRemark());
				ticketIndentDetail.setBuyType(t.getBuyType());
				TicketPrice ticketPrice = checkTopLimit(ticketIndent, ticketIndentDetail, "02", "", false, userId, roleId,true);
				tpMap.put("tp_"+ticketPrice.getTicketPriceId(), ticketPrice);
				TicketType ticketType = ticketPrice.getTicketType();
				if("02".equals(ticketType.getTicketBigType())){//项目票
					
					String gate =  ticketType.getGate();
					int amount = ticketIndentDetail.getAmount();
					String gateName = ticketType.getGateText().replaceAll("闸口","票");
					
					int gaNum = gameNums.get("gate"+gate)==null?0:gameNums.get("gate"+gate);
					
					gaNum =  gaNum + amount;
					gameNums.put("gate"+gate, gaNum);
					gameGate.put("gate"+gate, gateName);
				}else{
					gateAmount = gateAmount +ticketIndentDetail.getAmount();
				}
			}
		}
		
		//20141203 限制散客每日购票不超过5张 start
		DetachedCriteria tiDc = DetachedCriteria.forClass(TicketIndent.class);
		tiDc.add(Property.forName("inputer").eq(sysUser.getUserId()));
		tiDc.add(Property.forName("useDate").eq(ticketIndent.getUseDate()));
		tiDc.add(Property.forName("status").in(new String[]{"02","03","04","05"}));
		//20150514 start
		tiDc.add(Property.forName("indentType").eq("01"));
		//20150514 end
		List<TicketIndent> tiList = ticketIndentDao.findByCriteria(tiDc);
		for(TicketIndent ti:tiList){
			gateAmount = gateAmount +ti.getTotalAmount();
		}
		if(gateAmount>5){
			throw new BusinessException("您购买的"+CommonMethod.timeToString(Timestamp.valueOf(ticketIndent.getUseDate().toString()))+"日的门票总数量已大于5张，不能购买！","");
		}
		//20141203 限制散客每日购票不超过5张 end
		String iTop = findSysParam("INDENT_TOPLIMIT");
		int inTop = 0;
		if(iTop!=null && iTop!=""){
			inTop = Integer.valueOf(iTop);
		}
		
		for(String key : gameNums.keySet()){
			int gaNum = gameNums.get(key);
			if(inTop>0 && gaNum > inTop){
				throw new BusinessException("您购买的"+gameGate.get(key)+"数量："+gaNum+"已大于单次购买项目票上限："+inTop,"");
			}
		}
			
		
		
		for (TicketIndentDetail ticketIndentDetail : coll) {//保存定单明细计算总数量总金额
			if(ticketIndentDetail.getAmount()!=null && ticketIndentDetail.getAmount()>0){
				ticketIndentDetail.setRemark(t.getRemark());
				ticketIndentDetail.setBuyType(t.getBuyType());
				TicketPrice ticketPrice=null;
				if(tpMap.size()>0){
					ticketPrice = tpMap.get("tp_"+ticketIndentDetail.getTicketPrice().getTicketPriceId());
				}else{
					ticketPrice = checkTopLimit(ticketIndent, ticketIndentDetail, "02", "", false, userId, roleId,true);
				}
				TicketType ticketType = ticketPrice.getTicketType();
				
				ticketIndentDetail.setIndentDetailId(""+getNextKey("Ticket_Indent_Detail", 1));
				ticketIndentDetail.setTicketIndent(ticketIndent);
				ticketIndentDetail.setUnitPrice(ticketPrice.getUnitPrice());
				
				ticketIndentDetail.setEntranceUnitPrice(ticketPrice.getEntranceUnitPrice());
				ticketIndentDetail.setRopewayUnitPrice(ticketPrice.getRopewayUnitPrice());
				ticketIndentDetail.setEntranceCompany(ticketPrice.getEntranceCompany());
				ticketIndentDetail.setRopewayCompany(ticketPrice.getRopewayCompany());
				
				ticketIndentDetail.setSubtotal(ticketIndentDetail.getAmount()*ticketPrice.getUnitPrice());
				ticketIndentDetail.setInputer(userId);
				ticketIndentDetail.setInputTime(ctime);
				ticketIndentDetail.setPerFee(perFee);
				ticketIndentDetail.setPayFee(ticketIndentDetail.getAmount()*ticketPrice.getUnitPrice()*ticketIndentDetail.getPerFee());
				
				
				int remainToplimit = ticketPrice.getRemainToplimit() == null ? 0 : ticketPrice.getRemainToplimit();
				remainToplimit = remainToplimit - ticketIndentDetail.getAmount();
				ticketPrice.setRemainToplimit(remainToplimit);
				totalAmount += ticketIndentDetail.getAmount();
				totalPrice += ticketIndentDetail.getAmount()*ticketPrice.getUnitPrice();
				ticketPriceDao.update(ticketPrice);
			}
		}
		
		//购买票时，设定订单类型   2014-02-21 tt
		setIndentType(ticketIndent,coll);
		if("02".equals(ticketIndent.getIndentType())
				&& "02".equals(ticketIndent.getCustomerType())
				&& !"05".equals(ticketIndent.getPayType()) 
				&& !"06".equals(ticketIndent.getPayType())){//20150624 首页购买观景索道票时设置remark tt
			ticketIndent.setRemark("home");
		}
		
		ticketIndent.setPayType("02");//网上支付 2014-02-21 tt
		ticketIndent.setStatus("01");//未支付
		ticketIndent.setTotalAmount(totalAmount);
		ticketIndent.setTotalPrice(totalPrice);
		ticketIndent.setPerFee(perFee);
		ticketIndent.setPayFee(totalPrice*perFee);
		ticketIndent.setSynchroState("00");//初始未同步状态
		
		//游玩当日门票上限check 2014-01-26 tt
		checkAmount(ticketIndent);
		
		if(ticketIndent.getIndentId()==null || ticketIndent.getIndentId().trim().isEmpty()){//无id时新增订单
			ticketIndent.setCheckCode(findCheckCode(ticketIndent));//验证码
			ticketIndent.setIndentCode(getNextIndentCode(CommonMethod.getCurrentYMD()+"000001",CommonMethod.getCurrentYMD()));
			ticketIndent.setIndentId(getNextKey("ticket_Indent", 1).toString());
			ticketIndent.setInputer(userId);
			ticketIndent.setInputTime(ctime);
			ticketIndentDao.save(ticketIndent);
		}
		else{//修改订单,首先删除所有订单明细
			ticketIndentDetailDao.deleteByIndentId(ticketIndent.getIndentId());
			ticketIndent.setUpdater(userId);
			ticketIndent.setUpdateTime(ctime);
			ticketIndentDao.update(ticketIndent);
		}
		for (TicketIndentDetail ticketIndentDetail : coll) {//保存完订单信息，再保存定单明细信息
			if(ticketIndentDetail.getIndentDetailId()!=null && !ticketIndentDetail.getIndentDetailId().isEmpty()){
				ticketIndentDetailDao.save(ticketIndentDetail);
			}
		}
	}
	
	@Override
	public void saveCheck(Collection<TicketIndentDetail> coll,TicketIndentDetail t) {
		TicketIndent ticketIndent = ticketIndentDao.findById(t.getTicketIndent().getIndentId());
		
		//将原订单明细保存到历史记录
		List<TicketIndentDetail> oldList = ticketIndentDetailDao.findByProperty("ticketIndent", ticketIndent);
		
		for(TicketIndentDetail tid : oldList){
			TicketDetailHis tdh = new TicketDetailHis();
			
			tdh.setDetailHisId(getNextKey("TICKET_DETAIL_HIS", 1));
			tdh.setIndentDetailId(tid.getIndentDetailId());
			tdh.setTicketIndent(tid.getTicketIndent());
			tdh.setTicketPrice(tid.getTicketPrice());
			tdh.setIndentDetailCode(tid.getIndentDetailCode());
			tdh.setUnitPrice(tid.getUnitPrice());
			tdh.setAmount(tid.getAmount());
			tdh.setPerFee(tid.getPerFee());
			tdh.setSubtotal(tid.getSubtotal());
			tdh.setRemark(tid.getRemark());
			tdh.setInputer(tid.getInputer());
			tdh.setInputTime(tid.getInputTime());
			tdh.setUpdater(tid.getUpdater());
			tdh.setUpdateTime(tid.getUpdateTime());
			tdh.setEntranceUnitPrice(tid.getEntranceUnitPrice());
			tdh.setEntranceCompany(tid.getEntranceCompany());
			tdh.setRopewayUnitPrice(tid.getRopewayUnitPrice());
			tid.setRopewayCompany(tid.getRopewayCompany());
			
			ticketDetailHisDao.save(tdh);
		}
		
		
		//删除订单明细
		ticketIndentDetailDao.deleteByIndentId(ticketIndent.getIndentId());
		
		//保存新的订单明细并计算合计
		int totalAmount = 0;
		double totalPrice = 0;
		for (TicketIndentDetail ticketIndentDetail : coll) {//保存定单明细计算总数量总金额
			if(ticketIndentDetail.getAmount()!=null && ticketIndentDetail.getAmount()>0){
				TicketPrice ticketPrice;
				
				DetachedCriteria dc = DetachedCriteria.forClass(TicketPrice.class);
				dc.createAlias("ticketType", "ticketType");
				dc.add(Property.forName("startDate").le(CommonMethod.toEndTime(Timestamp.valueOf(ticketIndent.getUseDate().toString()))));//开始时间<=当前时间
				dc.add(Property.forName("endDate").ge(CommonMethod.toStartTime(Timestamp.valueOf(ticketIndent.getUseDate().toString()))));//结束时间>=当前时间
				dc.add(Property.forName("status").eq("11"));
				dc.add(Property.forName("ticketType.status").eq("11"));
				dc.add(Property.forName("ticketType.ticketId").eq(ticketIndentDetail.getTicketPrice().getTicketType().getTicketId()));
				//dc.addOrder(Order.asc("unitPrice"));
				List<TicketPrice> lsitTP = ticketPriceDao.findByCriteria(dc);
				double perFee = Double.parseDouble(findSysParam("PER_FEE"));
				if(lsitTP!=null && lsitTP.size()>0){
					ticketPrice = lsitTP.get(0);
					
					ticketIndentDetail.setIndentDetailId("c"+getNextKey("Ticket_Indent_Detail", 1));
					ticketIndentDetail.setTicketIndent(ticketIndent);
					ticketIndentDetail.setTicketPrice(ticketPrice);
					ticketIndentDetail.setUnitPrice(ticketPrice.getUnitPrice());
					ticketIndentDetail.setSubtotal(ticketIndentDetail.getAmount()*ticketPrice.getUnitPrice());
					ticketIndentDetail.setInputer(ticketIndent.getInputer());
					ticketIndentDetail.setInputTime(ticketIndent.getInputTime());
					ticketIndentDetail.setPerFee(perFee);
					ticketIndentDetail.setPayFee(ticketIndentDetail.getAmount()*ticketPrice.getUnitPrice()*ticketIndentDetail.getPerFee());
					
					totalAmount += ticketIndentDetail.getAmount();
					totalPrice += ticketIndentDetail.getAmount()*ticketPrice.getUnitPrice();
				}
				
			}
		}
		
		//修改订单状态
		double totalPriceOld = ticketIndent.getTotalPrice() - totalPrice;
		ticketIndent.setStatus("03");//未支付
		ticketIndent.setTotalAmount(totalAmount);
		ticketIndent.setTotalPrice(totalPrice);
		
		ticketIndent.setUpdater(getCurrentUserId());
		ticketIndent.setUpdateTime(CommonMethod.getTimeStamp());
		ticketIndentDao.update(ticketIndent);
		
		for (TicketIndentDetail ticketIndentDetail : coll) {//保存完订单信息，再保存定单明细信息
			if(ticketIndentDetail.getIndentDetailId()!=null && !ticketIndentDetail.getIndentDetailId().isEmpty()){
				ticketIndentDetailDao.save(ticketIndentDetail);
			}
		}
		//修改旅行社余额
		SysUser sysUser = sysUserDao.findById(ticketIndent.getInputer());
		Travservice baseTravelAgency = baseTravelAgencyDao.findById(sysUser.getBaseTravelAgency().getTravelAgencyId());
		
		Double cshB = baseTravelAgency.getCashBalance();
		double cashBal = 0d;
		if(cshB!=null){
			cashBal = Double.valueOf(cshB);
		}
		
		baseTravelAgency.setCashBalance(cashBal+totalPriceOld);
		baseTravelAgency.setUpdater(getCurrentUserId());
		baseTravelAgency.setUpdateTime(CommonMethod.getTimeStamp());
		
		baseTravelAgencyDao.update(baseTravelAgency);
		
		//添加交易记录
		BaseTranRec baseTranRec = new BaseTranRec();
		baseTranRec.setTicketIndent(ticketIndent);
		baseTranRec.setMoney((0-totalPriceOld));
		baseTranRec.setPaymentType("02");//支付类型:支付
		baseTranRec.setPayType("02");//交易类型,通过余额交易
		baseTranRec.setPaymentTime(CommonMethod.getTimeStamp());
		baseTranRecService.save(baseTranRec);
		
		//添加充值记录
		BaseRechargeRec baseRechargeRec = new BaseRechargeRec();
		
		baseRechargeRec.setRechargeRecId(getNextKey("BASE_RECHARGE_REC", 1));
		baseRechargeRec.setBaseTravelAgency(baseTravelAgency);
		baseRechargeRec.setTicketIndent(ticketIndent);
		baseRechargeRec.setSourceDescription("团客审核退款");
		baseRechargeRec.setCashMoney(totalPriceOld);
		baseRechargeRec.setCashBalance(baseTravelAgency.getCashBalance());
		baseRechargeRec.setPaymentType("02");
		baseRechargeRec.setInputer(getCurrentUserId());
		baseRechargeRec.setInputTime(CommonMethod.getTimeStamp());
		baseRechargeRecService.save(baseRechargeRec);
		
		
	}
	
	
	/**
	 * 省内旅行社团购订单和订单明细保存（项目票）
	 */
	@Override
	public void saveGames(Collection<TicketIndentDetail> coll,TicketIndentDetail t) {
		TicketIndent ticketIndent = t.getTicketIndent();
		int totalAmount = 0;
		int gateAmount = 0;
		double totalPrice = 0;
		double perFee = Double.parseDouble(findSysParam("PER_FEE"));
		
		Timestamp ctime = CommonMethod.getTimeStamp();
		if(ticketIndent.getUseDate().before(CommonMethod.toStartTime(CommonMethod.getTimeStamp()))){
			throw new BusinessException("游玩日期已过，无法购票!","");
		}
		//身份证和手机验证
		if((ticketIndent.getIdCardNumber()==null || ticketIndent.getIdCardNumber().isEmpty())){
			throw new BusinessException("取票人身份证必须填写，否则无法取票！","");
		}
		
		String BUY_TIME = sysParamService.findSysParam("BUY_TIME");
		String timeStr = CommonMethod.timeToString(Timestamp.valueOf(ticketIndent.getUseDate().toString()))+" "+BUY_TIME+":00";
		
		Timestamp endTime = CommonMethod.string2Time1(timeStr);
		
		if(endTime.before(ctime)){
			String BUY_TIME_CHECK = sysParamService.findSysParam("BUY_TIME_CHECK");
			if("true".equals(BUY_TIME_CHECK)){
				throw new BusinessException("超过"+BUY_TIME+"，交通索道已关闭，无法购票!","");
			}
		}
		
//		//重复购买检测，每个身份证一个月限定二张
//		DetachedCriteria dcI = DetachedCriteria.forClass(TicketIndent.class);
//		dcI.add(Property.forName("idCardNumber").eq(ticketIndent.getIdCardNumber()));
//		dcI.add(Property.forName("useDate").le(ticketIndent.getUseDate()));
//		dcI.add(Property.forName("useDate").ge(CommonMethod.string2Time1(CommonMethod.addMonth(ticketIndent.getUseDate().toString(), -1))));
//		dcI.add(Property.forName("indentType").eq("05"));
//		dcI.add(Property.forName("status").in(new String[]{"01","02","03","04","05","08"}));
//		List<TicketIndent> tisI = ticketIndentDao.findByCriteria(dcI);
//		if(tisI!=null && tisI.size()>1){
//			throw new BusinessException("身份证："+ticketIndent.getIdCardNumber()+"在一个月内已购买过2次项目票，不能再次购买！","");
//		}
		
		if("01".equals(ticketIndent.getCustomerType()) && !"1".equals(t.getBuyType())){//团客订单判断是否需要新增导游
			ticketIndent.setLinkman(ticketIndent.getLinkman().trim());
			boolean isHave = false;
			BaseTourGuide btg = null;
			if(CommonMethod.isNumber(ticketIndent.getLinkman())){//是不是整数或文字
				btg = baseTourGuideDao.findById(Long.parseLong(ticketIndent.getLinkman()));
			}else{
				DetachedCriteria dcBTU = DetachedCriteria.forClass(BaseTourGuide.class);
				dcBTU.add(Property.forName("name").eq(ticketIndent.getLinkman()));
				dcBTU.add(Property.forName("phoneNumber").eq(ticketIndent.getPhoneNumber()));
				dcBTU.add(Property.forName("idCardNumber").eq(ticketIndent.getIdCardNumber()));
				dcBTU.add(Property.forName("guideNumber").eq(ticketIndent.getGuideNumber()));
				
				List<BaseTourGuide> listSU = baseTourGuideDao.findByCriteria(dcBTU);;
				for(BaseTourGuide sr : listSU){
					btg = sr;
				}
			}
			
			if(btg!=null){
				isHave = true;
			}
			
			if(isHave){
				ticketIndent.setLinkman(btg.getName());
			}
			else{//新增导游
				btg = new BaseTourGuide();
				SysUser sysUser = getSysUser();
//				BaseTravelAgency baseTravelAgency = baseTravelAgencyDao.findById(sysUser.getBaseTravelAgency().getTravelAgencyId());
				
				btg.setTourGuideId(getNextKey("Base_Tour_Guide", 1));
				btg.setName(ticketIndent.getLinkman());
				btg.setPhoneNumber(ticketIndent.getPhoneNumber());
				btg.setIdCardNumber(ticketIndent.getIdCardNumber());
				btg.setGuideNumber(ticketIndent.getGuideNumber());
				btg.setBaseTravelAgency(sysUser.getBaseTravelAgency());
				baseTourGuideDao.save(btg);
			}
		}
		
		boolean updateRemain = true;
		Long userId=null;
		if(ticketIndent.getIndentId()==null || ticketIndent.getIndentId().trim().isEmpty()){
			userId = getCurrentUserId();
		}else{
			userId = ticketIndent.getInputer();
			updateRemain = false;
		}
		Long roleId = 0L;
		if(userId==null){
			userId = 0L;
			DetachedCriteria dcSU = DetachedCriteria.forClass(SysRole.class);
			dcSU.add(Property.forName("roleCode").eq("003"));
			List<SysRole> listSR = sysRoleDao.findByCriteria(dcSU);
			for(SysRole sr : listSR){
				roleId = sr.getRoleId();
			}
			
		}else{
			DetachedCriteria dcSU = DetachedCriteria.forClass(SysUserRole.class);
			dcSU.add(Property.forName("sysUser.userId").eq(userId));
			List<SysUserRole> listSU = sysUserRoleDao.findByCriteria(dcSU);
			for(SysUserRole sr : listSU){
				roleId = sr.getSysRole().getRoleId();
			}
		}
		
		String isOutbound = t.getTicketIndent().getIsOutbound();//'11'是境外团
		
		
		
		boolean isAgr = sysUserDao.findIsAgreementByUseId(userId);
		
		Map<String,Integer> gameNums = new HashMap<String,Integer>();
		Map<String,String> gameGate = new HashMap<String,String>();
		Map<String,TicketPrice> tpMap = new HashMap<String,TicketPrice>();
		if("02".equals(ticketIndent.getCustomerType())){//散客需判断购买数量
			for (TicketIndentDetail ticketIndentDetail : coll) {
				if(ticketIndentDetail.getAmount()!=null && ticketIndentDetail.getAmount()>0){
					ticketIndentDetail.setRemark(t.getRemark());
					ticketIndentDetail.setBuyType(t.getBuyType());
					TicketPrice ticketPrice = checkTopLimit(ticketIndent, ticketIndentDetail, ticketIndent.getCustomerType(), isOutbound, isAgr, userId, roleId, updateRemain);
					tpMap.put("tp_"+ticketPrice.getTicketPriceId(), ticketPrice);
					TicketType ticketType = ticketPrice.getTicketType();
					if("02".equals(ticketType.getTicketBigType())){//项目票
						
						String gate =  ticketType.getGate();
						int amount = ticketIndentDetail.getAmount();
						String gateName = ticketType.getGateText().replaceAll("闸口","票");
						
						int gaNum = gameNums.get("gate"+gate)==null?0:gameNums.get("gate"+gate);
						
						gaNum =  gaNum + amount;
						gameNums.put("gate"+gate, gaNum);
						gameGate.put("gate"+gate, gateName);
					}else{
						gateAmount = gateAmount +ticketIndentDetail.getAmount();
					}
				}
			}
			
			String iTop = findSysParam("GAME_TOPLIMIT");
			int inTop = 0;
			if(iTop!=null && iTop!=""){
				inTop = Integer.valueOf(iTop);
			}
			
			for(String key : gameNums.keySet()){
				int gaNum = gameNums.get(key);
				if(inTop>0 && gaNum > inTop){
					throw new BusinessException("您购买的"+gameGate.get(key)+"数量："+gaNum+"已大于单次购买项目票套票的上限："+inTop,"");
				}
			}
			
			//20141203 限制散客每日购票不超过5张 start
			if(!"05".equals(ticketIndent.getPayType()) && !"06".equals(ticketIndent.getPayType())){
				DetachedCriteria tiDc = DetachedCriteria.forClass(TicketIndent.class);
				tiDc.add(Property.forName("inputer").eq(getCurrentUserId()));
				tiDc.add(Property.forName("useDate").eq(ticketIndent.getUseDate()));
				tiDc.add(Property.forName("status").in(new String[]{"02","03","04","05"}));
				//20150514 start
				tiDc.add(Property.forName("indentType").eq("01"));
				//20150514 end
				List<TicketIndent> tiList = ticketIndentDao.findByCriteria(tiDc);
				for(TicketIndent ti:tiList){
					gateAmount = gateAmount +ti.getTotalAmount();
				}
				if(gateAmount>5){
					throw new BusinessException("您购买的"+CommonMethod.timeToString(Timestamp.valueOf(ticketIndent.getUseDate().toString()))+"日的门票总数量已大于5张，不能购买！","");
				}
			}
			//20141203 限制散客每日购票不超过5张 end
		}
		
		for (TicketIndentDetail ticketIndentDetail : coll) {//保存定单明细计算总数量总金额
			if(ticketIndentDetail.getAmount()!=null && ticketIndentDetail.getAmount()>0){
				ticketIndentDetail.setRemark(t.getRemark());
				ticketIndentDetail.setBuyType(t.getBuyType());
				TicketPrice ticketPrice=null;
				
				if(tpMap.size()>0){
					ticketPrice = tpMap.get("tp_"+ticketIndentDetail.getTicketPrice().getTicketPriceId());
				}else{
					ticketPrice = checkTopLimit(ticketIndent, ticketIndentDetail, ticketIndent.getCustomerType(), isOutbound, isAgr, userId, roleId, updateRemain);
				}
				
				ticketIndentDetail.setTicketPrice(ticketPrice);
				TicketType ticketType = ticketPrice.getTicketType();
				
				checkActivity(ticketType,ticketIndentDetail,t, ticketIndent);
				ticketIndentDetail.setIndentDetailId(""+getNextKey("Ticket_Indent_Detail", 1));
				ticketIndentDetail.setTicketIndent(ticketIndent);
				//境外团购票和协议旅行社计算价格为打印价格
				if("11".equals(isOutbound) || sysUserDao.findIsAgreementByUseId(userId)){//lzp 2013-11-10 合并购票核心部分
					ticketIndentDetail.setUnitPrice(ticketPrice.getPrintPrice());
					ticketIndentDetail.setEntranceUnitPrice(ticketPrice.getEntrancePrice());
					ticketIndentDetail.setRopewayUnitPrice(ticketPrice.getRopewayPrice());
				}else{
					ticketIndentDetail.setUnitPrice(ticketPrice.getUnitPrice());
					ticketIndentDetail.setEntranceUnitPrice(ticketPrice.getEntranceUnitPrice());
					ticketIndentDetail.setRopewayUnitPrice(ticketPrice.getRopewayUnitPrice());
				}
				
				ticketIndentDetail.setEntranceCompany(ticketPrice.getEntranceCompany());
				ticketIndentDetail.setRopewayCompany(ticketPrice.getRopewayCompany());
				
				ticketIndentDetail.setSubtotal(ticketIndentDetail.getAmount()*ticketIndentDetail.getUnitPrice());
				ticketIndentDetail.setInputer(getCurrentUserId());
				ticketIndentDetail.setInputTime(ctime);
				ticketIndentDetail.setPerFee(perFee);
				ticketIndentDetail.setPayFee(ticketIndentDetail.getAmount()*ticketIndentDetail.getUnitPrice()*ticketIndentDetail.getPerFee());
				
				if("02".equals(ticketType.getTicketBigType())){//项目票
					
					int remainToplimit = ticketPrice.getRemainToplimit() == null ? 0 : ticketPrice.getRemainToplimit();
					remainToplimit = remainToplimit - ticketIndentDetail.getAmount();
					if(remainToplimit<0){
						throw new BusinessException("您选择的项目剩余数量不足！","");
					}
					ticketPrice.setRemainToplimit(remainToplimit);
					
				}
				
				totalAmount += ticketIndentDetail.getAmount();
				totalPrice += ticketIndentDetail.getAmount()*ticketIndentDetail.getUnitPrice();
				ticketPriceDao.update(ticketPrice);
			}
		}
		
		//购买票时，设定订单类型   2015-05-27 tt
//		setIndentType(ticketIndent,coll);
		ticketIndent.setIndentType("05");
		
		if("05".equals(ticketIndent.getPayType()) || "06".equals(ticketIndent.getPayType())){//微信或微博订单
			ticketIndent.setStatus("03");
		}else if(!"21".equals(ticketIndent.getStatus())){//协议旅行社保存订单
			ticketIndent.setStatus("01");//未支付
		}
		ticketIndent.setTotalAmount(totalAmount);
		ticketIndent.setTotalPrice(totalPrice);
		ticketIndent.setPerFee(perFee);
		ticketIndent.setPayFee(totalPrice*perFee);
		ticketIndent.setSynchroState("00");//初始未同步状态
		ticketIndent.setIsOutbound(isOutbound);//非境外订单
		if("1".equals(t.getBuyType())){
			ticketIndent.setTelNumber("1");
		}
		//游玩当日门票上限check 2014-01-26 tt
//		checkAmount(ticketIndent);//2015-05-27 tt
//		if(totalAmount>1){
//			throw new BusinessException("单次购买项目套票数量不能超过1张！","");
//		}
		
		if(t.getTicketIndent().getIsOutbound()!=null && t.getTicketIndent().getIsOutbound().trim().equals("11")){
			ticketIndent.setStatus("21");//未支付
		}
		if(ticketIndent.getIndentId()==null || ticketIndent.getIndentId().trim().isEmpty()){//无id时新增订单
			ticketIndent.setCheckCode(findCheckCode(ticketIndent));//验证码
			ticketIndent.setIndentCode(getNextIndentCode(CommonMethod.getCurrentYMD()+"000001",CommonMethod.getCurrentYMD()));
			ticketIndent.setIndentId(getNextKey("ticket_Indent", 1).toString());
			ticketIndent.setInputer(getCurrentUserId());
			ticketIndent.setInputTime(ctime);
			ticketIndentDao.save(ticketIndent);
		}
		else{//修改订单,首先删除所有订单明细
			ticketIndentDetailDao.deleteByIndentId(ticketIndent.getIndentId());
			ticketIndent.setUpdater(getCurrentUserId());
			ticketIndent.setUpdateTime(ctime);
			ticketIndentDao.update(ticketIndent);
		}
		for (TicketIndentDetail ticketIndentDetail : coll) {//保存完订单信息，再保存定单明细信息
			if(ticketIndentDetail.getIndentDetailId()!=null && !ticketIndentDetail.getIndentDetailId().isEmpty()){
				ticketIndentDetailDao.save(ticketIndentDetail);
			}
		}
	}
	
	/**
	 * 保存订单和订单明细（首页购买项目票）
	 * @param coll
	 * @param t
	 */
	@Override
	public void saveHomeGames(Collection<TicketIndentDetail> coll,TicketIndentDetail t) {
		TicketIndent ticketIndent = t.getTicketIndent();
		int totalAmount = 0;
		int gateAmount = 0;
		double totalPrice = 0;
		double perFee = Double.parseDouble(findSysParam("PER_FEE"));
		Timestamp ctime = CommonMethod.getTimeStamp();
		
		if(ticketIndent.getUseDate().before(CommonMethod.toStartTime(CommonMethod.getTimeStamp()))){
			throw new BusinessException("游玩日期已过，无法购票!","");
		}
		
		//身份证和手机验证
		if((ticketIndent.getIdCardNumber()==null || ticketIndent.getIdCardNumber().isEmpty())){
			throw new BusinessException("取票人身份证必须填写，否则无法取票！","");
		}
		
		String BUY_TIME = sysParamService.findSysParam("BUY_TIME");
		String timeStr = CommonMethod.timeToString(Timestamp.valueOf(ticketIndent.getUseDate().toString()))+" "+BUY_TIME+":00";
		
		Timestamp endTime = CommonMethod.string2Time1(timeStr);
		
		if(endTime.before(ctime)){
			String BUY_TIME_CHECK = sysParamService.findSysParam("BUY_TIME_CHECK");
			if("true".equals(BUY_TIME_CHECK)){
				throw new BusinessException("超过"+BUY_TIME+"，交通索道已关闭，无法购票!","");
			}
		}
		
		//重复购买检测，每个身份证一个月限定二张
		DetachedCriteria dcI = DetachedCriteria.forClass(TicketIndent.class);
		dcI.add(Property.forName("idCardNumber").eq(ticketIndent.getIdCardNumber()));
		dcI.add(Property.forName("useDate").le(ticketIndent.getUseDate()));
		dcI.add(Property.forName("useDate").ge(CommonMethod.string2Time1(CommonMethod.addMonth(ticketIndent.getUseDate().toString(), -1))));
		dcI.add(Property.forName("indentType").eq("05"));
		dcI.add(Property.forName("status").in(new String[]{"01","02","03","04","05","08"}));
		List<TicketIndent> tisI = ticketIndentDao.findByCriteria(dcI);
		if(tisI!=null && tisI.size()>1){
			throw new BusinessException("身份证："+ticketIndent.getIdCardNumber()+"在一个月内已购买过2次项目票，不能再次购买！","");
		}
		
		String msg = "";
		
		String phoneNumber = ticketIndent.getPhoneNumber();
		
		DetachedCriteria dc = DetachedCriteria.forClass(SysUser.class);
		dc.add(Property.forName("phoneNumber").eq(phoneNumber.trim()));
		List<SysUser> suList = sysUserDao.findByCriteria(dc);
		SysUser sysUser;
		if(suList!=null && suList.size()>0){
			sysUser = suList.get(0); 
			
			if("01".equals(sysUser.getUserType())){//员工
				throw new BusinessException("您的手机号已注册成为员工，不能在首页购票!","");
			}else if("03".equals(sysUser.getUserType())){//旅行社
				throw new BusinessException("您的手机号已注册成为旅行社账号，不能在首页购票!","");
			}
			
			msg = "感谢您使用西岭雪山订票系统，您已有西岭雪山电子商务平台账户！登录后可自行退订、取消订单，账户号为："+
				sysUser.getUserCode()+"，登录的网址为：http://www.xilingxueshan.cn/booking。";
		}else{//添加新账户
			sysUser = new SysUser();
			
			String uCode = ticketIndent.getPhoneNumber();
			if(ticketIndent.getPhoneNumber()==null || ticketIndent.getPhoneNumber().isEmpty()){
				uCode = baseTouristDao.getNextUserCode();
			}
			sysUser.setUserCode(uCode);
			long captcha =  Math.round(Math.random()*(999999-100000)+100000);
			
			sysUser.setPassword(MD5.getMD5(String.valueOf(captcha)));
			sysUser.setUserType("02");
			sysUser.setStatus("11");
			sysUser.setUserName(ticketIndent.getLinkman());
			sysUser.setPhoneNumber(ticketIndent.getPhoneNumber());
			sysUser.setUserId(getNextKey("SYS_USER".toUpperCase(), 1));
			SysUserRole sysUserRole = new SysUserRole();
			sysUserRole.setSysUser(sysUser);
			
			
			List<SysRole> srList = sysRoleDao.findByProperty("roleCode", findSysParam("ROLE_TOURIST_CODE"));
			SysRole sysRole = srList.get(0);
			sysUserRole.setSysRole(sysRole);
			sysUserRole.setUserRoleId(getNextKey("SYS_USER_ROLE".toUpperCase(), 1));
			
			BaseTourist bt = new BaseTourist();
			
			bt.setTouristId(getNextKey("BASE_TOURIST".toUpperCase(), 1));
			bt.setTouristName(ticketIndent.getLinkman());
			bt.setPhoneNumber(ticketIndent.getPhoneNumber());
			bt.setIdCardNumber(ticketIndent.getIdCardNumber());
			bt.setInputTime(CommonMethod.getTimeStamp());
			sysUser.setBaseTourist(bt);
			
			baseTouristDao.save(bt);
			sysUserDao.save(sysUser);
			sysUserRoleDao.save(sysUserRole);
			
			msg = "感谢您使用西岭雪山订票系统，我们为您注册了西岭雪山电子商务平台账户！登录后可自行退订、取消订单，<font color='red' style='font: 14px/35px \"宋体\";font-weight: bold;'>账户号为："+
				sysUser.getUserCode()+"，初始密码为："+
				captcha+"，建议尽快修改密码</font>，登录的网址为：<font color='red' style='font: 14px/35px \"宋体\";font-weight: bold;'>http://www.xilingxueshan.cn/booking</font>。";
			
			if(!(ticketIndent.getPhoneNumber()==null || ticketIndent.getPhoneNumber().isEmpty())){
				CommonMethod.sendSms(sysUser.getPhoneNumber(), "感谢您使用西岭雪山订票系统，我们为您注册了西岭雪山电子商务平台账户！登录后可自行退订、取消订单，账户号为："+
						sysUser.getUserCode()+"，初始密码为："+
						captcha+"，建议尽快修改密码，登录的网址为：http://www.xilingxueshan.cn/booking。",sysParamService.findSysParam("USE_NEW_PLATFROM"));
			}
			
		}
		ticketIndent.setMsg(msg);
		//ActionContext.getContext().getSession().put("userId", sysUser.getUserId());
		
		Long userId = sysUser.getUserId();
		
		Long roleId = 0L;
		if(userId==null){
			userId = 0L;
			DetachedCriteria dcSU = DetachedCriteria.forClass(SysRole.class);
			dcSU.add(Property.forName("roleCode").eq("003"));
			List<SysRole> listSR = sysRoleDao.findByCriteria(dcSU);
			for(SysRole sr : listSR){
				roleId = sr.getRoleId();
			}
			
		}else{
			DetachedCriteria dcSU = DetachedCriteria.forClass(SysUserRole.class);
			dcSU.add(Property.forName("sysUser.userId").eq(userId));
			List<SysUserRole> listSU = sysUserRoleDao.findByCriteria(dcSU);
			for(SysUserRole sr : listSU){
				roleId = sr.getSysRole().getRoleId();
			}
		}
		
		Map<String,Integer> gameNums = new HashMap<String,Integer>();
		Map<String,String> gameGate = new HashMap<String,String>();
		Map<String,TicketPrice> tpMap = new HashMap<String,TicketPrice>();
		//散客需判断购买数量
		for (TicketIndentDetail ticketIndentDetail : coll) {
			if(ticketIndentDetail.getAmount()!=null && ticketIndentDetail.getAmount()>0){
				ticketIndentDetail.setRemark(t.getRemark());
				ticketIndentDetail.setBuyType(t.getBuyType());
				TicketPrice ticketPrice = checkTopLimit(ticketIndent, ticketIndentDetail, "02", "", false, userId, roleId,true);
				tpMap.put("tp_"+ticketPrice.getTicketPriceId(), ticketPrice);
				TicketType ticketType = ticketPrice.getTicketType();
				if("02".equals(ticketType.getTicketBigType())){//项目票
					
					String gate =  ticketType.getGate();
					int amount = ticketIndentDetail.getAmount();
					String gateName = ticketType.getGateText().replaceAll("闸口","票");
					
					int gaNum = gameNums.get("gate"+gate)==null?0:gameNums.get("gate"+gate);
					
					gaNum =  gaNum + amount;
					gameNums.put("gate"+gate, gaNum);
					gameGate.put("gate"+gate, gateName);
				}else{
					gateAmount = gateAmount +ticketIndentDetail.getAmount();
				}
			}
		}
		
		//20141203 限制散客每日购票不超过5张 start
		DetachedCriteria tiDc = DetachedCriteria.forClass(TicketIndent.class);
		tiDc.add(Property.forName("inputer").eq(sysUser.getUserId()));
		tiDc.add(Property.forName("useDate").eq(ticketIndent.getUseDate()));
		tiDc.add(Property.forName("status").in(new String[]{"02","03","04","05"}));
		//20150514 start
		tiDc.add(Property.forName("indentType").eq("01"));
		//20150514 end
		List<TicketIndent> tiList = ticketIndentDao.findByCriteria(tiDc);
		for(TicketIndent ti:tiList){
			gateAmount = gateAmount +ti.getTotalAmount();
		}
		if(gateAmount>5){
			throw new BusinessException("您购买的"+CommonMethod.timeToString(Timestamp.valueOf(ticketIndent.getUseDate().toString()))+"日的门票总数量已大于5张，不能购买！","");
		}
		//20141203 限制散客每日购票不超过5张 end
		String iTop = findSysParam("GAME_TOPLIMIT");
		int inTop = 0;
		if(iTop!=null && iTop!=""){
			inTop = Integer.valueOf(iTop);
		}
		
		for(String key : gameNums.keySet()){
			int gaNum = gameNums.get(key);
			if(inTop>0 && gaNum > inTop){
				throw new BusinessException("您购买的"+gameGate.get(key)+"数量："+gaNum+"已大于单次购买项目票上限："+inTop,"");
			}
		}
		
		for (TicketIndentDetail ticketIndentDetail : coll) {//保存定单明细计算总数量总金额
			if(ticketIndentDetail.getAmount()!=null && ticketIndentDetail.getAmount()>0){
				ticketIndentDetail.setRemark(t.getRemark());
				ticketIndentDetail.setBuyType(t.getBuyType());
				TicketPrice ticketPrice=null;
				if(tpMap.size()>0){
					ticketPrice = tpMap.get("tp_"+ticketIndentDetail.getTicketPrice().getTicketPriceId());
				}else{
					ticketPrice = checkTopLimit(ticketIndent, ticketIndentDetail, "02", "", false, userId, roleId,true);
				}
				TicketType ticketType = ticketPrice.getTicketType();
				
				ticketIndentDetail.setIndentDetailId(""+getNextKey("Ticket_Indent_Detail", 1));
				ticketIndentDetail.setTicketIndent(ticketIndent);
				ticketIndentDetail.setUnitPrice(ticketPrice.getUnitPrice());
				
				ticketIndentDetail.setEntranceUnitPrice(ticketPrice.getEntranceUnitPrice());
				ticketIndentDetail.setRopewayUnitPrice(ticketPrice.getRopewayUnitPrice());
				ticketIndentDetail.setEntranceCompany(ticketPrice.getEntranceCompany());
				ticketIndentDetail.setRopewayCompany(ticketPrice.getRopewayCompany());
				
				ticketIndentDetail.setSubtotal(ticketIndentDetail.getAmount()*ticketPrice.getUnitPrice());
				ticketIndentDetail.setInputer(userId);
				ticketIndentDetail.setInputTime(ctime);
				ticketIndentDetail.setPerFee(perFee);
				ticketIndentDetail.setPayFee(ticketIndentDetail.getAmount()*ticketPrice.getUnitPrice()*ticketIndentDetail.getPerFee());
				
				
				int remainToplimit = ticketPrice.getRemainToplimit() == null ? 0 : ticketPrice.getRemainToplimit();
				remainToplimit = remainToplimit - ticketIndentDetail.getAmount();
				ticketPrice.setRemainToplimit(remainToplimit);
				totalAmount += ticketIndentDetail.getAmount();
				totalPrice += ticketIndentDetail.getAmount()*ticketPrice.getUnitPrice();
				ticketPriceDao.update(ticketPrice);
			}
		}
		
		//购买票时，设定订单类型   2014-02-21 tt
//		setIndentType(ticketIndent,coll);
		ticketIndent.setIndentType("05");//2015-05-27 tt
		ticketIndent.setRemark("home");//2015-06-24 tt
		ticketIndent.setPayType("02");//网上支付 2014-02-21 tt
		ticketIndent.setStatus("01");//未支付
		ticketIndent.setTotalAmount(totalAmount);
		ticketIndent.setTotalPrice(totalPrice);
		ticketIndent.setPerFee(perFee);
		ticketIndent.setPayFee(totalPrice*perFee);
		ticketIndent.setSynchroState("00");//初始未同步状态
		
		//游玩当日门票上限check 2014-01-26 tt
//		checkAmount(ticketIndent);//2015-05-27 tt
		if(totalAmount>1){
			throw new BusinessException("单次购买项目套票数量不能超过1张！","");
		}
		
		if(ticketIndent.getIndentId()==null || ticketIndent.getIndentId().trim().isEmpty()){//无id时新增订单
			ticketIndent.setCheckCode(findCheckCode(ticketIndent));//验证码
			ticketIndent.setIndentCode(getNextIndentCode(CommonMethod.getCurrentYMD()+"000001",CommonMethod.getCurrentYMD()));
			ticketIndent.setIndentId(getNextKey("ticket_Indent", 1).toString());
			ticketIndent.setInputer(userId);
			ticketIndent.setInputTime(ctime);
			ticketIndentDao.save(ticketIndent);
		}
		else{//修改订单,首先删除所有订单明细
			ticketIndentDetailDao.deleteByIndentId(ticketIndent.getIndentId());
			ticketIndent.setUpdater(userId);
			ticketIndent.setUpdateTime(ctime);
			ticketIndentDao.update(ticketIndent);
		}
		for (TicketIndentDetail ticketIndentDetail : coll) {//保存完订单信息，再保存定单明细信息
			if(ticketIndentDetail.getIndentDetailId()!=null && !ticketIndentDetail.getIndentDetailId().isEmpty()){
				ticketIndentDetailDao.save(ticketIndentDetail);
			}
		}
	}
	
	/**
	 * 观景索道活动票订单和订单明细保存
	 */
	@Override
	public void saveAcProject(Collection<TicketIndentDetail> coll,TicketIndentDetail t) {
		TicketIndent ticketIndent = t.getTicketIndent();
		int totalAmount = 0;
		int cdLimitAmount=10;//成都
		int dyLimitAmount=10;//德阳
		int myLimitAmount=10;//绵阳
		int snLimitAmount=2;//遂宁
		int ncLimitAmount=10;//南充
		int zyLimitAmount=10;//资阳
		int jyLimitAmount=10;//简阳
//		int gateAmount = 0;
		double totalPrice = 0;
		double perFee = Double.parseDouble(findSysParam("PER_FEE"));
		
		Timestamp ctime = CommonMethod.getTimeStamp();
		if(ticketIndent.getUseDate().before(CommonMethod.toStartTime(CommonMethod.getTimeStamp()))){
			throw new BusinessException("游玩日期已过，无法购票!","");
		}
		//身份证和手机验证
		if(ticketIndent.getIdCardNumber()==null || ticketIndent.getIdCardNumber().isEmpty()
			|| ticketIndent.getPhoneNumber()==null || ticketIndent.getPhoneNumber().isEmpty()){
			throw new BusinessException("取票人身份证和取票人手机号必须填写，否则无法取票！","");
		}
		
		String BUY_TIME = sysParamService.findSysParam("BUY_TIME");
		String timeStr = CommonMethod.timeToString(Timestamp.valueOf(ticketIndent.getUseDate().toString()))+" "+BUY_TIME+":00";
		
		Timestamp endTime = CommonMethod.string2Time1(timeStr);
		
		if(endTime.before(ctime)){
			String BUY_TIME_CHECK = sysParamService.findSysParam("BUY_TIME_CHECK");
			if("true".equals(BUY_TIME_CHECK)){
				throw new BusinessException("超过"+BUY_TIME+"，交通索道已关闭，无法购票!","");
			}
		}
		
		//重复购买检测，每个身份证只能购买一张日月坪观景索道活动票
		DetachedCriteria dcI = DetachedCriteria.forClass(TicketIndentDetail.class);
		dcI.createAlias("ticketIndent", "ticketIndent");
		dcI.createAlias("ticketPrice", "ticketPrice");
		dcI.createAlias("ticketPrice.ticketType", "ticketType");
		dcI.add(Property.forName("ticketIndent.idCardNumber").eq(ticketIndent.getIdCardNumber()));
		dcI.add(Property.forName("ticketIndent.status").in(new String[]{"01","02","03","04","05","08"}));
		dcI.add(Property.forName("ticketType.ticketCode").eq("05"));
		dcI.add(Property.forName("ticketType.specialType").eq("07"));
		dcI.add(Property.forName("ticketType.ticketBigType").eq("02"));
		List<TicketIndentDetail> tisI = ticketIndentDetailDao.findByCriteria(dcI);
		if(tisI!=null && tisI.size()>0){
			throw new BusinessException("身份证："+ticketIndent.getIdCardNumber()+"已购买过日月坪观景索道活动票，不能重复购买！","");
		}
		
		//重复购买检测，每个手机只能购买一张日月坪观景索道活动票
		DetachedCriteria dcP = DetachedCriteria.forClass(TicketIndentDetail.class);
		dcP.createAlias("ticketIndent", "ticketIndent");
		dcP.createAlias("ticketPrice", "ticketPrice");
		dcP.createAlias("ticketPrice.ticketType", "ticketType");
		dcP.add(Property.forName("ticketIndent.phoneNumber").eq(ticketIndent.getPhoneNumber()));
		dcP.add(Property.forName("ticketIndent.status").in(new String[]{"01","02","03","04","05","08"}));
		dcP.add(Property.forName("ticketType.ticketCode").eq("05"));
		dcP.add(Property.forName("ticketType.specialType").eq("07"));
		dcP.add(Property.forName("ticketType.ticketBigType").eq("02"));
		List<TicketIndentDetail> tisP = ticketIndentDetailDao.findByCriteria(dcP);
		if(tisP!=null && tisP.size()>0){
			throw new BusinessException("手机号："+ticketIndent.getPhoneNumber()+"已购买过日月坪观景索道活动票，不能重复购买！","");
		}
		
		boolean updateRemain = true;
		Long userId=null;
		if(ticketIndent.getIndentId()==null || ticketIndent.getIndentId().trim().isEmpty()){
			userId = getCurrentUserId();
		}else{
			userId = ticketIndent.getInputer();
			updateRemain = false;
		}
		Long roleId = 0L;
		if(userId==null){
			userId = 0L;
			DetachedCriteria dcSU = DetachedCriteria.forClass(SysRole.class);
			dcSU.add(Property.forName("roleCode").eq("003"));
			List<SysRole> listSR = sysRoleDao.findByCriteria(dcSU);
			for(SysRole sr : listSR){
				roleId = sr.getRoleId();
			}
		}else{
			DetachedCriteria dcSU = DetachedCriteria.forClass(SysUserRole.class);
			dcSU.add(Property.forName("sysUser.userId").eq(userId));
			List<SysUserRole> listSU = sysUserRoleDao.findByCriteria(dcSU);
			for(SysUserRole sr : listSU){
				roleId = sr.getSysRole().getRoleId();
			}
		}
		
		String isOutbound = t.getTicketIndent().getIsOutbound();//'11'是境外团
		
		boolean isAgr = sysUserDao.findIsAgreementByUseId(userId);
		
//		Map<String,Integer> gameNums = new HashMap<String,Integer>();
//		Map<String,String> gameGate = new HashMap<String,String>();
		Map<String,TicketPrice> tpMap = new HashMap<String,TicketPrice>();
//		if("02".equals(ticketIndent.getCustomerType())){//散客需判断购买数量
//			for (TicketIndentDetail ticketIndentDetail : coll) {
//				if(ticketIndentDetail.getAmount()!=null && ticketIndentDetail.getAmount()>0){
//					ticketIndentDetail.setRemark(t.getRemark());
//					ticketIndentDetail.setBuyType(t.getBuyType());
//					TicketPrice ticketPrice = checkTopLimit(ticketIndent, ticketIndentDetail, ticketIndent.getCustomerType(), isOutbound, isAgr, userId, roleId, updateRemain);
//					tpMap.put("tp_"+ticketPrice.getTicketPriceId(), ticketPrice);
//					TicketType ticketType = ticketPrice.getTicketType();
//					if("02".equals(ticketType.getTicketBigType())){//项目票
//						
//						String gate =  ticketType.getGate();
//						int amount = ticketIndentDetail.getAmount();
//						String gateName = ticketType.getGateText().replaceAll("闸口","票");
//						
//						int gaNum = gameNums.get("gate"+gate)==null?0:gameNums.get("gate"+gate);
//						
//						gaNum =  gaNum + amount;
//						gameNums.put("gate"+gate, gaNum);
//						gameGate.put("gate"+gate, gateName);
//					}else{
//						gateAmount = gateAmount +ticketIndentDetail.getAmount();
//					}
//				}
//			}
//			
//			String iTop = findSysParam("INDENT_TOPLIMIT");
//			int inTop = 0;
//			if(iTop!=null && iTop!=""){
//				inTop = Integer.valueOf(iTop);
//			}
//			
//			for(String key : gameNums.keySet()){
//				int gaNum = gameNums.get(key);
//				if(inTop>0 && gaNum > inTop){
//					throw new BusinessException("您购买的"+gameGate.get(key)+"数量："+gaNum+"已大于购买单种项目票的上限："+inTop,"");
//				}
//			}
//			
//			//20141203 限制散客每日购票不超过5张 start
//			if(!"05".equals(ticketIndent.getPayType()) && !"06".equals(ticketIndent.getPayType())){
//				DetachedCriteria tiDc = DetachedCriteria.forClass(TicketIndent.class);
//				tiDc.add(Property.forName("inputer").eq(getCurrentUserId()));
//				tiDc.add(Property.forName("useDate").eq(ticketIndent.getUseDate()));
//				tiDc.add(Property.forName("status").in(new String[]{"02","03","04","05"}));
//				//20150514 start
//				tiDc.add(Property.forName("indentType").eq("01"));
//				//20150514 end
//				List<TicketIndent> tiList = ticketIndentDao.findByCriteria(tiDc);
//				for(TicketIndent ti:tiList){
//					gateAmount = gateAmount +ti.getTotalAmount();
//				}
//				if(gateAmount>5){
//					throw new BusinessException("您购买的"+CommonMethod.timeToString(ticketIndent.getUseDate())+"日的门票总数量已大于5张，不能购买！","");
//				}
//			}
//			//20141203 限制散客每日购票不超过5张 end
//		}
		
		for (TicketIndentDetail ticketIndentDetail : coll) {//保存定单明细计算总数量总金额
			if(ticketIndentDetail.getAmount()!=null && ticketIndentDetail.getAmount()>0){
				ticketIndentDetail.setRemark(t.getRemark());
				ticketIndentDetail.setBuyType(t.getBuyType());
				TicketPrice ticketPrice=null;
				
				if(tpMap.size()>0){
					ticketPrice = tpMap.get("tp_"+ticketIndentDetail.getTicketPrice().getTicketPriceId());
				}else{
					ticketPrice = checkTopLimit(ticketIndent, ticketIndentDetail, ticketIndent.getCustomerType(), isOutbound, isAgr, userId, roleId, updateRemain);
				}
				
				ticketIndentDetail.setTicketPrice(ticketPrice);
				TicketType ticketType = ticketPrice.getTicketType();
				
				checkActivity(ticketType,ticketIndentDetail,t, ticketIndent);
				ticketIndentDetail.setIndentDetailId(""+getNextKey("Ticket_Indent_Detail", 1));
				ticketIndentDetail.setTicketIndent(ticketIndent);
				//境外团购票和协议旅行社计算价格为打印价格
				if("11".equals(isOutbound) || sysUserDao.findIsAgreementByUseId(userId)){//lzp 2013-11-10 合并购票核心部分
					ticketIndentDetail.setUnitPrice(ticketPrice.getPrintPrice());
					ticketIndentDetail.setEntranceUnitPrice(ticketPrice.getEntrancePrice());
					ticketIndentDetail.setRopewayUnitPrice(ticketPrice.getRopewayPrice());
				}else{
					ticketIndentDetail.setUnitPrice(ticketPrice.getUnitPrice());
					ticketIndentDetail.setEntranceUnitPrice(ticketPrice.getEntranceUnitPrice());
					ticketIndentDetail.setRopewayUnitPrice(ticketPrice.getRopewayUnitPrice());
				}
				
				ticketIndentDetail.setEntranceCompany(ticketPrice.getEntranceCompany());
				ticketIndentDetail.setRopewayCompany(ticketPrice.getRopewayCompany());
				
				ticketIndentDetail.setSubtotal(ticketIndentDetail.getAmount()*ticketIndentDetail.getUnitPrice());
				ticketIndentDetail.setInputer(getCurrentUserId());
				ticketIndentDetail.setInputTime(ctime);
				ticketIndentDetail.setPerFee(perFee);
				ticketIndentDetail.setPayFee(ticketIndentDetail.getAmount()*ticketIndentDetail.getUnitPrice()*ticketIndentDetail.getPerFee());
				
				if("02".equals(ticketType.getTicketBigType())){//项目票
					
					int remainToplimit = ticketPrice.getRemainToplimit() == null ? 0 : ticketPrice.getRemainToplimit();
					remainToplimit = remainToplimit - ticketIndentDetail.getAmount();
					if(remainToplimit<0){
						throw new BusinessException("您选择的观景索道票剩余数量不足！","");
					}
					ticketPrice.setRemainToplimit(remainToplimit);
					
				}
				
				totalAmount += ticketIndentDetail.getAmount();
				totalPrice += ticketIndentDetail.getAmount()*ticketIndentDetail.getUnitPrice();
				ticketPriceDao.update(ticketPrice);
			}
		}
		
//		//获取身份证信息
//		String idCardMessage4 =ticketIndent.getIdCardNumber().substring(0,4);
//		String idCardMessage6 =ticketIndent.getIdCardNumber().substring(0,6);
//		//5101 成都市5107  绵阳市 5106  德阳市5113  南充市5129  南充市5109  遂宁市5120  资阳市513901  资阳地区513902  简阳511027  简阳511026  511081 资阳
//		if("5101".equals(idCardMessage4) || "5106".equals(idCardMessage4) || "5107".equals(idCardMessage4) ||"5109".equals(idCardMessage4)
//			||"5113".equals(idCardMessage4) || "5120".equals(idCardMessage4) ||"5129".equals(idCardMessage4)
//			|| "511026".equals(idCardMessage6) || "511027".equals(idCardMessage6) ||"511081".equals(idCardMessage6)
//			||"513901".equals(idCardMessage6) || "513902".equals(idCardMessage6)){
//			//成都地区的购票数量限制
//			if("5101".equals(idCardMessage4)){
//				DetachedCriteria dcCd = DetachedCriteria.forClass(TicketIndent.class);
//				dcCd.add(Property.forName("idCardNumber").like(idCardMessage4+"%"));
//				dcCd.add(Property.forName("status").in(new String[]{"01","02","03","04","05","08"}));
//				dcCd.add(Property.forName("indentType").eq("02"));
//				dcCd.add(Property.forName("customerType").eq("01"));
//				dcCd.add(Property.forName("telNumber").eq("1"));
//				dcCd.add(Property.forName("faxNumber").isNull());
//				List<TicketIndent> tiCd = ticketIndentDao.findByCriteria(dcCd);
//				if(tiCd!=null && (tiCd.size()+totalAmount>cdLimitAmount)){
//					throw new BusinessException("成都地区购买日月坪观景索道活动票不能超过"+cdLimitAmount+"张！","");
//				}
//			}else if("5106".equals(idCardMessage4)){//德阳地区的购票数量限制
//				DetachedCriteria dcDy = DetachedCriteria.forClass(TicketIndent.class);
//				dcDy.add(Property.forName("idCardNumber").like(idCardMessage4+"%"));
//				dcDy.add(Property.forName("status").in(new String[]{"01","02","03","04","05","08"}));
//				dcDy.add(Property.forName("indentType").eq("02"));
//				dcDy.add(Property.forName("customerType").eq("01"));
//				dcDy.add(Property.forName("telNumber").eq("1"));
//				dcDy.add(Property.forName("faxNumber").isNull());
//				List<TicketIndent> tiDy = ticketIndentDao.findByCriteria(dcDy);
//				if(tiDy!=null && (tiDy.size()+totalAmount>dyLimitAmount)){
//					throw new BusinessException("德阳地区购买日月坪观景索道活动票不能超过"+dyLimitAmount+"张！","");
//				}
//			}else if("5107".equals(idCardMessage4)){//绵阳地区的购票数量限制
//				DetachedCriteria dcMy = DetachedCriteria.forClass(TicketIndent.class);
//				dcMy.add(Property.forName("idCardNumber").like(idCardMessage4+"%"));
//				dcMy.add(Property.forName("status").in(new String[]{"01","02","03","04","05","08"}));
//				dcMy.add(Property.forName("indentType").eq("02"));
//				dcMy.add(Property.forName("customerType").eq("01"));
//				dcMy.add(Property.forName("telNumber").eq("1"));
//				dcMy.add(Property.forName("faxNumber").isNull());
//				List<TicketIndent> tiMy = ticketIndentDao.findByCriteria(dcMy);
//				if(tiMy!=null && (tiMy.size()+totalAmount>myLimitAmount)){
//					throw new BusinessException("绵阳地区购买日月坪观景索道活动票不能超过"+myLimitAmount+"张！","");
//				}
//			}else if("5109".equals(idCardMessage4)){//遂宁地区的购票数量限制
//				DetachedCriteria dcSn = DetachedCriteria.forClass(TicketIndent.class);
//				dcSn.add(Property.forName("idCardNumber").like(idCardMessage4+"%"));
//				dcSn.add(Property.forName("status").in(new String[]{"01","02","03","04","05","08"}));
//				dcSn.add(Property.forName("indentType").eq("02"));
//				dcSn.add(Property.forName("customerType").eq("01"));
//				dcSn.add(Property.forName("telNumber").eq("1"));
//				dcSn.add(Property.forName("faxNumber").isNull());
//				List<TicketIndent> tiSn = ticketIndentDao.findByCriteria(dcSn);
//				if(tiSn!=null && (tiSn.size()+totalAmount>snLimitAmount)){
//					throw new BusinessException("遂宁地区购买日月坪观景索道活动票不能超过"+snLimitAmount+"张！","");
//				}
//			}else if("5113".equals(idCardMessage4) || "5129".equals(idCardMessage4)){//南充地区的购票数量限制
//				DetachedCriteria dcNc = DetachedCriteria.forClass(TicketIndent.class);
//				dcNc.add(Restrictions.or(Restrictions.like("idCardNumber", "5113%"),Restrictions.like("idCardNumber", "5129%")));
//				dcNc.add(Property.forName("status").in(new String[]{"01","02","03","04","05","08"}));
//				dcNc.add(Property.forName("indentType").eq("02"));
//				dcNc.add(Property.forName("customerType").eq("01"));
//				dcNc.add(Property.forName("telNumber").eq("1"));
//				dcNc.add(Property.forName("faxNumber").isNull());
//				List<TicketIndent> tiNc = ticketIndentDao.findByCriteria(dcNc);
//				if(tiNc!=null && (tiNc.size()+totalAmount>ncLimitAmount)){
//					throw new BusinessException("南充地区购买日月坪观景索道活动票不能超过"+ncLimitAmount+"张！","");
//				}
//			}else if("5120".equals(idCardMessage4) || "511026".equals(idCardMessage6) 
//					||"511081".equals(idCardMessage6) ||"513901".equals(idCardMessage6)){//资阳地区的购票数量限制
//				DetachedCriteria dcZy = DetachedCriteria.forClass(TicketIndent.class);
//				dcZy.add(Restrictions.or(Restrictions.like("idCardNumber", "5120%"),
//						Restrictions.or(Restrictions.or(Restrictions.like("idCardNumber", "511081%"),
//								Restrictions.like("idCardNumber", "513901%")),
//								Restrictions.like("idCardNumber", "511026%"))));
//				dcZy.add(Property.forName("status").in(new String[]{"01","02","03","04","05","08"}));
//				dcZy.add(Property.forName("indentType").eq("02"));
//				dcZy.add(Property.forName("customerType").eq("01"));
//				dcZy.add(Property.forName("telNumber").eq("1"));
//				dcZy.add(Property.forName("faxNumber").isNull());
//				List<TicketIndent> tiZy = ticketIndentDao.findByCriteria(dcZy);
//				if(tiZy!=null && (tiZy.size()+totalAmount>zyLimitAmount)){
//					throw new BusinessException("资阳地区购买日月坪观景索道活动票不能超过"+zyLimitAmount+"张！","");
//				}
//			}else if("511027".equals(idCardMessage6) || "513902".equals(idCardMessage6)){//简阳地区的购票数量限制
//				DetachedCriteria dcJy = DetachedCriteria.forClass(TicketIndent.class);
//				dcJy.add(Restrictions.or(Restrictions.like("idCardNumber", "5113%"),Restrictions.like("idCardNumber", "5129%")));
//				dcJy.add(Property.forName("status").in(new String[]{"01","02","03","04","05","08"}));
//				dcJy.add(Property.forName("indentType").eq("02"));
//				dcJy.add(Property.forName("customerType").eq("01"));
//				dcJy.add(Property.forName("telNumber").eq("1"));
//				dcJy.add(Property.forName("faxNumber").isNull());
//				List<TicketIndent> tiJy = ticketIndentDao.findByCriteria(dcJy);
//				if(tiJy!=null && (tiJy.size()+totalAmount>jyLimitAmount)){
//					throw new BusinessException("简阳地区购买日月坪观景索道活动票不能超过"+jyLimitAmount+"张！","");
//				}
//			}
//		}else{
//			throw new BusinessException("成都、绵阳、德阳、南充、遂宁、资阳、简阳以外的省市不能购买日月坪观景索道活动票！","");
//		}
		
		//购买票时，设定订单类型   2013-12-10 lzp
		setIndentType(ticketIndent,coll);
		
		if("05".equals(ticketIndent.getPayType()) || "06".equals(ticketIndent.getPayType())){//微信或微博订单
			ticketIndent.setStatus("03");
		}else if(!"21".equals(ticketIndent.getStatus())){//协议旅行社保存订单
			ticketIndent.setStatus("01");//未支付
		}
		ticketIndent.setTotalAmount(totalAmount);
		ticketIndent.setTotalPrice(totalPrice);
		ticketIndent.setPerFee(perFee);
		ticketIndent.setPayFee(totalPrice*perFee);
		ticketIndent.setSynchroState("00");//初始未同步状态
		ticketIndent.setIsOutbound(isOutbound);//非境外订单
		if("1".equals(t.getBuyType())){
			ticketIndent.setTelNumber("1");
		}
		//游玩当日门票上限check 2014-01-26 tt
//		checkAmount(ticketIndent);
		
		if(t.getTicketIndent().getIsOutbound()!=null && t.getTicketIndent().getIsOutbound().trim().equals("11")){
			ticketIndent.setStatus("21");//未支付
		}
		if(ticketIndent.getIndentId()==null || ticketIndent.getIndentId().trim().isEmpty()){//无id时新增订单
			ticketIndent.setCheckCode(findCheckCode(ticketIndent));//验证码
			ticketIndent.setIndentCode(getNextIndentCode(CommonMethod.getCurrentYMD()+"000001",CommonMethod.getCurrentYMD()));
			ticketIndent.setIndentId(getNextKey("ticket_Indent", 1).toString());
			ticketIndent.setInputer(getCurrentUserId());
			ticketIndent.setInputTime(ctime);
			ticketIndentDao.save(ticketIndent);
		}
		else{//修改订单,首先删除所有订单明细
			ticketIndentDetailDao.deleteByIndentId(ticketIndent.getIndentId());
			ticketIndent.setUpdater(getCurrentUserId());
			ticketIndent.setUpdateTime(ctime);
			ticketIndentDao.update(ticketIndent);
		}
		for (TicketIndentDetail ticketIndentDetail : coll) {//保存完订单信息，再保存定单明细信息
			if(ticketIndentDetail.getIndentDetailId()!=null && !ticketIndentDetail.getIndentDetailId().isEmpty()){
				ticketIndentDetailDao.save(ticketIndentDetail);
			}
		}
	}
	
	@Override
	public boolean saveReturn(TicketIndentDetail t) {
		TicketIndent ticketIndent = ticketIndentDao.findById(t.getTicketIndent().getIndentId());
		
		//验证订单是否符合退票条件,
		if(!"01".equals(ticketIndent.getStatus())) {
			return false;//不符合退票条件则不执行退票
		}
		
		//获取订单总金额
//		double totalPrice = ticketIndent.getTotalPrice();
		
		//获取客户类型
		String customerType = ticketIndent.getCustomerType();
		
//		if("00".equals(ticketIndent.getSynchroState())){//对于尚未同步的，直接退款
//			//修改订单状态和同步状态
//			ticketIndent.setStatus("06");//已退票
//			ticketIndent.setSynchroState("05");//退票已同步
//			ticketIndent.setUpdater(getCurrentUserId());
//			ticketIndent.setUpdateTime(CommonMethod.getTimeStamp());
//		}
//		else{
			//修改订单状态和同步状态
//			ticketIndent.setStatus("08");//申请退票
//			ticketIndent.setSynchroState("04");//退票未同步
//			ticketIndent.setUpdater(getCurrentUserId());
//			ticketIndent.setUpdateTime(CommonMethod.getTimeStamp());
//		}
		
		
		if("01".equals(customerType)){//旅行社
			//修改旅行社余额
			User user = ticketIndent.getUser();
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Travservice.class);
			detachedCriteria.add(Property.forName("user").eq(user));
			List<Travservice> travservices = baseTravelAgencyService.findByCriteria(detachedCriteria);
			Travservice baseTravelAgency = travservices.get(0);
			baseTravelAgency.setCashBalance(baseTravelAgency.getCashBalance() + t.getSubtotal());
			baseTravelAgencyDao.update(baseTravelAgency);
			
			//添加交易记录
			BaseTranRec baseTranRec = new BaseTranRec();
			baseTranRec.setTicketIndent(ticketIndent);
			baseTranRec.setTicketIndentDetail(t);
			baseTranRec.setMoney((0 - t.getSubtotal()));
			switch (ticketIndent.getPayType()) {
			case "01":
				baseTranRec.setPaymentType("08");// 支付类型:退款
				baseTranRec.setPayType("01");//	交易类型,通过余额交易
				baseTranRec.setPaymentTime(CommonMethod.getTimeStamp());
				baseTranRecService.save(baseTranRec);

				//添加现金账户记录
				BaseCashAccount baseCashAccount = new BaseCashAccount();
				baseCashAccount.setBaseTravelAgency(baseTravelAgency);
				baseCashAccount.setCashBalance(t.getSubtotal());
				baseCashAccountService.save(baseCashAccount);

				//添加充值记录
				BaseRechargeRec baseRechargeRec = new BaseRechargeRec();
				baseRechargeRec.setRechargeRecId(getNextKey("BASE_RECHARGE_REC", 1));
				baseRechargeRec.setBaseTravelAgency(baseTravelAgency);
				baseRechargeRec.setTicketIndent(ticketIndent);
				baseRechargeRec.setTicketIndentDetail(t);
				baseRechargeRec.setSourceDescription("团客退票");
				baseRechargeRec.setCashMoney(t.getSubtotal());
				baseRechargeRec.setCashBalance(baseTravelAgency.getCashBalance());
				baseRechargeRec.setPaymentType("08");
				baseRechargeRec.setInputer(getCurrentUserId());
				baseRechargeRec.setInputTime(CommonMethod.getTimeStamp());
				baseRechargeRecService.save(baseRechargeRec);

				break;
			
			case "02":
				baseTranRec.setPaymentType("08");	//支付类型:退款
				baseTranRec.setPayType("02");		//交易类型,通过奖励款交易
				baseTranRec.setPaymentTime(CommonMethod.getTimeStamp());
				baseTranRecService.save(baseTranRec);
				
				//添加奖励款账户记录
				detachedCriteria = DetachedCriteria.forClass(BaseRewardType.class);
				detachedCriteria.add(Property.forName("rewardTypeName").eq("奖励退款"));
				List<BaseRewardType> rewardTypes = baseRewardTypeDao.findByCriteria(detachedCriteria);
				BaseRewardAccount rewardAccount = new BaseRewardAccount(rewardTypes.get(0), baseTravelAgency, t.getSubtotal(), "00", 0.0); 
				baseRewardAccountService.save(rewardAccount);
				
				//添加充值记录
				baseRechargeRec = new BaseRechargeRec();
				baseRechargeRec.setRechargeRecId(getNextKey("BASE_RECHARGE_REC", 1));
				baseRechargeRec.setBaseTravelAgency(baseTravelAgency);
				baseRechargeRec.setTicketIndent(ticketIndent);
				baseRechargeRec.setTicketIndentDetail(t);
				baseRechargeRec.setSourceDescription("团客退票");
				baseRechargeRec.setRewardMoney(t.getSubtotal());
				baseRechargeRec.setRewardBalance(t.getSubtotal());
				baseRechargeRec.setPaymentType("08");
				baseRechargeRec.setInputer(getCurrentUserId());
				baseRechargeRec.setInputTime(CommonMethod.getTimeStamp());
				baseRechargeRecService.save(baseRechargeRec);				
				break;
			}

			//修改剩余票数
			TicketPrice ticketPrice = ticketPriceDao.findById(t.getTicketPrice().getTicketPriceId());
			int remainToplimit = ticketPrice.getRemainToplimit() == null ? 0 : ticketPrice.getRemainToplimit();
			remainToplimit = remainToplimit + t.getAmount();
			ticketPrice.setRemainToplimit(remainToplimit);
			ticketPriceDao.update(ticketPrice);
			
			t.setStatus("08");//申请退票
			// ticketIndentDetailDao.update(t);
			ticketIndentDetailDao.merge(t);
		
			boolean allReturn = true;
			for (TicketIndentDetail indentDetail : ticketIndent.getTicketIndentDetails()) {
				if (!indentDetail.equals(t) && "01".equals(indentDetail.getStatus())) {
					allReturn = false;
				}
			}
			
			if (allReturn) {
				ticketIndent.setStatus("08");//申请退票
				ticketIndent.setSynchroState("04");//退票未同步
				ticketIndent.setUpdater(getCurrentUserId());
				ticketIndent.setUpdateTime(CommonMethod.getTimeStamp());
				ticketIndentDao.update(ticketIndent);
			}
		} else if("02".equals(customerType)){//散客 退票
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(BaseTranRec.class);
			detachedCriteria.add(Property.forName("ticketIndent").eq(ticketIndent));
			detachedCriteria.add(Property.forName("trxId").ne("11111111111111111111111111111111"));
			// List<BaseTranRec> baseTranRecs = baseTranRecDao.findByProperty("ticketIndent.indentId", ticketIndent.getIndentId());
			List<BaseTranRec> baseTranRecs = baseTranRecDao.findByCriteria(detachedCriteria);
			if(baseTranRecs!=null && !baseTranRecs.isEmpty()) {
				BaseTranRec baseTranRec = baseTranRecs.get(0);
				payFactoryService.init();
				RefundParams refundParams = payFactoryService.initRefundParams(baseTranRec,CommonMethod.format2db(t.getSubtotal()),PayFactoryService.TICKET);
				if(payFactoryService.refund(refundParams)){//退款成功
					try{
						BaseTranRec rec = new BaseTranRec();
						rec.setTrxId("11111111111111111111111111111111");
						rec.setTicketIndent(ticketIndent);
						rec.setTicketIndentDetail(t);
						rec.setMoney(0 - t.getSubtotal());//负数
						rec.setPerFee(baseTranRec.getPerFee());
						rec.setPayFee(baseTranRec.getPayFee());
						rec.setPaymentType("08");//退款
						rec.setPayType("00");//交易类型,通过支付宝交易
						rec.setPaymentTime(CommonMethod.getTimeStamp());
						baseTranRecService.save(rec);
						
						//修改剩余票数
						TicketPrice ticketPrice = ticketPriceDao.findById(t.getTicketPrice().getTicketPriceId());
						int remainToplimit = ticketPrice.getRemainToplimit() == null ? 0 : ticketPrice.getRemainToplimit();
						remainToplimit = remainToplimit + t.getAmount();
						ticketPrice.setRemainToplimit(remainToplimit);
						ticketPriceDao.update(ticketPrice);
						
						t.setStatus("08");
						// ticketIndentDetailDao.update(t);
						ticketIndentDetailDao.merge(t);
				
						boolean allReturn = true;
						for (TicketIndentDetail indentDetail : ticketIndent.getTicketIndentDetails()) {
							if (!indentDetail.equals(t) && "01".equals(indentDetail.getStatus())) {
								allReturn = false;
							}
						}
						
						if (allReturn) {
							ticketIndent.setStatus("08");//申请退票
							ticketIndent.setSynchroState("04");//退票未同步
							ticketIndent.setUpdater(getCurrentUserId());
							ticketIndent.setUpdateTime(CommonMethod.getTimeStamp());
							// ticketIndentDao.update(ticketIndent);
							ticketIndentDao.merge(ticketIndent);
						}
					}
					catch (Exception e) {
						e.printStackTrace();
						logger.error("支付宝退款成功后,保存交易记录出错,订单号:"+ticketIndent.getIndentCode());
					}
				}
				else{//退款失败
					// ticketIndent.setStatus("08");//申请退订
					// ticketIndent.setSynchroState("06");//退款未同步
					return false;
				}
			}
		}
		// ticketIndentDao.update(ticketIndent);
		return true;
	}
	
	@Override
	public void saveReturnByMobile(TicketIndentDetail t) {
		TicketIndent ticketIndent = ticketIndentDao.findById(t.getTicketIndent().getIndentId());
		
		//验证订单是否符合退票条件,
		if(!"02".equals(ticketIndent.getStatus()) && !"03".equals(ticketIndent.getStatus())){
			return;//不符合退票条件则不执行退票
		}
		
		TicketIndentClient client = new TicketIndentClient();
		List<TicketIndent> indents = new ArrayList<TicketIndent>();
		indents.add(ticketIndent);
		
		ReturnInfo ri = client.returnTickets(indents);
		if("-1".equals(ri.getResult().trim())){//接口发生错误
			for (TicketIndent ti : indents) {//接口调用失败
				throw new BusinessException("与景区同步失败，退订失败！","");
			}
		}else{
			XStream xsStream = new XStream();
			xsStream.alias("obj", TicketIndent.class);
			xsStream.alias("root", List.class);
			List<TicketIndent> returnIndents = (List<TicketIndent>)xsStream.fromXML(ri.getResult());
			
			for (TicketIndent ti : returnIndents) {//接口调用失败
				if("06".equals(ti.getStatus())){//已退订
					continue;
				}else if("05".equals(ti.getStatus())){
					throw new BusinessException("订单已消费，退订失败！","");
				}else{
					throw new BusinessException("订单已出票，退订失败！","");
				}
			}
			
			//添加财务记录
			financeTicketAccountsService.saveReturnInfo(ticketIndent,null,null);
			
			ticketIndent.setStatus("06");
			ticketIndent.setSynchroState("05");
			ticketIndentDao.merge(ticketIndent);//保存回传的退订信息
			
			//修改剩余票数
			Long userId= ticketIndent.getInputer();
			Long roleId = 0L;
			if(userId==null){
				userId = 0L;
				DetachedCriteria dcSU = DetachedCriteria.forClass(SysRole.class);
				dcSU.add(Property.forName("roleCode").eq("003"));
				List<SysRole> listSR = sysRoleDao.findByCriteria(dcSU);
				for(SysRole sr : listSR){
					roleId = sr.getRoleId();
				}
				
			}else{
				DetachedCriteria dcSU = DetachedCriteria.forClass(SysUserRole.class);
				dcSU.add(Property.forName("sysUser.userId").eq(userId));
				List<SysUserRole> listSU = sysUserRoleDao.findByCriteria(dcSU);
				for(SysUserRole sr : listSU){
					roleId = sr.getSysRole().getRoleId();
				}
			}
			DetachedCriteria dcTI = DetachedCriteria.forClass(TicketIndentDetail.class);
			dcTI.add(Property.forName("ticketIndent.indentId").eq(ticketIndent.getIndentId()));
			List<TicketIndentDetail> tidList =  ticketIndentDetailDao.findByCriteria(dcTI);
			for (TicketIndentDetail ticketIndentDetail : tidList) {
					TicketPrice ticketPrice = ticketPriceDao.findById(ticketIndentDetail.getTicketPrice().getTicketPriceId());
					if(ticketPrice.getRemainToplimit() != null && ticketPrice.getToplimit() !=null){
						int remainToplimit = ticketPrice.getRemainToplimit() == null ? 0 : ticketPrice.getRemainToplimit();
						remainToplimit = remainToplimit + ticketIndentDetail.getAmount();
						ticketPrice.setRemainToplimit(remainToplimit);
						ticketPriceDao.update(ticketPrice);
					}
					
					int updateNums = 0 - ticketIndentDetail.getAmount();
					ticketUserTypeService.updateReminTopLimit(userId, ticketPrice.getTicketType().getTicketId(), updateNums);
					ticketRoleTypeService.updateReminTopLimit(roleId, ticketPrice.getTicketType().getTicketId(), updateNums);
			}
		}
		
		
	}

	@Override
	public JsonPager<TicketIndentDetail> findJsonPageByCriteriaForConfirm(
			JsonPager<TicketIndentDetail> jp,
			TicketIndentDetail ticketIndentDetail) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketIndentDetail.class);
		dc.createAlias("ticketPrice", "ticketPrice");
		dc.createAlias("ticketPrice.ticketType", "ticketType");
		dc.add(Property.forName("ticketIndent.indentId").eq(ticketIndentDetail.getTicketIndent().getIndentId()));
		return ticketIndentDetailDao.findJsonPageByCriteria(jp, dc);
	}
	
	
	
	/**
	 * 生成验证码
	 * @return
	 */
	private String findCheckCode(TicketIndent t){
		String returnStr = "";
		DetachedCriteria dc = DetachedCriteria.forClass(TicketIndent.class);
		dc.add(Property.forName("phoneNumber").eq(t.getPhoneNumber()));
		List<TicketIndent> list = ticketIndentDao.findByCriteria(dc);
		
		for (int i = 0; i < 6; i++) {//产生六位随机数
			returnStr = returnStr+""+(int)(1+Math.random()*9);
		}
		
		for (TicketIndent ticketIndent : list) {
			if(returnStr.equals(ticketIndent.getCheckCode())){//如果相同就继续产生下个随机数
				return findCheckCode(t);
			}
		}
		return returnStr;
	}
	
	/**
	 * 增加游玩当日门票上限check  2014-01-26 tt
	 * @param ticketIndent
	 */
	private void checkAmount(TicketIndent ticketIndent){
		//游玩当日
		String useDate = CommonMethod.timeToString(Timestamp.valueOf(ticketIndent.getUseDate().toString()));
		String paramName = "amount_toplimit";
		int useAmount = 0;
		int amount = 0;
		int limitAmount=0;
		useAmount = ticketIndentDao.findCountByUsedate(useDate);
		amount = ticketIndent.getTotalAmount();
		
		String limitAmountTmp = sysParamService.findSysParam(paramName);
		if(!limitAmountTmp.isEmpty()){
			limitAmount = Integer.valueOf(limitAmountTmp);
		}
		
		if(useAmount + amount > limitAmount){
			int residueAmount = limitAmount-useAmount;
			if(residueAmount < 0){
				residueAmount = 0;
			}
			throw new BusinessException("您购买的门票数量已大于本日购买门票上限，本日门票还剩余："+residueAmount+"张，请确认。","");
		}
	}
	
	public String findCheckAmount(String useDate){
		//游玩当日
		String paramName = "amount_toplimit";
		int useAmount = 0;
		int limitAmount=0;
		useAmount = ticketIndentDao.findCountByUsedate(useDate);
		
		String limitAmountTmp = sysParamService.findSysParam(paramName);
		if(!limitAmountTmp.isEmpty()){
			limitAmount = Integer.valueOf(limitAmountTmp);
		}
		return String.valueOf(limitAmount-useAmount);
	}
	
	public List<TicketIndentDetail> findIndentDetailList(TicketIndent t){
		DetachedCriteria dc = DetachedCriteria.forClass(TicketIndentDetail.class);
		dc.createAlias("ticketPrice", "ticketPrice");
		dc.createAlias("ticketPrice.ticketType", "ticketType");
		dc.createAlias("ticketIndent", "ticketIndent");
		dc.add(Property.forName("ticketIndent.inputer").eq(t.getInputer()));
		return ticketIndentDetailDao.findByCriteria(dc);
	}
}