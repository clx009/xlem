package service.ticket.ticketDiscount;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.TicketDiscount;

import common.CommonMethod;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.ticket.ticketDiscount.TicketDiscountDao;

@Service("ticketDiscountService")
public class TicketDiscountServiceImpl extends BaseServiceImpl<TicketDiscount>
		implements TicketDiscountService {
	@Autowired
	private TicketDiscountDao ticketDiscountDao;
	
	private static Logger logger = Logger.getLogger("bookingService");
	
	@Override
	@Resource(name = "ticketDiscountDao")
	protected void initBaseDAO(BaseDao<TicketDiscount> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<TicketDiscount> findPageByCriteria(
			PaginationSupport<TicketDiscount> ps, TicketDiscount t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketDiscount.class);
		return ticketDiscountDao.findPageByCriteria(ps,Order.desc("ticketDiscountId"), dc);
	}

	@Override
	public void save(TicketDiscount t) {
		t.setTicketDiscountId(getNextKey("TicketDiscount".toUpperCase(), 1));
		ticketDiscountDao.save(t);
	}
	
	@Override
	public List<TicketDiscount> findDiscountByIds(String Ids) {
		if(Ids==null || Ids.isEmpty()){
			return new ArrayList<TicketDiscount>();
		}
		
		DetachedCriteria dc = DetachedCriteria.forClass(TicketDiscount.class);
		
		List<Long> idsList = new ArrayList<Long>();
		String[] id = Ids.split(",");
		
		if(id.length>0){
			for(String i : id){
				idsList.add(Long.valueOf(i));
			}
		}
		
		dc.add(Property.forName("ticketDiscountId").in(idsList));
		List<TicketDiscount> list = ticketDiscountDao.findByCriteria(dc);
		return list;
	}
	
	@Override
	public List<TicketDiscount> findDiscountAll() {
		List<TicketDiscount> list = ticketDiscountDao.findAll();
		return list;
	}
	
	@Override
	public JsonPager<TicketDiscount> findJsonPageByCriteria(
			JsonPager<TicketDiscount> jp, TicketDiscount t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketDiscount.class);
		if(t.getModuleCode()!=null && !t.getModuleCode().equals("")){
			dc.add(Property.forName("moduleCode").eq(t.getModuleCode()));
		}
		dc.addOrder(Order.asc("ticketDiscountId"));
		return ticketDiscountDao.findJsonPageByCriteria(jp, dc);
	}

	@Override
	public void save(Collection<TicketDiscount> coll) {
		for (TicketDiscount ticketDiscount : coll) {
			if (ticketDiscount.getTicketDiscountId() == null) {
				ticketDiscount.setTicketDiscountId(getNextKey("Ticket_Discount", 1));
				ticketDiscount.setInputer(getCurrentUserId());
				ticketDiscount.setInputTime(CommonMethod.getTimeStamp());
				ticketDiscountDao.save(ticketDiscount);
			} else {
				ticketDiscount.setUpdater(getCurrentUserId());
				ticketDiscount.setUpdateTime(CommonMethod.getTimeStamp());
				ticketDiscountDao.update(ticketDiscount);
			}
		}
	}
}