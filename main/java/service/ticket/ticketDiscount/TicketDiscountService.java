package service.ticket.ticketDiscount;
import java.util.Collection;
import java.util.List;

import com.xlem.dao.TicketDiscount;

import common.service.BaseService;

public interface TicketDiscountService extends BaseService<TicketDiscount> {

	void save(Collection<TicketDiscount> coll);

	public List<TicketDiscount> findDiscountByIds(String Ids);

	public List<TicketDiscount> findDiscountAll();
}