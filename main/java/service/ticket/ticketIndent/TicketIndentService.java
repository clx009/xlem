package service.ticket.ticketIndent;
import java.util.List;
import java.util.Map;

import com.xlem.dao.TicketIndent;

import common.action.JsonPager;
import common.pay.PayParams;
import common.service.BaseService;

public interface TicketIndentService extends BaseService<TicketIndent> {
	
	/**
	 * 
	 * @param t
	 * @param userId
	 * @param jp
	 * @param forPage 查询页面，“returnPage”时之查询status为‘02’和‘03’的记录
	 * @return
	 */
	public JsonPager<TicketIndent> findIndentList(JsonPager<TicketIndent> jp,TicketIndent t,String forPage);
	
	/**
	 * 查询总数量和总金额
	 * @param t
	 * @param userId
	 * @param jp
	 * @param forPage 查询页面，“returnPage”时之查询status为‘02’和‘03’的记录
	 * @return
	 */
	public Map<String,Double> findIndentSums(JsonPager<TicketIndent> jp,TicketIndent t,String forPage);
	
	/**
	 * 计算订单分账
	 * @param indentId
	 * @return Map:{snow=,joint=} snow:旅游公司,joint:股份公司
	 */
	public Map<String,Double> calcPrice(String indentId);
	
	/**
	 * 生成支付参数并返回
	 * @param ticketIndent
	 * @return 
	 */
	PayParams savePayParams(TicketIndent ticketIndent);
	
	/**
	 * 微博、微信订单仅记录财务信息
	 * @param ticketIndent
	 */
	void saveFinMsg(TicketIndent ticketIndent);
	
	/**
	 * 校验支付返回信息
	 * @param hmac
	 * @param p1_MerId
	 * @param r0_Cmd
	 * @param r1_Code
	 * @param r2_TrxId
	 * @param r3_Amt
	 * @param r4_Cur
	 * @param r5_Pid
	 * @param r6_Order
	 * @param r7_Uid
	 * @param r8_MP
	 * @param r9_BType
	 * @return 
	 */
	public boolean findPayResult(String hmac, String p1_MerId, String r0_Cmd,
			String r1_Code, String r2_TrxId, String r3_Amt, String r4_Cur,
			String r5_Pid, String r6_Order, String r7_Uid, String r8_MP,
			String r9_BType);
	
	/**
	 * 保存支付结果,更新订单状态,并生成交易记录
	 * @param ticketIndent
	 * @param r2_TrxId
	 * @param r3_Amt
	 * @param rp_PayDate
	 */
	public void savePayResult(TicketIndent ticketIndent, String r2_TrxId,
			String r3_Amt, String rp_PayDate, String out_trade_no,boolean isAct);
	
	/**
	 * 查询订单
	 * @param r6_Order
	 * @return
	 */
	public List<TicketIndent> findIndentForPayResult(String r6_Order);
	
	/**
	 * 生成支付参数
	 * @param ticketIndent
	 * @return
	 */
	public PayParams savePayParamsForTourist(TicketIndent ticketIndent);
	
	/**
	 * 取消订单
	 * @param ticketIndent
	 */
	public void updateForClose(TicketIndent ticketIndent);
	
	/**
	 * 改签前查询票价是否为更
	 * @param ticketIndent
	 * @return
	 */
	public boolean findBeforChange(TicketIndent ticketIndent);
	
	/**
	 * 保存改签订单
	 * @param ticketIndent
	 */
	public void saveChange(TicketIndent ticketIndent);
	
	/**
	 * 审核外地旅行社订单
	 */
	public TicketIndent saveCheckSub(TicketIndent ticketIndent);
	
	/**
	 * 拒绝外地旅行社订单
	 */
	public TicketIndent saveCheckNot(TicketIndent ticketIndent);
	
	/**
	 * 查询订单信息(项目票主订单)
	 */
	public TicketIndent findProjectTicketInfo(String indentCode);
	/**
	 * 查询活动订单
	 */
	public TicketIndent findActIndent(Long userId,Long activityId);
	
	/**
	 * 退现信息确认页面确认退款
	 * @param ticketIndent
	 */
	public void affirmRefund(TicketIndent ticketIndent);
	
}