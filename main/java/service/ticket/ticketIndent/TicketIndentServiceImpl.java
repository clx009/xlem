package service.ticket.ticketIndent;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.BaseRewardAccount;
import com.xlem.dao.BaseTranRec;
import com.xlem.dao.SysRole;
import com.xlem.dao.SysUser;
import com.xlem.dao.SysUserRole;
import com.xlem.dao.TicketIndent;
import com.xlem.dao.TicketIndentCheckLog;
import com.xlem.dao.TicketIndentDetail;
import com.xlem.dao.TicketPrice;
import com.xlem.dao.Travservice;
import com.xlem.dao.TravserviceHome;

import service.comm.baseRechargeRec.BaseRechargeRecService;
import service.comm.baseTranRec.BaseTranRecService;
import service.comm.reg.baseTravelAgency.BaseTravelAgencyService;
import service.ticket.financeTicketAccounts.FinanceTicketAccountsService;
import service.ticket.ticketIndentDetail.TicketIndentDetailService;
import service.ticket.ticketRoleType.TicketRoleTypeService;
import service.ticket.ticketUserType.TicketUserTypeService;

// import com.yeepay.PaymentForOnlineService;
import common.BusinessException;
import common.CommonMethod;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.pay.PayFactoryService;
import common.pay.PayParams;
import common.service.BaseServiceImpl;

import dao.comm.baseCashAccount.BaseCashAccountDao;
import dao.comm.baseMessageRec.BaseMessageRecDao;
import dao.comm.baseRechargeRec.BaseRechargeRecDao;
import dao.comm.baseRewardAccount.BaseRewardAccountDao;
import dao.comm.reg.baseTravelAgency.BaseTravelAgencyDao;
import dao.sys.sysParam.SysParamDao;
import dao.sys.sysRole.SysRoleDao;
import dao.sys.sysUser.SysUserDao;
import dao.sys.sysUserRole.SysUserRoleDao;
import dao.ticket.ticketBarcodeRec.TicketBarcodeRecDao;
import dao.ticket.ticketIndent.TicketIndentDao;
import dao.ticket.ticketIndentCheckLog.TicketIndentCheckLogDao;
import dao.ticket.ticketIndentDetail.TicketIndentDetailDao;
import dao.ticket.ticketPrice.TicketPriceDao;

@Service("ticketIndentService")
public class TicketIndentServiceImpl extends BaseServiceImpl<TicketIndent>
		implements TicketIndentService {
	@Autowired
	private TicketIndentDao ticketIndentDao;
	@Autowired
	private BaseTranRecService baseTranRecService;
	@Autowired
	private BaseTravelAgencyDao baseTravelAgencyDao;
	@Autowired
	private BaseTravelAgencyService baseTravelAgencyService;
	@Autowired
	private BaseRechargeRecService baseRechargeRecService;
	@Autowired
	private PayFactoryService payFactoryService;
	@Autowired
	private TicketPriceDao ticketPriceDao;
	@Autowired
	private TicketIndentDetailDao ticketIndentDetailDao;
	@Autowired
	private TicketIndentDetailService ticketIndentDetailService;
	@Autowired
	private TicketIndentCheckLogDao ticketIndentCheckLogDao;
	@Autowired
	private SysUserDao sysUserDao;
	@Autowired
	private BaseMessageRecDao baseMessageRecDao;
	@Autowired
	private BaseRewardAccountDao baseRewardAccountDao;
	@Autowired
	private TicketBarcodeRecDao ticketBarcodeRecDao;
	@Autowired
	private SysUserRoleDao sysUserRoleDao;
	@Autowired
	private SysRoleDao sysRoleDao;
	@Autowired
	private TicketUserTypeService ticketUserTypeService;
	@Autowired
	private TicketRoleTypeService ticketRoleTypeService;
	@Autowired
	private BaseCashAccountDao baseCashAccountDao;
	@Autowired
	private FinanceTicketAccountsService financeTicketAccountsService;
	@Autowired
	private SysParamDao sysParamDao;
	@Autowired
	private BaseRechargeRecDao baseRechargeRecDao;
	
	
	
	@Override
	@Resource(name = "ticketIndentDao")
	protected void initBaseDAO(BaseDao<TicketIndent> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<TicketIndent> findPageByCriteria(
			PaginationSupport<TicketIndent> ps, TicketIndent t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketIndent.class);
		return ticketIndentDao.findPageByCriteria(ps,
				Order.desc("ticketIndentId"), dc);
	}

	@Override
	public void save(TicketIndent t) {
		// t.setIndentId(String.valueOf(getNextKey("Ticket_Indent".toUpperCase(), 1)));
		ticketIndentDao.save(t);
	}

	@Override
	public JsonPager<TicketIndent> findJsonPageByCriteria(
			JsonPager<TicketIndent> jp, TicketIndent t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketIndent.class);

		dc.add(Property.forName("inputer").eq(getCurrentUserId()));

		if (t.getIndentCode() != null && !t.getIndentCode().trim().isEmpty()) {
			dc.add(Property.forName("indentCode").eq(t.getIndentCode().trim()));
		}

		if (t.getPhoneNumber() != null && !t.getPhoneNumber().trim().isEmpty()) {
			dc.add(Property.forName("phoneNumber")
					.eq(t.getPhoneNumber().trim()));
		}

		if (t.getIdCardNumber() != null
				&& !t.getIdCardNumber().trim().isEmpty()) {
			dc.add(Property.forName("idCardNumber").eq(
					t.getIdCardNumber().trim()));
		}

		if (t.getStartDate() != null) {
			dc.add(Property.forName("inputTime").ge(t.getStartDate()));
		}

		if (t.getEndDate() != null) {
			dc.add(Property.forName("inputTime").le(CommonMethod.toEndTime(t.getEndDate())));
		}

		// dc.createAlias("ticketIndentDetail", "ticketIndentDetails");

		dc.addOrder(Order.desc("inputTime"));
		return ticketIndentDao.findJsonPageByCriteria(jp, dc);
	}

	
	/**
	 * 计算订单分账
	 * @param indentId
	 * @return Map:{snow=,joint=} snow:旅游公司,joint:股份公司
	 */
	public Map<String,Double> calcPrice(String indentId) {
		
		DetachedCriteria dc = DetachedCriteria.forClass(TicketIndentDetail.class);
		dc.add(Property.forName("ticketIndent.indentId").eq(indentId));
		List<TicketIndentDetail> tidList =  ticketIndentDetailDao.findByCriteria(dc);
		Map<String,Double> reMap = new HashMap<String,Double>();
		for (TicketIndentDetail ticketIndentDetail : tidList) {
			int amount = ticketIndentDetail.getAmount();
			
			double euPrice = ticketIndentDetail.getEntranceUnitPrice()==null?0:ticketIndentDetail.getEntranceUnitPrice();
			euPrice = euPrice * amount;
			Double EP = reMap.get("snow");
			if(EP==null){
				EP = 0D;
			}
			reMap.put("snow", euPrice+EP);
			
			double ruPrice = ticketIndentDetail.getRopewayUnitPrice()==null?0:ticketIndentDetail.getRopewayUnitPrice();
			ruPrice = ruPrice * amount;
			Double RP = reMap.get("joint");
			if(RP==null){
				RP = 0D;
			}
			reMap.put("joint", ruPrice+RP);
		}
		return reMap;
	}
	
	
	@Override
	public PayParams savePayParams(TicketIndent ticketIndent){
		
		Timestamp thisTime = CommonMethod.getTimeStamp();
		String payMag = "";//需要在页面提示的信息
		
		TicketIndent ticketIndentFdb = ticketIndentDao.findById(ticketIndent.getIndentId());
		
//		String indentType = ticketIndentFdb.getIndentType();
		
		//追加项目票时，奖励款不能支付   2014-02-20 tt
//		if("04".equals(indentType) || "02".equals(indentType)){
//			if("01".equals(ticketIndent.getPayType())){
//				throw new BusinessException("奖励款不能购买项目票！","");
//			}
//		}
		if("02".equals(ticketIndentFdb.getStatus()) || "03".equals(ticketIndentFdb.getStatus())){
			throw new BusinessException("订单已支付,请不要重复提交!","");
		}else if(!"01".equals(ticketIndentFdb.getStatus()) && !"23".equals(ticketIndentFdb.getStatus())){
			throw new BusinessException("订单当前状态不能支付!","");
		}
		
		Long userId = getCurrentUserId();
		Long roleId = 0L;
		if(userId==null){
			userId = 0L;
			DetachedCriteria dcSU = DetachedCriteria.forClass(SysRole.class);
			dcSU.add(Property.forName("roleCode").eq("003"));
			List<SysRole> listSR = sysRoleDao.findByCriteria(dcSU);
			for(SysRole sr : listSR){
				roleId = sr.getRoleId();
			}
			
		}else{
			DetachedCriteria dcSU = DetachedCriteria.forClass(SysUserRole.class);
			dcSU.add(Property.forName("sysUser.userId").eq(userId));
			List<SysUserRole> listSU = sysUserRoleDao.findByCriteria(dcSU);
			for(SysUserRole sr : listSU){
				roleId = sr.getSysRole().getRoleId();
			}
		}
		
		DetachedCriteria dc = DetachedCriteria.forClass(TicketIndentDetail.class);
		dc.add(Property.forName("ticketIndent.indentId").eq(ticketIndent.getIndentId()));
		List<TicketIndentDetail> tdList = ticketIndentDetailService.findByCriteria(dc);
		
		boolean isAgr = sysUserDao.findIsAgreementByUseId(userId);
		//是否不需要发送短信
		boolean isAct = false;
		//是否不需要窗口验证
		boolean isCheck = true;
		//是否为索道套票
		boolean isCablewayPackages = false;
		for(TicketIndentDetail tid : tdList){
			tid.setBuyType(ticketIndentFdb.getTelNumber());
			if("02".equals(ticketIndentFdb.getIndentType()) //观景索道票
				||"04".equals(ticketIndentFdb.getIndentType())//观景索道票
				||"05".equals(ticketIndentFdb.getIndentType())){//项目票
				ticketIndentFdb.setAuditor(1L);
			}
			ticketIndentDetailService.checkTopLimit(ticketIndentFdb, tid, "01", ticketIndentFdb.getIsOutbound(), isAgr, userId, roleId, false);
			
			TicketPrice tps = ticketPriceDao.findById(tid.getTicketPrice().getTicketPriceId());
			long temTicketId = tps.getTicketType().getTicketId();
			if(temTicketId==3040 || temTicketId==3041 || temTicketId==3042 || //途牛活动
					3046==temTicketId || 3047==temTicketId || 3048==temTicketId || 3049==temTicketId || //携程活动
					3058==temTicketId || 3059==temTicketId || 3060==temTicketId || 3061==temTicketId || 3066==temTicketId || 3067==temTicketId //同程活动
					|| 3062==temTicketId || 3063==temTicketId){//日月坪抢购活动票
				isAct =  true;
			}
			//观景索道活动票
			if("07".equals(tps.getTicketType().getSpecialType())){
				isAct =  true;
			}
			//索道套票
			if("01".equals(tps.getTicketType().getActivity()) 
					|| "02".equals(tps.getTicketType().getActivity())//夜场滑雪套票
					|| "03".equals(tps.getTicketType().getActivity())){//夜场滑雪2小时滑雪票
				isAct =  true;
				isCheck = false;
			}
			if("01".equals(tps.getTicketType().getActivity())){//索道套票
				isCablewayPackages = true;
			}
		}
		//购买索道套票时，不能使用奖励款
		if(isCablewayPackages){
			if("01".equals(ticketIndent.getPayType())){
				throw new BusinessException("奖励款不能购买索道套票！","");
			}
		}
		
		ticketIndentFdb.setDefaultbank(ticketIndent.getDefaultbank());
		ticketIndentFdb.setPayType(ticketIndent.getPayType());
		
		
		//查询订单已扣现金余额,在交易记录中查询
		DetachedCriteria dcBtr = DetachedCriteria.forClass(BaseTranRec.class);
		dcBtr.add(Property.forName("ticketIndent.indentId").eq(ticketIndent.getIndentId()));
		dcBtr.add(Property.forName("paymentType").in(new String[]{"01","03"}));
		dcBtr.add(Property.forName("payType").ne("01"));//排除网银支付的记录
		List<BaseTranRec> btrsList = baseTranRecService.findByCriteria(dcBtr);
		
		List<BaseTranRec> btrList = baseTranRecService.excludeReturnBackInfo(btrsList);
		
		double prepaid = 0;
		//股份公司预扣款
		double prepaidJoint = 0;
		//雪山公司预扣款
		double prepaidSnow = 0;
		
		double prepaidTopup = 0;
		double prepaidTopupJoint = 0;
		double prepaidTopupSnow = 0;
		
		for(BaseTranRec btr : btrList){
			String pType  = btr.getPayType();
			if("03".equals(pType)){//预付款支付
				prepaidTopup = prepaidTopup + (btr.getMoney()==null ? 0d : btr.getMoney());
				prepaidTopupJoint = prepaidTopupJoint + (btr.getMoneyJoint()==null ? 0d : btr.getMoneyJoint());
				prepaidTopupSnow = prepaidTopupSnow + (btr.getMoneySnow()==null ? 0d : btr.getMoneySnow());
			}else{//余额付款
				prepaid = prepaid + (btr.getMoney()==null ? 0d : btr.getMoney());
				prepaidJoint = prepaidJoint + (btr.getMoneyJoint()==null ? 0d : btr.getMoneyJoint());
				prepaidSnow = prepaidSnow + (btr.getMoneySnow()==null ? 0d : btr.getMoneySnow());
			}
		}
		
		Double moneyFdb = ticketIndentFdb.getTotalPrice()-prepaid;//订单总金额,要减去预付款的金额
		Double moneyNeedPay = 0d;//需要支付金额
		Double moneyFeePay = 0d;//分润金额
		Travservice baseTravelAgency = baseTravelAgencyService.findTravelAgencyByUser(userId);
		
		ticketIndentFdb.setUpdater(getCurrentUserId());
		ticketIndentFdb.setUpdateTime(thisTime);
		
		baseTravelAgency.setUpdater(getCurrentUserId());
		baseTravelAgency.setUpdateTime(thisTime);
		
		//股份公司余额
		double cashBalanceJoin = (baseTravelAgency.getCashBalance()==null ? 0d : baseTravelAgency.getCashBalance());
		//雪山公司余额
		double cashBalanceSnow = (baseTravelAgency.getCashBalanceSnow()==null ? 0d : baseTravelAgency.getCashBalanceSnow());
		//当前现金余额
		double cashBalance = cashBalanceJoin+cashBalanceSnow;

		//股份公司余额
		double cashTopupBalanceJoin = (baseTravelAgency.getCashTopupBalance()==null ? 0d : baseTravelAgency.getCashTopupBalance());
		//雪山公司余额
		double cashTopupBalanceSnow = (baseTravelAgency.getCashTopupBalanceSnow()==null ? 0d : baseTravelAgency.getCashTopupBalanceSnow());
		//当前现金余额
		double cashTopupBalance = cashTopupBalanceJoin+cashTopupBalanceSnow;
		
		//计算订单分账
		Map<String,Double> priceMap = calcPrice(ticketIndent.getIndentId());
		//股份公司金额
		double indPriceJoint = priceMap.get("joint");
		//雪山公司金额
		double indPriceSnow = priceMap.get("snow");
		
		
		if(ticketIndentFdb.getPayType()!=null && ticketIndentFdb.getPayType().trim().equals("02")){//使用现金余额付款方式
			
			if(prepaidTopupJoint>0 || prepaidTopupSnow>0){
				throw new BusinessException("该订单已使用预付款余额,不允许再使用现金余额支付!","");
			}
			
//			//如果雪山公司不需要支付余额中需减去雪山公司余额
//			if(indPriceSnow<=0){
//				cashBalance = cashBalance - cashBalanceSnow;
//				cashBalanceSnow = 0;
//			}
//			
//			//如果股份公司不需要支付余额中需减去股份公司余额
//			if(indPriceJoint<=0){
//				cashBalance = cashBalance - cashBalanceJoin;
//				cashBalanceJoin = 0;
//			}
			
			if((prepaidJoint>cashBalanceJoin || prepaidSnow>cashBalanceSnow) &&//从现金余额付款大于了实际现金余额时，给出提示重新确认
				!(prepaid>0)){//没有预扣现金余额的，给出提示重新确认
				if(prepaidJoint>cashBalanceJoin){
					throw new BusinessException("因其他订单交易,当前股份公司现金余额已变为:"+cashBalanceJoin+",请再次确认!","");
				}else{
					throw new BusinessException("因其他订单交易,当前雪山公司现金余额已变为:"+cashBalanceSnow+",请再次确认!","");
				}
			}
			else if((cashBalanceSnow+prepaidSnow)>0 || (cashBalanceJoin+prepaidJoint)>0){//有现金余额
				if(cashBalanceJoin>=(indPriceJoint-prepaidJoint) && cashBalanceSnow>=(indPriceSnow-prepaidSnow)){//如果使用的现金余额大于等于订单总金额，股份公司余额和雪山公司余额都大于订单金额
					moneyNeedPay = 0d;
					moneyFeePay = 0d;
					if("1".equals(ticketIndentFdb.getTelNumber()) || "05".equals(ticketIndentFdb.getIndentType()) || isCheck==false){//代散客购票或者购买项目票或者索道套票
						ticketIndentFdb.setStatus("03");//团客已支付
					}
					else{
						ticketIndentFdb.setStatus("02");//团客待审核
					}
					ticketIndentDao.update(ticketIndentFdb);
					
					//用冲退替换删除
					baseRechargeRecService.saveReturnBackInfo(ticketIndentFdb, baseTravelAgency, null);
					
					baseTravelAgency.setCashBalance(cashBalanceJoin-(indPriceJoint-prepaidJoint));//设置现金余额为现有余额减去订单总金额
					baseTravelAgency.setCashBalanceSnow(cashBalanceSnow-(indPriceSnow-prepaidSnow));
					baseTravelAgencyDao.update(baseTravelAgency);//更新旅行社数据
					
					
					//产生一条充值记录（支付）
					
					BaseRechargeRec baseRechargeRec = new BaseRechargeRec();
					baseRechargeRec.setBaseTravelAgency(baseTravelAgency);
					baseRechargeRec.setTicketIndent(ticketIndentFdb);
					baseRechargeRec.setSourceDescription("支付票订单");
					baseRechargeRec.setCashMoney(0-indPriceJoint);//支出,充值为负
					baseRechargeRec.setCashBalance(baseTravelAgency.getCashBalance());
					baseRechargeRec.setCashMoneySnow(0-indPriceSnow);//支出,充值为负
					baseRechargeRec.setCashBalanceSnow(baseTravelAgency.getCashBalanceSnow());
					baseRechargeRec.setFundType("01");
					baseRechargeRec.setPaymentType("01");//支付类型为:01支付
					baseRechargeRec.setInputer(getCurrentUserId());
					baseRechargeRec.setInputTime(thisTime);
					baseRechargeRecService.save(baseRechargeRec);
					
					//此时无需调用易宝,产生一条由现金余额支付的交易记录
					
					//用冲退替换删除
					baseTranRecService.saveReturnBackInfo(ticketIndentFdb);
					
					BaseTranRec baseTranRec = new BaseTranRec();
					baseTranRec.setTicketIndent(ticketIndentFdb);
					baseTranRec.setMoney(ticketIndentFdb.getTotalPrice());
					baseTranRec.setMoneyJoint(indPriceJoint);
					baseTranRec.setMoneySnow(indPriceSnow);
					baseTranRec.setPaymentType("01");//支付类型:支付
					baseTranRec.setPayType("02");//交易类型,通过余额交易
					baseTranRec.setPaymentTime(thisTime);
					baseTranRecService.save(baseTranRec);
					
					//如果有财务记录则先冲退
					financeTicketAccountsService.saveReturnBackInfo(ticketIndentFdb, baseTranRec, baseRechargeRec);
					
					//添加财务记录
					financeTicketAccountsService.savePayInfo(ticketIndentFdb, baseTranRec, baseRechargeRec,"11", indPriceJoint, indPriceSnow);
					
					
				}else if(cashBalanceJoin<(indPriceJoint-prepaidJoint) && cashBalanceSnow>=(indPriceSnow-prepaidSnow)){//股份公司账户余额不足，需提示充现金

					double moneyNeedPayJoint = indPriceJoint-(cashBalanceJoin+prepaidJoint);
					double moneyNeedPaySnow = 0;
					moneyNeedPay = moneyNeedPayJoint + moneyNeedPaySnow;
					moneyFeePay = moneyNeedPayJoint;
					
					payMag = "当前股份公司现金余额为:"+cashBalanceJoin+"，还需网银支付："+moneyNeedPayJoint;
					
					ticketIndentDao.update(ticketIndentFdb);
					
					//用冲退替换删除
					baseRechargeRecService.saveReturnBackInfo(ticketIndentFdb, baseTravelAgency, null);
					
					baseTravelAgency.setCashBalance(0d);//设置现金余额为0
					baseTravelAgency.setCashBalanceSnow(cashBalanceSnow-(indPriceSnow-prepaidSnow));
					baseTravelAgencyDao.update(baseTravelAgency);//更新旅行社数据
					
					//用冲退替换删除
					baseTranRecService.saveReturnBackInfo(ticketIndentFdb);
					
					BaseTranRec baseTranRec = new BaseTranRec();
					//baseTranRec.setTrxId(r2_TrxId);
					baseTranRec.setTicketIndent(ticketIndentFdb);
					baseTranRec.setMoney((cashBalanceJoin+prepaidJoint)+indPriceSnow);//余额要加上预扣款才对
					baseTranRec.setMoneyJoint(cashBalanceJoin+prepaidJoint);
					baseTranRec.setMoneySnow(indPriceSnow);
					baseTranRec.setPaymentType("01");//支付类型:支付
					baseTranRec.setPayType("02");//交易类型,通过余额交易
					baseTranRec.setPaymentTime(thisTime);
					baseTranRecService.save(baseTranRec);
					
					//如使用现金帐户,则产生一条充值记录（支付）
					BaseRechargeRec baseRechargeRec = new BaseRechargeRec();
					baseRechargeRec.setBaseTravelAgency(baseTravelAgency);
					baseRechargeRec.setTicketIndent(ticketIndentFdb);
					baseRechargeRec.setSourceDescription("支付票订单");
					baseRechargeRec.setCashMoney(0-(cashBalanceJoin+prepaidJoint));//支出,充值为负//余额要加上预扣款才对
					baseRechargeRec.setCashBalance(baseTravelAgency.getCashBalance());
					baseRechargeRec.setCashMoneySnow(0-indPriceSnow);//支出,充值为负
					baseRechargeRec.setCashBalanceSnow(baseTravelAgency.getCashBalanceSnow());
					baseRechargeRec.setFundType("01");
					baseRechargeRec.setPaymentType("01");//支付类型为:01支付
					baseRechargeRec.setInputer(ticketIndentFdb.getInputer());
					baseRechargeRec.setInputTime(thisTime);
					baseRechargeRecService.save(baseRechargeRec);
					
					
					//如果有财务记录则先冲退
					financeTicketAccountsService.saveReturnBackInfo(ticketIndentFdb, baseTranRec, baseRechargeRec);
					
					//添加财务记录
					financeTicketAccountsService.savePayInfo(ticketIndentFdb, baseTranRec, baseRechargeRec,"00", indPriceJoint, indPriceSnow);
						
				}else if(cashBalanceJoin>=(indPriceJoint-prepaidJoint) && cashBalanceSnow<(indPriceSnow-prepaidSnow)){//雪山公司账户余额不足，需提示充现金
					
					double moneyNeedPayJoint = 0;
					double moneyNeedPaySnow = indPriceSnow-(cashBalanceSnow+prepaidSnow);
					moneyNeedPay = moneyNeedPayJoint + moneyNeedPaySnow;
					moneyFeePay = moneyNeedPayJoint;
					
					payMag = "当前雪山公司现金余额为:"+cashBalanceSnow+"，还需网银支付："+moneyNeedPaySnow;
					
					ticketIndentDao.update(ticketIndentFdb);
					
					//用冲退替换删除
					baseRechargeRecService.saveReturnBackInfo(ticketIndentFdb, baseTravelAgency, null);
					
					baseTravelAgency.setCashBalance(cashBalanceJoin-(indPriceJoint-prepaidJoint));//设置现金余额为0
					baseTravelAgency.setCashBalanceSnow(0d);
					baseTravelAgencyDao.update(baseTravelAgency);//更新旅行社数据
					
					//用冲退替换删除
					baseTranRecService.saveReturnBackInfo(ticketIndentFdb);
					
					BaseTranRec baseTranRec = new BaseTranRec();
					//baseTranRec.setTrxId(r2_TrxId);
					baseTranRec.setTicketIndent(ticketIndentFdb);
					baseTranRec.setMoney(indPriceJoint+(cashBalanceSnow+prepaidSnow));
					baseTranRec.setMoneyJoint(indPriceJoint);
					baseTranRec.setMoneySnow(cashBalanceSnow+prepaidSnow);
					baseTranRec.setPaymentType("01");//支付类型:支付
					baseTranRec.setPayType("02");//交易类型,通过余额交易
					baseTranRec.setPaymentTime(thisTime);
					baseTranRecService.save(baseTranRec);
					
					//如使用现金帐户,则产生一条充值记录（支付）
					BaseRechargeRec baseRechargeRec = new BaseRechargeRec();
					baseRechargeRec.setBaseTravelAgency(baseTravelAgency);
					baseRechargeRec.setTicketIndent(ticketIndentFdb);
					baseRechargeRec.setSourceDescription("支付票订单");
					baseRechargeRec.setCashMoney(0-indPriceJoint);//支出,充值为负
					baseRechargeRec.setCashBalance(baseTravelAgency.getCashBalance());
					baseRechargeRec.setCashMoneySnow(0-(cashBalanceSnow+prepaidSnow));//支出,充值为负
					baseRechargeRec.setCashBalanceSnow(baseTravelAgency.getCashBalanceSnow());
					baseRechargeRec.setFundType("01");
					baseRechargeRec.setPaymentType("01");//支付类型为:01支付
					baseRechargeRec.setInputer(ticketIndentFdb.getInputer());
					baseRechargeRec.setInputTime(thisTime);
					baseRechargeRecService.save(baseRechargeRec);
					
					//如果有财务记录则先冲退
					financeTicketAccountsService.saveReturnBackInfo(ticketIndentFdb, baseTranRec, baseRechargeRec);
					
					//添加财务记录
					financeTicketAccountsService.savePayInfo(ticketIndentFdb, baseTranRec, baseRechargeRec,"00", indPriceJoint, indPriceSnow);
					
					
				}else{//两个账户余额都不足
					
					double moneyNeedPayJoint = indPriceJoint-(cashBalanceJoin+prepaidJoint);
					double moneyNeedPaySnow = indPriceSnow-(cashBalanceSnow+prepaidSnow);
					moneyNeedPay = moneyNeedPayJoint + moneyNeedPaySnow;
					moneyFeePay = moneyNeedPayJoint;
					
					ticketIndentDao.update(ticketIndentFdb);
					
					//用冲退替换删除
					baseRechargeRecService.saveReturnBackInfo(ticketIndentFdb, baseTravelAgency, null);
					
					baseTravelAgency.setCashBalance(0d);//设置现金余额为0
					baseTravelAgency.setCashBalanceSnow(0d);
					baseTravelAgencyDao.update(baseTravelAgency);//更新旅行社数据
					
					//用冲退替换删除
					baseTranRecService.saveReturnBackInfo(ticketIndentFdb);
					
					BaseTranRec baseTranRec = new BaseTranRec();
					//baseTranRec.setTrxId(r2_TrxId);
					baseTranRec.setTicketIndent(ticketIndentFdb);
					baseTranRec.setMoney((cashBalanceJoin+prepaidJoint)+(cashBalanceSnow+prepaidSnow));
					baseTranRec.setMoneyJoint(cashBalanceJoin+prepaidJoint);
					baseTranRec.setMoneySnow(cashBalanceSnow+prepaidSnow);
					baseTranRec.setPaymentType("01");//支付类型:支付
					baseTranRec.setPayType("02");//交易类型,通过余额交易
					baseTranRec.setPaymentTime(thisTime);
					baseTranRecService.save(baseTranRec);
					
					//如使用现金帐户,则产生一条充值记录（支付）
					BaseRechargeRec baseRechargeRec = new BaseRechargeRec();
					baseRechargeRec.setBaseTravelAgency(baseTravelAgency);
					baseRechargeRec.setTicketIndent(ticketIndentFdb);
					baseRechargeRec.setSourceDescription("支付票订单");
					baseRechargeRec.setCashMoney(0-(cashBalanceJoin+prepaidJoint));//支出,充值为负
					baseRechargeRec.setCashBalance(baseTravelAgency.getCashBalance());
					baseRechargeRec.setCashMoneySnow(0-(cashBalanceSnow+prepaidSnow));//支出,充值为负
					baseRechargeRec.setCashBalanceSnow(baseTravelAgency.getCashBalanceSnow());
					baseRechargeRec.setFundType("01");
					baseRechargeRec.setPaymentType("01");//支付类型为:01支付
					baseRechargeRec.setInputer(ticketIndentFdb.getInputer());
					baseRechargeRec.setInputTime(thisTime);
					baseRechargeRecService.save(baseRechargeRec);
					
					
					//如果有财务记录则先冲退
					financeTicketAccountsService.saveReturnBackInfo(ticketIndentFdb, baseTranRec, baseRechargeRec);
					
					//添加财务记录
					financeTicketAccountsService.savePayInfo(ticketIndentFdb, baseTranRec, baseRechargeRec,"00", indPriceJoint, indPriceSnow);
				}
			}
			else{//无现金余额不做处理
				moneyNeedPay = moneyFdb;//需要支付金额等于订单金额
				moneyFeePay = indPriceJoint;
				
				//用冲退替换删除
				baseTranRecService.saveReturnBackInfo(ticketIndentFdb);
			}
		}else if(ticketIndentFdb.getPayType()!=null && ticketIndentFdb.getPayType().trim().equals("04")){//预付款支付
			
			if(prepaidJoint>0 || prepaidSnow>0){
				throw new BusinessException("该订单已使用现金余额,不允许再使用预付款支付!","");
			}
			
			if(cashTopupBalanceJoin>=(indPriceJoint-prepaidTopupJoint) && cashTopupBalanceSnow>=(indPriceSnow-prepaidTopupSnow)){//如果使用的现金余额大于等于订单总金额，股份公司余额和雪山公司余额都大于订单金额
				if("1".equals(ticketIndentFdb.getTelNumber()) || "05".equals(ticketIndentFdb.getIndentType()) || isCheck==false){//代散客购票或者购买项目票或者购买索道套票
					ticketIndentFdb.setStatus("03");//团客已支付
				}
				else{
					ticketIndentFdb.setStatus("02");//团客待审核
				}
				ticketIndentDao.update(ticketIndentFdb);
				
				//用冲退替换删除
				baseRechargeRecService.saveReturnBackInfo(ticketIndentFdb, baseTravelAgency, null);
				
				baseTravelAgency.setCashTopupBalance(cashTopupBalanceJoin-(indPriceJoint-prepaidTopupJoint));//设置现金余额为现有余额减去订单总金额
				baseTravelAgency.setCashTopupBalanceSnow(cashTopupBalanceSnow-(indPriceSnow-prepaidTopupSnow));
				baseTravelAgencyDao.update(baseTravelAgency);//更新旅行社数据
				
				
				//产生一条充值记录（支付）
				
				BaseRechargeRec baseRechargeRec = new BaseRechargeRec();
				baseRechargeRec.setBaseTravelAgency(baseTravelAgency);
				baseRechargeRec.setTicketIndent(ticketIndentFdb);
				baseRechargeRec.setSourceDescription("支付票订单");
				baseRechargeRec.setCashMoney(0-indPriceJoint);//支出,充值为负
				baseRechargeRec.setCashBalance(baseTravelAgency.getCashTopupBalance());
				baseRechargeRec.setCashMoneySnow(0-indPriceSnow);//支出,充值为负
				baseRechargeRec.setCashBalanceSnow(baseTravelAgency.getCashTopupBalanceSnow());
				baseRechargeRec.setFundType("03");
				baseRechargeRec.setPaymentType("01");//支付类型为:01支付
				baseRechargeRec.setInputer(getCurrentUserId());
				baseRechargeRec.setInputTime(thisTime);
				baseRechargeRecService.save(baseRechargeRec);
				
				//用冲退替换删除
				baseTranRecService.saveReturnBackInfo(ticketIndentFdb);
				
				BaseTranRec baseTranRec = new BaseTranRec();
				baseTranRec.setTicketIndent(ticketIndentFdb);
				baseTranRec.setMoney(ticketIndentFdb.getTotalPrice());
				baseTranRec.setMoneyJoint(indPriceJoint);
				baseTranRec.setMoneySnow(indPriceSnow);
				baseTranRec.setPaymentType("01");//支付类型:支付
				baseTranRec.setPayType("03");//交易类型,通过余额交易
				baseTranRec.setPaymentTime(thisTime);
				baseTranRecService.save(baseTranRec);
				
				//如果有财务记录则先冲退
				financeTicketAccountsService.saveReturnBackInfo(ticketIndentFdb, baseTranRec, baseRechargeRec);
				
				//添加财务记录
				financeTicketAccountsService.savePayInfo(ticketIndentFdb, baseTranRec, baseRechargeRec,"11", indPriceJoint, indPriceSnow);
				
			}else if(cashTopupBalanceJoin<(indPriceJoint-prepaidTopupJoint) && cashTopupBalanceSnow>=(indPriceSnow-prepaidTopupSnow)){//股份公司账户余额不足，需提示充现金
				throw new BusinessException("当前索道预付款余额为:"+cashTopupBalanceJoin+",已不足以支付此订单,请选择其他支付方式!","");
			}else if(cashTopupBalanceJoin>=(indPriceJoint-prepaidTopupJoint) && cashTopupBalanceSnow<(indPriceSnow-prepaidTopupSnow)){//雪山公司账户余额不足，需提示充现金
				throw new BusinessException("当前门票预付款余额为:"+cashTopupBalanceSnow+",已不足以支付此订单,请选择其他支付方式!","");
			}else{//两个账户余额都不足
				throw new BusinessException("当前预付款余额为:"+(cashTopupBalanceJoin+cashTopupBalanceSnow)+",已不足以支付此订单,请选择其他支付方式!","");
			}
			
		}else{//使用奖励金余额付款方式
			
			//2014-02-10已选择现金交易并扣除了现金余额的不允许改为奖励款支付。
			if(prepaidJoint>0 || prepaidSnow>0){
				throw new BusinessException("该订单已使用现金余额,不允许再使用奖励款支付!","");
			}
			if(prepaidTopupJoint>0 || prepaidTopupSnow>0){
				throw new BusinessException("该订单已使用预付款余额,不允许再使用奖励款支付!","");
			}
			//旧代码2013-04-25以前 end
			BaseRewardAccount bra = baseRewardAccountDao.findAccount(baseTravelAgency.getTravelAgencyId(), "30",Timestamp.valueOf(ticketIndentFdb.getUseDate().toString()), Timestamp.valueOf(ticketIndentFdb.getUseDate().toString()));
			// 2014-07-08增加奖励款无记录的判断  start tt
			if(bra == null){
				throw new BusinessException("当前奖励款余额为:0,已不足以支付此订单,请选择其他支付方式!","");
			}
			// 2014-07-08增加奖励款无记录的判断  end tt
			double balanceJoint = bra.getRewardBalance() == null ? 0 : bra.getRewardBalance();
			double balanceSnow = bra.getRewardBalanceSnow() == null ? 0 : bra.getRewardBalanceSnow();
			
			if(balanceJoint>=indPriceJoint && balanceSnow>=indPriceSnow){//奖励金余额必须大于等于订单金额,股份公司和雪山公司都必须大于订单金额
				if("1".equals(ticketIndentFdb.getTelNumber()) || "05".equals(ticketIndentFdb.getIndentType()) || isCheck==false){//代散客购票或者购买项目票或者购买索道套票
					ticketIndentFdb.setStatus("03");//团客已支付
				} 
				else{
					ticketIndentFdb.setStatus("02");//未审核
				}
				ticketIndentDao.update(ticketIndentFdb);
				
				//用冲退替换删除
				baseRechargeRecService.saveReturnBackInfo(ticketIndentFdb, baseTravelAgency, bra);
				
				bra.setRewardBalance(balanceJoint-indPriceJoint);//设置奖励金余额
				bra.setRewardBalanceSnow(balanceSnow-indPriceSnow);
				baseRewardAccountDao.update(bra);//更新旅行社奖励款帐户数据
				
				//产生一条充值记录（支付）
				BaseRechargeRec baseRechargeRec = new BaseRechargeRec();
				baseRechargeRec.setBaseTravelAgency(baseTravelAgency);
				baseRechargeRec.setTicketIndent(ticketIndentFdb);
				baseRechargeRec.setSourceDescription("支付票订单");
				baseRechargeRec.setRewardMoney(0-indPriceJoint);//支出,充值为负
				baseRechargeRec.setRewardBalance(bra.getRewardBalance());
				baseRechargeRec.setRewardMoneySnow(0-indPriceSnow);//支出,充值为负
				baseRechargeRec.setRewardBalanceSnow(bra.getRewardBalanceSnow());
				baseRechargeRec.setBaseRewardAccount(bra);
				baseRechargeRec.setFundType("02");
				baseRechargeRec.setPaymentType("01");//支付类型为:01支付
				baseRechargeRec.setInputer(getCurrentUserId());
				baseRechargeRec.setInputTime(thisTime);
				baseRechargeRecService.save(baseRechargeRec);
				
				//如果有财务记录则先冲退
				financeTicketAccountsService.saveReturnBackInfo(ticketIndentFdb, null, baseRechargeRec);
				
				//添加财务记录
				financeTicketAccountsService.savePayInfo(ticketIndentFdb, null, baseRechargeRec,"11", indPriceJoint, indPriceSnow);
			}else if(balanceJoint>=indPriceJoint && balanceSnow<indPriceSnow){//雪山公司余额不足
				throw new BusinessException("当前门票奖励款余额为:"+balanceSnow+",已不足以支付此订单,请选择其他支付方式!","");
			}else if(balanceJoint<indPriceJoint && balanceSnow>=indPriceSnow){//股份公司余额不足
				throw new BusinessException("当前索道奖励款余额为:"+balanceJoint+",已不足以支付此订单,请选择其他支付方式!","");
			}else{//金额不足
				throw new BusinessException("当前奖励款余额为:"+(balanceJoint+balanceSnow)+",已不足以支付此订单,请选择其他支付方式!","");
			}
			
		}
		
		PayParams payParams = new PayParams();
		
		if(CommonMethod.format2db(moneyNeedPay).equals("0.00")){//无需易宝返回成功信息,由action判断
			if(ticketIndentFdb.getIndentType()!=null && "05".equals(ticketIndentFdb.getIndentType())){
				//发送成功短信
				String content = "尊敬的客户您好，本次交易已成功。请在有效的游玩日期凭身份证到现场进行换票，"+"订单号为："+
						ticketIndentFdb.getIndentCode()+"。身份证是您唯一的取票凭证，请确保携带。";
				CommonMethod.sendSms(ticketIndentFdb.getPhoneNumber(), content,sysParamDao.findSysParam("USE_NEW_PLATFROM"));
				SysUser sysUser = sysUserDao.findById(getCurrentUserId());
				if(sysUser.getMailbox() != null && !sysUser.getMailbox().isEmpty()){
					String mailCon = "本次交易已成功。请在有效的游玩日期凭身份证到现场进行换票，"+"订单号为："+
							ticketIndentFdb.getIndentCode()+"。身份证是您唯一的交易凭证，请确保携带。";
					CommonMethod.sendMail(sysUser.getMailbox(),
							sysUser.getUserName(), 
							mailCon);
				}
			}else{
				//发送成功短信
				String content ="";
				String mailCon ="";
				if(isAct){
					content = "尊敬的客户您好，本次交易已成功。请在有效的游玩日期凭二代身份证原件到现场进行换票，"+"订单号为："+
					ticketIndentFdb.getIndentCode()+"。二代身份证原件是您唯一的取票凭证，请确保携带。";
					
					mailCon = "本次交易已成功。请在有效的游玩日期凭二代身份证原件到现场进行换票，"+"订单号为："+
					ticketIndentFdb.getIndentCode()+"。二代身份证原件是您唯一的交易凭证，请确保携带。";
				}else{
					content = "尊敬的客户您好，本次交易已成功。请在有效的游玩日期凭身份证和验证码到现场进行换票，此次验证码为："+
					ticketIndentFdb.getCheckCode()+"，订单号为："+
					ticketIndentFdb.getIndentCode()+"。验证码是您唯一的交易凭证，请妥善保管，如有泄漏，后果自负。";
					
					mailCon = "本次交易已成功。请在有效的游玩日期凭身份证和验证码到现场进行换票，此次验证码为："+
					ticketIndentFdb.getCheckCode()+"，订单号为："+
					ticketIndentFdb.getIndentCode()+"。验证码是您唯一的交易凭证，请妥善保管，如有泄漏，后果自负。";
				}
//				String content = "尊敬的客户您好，本次交易已成功。请在有效的游玩日期凭身份证和验证码到现场进行换票，此次验证码为："+
//						ticketIndentFdb.getCheckCode()+"，订单号为："+
//						ticketIndentFdb.getIndentCode()+"。验证码是您唯一的交易凭证，请妥善保管，如有泄漏，后果自负。";
				CommonMethod.sendSms(ticketIndentFdb.getPhoneNumber(), content,sysParamDao.findSysParam("USE_NEW_PLATFROM"));
				SysUser sysUser = sysUserDao.findById(getCurrentUserId());
				if(sysUser.getMailbox() != null && !sysUser.getMailbox().isEmpty()){
//					String mailCon = "本次交易已成功。请在有效的游玩日期凭身份证和验证码到现场进行换票，此次验证码为："+
//							ticketIndentFdb.getCheckCode()+"，订单号为："+
//							ticketIndentFdb.getIndentCode()+"。验证码是您唯一的交易凭证，请妥善保管，如有泄漏，后果自负。";
					CommonMethod.sendMail(sysUser.getMailbox(),
							sysUser.getUserName(), 
							mailCon);
				}	
			}
			payParams.setIsUsePay(false);//未使用网银	
			return payParams;
		}
		
		
		
		//生成易宝交易信息
		payFactoryService.init();
		payParams = payFactoryService.initPayParams(ticketIndentFdb,moneyNeedPay,moneyFeePay);
		payParams.setPayMsg(payMag);
		payParams.setPay_test(sysParamDao.findSysParam("PAY_TEST"));
		
		//生成现金交易记录
		BaseTranRec baseTranRec = new BaseTranRec();
		baseTranRec.setTicketIndent(ticketIndentFdb);
		baseTranRec.setMoney(moneyNeedPay);
		baseTranRec.setMoneyJoint(moneyFeePay);
		baseTranRec.setMoneySnow(moneyNeedPay-moneyFeePay);
		baseTranRec.setPaymentType("01");//支付类型支付
		baseTranRec.setPayType("01");//交易类型,通过易宝交易
		baseTranRec.setPaymentTime(thisTime);
		baseTranRec.setPaymentAccount("01");
		baseTranRec.setRoyaltyParameters(payParams.getRoyalty_parameters());
		baseTranRecService.save(baseTranRec);
		
		
		return payParams;
	}

	@Override
	public void saveFinMsg(TicketIndent ticketIndent){
		
		Timestamp thisTime = CommonMethod.getTimeStamp();
		TicketIndent ticketIndentFdb = ticketIndentDao.findById(ticketIndent.getIndentId());
		
		Long userId = getCurrentUserId();
		Long roleId = 0L;
		if(userId==null){
			userId = 0L;
			DetachedCriteria dcSU = DetachedCriteria.forClass(SysRole.class);
			dcSU.add(Property.forName("roleCode").eq("003"));
			List<SysRole> listSR = sysRoleDao.findByCriteria(dcSU);
			for(SysRole sr : listSR){
				roleId = sr.getRoleId();
			}
		}else{
			DetachedCriteria dcSU = DetachedCriteria.forClass(SysUserRole.class);
			dcSU.add(Property.forName("sysUser.userId").eq(userId));
			List<SysUserRole> listSU = sysUserRoleDao.findByCriteria(dcSU);
			for(SysUserRole sr : listSU){
				roleId = sr.getSysRole().getRoleId();
			}
		}
		
		DetachedCriteria dc = DetachedCriteria.forClass(TicketIndentDetail.class);
		dc.add(Property.forName("ticketIndent.indentId").eq(ticketIndent.getIndentId()));
		List<TicketIndentDetail> tdList = ticketIndentDetailService.findByCriteria(dc);
		
		for(TicketIndentDetail tid : tdList){
			tid.setBuyType(ticketIndentFdb.getTelNumber());
			ticketIndentDetailService.checkTopLimit(ticketIndentFdb, tid, "02", ticketIndentFdb.getIsOutbound(), false, userId, roleId, false);
		}
		
		ticketIndentFdb.setPayType(ticketIndent.getPayType());
		
		ticketIndentFdb.setUpdater(getCurrentUserId());
		ticketIndentFdb.setUpdateTime(thisTime);
		
		//计算订单分账
		Map<String,Double> priceMap = calcPrice(ticketIndent.getIndentId());
		//股份公司金额
		double indPriceJoint = priceMap.get("joint");
		//雪山公司金额
		double indPriceSnow = priceMap.get("snow");
		
		//如果有财务记录则先冲退
		financeTicketAccountsService.saveReturnBackInfo(ticketIndentFdb, null, null);
		
		//添加财务记录
		financeTicketAccountsService.savePayInfo(ticketIndentFdb, null, null,"11", indPriceJoint, indPriceSnow);
		boolean isAct = false;
		for(TicketIndentDetail tid : tdList){
			TicketPrice tps = ticketPriceDao.findById(tid.getTicketPrice().getTicketPriceId());
			//索道套票
			if("01".equals(tps.getTicketType().getActivity())){
				isAct =  true;
			}
		}
		//发送成功短信
		String content ="";
		String mailCon ="";
		if(isAct){
			content = "尊敬的客户您好，本次交易已成功。请在有效的游玩日期凭二代身份证原件到现场进行换票，"+"订单号为："+
			ticketIndentFdb.getIndentCode()+"。二代身份证原件是您唯一的取票凭证，请确保携带。";
			
			mailCon = "本次交易已成功。请在有效的游玩日期凭二代身份证原件到现场进行换票，"+"订单号为："+
			ticketIndentFdb.getIndentCode()+"。二代身份证原件是您唯一的交易凭证，请确保携带。";
		}else{
			content = "尊敬的客户您好，本次交易已成功。请在有效的游玩日期凭身份证和验证码到现场进行换票，此次验证码为："+
			ticketIndentFdb.getCheckCode()+"，订单号为："+
			ticketIndentFdb.getIndentCode()+"。验证码是您唯一的交易凭证，请妥善保管，如有泄漏，后果自负。";
			
			mailCon = "本次交易已成功。请在有效的游玩日期凭身份证和验证码到现场进行换票，此次验证码为："+
			ticketIndentFdb.getCheckCode()+"，订单号为："+
			ticketIndentFdb.getIndentCode()+"。验证码是您唯一的交易凭证，请妥善保管，如有泄漏，后果自负。";
		}
		
		//发送成功短信
//		String content = "尊敬的客户您好，本次交易已成功。请在有效的游玩日期凭身份证和验证码到现场进行换票，此次验证码为："+
//				ticketIndentFdb.getCheckCode()+"，订单号为："+
//				ticketIndentFdb.getIndentCode()+"。验证码是您唯一的交易凭证，请妥善保管，如有泄漏，后果自负。";
		CommonMethod.sendSms(ticketIndentFdb.getPhoneNumber(), content,sysParamDao.findSysParam("USE_NEW_PLATFROM"));
		SysUser sysUser = sysUserDao.findById(getCurrentUserId());
		if(sysUser.getMailbox() != null && !sysUser.getMailbox().isEmpty()){
//			String mailCon = "本次交易已成功。请在有效的游玩日期凭身份证和验证码到现场进行换票，此次验证码为："+
//					ticketIndentFdb.getCheckCode()+"，订单号为："+
//					ticketIndentFdb.getIndentCode()+"。验证码是您唯一的交易凭证，请妥善保管，如有泄漏，后果自负。";
			CommonMethod.sendMail(sysUser.getMailbox(),
					sysUser.getUserName(), 
					mailCon);
		}		
		
	}
	
	@Override 
	public JsonPager<TicketIndent> findIndentList(JsonPager<TicketIndent> jp,TicketIndent t,String forPage) {
		//jp.setTotal(ticketIndentDao.findIndentListCount(t, getCurrentUserId(),jp,forPage));
		//jp.setRoot(ticketIndentDao.findIndentList(t, getCurrentUserId(),jp,forPage));
		
		long userId = getCurrentUserId();
		if("rForMKT".equals(forPage)){
			userId = t.getInputer();
		}
		
		return ticketIndentDao.findIndent(t, userId, jp, forPage);
	}
	
	@Override 
	public Map<String,Double> findIndentSums(JsonPager<TicketIndent> jp,TicketIndent t,String forPage) {
		return ticketIndentDao.findIndentSums(t, getCurrentUserId(),jp,forPage);
	}

	@Override
	public boolean findPayResult(String hmac, String p1_MerId, String r0_Cmd,
			String r1_Code, String r2_TrxId, String r3_Amt, String r4_Cur,
			String r5_Pid, String r6_Order, String r7_Uid, String r8_MP,
			String r9_BType) {
		String keyValue = findSysParam("KEY_VALUE");
		/*return(PaymentForOnlineService.verifyCallback(hmac, p1_MerId,
				r0_Cmd, r1_Code, r2_TrxId, r3_Amt,
				r4_Cur, r5_Pid, r6_Order, r7_Uid,
				r8_MP, r9_BType, keyValue ));*/
		return true;
	}

	@Override
	public void savePayResult(TicketIndent ticketIndent, String r2_TrxId,
			String r3_Amt, String rp_PayDate, String out_trade_no,boolean isAct) {
		//更新订单
		if(ticketIndent.getCustomerType().equals(findSysParam("CUSTOMER_TYPE_GROUP"))){//团客
			if("1".equals(ticketIndent.getTelNumber()) || isAct==true){//代散客购票或者购买索道套票
				ticketIndent.setStatus("03");//已支付
			}
			else{
				ticketIndent.setStatus("02");//未审核
			}
		}
		else{
			ticketIndent.setStatus("03");//已支付
		}
		double perFee = Double.parseDouble(findSysParam("PER_FEE"));
		
		ticketIndent.setPayType("02");
		ticketIndentDao.update(ticketIndent);//更新订单
		
		//将预扣款记录改为已支付未消费记录
		financeTicketAccountsService.savePayToPaid(ticketIndent);
		
		//修改现金交易记录
		DetachedCriteria dcBt = DetachedCriteria.forClass(BaseTranRec.class);
		dcBt.add(Property.forName("ticketIndent.indentId").eq(ticketIndent.getIndentId()));
		dcBt.add(Property.forName("paymentType").in(new String[]{"01","03"}));
		dcBt.add(Property.forName("payType").eq("01"));
		List<BaseTranRec> btsList = baseTranRecService.findByCriteria(dcBt);
		
		List<BaseTranRec> btList = baseTranRecService.excludeReturnBackInfo(btsList);
		
		for(BaseTranRec bt : btList){
			bt.setTrxId(r2_TrxId);
			bt.setPerFee(perFee);
			bt.setBatchNo(out_trade_no);
			bt.setPayFee(perFee*Double.parseDouble(r3_Amt));
			bt.setPaymentTime(CommonMethod.string2Time3(rp_PayDate));
			baseTranRecService.update(bt);
			
			//添加财务记录
			financeTicketAccountsService.savePayInfo(ticketIndent, bt, null,"11", 0d, 0d);
		}
		
		
		/**生成交易记录
		BaseTranRec baseTranRec = new BaseTranRec();
		baseTranRec.setTrxId(r2_TrxId);
		baseTranRec.setTicketIndent(ticketIndent);
		baseTranRec.setMoney(Double.parseDouble(r3_Amt));
		baseTranRec.setPaymentType("01");//支付类型支付
		baseTranRec.setPayType("01");//交易类型,通过易宝交易
		baseTranRec.setPerFee(perFee);
		baseTranRec.setBatchNo(out_trade_no);
		baseTranRec.setPayFee(perFee*Double.parseDouble(r3_Amt));
		baseTranRec.setPaymentTime(CommonMethod.string2Time3(rp_PayDate));
		baseTranRec.setPaymentAccount("01");
		baseTranRecService.save(baseTranRec);**/
		
	}

	@Override
	public List<TicketIndent> findIndentForPayResult(String r6_Order) {
		
		/**2014-02-11解决进入支付宝页面未支付，金额改变后再次进入支付宝页面时报“交易金额与交易中不一致”的错误
		 * Out_trade_no改为  ‘indentCode’ 加 ‘_’ 加 ‘时间戳’
		 */
		String indentCode = r6_Order.substring(0, r6_Order.indexOf("_"));
		
		DetachedCriteria dc = DetachedCriteria.forClass(TicketIndent.class);
		dc.add(Property.forName("indentCode").eq(indentCode));
		List<TicketIndent> list = ticketIndentDao.findByCriteria(dc);
		return list;
	}

	@Override
	public PayParams savePayParamsForTourist(TicketIndent ticketIndent) {
		String dB = ticketIndent.getDefaultbank();
		String remark = ticketIndent.getRemark();
		ticketIndent = ticketIndentDao.findById(ticketIndent.getIndentId());
		ticketIndent.setDefaultbank(dB);
		ticketIndent.setRemark(remark);
		
		//计算订单分账
		Map<String,Double> priceMap = calcPrice(ticketIndent.getIndentId());
		//股份公司金额
		double indPriceJoint = priceMap.get("joint");
		//雪山公司金额
		double indPriceSnow = priceMap.get("snow");
		
		/*Long userId = getCurrentUserId();
		Long roleId = 0L;
		if(userId==null){
			userId = 0L;
			DetachedCriteria dcSU = DetachedCriteria.forClass(SysRole.class);
			dcSU.add(Property.forName("roleCode").eq("003"));
			List<SysRole> listSR = sysRoleDao.findByCriteria(dcSU);
			for(SysRole sr : listSR){
				roleId = sr.getRoleId();
			}
			
		}else{
			DetachedCriteria dcSU = DetachedCriteria.forClass(SysUserRole.class);
			dcSU.add(Property.forName("sysUser.userId").eq(userId));
			List<SysUserRole> listSU = sysUserRoleDao.findByCriteria(dcSU);
			for(SysUserRole sr : listSU){
				roleId = sr.getSysRole().getRoleId();
			}
		}
		
		DetachedCriteria dc = DetachedCriteria.forClass(TicketIndentDetail.class);
		dc.createAlias("ticketPrice", "tp",DetachedCriteria.INNER_JOIN);
		dc.createAlias("tp.ticketType", "tt",DetachedCriteria.INNER_JOIN);
		dc.add(Property.forName("ticketIndent.indentId").eq(ticketIndent.getIndentId()));
		List<TicketIndentDetail> tdList = ticketIndentDetailDao.findByCriteria(dc);
		
		for(TicketIndentDetail tid : tdList){
			tid.setBuyType(ticketIndent.getTelNumber());
			tid.setRemark(ticketIndent.getRemark());
			ticketIndentDetailService.checkTopLimit(ticketIndent, tid, "02", "", false, userId, roleId, false);
		} */
		
		
		PayParams payParams = new PayParams();
		Double moneyNeedPay = 0d;//需要支付金额
		moneyNeedPay = ticketIndent.getTotalPrice();
		if(CommonMethod.format2db(moneyNeedPay).equals("0.00")){//无需易宝返回成功信息,由action判断
			ticketIndent.setPayType("02");//网上支付
			payParams.setIsUsePay(false);//未使用网银支付
			return payParams;
		}
		
		//生成易宝交易信息
		payFactoryService.init();
		payParams = payFactoryService.initPayParams(ticketIndent,moneyNeedPay,indPriceJoint);
		payParams.setPay_test(sysParamDao.findSysParam("PAY_TEST"));
		//生成现金交易记录
//		DetachedCriteria dcBt = DetachedCriteria.forClass(BaseTranRec.class);
//		dcBt.add(Property.forName("ticketIndent.indentId").eq(ticketIndent.getIndentId()));
//		dcBt.add(Property.forName("paymentType").eq("01"));
//		dcBt.add(Property.forName("payType").eq("01"));
//		List<BaseTranRec> btList = baseTranRecService.findByCriteria(dcBt);
//		for(BaseTranRec bt : btList){
//			baseTranRecService.delete(bt);
//		}
		//用冲退替换删除
		baseTranRecService.saveReturnBackInfo(ticketIndent);
		
		BaseTranRec baseTranRec = new BaseTranRec();
		baseTranRec.setTicketIndent(ticketIndent);
		baseTranRec.setMoney(moneyNeedPay);
		baseTranRec.setMoneyJoint(indPriceJoint);
		baseTranRec.setMoneySnow(indPriceSnow);
		baseTranRec.setPaymentType("01");//支付类型支付
		baseTranRec.setPayType("01");//交易类型,通过易宝交易
		baseTranRec.setPaymentTime(CommonMethod.getTimeStamp());
		baseTranRec.setPaymentAccount("01");
		baseTranRec.setRoyaltyParameters(payParams.getRoyalty_parameters());
		baseTranRecService.save(baseTranRec);
		
		return payParams;
	}

	@Override
	public void updateForClose(TicketIndent ticketIndent) {
		
		//查询订单已扣现金余额,在交易记录中查询
		DetachedCriteria dcBtr = DetachedCriteria.forClass(BaseTranRec.class);
		dcBtr.add(Property.forName("ticketIndent.indentId").eq(ticketIndent.getIndentId()));
		dcBtr.add(Property.forName("paymentType").in(new String[]{"01","03"}));
		dcBtr.add(Property.forName("payType").ne("01"));//排除网银支付的记录
		List<BaseTranRec> btrsList = baseTranRecService.findByCriteria(dcBtr);
		
		List<BaseTranRec> btrList = baseTranRecService.excludeReturnBackInfo(btrsList);
		
		double prepaid = 0;
		//股份公司预扣款
		double prepaidJoint = 0;
		//雪山公司预扣款
		double prepaidSnow = 0;
		
		double prepaidTopup = 0;
		double prepaidTopupJoint = 0;
		double prepaidTopupSnow = 0;
		
		for(BaseTranRec btr : btrList){
			String pType  = btr.getPayType();
			if("03".equals(pType)){//预付款支付
				prepaidTopup = prepaidTopup + (btr.getMoney()==null ? 0d : btr.getMoney());
				prepaidTopupJoint = prepaidTopupJoint + (btr.getMoneyJoint()==null ? 0d : btr.getMoneyJoint());
				prepaidTopupSnow = prepaidTopupSnow + (btr.getMoneySnow()==null ? 0d : btr.getMoneySnow());
			}else{//余额付款
				prepaid = prepaid + (btr.getMoney()==null ? 0d : btr.getMoney());
				prepaidJoint = prepaidJoint + (btr.getMoneyJoint()==null ? 0d : btr.getMoneyJoint());
				prepaidSnow = prepaidSnow + (btr.getMoneySnow()==null ? 0d : btr.getMoneySnow());
			}
		}
		
		if(prepaid>0){//存在预付款的,需退回余额中
			Travservice baseTravelAgency = baseTravelAgencyService.findTravelAgencyByUser(getCurrentUserId());
			baseTravelAgency.setCashBalance(prepaidJoint+baseTravelAgency.getCashBalance());//增加余额
			baseTravelAgency.setCashBalanceSnow(prepaidSnow+baseTravelAgency.getCashBalanceSnow());
			
			ticketIndent.setPrepaid(0d);//清空订单中预支付金额
			baseTravelAgencyDao.update(baseTravelAgency);//保存余额账户
			//产生一条充值记录（支付）
			BaseRechargeRec baseRechargeRec = new BaseRechargeRec();
			baseRechargeRec.setBaseTravelAgency(baseTravelAgency);
			baseRechargeRec.setTicketIndent(ticketIndent);
			baseRechargeRec.setSourceDescription("取消订单退现金余额");
			baseRechargeRec.setCashMoney(prepaidJoint);//支出,充值为负
			baseRechargeRec.setCashBalance(baseTravelAgency.getCashBalance());
			baseRechargeRec.setCashMoneySnow(prepaidSnow);
			baseRechargeRec.setCashBalanceSnow(baseTravelAgency.getCashBalanceSnow());
			baseRechargeRec.setFundType("01");
			baseRechargeRec.setPaymentType("02");//支付类型为:01支付
			baseRechargeRec.setInputer(getCurrentUserId());
			baseRechargeRec.setInputTime(CommonMethod.getTimeStamp());
			baseRechargeRecService.save(baseRechargeRec);
			
			//此时无需调用易宝,产生一条由现金余额支付的交易记录
			BaseTranRec baseTranRec = new BaseTranRec();
			baseTranRec.setTicketIndent(ticketIndent);
			baseTranRec.setMoney(-prepaid);
			baseTranRec.setMoneyJoint(-prepaidJoint);
			baseTranRec.setMoneySnow(-prepaidSnow);
			baseTranRec.setPaymentType("02");//支付类型:支付
			baseTranRec.setPayType("02");//交易类型,通过余额交易
			baseTranRec.setPaymentTime(CommonMethod.getTimeStamp());
			baseTranRecService.save(baseTranRec);
			
			//冲退财务记录
			financeTicketAccountsService.saveReturnBackInfo(ticketIndent, baseTranRec, baseRechargeRec);
		}
		
		if(prepaidTopup>0){//存在预付款的,需退回余额中
			Travservice baseTravelAgency = baseTravelAgencyService.findTravelAgencyByUser(getCurrentUserId());
			baseTravelAgency.setCashTopupBalance(prepaidTopupJoint+baseTravelAgency.getCashTopupBalance());//增加余额
			baseTravelAgency.setCashTopupBalanceSnow(prepaidTopupSnow+baseTravelAgency.getCashTopupBalanceSnow());
			
			ticketIndent.setPrepaid(0d);//清空订单中预支付金额
			baseTravelAgencyDao.update(baseTravelAgency);//保存余额账户
			//产生一条充值记录（支付）
			BaseRechargeRec baseRechargeRec = new BaseRechargeRec();
			baseRechargeRec.setBaseTravelAgency(baseTravelAgency);
			baseRechargeRec.setTicketIndent(ticketIndent);
			baseRechargeRec.setSourceDescription("取消订单退现金余额");
			baseRechargeRec.setCashMoney(prepaidTopupJoint);//支出,充值为负
			baseRechargeRec.setCashBalance(baseTravelAgency.getCashTopupBalance());
			baseRechargeRec.setCashMoneySnow(prepaidTopupSnow);
			baseRechargeRec.setCashBalanceSnow(baseTravelAgency.getCashTopupBalanceSnow());
			baseRechargeRec.setFundType("03");
			baseRechargeRec.setPaymentType("02");//支付类型为:01支付
			baseRechargeRec.setInputer(getCurrentUserId());
			baseRechargeRec.setInputTime(CommonMethod.getTimeStamp());
			baseRechargeRecService.save(baseRechargeRec);
			
			//此时无需调用易宝,产生一条由现金余额支付的交易记录
			BaseTranRec baseTranRec = new BaseTranRec();
			baseTranRec.setTicketIndent(ticketIndent);
			baseTranRec.setMoney(-prepaidTopup);
			baseTranRec.setMoneyJoint(-prepaidTopupJoint);
			baseTranRec.setMoneySnow(-prepaidTopupSnow);
			baseTranRec.setPaymentType("02");//支付类型:支付
			baseTranRec.setPayType("03");//交易类型,通过余额交易
			baseTranRec.setPaymentTime(CommonMethod.getTimeStamp());
			baseTranRecService.save(baseTranRec);
			
			//冲退财务记录
			financeTicketAccountsService.saveReturnBackInfo(ticketIndent, baseTranRec, baseRechargeRec);
		}
		
		
		//修改剩余票数
		Long userId= ticketIndent.getInputer();
		Long roleId = 0L;
		if(userId==null){
			userId = 0L;
			DetachedCriteria dcSU = DetachedCriteria.forClass(SysRole.class);
			dcSU.add(Property.forName("roleCode").eq("003"));
			List<SysRole> listSR = sysRoleDao.findByCriteria(dcSU);
			for(SysRole sr : listSR){
				roleId = sr.getRoleId();
			}
			
		}else{
			DetachedCriteria dcSU = DetachedCriteria.forClass(SysUserRole.class);
			dcSU.add(Property.forName("sysUser.userId").eq(userId));
			List<SysUserRole> listSU = sysUserRoleDao.findByCriteria(dcSU);
			for(SysUserRole sr : listSU){
				roleId = sr.getSysRole().getRoleId();
			}
		}
		
		DetachedCriteria dc = DetachedCriteria.forClass(TicketIndentDetail.class);
		dc.add(Property.forName("ticketIndent.indentId").eq(ticketIndent.getIndentId()));
		List<TicketIndentDetail> tidList =  ticketIndentDetailDao.findByCriteria(dc);
		for (TicketIndentDetail ticketIndentDetail : tidList) {
				TicketPrice ticketPrice = ticketPriceDao.findById(ticketIndentDetail.getTicketPrice().getTicketPriceId());
				if(ticketPrice.getRemainToplimit() != null && ticketPrice.getToplimit() !=null){
					int remainToplimit = ticketPrice.getRemainToplimit() == null ? 0 : ticketPrice.getRemainToplimit();
					remainToplimit = remainToplimit + ticketIndentDetail.getAmount();
					ticketPrice.setRemainToplimit(remainToplimit);
					ticketPriceDao.update(ticketPrice);
				}
				
				int updateNums = 0 - ticketIndentDetail.getAmount();
				ticketUserTypeService.updateReminTopLimit(userId, ticketPrice.getTicketType().getTicketId(), updateNums);
				ticketRoleTypeService.updateReminTopLimit(roleId, ticketPrice.getTicketType().getTicketId(), updateNums);
		}
		
		ticketIndent.setStatus("07");//订单状态置为已取消
		ticketIndent.setUpdater(getCurrentUserId());
		ticketIndent.setUpdateTime(CommonMethod.getTimeStamp());
		ticketIndentDao.update(ticketIndent);//保存取消订单
	}

	@Override
	public boolean findBeforChange(TicketIndent ticketIndent) {
		boolean rb = true;
		Timestamp useDate = Timestamp.valueOf(ticketIndent.getUseDate().toString());
		DetachedCriteria dc = DetachedCriteria.forClass(TicketPrice.class);
		dc.createAlias("ticketType", "ticketType");
		dc.add(Property.forName("startDate").le(CommonMethod.toEndTime(useDate)));//开始时间<=当前时间
		dc.add(Property.forName("endDate").ge(CommonMethod.toStartTime(useDate)));//结束时间>=当前时间
		dc.add(Property.forName("status").eq("11"));
		dc.add(Property.forName("ticketType.customerType").eq(findSysParam("CUSTOMER_TYPE_TOURIST")));
		dc.add(Property.forName("ticketType.status").eq("11"));
		dc.addOrder(Order.desc("unitPrice"));
		List<TicketPrice> tps = ticketPriceDao.findByCriteria(dc);
		if(tps==null || tps.isEmpty()){
			rb = false;
			return rb;
		}
		HashMap<Long, TicketPrice> tpHm = new HashMap<Long, TicketPrice>();
		for (TicketPrice ticketPrice : tps) {
			tpHm.put(ticketPrice.getTicketType().getTicketId(), ticketPrice);
		}
		
		DetachedCriteria tidDc = DetachedCriteria.forClass(TicketIndentDetail.class);
		tidDc.createAlias("ticketPrice", "ticketPrice");
		tidDc.createAlias("ticketPrice.ticketType", "ticketType");
		tidDc.add(Property.forName("ticketIndent.indentId").eq(ticketIndent.getIndentId()));
		List<TicketIndentDetail> tids = ticketIndentDetailDao.findByCriteria(tidDc);
		for (TicketIndentDetail ticketIndentDetail : tids) {
			if(tpHm.get(ticketIndentDetail.getTicketPrice().getTicketType().getTicketId())==null){
				rb = false;
				return rb;
			}else{
				
				TicketPrice tpNew = tpHm.get(ticketIndentDetail.getTicketPrice().getTicketType().getTicketId());
				
				double unPeiceNew = tpNew.getUnitPrice();
				double unPeice = ticketIndentDetail.getTicketPrice().getUnitPrice();
				
				if(unPeiceNew!=unPeice){
					rb = false;
					return rb;
				}
				
				String bigType = ticketIndentDetail.getTicketPrice().getTicketType().getTicketBigType();
				
				if("02".equals(bigType)){//项目票判断剩余数量
					
					int nums = ticketIndentDetail.getAmount();
					int rTopLimitNew = tpNew.getRemainToplimit();
					//不是同一条价格记录的情况下，剩余票数小于订票数则不能改签。
					if(ticketIndentDetail.getTicketPrice().getTicketPriceId() != tpNew.getTicketPriceId() && rTopLimitNew<nums){
						rb = false;
						return rb;
					}
					
				}
			}
		}
		
//		return false;
		return rb;
	}

	@Override
	public void saveChange(TicketIndent ticketIndent) {
		TicketIndent tiData = ticketIndentDao.findById(ticketIndent.getIndentId());
		if(!"03".equals(tiData.getStatus())){//首先判断状态
			throw new BusinessException("当前订单状态不正确,不能改签!");
		}
		if(findBeforChange(ticketIndent)){//再判断订单是否能改签到目标日期
			tiData.setUseDate(ticketIndent.getUseDate());//修改订单
			tiData.setUpdater(getCurrentUserId());
			tiData.setUpdateTime(CommonMethod.getTimeStamp());
			ticketIndentDao.update(tiData);
			
			//修改剩余票数
			Timestamp useDate = Timestamp.valueOf(ticketIndent.getUseDate().toString());
			DetachedCriteria dc = DetachedCriteria.forClass(TicketPrice.class);
			dc.createAlias("ticketType", "ticketType");
			dc.add(Property.forName("startDate").le(CommonMethod.toEndTime(useDate)));//开始时间<=当前时间
			dc.add(Property.forName("endDate").ge(CommonMethod.toStartTime(useDate)));//结束时间>=当前时间
			dc.add(Property.forName("status").eq("11"));
			dc.add(Property.forName("ticketType.customerType").eq(findSysParam("CUSTOMER_TYPE_TOURIST")));
			dc.add(Property.forName("ticketType.status").eq("11"));
			dc.addOrder(Order.desc("unitPrice"));
			List<TicketPrice> tps = ticketPriceDao.findByCriteria(dc);
			
			HashMap<Long, TicketPrice> tpHm = new HashMap<Long, TicketPrice>();
			for (TicketPrice ticketPrice : tps) {
				tpHm.put(ticketPrice.getTicketType().getTicketId(), ticketPrice);
			}
			
			DetachedCriteria tidDc = DetachedCriteria.forClass(TicketIndentDetail.class);
			tidDc.createAlias("ticketPrice", "ticketPrice");
			tidDc.createAlias("ticketPrice.ticketType", "ticketType");
			tidDc.add(Property.forName("ticketIndent.indentId").eq(ticketIndent.getIndentId()));
			List<TicketIndentDetail> tids = ticketIndentDetailDao.findByCriteria(tidDc);
			for (TicketIndentDetail ticketIndentDetail : tids) {
				
					TicketPrice tpNew = tpHm.get(ticketIndentDetail.getTicketPrice().getTicketType().getTicketId());
					TicketPrice tp = ticketIndentDetail.getTicketPrice();
					
					if(tpNew.getTicketPriceId()!=tp.getTicketPriceId()){
						int remainToplimitN = tpNew.getRemainToplimit() == null ? 0 : tpNew.getRemainToplimit();
						remainToplimitN = remainToplimitN - ticketIndentDetail.getAmount();
						tpNew.setRemainToplimit(remainToplimitN);
						ticketPriceDao.update(tpNew);
						
						int remainToplimit = tp.getRemainToplimit() == null ? 0 : tp.getRemainToplimit();
						remainToplimit = remainToplimit + ticketIndentDetail.getAmount();
						tp.setRemainToplimit(remainToplimit);
						ticketPriceDao.update(tp);
						
						ticketIndentDetail.setTicketPrice(tpNew);
						ticketIndentDetailDao.update(ticketIndentDetail);
					}
					
			}
		}
		else{
			throw new BusinessException("当前订单不能改签至目标日期!");
		}
	}
	
	/**
	 * 审核外地旅行社订单
	 */
	@Override
	public TicketIndent saveCheckSub(TicketIndent ticketIndent){
		TicketIndent tiData = ticketIndentDao.findById(ticketIndent.getIndentId());
		
		String discount = ticketIndent.getDiscount();
		double dis = Double.valueOf(discount);
		dis = dis / 10;
		
		int totalAmount = 0;
		double totalPrice = 0;
		
		DetachedCriteria dcTP = DetachedCriteria.forClass(TicketIndentDetail.class);
		dcTP.createAlias("ticketPrice", "ticketPrice");
		dcTP.createAlias("ticketPrice.ticketType", "ticketType");
		dcTP.add(Property.forName("ticketIndent.indentId").eq(ticketIndent.getIndentId()));
		//List<TicketPrice> tpOldList = ticketPriceDao.findByCriteria(dcTP);
		List<TicketIndentDetail> tids = ticketIndentDetailDao.findByCriteria(dcTP);
		for (TicketIndentDetail ticketIndentDetail : tids) {
			
//			if("04".equals(ticketIndentDetail.getTicketPrice().getTicketType().getSpecialType())){
//				ticketIndentDetail.setUnitPrice(ticketIndentDetail.getUnitPrice());
//			}else{
				ticketIndentDetail.setUnitPrice(ticketIndentDetail.getUnitPrice()*dis);
				ticketIndentDetail.setEntranceUnitPrice(ticketIndentDetail.getEntranceUnitPrice()*dis);
				ticketIndentDetail.setRopewayUnitPrice(ticketIndentDetail.getRopewayUnitPrice()*dis);
//			}
			
			
			ticketIndentDetail.setSubtotal(ticketIndentDetail.getUnitPrice()*ticketIndentDetail.getAmount());
			ticketIndentDetail.setPayFee(ticketIndentDetail.getUnitPrice()*ticketIndentDetail.getAmount()*ticketIndentDetail.getPerFee());
			
			ticketIndentDetailDao.update(ticketIndentDetail);
			
			totalAmount += ticketIndentDetail.getAmount();
			totalPrice += ticketIndentDetail.getAmount()*ticketIndentDetail.getUnitPrice();
		}
		
		tiData.setTotalAmount(totalAmount);
		tiData.setTotalPrice(totalPrice);
		tiData.setPayFee(totalPrice*tiData.getPerFee());
		tiData.setDiscount(ticketIndent.getDiscount());
		tiData.setStatus("23");
		tiData.setUpdater(getCurrentUserId());
		tiData.setUpdateTime(CommonMethod.getTimeStamp());
		ticketIndentDao.update(tiData);
		
		TicketIndentCheckLog ticl = new TicketIndentCheckLog();
		ticl.setCheckLogId(getNextKey("TICKET_INDENT_CHECK_LOG".toUpperCase(), 1));
		ticl.setTicketIndent(tiData);
		ticl.setInputer(getCurrentUserId());
		ticl.setInputTime(CommonMethod.getTimeStamp());
		ticl.setStatus("00");
		ticl.setDiscount(discount);
		ticketIndentCheckLogDao.save(ticl);
		
		String content = "旅行社用户您好，您提交的网络订单申请（"+
				tiData.getIndentCode()+"）已经审核通过，请及时付款。";
		CommonMethod.sendSms(tiData.getPhoneNumber(), content,sysParamDao.findSysParam("USE_NEW_PLATFROM"));
		SysUser sysUser = sysUserDao.findById(tiData.getInputer());
		if(sysUser.getMailbox() != null && !sysUser.getMailbox().isEmpty()){
			String mailCon = "您提交的网络订单申请（"+
					tiData.getIndentCode()+"）已经审核通过，请及时付款。";
//			BaseMessageRec mes = new BaseMessageRec(getNextKey("Base_Message_Rec", 1));
//			mes.setFunctionModule(ticketIndent.getIndentId().toString());
//			mes.setMessageContent(mailCon);
//			mes.setSendTime(CommonMethod.getTimeStamp());
//			mes.setPhoneNumber(sysUser.getMailbox());
//			baseMessageRecDao.save(mes);
			CommonMethod.sendMail(sysUser.getMailbox(),
					sysUser.getUserName(), 
					mailCon);
		}		
		
		return tiData;
	}
	
	/**
	 * 拒绝外地旅行社订单
	 */
	@Override
	public TicketIndent saveCheckNot(TicketIndent ticketIndent){
		TicketIndent tiData = ticketIndentDao.findById(ticketIndent.getIndentId());
		
		tiData.setRemark(ticketIndent.getRemark());
		tiData.setStatus("24");
		tiData.setUpdater(getCurrentUserId());
		tiData.setUpdateTime(CommonMethod.getTimeStamp());
		ticketIndentDao.update(tiData);
		
		TicketIndentCheckLog ticl = new TicketIndentCheckLog();
		ticl.setCheckLogId(getNextKey("TICKET_INDENT_CHECK_LOG".toUpperCase(), 1));
		ticl.setTicketIndent(tiData);
		ticl.setInputer(getCurrentUserId());
		ticl.setInputTime(CommonMethod.getTimeStamp());
		ticl.setStatus("11");
		ticl.setRemark(ticketIndent.getRemark());
		ticketIndentCheckLogDao.save(ticl);
		
		String content = "旅行社用户您好，您提交的网络订单申请（"+
				tiData.getIndentCode()+"）已经被拒绝，拒绝原因："+tiData.getRemark()+"，请联系西岭雪山市场部确认。";
		CommonMethod.sendSms(tiData.getPhoneNumber(), content,sysParamDao.findSysParam("USE_NEW_PLATFROM"));
		SysUser sysUser = sysUserDao.findById(tiData.getInputer());
		if(sysUser.getMailbox() != null && !sysUser.getMailbox().isEmpty()){
			String mailCon = "您提交的网络订单申请（"+
					tiData.getIndentCode()+"）已经被拒绝，拒绝原因："+tiData.getRemark()+"，请联系西岭雪山市场部确认。";
//			BaseMessageRec mes = new BaseMessageRec(getNextKey("Base_Message_Rec", 1));
//			mes.setFunctionModule(ticketIndent.getIndentId().toString());
//			mes.setMessageContent(mailCon);
//			mes.setSendTime(CommonMethod.getTimeStamp());
//			mes.setPhoneNumber(sysUser.getMailbox());
//			baseMessageRecDao.save(mes);
			CommonMethod.sendMail(sysUser.getMailbox(),
					sysUser.getUserName(), 
					mailCon);
		}		
		
		//修改剩余票数
		Long userId= ticketIndent.getInputer();
		Long roleId = 0L;
		if(userId==null){
			userId = 0L;
			DetachedCriteria dcSU = DetachedCriteria.forClass(SysRole.class);
			dcSU.add(Property.forName("roleCode").eq("003"));
			List<SysRole> listSR = sysRoleDao.findByCriteria(dcSU);
			for(SysRole sr : listSR){
				roleId = sr.getRoleId();
			}
			
		}else{
			DetachedCriteria dcSU = DetachedCriteria.forClass(SysUserRole.class);
			dcSU.add(Property.forName("sysUser.userId").eq(userId));
			List<SysUserRole> listSU = sysUserRoleDao.findByCriteria(dcSU);
			for(SysUserRole sr : listSU){
				roleId = sr.getSysRole().getRoleId();
			}
		}
		DetachedCriteria dcTI = DetachedCriteria.forClass(TicketIndentDetail.class);
		dcTI.add(Property.forName("ticketIndent.indentId").eq(ticketIndent.getIndentId()));
		List<TicketIndentDetail> tidList =  ticketIndentDetailDao.findByCriteria(dcTI);
		for (TicketIndentDetail ticketIndentDetail : tidList) {
				TicketPrice ticketPrice = ticketPriceDao.findById(ticketIndentDetail.getTicketPrice().getTicketPriceId());
				if(ticketPrice.getRemainToplimit() != null && ticketPrice.getToplimit() !=null){
					int remainToplimit = ticketPrice.getRemainToplimit() == null ? 0 : ticketPrice.getRemainToplimit();
					remainToplimit = remainToplimit + ticketIndentDetail.getAmount();
					ticketPrice.setRemainToplimit(remainToplimit);
					ticketPriceDao.update(ticketPrice);
				}
				
				int updateNums = 0 - ticketIndentDetail.getAmount();
				ticketUserTypeService.updateReminTopLimit(userId, ticketPrice.getTicketType().getTicketId(), updateNums);
				ticketRoleTypeService.updateReminTopLimit(roleId, ticketPrice.getTicketType().getTicketId(), updateNums);
		}
		
		return tiData;
	}
	
	
	/**
	 * 查询订单信息(项目票主订单)
	 */
	@Override
	public TicketIndent findProjectTicketInfo(String indentCode){
		TicketIndent tiData = new TicketIndent();
		
		DetachedCriteria tidDc = DetachedCriteria.forClass(TicketIndentDetail.class);
		tidDc.createAlias("ticketIndent", "ticketIndent");
		tidDc.createAlias("ticketPrice", "ticketPrice");
		tidDc.createAlias("ticketPrice.ticketType", "ticketType");
		tidDc.add(Property.forName("ticketIndent.indentCode").eq(indentCode));
		List<TicketIndentDetail> tids = ticketIndentDetailDao.findByCriteria(tidDc);
		
		if(tids.isEmpty() || tids.size() == 0){
			throw new BusinessException("该订单不存在，不能购买项目票。","");
		}else{
			tiData = tids.get(0).getTicketIndent();
			//带散客购票的时候，不能补买项目票
			if("01".equals(tiData.getCustomerType()) && "1".equals(tiData.getTelNumber())){
				throw new BusinessException("代散客购票的订单，不能购买项目票。","");
			}
			//订单状态为已打印或已消费以外的时候，不能补买项目票
			if("04".equals(tiData.getStatus()) || "05".equals(tiData.getStatus())){
			}else{
				throw new BusinessException("订单在未出票的状态下，不能购买项目票。","");
			}
		}
		for(TicketIndentDetail tid :tids){
			if("02".equals(tid.getTicketPrice().getTicketType().getTicketBigType())){
				throw new BusinessException("该订单已经包含项目票，不能再重复购买。","");
			}
		}
		
		String useDate =CommonMethod.getCurrentDate();
//		DetachedCriteria dc2 = DetachedCriteria.forClass(TicketBarcodeRec.class);
//		tidDc.createAlias("ticketIndentDetail", "ticketIndentDetail");
//		dc2.add(Property.forName("ticketIndentDetail.indentDetailId").eq(tids.get(0).getIndentDetailId()));
//		List<TicketBarcodeRec> dataList2 = ticketBarcodeRecDao.findByCriteria(dc2);
		
		if(CommonMethod.timeToString(Timestamp.valueOf(tiData.getUseDate().toString())).compareTo(useDate)<0){
			throw new BusinessException("该订单已过有效期，不能购买项目票。","");
		}
		
		DetachedCriteria dc = DetachedCriteria.forClass(TicketIndent.class);
		dc.add(Property.forName("parentsIndentId").eq(tids.get(0).getTicketIndent().getIndentId()));
		dc.add(Property.forName("status").ne("07"));
		dc.add(Property.forName("status").ne("06"));
		List<TicketIndent> list = ticketIndentDao.findByCriteria(dc);
		
		if(!list.isEmpty() || list.size() > 0){
			if("01".equals(list.get(0).getStatus())){
				throw new BusinessException("该订单已经购买项目票，请取消项目票订单后再购买。","");
			}else{
				throw new BusinessException("该订单已经购买项目票，不能再重复购买。","");
			}
		}
		
		
		
//		List<String> statusList = new ArrayList<String>();
////		statusList.add("02");//待审核
////		statusList.add("03");//已支付
//		statusList.add("04");//已打印
//		statusList.add("05");//已消费
//		
//		DetachedCriteria dc1 = DetachedCriteria.forClass(TicketIndent.class);
//		dc1.add(Property.forName("indentId").eq(tids.get(0).getTicketIndent().getIndentId()));
//		dc1.add(Property.forName("status").in(statusList));
//		List<TicketIndent> dataList = ticketIndentDao.findByCriteria(dc1);
//		
//		if(dataList.isEmpty() || dataList.size() == 0){
//			throw new BusinessException("该订单不能补买项目票。");
//		}else{
//			if("01".equals(dataList.get(0).getCustomerType()) && "1".equals(dataList.get(0).getTelNumber())){
//				throw new BusinessException("代散客购票的订单，不能补买项目票。");
//			}
//			String useDate =CommonMethod.getCurrentDate();
//			DetachedCriteria dc2 = DetachedCriteria.forClass(TicketBarcodeRec.class);
//			tidDc.createAlias("ticketIndentDetail", "ticketIndentDetail");
//			dc2.add(Property.forName("ticketIndentDetail.indentDetailId").eq(tids.get(0).getIndentDetailId()));
//			List<TicketBarcodeRec> dataList2 = ticketBarcodeRecDao.findByCriteria(dc2);
//			
//			if(CommonMethod.timeToString(dataList2.get(0).getPrintTime()).compareTo(useDate)<0){
//				throw new BusinessException("该订单已过有效期，不能补买项目票。");
//			}
//			tiData = dataList.get(0);
//		}
		
		return tiData;
	}
	
	/**
	 * 查询活动订单
	 */
	@Override
	public TicketIndent findActIndent(Long userId, Long activityId) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketIndent.class);
		dc.add(Property.forName("inputer").eq(userId));
		dc.add(Property.forName("activityId").eq(activityId));
		List<TicketIndent> tList = ticketIndentDao.findByCriteria(dc);
		if(tList!=null&&tList.size()>0){
			return tList.get(0);
		}else{
			return null;
		}
		
	}
	
	@Override
	public void affirmRefund(TicketIndent ticketIndent) {
		
		long userId = ticketIndent.getInputer();
		Travservice bta = baseTravelAgencyService.findTravelAgencyByUser(userId);
		if(bta.getTravelAgencyId()==null){
			throw new BusinessException("客户信息不正确，退款确认失败！","");
		}
		double cb = bta.getCashBalance()==null?0d:bta.getCashBalance();
		double cbs = bta.getCashBalanceSnow()==null?0d:bta.getCashBalanceSnow();
		if((cb+cbs)<=0d){
			throw new BusinessException("客户已没有余额，不能退款。","");
		}
		Timestamp ctime = CommonMethod.getTimeStamp();
		
		long uId = getCurrentUserId();
		
		bta.setCashBalance(0d);
		bta.setCashBalanceSnow(0d);
		bta.setUpdater(uId);
		bta.setUpdateTime(ctime);
		baseTravelAgencyDao.update(bta);
		//添加充值记录
		BaseRechargeRec baseRechargeRec = new BaseRechargeRec();
		baseRechargeRec.setRechargeRecId(getNextKey("BASE_RECHARGE_REC", 1));
		baseRechargeRec.setBaseTravelAgency(bta);
		baseRechargeRec.setSourceDescription("团客退现");
		baseRechargeRec.setCashMoney(0-cb);
		baseRechargeRec.setCashBalance(bta.getCashBalance());
		baseRechargeRec.setCashMoneySnow(0-cbs);
		baseRechargeRec.setCashBalanceSnow(bta.getCashBalanceSnow());
		baseRechargeRec.setPaymentType("01");//退款
		baseRechargeRec.setFundType("01");
		baseRechargeRec.setInputer(uId);
		baseRechargeRec.setInputTime(ctime);
		baseRechargeRecDao.save(baseRechargeRec);
		
		//添加财务记录
		financeTicketAccountsService.saveReturnInfo(baseRechargeRec);
	}
	
}
