package service.ticket.ticketYear;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.TicketUseRec;

import common.action.JsonPager;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.ticket.ticketYear.TicketUseRecDao;

@Service("ticketUseRecService")
public class TicketUseRecServiceImpl extends BaseServiceImpl<TicketUseRec>
		implements TicketUseRecService {
	@Autowired
	private TicketUseRecDao ticketUseRecDao;

	@Override
	@Resource(name = "ticketUseRecDao")
	protected void initBaseDAO(BaseDao<TicketUseRec> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public JsonPager<TicketUseRec> findJsonPageByCriteria(
			JsonPager<TicketUseRec> jp, TicketUseRec t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketUseRec.class);
		dc.add(Property.forName("ticketYear.ticketYearId").eq(t.getTicketYear().getTicketYearId()));
		dc.addOrder(Order.desc("useTime"));
		return ticketUseRecDao.findJsonPageByCriteria(jp, dc);
	}

}