package service.ticket.ticketYear;

import java.util.Collection;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.TicketYear;

import common.CommonMethod;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.ticket.ticketYear.TicketYearDao;

@Service("ticketYearService")
public class TicketYearServiceImpl extends BaseServiceImpl<TicketYear>
		implements TicketYearService {
	@Autowired
	private TicketYearDao ticketYearDao;

	@Override
	@Resource(name = "ticketYearDao")
	protected void initBaseDAO(BaseDao<TicketYear> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<TicketYear> findPageByCriteria(
			PaginationSupport<TicketYear> ps, TicketYear t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketYear.class);
		return ticketYearDao.findPageByCriteria(ps, Order.desc("ticketYearId"), dc);
	}

	@Override
	public void save(TicketYear t) {
		t.setTicketYearId(getNextKey("Ticket_Year".toUpperCase(), 1));
		ticketYearDao.save(t);
	}

	@Override
	public JsonPager<TicketYear> findJsonPageByCriteria(
			JsonPager<TicketYear> jp, TicketYear t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketYear.class);
		if(t.getUserName()!=null && !t.getUserName().trim().isEmpty()){
			dc.add(Property.forName("userName").like(t.getUserName().trim(),MatchMode.ANYWHERE));
		}
		if(t.getIdCardNumber()!=null && !t.getIdCardNumber().trim().isEmpty()){
			dc.add(Property.forName("idCardNumber").like(t.getIdCardNumber().trim(),MatchMode.ANYWHERE));
		}
		if(t.getPhoneNumber()!=null && !t.getPhoneNumber().trim().isEmpty()){
			dc.add(Property.forName("phoneNumber").like(t.getPhoneNumber().trim(),MatchMode.ANYWHERE));
		}
		dc.addOrder(Order.asc("ticketYearId"));
		return ticketYearDao.findJsonPageByCriteria(jp, dc);
	}

	@Override
	public void save(Collection<TicketYear> coll) {
		for (TicketYear ticketYear : coll) {
			if (ticketYear.getTicketYearId() == null) {
				ticketYear.setTicketYearId(getNextKey("Ticket_Year", 1));
				ticketYear.setInputer(getCurrentUserId());
				ticketYear.setInputTime(CommonMethod.getTimeStamp());
				ticketYearDao.save(ticketYear);
			} else {
				ticketYear.setUpdater(getCurrentUserId());
				ticketYear.setUpdateTime(CommonMethod.getTimeStamp());
				ticketYearDao.update(ticketYear);
			}
		}
	}
}