package service.ticket.ticketYear;
import java.util.Collection;

import com.xlem.dao.TicketYear;

import common.service.BaseService;

public interface TicketYearService extends BaseService<TicketYear> {

	void save(Collection<TicketYear> coll);

}