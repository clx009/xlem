package service.ticket.ticketIndentCheckLog;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.TicketIndentCheckLog;

import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.ticket.ticketIndentCheckLog.TicketIndentCheckLogDao;

@Service("ticketIndentCheckLogService")
public class TicketIndentCheckLogServiceImpl extends BaseServiceImpl<TicketIndentCheckLog>
		implements TicketIndentCheckLogService {
	@Autowired
	private TicketIndentCheckLogDao ticketIndentCheckLogDao;

	@Override
	@Resource(name = "ticketIndentCheckLogDao")
	protected void initBaseDAO(BaseDao<TicketIndentCheckLog> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<TicketIndentCheckLog> findPageByCriteria(
			PaginationSupport<TicketIndentCheckLog> ps, TicketIndentCheckLog t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketIndentCheckLog.class);
		return ticketIndentCheckLogDao.findPageByCriteria(ps,
				Order.desc("ticketIndentCheckLogId"), dc);
	}

	@Override
	public List<TicketIndentCheckLog> findByIndentId(String indentId) {
		if(indentId==null || indentId.trim().isEmpty()){
			return new ArrayList<TicketIndentCheckLog>();
		}
		DetachedCriteria dcDetail = DetachedCriteria.forClass(TicketIndentCheckLog.class);
		dcDetail.add(Property.forName("ticketIndent.indentId").eq(indentId));
		List<TicketIndentCheckLog> tm = ticketIndentCheckLogDao.findByCriteria(dcDetail);
		return tm;
	}
	
	@Override
	public void save(TicketIndentCheckLog t) {
		ticketIndentCheckLogDao.save(t);
	}

	@Override
	public JsonPager<TicketIndentCheckLog> findJsonPageByCriteria(
			JsonPager<TicketIndentCheckLog> jp, TicketIndentCheckLog t) {
		// TODO Auto-generated method stub
		return null;
	}
}