package service.ticket.ticketIndentCheckLog;
import java.util.List;

import com.xlem.dao.TicketIndentCheckLog;

import common.service.BaseService;

public interface TicketIndentCheckLogService extends BaseService<TicketIndentCheckLog> {

	public List<TicketIndentCheckLog> findByIndentId(String indentId);
	
}