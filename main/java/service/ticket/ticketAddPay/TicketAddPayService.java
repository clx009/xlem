package service.ticket.ticketAddPay;
import java.util.List;

import com.xlem.dao.TicketAddPay;

import common.service.BaseService;
public interface TicketAddPayService extends BaseService<TicketAddPay> {

	/**
	 * 保存接口回传回来的信息
	 * @param tbr
	 */
	public void saveList(List<TicketAddPay> tbr);

}