package service.ticket.ticketAddPay;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.TicketAddPay;

import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.ticket.ticketAddPay.TicketAddPayDao;

@Service("ticketAddPayService")
public class TicketAddPayServiceImpl extends BaseServiceImpl<TicketAddPay>
		implements TicketAddPayService {
	@Autowired
	private TicketAddPayDao ticketAddPayDao;

	@Override
	@Resource(name = "ticketAddPayDao")
	protected void initBaseDAO(BaseDao<TicketAddPay> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<TicketAddPay> findPageByCriteria(
			PaginationSupport<TicketAddPay> ps, TicketAddPay t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketAddPay.class);
		return ticketAddPayDao.findPageByCriteria(ps,
				Order.desc("ticketAddPayId"), dc);
	}

	@Override
	public void save(TicketAddPay t) {
		t.setAddPayId(getNextKey("Ticket_Add_Pay".toUpperCase(), 1).toString());
		ticketAddPayDao.save(t);
	}

	@Override
	public JsonPager<TicketAddPay> findJsonPageByCriteria(
			JsonPager<TicketAddPay> jp, TicketAddPay t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveList(List<TicketAddPay> tbr) {
		if(tbr!=null && !tbr.isEmpty()){
			for (int i = 0; i < tbr.size(); i++) {
				ticketAddPayDao.saveOrUpdate(tbr.get(i));
			}
		}
	}
}