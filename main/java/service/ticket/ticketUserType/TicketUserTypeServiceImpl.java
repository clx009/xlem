package service.ticket.ticketUserType;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.SysUser;
import com.xlem.dao.TicketUserType;

import common.CommonMethod;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.sys.sysUser.SysUserDao;
import dao.ticket.ticketUserType.TicketUserTypeDao;
@Service("ticketUserTypeService")
public class TicketUserTypeServiceImpl extends BaseServiceImpl<TicketUserType> implements TicketUserTypeService {
@Autowired
private TicketUserTypeDao ticketUserTypeDao;

@Autowired
private SysUserDao sysUserDao;

	@Override
	@Resource(name="ticketUserTypeDao")
	protected void initBaseDAO(BaseDao<TicketUserType> baseDao) {
		setBaseDao(baseDao);
	}
@Override
public PaginationSupport<TicketUserType> findPageByCriteria(PaginationSupport<TicketUserType> ps, TicketUserType t) {
	DetachedCriteria dc = DetachedCriteria.forClass(TicketUserType.class);
	return ticketUserTypeDao.findPageByCriteria(ps, Order.desc("ticketUserTypeId"), dc);
}
@Override
public void save(TicketUserType t) {
	t.setUserTypeId(getNextKey("TicketUserType".toUpperCase(), 1));
	ticketUserTypeDao.save(t);
}
@Override
public JsonPager<TicketUserType> findJsonPageByCriteria(
		JsonPager<TicketUserType> jp, TicketUserType t) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public List<TicketUserType> findByCriteria(TicketUserType t) {
	DetachedCriteria dc = DetachedCriteria.forClass(TicketUserType.class);
	dc.createAlias("sysUser", "sysUser");
	dc.createAlias("ticketType", "ticketType");
	if(t.getSysUser()!=null && t.getSysUser().getUserId()!=null){
		dc.add(Property.forName("sysUser.userId").eq(t.getSysUser().getUserId()));
	}
	if(t.getTicketType()!=null && t.getTicketType().getTicketId()!=null){
		dc.add(Property.forName("ticketType.ticketId").eq(t.getTicketType().getTicketId()));
	}
	List<TicketUserType> trList = ticketUserTypeDao.findByCriteria(dc);
	return trList;
}

@Override
public void updateReminTopLimit(long userId, long ticketId,int updateNums) {
	DetachedCriteria dc = DetachedCriteria.forClass(TicketUserType.class);
	dc.add(Property.forName("sysUser.userId").eq(userId));
	dc.add(Property.forName("ticketType.ticketId").eq(ticketId));
	List<TicketUserType> trList = ticketUserTypeDao.findByCriteria(dc);
	
	for(TicketUserType trt : trList){
		Integer Toplimit = trt.getToplimit()==null?null:trt.getToplimit();
		if(Toplimit!=null && Toplimit>0){
			trt.setRemainToplimit(trt.getRemainToplimit()-updateNums); 
			ticketUserTypeDao.update(trt);
		}
	}
	
}


@Override
public void save(Collection<TicketUserType> coll,long userId) {
	SysUser sysUser = sysUserDao.findById(userId);
	for (TicketUserType ticketUserType : coll) {
		if((ticketUserType.getUsePage()==null || ticketUserType.getUsePage().trim().equals(""))
				&& ticketUserType.getToplimit()==null && ticketUserType.getRemainToplimit()==null
				&& ticketUserType.getIndentToplimit()==null && ticketUserType.getUserIndentToplimit()==null){
			continue;
		}
			ticketUserType.setSysUser(sysUser);
			ticketUserType.setStatus("11");
			if (ticketUserType.getUserTypeId() == null || ticketUserType.getUserTypeId()==0L) {
				ticketUserType.setUserTypeId(getNextKey("TICKET_USER_TYPE", 1));
				ticketUserType.setInputer(getCurrentUserId());
				ticketUserType.setInputTime(CommonMethod.getTimeStamp());
				ticketUserTypeDao.save(ticketUserType);
			} else {
				ticketUserType.setUpdater(getCurrentUserId());
				ticketUserType.setUpdateTime(CommonMethod.getTimeStamp());
				ticketUserTypeDao.update(ticketUserType);
			}
		}
	
}

}