package service.ticket.ticketUserType;
import java.util.Collection;
import java.util.List;

import com.xlem.dao.TicketUserType;

import common.service.BaseService;

public interface TicketUserTypeService extends BaseService<TicketUserType> {
	/**
	 * 修改剩余数量
	 * @param userId
	 * @param ticketId
	 * @param updateNums
	 */
	public void updateReminTopLimit(long userId, long ticketId,int updateNums);
	
	List<TicketUserType> findByCriteria(TicketUserType t);
	
	public void save(Collection<TicketUserType> coll,long roleId);
}