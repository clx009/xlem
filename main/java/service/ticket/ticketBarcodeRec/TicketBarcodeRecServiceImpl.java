package service.ticket.ticketBarcodeRec;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import service.comm.baseRechargeRec.BaseRechargeRecService;
import service.comm.baseTranRec.BaseTranRecService;
import service.comm.reg.baseTravelAgency.BaseTravelAgencyService;
import service.job.TicketIndentJobService;
import service.ticket.financeTicketAccounts.FinanceTicketAccountsService;
import service.ticket.ticketIndentDetail.TicketIndentDetailService;
import ws.ReturnInfo;
import ws.client.ticketIndent.TicketIndentClient;

import com.thoughtworks.xstream.XStream;
import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.BaseRewardAccount;
import com.xlem.dao.BaseTranRec;
import com.xlem.dao.ConsumeTicketInfo;
import com.xlem.dao.SysUser;
import com.xlem.dao.TicketAccounts;
import com.xlem.dao.TicketBarcodeRec;
import com.xlem.dao.TicketDetailHis;
import com.xlem.dao.TicketIndent;
import com.xlem.dao.TicketIndentDetail;
import com.xlem.dao.TicketType;
import com.xlem.dao.Travservice;

import common.BusinessException;
import common.CodeListener;
import common.CommonMethod;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.comm.baseRechargeRec.BaseRechargeRecDao;
import dao.comm.baseRewardAccount.BaseRewardAccountDao;
import dao.comm.reg.baseTravelAgency.BaseTravelAgencyDao;
import dao.sys.sysUser.SysUserDao;
import dao.ticket.ticketBarcodeRec.TicketBarcodeRecDao;
import dao.ticket.ticketIndent.TicketIndentDao;
import dao.ticket.ticketIndentDetail.TicketDetailHisDao;
import dao.ticket.ticketIndentDetail.TicketIndentDetailDao;

/**
 * @author STKJ08
 *
 */
@Service("ticketBarcodeRecService")
public class TicketBarcodeRecServiceImpl extends
		BaseServiceImpl<TicketBarcodeRec> implements TicketBarcodeRecService {
	@Autowired
	private TicketBarcodeRecDao ticketBarcodeRecDao;
	@Autowired
	private TicketIndentDao ticketIndentDao;
	@Autowired
	private TicketIndentDetailDao ticketIndentDetailDao;
	@Autowired
	private TicketDetailHisDao ticketDetailHisDao;
	@Autowired
	private BaseTravelAgencyDao baseTravelAgencyDao;
	@Autowired
	private SysUserDao sysUserDao; 
	@Autowired
	private BaseTranRecService baseTranRecService;
	@Autowired
	private BaseRechargeRecService baseRechargeRecService;
	@Autowired
	private TicketIndentJobService ticketIndentJobService;
	@Autowired
	private BaseTravelAgencyService baseTravelAgencyService;
	@Autowired
	private BaseRewardAccountDao baseRewardAccountDao;
	@Autowired
	private BaseRechargeRecDao baseRechargeRecDao;
	@Autowired
	private FinanceTicketAccountsService financeTicketAccountsService;
	@Autowired
	private TicketIndentDetailService  ticketIndentDetailService;
	
	private static Logger logger = Logger.getLogger("bookingService");
	
	@Override
	@Resource(name = "ticketBarcodeRecDao")
	protected void initBaseDAO(BaseDao<TicketBarcodeRec> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<TicketBarcodeRec> findPageByCriteria(
			PaginationSupport<TicketBarcodeRec> ps, TicketBarcodeRec t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketBarcodeRec.class);
		return ticketBarcodeRecDao.findPageByCriteria(ps,
				Order.desc("ticketBarcodeRecId"), dc);
	}

	@Override
	public void save(TicketBarcodeRec t) {
		t.setBarcodeRecId(getNextKey("Ticket_Barcode_Rec".toUpperCase(), 1).toString());
		ticketBarcodeRecDao.save(t);
	}

	@Override
	public JsonPager<TicketBarcodeRec> findJsonPageByCriteria(
			JsonPager<TicketBarcodeRec> jp, TicketBarcodeRec t) {
		return null;
	}

	@Override
	public JsonPager<TicketAccounts> findGridByAccounts() {
		JsonPager<TicketAccounts> jp = new JsonPager<TicketAccounts>();
		List<TicketAccounts> taList = ticketBarcodeRecDao.findGridByAccounts();
		jp.setRoot(taList);
		jp.setTotal(taList.size());
		return jp;
	}
	
	@Override
	public JsonPager<TicketAccounts> findGridBySubAccounts() {
		JsonPager<TicketAccounts> jp = new JsonPager<TicketAccounts>();
		List<TicketAccounts> taList = ticketBarcodeRecDao.findGridBySubAccounts();
		
		List<TicketAccounts> taListSub = new ArrayList<TicketAccounts>();
		
		String eCompany= CodeListener.getNameByCode("TICKET_MODULE", "company", "01");
		String rCompany= CodeListener.getNameByCode("TICKET_MODULE", "company", "02");
		
		for(TicketAccounts ta : taList){
			boolean setAmount = true;
			if((ta.getSubtotalGroupE()!=null && ta.getSubtotalGroupE()>0d ) || 
					(ta.getSubtotalGroupDE()!=null && ta.getSubtotalGroupDE()>0d ) ||
					(ta.getSubtotalE()!=null && ta.getSubtotalE()>0d ) ||
					(ta.getSubtotalLocaleE()!=null && ta.getSubtotalLocaleE()>0d )){
				TicketAccounts taE = new TicketAccounts(ta,"E",eCompany,true);
				taListSub.add(taE);
				setAmount = false;
			}
			
			if((ta.getSubtotalGroupR()!=null && ta.getSubtotalGroupR()>0d ) || 
					(ta.getSubtotalGroupDR()!=null && ta.getSubtotalGroupDR()>0d ) ||
					(ta.getSubtotalR()!=null && ta.getSubtotalR()>0d ) ||
					(ta.getSubtotalLocaleR()!=null && ta.getSubtotalLocaleR()>0d )){
				TicketAccounts taR = new TicketAccounts(ta,"R",rCompany,setAmount);
				taListSub.add(taR);
			}
			
		}
		
		jp.setRoot(taListSub);
		jp.setTotal(taListSub.size());
		return jp;
	}

	@Override
	public void savePrintBack(List<TicketBarcodeRec> tbr) {
		Set<TicketIndent> indents = new HashSet<TicketIndent>();
		Set<TicketIndentDetail> indentDetails = new HashSet<TicketIndentDetail>();
		Map<String, List<TicketBarcodeRec>> ticketMap = new HashMap<String, List<TicketBarcodeRec>>();
		for (TicketBarcodeRec ticketBarcodeRec : tbr) {
			TicketIndentDetail detail = ticketBarcodeRec.getTicketIndentDetail();
			List<TicketBarcodeRec> tbrList = ticketMap.get(detail.getTicketIndent().getIndentId());
			if(tbrList==null){
				tbrList = new ArrayList<TicketBarcodeRec>();
			}
			tbrList.add(ticketBarcodeRec);
			ticketMap.put(detail.getTicketIndent().getIndentId(), tbrList);
			indents.add(detail.getTicketIndent());
			/**
			 * 1.团客订单且不为代散客订单
			 * 2.以C开头订单明细，即自助购票
			 */
//			if(("01".equals(detail.getTicketIndent().getCustomerType()) && !"1".equals(detail.getTicketIndent().getTelNumber())) 
//					|| detail.getTicketIndent().getIndentId().indexOf("C")!=-1){
				indentDetails.add(detail);
//			}
		}
		//把detail填充到对应的indent中
		for(TicketIndentDetail detail:indentDetails){
			for (TicketIndent indent : indents) {
				if(detail.getTicketIndent().getIndentId().equals(indent.getIndentId())){
					indent.getDetails().add(detail);
				}
			}
		}
		
		for (TicketIndent ticketIndent : indents) {
			TicketIndent indent = ticketIndentDao.findById(ticketIndent.getIndentId());//当前订单信息
			//将原订单明细保存到历史记录
			List<TicketIndentDetail> oldList = ticketIndentDetailDao.findByProperty("ticketIndent.indentId", ticketIndent.getIndentId());
			if(indent!=null && !CommonMethod.format3db(indent.getTotalPrice()).equals(CommonMethod.format3db(ticketIndent.getTotalPrice()))){//价格不相等，就退款
				indent.setDetails(oldList);
				if("01".equals(ticketIndent.getPayType().trim())){//01奖励款退款
					rewardReturn(indent,ticketIndent);
				}else{//02现金
					
				String pType = ticketIndent.getPayType();
					
				//获取股份公司和雪山公司退款总额
				Map<String,Double> map  = calcReturnMoney(indent,ticketIndent);
				
				double snowcomRm = map.get("snow");//雪山账户退款总额
				double sunonRm = map.get("joint");//股份公司退款总额
					
				//修改旅行社余额
				SysUser sysUser = sysUserDao.findById(indent.getInputer());
				Travservice baseTravelAgency = baseTravelAgencyDao.findById(sysUser.getBaseTravelAgency().getTravelAgencyId());
				
				double cBalance = 0d;
				double cBalanceSnow = 0d;
				
				String fundType = "";
				if("02".equals(pType)){//现金支付
					if(baseTravelAgency.getCashBalance()==null){
						baseTravelAgency.setCashBalance(0d);
					}
					if(baseTravelAgency.getCashBalanceSnow()==null){
						baseTravelAgency.setCashBalanceSnow(0d);
					}
					
					baseTravelAgency.setCashBalance(baseTravelAgency.getCashBalance()+sunonRm);
					baseTravelAgency.setCashBalanceSnow(baseTravelAgency.getCashBalanceSnow()+snowcomRm);
					
					cBalance = baseTravelAgency.getCashBalance();
					cBalanceSnow =  baseTravelAgency.getCashBalanceSnow();
					
					fundType = "01";
				}
				if("04".equals(pType)){//预付款支付
					if(baseTravelAgency.getCashTopupBalance()==null){
						baseTravelAgency.setCashTopupBalance(0d);
					}
					if(baseTravelAgency.getCashTopupBalanceSnow()==null){
						baseTravelAgency.setCashTopupBalanceSnow(0d);
					}
					
					baseTravelAgency.setCashTopupBalance(baseTravelAgency.getCashTopupBalance()+sunonRm);
					baseTravelAgency.setCashTopupBalanceSnow(baseTravelAgency.getCashTopupBalanceSnow()+snowcomRm);
					
					cBalance = baseTravelAgency.getCashTopupBalance();
					cBalanceSnow =  baseTravelAgency.getCashTopupBalanceSnow();
					
					fundType = "03";
				}
				
				
				baseTravelAgency.setUpdateTime(CommonMethod.getTimeStamp());
				
				baseTravelAgencyDao.update(baseTravelAgency);
				
				//添加交易记录
				BaseTranRec baseTranRec = new BaseTranRec();
				baseTranRec.setTicketIndent(indent);
				baseTranRec.setMoney((0-sunonRm)+(0-snowcomRm));
				baseTranRec.setMoneyJoint((0-sunonRm));
				baseTranRec.setMoneySnow((0-snowcomRm));
				baseTranRec.setPaymentType("02");//支付类型:退款
				if("04".equals(pType)){
					baseTranRec.setPayType("03");//交易类型,通过预付款交易
				}else{
					baseTranRec.setPayType("02");//交易类型,通过余额交易
				}
				baseTranRec.setPaymentTime(CommonMethod.getTimeStamp());
				
				
				//添加充值记录
				BaseRechargeRec baseRechargeRec = new BaseRechargeRec();
				
				baseRechargeRec.setRechargeRecId(getNextKey("BASE_RECHARGE_REC", 1));
				baseRechargeRec.setBaseTravelAgency(baseTravelAgency);
				baseRechargeRec.setTicketIndent(indent);
				baseRechargeRec.setSourceDescription("团客审核退款");
				baseRechargeRec.setCashMoney(sunonRm);
				baseRechargeRec.setCashMoneySnow(snowcomRm);
				baseRechargeRec.setCashBalance(cBalance);
				baseRechargeRec.setCashBalanceSnow(cBalanceSnow);
				baseRechargeRec.setPaymentType("02");//退款
				baseRechargeRec.setFundType(fundType);
//				baseRechargeRec.setInputer(getCurrentUserId());
				baseRechargeRec.setInputTime(CommonMethod.getTimeStamp());
				
				//记录财务信息
				financeTicketAccountsService.savePrintBackInfo(indent, ticketIndent, baseTranRec, baseRechargeRec);
				
				baseTranRecService.save(baseTranRec);
				baseRechargeRecService.save(baseRechargeRec);
				
				
				}
			}else if(indent!=null){//价格相等则写财务记录
				indent.setDetails(oldList);
				BaseRechargeRec brr = null;
				
				DetachedCriteria dc = DetachedCriteria.forClass(BaseRechargeRec.class);
				dc.add(Property.forName("ticketIndent.indentId").eq(ticketIndent.getIndentId()));
				List<BaseRechargeRec> ftaList =  baseRechargeRecDao.findByCriteria(dc);
				
				//排除冲退和已被冲退的记录
				List<BaseRechargeRec> ftaListRe =  baseRechargeRecService.excludeReturnBackInfo(ftaList);
				
				if(ftaListRe!=null && ftaListRe.size()>0){
					brr = ftaListRe.get(0);
				}
					
				
				//记录财务信息
				financeTicketAccountsService.savePrintBackInfo(indent, ticketIndent, null, brr);
				
			}
			if(indent==null || indent.getIndentCode()==null){
				ticketIndent.setVersion(0);
				ticketIndent.setStatus("04");//修改订单状态为已打印
				ticketIndent.setSynchroState("02");//完成出票同步
				ticketIndentDao.save(ticketIndent);
			}
			else {
				ticketIndent.setVersion(indent.getVersion());
				ticketIndent.setStatus("04");//修改订单状态为已打印
				ticketIndent.setSynchroState("02");//完成出票同步
				ticketIndentDao.merge(ticketIndent);
			}
			boolean isAct = false;
			List<TicketBarcodeRec> tbrList = (List<TicketBarcodeRec>) ticketMap.get(ticketIndent.getIndentId());
			for (TicketBarcodeRec ticketBarcodeRec : tbrList) {
				TicketIndentDetail detailTmp = ticketBarcodeRec.getTicketIndentDetail();
				TicketType tt = detailTmp.getTicketPrice().getTicketType();
				if(isAct == false && "01".equals(tt.getActivity())){
					isAct = true;
				}
			}
			if((indent!=null && "01".equals(indent.getCustomerType()) && !"1".equals(indent.getTelNumber()) && isAct==false)
					|| (indent!=null && ("02".equals(indent.getIndentType()) && !"01".equals(indent.getCustomerType())))
					|| (indent!=null && ("05".equals(indent.getIndentType()) && !"01".equals(indent.getCustomerType())))
					|| (indent!=null && ("05".equals(indent.getIndentType()) && "01".equals(indent.getCustomerType())
							&& "1".equals(indent.getTelNumber())))){
				
				for(TicketIndentDetail tid : oldList){
					TicketDetailHis tdh = new TicketDetailHis();
					
					tdh.setDetailHisId(getNextKey("TICKET_DETAIL_HIS", 1));
					tdh.setIndentDetailId(tid.getIndentDetailId());
					tdh.setTicketIndent(tid.getTicketIndent());
					tdh.setTicketPrice(tid.getTicketPrice());
					tdh.setIndentDetailCode(tid.getIndentDetailCode());
					tdh.setUnitPrice(tid.getUnitPrice());
					tdh.setAmount(tid.getAmount());
					tdh.setPerFee(tid.getPerFee());
					tdh.setSubtotal(tid.getSubtotal());
					tdh.setRemark(tid.getRemark());
					tdh.setInputer(tid.getInputer());
					tdh.setInputTime(tid.getInputTime());
					tdh.setUpdater(tid.getUpdater());
					tdh.setUpdateTime(tid.getUpdateTime());
					tdh.setEntranceUnitPrice(tid.getEntranceUnitPrice());
					tdh.setEntranceCompany(tid.getEntranceCompany());
					tdh.setRopewayUnitPrice(tid.getRopewayUnitPrice());
					tid.setRopewayCompany(tid.getRopewayCompany());
					
					ticketDetailHisDao.save(tdh);
				}
				
				ticketIndentDetailDao.deleteByIndentId(ticketIndent.getIndentId());
			
			}
		}
		
		for (TicketIndentDetail ticketIndentDetail : indentDetails) {//保存明细
			boolean isAct = false;
			List<TicketBarcodeRec> tbrList = (List<TicketBarcodeRec>) ticketMap.get(ticketIndentDetail.getTicketIndent().getIndentId());
			for (TicketBarcodeRec ticketBarcodeRec : tbrList) {
				TicketIndentDetail detailTmp = ticketBarcodeRec.getTicketIndentDetail();
				TicketType tt = detailTmp.getTicketPrice().getTicketType();
				if(isAct == false && "01".equals(tt.getActivity())){
					isAct = true;
				}
			}
			
			if(("01".equals(ticketIndentDetail.getTicketIndent().getCustomerType()) && !"1".equals(ticketIndentDetail.getTicketIndent().getTelNumber()) && isAct==false) 
			|| ticketIndentDetail.getTicketIndent().getIndentId().indexOf("C")!=-1
			|| ("02".equals(ticketIndentDetail.getTicketIndent().getIndentType()) && !"01".equals(ticketIndentDetail.getTicketIndent().getCustomerType()))
			|| ("05".equals(ticketIndentDetail.getTicketIndent().getIndentType()) && !"01".equals(ticketIndentDetail.getTicketIndent().getCustomerType()))
			|| ("05".equals(ticketIndentDetail.getTicketIndent().getIndentType()) && "01".equals(ticketIndentDetail.getTicketIndent().getCustomerType())
					&& "1".equals(ticketIndentDetail.getTicketIndent().getTelNumber()))){
				ticketIndentDetailDao.save(ticketIndentDetail);
			}
		}
		
		for (TicketBarcodeRec ticketBarcodeRec : tbr) {
			ticketBarcodeRecDao.save(ticketBarcodeRec);
		}
	}

	/**
	 * @param indent booking订单
	 * @param ticketIndent 接口传回订单数据 即Cache订单
	 */
	private void rewardReturn(TicketIndent indent, TicketIndent ticketIndent) {
		Map<String,Double> map  = calcReturnMoney(indent,ticketIndent);
		
		double snowcomRm = map.get("snow");//雪山账户退款总额
		double sunonRm = map.get("joint");//股份公司退款总额
		
		//double rm = indent.getTotalPrice()-ticketIndent.getTotalPrice();//需退款金额
		Travservice baseTravelAgency = baseTravelAgencyService.findTravelAgencyByUser(ticketIndent.getInputer());
		//1.修改奖励款余额
		BaseRewardAccount baseRewardAccount= baseRewardAccountDao.findAccount(baseTravelAgency.getTravelAgencyId(), "30",Timestamp.valueOf(indent.getUseDate().toString()),Timestamp.valueOf(indent.getUseDate().toString()));//票暂时固定30
		if(baseRewardAccount==null){//退票时，奖励款已过期
			return;
		}else{
			baseRewardAccount.setRewardBalance(baseRewardAccount.getRewardBalance()+sunonRm);
			baseRewardAccount.setRewardBalanceSnow(baseRewardAccount.getRewardBalanceSnow()+snowcomRm);
			baseRewardAccountDao.update(baseRewardAccount);
		}
		//2.添加奖励款充值记录
		BaseRechargeRec brr = new BaseRechargeRec(getNextKey("Base_Recharge_Rec", 1));
		brr.setBaseRewardAccount(baseRewardAccount);
		brr.setBaseTravelAgency(baseTravelAgency);
		brr.setInputTime(CommonMethod.getTimeStamp());
		brr.setFundType("02");
		brr.setPaymentType("02");//02退款
		brr.setSourceDescription("审核调整退款");
		brr.setRewardBalance(baseRewardAccount.getRewardBalance());
		brr.setRewardBalanceSnow(baseRewardAccount.getRewardBalanceSnow());
		brr.setRewardMoney(sunonRm);
		brr.setRewardMoneySnow(snowcomRm);
		brr.setTicketIndent(ticketIndent);
		
		//记录财务信息
		financeTicketAccountsService.savePrintBackInfo(indent, ticketIndent, null, brr);
		
		baseRechargeRecDao.save(brr);
	}

	private Map<String, Double> calcReturnMoney(TicketIndent oldIndent,
			TicketIndent newIndent) {
		List<TicketIndentDetail> oldDetails = oldIndent.getDetails();
		List<TicketIndentDetail> newDetails = newIndent.getDetails();
		Map<String,Double> map = new HashMap<String, Double>();
		Double countReturn = 0D;//股份公司  退款总额
		Double countReturnSnow = 0D;//雪山公司  退款总额
		
		//将新旧订单明细数据放到map中用priceId做key
		Map<String,TicketIndentDetail> newDetailsMap = new HashMap<String,TicketIndentDetail>();
		for(TicketIndentDetail tid : newDetails){
			newDetailsMap.put("priceId_"+tid.getTicketPrice().getTicketPriceId(), tid);
		}
		
		for (TicketIndentDetail oldDetail : oldDetails) {
			
			String key = "priceId_"+oldDetail.getTicketPrice().getTicketPriceId();
			int amountOld = oldDetail.getAmount() == null ? 0 : oldDetail.getAmount();
			double moneyOld = (oldDetail.getRopewayUnitPrice() == null ? 0 : oldDetail.getRopewayUnitPrice())*amountOld;
			double moneySnowOld = (oldDetail.getEntranceUnitPrice() == null ? 0 : oldDetail.getEntranceUnitPrice())*amountOld;
			
			TicketIndentDetail newDetail = newDetailsMap.get(key);
			
			if(newDetail!=null){
				int amountNew = newDetail.getAmount() == null ? 0 : newDetail.getAmount();
				
				double uPriceSnow = newDetail.getEntranceUnitPrice()==null ? 0 : newDetail.getEntranceUnitPrice();
				double uPrice = newDetail.getRopewayUnitPrice()==null ? 0 : newDetail.getRopewayUnitPrice();
				
				double money = uPrice*amountNew;
				double moneySnow = uPriceSnow*amountNew;
				
				countReturn += moneyOld-money;
				countReturnSnow += moneySnowOld-moneySnow;
				
			}else{
				
				countReturn += 0-moneyOld;
				countReturnSnow += 0-moneySnowOld;
				
			}
			
		}
		
		
//		for (TicketIndentDetail oldDetail : oldDetails) {
//			Long ticketPriceId = oldDetail.getTicketPrice().getTicketPriceId();
//			int count = 0;
//			for (TicketIndentDetail newDetail : newDetails) {
//				Long ticketPriceIdNew = newDetail.getTicketPrice().getTicketPriceId();
//				if(ticketPriceId == ticketPriceIdNew){
//					int amountOld = oldDetail.getAmount() == null ? 0 : oldDetail.getAmount();//总数量
//					int amountNew = newDetail.getAmount() == null ? 0 : newDetail.getAmount();//回传总数量
//					int amountReturn = amountOld - amountNew;
//					if(amountReturn!=0){//某一种票不是全部都退的情况  退票数量amountReturn
//						countReturn += (oldDetail.getRopewayUnitPrice() == null ? 0 : oldDetail.getRopewayUnitPrice())*amountReturn;
//						countReturnSnow += (oldDetail.getEntranceUnitPrice() == null ? 0 : oldDetail.getEntranceUnitPrice())*amountReturn;
//					}
//					count = 1;
//					break;
//				}
//			}
//			if(count == 0){//某一种类型的票全退的时候
//				countReturn += (oldDetail.getRopewayUnitPrice() == null ? 0 : oldDetail.getRopewayUnitPrice())*oldDetail.getAmount();
//				countReturnSnow += (oldDetail.getEntranceUnitPrice() == null ? 0 : oldDetail.getEntranceUnitPrice())*oldDetail.getAmount();
//			}
//		}
		map.put("joint", countReturn);//股份公司   总退款
		map.put("snow", countReturnSnow);//雪山公司 总退款
		return map;
	}

	@Override
	public void saveCheckBack(List<TicketBarcodeRec> tbr) {
		for (TicketBarcodeRec ticketBarcodeRec : tbr) {
			ticketBarcodeRecDao.saveOrUpdate(ticketBarcodeRec);
		}
		ticketIndentJobService.saveTicketUse();
	}

	@Override
	public JsonPager<ConsumeTicketInfo> findTicketStatisticsInfoForMKT(
			JsonPager<ConsumeTicketInfo> jsonPager, Timestamp startTime,
			Timestamp endTime,String discount, String ticketTypes, String eliminateTimes) {
//		jsonPager = 
//		jsonPager.setTotal(this.ticketBarcodeRecDao.findTicketStatisticRowCountForMKT(startTime, endTime));
//		jsonPager.setRoot(this.ticketBarcodeRecDao.findTicketStatisticsDataForMKT(jsonPager, startTime, endTime));
		return ticketBarcodeRecDao.findTicketStatisticsInfoForMKT(jsonPager,startTime,endTime,discount, ticketTypes, eliminateTimes);
	}
	
	public List<Map<String,Object>> findRealPrint(Timestamp startT,Timestamp endT){
		
		List<Map<String,Object>> r = new ArrayList<Map<String,Object>>();
		
		TicketIndentClient tic = new TicketIndentClient();
		try{
			
			String start = startT == null ? null : CommonMethod.timeToString(startT);
			String end = endT == null ? null : CommonMethod.timeToString(endT);
			
			ReturnInfo ri = tic.findRealPrint(start, end);
			if(ri.getResult().trim().equals("-1")){//接口调用失败
				logger.error("查询出票数量出错:"+ri.getError());
				throw new BusinessException("网络中断,请稍后再试!");
			}else{
				XStream xsRoomTypes=new XStream();
				xsRoomTypes.alias("obj",Map.class);
				xsRoomTypes.alias("root",List.class);
				List<Map<String,Object>> re =(List<Map<String,Object>>) xsRoomTypes.fromXML(ri.getResult());
				
				/**
				 * map的组成及key对应的payType
				 * key		payType			type
				 * 00		00				现场售票
				 * 11		01\02\04\null	平台取票
				 * 22		03				自助购票
				 * 33		05\06			移动终端取票
				 */
				Map<String,Map<String,Object>> tempMap = new HashMap<String,Map<String,Object>>();
				
				for(Map<String,Object> typeMap : re){
					
					String payType = typeMap.get("payType").toString();
					String key;
					String type;
					
					if("00".equals(payType)){
						key = "00";
						type = "现场售票";
					}else if("01".equals(payType) || "02".equals(payType) || "04".equals(payType)){
						key = "11";
						type = "平台取票";
					}else if("03".equals(payType)){
						key = "22";
						type = "自助购票";
					}else if("05".equals(payType) || "06".equals(payType)){
						key = "33";
						type = "移动终端取票";
					}else{
						key = "11";
						type = "平台取票";
					}
					
					Map<String,Object> subMap = tempMap.get(key);
					if(subMap==null){
						subMap = new HashMap<String,Object>();
						subMap.put("payType", key);
						subMap.put("type", type);
						subMap.put("nums", 0);
						subMap.put("sums", 0.0);
					}
					
					int nums = typeMap.get("nums")==null?0:Integer.valueOf(typeMap.get("nums").toString());
					double sums = typeMap.get("sums")==null?0d:Double.valueOf(typeMap.get("sums").toString());
					subMap.put("nums", nums + (subMap.get("nums")==null?0:Integer.valueOf(subMap.get("nums").toString())));
					subMap.put("sums", sums + (subMap.get("sums")==null?0d:Double.valueOf(subMap.get("sums").toString())));
					
					tempMap.put(key, subMap);
					
				}
				
				//用于排序
				List<String> keyList = new ArrayList<String>();
				keyList.add("00");
				keyList.add("11");
				keyList.add("22");
				keyList.add("33");
				
				for(String key : keyList){
					Map<String,Object> subMap = tempMap.get(key);
					if(subMap==null){
						String type = "平台取票";
						if("00".equals(key)){
							type = "现场售票";
						}else if("11".equals(key)){
							type = "平台取票";
						}else if("22".equals(key)){
							type = "自助购票";
						}else if("33".equals(key)){
							type = "移动终端取票";
						}
						subMap = new HashMap<String,Object>();
						subMap.put("payType", key);
						subMap.put("type", type);
						subMap.put("nums", 0);
						subMap.put("sums", 0.0);
					}
					r.add(subMap);
				}
				
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			logger.error("查询出票数量出错:"+e.getMessage());
			throw new BusinessException(e.getMessage());
		}
		
		return r;
	}
}