package service.ticket.ticketBarcodeRec;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import com.xlem.dao.ConsumeTicketInfo;
import com.xlem.dao.TicketAccounts;
import com.xlem.dao.TicketBarcodeRec;

import common.action.JsonPager;
import common.service.BaseService;

public interface TicketBarcodeRecService extends BaseService<TicketBarcodeRec> {

	public JsonPager<TicketAccounts> findGridByAccounts();
	
	public JsonPager<TicketAccounts> findGridBySubAccounts();
	
	/**
	 * 保存回传打印票记录
	 * @param tbr
	 */
	void savePrintBack(List<TicketBarcodeRec> tbr);

	public void saveCheckBack(List<TicketBarcodeRec> tbr);

	public JsonPager<ConsumeTicketInfo> findTicketStatisticsInfoForMKT(
			JsonPager<ConsumeTicketInfo> jsonPager, Timestamp startTime,
			Timestamp endTime, String discount, String ticketTypes, String eliminateTimes);
	
	/**
	 * 按时间段查询出票数量
	 * @param start
	 * @param end
	 * @return
	 */
	public List<Map<String,Object>> findRealPrint(Timestamp start,Timestamp end);
}