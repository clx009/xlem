package service.ticket.financeTicketAccounts;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.BaseTranRec;
import com.xlem.dao.FinanceTicketAccounts;
import com.xlem.dao.TicketIndent;
import com.xlem.dao.TicketIndentDetail;
import com.xlem.dao.TicketType;

import common.BusinessException;
import common.CommonMethod;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.ticket.financeTicketAccounts.AllStatementObject;
import dao.ticket.financeTicketAccounts.FinanceTicketAccountsDao;
import dao.ticket.financeTicketAccounts.StatementObject;
import dao.ticket.financeTicketAccounts.StatementObjectForPage;
import dao.ticket.ticketIndentDetail.TicketIndentDetailDao;

@Service("financeTicketAccountsService")
public class FinanceTicketAccountsServiceImpl extends
		BaseServiceImpl<FinanceTicketAccounts> implements
		FinanceTicketAccountsService {
	@Autowired
	private FinanceTicketAccountsDao financeTicketAccountsDao;
	@Autowired
	private TicketIndentDetailDao ticketIndentDetailDao;

	@Override
	@Resource(name = "financeTicketAccountsDao")
	protected void initBaseDAO(BaseDao<FinanceTicketAccounts> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<FinanceTicketAccounts> findPageByCriteria(
			PaginationSupport<FinanceTicketAccounts> ps, FinanceTicketAccounts t) {
		DetachedCriteria dc = DetachedCriteria
				.forClass(FinanceTicketAccounts.class);
		return financeTicketAccountsDao.findPageByCriteria(ps,
				Order.desc("financeTicketAccountsId"), dc);
	}

	@Override
	public void save(FinanceTicketAccounts t) {
		t.setTicketAccountsId(getNextKey(
				"Finance_Ticket_Accounts".toUpperCase(), 1));
		financeTicketAccountsDao.save(t);
	}

	@Override
	public JsonPager<FinanceTicketAccounts> findJsonPageByCriteria(
			JsonPager<FinanceTicketAccounts> jp, FinanceTicketAccounts t) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 排除冲退和已被冲退的记录
	 * @return
	 */
	private List<FinanceTicketAccounts> excludeReturnBackInfo(List<FinanceTicketAccounts> ftaList) {
		
		List<FinanceTicketAccounts> ftaListRet = new ArrayList<FinanceTicketAccounts>();
		
		//先找出冲退和已被冲退的记录Id
		Map<String,Long> excludeIds = new HashMap<String,Long>();
		for(FinanceTicketAccounts fta : ftaList){
			Long bId = fta.getBackAccountsId();
			if(bId!=null){//是冲退记录
				excludeIds.put("id_"+bId, bId);//被冲退记录ID
				Long id = fta.getTicketAccountsId();
				excludeIds.put("id_"+id, id);//冲退记录ID
			}
		}
		
		//将其他记录放到新List
		for(FinanceTicketAccounts fta : ftaList){
			Long id = fta.getTicketAccountsId();
			
			Long excludeId = excludeIds.get("id_"+id);
			if(excludeId==null){
				ftaListRet.add(fta);
			}
		}
		
		return ftaListRet;
	}
	
	@Override
	public void saveReturnBackInfo(TicketIndent ticketIndent,
			BaseTranRec baseTranRec,BaseRechargeRec baseRechargeRec) {
		
		Timestamp thisTime = CommonMethod.getTimeStamp();
		
		DetachedCriteria dc = DetachedCriteria.forClass(FinanceTicketAccounts.class);
		dc.add(Property.forName("ticketIndentId").eq(ticketIndent.getIndentId()));
		List<FinanceTicketAccounts> ftaList =  financeTicketAccountsDao.findByCriteria(dc);
		
		Long rechargeRecId = baseRechargeRec == null ? null : baseRechargeRec.getRechargeRecId();
		String tranRecId = baseTranRec == null ? null : baseTranRec.getTranRecId();
		
		//排除冲退和已被冲退的记录
		List<FinanceTicketAccounts> ftaListRe =  excludeReturnBackInfo(ftaList);
		
		for(FinanceTicketAccounts ftaO : ftaListRe){
			
			FinanceTicketAccounts ftaN = new FinanceTicketAccounts(getNextKey("Finance_Ticket_Accounts", 1));
			ftaN.setAccountsType(ftaO.getAccountsType());
			ftaN.setInputTime(thisTime);
			ftaN.setPaymentType(FinanceTicketAccounts.PAYMENTTYPE_09);
			ftaN.setRechargeRecId(rechargeRecId);
			ftaN.setTranRecId(tranRecId);
			ftaN.setTicketIndentId(ticketIndent.getIndentId());
			ftaN.setTicketType(ftaO.getTicketType());
			ftaN.setTikcetIndentCode(ftaO.getTikcetIndentCode());
			ftaN.setMoney(0-(ftaO.getMoney()==null ? 0 : ftaO.getMoney()));
			ftaN.setMoneySnow(0-(ftaO.getMoneySnow()==null ? 0 : ftaO.getMoneySnow()));
			ftaN.setAmount(0-(ftaO.getAmount()==null ? 0 : ftaO.getAmount()));
			ftaN.setTicketPriceId(ftaO.getTicketPriceId());
			ftaN.setBackAccountsId(ftaO.getTicketAccountsId());
			
			financeTicketAccountsDao.save(ftaN);
		}
		
		
	}
	
	@Override
	public void savePayToPaid(TicketIndent ticketIndent) {
		
		Timestamp thisTime = CommonMethod.getTimeStamp();
		
		DetachedCriteria dc = DetachedCriteria.forClass(FinanceTicketAccounts.class);
		dc.add(Property.forName("ticketIndentId").eq(ticketIndent.getIndentId()));
		List<FinanceTicketAccounts> ftaList =  financeTicketAccountsDao.findByCriteria(dc);
		
		//排除冲退和已被冲退的记录
		List<FinanceTicketAccounts> ftaListRe =  excludeReturnBackInfo(ftaList);
		
		for(FinanceTicketAccounts ftaO : ftaListRe){
			
			//先冲退
			FinanceTicketAccounts ftaB = new FinanceTicketAccounts(getNextKey("Finance_Ticket_Accounts", 1));
			ftaB.setAccountsType(ftaO.getAccountsType());
			ftaB.setInputTime(thisTime);
			ftaB.setPaymentType(FinanceTicketAccounts.PAYMENTTYPE_09);
			ftaB.setRechargeRecId(ftaO.getRechargeRecId());
			ftaB.setTranRecId(ftaO.getTranRecId());
			ftaB.setTicketIndentId(ticketIndent.getIndentId());
			ftaB.setTicketType(ftaO.getTicketType());
			ftaB.setTikcetIndentCode(ftaO.getTikcetIndentCode());
			ftaB.setMoney(0-(ftaO.getMoney()==null ? 0 : ftaO.getMoney()));
			ftaB.setMoneySnow(0-(ftaO.getMoneySnow()==null ? 0 : ftaO.getMoneySnow()));
			ftaB.setAmount(0-(ftaO.getAmount()==null ? 0 : ftaO.getAmount()));
			ftaB.setTicketPriceId(ftaO.getTicketPriceId());
			ftaB.setBackAccountsId(ftaO.getTicketAccountsId());
			
			financeTicketAccountsDao.save(ftaB);
			
			//添加支付记录
			FinanceTicketAccounts ftaN = new FinanceTicketAccounts(getNextKey("Finance_Ticket_Accounts", 1));
			ftaN.setAccountsType(ftaO.getAccountsType());
			ftaN.setInputTime(thisTime);
			ftaN.setPaymentType(FinanceTicketAccounts.PAYMENTTYPE_02);
			ftaN.setRechargeRecId(ftaO.getRechargeRecId());
			ftaN.setTranRecId(ftaO.getTranRecId());
			ftaN.setTicketIndentId(ticketIndent.getIndentId());
			ftaN.setTicketType(ftaO.getTicketType());
			ftaN.setTikcetIndentCode(ftaO.getTikcetIndentCode());
			ftaN.setMoney((ftaO.getMoney()==null ? 0 : ftaO.getMoney()));
			ftaN.setMoneySnow((ftaO.getMoneySnow()==null ? 0 : ftaO.getMoneySnow()));
			ftaN.setAmount((ftaO.getAmount()==null ? 0 : ftaO.getAmount()));
			ftaN.setTicketPriceId(ftaO.getTicketPriceId());
			
			financeTicketAccountsDao.save(ftaN);
		}
		
		
	}
	
	@Override
	public void savePayInfo(TicketIndent ticketIndent,
			BaseTranRec baseTranRec,BaseRechargeRec baseRechargeRec,String payStatus, double indPriceJoint, double indPriceSnow) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketIndentDetail.class);
		dc.createAlias("ticketPrice", "ticketPrice");
		dc.createAlias("ticketPrice.ticketType", "ticketType");
		dc.add(Property.forName("ticketIndent.indentId").eq(ticketIndent.getIndentId()));
		dc.addOrder(Order.asc("indentDetailId"));//按订单明细排序保证同意订单每次进来查询的顺序一致
		List<TicketIndentDetail> tidList =  ticketIndentDetailDao.findByCriteria(dc);
		List<FinanceTicketAccounts> ticketAccounts = new ArrayList<FinanceTicketAccounts>();
		
		Timestamp thisTime = CommonMethod.getTimeStamp();
		
		//查询之前的财务记录用于计算新的记录应对应哪条订单明细
		DetachedCriteria dcFTA = DetachedCriteria.forClass(FinanceTicketAccounts.class);
		dcFTA.add(Property.forName("ticketIndentId").eq(ticketIndent.getIndentId()));
		dcFTA.addOrder(Order.asc("paymentType"));//排序将网银支付记录排在前面
		List<FinanceTicketAccounts> ftaList =  financeTicketAccountsDao.findByCriteria(dcFTA);
		//排除冲退和已被冲退的记录
		List<FinanceTicketAccounts> ftaListRe =  excludeReturnBackInfo(ftaList);
		Map<String,FinanceTicketAccounts> oldFtaMap = new HashMap<String,FinanceTicketAccounts>();
		for(FinanceTicketAccounts fta : ftaListRe){
			oldFtaMap.put("priceId_"+fta.getTicketPriceId(), fta);
		}
		
		
		//判断账目类型
		String accountsType = getAccountsType(baseRechargeRec);
		if("".equals(accountsType)){
			accountsType = ticketIndent.getPayType();
		}
		
		String iPtype = ticketIndent.getPayType();
		
		//判断支付类型
		String pType = "";
		
		if("05".equals(iPtype) || "06".equals(iPtype)){//微信或微博订单
			pType = FinanceTicketAccounts.PAYMENTTYPE_01;
		}else if("01".equals(iPtype)){//奖励款
			pType = FinanceTicketAccounts.PAYMENTTYPE_02;
			accountsType="01";
		}else if("02".equals(iPtype)){//现金
			String payType = baseTranRec.getPayType();
			accountsType="02";
			if("01".equals(payType)){//网银支付
				pType = FinanceTicketAccounts.PAYMENTTYPE_01;
			}else{
				if("00".equals(payStatus)){//未支付
					pType = FinanceTicketAccounts.PAYMENTTYPE_00;
				}else{
					pType = FinanceTicketAccounts.PAYMENTTYPE_02;
				}
			}
		}else if("04".equals(iPtype)){//预付款
			String payType = baseRechargeRec.getPaymentType();
			accountsType="04";
			if("03".equals(payType)){
				pType = FinanceTicketAccounts.PAYMENTTYPE_03;
			}else{
				if("00".equals(payStatus)){//未支付
					pType = FinanceTicketAccounts.PAYMENTTYPE_00;
				}else{
					pType = FinanceTicketAccounts.PAYMENTTYPE_02;
				}
			}
		}else{
			throw new BusinessException("错误的付款方式");
		}
		
		Long rechargeRecId = baseRechargeRec == null ? null : baseRechargeRec.getRechargeRecId();
		String tranRecId = baseTranRec == null ? null : baseTranRec.getTranRecId();
		//财务数据记录交易记录或充值记录中的数据
		double mJoint = 0;
		double mSnow = 0;
		
		if("05".equals(iPtype) || "06".equals(iPtype)){//微信或微博订单
			mJoint = indPriceJoint;
			mSnow = indPriceSnow;
		}else if(baseTranRec!=null){
			mJoint = baseTranRec.getMoneyJoint();
			mSnow = baseTranRec.getMoneySnow();
		}else{
			if("01".equals(accountsType)){
				mJoint = baseRechargeRec.getRewardMoney();
				mSnow = baseRechargeRec.getRewardMoneySnow();
			}else{
				mJoint = baseRechargeRec.getCashMoney();
				mSnow = baseRechargeRec.getCashMoneySnow();
			}
		}
		
		if(mJoint<0){
			mJoint = 0-mJoint;
		}
		if(mSnow<0){
			mSnow = 0-mSnow;
		}
		
		for (TicketIndentDetail detail : tidList) {
			//生成财务科目
			TicketType type = detail.getTicketPrice().getTicketType();
			
			int amount = detail.getAmount();
			
			double uPriceSnow = detail.getEntranceUnitPrice()==null ? 0 : detail.getEntranceUnitPrice();
			double uPrice = detail.getRopewayUnitPrice()==null ? 0 : detail.getRopewayUnitPrice();
			
			double moneySnow = uPriceSnow*amount;
			double money = uPrice*amount;
			
			//订单中金额为0时需要记录财务数据
			boolean tagSnow = moneySnow==0;
			boolean tag = money==0;
			
			
			//查询是否有对应的财务记录
			FinanceTicketAccounts oldFTA = oldFtaMap.get("priceId_"+detail.getTicketPrice().getTicketPriceId());
			if(oldFTA!=null){//有对应记录，需要减去金额和数量
				money = money-(oldFTA.getMoney()==null ? 0 : oldFTA.getMoney());
				moneySnow = moneySnow-(oldFTA.getMoneySnow()==null ? 0 : oldFTA.getMoneySnow());
				//amount = amount - (oldFTA.getAmount()==null ? 0 : oldFTA.getAmount());
			}
			
			//多条订单明细记录时，交易记录数据可能会大于订单明细金额。
			if(money>=mJoint){
				money = mJoint;
				mJoint = 0;
			}else{
				mJoint = mJoint - money;
			}
			if(moneySnow>=mSnow){
				moneySnow = mSnow;
				mSnow = 0;
			}else{
				mSnow = mSnow - moneySnow;
			}
			
			//计算数量
			int amountJoint = 0;
			int amountSnow = 0;
			
			if(money==0 && tag){//门票金额为0同时需要记录门票财务信息时门票数量与索道相同
			}else{
				amountJoint = CommonMethod.doubleToInt(money/uPrice);
			}
			
			if(moneySnow==0 && tagSnow){//门票金额为0同时需要记录门票财务信息时门票数量与索道相同
				amountSnow = amountJoint;
			}else{
				amountSnow = CommonMethod.doubleToInt(moneySnow/uPriceSnow);
			}
			
			if(amountJoint==0 && tag){
				amountJoint = amountSnow;
			}
			//特殊优惠票票大类和门票同样处理
			if("01".equals(type.getTicketBigType()) || "03".equals(type.getTicketBigType())){
				
				//门票 计入雪山公司
				if(moneySnow!=0 || tagSnow){
					FinanceTicketAccounts financeTicketAccountsSnow = new FinanceTicketAccounts(getNextKey("Finance_Ticket_Accounts", 1));
					financeTicketAccountsSnow.setAccountsType(accountsType);
					financeTicketAccountsSnow.setInputTime(thisTime);
					financeTicketAccountsSnow.setPaymentType(pType);
					financeTicketAccountsSnow.setRechargeRecId(rechargeRecId);
					financeTicketAccountsSnow.setTranRecId(tranRecId);
					financeTicketAccountsSnow.setTicketIndentId(ticketIndent.getIndentId());
					financeTicketAccountsSnow.setTicketType("01");
					financeTicketAccountsSnow.setTikcetIndentCode(ticketIndent.getIndentCode());
					financeTicketAccountsSnow.setMoneySnow(moneySnow);
					financeTicketAccountsSnow.setAmount(amountSnow);
					financeTicketAccountsSnow.setTicketPriceId(detail.getTicketPrice().getTicketPriceId());
					ticketAccounts.add(financeTicketAccountsSnow);
				}
				
				// 索道票 计入股份公司
				if(money!=0 || tag){
					FinanceTicketAccounts financeTicketAccounts = new FinanceTicketAccounts(getNextKey("Finance_Ticket_Accounts", 1));
					financeTicketAccounts.setAccountsType(accountsType);
					financeTicketAccounts.setInputTime(thisTime);
					financeTicketAccounts.setPaymentType(pType);
					financeTicketAccounts.setRechargeRecId(rechargeRecId);
					financeTicketAccounts.setTranRecId(tranRecId);
					financeTicketAccounts.setTicketIndentId(ticketIndent.getIndentId());
					financeTicketAccounts.setTicketType("02");
					financeTicketAccounts.setTikcetIndentCode(ticketIndent.getIndentCode());
					financeTicketAccounts.setTicketPriceId(detail.getTicketPrice().getTicketPriceId());
					financeTicketAccounts.setMoney(money);
					financeTicketAccounts.setAmount(amountJoint);
					ticketAccounts.add(financeTicketAccounts);
				}
				
			}else if("02".equals(type.getTicketBigType())){
				//项目票 计入股份公司
				if(money!=0 || tag){
					FinanceTicketAccounts financeTicketAccounts = new FinanceTicketAccounts(getNextKey("Finance_Ticket_Accounts", 1));
					financeTicketAccounts.setAccountsType(accountsType);
					financeTicketAccounts.setInputTime(thisTime);
					financeTicketAccounts.setPaymentType(pType);
					financeTicketAccounts.setRechargeRecId(rechargeRecId);
					financeTicketAccounts.setTranRecId(tranRecId);
					financeTicketAccounts.setTicketIndentId(ticketIndent.getIndentId());
					financeTicketAccounts.setTicketType(getFinanceTypeByTicketType(type));
					financeTicketAccounts.setTikcetIndentCode(ticketIndent.getIndentCode());
					financeTicketAccounts.setTicketPriceId(detail.getTicketPrice().getTicketPriceId());
					financeTicketAccounts.setMoney(money+moneySnow);
					financeTicketAccounts.setAmount(amountJoint);
					ticketAccounts.add(financeTicketAccounts);
				}
			}else{
				throw new BusinessException("错误的票大类");
			}
		}
		
		for(FinanceTicketAccounts fta : ticketAccounts){
			financeTicketAccountsDao.save(fta);
		}
		
	}
	
	@Override
	public void savePayInfo(TicketIndent ticketIndent, BaseTranRec baseTranRec) {
		
		Long rechargeRecId = null;
		String tranRecId = baseTranRec == null ? null : baseTranRec.getTranRecId();
		
		List<FinanceTicketAccounts> ticketAccounts = new ArrayList<FinanceTicketAccounts>();
		
		Timestamp thisTime = CommonMethod.getTimeStamp();
		
		List<TicketIndentDetail> tidList = ticketIndent.getDetails();
		
		//判断账目类型
		String accountsType = FinanceTicketAccounts.ACCOUNTSTYPE_03;
		
		//判断支付类型
		String pType = FinanceTicketAccounts.PAYMENTTYPE_01;
		
		
		//财务数据记录交易记录或充值记录中的数据
		double mJoint = baseTranRec.getMoneyJoint();
		double mSnow = baseTranRec.getMoneySnow();
		
		if(mJoint<0){
			mJoint = 0-mJoint;
		}
		if(mSnow<0){
			mSnow = 0-mSnow;
		}
		
		for (TicketIndentDetail detail : tidList) {
			//生成财务科目
			TicketType type = detail.getTicketPrice().getTicketType();
			
			int amount = detail.getAmount();
			
			double uPriceSnow = detail.getEntranceUnitPrice()==null ? 0 : detail.getEntranceUnitPrice();
			double uPrice = detail.getRopewayUnitPrice()==null ? 0 : detail.getRopewayUnitPrice();
			
			double moneySnow = uPriceSnow*amount;
			double money = uPrice*amount;
			
			//订单中金额为0时需要记录财务数据
			boolean tagSnow = moneySnow==0;
			boolean tag = money==0;
			
			//多条订单明细记录时，交易记录数据可能会大于订单明细金额。
			if(money>=mJoint){
				money = mJoint;
				mJoint = 0;
			}else{
				mJoint = mJoint - money;
			}
			if(moneySnow>=mSnow){
				moneySnow = mSnow;
				mSnow = 0;
			}else{
				mSnow = mSnow - moneySnow;
			}
			
			//计算数量
			int amountJoint = 0;
			int amountSnow = 0;
			
			if(money==0 && tag){//门票金额为0同时需要记录门票财务信息时门票数量与索道相同
			}else{
				amountJoint = CommonMethod.doubleToInt(money/uPrice);
			}
			
			if(moneySnow==0 && tagSnow){//门票金额为0同时需要记录门票财务信息时门票数量与索道相同
				amountSnow = amountJoint;
			}else{
				amountSnow = CommonMethod.doubleToInt(moneySnow/uPriceSnow);
			}
			
			if(amountJoint==0 && tag){
				amountJoint = amountSnow;
			}
			
			//支付
			//特殊优惠票票大类和门票同样处理
			if("01".equals(type.getTicketBigType()) || "03".equals(type.getTicketBigType())){
				
				//门票 计入雪山公司
				if(moneySnow!=0 || tagSnow){
					FinanceTicketAccounts financeTicketAccountsSnow = new FinanceTicketAccounts(getNextKey("Finance_Ticket_Accounts", 1));
					financeTicketAccountsSnow.setAccountsType(accountsType);
					financeTicketAccountsSnow.setInputTime(thisTime);
					financeTicketAccountsSnow.setPaymentType(pType);
					financeTicketAccountsSnow.setRechargeRecId(rechargeRecId);
					financeTicketAccountsSnow.setTranRecId(tranRecId);
					financeTicketAccountsSnow.setTicketIndentId(ticketIndent.getIndentId());
					financeTicketAccountsSnow.setTicketType("01");
					financeTicketAccountsSnow.setTikcetIndentCode(ticketIndent.getIndentCode());
					financeTicketAccountsSnow.setMoneySnow(moneySnow);
					financeTicketAccountsSnow.setAmount(amountSnow);
					financeTicketAccountsSnow.setTicketPriceId(detail.getTicketPrice().getTicketPriceId());
					ticketAccounts.add(financeTicketAccountsSnow);
				}
				
				// 索道票 计入股份公司
				if(money!=0 || tag){
					FinanceTicketAccounts financeTicketAccounts = new FinanceTicketAccounts(getNextKey("Finance_Ticket_Accounts", 1));
					financeTicketAccounts.setAccountsType(accountsType);
					financeTicketAccounts.setInputTime(thisTime);
					financeTicketAccounts.setPaymentType(pType);
					financeTicketAccounts.setRechargeRecId(rechargeRecId);
					financeTicketAccounts.setTranRecId(tranRecId);
					financeTicketAccounts.setTicketIndentId(ticketIndent.getIndentId());
					financeTicketAccounts.setTicketType("02");
					financeTicketAccounts.setTikcetIndentCode(ticketIndent.getIndentCode());
					financeTicketAccounts.setTicketPriceId(detail.getTicketPrice().getTicketPriceId());
					financeTicketAccounts.setMoney(money);
					financeTicketAccounts.setAmount(amountJoint);
					ticketAccounts.add(financeTicketAccounts);
				}
				
			}else if("02".equals(type.getTicketBigType())){
				//项目票 计入股份公司
				if(money!=0 || tag){
					FinanceTicketAccounts financeTicketAccounts = new FinanceTicketAccounts(getNextKey("Finance_Ticket_Accounts", 1));
					financeTicketAccounts.setAccountsType(accountsType);
					financeTicketAccounts.setInputTime(thisTime);
					financeTicketAccounts.setPaymentType(pType);
					financeTicketAccounts.setRechargeRecId(rechargeRecId);
					financeTicketAccounts.setTranRecId(tranRecId);
					financeTicketAccounts.setTicketIndentId(ticketIndent.getIndentId());
					financeTicketAccounts.setTicketType(getFinanceTypeByTicketType(type));
					financeTicketAccounts.setTikcetIndentCode(ticketIndent.getIndentCode());
					financeTicketAccounts.setTicketPriceId(detail.getTicketPrice().getTicketPriceId());
					financeTicketAccounts.setMoney(money+moneySnow);
					financeTicketAccounts.setAmount(amountJoint);
					ticketAccounts.add(financeTicketAccounts);
				}
			}else{
				throw new BusinessException("错误的票大类");
			}
			
		}
		
		for(FinanceTicketAccounts ftaO : ticketAccounts){
			//支付
			financeTicketAccountsDao.save(ftaO);
			
			//抵扣
			FinanceTicketAccounts ftaN = new FinanceTicketAccounts(getNextKey("Finance_Ticket_Accounts", 1));
			double ftaMoneyOld = ftaO.getMoney()==null ? 0 : ftaO.getMoney();
			double ftaMoneySnowOld = ftaO.getMoneySnow()==null ? 0 : ftaO.getMoneySnow();
			ftaN.setAccountsType(ftaO.getAccountsType());
			ftaN.setInputTime(thisTime);
			ftaN.setPaymentType(FinanceTicketAccounts.PAYMENTTYPE_10);
			ftaN.setRechargeRecId(rechargeRecId);
			ftaN.setTranRecId(tranRecId);
			ftaN.setTicketIndentId(ftaO.getTicketIndentId());
			ftaN.setTicketType(ftaO.getTicketType());
			ftaN.setTikcetIndentCode(ftaO.getTikcetIndentCode());
			ftaN.setMoney(0-ftaMoneyOld);
			ftaN.setMoneySnow(0-ftaMoneySnowOld);
			ftaN.setAmount(0-(ftaO.getAmount()==null ? 0 : ftaO.getAmount()));
			ftaN.setTicketPriceId(ftaO.getTicketPriceId());
			ftaN.setBackAccountsId(ftaO.getTicketAccountsId());
			financeTicketAccountsDao.save(ftaN);
			
			//消费
			FinanceTicketAccounts ftaXF = new FinanceTicketAccounts(getNextKey("Finance_Ticket_Accounts", 1));
			ftaXF.setAccountsType(ftaO.getAccountsType());
			ftaXF.setInputTime(thisTime);
			ftaXF.setPaymentType(FinanceTicketAccounts.PAYMENTTYPE_04);
			ftaXF.setRechargeRecId(rechargeRecId);
			ftaXF.setTranRecId(tranRecId);
			ftaXF.setTicketIndentId(ftaO.getTicketIndentId());
			ftaXF.setTicketType(ftaO.getTicketType());
			ftaXF.setTikcetIndentCode(ftaO.getTikcetIndentCode());
			ftaXF.setMoney(ftaMoneyOld);
			ftaXF.setMoneySnow(ftaMoneySnowOld);
			ftaXF.setAmount(ftaO.getAmount()==null ? 0 : ftaO.getAmount());
			ftaXF.setTicketPriceId(ftaO.getTicketPriceId());
			financeTicketAccountsDao.save(ftaXF);
		}
		
	}
	
	@Override
	public void savePrintBackInfo(TicketIndent oldIndent,TicketIndent newIndent,
			BaseTranRec baseTranRec,BaseRechargeRec baseRechargeRec) {
		List<FinanceTicketAccounts> ticketAccounts = new ArrayList<FinanceTicketAccounts>();
		
		Timestamp thisTime = CommonMethod.getTimeStamp();
		
		DetachedCriteria dc = DetachedCriteria.forClass(FinanceTicketAccounts.class);
		dc.add(Property.forName("ticketIndentId").eq(newIndent.getIndentId()));
		dc.addOrder(Order.desc("paymentType"));//排序将余额支付记录排在前面，优先扣余额支付的钱到实收
		List<FinanceTicketAccounts> ftaList =  financeTicketAccountsDao.findByCriteria(dc);
		
		Long rechargeRecId = baseRechargeRec == null ? null : baseRechargeRec.getRechargeRecId();
		String tranRecId = baseTranRec == null ? null : baseTranRec.getTranRecId();
		//排除冲退和已被冲退的记录
		List<FinanceTicketAccounts> ftaListRe =  excludeReturnBackInfo(ftaList);
		
		//将新旧订单明细数据放到map中用priceId做key
		List<TicketIndentDetail> oldDetails = oldIndent.getDetails();
		Map<String,TicketIndentDetail> oldDetailsMap = new HashMap<String,TicketIndentDetail>();
		for(TicketIndentDetail tid : oldDetails){
			oldDetailsMap.put("priceId_"+tid.getTicketPrice().getTicketPriceId(), tid);
		}
		
		List<TicketIndentDetail> newDetails = newIndent.getDetails();
		Map<String,TicketIndentDetail> newDetailsMap = new HashMap<String,TicketIndentDetail>();
		for(TicketIndentDetail tid : newDetails){
			newDetailsMap.put("priceId_"+tid.getTicketPrice().getTicketPriceId(), tid);
		}
		
		Map<String,Double> moneyNewMap = new HashMap<String,Double>();
		Map<String,Double> moneySnowNewMap = new HashMap<String,Double>();
		
		for(FinanceTicketAccounts ftaO : ftaListRe){
			
			if("01".equals(oldIndent.getPayType().trim())){//01奖励款退款
				rechargeRecId = rechargeRecId == null ? ftaO.getRechargeRecId() : rechargeRecId;
			}
			
			double ftaMoneyOld = ftaO.getMoney()==null ? 0 : ftaO.getMoney();
			double ftaMoneySnowOld = ftaO.getMoneySnow()==null ? 0 : ftaO.getMoneySnow();
			
			//先抵扣
			FinanceTicketAccounts ftaN = new FinanceTicketAccounts(getNextKey("Finance_Ticket_Accounts", 1));
			
			ftaN.setAccountsType(ftaO.getAccountsType());
			ftaN.setInputTime(thisTime);
			ftaN.setPaymentType(FinanceTicketAccounts.PAYMENTTYPE_10);
			ftaN.setRechargeRecId(rechargeRecId);
			ftaN.setTranRecId(tranRecId);
			ftaN.setTicketIndentId(newIndent.getIndentId());
			ftaN.setTicketType(ftaO.getTicketType());
			ftaN.setTikcetIndentCode(ftaO.getTikcetIndentCode());
			ftaN.setMoney(0-ftaMoneyOld);
			ftaN.setMoneySnow(0-ftaMoneySnowOld);
			ftaN.setAmount(0-(ftaO.getAmount()==null ? 0 : ftaO.getAmount()));
			ftaN.setTicketPriceId(ftaO.getTicketPriceId());
			ftaN.setBackAccountsId(ftaO.getTicketAccountsId());
			
			ticketAccounts.add(ftaN);
			
			
			//计算实收金额同时保存实收财务数据
			String key = "priceId_"+ftaO.getTicketPriceId();
			TicketIndentDetail oldDetail = oldDetailsMap.get(key);
			
			int amountOld = oldDetail.getAmount() == null ? 0 : oldDetail.getAmount();
			double moneyOld = (oldDetail.getRopewayUnitPrice() == null ? 0 : oldDetail.getRopewayUnitPrice())*amountOld;
			double moneySnowOld = (oldDetail.getEntranceUnitPrice() == null ? 0 : oldDetail.getEntranceUnitPrice())*amountOld;
			
			String paymentType = ftaO.getPaymentType();
			
			TicketIndentDetail newDetail = newDetailsMap.get(key);
			
			String tType = ftaO.getTicketType();
			
			double moneyNew = moneyNewMap.get(key) == null ? 0d : moneyNewMap.get(key);
			double moneySnowNew = moneySnowNewMap.get(key) == null ? 0d : moneySnowNewMap.get(key);
			
			if(newDetail!=null){//明细票没有全退需保存实收财务记录及退款记录
				int amountNew = newDetail.getAmount() == null ? 0 : newDetail.getAmount();
				
				double uPriceSnow = newDetail.getEntranceUnitPrice()==null ? 0 : newDetail.getEntranceUnitPrice();
				double uPrice = newDetail.getRopewayUnitPrice()==null ? 0 : newDetail.getRopewayUnitPrice();
				
				double money = uPrice*amountNew-moneyNew;
				double moneySnow = uPriceSnow*amountNew-moneySnowNew;
				
				double reMoney = 0d;
				double reMoneySnow = 0d;
				if(ftaMoneyOld==0d){
					money = 0;
				}else if(money>=ftaMoneyOld){
					moneyNew += ftaMoneyOld;
					money = ftaMoneyOld;
				}else{
					moneyNew += money;
					reMoney = ftaMoneyOld - money;
				}
				
				if(ftaMoneySnowOld==0d){
					moneySnow = 0;
				}else if(moneySnow>=ftaMoneySnowOld){
					moneySnowNew += ftaMoneySnowOld;
					moneySnow = ftaMoneySnowOld;
				}else{
					moneySnowNew += moneySnow;
					reMoneySnow = ftaMoneySnowOld - moneySnow;
				}
				
				moneyNewMap.put(key, moneyNew);
				moneySnowNewMap.put(key, moneySnowNew);
				
				//计算数量
				int amountJoint = 0;
				int amountSnow = 0;
				
				if(uPriceSnow==0 && uPrice==0){
					amountJoint=amountNew;
					amountSnow=amountNew;
				}else if(uPriceSnow==0){
					amountJoint = CommonMethod.doubleToInt(money/uPrice);
					
					if(moneySnow==0){//门票金额为0同时需要记录门票财务信息时门票数量与索道相同
						amountSnow = amountJoint;
					}else{
						amountSnow = CommonMethod.doubleToInt(moneySnow/uPriceSnow);
					}
				}else if(uPrice==0){
					amountSnow = CommonMethod.doubleToInt(moneySnow/uPriceSnow);
					
					if(money==0){//门票金额为0同时需要记录门票财务信息时门票数量与索道相同
						amountJoint = amountSnow;
					}else{
						amountJoint = CommonMethod.doubleToInt(money/uPrice);
					}
				}else{
					amountJoint=amountNew;
					amountSnow=amountNew;
				}
				
				
				if(money > 0d || moneySnow > 0d){
					FinanceTicketAccounts ftaReal = new FinanceTicketAccounts(getNextKey("Finance_Ticket_Accounts", 1));
					
					ftaReal.setAccountsType(ftaO.getAccountsType());
					ftaReal.setInputTime(thisTime);
					
					String paymentTypeReal="";
					if(FinanceTicketAccounts.PAYMENTTYPE_01.equals(paymentType)){//网银已支付未消费金额改为网银支付已消费金额
						paymentTypeReal = FinanceTicketAccounts.PAYMENTTYPE_04;
					}else if(FinanceTicketAccounts.PAYMENTTYPE_02.equals(paymentType)){//余额已支付未消费金额改为余额支付已消费金额
						paymentTypeReal = FinanceTicketAccounts.PAYMENTTYPE_05;
					}else{
						throw new BusinessException("不支持的付款类型","");
					}
					
					ftaReal.setPaymentType(paymentTypeReal);
					ftaReal.setRechargeRecId(rechargeRecId);
					ftaReal.setTranRecId(tranRecId);
					ftaReal.setTicketIndentId(newIndent.getIndentId());
					ftaReal.setTicketType(ftaO.getTicketType());
					ftaReal.setTikcetIndentCode(ftaO.getTikcetIndentCode());
					
					if("01".equals(tType)){//门票
						ftaReal.setMoneySnow(moneySnow);
						ftaReal.setAmount(amountSnow);
					}else{//其他票
						ftaReal.setMoney(money);
						ftaReal.setAmount(amountJoint);
					}
					
					ftaReal.setTicketPriceId(ftaO.getTicketPriceId());
					
					ticketAccounts.add(ftaReal);
				}
				
				if(reMoney > 0d || reMoneySnow > 0d){//有退票需做退款记录
					
					FinanceTicketAccounts ftaReturn = new FinanceTicketAccounts(getNextKey("Finance_Ticket_Accounts", 1));
					
					ftaReturn.setAccountsType(ftaO.getAccountsType());
					ftaReturn.setInputTime(thisTime);
					
					String paymentTypeReturn="";
					if(FinanceTicketAccounts.PAYMENTTYPE_01.equals(paymentType)){//网银已支付未消费金额改为网银付款退款到余额
						paymentTypeReturn = FinanceTicketAccounts.PAYMENTTYPE_07;
					}else if(FinanceTicketAccounts.PAYMENTTYPE_02.equals(paymentType)){//余额已支付未消费金额改为余额付款退款到余额
						paymentTypeReturn = FinanceTicketAccounts.PAYMENTTYPE_08;
					}else{
						throw new BusinessException("不支持的付款类型","");
					}
					
					ftaReturn.setPaymentType(paymentTypeReturn);
					ftaReturn.setRechargeRecId(rechargeRecId);
					ftaReturn.setTranRecId(tranRecId);
					ftaReturn.setTicketIndentId(newIndent.getIndentId());
					ftaReturn.setTicketType(ftaO.getTicketType());
					ftaReturn.setTikcetIndentCode(ftaO.getTikcetIndentCode());
					
					//计算数量
					int amountJointRe = amountJoint-(ftaO.getAmount()==null ? 0 : ftaO.getAmount());
					int amountSnowRe = amountSnow-(ftaO.getAmount()==null ? 0 : ftaO.getAmount());
					
					
					if("01".equals(tType)){//门票
						ftaReturn.setMoneySnow(0-reMoneySnow);
						ftaReturn.setAmount(amountSnowRe);
					}else{//其他票
						ftaReturn.setMoney(0-reMoney);
						ftaReturn.setAmount(amountJointRe);
					}
					ftaReturn.setTicketPriceId(ftaO.getTicketPriceId());
					
					ticketAccounts.add(ftaReturn);
					
				}
				
			}else{//明细全退，需做退款记录
				FinanceTicketAccounts ftaReturn = new FinanceTicketAccounts(getNextKey("Finance_Ticket_Accounts", 1));
				
				ftaReturn.setAccountsType(ftaO.getAccountsType());
				ftaReturn.setInputTime(thisTime);
				
				String paymentTypeReturn="";
				if(FinanceTicketAccounts.PAYMENTTYPE_01.equals(paymentType)){//网银已支付未消费金额改为网银付款退款到余额
					paymentTypeReturn = FinanceTicketAccounts.PAYMENTTYPE_07;
				}else if(FinanceTicketAccounts.PAYMENTTYPE_02.equals(paymentType)){//余额已支付未消费金额改为余额付款退款到余额
					paymentTypeReturn = FinanceTicketAccounts.PAYMENTTYPE_08;
				}else{
					throw new BusinessException("不支持的付款类型","");
				}
				
				ftaReturn.setPaymentType(paymentTypeReturn);
				ftaReturn.setRechargeRecId(rechargeRecId);
				ftaReturn.setTranRecId(tranRecId);
				ftaReturn.setTicketIndentId(newIndent.getIndentId());
				ftaReturn.setTicketType(ftaO.getTicketType());
				ftaReturn.setTikcetIndentCode(ftaO.getTikcetIndentCode());
				
				if("01".equals(tType)){//门票
					ftaReturn.setMoneySnow(0-moneySnowOld);
				}else{//其他票
					ftaReturn.setMoney(0-moneyOld);
				}
				
				ftaReturn.setAmount(0-ftaO.getAmount());
				ftaReturn.setTicketPriceId(ftaO.getTicketPriceId());
				
				ticketAccounts.add(ftaReturn);
			}
			
		}
		
		for(FinanceTicketAccounts fta : ticketAccounts){
			financeTicketAccountsDao.save(fta);
		}
		
	}
	
	
	@Override
	public void saveReturnInfo(TicketIndent ticketIndent,
			BaseTranRec baseTranRec,BaseRechargeRec baseRechargeRec) {
		List<FinanceTicketAccounts> ticketAccounts = new ArrayList<FinanceTicketAccounts>();
		
		Long rechargeRecId = baseRechargeRec == null ? null : baseRechargeRec.getRechargeRecId();
		int sdIndexOf = baseRechargeRec == null ? -1 : baseRechargeRec.getSourceDescription().indexOf("退现");
		String tranRecId = baseTranRec == null ? null : baseTranRec.getTranRecId();
		String returnPayType = baseTranRec == null ? null : baseTranRec.getPayType();
		
		DetachedCriteria dcFTA = DetachedCriteria.forClass(FinanceTicketAccounts.class);
		dcFTA.add(Property.forName("ticketIndentId").eq(ticketIndent.getIndentId()));
		dcFTA.addOrder(Order.asc("paymentType"));//排序将网银支付记录排在前面
		List<FinanceTicketAccounts> ftaList =  financeTicketAccountsDao.findByCriteria(dcFTA);
		
		//排除冲退和已被冲退的记录
		List<FinanceTicketAccounts> ftaListRe =  excludeReturnBackInfo(ftaList);
		
		Timestamp thisTime = CommonMethod.getTimeStamp();
		
		for(FinanceTicketAccounts ftaO : ftaListRe){
			
			int amountFtaOld = (ftaO.getAmount() == null ? 0 : ftaO.getAmount());
			double moneyFtaOld = (ftaO.getMoney() == null ? 0 : ftaO.getMoney());
			double moneyFtaSnowOld = (ftaO.getMoneySnow() == null ? 0 : ftaO.getMoneySnow());
			
			String paymentType = ftaO.getPaymentType();
			
			FinanceTicketAccounts ftaReturn = new FinanceTicketAccounts(getNextKey("Finance_Ticket_Accounts", 1));
			
			ftaReturn.setAccountsType(ftaO.getAccountsType());
			ftaReturn.setInputTime(thisTime);
			
			String paymentTypeReturn="";
			if(FinanceTicketAccounts.PAYMENTTYPE_01.equals(paymentType)){//网银已支付未消费金额改为网银付款退款到余额
				paymentTypeReturn = FinanceTicketAccounts.PAYMENTTYPE_07;
			}else if(FinanceTicketAccounts.PAYMENTTYPE_02.equals(paymentType)){//余额已支付未消费金额改为余额付款退款到余额
				paymentTypeReturn = FinanceTicketAccounts.PAYMENTTYPE_08;
			}else if(FinanceTicketAccounts.PAYMENTTYPE_00.equals(paymentType)){//订单预扣余额改为冲退
				paymentTypeReturn = FinanceTicketAccounts.PAYMENTTYPE_09;
			}else{
				throw new BusinessException("不支持的付款类型","");
			}
			if("01".equals(returnPayType) || sdIndexOf>0){//退现
				paymentTypeReturn = FinanceTicketAccounts.PAYMENTTYPE_06;
			}
			if("05".equals(ticketIndent.getPayType()) || "06".equals(ticketIndent.getPayType())){//微信或微博订单
				paymentTypeReturn = FinanceTicketAccounts.PAYMENTTYPE_06;
			}

			ftaReturn.setPaymentType(paymentTypeReturn);
			ftaReturn.setRechargeRecId(rechargeRecId);
			ftaReturn.setTranRecId(tranRecId);
			ftaReturn.setTicketIndentId(ticketIndent.getIndentId());
			ftaReturn.setTicketType(ftaO.getTicketType());
			ftaReturn.setTikcetIndentCode(ftaO.getTikcetIndentCode());
			ftaReturn.setMoney(0-moneyFtaOld);
			ftaReturn.setMoneySnow(0-moneyFtaSnowOld);
			ftaReturn.setAmount(0-amountFtaOld);
			ftaReturn.setTicketPriceId(ftaO.getTicketPriceId());
			
			ticketAccounts.add(ftaReturn);
			
		}
		
		for(FinanceTicketAccounts fta : ticketAccounts){
			financeTicketAccountsDao.save(fta);
		}
		
	}
	
	@Override
	public void saveReturnInfo(BaseRechargeRec baseRechargeRec) {
		
		Long rechargeRecId = baseRechargeRec == null ? null : baseRechargeRec.getRechargeRecId();
		
		Timestamp thisTime = Timestamp.valueOf(baseRechargeRec.getInputTime().toString());
		
		//先抵扣
//		FinanceTicketAccounts ftaDeduction = new FinanceTicketAccounts(getNextKey("Finance_Ticket_Accounts", 1));
//		
//		ftaDeduction.setAccountsType("02");
//		ftaDeduction.setInputTime(thisTime);
//
//		ftaDeduction.setPaymentType(FinanceTicketAccounts.PAYMENTTYPE_11);
//		ftaDeduction.setRechargeRecId(rechargeRecId);
//		ftaDeduction.setMoney(baseRechargeRec.getCashMoney());
//		ftaDeduction.setMoneySnow(baseRechargeRec.getCashMoneySnow());
//		financeTicketAccountsDao.save(ftaDeduction);
		
		//退现记录
		FinanceTicketAccounts ftaReturn = new FinanceTicketAccounts(getNextKey("Finance_Ticket_Accounts", 1));
		
		ftaReturn.setAccountsType("02");
		ftaReturn.setInputTime(thisTime);

		ftaReturn.setPaymentType(FinanceTicketAccounts.PAYMENTTYPE_06);
		ftaReturn.setRechargeRecId(rechargeRecId);
		ftaReturn.setMoney(baseRechargeRec.getCashMoney());
		ftaReturn.setMoneySnow(baseRechargeRec.getCashMoneySnow());
		financeTicketAccountsDao.save(ftaReturn);
		
	}


	@Override
	public void saveRewardOrCashTopUpInfo(BaseRechargeRec baseRechargeRec) {
		FinanceTicketAccounts financeTicketAccounts = new FinanceTicketAccounts(getNextKey("FINANCE_TICKET_ACCOUNTS", 1));
		//判断账目类型
		String accountsType = getAccountsType(baseRechargeRec);
		financeTicketAccounts.setAccountsType(accountsType);
		String paymentType = "03";
		
		//支付类型:
//		01网银已支付未消费金额，
//		02余额已支付未消费金额，
//		03现金充值金额，
//		04网银支付已消费金额，
//		05余额支付已消费金额，
//		06退款到卡上金额，
//		07网银付款退款到余额，
//		08余额付款退款到余额，
//		09冲退（做订单时暂扣余额在付款时需冲退，打印回传时如有退票需先冲退）
		double money = 0d;
		double moneySnow = 0d;
		if("01".equals(accountsType)){//奖励款
			money = baseRechargeRec.getRewardMoney() == null ? 0D : baseRechargeRec.getRewardMoney();
			moneySnow = baseRechargeRec.getRewardMoneySnow() == null ? 0D : baseRechargeRec.getRewardMoneySnow();
		}else if("02".equals(accountsType) || "04".equals(accountsType)){//现金或者现金充值
			money = baseRechargeRec.getCashMoney()== null ? 0D : baseRechargeRec.getCashMoney();
			moneySnow = baseRechargeRec.getCashMoneySnow() == null ? 0D : baseRechargeRec.getCashMoneySnow();
		}else{
			throw new BusinessException("不支持的账目类型");
		}
		financeTicketAccounts.setAccountsType(accountsType);
		financeTicketAccounts.setPaymentType(paymentType);
		financeTicketAccounts.setMoney(money);
		financeTicketAccounts.setMoneySnow(moneySnow);
		financeTicketAccounts.setInputTime(baseRechargeRec.getInputTime());
		financeTicketAccounts.setRechargeRecId(baseRechargeRec.getRechargeRecId());
		financeTicketAccountsDao.save(financeTicketAccounts);
	}
	
	@Override
	public JsonPager<StatementObjectForPage> findCountStatement(StatementObject fta) {
		JsonPager<StatementObjectForPage> jp = new JsonPager<StatementObjectForPage>();
		List<StatementObjectForPage> taList = financeTicketAccountsDao.findTotalStatement(fta);
		jp.setRoot(taList);
		jp.setTotal(taList.size());
		return jp;
	}
	
	@Override
	public List<AllStatementObject> findAllStatement(StatementObject fta){
		return financeTicketAccountsDao.findAllStatement(fta);
	}
	
	
	@Override
	public JsonPager<StatementObjectForPage> findDetailStatement(StatementObject fta) {
		JsonPager<StatementObjectForPage> jp = new JsonPager<StatementObjectForPage>();
		List<StatementObjectForPage> taList = financeTicketAccountsDao.findDeatilStatement(fta);
		jp.setRoot(taList);
		jp.setTotal(taList.size());
		return jp;
	}
	
	@Override
	public JsonPager<StatementObject> findDetailStatement(int start,int limit, StatementObject statementObject) {
		return financeTicketAccountsDao.findDeatilOfRecharge(start,limit, statementObject);
	}
	
	private String getFinanceTypeByTicketType(TicketType type) {
		//gate:01交通索道闸口，02普通闸口，03观景索道闸口，04滑雪场闸口，05雪地摩托闸口，07项目票套票闸口
		//票类型：01门票，02索道票，03观景索道票，04滑雪票，05雪地摩托票，06滑草票  07项目票套票
		String gate = type.getGate();
		if("03".equals(gate)){
			return gate;
		}else if("04".equals(gate)){
			return gate;
		}else if("05".equals(gate)){
			return gate;
		}else if("06".equals(gate)){
			return gate;
		}else if("07".equals(gate)){
			return gate;
		}else{
			throw new BusinessException("错误的闸口类型");
		}
	}
	
	
	private String getAccountsType(BaseRechargeRec baseRechargeRec){
//		String accountsType = FinanceTicketAccounts.ACCOUNTSTYPE_02;
//		if(baseRechargeRec!=null && baseRechargeRec.getRewardMoney()!=null){//奖励款
//			accountsType = FinanceTicketAccounts.ACCOUNTSTYPE_01;
//		}
		String accountsType ="";
		if(baseRechargeRec!=null && "02".equals(baseRechargeRec.getFundType())){//奖励款
			accountsType = FinanceTicketAccounts.ACCOUNTSTYPE_01;
		}else if(baseRechargeRec!=null && "01".equals(baseRechargeRec.getFundType())){//现金
			accountsType = FinanceTicketAccounts.ACCOUNTSTYPE_02;
		}else if(baseRechargeRec!=null && "03".equals(baseRechargeRec.getFundType())){//现金充值
			accountsType = FinanceTicketAccounts.ACCOUNTSTYPE_04;
		}
		return accountsType;
	}
}