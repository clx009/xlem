package service.ticket.financeTicketAccounts;
import java.util.List;

import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.BaseTranRec;
import com.xlem.dao.FinanceTicketAccounts;
import com.xlem.dao.TicketIndent;

import common.action.JsonPager;
import common.service.BaseService;

import dao.ticket.financeTicketAccounts.AllStatementObject;
import dao.ticket.financeTicketAccounts.StatementObject;
import dao.ticket.financeTicketAccounts.StatementObjectForPage;
public interface FinanceTicketAccountsService extends BaseService<FinanceTicketAccounts> {

	/**
	 * 保存冲退财务信息，有财务信息则冲退，没有则不做操作
	 * @param ticketIndent 订单信息
	 * @param baseTranRec 交易信息
	 * @param baseRechargeRec 充值退款信息
	 */
	public void saveReturnBackInfo(TicketIndent ticketIndent, BaseTranRec baseTranRec, BaseRechargeRec baseRechargeRec);
	
	/**
	 * 将预付记录转为已付记录 流程：1冲退预付记录，2添加已付记录
	 * @param ticketIndent  订单信息
	 */
	public void savePayToPaid(TicketIndent ticketIndent);
	
	/**
	 * 保存付款财务信息
	 * @param ticketIndent 订单信息
	 * @param baseTranRec 交易信息
	 * @param baseRechargeRec 充值退款信息
	 * @param payStatus 订单状态：00未支付、11已支付
	 */
	void savePayInfo(TicketIndent ticketIndent, BaseTranRec baseTranRec, BaseRechargeRec baseRechargeRec,String payStatus, double indPriceJoint, double indPriceSnow);
	
	/**
	 * 保存自助购票机财务信息
	 * @param ticketIndent 订单信息
	 * @param baseTranRec 交易信息
	 */
	void savePayInfo(TicketIndent ticketIndent, BaseTranRec baseTranRec);
	
	/**
	 * 打印回传财务信息
	 * @param oldIndent 订单信息
	 * @param newIndent 回传的 订单信息
	 * @param baseTranRec 交易信息
	 * @param baseRechargeRec 充值退款信息
	 */
	public void savePrintBackInfo(TicketIndent oldIndent,TicketIndent newIndent,
			BaseTranRec baseTranRec,BaseRechargeRec baseRechargeRec);
	
	/**
	 * 保存退票财务信息
	 * @param ticketIndent 订单信息
	 * @param baseTranRec 交易信息
	 * @param baseRechargeRec 充值退款信息
	 */
	void saveReturnInfo(TicketIndent ticketIndent, BaseTranRec baseTranRec, BaseRechargeRec baseRechargeRec);
	
	/**
	 * 保存退现财务信息
	 * @param baseRechargeRec 充值退款信息
	 */
	void saveReturnInfo(BaseRechargeRec baseRechargeRec);
	
	/**
	 * 保存奖励款充值和现金充值  财务信息
	 * @param baseRechargeRec 充值记录
	 */
	void saveRewardOrCashTopUpInfo( BaseRechargeRec baseRechargeRec);
	/**
	 * 财务报表总报表
	 */
	JsonPager<StatementObjectForPage> findCountStatement(StatementObject fta);
	
	
	/**
	 * 财务报表总报表 肖宇翔
	 */
	List<AllStatementObject> findAllStatement(StatementObject fta);
	
	
	
	/**
	 * 财务报表明细表(网银支付以消费金额)
	 */
	JsonPager<StatementObjectForPage> findDetailStatement(StatementObject fta);
	/***
	 * 财务报表明细表（现金充值明细）
	 * @param pager
	 * @param fta
	 * @return
	 */
	JsonPager<StatementObject> findDetailStatement(int start,int limit, StatementObject fta);
}