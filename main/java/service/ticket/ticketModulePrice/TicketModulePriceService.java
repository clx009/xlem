package service.ticket.ticketModulePrice;
import java.util.Map;

import com.xlem.dao.TicketModulePrice;

import common.action.JsonPager;
import common.service.BaseService;

public interface TicketModulePriceService extends BaseService<TicketModulePrice> {

	void save(String json);
	void savePauseDate(TicketModulePrice t);
	public JsonPager<Map> findJsonMapPageByCriteria(JsonPager<Map> jp, TicketModulePrice t);
	Map<String,Object> findCheck(String json);

}