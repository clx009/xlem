package service.ticket.ticketModulePrice;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.TicketDiscount;
import com.xlem.dao.TicketModule;
import com.xlem.dao.TicketModulePrice;
import com.xlem.dao.TicketPrice;
import com.xlem.dao.TicketType;

import ws.ReturnInfo;
import ws.client.ticketIndent.TicketIndentClient;

import common.BusinessException;
import common.CommonMethod;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.jsonProcessor.CommonJsonConfig;
import common.service.BaseServiceImpl;

import dao.ticket.ticketDiscount.TicketDiscountDao;
import dao.ticket.ticketModule.TicketModuleDao;
import dao.ticket.ticketModulePrice.TicketModulePriceDao;
import dao.ticket.ticketPrice.TicketPriceDao;
import dao.ticket.ticketType.TicketTypeDao;

@Service("ticketModulePriceService")
public class TicketModulePriceServiceImpl extends BaseServiceImpl<TicketModulePrice>
		implements TicketModulePriceService {
	@Autowired
	private TicketModulePriceDao ticketModulePriceDao;
	@Autowired
	private TicketModuleDao ticketModuleDao;
	@Autowired
	private TicketTypeDao ticketTypeDao;
	@Autowired
	private TicketDiscountDao ticketDiscountDao;
	@Autowired
	private TicketPriceDao ticketPriceDao;
	
	
	private static Logger logger = Logger.getLogger("bookingService");
	
	@Override
	@Resource(name = "ticketModulePriceDao")
	protected void initBaseDAO(BaseDao<TicketModulePrice> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<TicketModulePrice> findPageByCriteria(
			PaginationSupport<TicketModulePrice> ps, TicketModulePrice t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketModulePrice.class);
		return ticketModulePriceDao.findPageByCriteria(ps, Order.desc("ticketModulePriceId"), dc);
	}

	@Override
	public void save(TicketModulePrice t) {
		t.setTicketModulePriceId(getNextKey("Ticket_Module_Price".toUpperCase(), 1));
		ticketModulePriceDao.save(t);
	}

	@Override
	public JsonPager<TicketModulePrice> findJsonPageByCriteria(
			JsonPager<TicketModulePrice> jp, TicketModulePrice t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketModulePrice.class);
		dc.addOrder(Order.asc("ticketModulePriceId"));
		return ticketModulePriceDao.findJsonPageByCriteria(jp, dc);
	}
	
	@Override
	public JsonPager<Map> findJsonMapPageByCriteria(JsonPager<Map> jp, TicketModulePrice t) {
		Map map = ticketModulePriceDao.findModulePriceListCount(getCurrentUserId());
		jp.setTotal(Integer.valueOf(map.get("total").toString()));
		jp.setRoot(ticketModulePriceDao.findModulePriceList(t, getCurrentUserId(), jp, (List)map.get("timeList")));
		return jp;
	}

	@Override
	public void save(String json) {
		
		JSONArray js =JSONArray.fromObject(json);
		CommonJsonConfig jsonConfig = new CommonJsonConfig();

		List<HashMap> jsList = JSONArray.toList(js, new HashMap(), jsonConfig);
		
		List<TicketModule> tmList = ticketModuleDao.findAll();
		
		List<TicketModulePrice> tmpList = new ArrayList<TicketModulePrice>();
		List startDateList = new ArrayList();
		for (HashMap teMap : jsList) {
			
			String startDate = teMap.get("startDate").toString();
			startDateList.add(CommonMethod.toStartTime(CommonMethod.string2Time1(startDate)));
			String endDate = teMap.get("endDate").toString();
			String inputer = teMap.get("inputer").toString();
			String inputTime = teMap.get("inputTime").toString();
			
			for (TicketModule ticketModule : tmList) {
				String sta = ticketModule.getStatus();
				if("00".equals(sta) || !"01".equals(ticketModule.getTicketBigType())){
					continue;
				}
				
				long tmId = ticketModule.getTicketModuleId();
				
//				if(teMap.get("unitPrice_"+tmId)!=null && !teMap.get("unitPrice_"+tmId).equals("") &&
//						teMap.get("unitPriceGroup_"+tmId)!=null && !teMap.get("unitPriceGroup_"+tmId).equals("") &&
//						teMap.get("printPrice_"+tmId)!=null && !teMap.get("printPrice_"+tmId).equals("")){
					
					TicketModulePrice tmpTemp = new TicketModulePrice();
					
					if(teMap.get("ticketModulePriceId_"+tmId)!=null && !teMap.get("ticketModulePriceId_"+tmId).equals("") && !teMap.get("ticketModulePriceId_"+tmId).equals("undefined")){
						String ticketModulePriceId = teMap.get("ticketModulePriceId_"+tmId).toString();
						tmpTemp.setTicketModulePriceId(Long.valueOf(ticketModulePriceId));
					}
					
					if("".equals(teMap.get("unitPrice_"+tmId))){
						teMap.put("unitPrice_"+tmId,"0");
					}
					if(teMap.get("unitPrice_"+tmId)!=null && !teMap.get("unitPrice_"+tmId).equals("")){
						String unitPrice = teMap.get("unitPrice_"+tmId).toString();
						tmpTemp.setUnitPrice(Double.valueOf(unitPrice));
					}
					if("".equals(teMap.get("unitPriceGroup_"+tmId))){
						teMap.put("unitPriceGroup_"+tmId,"0");
					}
					if(teMap.get("unitPriceGroup_"+tmId)!=null && !teMap.get("unitPriceGroup_"+tmId).equals("")){
						String unitPriceGroup = teMap.get("unitPriceGroup_"+tmId).toString();
						tmpTemp.setUnitPriceGroup(Double.valueOf(unitPriceGroup));
					}
					if("".equals(teMap.get("printPrice_"+tmId))){
						teMap.put("printPrice_"+tmId,"0");
					}
					if(teMap.get("printPrice_"+tmId)!=null && !teMap.get("printPrice_"+tmId).equals("")){
						String printPrice = teMap.get("printPrice_"+tmId).toString();
						tmpTemp.setPrintPrice(Double.valueOf(printPrice));
					}
					
					tmpTemp.setStartDate(CommonMethod.toStartTime(CommonMethod.string2Time1(startDate)));
					tmpTemp.setEndDate(CommonMethod.toEndTime(CommonMethod.string2Time1(endDate)));
					tmpTemp.setTicketModule(ticketModule);
					if(inputer!=null && !inputer.isEmpty()){
						tmpTemp.setInputer(Long.valueOf(inputer));
					}
					if(inputTime!=null && !inputTime.isEmpty()){
						tmpTemp.setInputTime(CommonMethod.string2Time1(inputTime));
					}
					tmpList.add(tmpTemp);
//				}
			}
//			System.out.println("================================");
//			System.out.println(teMap);
			
		}
		
		
		//System.out.println(tmpList);
		//保存票类型价格
		Map<Long,Map<Timestamp,TicketModulePrice>> tmMap = new HashMap<Long,Map<Timestamp,TicketModulePrice>>();
		for (TicketModulePrice ticketModulePrice : tmpList) {
			if (ticketModulePrice.getTicketModulePriceId() == null) {
				ticketModulePrice.setTicketModulePriceId(getNextKey("ticket_Module_Price", 1));
				ticketModulePrice.setStatus("11");
				ticketModulePrice.setInputer(getCurrentUserId());
				ticketModulePrice.setInputTime(CommonMethod.getTimeStamp());
				ticketModulePriceDao.save(ticketModulePrice);
			} else {
				ticketModulePrice.setUpdater(getCurrentUserId());
				ticketModulePrice.setUpdateTime(CommonMethod.getTimeStamp());
				ticketModulePriceDao.update(ticketModulePrice);
			}
			Map<Timestamp,TicketModulePrice> temp = tmMap.get(ticketModulePrice.getTicketModule().getTicketModuleId());
			if(temp==null){
				temp = new HashMap<Timestamp,TicketModulePrice>();
			}
			temp.put(Timestamp.valueOf(ticketModulePrice.getStartDate().toString()), ticketModulePrice);
			tmMap.put(ticketModulePrice.getTicketModule().getTicketModuleId(), temp);
		}
		
		//组合票价
		List<TicketDiscount> tdList = ticketDiscountDao.findAll();
		Map<Long,TicketDiscount> tdMap = new HashMap<Long,TicketDiscount>();
		for (TicketDiscount ticketDiscount : tdList) {
			tdMap.put(ticketDiscount.getTicketDiscountId(), ticketDiscount);
		}
		
		DetachedCriteria dcTP = DetachedCriteria.forClass(TicketPrice.class);
		dcTP.createAlias("ticketType", "ticketType");
		dcTP.add(Property.forName("startDate").in(startDateList));
		dcTP.add(Property.forName("ticketType.specialType").ne("04"));
		List<TicketPrice> tpOldList = ticketPriceDao.findByCriteria(dcTP);
		Map<Long,Map<Timestamp,TicketPrice>> tpMap = new HashMap<Long,Map<Timestamp,TicketPrice>>();
		for (TicketPrice ticketPrice : tpOldList) {
			Map<Timestamp,TicketPrice> temp = tpMap.get(ticketPrice.getTicketType().getTicketId());
			
			if(temp==null){
				temp = new HashMap<Timestamp,TicketPrice>();
			}
			temp.put(Timestamp.valueOf(ticketPrice.getStartDate().toString()), ticketPrice);
			tpMap.put(ticketPrice.getTicketType().getTicketId(), temp);
		}
		
		List<TicketType> ttList = ticketTypeDao.findAll();
		
		List<TicketPrice> tpList = new ArrayList<TicketPrice>();
		for (TicketType ticketType : ttList) {
			Long tdID = ticketType.getTicketDiscountId();
			if(tdID==null){
				continue;
			}
			long tdId = Long.valueOf(tdID);
			double dis = 1d;//默认折扣为1折
			TicketDiscount ticketDiscount = tdMap.get(tdId);
			if(ticketDiscount!=null){
				dis = ticketDiscount.getDiscount();
			}
			
			String ttM = ticketType.getTicketModuleId();
			if(ttM==null || ttM.isEmpty()){
				continue;
			}
			String[] ttmS = ttM.split("\\|");
			Map<Timestamp,TicketPrice> tempTP = tpMap.get(ticketType.getTicketId());
			if(tempTP==null){
				tempTP = new HashMap<Timestamp,TicketPrice>();
			}
			
			if(ttmS.length==1){//单独门票
				long tm = Long.valueOf(ttmS[0]);
				Map<Timestamp,TicketModulePrice> temp = tmMap.get(tm);
				if(temp==null){
					continue;
				}
				for (Timestamp startDate : temp.keySet()) {
					
					TicketPrice ticketPrice = tempTP.get(startDate);
					if(ticketPrice==null){
						ticketPrice=new TicketPrice();
						ticketPrice.setTicketType(ticketType);
						ticketPrice.setStatus("11");
					}
					TicketModulePrice TicketModulePrice = temp.get(startDate);
					
					String cType = ticketType.getCustomerType();
					
					Double unitPrice = "01".equals(cType) ? TicketModulePrice.getUnitPriceGroup() : TicketModulePrice.getUnitPrice();
					if(unitPrice!=null){
						
						if(TicketModulePrice.getPrintPrice()==null){//如果打印价未填
							if(TicketModulePrice.getUnitPrice()!=null){
								ticketPrice.setPrintPrice(TicketModulePrice.getUnitPrice()*dis);
							}else if(TicketModulePrice.getUnitPriceGroup()!=null){
								ticketPrice.setPrintPrice(TicketModulePrice.getUnitPriceGroup()*dis);
							}
						}else{
							ticketPrice.setPrintPrice(TicketModulePrice.getPrintPrice()*dis);
						}
						
						if(dis==1d){
							ticketPrice.setUnitPrice(unitPrice*dis);
						}else{//半价及免费票按打印价执行
							ticketPrice.setUnitPrice(TicketModulePrice.getPrintPrice()*dis);
						}
						
						String com = TicketModulePrice.getTicketModule().getCompany();
						if("01".equals(com)){//雪山公司
							ticketPrice.setEntrancePrice(TicketModulePrice.getPrintPrice());
							ticketPrice.setEntranceUnitPrice(ticketPrice.getUnitPrice());
							ticketPrice.setEntranceCompany(com);
							
							ticketPrice.setRopewayPrice(0d);
							ticketPrice.setRopewayUnitPrice(0d);
						}else if("02".equals(com)){//股份公司
							ticketPrice.setRopewayPrice(TicketModulePrice.getPrintPrice());
							ticketPrice.setRopewayUnitPrice(ticketPrice.getUnitPrice());
							ticketPrice.setRopewayCompany(com);
							
							ticketPrice.setEntrancePrice(0d);
							ticketPrice.setEntranceUnitPrice(0d);
						}
						
						
						ticketPrice.setStartDate(TicketModulePrice.getStartDate());
						ticketPrice.setEndDate(TicketModulePrice.getEndDate());
						
						tpList.add(ticketPrice);
					}
					
				}
			}else{//组合门票
				long tm = Long.valueOf(ttmS[0]);
				Map<Timestamp,TicketModulePrice> temp = tmMap.get(tm);
				if(temp==null){
					continue;
				}
				for (Timestamp startDate : temp.keySet()) {
					
					long tm2 = Long.valueOf(ttmS[1]);
					Map<Timestamp,TicketModulePrice> temp2 = tmMap.get(tm2);
					if(temp2==null){
						continue;
					}
					TicketModulePrice TicketModulePrice2 = temp2.get(startDate);
					
					TicketPrice ticketPrice = tempTP.get(startDate);
					if(ticketPrice==null){
						ticketPrice=new TicketPrice();
						ticketPrice.setTicketType(ticketType);
						ticketPrice.setStatus("11");
					}
					TicketModulePrice TicketModulePrice = temp.get(startDate);
					
					String cType = ticketType.getCustomerType();
					
					//用打印价格计算
					Double unitPrice = "01".equals(cType) ? TicketModulePrice.getUnitPriceGroup() : TicketModulePrice.getUnitPrice();
					Double unitPrice2 = "01".equals(cType) ? TicketModulePrice2.getUnitPriceGroup() : TicketModulePrice2.getUnitPrice();
					if(unitPrice!=null && unitPrice2!=null){
						double up = 0;
						if(TicketModulePrice.getPrintPrice()==null){//如果打印价未填
							if(TicketModulePrice.getUnitPrice()!=null){
								up = TicketModulePrice.getUnitPrice();
							}else if(TicketModulePrice.getUnitPriceGroup()!=null){
								up = TicketModulePrice.getUnitPriceGroup();
							}
						}else{
							up = TicketModulePrice.getPrintPrice();
						}
						double up2 = 0;
						if(TicketModulePrice2.getPrintPrice()==null){//如果打印价未填
							if(TicketModulePrice2.getUnitPrice()!=null){
								up2 = TicketModulePrice2.getUnitPrice();
							}else if(TicketModulePrice2.getUnitPriceGroup()!=null){
								up2 = TicketModulePrice2.getUnitPriceGroup();
							}
						}else{
							up2 = TicketModulePrice2.getPrintPrice();
						}
						
						String com = TicketModulePrice.getTicketModule().getCompany();
						String com2 = TicketModulePrice2.getTicketModule().getCompany();
						
						ticketPrice.setEntranceUnitPrice(0D);
						ticketPrice.setRopewayUnitPrice(0D);
						
						if(dis==1d){
							double unitPriceT = unitPrice*dis + unitPrice2;
							ticketPrice.setUnitPrice(unitPriceT);
							if("01".equals(com)){
								ticketPrice.setEntranceUnitPrice(ticketPrice.getEntranceUnitPrice()+unitPrice*dis);
							}else if("02".equals(com)){
								ticketPrice.setRopewayUnitPrice(ticketPrice.getRopewayUnitPrice()+unitPrice*dis);
							}
							
							if("01".equals(com2)){
								ticketPrice.setEntranceUnitPrice(ticketPrice.getEntranceUnitPrice()+unitPrice2);
							}else if("02".equals(com2)){
								ticketPrice.setRopewayUnitPrice(ticketPrice.getRopewayUnitPrice()+unitPrice2);
							}
							
						}else{//半价及免费票按打印价执行
							ticketPrice.setUnitPrice(up*dis+up2);
							if("01".equals(com)){
								ticketPrice.setEntranceUnitPrice(ticketPrice.getEntranceUnitPrice()+up*dis);
							}else if("02".equals(com)){
								ticketPrice.setRopewayUnitPrice(ticketPrice.getRopewayUnitPrice()+up*dis);
							}
							
							if("01".equals(com2)){
								ticketPrice.setEntranceUnitPrice(ticketPrice.getEntranceUnitPrice()+up2);
							}else if("02".equals(com2)){
								ticketPrice.setRopewayUnitPrice(ticketPrice.getRopewayUnitPrice()+up2);
							}
						}
						
						ticketPrice.setEntrancePrice(0D);
						ticketPrice.setRopewayPrice(0D);
						
						if("01".equals(com)){
							ticketPrice.setEntrancePrice(up*dis);
						}else if("02".equals(com)){
							ticketPrice.setRopewayPrice(up*dis);
						}
						
						if("01".equals(com2)){
							ticketPrice.setEntrancePrice(up2);
						}else if("02".equals(com2)){
							ticketPrice.setRopewayPrice(up2);
						}
						
						ticketPrice.setEntranceCompany("01");
						ticketPrice.setRopewayCompany("02");
						
						ticketPrice.setPrintPrice(up*dis+up2);
						ticketPrice.setStartDate(TicketModulePrice.getStartDate());
						ticketPrice.setEndDate(TicketModulePrice.getEndDate());
						
						tpList.add(ticketPrice);
					}
				}
			}
			
		}
		
		for (TicketPrice ticketPrice : tpList) {
			ticketPrice.setStatus("00");//价格变动需要审核，全改为未启用
			if (ticketPrice.getTicketPriceId() == null) {
				ticketPrice.setTicketPriceId(getNextKey("Ticket_Price", 1));
				ticketPrice.setInputer(getCurrentUserId());
				ticketPrice.setInputTime(CommonMethod.getTimeStamp());
				ticketPriceDao.save(ticketPrice);
			} else {
				ticketPrice.setUpdater(getCurrentUserId());
				ticketPrice.setUpdateTime(CommonMethod.getTimeStamp());
				ticketPriceDao.update(ticketPrice);
			}
		}
		
		TicketIndentClient tic = new TicketIndentClient();
		try{
			ReturnInfo ri = tic.writeTicketPrice(tpList);
			if(ri.getResult().trim().equals("0")){//接口调用成功
			}
			else{
				logger.error("同步票价出错:"+ri.getError());
				throw new BusinessException("网络中断,请稍后再试!");
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			logger.error("同步票价出错:"+e.getMessage());
			throw new BusinessException(e);
		}
		
		
	}
	
	@Override
	public void savePauseDate(TicketModulePrice t) {
		
//		System.out.println(t.getStartDate());
		
		Timestamp pauseStartDate = null;
		Timestamp pauseEndDate = null;
		if(t!=null && t.getStartDate()!=null){
			pauseStartDate = CommonMethod.toStartTime(Timestamp.valueOf(t.getStartDate().toString()));
		}else{
			return;
		}
		if(t!=null && t.getEndDate()!=null){
			pauseEndDate = CommonMethod.toEndTime(Timestamp.valueOf(t.getEndDate().toString()));
		}else{
			return;
		}
		
//		System.out.println(CommonMethod.toEndTime(pauseStartDate));
//		System.out.println(CommonMethod.toStartTime(pauseEndDate));
		
		DetachedCriteria dcM = DetachedCriteria.forClass(TicketModulePrice.class);
		dcM.createAlias("ticketModule", "ticketModule");
		dcM.add(Property.forName("startDate").le(CommonMethod.toEndTime(Timestamp.valueOf(t.getStartDate().toString()))));//开始时间<=当前时间
		dcM.add(Property.forName("endDate").ge(CommonMethod.toStartTime(Timestamp.valueOf(t.getStartDate().toString()))));//结束时间>=当前时间
//		dcM.add(Property.forName("ticketModule.status").eq("11"));
		List<TicketModulePrice> tmpList = ticketModulePriceDao.findByCriteria(dcM);
//		System.out.println(tmpList.size());
		
		List<TicketModulePrice> updateTmpList = new ArrayList<TicketModulePrice>();
		List<TicketModulePrice> saveTmpList = new ArrayList<TicketModulePrice>();
		List<TicketModulePrice> deleteTmpList = new ArrayList<TicketModulePrice>();
		for(TicketModulePrice tm : tmpList){
			Map<String,Map<String,Timestamp>> reMap = this.checkTim(pauseStartDate, pauseEndDate, Timestamp.valueOf(tm.getStartDate().toString()), Timestamp.valueOf(tm.getEndDate().toString()));
			
			Map<String,Timestamp> updateMap = reMap.get("update");
			Map<String,Timestamp> saveMap = reMap.get("save");
			Map<String,Timestamp> deleteMap = reMap.get("delete");
			
			if(updateMap!=null){
				tm.setStartDate(updateMap.get("startDate"));
				tm.setEndDate(updateMap.get("endDate"));
				updateTmpList.add(tm);
			}
			if(saveMap!=null){
				TicketModulePrice tmp = new TicketModulePrice(getNextKey("ticket_Module_Price", 1), tm.getTicketModule(), tm.getUnitPrice(), tm.getUnitPriceGroup(), 
										tm.getPrintPrice(), saveMap.get("startDate"), saveMap.get("endDate"), tm.getStatus(), tm.getRemark(), 
										tm.getInputer(), Timestamp.valueOf(tm.getInputTime().toString()), tm.getUpdater(), Timestamp.valueOf(tm.getUpdateTime().toString()));
				saveTmpList.add(tmp);
			}
			if(deleteMap!=null){
				tm.setStatus("00");
				deleteTmpList.add(tm);
			}
			
		}
		
		for(TicketModulePrice tm : updateTmpList){
			ticketModulePriceDao.update(tm);
		}
		for(TicketModulePrice tm : saveTmpList){
			ticketModulePriceDao.save(tm);
		}
		for(TicketModulePrice tm : deleteTmpList){
			ticketModulePriceDao.update(tm);
		}
		
		DetachedCriteria dc = DetachedCriteria.forClass(TicketPrice.class);
		dc.createAlias("ticketType", "ticketType");
		dc.add(Property.forName("startDate").le(CommonMethod.toEndTime(Timestamp.valueOf(t.getStartDate().toString()))));//开始时间<=当前时间
		dc.add(Property.forName("endDate").ge(CommonMethod.toStartTime(Timestamp.valueOf(t.getStartDate().toString()))));//结束时间>=当前时间
//		dc.add(Property.forName("status").eq("11"));
//		dc.add(Property.forName("ticketType.status").eq("11"));
		List<TicketPrice> tpList = ticketPriceDao.findByCriteria(dc);
//		System.out.println(tpList.size());
		
		List<TicketPrice> updateTpList = new ArrayList<TicketPrice>();
		List<TicketPrice> saveTpList = new ArrayList<TicketPrice>();
		List<TicketPrice> deleteTpList = new ArrayList<TicketPrice>();
		for(TicketPrice tp : tpList){
			if("04".equals(tp.getTicketType().getTicketBigType())){//排除温泉票
				continue;
			}
			Map<String,Map<String,Timestamp>> reMap = this.checkTim(pauseStartDate, pauseEndDate, Timestamp.valueOf(tp.getStartDate().toString()), Timestamp.valueOf(tp.getEndDate().toString()));
			
			Map<String,Timestamp> updateMap = reMap.get("update");
			Map<String,Timestamp> saveMap = reMap.get("save");
			Map<String,Timestamp> deleteMap = reMap.get("delete");
			
			if(updateMap!=null){
				tp.setStartDate(updateMap.get("startDate"));
				tp.setEndDate(updateMap.get("endDate"));
				updateTpList.add(tp);
			}
			if(saveMap!=null){
				TicketPrice tmp = new TicketPrice(getNextKey("ticket_Price", 1), tp.getTicketType(), tp.getUnitPrice(), 
						tp.getPrintPrice(), saveMap.get("startDate"), saveMap.get("endDate"), tp.getStatus(), tp.getIsUse(), 
						tp.getRemark(), tp.getInputer(), Timestamp.valueOf(tp.getInputTime().toString()), tp.getUpdater(), Timestamp.valueOf(tp.getUpdateTime().toString()), 
						tp.getEntrancePrice(), tp.getRopewayPrice(), tp.getEntranceUnitPrice(), tp.getRopewayUnitPrice(), 
						tp.getEntranceCompany(),tp.getRopewayCompany(),tp.getToplimit(), tp.getRemainToplimit(), 
						tp.getTicketIndentDetails(), tp.getTicketDetailHises());
				saveTpList.add(tmp);
			}
			if(deleteMap!=null){
				tp.setStatus("00");
				deleteTpList.add(tp);
			}
		}
		
		List<TicketPrice> tpLi = new ArrayList<TicketPrice>();
		for(TicketPrice tm : updateTpList){
			ticketPriceDao.update(tm);
			tpLi.add(tm);
		}
		for(TicketPrice tm : saveTpList){
			ticketPriceDao.save(tm);
			tpLi.add(tm);
		}
		for(TicketPrice tm : deleteTpList){
			ticketPriceDao.update(tm);
			tpLi.add(tm);
		}
		
		TicketIndentClient tic = new TicketIndentClient();
		try{
			ReturnInfo ri = tic.writeTicketPrice(tpLi);
			if(ri.getResult().trim().equals("0")){//接口调用成功
			}
			else{
				logger.error("同步票价出错:"+ri.getError());
				throw new BusinessException("网络中断,请稍后再试!");
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			logger.error("同步票价出错:"+e.getMessage());
			throw new BusinessException(e);
		}
	}
	
	private Map<String,Map<String,Timestamp>> checkTim(Timestamp pauseStartDate, Timestamp pauseEndDate, Timestamp startDate, Timestamp endDate){
		Map<String,Map<String,Timestamp>> reMap = new HashMap<String,Map<String,Timestamp>>();
		
		pauseStartDate = CommonMethod.toStartTime(pauseStartDate);
		pauseEndDate = CommonMethod.toEndTime(pauseEndDate);
		startDate = CommonMethod.toStartTime(startDate);
		endDate = CommonMethod.toEndTime(endDate);
//		System.out.println("_______________________________________"+CommonMethod.addDay(pauseEndDate, -1));
//		System.out.println(pauseStartDate+":"+pauseEndDate+":"+startDate+":"+endDate);
		if(pauseStartDate.equals(startDate) && pauseEndDate.equals(endDate)){//需要取消的
			Map<String,Timestamp> temp = new HashMap<String,Timestamp>();
			temp.put("startDate", startDate);
			temp.put("endDate", endDate);
			reMap.put("delete", temp);
		}else if(pauseStartDate.equals(startDate)){//暂停日期在时间段开头
			Map<String,Timestamp> temp = new HashMap<String,Timestamp>();
			temp.put("startDate", CommonMethod.toStartTime(CommonMethod.addDay(pauseStartDate, 1)));
			temp.put("endDate", endDate);
			reMap.put("update", temp);
		}else if(pauseEndDate.equals(endDate)){//暂停日期在时间段结尾
			Map<String,Timestamp> temp = new HashMap<String,Timestamp>();
			temp.put("startDate", startDate);
			temp.put("endDate", CommonMethod.toEndTime(CommonMethod.addDay(pauseEndDate, -1)));
			reMap.put("update", temp);
		}else{//暂停日期在时间段中间
			Map<String,Timestamp> temp = new HashMap<String,Timestamp>();
			temp.put("startDate", startDate);
			temp.put("endDate", CommonMethod.toEndTime(CommonMethod.addDay(pauseEndDate, -1)));
			reMap.put("update", temp);
			Map<String,Timestamp> tempSave = new HashMap<String,Timestamp>();
			tempSave.put("startDate", CommonMethod.toStartTime(CommonMethod.addDay(pauseStartDate, 1)));
			tempSave.put("endDate", endDate);
			reMap.put("save", tempSave);
		}
//		System.out.println(pauseStartDate+":"+pauseEndDate+":"+startDate+":"+endDate);
//		System.out.println(reMap);
		return reMap;
	}
	
	
	/**
	 * 是否是门票
	 * @return true:门票，false:索道票
	 */
	private boolean isEntrance(String code) {
		char[] mChars = code.toCharArray();
		
		if(mChars[1]=='0'){//门票
			return true;
		}
//		if(mChars[0]=='0'){//索道票
//			System.out.println("索道票");
//		}
		return false;
	}
	
	/**
	 * 组合编码
	 * @param codeF 门票编码
	 * @param codeS 索道票编码
	 * @return
	 */
	private String combinationCode(String codeF,String codeS) {
		
		if(codeS.isEmpty()){
			return codeF;
		}
		
		String code = "";
		
		char[] fChars = codeF.toCharArray();
		char[] sChars = codeS.toCharArray();
		
		code = String.valueOf(fChars[0])+String.valueOf(sChars[1]);

		return code;
	}

	@Override
	public Map<String,Object> findCheck(String json) {
		
		JSONArray js =JSONArray.fromObject(json);
		CommonJsonConfig jsonConfig = new CommonJsonConfig();

		//查询已设置过的时间段用于判断时间交叉
		Map map = ticketModulePriceDao.findModulePriceTimesCount(getCurrentUserId());
		List oldTimeB = (List)map.get("timeBList");
		
		List<HashMap> jsList = JSONArray.toList(js, new HashMap(), jsonConfig);
		
		List<TicketModule> tmList = ticketModuleDao.findAll();
		
		String msg = "";
		
		for (HashMap teMap : jsList) {
			
			String startDate = teMap.get("startDate").toString();
			String endDate = teMap.get("endDate").toString();
			
			List oldTimeBT = new ArrayList();
			Map<String,String> tmpIdMap = new HashMap<String,String>();
			for (TicketModule ticketModule : tmList) {
				long tmId = ticketModule.getTicketModuleId();
				if(teMap.get("ticketModulePriceId_"+tmId)!=null && !teMap.get("ticketModulePriceId_"+tmId).equals("") && !teMap.get("ticketModulePriceId_"+tmId).equals("undefined")){
					String ticketModulePriceId = teMap.get("ticketModulePriceId_"+tmId).toString();
					tmpIdMap.put("tmpId_"+ticketModulePriceId, ticketModulePriceId);
				}
				
			}
			
			for(Object o : oldTimeB){
				Map otBT = (Map)o;
				String oTmpId = otBT.get("id").toString();
				if(tmpIdMap.get("tmpId_"+oTmpId)==null){//排除掉本条记录
					oldTimeBT.add(o);
				}
			}
			
			for (TicketModule ticketModule : tmList) {
				
				long tmId = ticketModule.getTicketModuleId();
				
				String tmpId = "";
				if(teMap.get("ticketModulePriceId_"+tmId)!=null && !teMap.get("ticketModulePriceId_"+tmId).equals("") && !teMap.get("ticketModulePriceId_"+tmId).equals("undefined")){
					String ticketModulePriceId = teMap.get("ticketModulePriceId_"+tmId).toString();
					tmpId = ticketModulePriceId;
				}
				
				//判断时间是否有交叉
				Map crossTime = CommonMethod.CrossValidationPeriod(CommonMethod.toStartTime(CommonMethod.string2Time1(startDate)), CommonMethod.toEndTime(CommonMethod.string2Time1(endDate)), oldTimeBT);
				if(crossTime!=null){//有交叉时间
					String exception = "你设置的时间段"+startDate+"至"+endDate+
												"与已有的时间段"+crossTime.get("startDate").toString().substring(0, 10)+
												"至"+crossTime.get("endDate").toString().substring(0, 10)+"交叉，请重新设置！";
					Map<String,Object> reMap = new HashMap<String,Object>();
					reMap.put("exception", exception);
					return reMap;
				}
				
				
				String sta = ticketModule.getStatus();
				if("00".equals(sta)){
					continue;
				}
				Double bottomPrice = ticketModule.getBottomPrice();
				if(bottomPrice==null){
					continue;
				}
				double bp =  ticketModule.getBottomPrice();
				String modName = ticketModule.getModuleName();
				if("".equals(teMap.get("unitPrice_"+tmId))){
					teMap.put("unitPrice_"+tmId,"0");
				}
				if(teMap.get("unitPrice_"+tmId)!=null && !teMap.get("unitPrice_"+tmId).equals("")){
					String unitPrice = teMap.get("unitPrice_"+tmId).toString();
					double unP = Double.valueOf(unitPrice);
					if(unP<bp){
						msg += startDate+"至"+endDate+"的"+modName+"预警价格为<span style='color:red;'>"+bp+"</span>，您设置散客价格为"+unitPrice+"!<br>";
					}
				}
				if("".equals(teMap.get("unitPriceGroup_"+tmId))){
					teMap.put("unitPriceGroup_"+tmId,"0");
				}
				if(teMap.get("unitPriceGroup_"+tmId)!=null && !teMap.get("unitPriceGroup_"+tmId).equals("")){
					String unitPriceGroup = teMap.get("unitPriceGroup_"+tmId).toString();
					double unpg = Double.valueOf(unitPriceGroup);
					if(unpg<bp){
						msg += startDate+"至"+endDate+"的"+modName+"预警价格为<span style='color:red;'>"+bp+"</span>，您设置团客价格为"+unitPriceGroup+"!<br>";
					}
				}
				if("".equals(teMap.get("printPrice_"+tmId))){
					teMap.put("printPrice_"+tmId,"0");
				}
				if(teMap.get("printPrice_"+tmId)!=null && !teMap.get("printPrice_"+tmId).equals("")){
					String printPrice = teMap.get("printPrice_"+tmId).toString();
					double pp = Double.valueOf(printPrice);
					if(pp<bp){
						msg += startDate+"至"+endDate+"的"+modName+"预警价格为<span style='color:red;'>"+bp+"</span>，您设置打印价格为"+printPrice+"!<br>";
					}
				}
					
			}
			
		}
		
		if(!"".equals(msg)){
			msg += "是否保存?";
		}
		
		Map<String,Object> reMap = new HashMap<String,Object>();
		reMap.put("msg", msg);
		return reMap;
	}
}