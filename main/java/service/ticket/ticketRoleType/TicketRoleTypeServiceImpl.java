package service.ticket.ticketRoleType;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.SysRole;
import com.xlem.dao.TicketRoleType;

import common.CommonMethod;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.sys.sysRole.SysRoleDao;
import dao.ticket.ticketRoleType.TicketRoleTypeDao;
@Service("ticketRoleTypeService")
public class TicketRoleTypeServiceImpl extends BaseServiceImpl<TicketRoleType> implements TicketRoleTypeService {
@Autowired
private TicketRoleTypeDao ticketRoleTypeDao;
@Autowired
private SysRoleDao sysRoleDao;
	@Override
	@Resource(name="ticketRoleTypeDao")
	protected void initBaseDAO(BaseDao<TicketRoleType> baseDao) {
		setBaseDao(baseDao);
	}
@Override
public PaginationSupport<TicketRoleType> findPageByCriteria(PaginationSupport<TicketRoleType> ps, TicketRoleType t) {
	DetachedCriteria dc = DetachedCriteria.forClass(TicketRoleType.class);
	return ticketRoleTypeDao.findPageByCriteria(ps, Order.desc("ticketRoleTypeId"), dc);
}
@Override
public void save(TicketRoleType t) {
	t.setRoleTypeId(getNextKey("TicketRoleType".toUpperCase(), 1));
	ticketRoleTypeDao.save(t);
}
@Override
public JsonPager<TicketRoleType> findJsonPageByCriteria(
		JsonPager<TicketRoleType> jp, TicketRoleType t) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public List<TicketRoleType> findByCriteria(TicketRoleType t) {
	DetachedCriteria dc = DetachedCriteria.forClass(TicketRoleType.class);
	dc.createAlias("sysRole", "sysRole");
	dc.createAlias("ticketType", "ticketType");
	if(t.getSysRole()!=null && t.getSysRole().getRoleId()!=null){
		dc.add(Property.forName("sysRole.roleId").eq(t.getSysRole().getRoleId()));
	}
	if(t.getTicketType()!=null && t.getTicketType().getTicketId()!=null){
		dc.add(Property.forName("ticketType.ticketId").eq(t.getTicketType().getTicketId()));
	}
	List<TicketRoleType> trList = ticketRoleTypeDao.findByCriteria(dc);
	return trList;
}

@Override
public void updateReminTopLimit(long roleId, long ticketId,int updateNums) {
	DetachedCriteria dc = DetachedCriteria.forClass(TicketRoleType.class);
	dc.add(Property.forName("sysRole.roleId").eq(roleId));
	dc.add(Property.forName("ticketType.ticketId").eq(ticketId));
	List<TicketRoleType> trList = ticketRoleTypeDao.findByCriteria(dc);
	
	for(TicketRoleType trt : trList){
		Integer Toplimit = trt.getToplimit()==null?null:trt.getToplimit();
		if(Toplimit!=null && Toplimit>0){
			trt.setRemainToplimit(trt.getRemainToplimit()-updateNums);
			ticketRoleTypeDao.update(trt);
		}
	} 
	
}

@Override
public void save(Collection<TicketRoleType> coll,long roleId) {
	SysRole sysRole = sysRoleDao.findById(roleId);
	for (TicketRoleType ticketRoleType : coll) {
		if((ticketRoleType.getUsePage()==null || ticketRoleType.getUsePage().trim().equals(""))
				&& ticketRoleType.getToplimit()==null && ticketRoleType.getRemainToplimit()==null
				&& ticketRoleType.getIndentToplimit()==null && ticketRoleType.getUserIndentToplimit()==null){
			continue;
		}
			ticketRoleType.setSysRole(sysRole);
			ticketRoleType.setStatus("11");
			if (ticketRoleType.getRoleTypeId() == null || ticketRoleType.getRoleTypeId()==0L) {
				ticketRoleType.setRoleTypeId(getNextKey("TICKET_ROLE_TYPE", 1));
				ticketRoleType.setInputer(getCurrentUserId());
				ticketRoleType.setInputTime(CommonMethod.getTimeStamp());
				ticketRoleTypeDao.save(ticketRoleType);
			} else {
				ticketRoleType.setUpdater(getCurrentUserId());
				ticketRoleType.setUpdateTime(CommonMethod.getTimeStamp());
				ticketRoleTypeDao.update(ticketRoleType);
			}
		}
	
}
}