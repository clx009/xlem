package service.ticket.ticketRoleType;
import java.util.Collection;
import java.util.List;

import com.xlem.dao.TicketRoleType;

import common.service.BaseService;
public interface TicketRoleTypeService extends BaseService<TicketRoleType> {

	/**
	 * 修改剩余数量
	 * @param roleId
	 * @param ticketId
	 * @param updateNums
	 */
	public void updateReminTopLimit(long roleId, long ticketId,int updateNums);

	List<TicketRoleType> findByCriteria(TicketRoleType t);
	
	public void save(Collection<TicketRoleType> coll,long roleId);
}