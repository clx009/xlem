package service.ticket.ticketModule;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.TicketDiscount;
import com.xlem.dao.TicketModule;
import com.xlem.dao.TicketType;

import ws.ReturnInfo;
import ws.client.ticketIndent.TicketIndentClient;

import common.BusinessException;
import common.CommonMethod;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.ticket.ticketDiscount.TicketDiscountDao;
import dao.ticket.ticketModule.TicketModuleDao;
import dao.ticket.ticketType.TicketTypeDao;

@Service("ticketModuleService")
public class TicketModuleServiceImpl extends BaseServiceImpl<TicketModule>
		implements TicketModuleService {
	@Autowired
	private TicketModuleDao ticketModuleDao;
	@Autowired
	private TicketTypeDao ticketTypeDao;
	@Autowired
	private TicketDiscountDao ticketDiscountDao;
	
	private static Logger logger = Logger.getLogger("bookingService");
	
	@Override
	@Resource(name = "ticketModuleDao")
	protected void initBaseDAO(BaseDao<TicketModule> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<TicketModule> findPageByCriteria(
			PaginationSupport<TicketModule> ps, TicketModule t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketModule.class);
		return ticketModuleDao.findPageByCriteria(ps, Order.desc("ticketModuleId"), dc);
	}

	@Override
	public void save(TicketModule t) {
		t.setTicketModuleId(getNextKey("Ticket_Module".toUpperCase(), 1));
		ticketModuleDao.save(t);
	}

	@Override
	public JsonPager<TicketModule> findJsonPageByCriteria(
			JsonPager<TicketModule> jp, TicketModule t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketModule.class);
		if(t.getStatus()!=null && !t.getStatus().equals("")){
			dc.add(Property.forName("status").eq(t.getStatus()));
			dc.add(Property.forName("ticketBigType").ne("02"));//不查出项目票
		}
		if(t.getModuleCode()!=null && !t.getModuleCode().equals("")){
			dc.add(Property.forName("moduleCode").eq(t.getModuleCode()));
		}
		
		if(t.getGate()!=null && t.getGate().equals("50")){
			dc.add(Property.forName("gate").eq(t.getGate()));
		}else{
			dc.add(Property.forName("gate").ne("50"));
		}
		
		
		dc.addOrder(Order.asc("ticketModuleId"));
		return ticketModuleDao.findJsonPageByCriteria(jp, dc);
	}

	@Override
	public void save(Collection<TicketModule> coll,String node) {
		List<TicketDiscount> tdList = ticketDiscountDao.findAll();
		Map<String,List<TicketDiscount>> tdMap = new HashMap<String,List<TicketDiscount>>();
		for(TicketDiscount td : tdList){
			
			String status = td.getStatus();
			
			if("00".equals(status)){
				continue;
			}
			
			String mCode = td.getModuleCode();
			List<TicketDiscount> tem = tdMap.get(mCode);
			if(tem==null){
				tem = new ArrayList<TicketDiscount>();
				TicketDiscount tdTemp = new TicketDiscount();//默认添加全价设置
				tdTemp.setTicketDiscountId(0L);
				tdTemp.setDiscount(1d);
				tdTemp.setDiscountName("");
				tem.add(tdTemp);
			}
			double dis = td.getDiscount();
			if(dis==1d){//如有全价设置则覆盖默认设置
				tem.set(0, td);
			}else{
				tem.add(td);
			}
			tdMap.put(mCode,tem);
		}
		
		List<TicketModule> newlist = new ArrayList<TicketModule>();
		Map<String,TicketType> ttMap = new HashMap<String,TicketType>();
		for (TicketModule ticketModule : coll) {
			if (ticketModule.getTicketModuleId() == null) {
				ticketModule.setTicketModuleId(getNextKey("Ticket_Module", 1));
				ticketModule.setInputer(getCurrentUserId());
				ticketModule.setInputTime(CommonMethod.getTimeStamp());
				ticketModuleDao.save(ticketModule);
			} else {
				ticketModule.setUpdater(getCurrentUserId());
				ticketModule.setUpdateTime(CommonMethod.getTimeStamp());
				ticketModuleDao.update(ticketModule);
			}
			
			String bigType = ticketModule.getTicketBigType();
			if("01".equals(bigType)){//门票
				newlist.add(ticketModule);
			}else{//项目票
				
				String ticketCode = ticketModule.getModuleCode();
				//游客票
				TicketType ttK = new TicketType();
				ttK.setTicketCode(ticketCode);
				ttK.setTicketName(ticketModule.getModuleName());
				ttK.setSpecialType("01");
				ttK.setCustomerType("02");
				ttK.setGate(ticketModule.getGate());
				ttK.setStatus(ticketModule.getStatus());
				ttK.setTicketDiscountId(0L);
				ttK.setTicketModuleId(String.valueOf(ticketModule.getTicketModuleId()));
				ttK.setTicketBigType(ticketModule.getTicketBigType());
				ttMap.put(ticketCode+"_02_01"+"_"+ticketModule.getGate()+"_"+ttK.getTicketModuleId(), ttK);
				
				//旅行社票
				TicketType ttY = new TicketType();
				ttY.setTicketCode(ticketCode);
				ttY.setTicketName(ticketModule.getModuleName());
				ttY.setSpecialType("01");
				ttY.setCustomerType("01");
				ttY.setGate(ticketModule.getGate());
				ttY.setStatus(ticketModule.getStatus());
				ttY.setTicketDiscountId(0L);
				ttY.setTicketModuleId(String.valueOf(ticketModule.getTicketModuleId()));
				ttY.setTicketBigType(ticketModule.getTicketBigType());
				ttMap.put(ticketCode+"_01_01"+"_"+ticketModule.getGate()+"_"+ttY.getTicketModuleId(), ttY);
			}
			
		}
		for(TicketModule ticketModule : newlist){
			String mCode = ticketModule.getModuleCode();
			
			if(isEntrance(mCode)){//如果是门票则产生组合
				
				String moduleName = ticketModule.getModuleName();
				
				long ticketModuleId = ticketModule.getTicketModuleId();
				
				//根据折扣设置组合门票种类
				List<TicketDiscount> tem = tdMap.get(mCode);
				
				//单独门票
				for(TicketDiscount td : tem){
					String typeName = td.getDiscountName()+moduleName;
					
					String ticketCode = combinationCode(mCode,"");
					String intro = td.getIntro();
					String specialType = "02";
					
					double dis = td.getDiscount();
					
					if(dis==0d){//免费
						specialType = "03";
						continue;//免费门票不能单独买
					}else if(dis==1d){//全票
						specialType = "01";
					}
					
					String gate = ticketModule.getGate();
					String status = ticketModule.getStatus();
					long ticketDiscountId = td.getTicketDiscountId();
					
					//游客票
					TicketType ttK = new TicketType();
					ttK.setTicketCode(ticketCode);
					ttK.setTicketName(typeName);
					ttK.setRemark(intro);
					ttK.setSpecialType(specialType);
					ttK.setCustomerType("02");
					ttK.setGate(gate);
					ttK.setStatus(status);
					ttK.setTicketDiscountId(ticketDiscountId);
					ttK.setTicketModuleId(String.valueOf(ticketModuleId));
					ttK.setTicketBigType(ticketModule.getTicketBigType());
					ttMap.put(ticketCode+"_02_"+specialType+"_"+gate+"_"+ttK.getTicketModuleId(), ttK);
					
					//旅行社票
					TicketType ttY = new TicketType();
					ttY.setTicketCode(ticketCode);
					ttY.setTicketName(typeName);
					ttY.setRemark(intro);
					ttY.setSpecialType(specialType);
					ttY.setCustomerType("01");
					ttY.setGate(gate);
					ttY.setStatus(status);
					ttY.setTicketDiscountId(ticketDiscountId);
					ttY.setTicketModuleId(String.valueOf(ticketModuleId));
					ttY.setTicketBigType(ticketModule.getTicketBigType());
					ttMap.put(ticketCode+"_01_"+specialType+"_"+gate+"_"+ttY.getTicketModuleId(), ttY);
				}
				
				
				
				//组合票种（门票+索道票）
				for(TicketModule tm : newlist){
					
					String tmCode = tm.getModuleCode();
					
					if(!isEntrance(tmCode)){//如果不是门票
						
						if("00".equals(tm.getStatus())){
							continue;
						}
						
						for(TicketDiscount td : tem){
							String tmName = tm.getModuleName();
							String typeName = td.getDiscountName()+moduleName + "+" + tmName;
							
							String ticketCode = combinationCode(mCode,tmCode);
							String intro = td.getIntro();
							String specialType = "02";
							
							double dis = td.getDiscount();
							if(dis==0d){//免费
								specialType = "03";
							}else if(dis==1d){//全票
								specialType = "01";
							}
							
							String gate = tm.getGate();
							String status = ticketModule.getStatus();
							long ticketDiscountId = td.getTicketDiscountId();
							
							//游客票
							TicketType ttK = new TicketType();
							ttK.setTicketCode(ticketCode);
							ttK.setTicketName(typeName);
							ttK.setRemark(intro);
							ttK.setSpecialType(specialType);
							ttK.setCustomerType("02");
							ttK.setGate(gate);
							ttK.setStatus(status);
							ttK.setTicketDiscountId(ticketDiscountId);
							ttK.setTicketModuleId(ticketModuleId+"|"+tm.getTicketModuleId());
							ttK.setTicketBigType(ticketModule.getTicketBigType());
							ttMap.put(ticketCode+"_02_"+specialType+"_"+gate+"_"+ttK.getTicketModuleId(), ttK);
							
							//旅行社票
							TicketType ttY = new TicketType();
							ttY.setTicketCode(ticketCode);
							ttY.setTicketName(typeName);
							ttY.setRemark(intro);
							ttY.setSpecialType(specialType);
							ttY.setCustomerType("01");
							ttY.setGate(gate);
							ttY.setStatus(status);
							ttY.setTicketDiscountId(ticketDiscountId);
							ttY.setTicketModuleId(ticketModuleId+"|"+tm.getTicketModuleId());
							ttY.setTicketBigType(ticketModule.getTicketBigType());
							ttMap.put(ticketCode+"_01_"+specialType+"_"+gate+"_"+ttY.getTicketModuleId(), ttY);
						}
						
					}
					
				}
				
			}
			
		}
		
		//保存票种
		DetachedCriteria dcTP = DetachedCriteria.forClass(TicketType.class);
		dcTP.add(Property.forName("specialType").ne("04"));
		List<TicketType> ttList = ticketTypeDao.findByCriteria(dcTP);
		
		Map<String,TicketType> ttOldMap = new HashMap<String,TicketType>();
		
		for (TicketType ticketType : ttList) {
			
			String ticCode = ticketType.getTicketCode();
			String cusType = ticketType.getCustomerType();
			String speType = ticketType.getSpecialType();
			String gate = ticketType.getGate();
			
//			Long tdisId = ticketType.getTicketDiscountId();
//			long tdId = tdisId == null ? 0L : tdisId.longValue();
			
			ttOldMap.put(ticCode+"_"+cusType+"_"+speType+"_"+gate+"_"+ticketType.getTicketModuleId(), ticketType);
		}
		
		//添加新记录
		List<TicketType> list = new ArrayList<TicketType>();
		for(String key : ttMap.keySet()) {
			TicketType temp = ttOldMap.get(key);
			TicketType tempNew = ttMap.get(key);
			if(temp==null){
				tempNew.setTicketId(getNextKey("Ticket_Type", 1));
				tempNew.setInputer(getCurrentUserId());
				tempNew.setInputTime(CommonMethod.getTimeStamp());
				list.add(tempNew);
				ticketTypeDao.save(tempNew);
			}
		}
		
		//修改旧记录
		for(String key : ttOldMap.keySet()) {
			TicketType temp = ttOldMap.get(key);
			TicketType tempNew = ttMap.get(key);
			if(tempNew==null){
				//temp.setStatus("00");
			}else{
				temp.setTicketCode(tempNew.getTicketCode());
				temp.setTicketName(tempNew.getTicketName());
//				temp.setIntro(tempNew.getIntro());
				temp.setSpecialType(tempNew.getSpecialType());
				temp.setCustomerType(tempNew.getCustomerType());
				temp.setGate(tempNew.getGate());
				temp.setStatus(tempNew.getStatus());
				temp.setTicketDiscountId(tempNew.getTicketDiscountId());
				temp.setTicketModuleId(tempNew.getTicketModuleId());
				
				temp.setUpdater(getCurrentUserId());
				temp.setUpdateTime(CommonMethod.getTimeStamp());
				temp.setTicketBigType(tempNew.getTicketBigType());
				list.add(temp);
				ticketTypeDao.update(temp);
			}
			
		}
		
		if("50".equals(node)){//温泉票
//			ws.hsw.client.ticketIndent.TicketIndentClient tic = new ws.hsw.client.ticketIndent.TicketIndentClient();
//			try{
//				ReturnInfo ri = tic.writeTicketType(list);
//				if(ri.getResult().trim().equals("0")){//接口调用成功
//				}
//				else{
//					logger.error("同步票种出错:"+ri.getError());
//					throw new BusinessException("网络中断,请稍后再试!");
//				}
//			}
//			catch (Exception e) {
//				e.printStackTrace();
//				logger.error("同步票种出错:"+e.getMessage());
//				throw new BusinessException(e.getMessage());
//			}
		}else{
			TicketIndentClient tic = new TicketIndentClient();
			try{
				ReturnInfo ri = tic.writeTicketType(list);
				if(ri.getResult().trim().equals("0")){//接口调用成功
				}
				else{
					logger.error("同步票种出错:"+ri.getError());
					throw new BusinessException("网络中断,请稍后再试!");
				}
			}
			catch (Exception e) {
				e.printStackTrace();
				logger.error("同步票种出错:"+e.getMessage());
				throw new BusinessException(e.getMessage());
			}
		}
		
		
	}
	
	/**
	 * 是否是门票
	 * @return true:门票，false:索道票
	 */
	private boolean isEntrance(String code) {
		char[] mChars = code.toCharArray();
		
		if(mChars[1]=='0'){//门票
			return true;
		}
//		if(mChars[0]=='0'){//索道票
//			System.out.println("索道票");
//		}
		return false;
	}
	
	/**
	 * 组合编码
	 * @param codeF 门票编码
	 * @param codeS 索道票编码
	 * @return
	 */
	private String combinationCode(String codeF,String codeS) {
		
		if(codeS.isEmpty()){
			return codeF;
		}
		
		String code = "";
		
		char[] fChars = codeF.toCharArray();
		char[] sChars = codeS.toCharArray();
		
		code = String.valueOf(fChars[0])+String.valueOf(sChars[1]);

		return code;
	}
}