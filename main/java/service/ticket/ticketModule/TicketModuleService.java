package service.ticket.ticketModule;
import java.util.Collection;

import com.xlem.dao.TicketModule;

import common.service.BaseService;

public interface TicketModuleService extends BaseService<TicketModule> {

	void save(Collection<TicketModule> coll,String node);

}