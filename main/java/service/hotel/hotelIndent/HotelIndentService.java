package service.hotel.hotelIndent;
import java.util.List;
import java.util.Map;

import com.xlem.dao.HotelIndent;
import com.xlem.dao.HotelIndentDetail;

import common.action.JsonPager;
import common.pay.PayParams;
import common.service.BaseService;

public interface HotelIndentService extends BaseService<HotelIndent> {
	/**
	 * 
	 * @param jp
	 * @param t
	 * @param forPage
	 * @return
	 */
	public JsonPager<HotelIndent> findIndentList(JsonPager<HotelIndent> jp,HotelIndent t,String forPage);

	public void updateForClose(HotelIndent hotelIndent);
	
	/**
	 * 保存已支付信息,生成交易记录
	 * @param hotelIndent
	 * @param r2_TrxId
	 * @param r3_Amt
	 * @param rp_PayDate
	 */
	public void savePayResult(HotelIndent hotelIndent, String r2_TrxId,
			String r3_Amt, String rp_PayDate,String out_trade_no);
	
	/**
	 * 获取市场部订单信息(订单查询)
	 * @param jp
	 * @param hotelIndent
	 * @param node
	 * @return
	 */
	public JsonPager<HotelIndent> findIndentListForMKT(
			JsonPager<HotelIndent> jp, HotelIndent hotelIndent, String node);
	
	/**
	 * 根据订单号获取易宝支付参数
	 * @param hotelIndent
	 * @return
	 */
	public PayParams findHotelIndentPayParams(HotelIndent hotelIndent);
	/**
	 * 根据订单号扣除奖励款
	 * @param hotelIndent
	 * @return
	 */
	public void updateHotelIndentByReward(HotelIndent hotelIndent);
	
	/**
	 * 释放房间数量
	 * @param hotelIndent
	 */
	public void saveReleaseRoom(List<HotelIndentDetail> hotelIndentDetails,HotelIndent hotelIndent);
	
	/**
	 * 市场部退订
	 * @param hotelIndent
	 * @return 
	 */
	public String updateForUnsubscribe(HotelIndent hotelIndent);
	
	/**
	 * 查询时统计总价
	 * @param jp
	 * @param hotelIndent
	 * @param node
	 * @return
	 */
	public Map<String,Double> findHotelIndentSums(JsonPager<HotelIndent> jp, HotelIndent hotelIndent, String node);
	
	/**
	 * 统计当天房间数量
	 * @param jp
	 * @param hotelIndent
	 * @param node
	 * @return
	 */
	public int findHotelIndentTodayNums();
}