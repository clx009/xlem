package service.hotel.hotelIndent;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.BaseRewardAccount;
import com.xlem.dao.BaseTranRec;
import com.xlem.dao.HotelActivityLimit;
import com.xlem.dao.HotelDetailHis;
import com.xlem.dao.HotelIndent;
import com.xlem.dao.HotelIndentDetail;
import com.xlem.dao.HotelPriceLimitDetail;
import com.xlem.dao.HotelRechargeRec;
import com.xlem.dao.SysUser;
import com.xlem.dao.Travservice;
import com.xlem.dao.TravserviceHome;

import service.comm.baseTranRec.BaseTranRecService;
import service.hotel.financeHotelAccounts.FinanceHotelAccountsService;

import common.BusinessException;
import common.CommonMethod;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.pay.PayFactoryService;
import common.pay.PayParams;
import common.service.BaseServiceImpl;

import dao.comm.baseRewardAccount.BaseRewardAccountDao;
import dao.comm.reg.baseTravelAgency.BaseTravelAgencyDao;
import dao.hotel.hotelActivityLimit.HotelActivityLimitDao;
import dao.hotel.hotelDetailHis.HotelDetailHisDao;
import dao.hotel.hotelIndent.HotelIndentDao;
import dao.hotel.hotelIndentDetail.HotelIndentDetailDao;
import dao.hotel.hotelPriceLimitDetail.HotelPriceLimitDetailDao;
import dao.hotel.hotelRechargeRec.HotelRechargeRecDao;
import dao.sys.sysParam.SysParamDao;

@Service("hotelIndentService")
public class HotelIndentServiceImpl extends BaseServiceImpl<HotelIndent>
		implements HotelIndentService {
	@Autowired
	private HotelIndentDao hotelIndentDao;
	@Autowired
	private BaseTranRecService baseTranRecService;
	@Autowired
	private HotelIndentDetailDao hotelIndentDetailDao;
	@Autowired
	private HotelDetailHisDao hotelDetailHisDao;
	@Autowired
	private HotelPriceLimitDetailDao hotelPriceLimitDetailDao; 
	@Autowired
	private HotelActivityLimitDao hotelActivityLimitDao; 
	@Autowired
	private BaseRewardAccountDao baseRewardAccountDao;
	@Autowired
	private HotelRechargeRecDao hotelRechargeRecDao;
	@Autowired
	private BaseTravelAgencyDao baseTravelAgencyDao;
	@Autowired
	private PayFactoryService payFactoryService;
	@Autowired
	private FinanceHotelAccountsService financeHotelAccountsService;
	@Autowired
	private SysParamDao sysParamDao;
	
	@Override
	@Resource(name = "hotelIndentDao")
	protected void initBaseDAO(BaseDao<HotelIndent> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<HotelIndent> findPageByCriteria(
			PaginationSupport<HotelIndent> ps, HotelIndent t) {
		DetachedCriteria dc = DetachedCriteria.forClass(HotelIndent.class);
		return hotelIndentDao.findPageByCriteria(ps,
				Order.desc("hotelIndentId"), dc);
	}

	@Override
	public void save(HotelIndent t) {
		t.setHotelIndentId(getNextKey("Hotel_Indent".toUpperCase(), 1));
		hotelIndentDao.save(t);
	}

	@Override
	public JsonPager<HotelIndent> findJsonPageByCriteria(
			JsonPager<HotelIndent> jp, HotelIndent t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JsonPager<HotelIndent> findIndentList(JsonPager<HotelIndent> jp,
			HotelIndent t, String forPage) {
		jp.setTotal(this.hotelIndentDao.findIndentListCount(t, this.getCurrentUserId(), jp, forPage));
		jp.setRoot(this.hotelIndentDao.findHotelIndentList(t, this.getCurrentUserId(), jp, forPage));
		return jp;
	}

	@Override
	public void updateForClose(HotelIndent hotelIndent) {
		hotelIndent.setStatus("06");
		hotelIndent.setUpdater(getCurrentUserId());
		hotelIndent.setUpdateTime(CommonMethod.getTimeStamp());
		this.hotelIndentDao.update(hotelIndent);
		
		//冲退交易记录
		baseTranRecService.saveReturnBackInfo(hotelIndent);
		//冲退财务记录
		financeHotelAccountsService.saveReturnBackInfo(hotelIndent,null,null);
		List<HotelIndentDetail> hotelIndentDetails = hotelIndentDetailDao.findByProperty("hotelIndent.hotelIndentId", hotelIndent.getHotelIndentId());
		for(HotelIndentDetail hid : hotelIndentDetails){
			hid.setAmountret(hid.getAmount());
		}
		saveReleaseRoom(hotelIndentDetails,hotelIndent);
	}

	@Override
	public void savePayResult(HotelIndent hotelIndent, String r2_TrxId,
			String r3_Amt, String rp_PayDate,String out_trade_no) {
		hotelIndent.setStatus("02");//已支付
		double perFee = Double.parseDouble(findSysParam("PER_FEE"));
		hotelIndent.setPayType("9");//西软为网络支付
		hotelIndent.setUpdater(null);
		hotelIndent.setUpdateTime(CommonMethod.getTimeStamp());
		if(hotelIndent.getReceivedMoney()==null || hotelIndent.getReceivedMoney().isNaN() || hotelIndent.getReceivedMoney()==0){
			hotelIndent.setReceivedMoney(Double.parseDouble(r3_Amt));//已支付金额
		}
		else{
			hotelIndent.setReceivedMoney(hotelIndent.getReceivedMoney()+Double.parseDouble(r3_Amt));//已支付金额
		}
		hotelIndentDao.update(hotelIndent);//更新订单
		
		//修改现金交易记录
		DetachedCriteria dcBt = DetachedCriteria.forClass(BaseTranRec.class);
		dcBt.add(Property.forName("hotelIndent.hotelIndentId").eq(hotelIndent.getHotelIndentId()));
		dcBt.add(Property.forName("paymentType").in(new String[]{"01","03"}));
		dcBt.add(Property.forName("payType").eq("01"));
		List<BaseTranRec> btsList = baseTranRecService.findByCriteria(dcBt);
		
		List<BaseTranRec> btList = baseTranRecService.excludeReturnBackInfo(btsList);
		
		for(BaseTranRec bt : btList){
			bt.setTrxId(r2_TrxId);
			bt.setPerFee(perFee);
			bt.setBatchNo(out_trade_no);
			bt.setPayFee(perFee*Double.parseDouble(r3_Amt));
			bt.setPaymentTime(CommonMethod.string2Time3(rp_PayDate));
			baseTranRecService.update(bt);
			
			//添加财务记录
			financeHotelAccountsService.savePayInfo(hotelIndent, bt, null);
		}
//		//生成交易记录
//		BaseTranRec baseTranRec = new BaseTranRec();
//		baseTranRec.setTrxId(r2_TrxId);
//		baseTranRec.setHotelIndent(hotelIndent);
//		baseTranRec.setMoney(Double.parseDouble(r3_Amt));
//		baseTranRec.setPaymentType("01");//支付类型支付
//		baseTranRec.setPayType("01");//交易类型,通过易宝交易
//		baseTranRec.setPerFee(perFee);
//		baseTranRec.setBatchNo(out_trade_no);
//		baseTranRec.setPayFee(perFee*Double.parseDouble(r3_Amt));
//		baseTranRec.setPaymentTime(CommonMethod.string2Time1(rp_PayDate));
//		baseTranRec.setPaymentAccount("02");
//		baseTranRecService.save(baseTranRec);
	}

	@Override
	public JsonPager<HotelIndent> findIndentListForMKT(
			JsonPager<HotelIndent> jp, HotelIndent t, String forPage) {
		jp.setTotal(this.hotelIndentDao.findIndentListCountForMKT(t, this.getCurrentUserId(), jp, forPage));
		jp.setRoot(this.hotelIndentDao.findHotelIndentListForMKT(t, this.getCurrentUserId(), jp, forPage));
		return jp;
	}

	@Override
	public PayParams findHotelIndentPayParams(HotelIndent t) {
		String dBank = t.getDefaultbank();
		t = hotelIndentDao.findById(t.getHotelIndentId());
		t.setDefaultbank(dBank);
		//生成易宝交易信息
		PayParams payParams = new PayParams();
		if("00".equals(t.getOrigin())){//网络订单需要去易宝支付
//			String keyValue = CommonMethod.formatString(findSysParam("KEY_VALUE"));
//			payParams.setP0_Cmd("Buy");
//			payParams.setP1_MerId(findSysParam("P1_MERID"));
//			payParams.setP2_Order(t.getIndentCode());
//			if(t.getReceivedMoney()!=null && !t.getReceivedMoney().isNaN() && t.getReceivedMoney()>0){
//				payParams.setP3_Amt(CommonMethod.format2db(t.getTotalPrice()-t.getReceivedMoney()));//减去已付款金额
//			}
//			else{
//				payParams.setP3_Amt(CommonMethod.format2db(t.getTotalPrice()));
//			}
//			payParams.setP4_Cur("CNY");
//			payParams.setP5_Pid("");
//			payParams.setP6_Pcat("酒店");
//			payParams.setP7_Pdesc("");
//			payParams.setP8_Url(Configuration.getInstance().getValue("resposeUrlForHotel"));
//			payParams.setP9_SAF("0");
//			payParams.setPa_MP("");
//			payParams.setPd_FrpId("");
//			payParams.setPm_Period("20");//20分钟不支付即过期
//			payParams.setPn_Unit("minute");//20分钟不支付即过期
//			payParams.setPr_NeedResponse("1");
//			payParams.setNodeAuthorizationURL(CommonMethod.formatString(findSysParam("YEEPAY_COMMON_REQURL")));
//			payParams.setHmac(PaymentForOnlineService
//					.getReqMd5HmacForOnlinePayment(payParams.getP0_Cmd(),
//							payParams.getP1_MerId(), payParams.getP2_Order(),
//							payParams.getP3_Amt(), payParams.getP4_Cur(),
//							CommonMethod.getNewString(payParams.getP5_Pid(),"GBK"), CommonMethod.getNewString(payParams.getP6_Pcat(),"GBK"),
//							CommonMethod.getNewString(payParams.getP7_Pdesc(),"GBK"), payParams.getP8_Url(),
//							payParams.getP9_SAF(), CommonMethod.getNewString(payParams.getPa_MP(),"GBK"),
//							payParams.getPd_FrpId(),payParams.getPm_Period(),payParams.getPn_Unit(),
//							payParams.getPr_NeedResponse(), keyValue));

			double moneyNeedPay = 0;
			if(t.getReceivedMoney()!=null && !t.getReceivedMoney().isNaN() && t.getReceivedMoney()>0){
				moneyNeedPay = t.getTotalPrice()-t.getReceivedMoney();//减去已付款金额
			}
			else{
				moneyNeedPay = t.getTotalPrice();
			}
			payFactoryService.init();
			payParams = payFactoryService.initPayParams(t,moneyNeedPay);
			payParams.setPay_test(sysParamDao.findSysParam("PAY_TEST"));
		}
		return payParams;
	}

	@Override
	public void updateHotelIndentByReward(HotelIndent hotelIndent) {
		long rewardAccountId = hotelIndent.getRewardAccountId();
		hotelIndent = hotelIndentDao.findById(hotelIndent.getHotelIndentId());
		//检查订单是否超时时间
		Long mi = Long.parseLong(sysParamDao.findSysParam("HOTEL_INDENT_AUTO_CANCEL_TIME"));//酒店订单自动取消时间（分钟）
		//实际时间比配置时间减少2分钟
		mi = mi - 2;
		//计算剩余时间
		//下订单到现在已过多长时间
		Long myhour = CommonMethod.diffMin( Timestamp.valueOf(hotelIndent.getInputTime().toString()), CommonMethod.getTimeStamp());
		//还剩余多少时间
		mi = mi - myhour;
		
		if(mi<0L){
			throw new BusinessException("订单超过支付时间未支付已被系统取消！");
		}
		
		
		double totalPrice = hotelIndent.getTotalPrice();
		hotelIndent.setPayType("01");
		hotelIndent.setStatus("02");
		hotelIndentDao.update(hotelIndent);
		
		SysUser sysUser = getSysUser();
		//扣除奖励款
		BaseRewardAccount baseRewardAccount =baseRewardAccountDao.findById(rewardAccountId);
		baseRewardAccount.setRewardBalance(baseRewardAccount.getRewardBalance()-totalPrice);
		baseRewardAccountDao.update(baseRewardAccount);
		//奖励款记录
		Travservice baseTravelAgency = baseTravelAgencyDao.findById(sysUser.getBaseTravelAgency().getTravelAgencyId());
		HotelRechargeRec hotelRechargeRec = new HotelRechargeRec();
		hotelRechargeRec.setHotelIndent(hotelIndent);
		hotelRechargeRec.setHotelRechargeRecId(getNextKey("HOTEL_RECHARGE_REC", 1));
		hotelRechargeRec.setBaseTravelAgency(baseTravelAgency);
		hotelRechargeRec.setBaseRewardAccount(baseRewardAccount);
		hotelRechargeRec.setSourceDescription("订房付款");
		hotelRechargeRec.setRewardMoney(0-hotelIndent.getTotalPrice());
		hotelRechargeRec.setRewardBalance(baseRewardAccount.getRewardBalance());
		hotelRechargeRec.setPaymentType("01");
		hotelRechargeRec.setInputer(getCurrentUserId());
		hotelRechargeRec.setInputTime(CommonMethod.getTimeStamp());
		hotelRechargeRecDao.save(hotelRechargeRec);
		
		//添加财务记录
		financeHotelAccountsService.savePayInfo(hotelIndent, null, hotelRechargeRec);
	}
	
	@Override
	public void saveReleaseRoom(List<HotelIndentDetail> hotelIndentDetails,HotelIndent hotelIndent) {
		//释放房间占用数量
		long hotelActivityId = hotelIndent.getHotelActivityId() == null ? 0L : hotelIndent.getHotelActivityId();
		
		if(hotelIndentDetails!=null && !hotelIndentDetails.isEmpty()){
			List<HotelDetailHis> hotelDetailHiss = hotelDetailHisDao.findByProperty("hotelIndent.hotelIndentId", hotelIndent.getHotelIndentId());
			int thisTime = 0;
			for (HotelDetailHis hdh : hotelDetailHiss) {
				if(hdh.getTime()==null){
					hdh.setTime(1);
					hotelDetailHisDao.update(hdh);
				}
				int t = hdh.getTime();
				if(thisTime<t){
					thisTime = t;
				}
			}
			thisTime++;
			long keyStart = getNextKey("Hotel_Detail_His", hotelIndentDetails.size());
			for (int i = 0; i < hotelIndentDetails.size(); i++) {//存入历史库
				HotelIndentDetail hid = hotelIndentDetails.get(i);
				HotelDetailHis hdh = new HotelDetailHis();
				hdh.setHotelDetailHisId(keyStart+i);
				hdh.setAmount(hid.getAmount());
				hdh.setBookingDate(hid.getBookingDate());
				hdh.setCutPrice(hid.getCutPrice());
				hdh.setHotelIndent(hotelIndent);
				hdh.setHotelIndentDetailCode(hid.getHotelIndentDetailCode());
				hdh.setHotelRoomType(hid.getHotelRoomType());
				hdh.setInputer(hid.getInputer());
				hdh.setInputTime(hid.getInputTime());
				hdh.setRemark(hid.getRemark());
				hdh.setSubtotal(hid.getSubtotal());
				hdh.setUnitPrice(hid.getUnitPrice());
				hdh.setUpdater(hid.getUpdater());
				hdh.setUpdateTime(hid.getUpdateTime());
				hdh.setTime(thisTime);
				hdh.setHotelIndentDetailId(hid.getHotelIndentDetailId());
				hotelDetailHisDao.save(hdh);//存历史
				hotelIndentDetailDao.delete(hid);//删明细
				int retAmo = hid.getAmount();
				if(hid.getAmount() !=hid.getAmountret()){//没有全部退房
					retAmo = hid.getAmountret()==null?0:hid.getAmountret();
					int surplus = hid.getAmount()-retAmo;
					HotelIndentDetail hidN = new HotelIndentDetail();
					hidN.setHotelIndentDetailId(getNextKey("HOTEL_INDENT_DETAIL", 1));
					hidN.setAmount(surplus);
					hidN.setBookingDate(hid.getBookingDate());
					hidN.setHotelIndent(hotelIndent);
					hidN.setHotelIndentDetailCode(hid.getHotelIndentDetailCode());
					hidN.setHotelRoomType(hid.getHotelRoomType());
					hidN.setInputer(hid.getInputer());
					hidN.setInputTime(hid.getInputTime());
					hidN.setRemark(hid.getRemark());
					hidN.setSubtotal(surplus*hid.getUnitPrice());
					hidN.setUnitPrice(hid.getUnitPrice());
					hotelIndentDetailDao.save(hidN);
				}
				
				//更新剩余数量
				if(hotelActivityId==0){
					DetachedCriteria dc = DetachedCriteria.forClass(HotelPriceLimitDetail.class);
					dc.add(Property.forName("hotelRoomType.roomTypeId").eq(hid.getHotelRoomType().getRoomTypeId()));
					dc.add(Property.forName("useDate").eq(hid.getBookingDate()));
					List<HotelPriceLimitDetail> hotelPriceLimitDetails = hotelPriceLimitDetailDao.findByCriteria(dc);
					if(hotelPriceLimitDetails!=null && !hotelPriceLimitDetails.isEmpty()){
						HotelPriceLimitDetail tempHpld = hotelPriceLimitDetails.get(0);
						tempHpld.setRemainAmount(tempHpld.getRemainAmount()+retAmo);
						hotelPriceLimitDetailDao.update(tempHpld);
					}
				}else{
					DetachedCriteria dc = DetachedCriteria.forClass(HotelActivityLimit.class);
					dc.createAlias("hotelRoomType", "hotelRoomType");
					dc.add(Property.forName("hotelRoomType.roomTypeId").eq(hid.getHotelRoomType().getRoomTypeId()));
					dc.add(Property.forName("hotelActivity.hotelActivityId").eq(hotelActivityId));
					dc.add(Property.forName("useDate").eq(hid.getBookingDate()));
					//dc.add(Property.forName("useDate").lt(hotelIndent.getEndDate()));
					dc.addOrder(Order.asc("useDate"));
					List<HotelActivityLimit> hotelActivityLimits = hotelActivityLimitDao.findByCriteria(dc);
					if(hotelActivityLimits!=null && !hotelActivityLimits.isEmpty()){
						HotelActivityLimit tempHpld = hotelActivityLimits.get(0);
						tempHpld.setRemainAmount(tempHpld.getRemainAmount()+retAmo);
						hotelActivityLimitDao.update(tempHpld);
					}
				}
				
			}
		}
	}

	@Override
	public String updateForUnsubscribe(HotelIndent hotelIndent) {
		hotelIndent.setStatus("05");
		hotelIndent.setUpdater(getCurrentUserId());
		hotelIndent.setUpdateTime(CommonMethod.getTimeStamp());
		this.hotelIndentDao.update(hotelIndent);
		List<HotelIndentDetail> hotelIndentDetails = hotelIndentDetailDao.findByProperty("hotelIndent.hotelIndentId", hotelIndent.getHotelIndentId());
		for(HotelIndentDetail hid : hotelIndentDetails){
			hid.setAmountret(hid.getAmount());
		}
		saveReleaseRoom(hotelIndentDetails,hotelIndent);
		//添加财务记录
		financeHotelAccountsService.saveReturnInfo(hotelIndent,hotelIndentDetails, null, null);
		return null;
	}

	@Override
	public Map<String,Double> findHotelIndentSums(JsonPager<HotelIndent> jp,
			HotelIndent hotelIndent, String node) {
		return hotelIndentDao.findHotelIndentSums(jp,hotelIndent,node,getCurrentUserId());
	}
	
	public int findHotelIndentTodayNums(){
		return hotelIndentDao.findHotelIndentTodayNums();
	}
}