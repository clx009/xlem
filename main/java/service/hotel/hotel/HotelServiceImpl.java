package service.hotel.hotel;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.Hotel;

import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.hotel.hotel.HotelDao;

@Service("hotelService")
public class HotelServiceImpl extends BaseServiceImpl<Hotel> implements HotelService {
	@Autowired
	private HotelDao hotelDao;

	@Override
	@Resource(name = "hotelDao")
	protected void initBaseDAO(BaseDao<Hotel> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<Hotel> findPageByCriteria(
			PaginationSupport<Hotel> ps, Hotel t) {
		DetachedCriteria dc = DetachedCriteria.forClass(Hotel.class);
		return hotelDao.findPageByCriteria(ps, Order.asc("serial"), dc);
	}

	@Override
	public void save(Hotel t) {
		t.setHotelId(getNextKey("Hotel".toUpperCase(), 1));
		hotelDao.save(t);
	}

	@Override
	public JsonPager<Hotel> findJsonPageByCriteria(JsonPager<Hotel> jp, Hotel t) {
		DetachedCriteria dc = DetachedCriteria.forClass(Hotel.class);
		dc.addOrder(Order.asc("serial"));
		return hotelDao.findJsonPageByCriteria(jp, dc);
	}

}