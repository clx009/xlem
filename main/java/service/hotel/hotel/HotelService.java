package service.hotel.hotel;
import com.xlem.dao.Hotel;

import common.action.JsonPager;
import common.service.BaseService;

public interface HotelService extends BaseService<Hotel> {
	
}