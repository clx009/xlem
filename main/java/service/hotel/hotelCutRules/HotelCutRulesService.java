package service.hotel.hotelCutRules;
import java.util.Collection;

import com.xlem.dao.HotelCutRules;

import common.action.JsonPager;
import common.service.BaseService;
public interface HotelCutRulesService extends BaseService<HotelCutRules> {

	void save(Collection<HotelCutRules> coll);

	JsonPager<HotelCutRules> findJsonPageForHotelCutRules(
			JsonPager<HotelCutRules> jp, HotelCutRules hotelCutRules);

}