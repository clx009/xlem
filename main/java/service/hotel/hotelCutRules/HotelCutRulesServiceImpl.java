package service.hotel.hotelCutRules;

import java.util.Collection;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.HotelCutRules;

import common.CommonMethod;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.hotel.hotelCutRules.HotelCutRulesDao;

@Service("hotelCutRulesService")
public class HotelCutRulesServiceImpl extends BaseServiceImpl<HotelCutRules>
		implements HotelCutRulesService {
	@Autowired
	private HotelCutRulesDao hotelCutRulesDao;

	@Override
	@Resource(name = "hotelCutRulesDao")
	protected void initBaseDAO(BaseDao<HotelCutRules> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<HotelCutRules> findPageByCriteria(
			PaginationSupport<HotelCutRules> ps, HotelCutRules t) {
		DetachedCriteria dc = DetachedCriteria.forClass(HotelCutRules.class);
		return hotelCutRulesDao.findPageByCriteria(ps,
				Order.desc("hotelCutRulesId"), dc);
	}

	@Override
	public void save(HotelCutRules t) {
		t.setCutRulesId(getNextKey("Hotel_Cut_Rules".toUpperCase(), 1));
		hotelCutRulesDao.save(t);
	}

	@Override
	public JsonPager<HotelCutRules> findJsonPageByCriteria(
			JsonPager<HotelCutRules> jp, HotelCutRules t) {
		return null;
	}

	@Override
	public void save(Collection<HotelCutRules> coll) {
		for(HotelCutRules cutRule:coll){
			if(cutRule.getCutRulesId()==null){
				cutRule.setCutRulesId(this.getNextKey("hotel_cut_rules", 1));
				cutRule.setInputer(this.getCurrentUserId());
				cutRule.setInputTime(CommonMethod.getTimeStamp());
				cutRule.setUpdater(this.getCurrentUserId());
				cutRule.setUpdateTime(CommonMethod.getTimeStamp());
				this.hotelCutRulesDao.save(cutRule);
			}else{
				cutRule.setUpdater(this.getCurrentUserId());
				cutRule.setUpdateTime(CommonMethod.getTimeStamp());
				this.hotelCutRulesDao.update(cutRule);
			}
		}
	}

	@Override
	public JsonPager<HotelCutRules> findJsonPageForHotelCutRules(
			JsonPager<HotelCutRules> jp, HotelCutRules hotelCutRules) {
		DetachedCriteria dc = DetachedCriteria.forClass(hotelCutRules.getClass());
		if(hotelCutRules.getCutRulesType()!=null && !hotelCutRules.getCutRulesType().isEmpty()){
			dc.add(Property.forName("cutRulesType").eq(hotelCutRules.getCutRulesType()));
		}
		dc.addOrder(Order.asc("limitHour"));
		return this.hotelCutRulesDao.findJsonPageByCriteria(jp, dc);
	}
}