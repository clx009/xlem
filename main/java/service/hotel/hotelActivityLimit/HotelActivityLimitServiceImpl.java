package service.hotel.hotelActivityLimit;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.HotelActivity;
import com.xlem.dao.HotelActivityDetail;
import com.xlem.dao.HotelActivityLimit;
import com.xlem.dao.SysUser;

import service.hotel.hotelBookingLimit.HotelBookingLimitService;

import common.BusinessException;
import common.CommonMethod;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;
import common.FullCalBean;

import dao.hotel.hotelActivity.HotelActivityDao;
import dao.hotel.hotelActivityDetail.HotelActivityDetailDao;
import dao.hotel.hotelActivityLimit.HotelActivityLimitDao;
import dao.hotel.hotelIndentDetail.HotelIndentDetailDao;
import dao.hotel.hotelPriceLimitDetail.HotelPriceLimitDetailDao;
import dao.hotel.roomType.RoomTypeDao;
@Service("hotelActivityLimitService")
public class HotelActivityLimitServiceImpl extends BaseServiceImpl<HotelActivityLimit> implements HotelActivityLimitService {
@Autowired
private HotelActivityLimitDao hotelActivityLimitDao;
@Autowired
private HotelActivityDetailDao hotelActivityDetailDao;
@Autowired
private HotelActivityDao hotelActivityDao;
@Autowired
private RoomTypeDao roomTypeDao;
@Autowired
private HotelBookingLimitService hotelBookingLimitService;
@Autowired
private HotelPriceLimitDetailDao hotelPriceLimitDetailDao;
@Autowired
private HotelIndentDetailDao hotelIndentDetailDao;

	@Override
	@Resource(name="hotelActivityLimitDao")
	protected void initBaseDAO(BaseDao<HotelActivityLimit> baseDao) {
		setBaseDao(baseDao);
	}
@Override
public PaginationSupport<HotelActivityLimit> findPageByCriteria(PaginationSupport<HotelActivityLimit> ps, HotelActivityLimit t) {
	DetachedCriteria dc = DetachedCriteria.forClass(HotelActivityLimit.class);
	return hotelActivityLimitDao.findPageByCriteria(ps, Order.desc("LimitId"), dc);
}
@Override
public void save(HotelActivityLimit t) {
	t.setLimitId(getNextKey("HotelActivityLimit".toUpperCase(), 1));
	hotelActivityLimitDao.save(t);
}
@Override
public JsonPager<HotelActivityLimit> findJsonPageByCriteria(
		JsonPager<HotelActivityLimit> jp, HotelActivityLimit t) {
	String customerType = "01";
	t.setEndDate(CommonMethod.addDay(t.getEndDate(), -1));
	if(t.getCustomerType()!=null && !t.getCustomerType().equals("")){//如无客户类型设置类型
		customerType = t.getCustomerType();
	}
	else{
		SysUser sysUser = getSysUser();
		if("03".equals(sysUser.getUserType())){
			customerType = "01";
		}
		else{
			customerType = "02";
		}
	}
	
	List<Timestamp> timestamps = CommonMethod.getTimeStampList(t.getStartDate(), t.getEndDate());
	
	double actPrice = checkActivtyAndGetPrice(timestamps, t, customerType);
	
	//查询剩余房和房上限
	DetachedCriteria dc = DetachedCriteria.forClass(HotelActivityLimit.class);
	dc.createAlias("hotelRoomType", "hotelRoomType");
	if(t.getHotelRoomType()!=null && t.getHotelRoomType().getRoomTypeId()!=null){
		dc.add(Property.forName("hotelRoomType.roomTypeId").eq(t.getHotelRoomType().getRoomTypeId()));
	}
	if(t.getHotelActivity()!=null && t.getHotelActivity().getHotelActivityId() !=null){
		dc.add(Property.forName("hotelActivity.hotelActivityId").eq(t.getHotelActivity().getHotelActivityId()));
	}
	if(t.getStartDate()!=null){
		dc.add(Property.forName("useDate").ge(t.getStartDate()));
	}
	if(t.getEndDate()!=null){
		dc.add(Property.forName("useDate").le(t.getEndDate()));
	}
	dc.addOrder(Order.asc("useDate"));
	jp = hotelActivityLimitDao.findJsonPageByCriteria(jp, dc);
	
	//补充缺少的日期信息
	List<HotelActivityLimit> details = jp.getRoot();
	
	if(details!=null && details.size()>0){
		Map<Timestamp, HotelActivityLimit> detailsMap = new HashMap<Timestamp, HotelActivityLimit>();
		for (int i = 0; i < details.size(); i++) {
			HotelActivityLimit hotelActivityDetail = details.get(i);
			if(hotelActivityDetail.getAmount()==null){
				hotelActivityDetail.setAmount(0);
			}
			detailsMap.put(Timestamp.valueOf(hotelActivityDetail.getUseDate().toString()), hotelActivityDetail);
		}
		for (int i = 0; i < timestamps.size(); i++) {
			Timestamp timestamp = timestamps.get(i);
			HotelActivityLimit hpld = detailsMap.get(timestamp);
			if(hpld==null){//不存在该日期的设置时,添加此项
				HotelActivityLimit detail = new HotelActivityLimit();
				detail.setHotelRoomType(t.getHotelRoomType());
				detail.setPrice(actPrice);
				detail.setUseDate(timestamp);
				detail.setBookingAmount(0);////初始化暂订和剩余房间数
				detail.setSurplusAmount(detail.getAmount());
				detail.setCustomerType(customerType);
				details.add(i, detail);
				detailsMap.put(timestamp, detail);
			}
			else{//初始化暂订和剩余房间数
				hpld.setPrice(actPrice);
				hpld.setBookingAmount(0);
				hpld.setCustomerType(customerType);
				hpld.setSurplusAmount(hpld.getAmount());
			}
		}
		
		//查询每天房间预订情况
		List<HotelActivityLimit> details2 = hotelIndentDetailDao.findUsedRoomEveryday(t);
		for (int i = 0; i < details2.size(); i++) {
			HotelActivityLimit roomDetail = details2.get(i);
			HotelActivityLimit limitDetail = detailsMap.get(roomDetail.getUseDate());
			if(limitDetail!=null){
				limitDetail.setBookingAmount(roomDetail.getBookingAmount());
				
				int amount = limitDetail.getAmount() == null ? 0 : limitDetail.getAmount();
				int bAmount = limitDetail.getBookingAmount() == null ? 0 : limitDetail.getBookingAmount();
				
				limitDetail.setSurplusAmount((amount-bAmount)<0?0:(amount-bAmount));
			}
		}
	}
	
	return jp;
}

/**
 * 验证活动是否有效并查询相关房间金额
 * @param timestamps
 * @return
 */
private double checkActivtyAndGetPrice(List<Timestamp> timestamps, HotelActivityLimit t,String customerType) {
	
	//查询活动日期
	DetachedCriteria dcAct = DetachedCriteria.forClass(HotelActivity.class);
	if(t.getHotelActivity()!=null && t.getHotelActivity().getHotelActivityId() !=null){
		dcAct.add(Property.forName("hotelActivityId").eq(t.getHotelActivity().getHotelActivityId()));
	}else{//没有传活动ID不查询出数据
		dcAct.add(Property.forName("hotelActivityId").eq(-1L));
	}
	if(t.getStartDate()!=null){
		dcAct.add(Property.forName("startDate").le(t.getStartDate()));
	}
	if(t.getEndDate()!=null){
		dcAct.add(Property.forName("endDate").ge(t.getEndDate()));
	}
	
	List<HotelActivity> haList = hotelActivityDao.findByCriteria(dcAct);
	if(haList==null || haList.size()<1){//没有符合条件的活动
		throw new BusinessException("没有符合条件的酒店活动！请重新确认订单！","");
	}
	
	//查询符合条件的活动明细
	DetachedCriteria dcActDet = DetachedCriteria.forClass(HotelActivityDetail.class);
	dcActDet.createAlias("hotelRoomType", "hotelRoomType");
	if(t.getHotelActivity()!=null && t.getHotelActivity().getHotelActivityId() !=null){
		dcActDet.add(Property.forName("hotelActivity.hotelActivityId").eq(t.getHotelActivity().getHotelActivityId()));
	}else{//没有传活动ID不查询出数据
		dcActDet.add(Property.forName("hotelActivity.hotelActivityId").eq(-1L));
	}
	dcActDet.add(Property.forName("customerType").eq(customerType));
	
	if(t.getHotelRoomType()!=null && t.getHotelRoomType().getRoomTypeId() !=null){
		dcActDet.add(Property.forName("hotelRoomType.roomTypeId").eq(t.getHotelRoomType().getRoomTypeId()));
	}
	List<HotelActivityDetail> hadList = hotelActivityDetailDao.findByCriteria(dcActDet);
	
	double actPrice = -1d;
	int days = timestamps.size();
	
	for(HotelActivityDetail had : hadList){
		
		int minDays = had.getMinDays() == null ? 0 : had.getMinDays();
		int maxDays = had.getMaxDays() == null ? 0 : had.getMaxDays();
		
		if((minDays<=days && days<=maxDays) || (maxDays==0 && minDays<=days)){//符合条件
			actPrice = had.getRoomPrice() == null ? 0d : had.getRoomPrice();
		}
		
	}
	
	if(actPrice==-1d){//没有符合条件的活动
		throw new BusinessException("没有符合条件的酒店活动！请重新确认订单！","");
	}
	
	return actPrice;
	
}

@Override
public List<FullCalBean> findByFull(Date startDate, Date endDate,HotelActivityLimit t) {
	DetachedCriteria dc = DetachedCriteria.forClass(HotelActivityLimit.class);
//	dc.createAlias("hotelRoomType", "hotelRoomType");
	dc.add(Property.forName("hotelRoomType.roomTypeId").eq(t.getHotelRoomType().getRoomTypeId()));
	dc.add(Property.forName("hotelActivity.hotelActivityId").eq(t.getHotelActivity().getHotelActivityId()));
	dc.add(Property.forName("useDate").ge(startDate));
	dc.add(Property.forName("useDate").le(endDate));
	dc.addOrder(Order.desc("useDate"));
	List<HotelActivityLimit> limitDetails  =hotelActivityLimitDao.findByCriteria(dc);
	
	Map<Timestamp,HotelActivityLimit> hpldMap = new HashMap<Timestamp,HotelActivityLimit>();
	if(!limitDetails.isEmpty()){
		for(HotelActivityLimit hp : limitDetails){
			hpldMap.put(Timestamp.valueOf(hp.getUseDate().toString()), hp);
		}
	}
	
	List<Timestamp> timeList = CommonMethod.getTimeStampList(CommonMethod.string2Time1(CommonMethod.dateToString(startDate)), 
				CommonMethod.string2Time1(CommonMethod.dateToString(endDate)));
	
	List<FullCalBean>  calList = new ArrayList<FullCalBean>();
	for(Timestamp ti : timeList){
		HotelActivityLimit limit = hpldMap.get(ti);
		FullCalBean cal = new FullCalBean();
		if(limit!=null){
			cal.setId(limit.getLimitId().toString());
			cal.setTitle(limit.getAmount()==null?"-":limit.getAmount().toString());
			cal.setStart(CommonMethod.toStartTime(Timestamp.valueOf(limit.getUseDate().toString())));
		}else{
			cal.setId("");
			cal.setTitle("-");
			cal.setStart(CommonMethod.toStartTime(ti));
		}
		calList.add(cal);
	}
	
	return calList;
}

@Override
public void saveFull(FullCalBean fullCalBean,HotelActivityLimit hal) {
	
	DetachedCriteria dc = DetachedCriteria.forClass(HotelActivityLimit.class);
	dc.add(Property.forName("useDate").ge(CommonMethod.toStartTime(fullCalBean.getStart())));
	dc.add(Property.forName("useDate").le(CommonMethod.toEndTime(fullCalBean.getEnd())));
	dc.add(Property.forName("hotelRoomType.roomTypeId").eq(hal.getHotelRoomType().getRoomTypeId()));
	dc.add(Property.forName("hotelActivity.hotelActivityId").eq(hal.getHotelActivity().getHotelActivityId()));
	dc.addOrder(Order.asc("useDate"));
	List<HotelActivityLimit> hpldList = hotelActivityLimitDao.findByCriteria(dc);
	
	Map<Timestamp,HotelActivityLimit> hpldMap = new HashMap<Timestamp,HotelActivityLimit>();
	if(!hpldList.isEmpty()){
		for(HotelActivityLimit hp : hpldList){
			hpldMap.put(Timestamp.valueOf(hp.getUseDate().toString()), hp);
		}
	}
	
	List<Timestamp> tList = CommonMethod.getTimeStampList(fullCalBean.getStart(), fullCalBean.getEnd());
	
	for(Timestamp t : tList){
		
		HotelActivityLimit hp = hpldMap.get(t);
		
		int remainAmount = Integer.parseInt(fullCalBean.getTitle()) - hotelPriceLimitDetailDao.findBookingAmount(t,hal.getHotelRoomType(),"act",hal.getHotelActivity().getHotelActivityId());
		if(remainAmount<0){
			remainAmount = 0;
		}
		
		if (hp!=null) {
			hp.setAmount(Integer.parseInt(fullCalBean.getTitle()));
			hp.setRemainAmount(remainAmount);
			hotelActivityLimitDao.update(hp);
			
		}else{
			HotelActivityLimit limit = new HotelActivityLimit();
			limit.setLimitId(getNextKey("HOTEL_ACTIVITY_LIMIT", 1));
			limit.setHotelActivity(hal.getHotelActivity());
			limit.setHotelRoomType(hal.getHotelRoomType());
			limit.setAmount(Integer.parseInt(fullCalBean.getTitle()));
			limit.setRemainAmount(remainAmount);
			limit.setUseDate(t);
			hotelActivityLimitDao.save(limit);
		}
	}
	
	//写酒店接口start
	hotelBookingLimitService.updateLockRoomFromXrws(fullCalBean, hal.getHotelRoomType());
	//写酒店接口end
	
}

public void checkDays(HotelActivityLimit t,String days){
	
	String customerType = "01";
	t.setEndDate(CommonMethod.addDay(t.getEndDate(), -1));
	if(t.getCustomerType()!=null && !t.getCustomerType().equals("")){//如无客户类型设置类型
		customerType = t.getCustomerType();
	}
	else{
		SysUser sysUser = getSysUser();
		if("03".equals(sysUser.getUserType())){
			customerType = "01";
		}
		else{
			customerType = "02";
		}
	}
	
	//查询活动日期
	DetachedCriteria dcAct = DetachedCriteria.forClass(HotelActivity.class);
	if(t.getHotelActivity()!=null && t.getHotelActivity().getHotelActivityId() !=null){
		dcAct.add(Property.forName("hotelActivityId").eq(t.getHotelActivity().getHotelActivityId()));
	}else{//没有传活动ID不查询出数据
		dcAct.add(Property.forName("hotelActivityId").eq(-1L));
	}
	if(t.getStartDate()!=null){
		dcAct.add(Property.forName("startDate").le(t.getStartDate()));
	}
	if(t.getEndDate()!=null){
		dcAct.add(Property.forName("endDate").ge(t.getEndDate()));
	}
	
	List<HotelActivity> haList = hotelActivityDao.findByCriteria(dcAct);
	if(haList==null || haList.size()<1){//没有符合条件的活动
		throw new BusinessException("没有符合条件的酒店活动！请重新确认订单！","");
	}
	
	//查询符合条件的活动明细
	DetachedCriteria dcActDet = DetachedCriteria.forClass(HotelActivityDetail.class);
	dcActDet.createAlias("hotelRoomType", "hotelRoomType");
	if(t.getHotelActivity()!=null && t.getHotelActivity().getHotelActivityId() !=null){
		dcActDet.add(Property.forName("hotelActivity.hotelActivityId").eq(t.getHotelActivity().getHotelActivityId()));
	}else{//没有传活动ID不查询出数据
		dcActDet.add(Property.forName("hotelActivity.hotelActivityId").eq(-1L));
	}
	dcActDet.add(Property.forName("customerType").eq(customerType));
	
	if(t.getHotelRoomType()!=null && t.getHotelRoomType().getRoomTypeId() !=null){
		dcActDet.add(Property.forName("hotelRoomType.roomTypeId").eq(t.getHotelRoomType().getRoomTypeId()));
	}
	if(t.getHotelRoomType()!=null && t.getHotelRoomType().getHotelId() !=null){
		dcActDet.add(Property.forName("hotelRoomType.hotel.hotelId").eq(t.getHotelRoomType().getHotelId()));
	}
	List<HotelActivityDetail> hadList = hotelActivityDetailDao.findByCriteria(dcActDet);
	
	int minDays = -1;
	int maxDays = -1;
	
	for(HotelActivityDetail had : hadList){
		
		int minDaysT = had.getMinDays() == null ? 0 : had.getMinDays();
		int maxDaysT = had.getMaxDays() == null ? 0 : had.getMaxDays();
		
		if(minDays<0 || minDaysT<minDays){
			minDays = minDaysT;
		}
		if((maxDays<0 || maxDaysT>maxDays) && maxDays!=0){
			maxDays = maxDaysT;
		}
	}
	
	int daysI = Integer.valueOf(days);
	
	if(daysI<minDays){//没有符合条件的活动
		throw new BusinessException("本活动最小订房天数为"+minDays+"天！您选择的预订时间为"+daysI+"天！请重新选择！","");
	}
	if(daysI>maxDays && maxDays!=0){
		throw new BusinessException("本活动最大订房天数为"+maxDays+"天！您选择的预订时间为"+daysI+"天！请重新选择！","");
	}
	
}
}