package service.hotel.hotelActivityLimit;
import java.util.Date;
import java.util.List;

import com.xlem.dao.HotelActivityLimit;

import common.FullCalBean;
import common.service.BaseService;

public interface HotelActivityLimitService extends BaseService<HotelActivityLimit> {

	public List<FullCalBean> findByFull(Date startDate, Date endDate,HotelActivityLimit t);
	
	public void saveFull(FullCalBean fullCalBean,HotelActivityLimit hotelActivityLimit);
	
	public void checkDays(HotelActivityLimit hotelActivityLimit,String days);
	
}