package service.hotel.roomType;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashMap;
import java.util.TreeMap;

import com.xlem.dao.HotelRoomType;
import com.xlem.dao.RoomInfoQueryParam;

import common.action.JsonPager;
import common.service.BaseService;

public interface RoomTypeService extends BaseService<HotelRoomType> {

	void save(Collection<HotelRoomType> coll);

	JsonPager<HotelRoomType> findJsonPageForQueryRoom(JsonPager<HotelRoomType> jp, HotelRoomType hotelRoomType);
	
	/**
	 * 市场部修改订房订单时，获取订房明细信息
	 * @param jp
	 * @param hotelRoomType
	 * @return
	 */
	JsonPager<HotelRoomType> findJsonPageForQueryRoomForMKT(
			JsonPager<HotelRoomType> jp, HotelRoomType hotelRoomType);
	
	TreeMap<String,TreeMap<String,String>> findHotelRoomInfo(String pageN);

	TreeMap<String,HashMap<String,String>> findStatisticalHotelRoomData(Timestamp startDate,Timestamp endDate);

	TreeMap<String, HashMap<String, String>> findRealTimeRoomInfo(
			RoomInfoQueryParam roomInfoQueryParam);
}