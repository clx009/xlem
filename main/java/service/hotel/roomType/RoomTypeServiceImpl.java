package service.hotel.roomType;

import java.sql.Timestamp; 
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ws.ReturnInfo;
import ws.hotel.client.HotelIndentClient;
import ws.hotel.comm.HotelCode;

import com.thoughtworks.xstream.XStream;
import com.xlem.dao.AvailableRoomInfo;
import com.xlem.dao.Hotel;
import com.xlem.dao.HotelRoomType;
import com.xlem.dao.RoomInfoQueryParam;
import com.xlem.dao.TicketType;

import common.BusinessException;
import common.CommonMethod;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.hotel.hotel.HotelDao;
import dao.hotel.hotelIndentDetail.HotelIndentDetailDao;
import dao.hotel.roomType.RoomTypeDao;

@Service("roomTypeService")
public class RoomTypeServiceImpl extends BaseServiceImpl<HotelRoomType>
		implements RoomTypeService {
	@Autowired
	private RoomTypeDao roomTypeDao;
	@Autowired
	private HotelDao hotelDao;
	@Autowired
	private HotelIndentDetailDao hotelIndentDetailDao;
	
	private HotelIndentClient hotelIndentClient=new HotelIndentClient();
	
	@Override
	@Resource(name = "roomTypeDao")
	protected void initBaseDAO(BaseDao<HotelRoomType> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<HotelRoomType> findPageByCriteria(
			PaginationSupport<HotelRoomType> ps, HotelRoomType t) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketType.class);
		return roomTypeDao.findPageByCriteria(ps, Order.desc("roomTypeCode"), dc);
	}

	@Override
	public void save(HotelRoomType t) {
		t.setRoomTypeId(getNextKey("Hotel_Room_Type".toUpperCase(), 1));
		roomTypeDao.save(t);
	}

	@Override
	public JsonPager<HotelRoomType> findJsonPageByCriteria(
			JsonPager<HotelRoomType> jp, HotelRoomType t) {
		DetachedCriteria dc = DetachedCriteria.forClass(HotelRoomType.class);
		dc.createAlias("hotel", "hotel");
		dc.addOrder(Order.asc("hotel.serial"));
		dc.addOrder(Order.asc("roomTypeCode"));
		return roomTypeDao.findJsonPageByCriteria(jp, dc);
	}

	@Override
	public void save(Collection<HotelRoomType> coll) {
		for (HotelRoomType hotelRoomType : coll) {
			Hotel hotel = hotelDao.findById(hotelRoomType.getHotelId());
			hotelRoomType.setHotel(hotel);
			if (hotelRoomType.getRoomTypeId() == null) {
				hotelRoomType.setRoomTypeId(getNextKey("hotel_Room_Type", 1));
				hotelRoomType.setInputer(getCurrentUserId());
				hotelRoomType.setInputTime(CommonMethod.getTimeStamp());
				roomTypeDao.save(hotelRoomType);
			} else {
				hotelRoomType.setUpdater(getCurrentUserId());
				hotelRoomType.setUpdateTime(CommonMethod.getTimeStamp());
				roomTypeDao.update(hotelRoomType);
			}
		}
	}

	@Override
	public JsonPager<HotelRoomType> findJsonPageForQueryRoom(
			JsonPager<HotelRoomType> jp, HotelRoomType t) {
		DetachedCriteria dc = DetachedCriteria.forClass(HotelRoomType.class);
		dc.createAlias("hotel", "hotel");
		if(t.getHotel()!=null && t.getHotel().getHotelId()!=null){
			dc.add(Property.forName("hotel.hotelId").eq(t.getHotel().getHotelId()));
		}
		dc.add(Property.forName("status").eq("11"));//启用
		dc.addOrder(Order.desc("hotel.hotelId"));
		dc.addOrder(Order.asc("roomTypeCode"));
		return roomTypeDao.findJsonPageByCriteria(jp, dc);
	}

	@Override
	public JsonPager<HotelRoomType> findJsonPageForQueryRoomForMKT(
			JsonPager<HotelRoomType> jp, HotelRoomType hotelRoomType) {
		hotelIndentDetailDao.findJsonPageForQueryRoom(jp,hotelRoomType);
		return jp;
	}
	/**
	 * 获取页面表头数据
	 * TreeMap<String,TreeMap<String,String>>
	 *           |        |
	 *           |        Key表示房型ID,Value表示房型名称 
	 *           表示酒店名称，使用TreeMap按照升序排序            
	 * 排序后方便处理
	 */
	@Override
	public TreeMap<String, TreeMap<String, String>> findHotelRoomInfo(String pageN) {
		List<HotelRoomType> hotelRoomTypeList=this.roomTypeDao.findAll();
		TreeMap<String, TreeMap<String, String>> hotelRoomTypeInfo=new TreeMap<String, TreeMap<String, String>>();
		for(HotelRoomType hotelRoomType:hotelRoomTypeList){
			if("realTime".equals(pageN) && hotelRoomType.getInputer()!=null && hotelRoomType.getInputer()<0L){//实时房态页面排除特价房
				continue;
			}
			if("11".equals(hotelRoomType.getStatus())){
				String hotelName=hotelRoomType.getHotel().getHotelName();
				TreeMap<String,String> roomTypeInfo=hotelRoomTypeInfo.get(hotelName);
				if(roomTypeInfo==null){
					roomTypeInfo=new TreeMap<String,String>();
					roomTypeInfo.put(hotelRoomType.getRoomTypeId().toString(),hotelRoomType.getTypeName()+"<br/>(已订)");
					roomTypeInfo.put(hotelRoomType.getRoomTypeId().toString()+"R",hotelRoomType.getTypeName()+"<br/>(剩余)");
					hotelRoomTypeInfo.put(hotelName, roomTypeInfo);
				}else{
					roomTypeInfo.put(hotelRoomType.getRoomTypeId().toString(),hotelRoomType.getTypeName()+"<br/>(已订)");
					roomTypeInfo.put(hotelRoomType.getRoomTypeId().toString()+"R",hotelRoomType.getTypeName()+"<br/>(剩余)");
				}
			}
		}		//realTime
		return hotelRoomTypeInfo;
	}
	@Override
	public TreeMap<String,HashMap<String,String>> findStatisticalHotelRoomData(Timestamp startDate,Timestamp endDate) {
		return this.roomTypeDao.findRoomTypeAmountInfo(startDate,endDate);
	}

	/**
	 * TreeMap<String,HashMap<String,String>>
	 *           |        |
	 *           |        Key表示房型ID,Value表示房型ID对应的房型可用数量     
	 *           表示查询 时间，使用TreeMap按照升序排序
	 *                  
	 */
	@Override
	public TreeMap<String, HashMap<String, String>> findRealTimeRoomInfo(
			RoomInfoQueryParam roomInfoQueryParam) {
		List<Hotel> hotels=this.hotelDao.findAll();
		TreeMap<String,HashMap<String,String>> realTimeRoomInfoMap=new TreeMap<String,HashMap<String,String>>();
		for(Hotel hotel:hotels){
			try {
				DetachedCriteria dc=DetachedCriteria.forClass(HotelRoomType.class);
				dc.add(Property.forName("hotel.hotelId").eq(hotel.getHotelId()));
				dc.add(Property.forName("status").eq("11"));
				dc.add(Property.forName("inputer").isNull());
				List<HotelRoomType> hrtList = roomTypeDao.findByCriteria(dc);
				roomInfoQueryParam.setHotelRoomTypes(hrtList);
				ReturnInfo retInfo=this.hotelIndentClient.getRealTimeRoomInfo(roomInfoQueryParam, HotelCode.valueOf((hotel.getHotelCode().trim())));
				this.fillRealTimeRoomInfoMap(retInfo,hotel,hrtList,realTimeRoomInfoMap);
			} catch (Exception e) {
				HashMap<String,String> excMap = realTimeRoomInfoMap.get("exc");
				if(excMap==null){
					excMap = new HashMap<String,String>();
				}
				if(e.getMessage().indexOf("JZ0C0: 连接已关闭")>=0 || e.getMessage().indexOf("timed out")>=0){//与酒店前台断开
					excMap.put(hotel.getHotelCode(), hotel.getHotelName()+":与酒店前台断开,无法查询数据！");
				}else if(e.getMessage().indexOf("接口出错")>=0){//与酒店服务器断开
					excMap.put(hotel.getHotelCode(), hotel.getHotelName()+":与酒店服务器断开,无法查询数据！");
				}else{//其他错误
					e.printStackTrace();
					throw new BusinessException("程序错误");
				}
				realTimeRoomInfoMap.put("exc",excMap);
			}
		}	
		return realTimeRoomInfoMap;
	}

	private void fillRealTimeRoomInfoMap(ReturnInfo retInfo,Hotel hotel,List<HotelRoomType> hrtList,
			TreeMap<String, HashMap<String, String>> realTimeRoomInfoMap){
		XStream infoXStream=new XStream();
		infoXStream.alias("obj",AvailableRoomInfo.class);
		infoXStream.alias("root",List.class);
		@SuppressWarnings("unchecked")
		List<AvailableRoomInfo> availRoomInfoList=(List<AvailableRoomInfo>)infoXStream.fromXML(retInfo.getResult());
		for(AvailableRoomInfo availableRoomInfo:availRoomInfoList){
			String currentDate=availableRoomInfo.getCurrentDate().toString();
			HashMap<String,String> roomInfo=realTimeRoomInfoMap.get(currentDate);
			if(roomInfo==null){
				roomInfo=new HashMap<String,String>();
				realTimeRoomInfoMap.put(currentDate,roomInfo);
			}
			for(HotelRoomType hotelRoomType:hrtList){
				if(hotelRoomType.getRoomTypeCode().equals(availableRoomInfo.getRoomTypeCode().trim())){
					roomInfo.put(hotelRoomType.getRoomTypeId().toString(),availableRoomInfo.getAvlquan().toString());
					break;
				}
			}
		}
	}
}








