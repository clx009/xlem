package service.hotel.financeHotelAccounts;
import java.util.List;

import com.xlem.dao.BaseTranRec;
import com.xlem.dao.FinanceHotelAccounts;
import com.xlem.dao.HotelCutNotes;
import com.xlem.dao.HotelIndent;
import com.xlem.dao.HotelIndentDetail;
import com.xlem.dao.HotelRechargeRec;

import common.service.BaseService;

import dao.ticket.financeTicketAccounts.AllStatementObject;
import dao.ticket.financeTicketAccounts.StatementObject;
public interface FinanceHotelAccountsService extends BaseService<FinanceHotelAccounts> {

	/**
	 * 保存冲退财务信息，有财务信息则冲退，没有则不做操作
	 * @param hotelIndent 订单信息
	 * @param baseTranRec 交易信息
	 * @param hotelRechargeRec 充值退款信息
	 */
	public void saveReturnBackInfo(HotelIndent hotelIndent, BaseTranRec baseTranRec, HotelRechargeRec hotelRechargeRec);
	
	/**
	 * 将预付记录转为已付记录 流程：1冲退预付记录，2添加已付记录
	 * @param hotelIndent  订单信息
	 */
	public void savePayToPaid(HotelIndent hotelIndent);
	
	
	/**
	 * 将已付记录转为已消费记录 流程：1抵扣已付记录，2添加已消费记录
	 * @param hotelIndent  订单信息
	 */
	public void savePaidToExpend(String hotelIndentId);
	
	
	/**
	 * 保存付款财务信息
	 * @param hotelIndent 订单信息
	 * @param baseTranRec 交易信息
	 * @param hotelRechargeRec 充值退款信息
	 */
	void savePayInfo(HotelIndent hotelIndent, BaseTranRec baseTranRec, HotelRechargeRec hotelRechargeRec);
	
	/**
	 * 保存退订财务信息
	 * @param ticketIndent 订单信息
	 * @param hotelIndentDetails 订单明细，在部分退订时计算金额
	 * @param baseTranRec 交易信息
	 * @param hotelRechargeRec 充值退款信息
	 */
	void saveReturnInfo(HotelIndent hotelIndent, List<HotelIndentDetail> hotelIndentDetails, BaseTranRec baseTranRec, HotelRechargeRec hotelRechargeRec);
	
	/**
	 * 保存扣款财务信息
	 * @param hotelIndent 订单信息
	 * @param hotelCutNotes 扣款信息
	 * @param hotelRechargeRec 充值记录
	 */
	void saveCutInfo(HotelIndent hotelIndent, HotelCutNotes hotelCutNotes, HotelRechargeRec hotelRechargeRec);
	
	/**
	 * 保存奖励款充值和现金充值  财务信息
	 * @param hotelRechargeRec 充值记录
	 */
	void saveRewardOrCashTopUpInfo(HotelRechargeRec hotelRechargeRec);
	
	/**
	 * 财务报表总报表 肖宇翔
	 */
	List<AllStatementObject> findAllStatement(StatementObject fta);
	
	/**
	 * 初始化财务数据 仅计算2014-07-15之后的订单 肖宇翔
	 */
	void initFinance();
}