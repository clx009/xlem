package service.hotel.financeHotelAccounts;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.BaseTranRec;
import com.xlem.dao.FinanceHotelAccounts;
import com.xlem.dao.HotelCutNotes;
import com.xlem.dao.HotelDetailHis;
import com.xlem.dao.HotelIndent;
import com.xlem.dao.HotelIndentDetail;
import com.xlem.dao.HotelRechargeRec;

import common.BusinessException;
import common.CommonMethod;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.comm.baseTranRec.BaseTranRecDao;
import dao.hotel.financeHotelAccounts.FinanceHotelAccountsDao;
import dao.hotel.hotelCutNotes.HotelCutNotesDao;
import dao.hotel.hotelDetailHis.HotelDetailHisDao;
import dao.hotel.hotelIndent.HotelIndentDao;
import dao.hotel.hotelIndentDetail.HotelIndentDetailDao;
import dao.hotel.hotelRechargeRec.HotelRechargeRecDao;
import dao.ticket.financeTicketAccounts.AllStatementObject;
import dao.ticket.financeTicketAccounts.StatementObject;
@Service("financeHotelAccountsService")
public class FinanceHotelAccountsServiceImpl extends BaseServiceImpl<FinanceHotelAccounts> implements FinanceHotelAccountsService {
@Autowired
private FinanceHotelAccountsDao financeHotelAccountsDao;
@Autowired
private HotelIndentDao hotelIndentDao;
@Autowired
private HotelIndentDetailDao hotelIndentDetailDao;
@Autowired
private HotelDetailHisDao hotelDetailHisDao;
@Autowired
private BaseTranRecDao baseTranRecDao;
@Autowired
private HotelRechargeRecDao hotelRechargeRecDao;

@Autowired
private HotelCutNotesDao hotelCutNotesDao;

	@Override
	@Resource(name="financeHotelAccountsDao")
	protected void initBaseDAO(BaseDao<FinanceHotelAccounts> baseDao) {
		setBaseDao(baseDao);
	}
@Override
public PaginationSupport<FinanceHotelAccounts> findPageByCriteria(PaginationSupport<FinanceHotelAccounts> ps, FinanceHotelAccounts t) {
	DetachedCriteria dc = DetachedCriteria.forClass(FinanceHotelAccounts.class);
	return financeHotelAccountsDao.findPageByCriteria(ps, Order.desc("financeHotelAccountsId"), dc);
}
@Override
public void save(FinanceHotelAccounts t) {
	t.setHotelAccountsId(getNextKey("Finance_Hotel_Accounts".toUpperCase(), 1));
	financeHotelAccountsDao.save(t);
}
@Override
public JsonPager<FinanceHotelAccounts> findJsonPageByCriteria(
		JsonPager<FinanceHotelAccounts> jp, FinanceHotelAccounts t) {
	// TODO Auto-generated method stub
	return null;
}

/**
 * 排除冲退和已被冲退的记录,排除扣款记录
 * @return
 */
private List<FinanceHotelAccounts> excludeReturnBackInfo(List<FinanceHotelAccounts> ftaList) {
	
	List<FinanceHotelAccounts> ftaListRet = new ArrayList<FinanceHotelAccounts>();
	
	//先找出冲退和已被冲退的记录Id
	Map<String,Long> excludeIds = new HashMap<String,Long>();
	for(FinanceHotelAccounts fta : ftaList){
		Long bId = fta.getBackAccountsId();
		if(bId!=null){//是冲退记录
			excludeIds.put("id_"+bId, bId);//被冲退记录ID
			Long id = fta.getHotelAccountsId();
			excludeIds.put("id_"+id, id);//冲退记录ID
		}
	}
	
	//将其他记录放到新List
	for(FinanceHotelAccounts fta : ftaList){
		Long id = fta.getHotelAccountsId();
		
		Long excludeId = excludeIds.get("id_"+id);
		if(excludeId==null && !FinanceHotelAccounts.PAYMENTTYPE_08.equals(fta.getPaymentType())){
			ftaListRet.add(fta);
		}
	}
	
	return ftaListRet;
}

@Override
public void saveReturnBackInfo(HotelIndent hotelIndent, BaseTranRec baseTranRec, HotelRechargeRec hotelRechargeRec){
	Timestamp thisTime = CommonMethod.getTimeStamp();
	this.saveReturnBackInfo(hotelIndent, baseTranRec, hotelRechargeRec, thisTime,"");
}

private void saveReturnBackInfo(HotelIndent hotelIndent, BaseTranRec baseTranRec, HotelRechargeRec hotelRechargeRec,Timestamp thisTime,String remark) {
	
	
	DetachedCriteria dc = DetachedCriteria.forClass(FinanceHotelAccounts.class);
	dc.add(Property.forName("hotelIndentId").eq(hotelIndent.getHotelIndentId().toString()));
	List<FinanceHotelAccounts> ftaList =  financeHotelAccountsDao.findByCriteria(dc);
	
	Long rechargeRecId = hotelRechargeRec == null ? null : hotelRechargeRec.getHotelRechargeRecId();
	String tranRecId = baseTranRec == null ? null : baseTranRec.getTranRecId();
	
	//排除冲退和已被冲退的记录
	List<FinanceHotelAccounts> ftaListRe =  excludeReturnBackInfo(ftaList);
	
	for(FinanceHotelAccounts ftaO : ftaListRe){
		
		FinanceHotelAccounts ftaB = new FinanceHotelAccounts(getNextKey("Finance_Hotel_Accounts", 1));
		ftaB.setAccountsType(ftaO.getAccountsType());
		ftaB.setInputTime(thisTime);
		ftaB.setPaymentType(FinanceHotelAccounts.PAYMENTTYPE_09);
		ftaB.setHotelRechargeRecId(ftaO.getHotelRechargeRecId());
		ftaB.setTranRecId(ftaO.getTranRecId());
		ftaB.setHotelIndentId(hotelIndent.getHotelIndentId().toString());
		ftaB.setHotelIndentCode(ftaO.getHotelIndentCode());
		ftaB.setHotelIndentDetailId(ftaO.getHotelIndentDetailId());
		ftaB.setRoomTypeId(ftaO.getRoomTypeId());
		ftaB.setTypeName(ftaO.getTypeName());
		ftaB.setMoney(0-(ftaO.getMoney()==null ? 0 : ftaO.getMoney()));
		ftaB.setAmount(0-(ftaO.getAmount()==null ? 0 : ftaO.getAmount()));
		ftaB.setBackAccountsId(ftaO.getHotelAccountsId());
		ftaB.setRemark(remark);
		financeHotelAccountsDao.save(ftaB);
	}
	
	
}

@Override
public void savePayToPaid(HotelIndent hotelIndent){
	Timestamp thisTime = CommonMethod.getTimeStamp();
	this.savePayToPaid(hotelIndent, thisTime,"");
}

private void savePayToPaid(HotelIndent hotelIndent,Timestamp thisTime,String remark) {
	
	
	DetachedCriteria dc = DetachedCriteria.forClass(FinanceHotelAccounts.class);
	dc.add(Property.forName("hotelIndentId").eq(hotelIndent.getHotelIndentId().toString()));
	List<FinanceHotelAccounts> ftaList =  financeHotelAccountsDao.findByCriteria(dc);
	
	//排除冲退和已被冲退的记录
	List<FinanceHotelAccounts> ftaListRe =  excludeReturnBackInfo(ftaList);
	
	for(FinanceHotelAccounts ftaO : ftaListRe){
		
		//先冲退
		FinanceHotelAccounts ftaB = new FinanceHotelAccounts(getNextKey("Finance_Hotel_Accounts", 1));
		
		
		ftaB.setAccountsType(ftaO.getAccountsType());
		ftaB.setInputTime(thisTime);
		ftaB.setPaymentType(FinanceHotelAccounts.PAYMENTTYPE_09);
		ftaB.setHotelRechargeRecId(ftaO.getHotelRechargeRecId());
		ftaB.setTranRecId(ftaO.getTranRecId());
		ftaB.setHotelIndentId(hotelIndent.getHotelIndentId().toString());
		ftaB.setHotelIndentCode(ftaO.getHotelIndentCode());
		ftaB.setHotelIndentDetailId(ftaO.getHotelIndentDetailId());
		ftaB.setRoomTypeId(ftaO.getRoomTypeId());
		ftaB.setTypeName(ftaO.getTypeName());
		ftaB.setMoney(0-(ftaO.getMoney()==null ? 0 : ftaO.getMoney()));
		ftaB.setAmount(0-(ftaO.getAmount()==null ? 0 : ftaO.getAmount()));
		ftaB.setBackAccountsId(ftaO.getHotelAccountsId());
		ftaB.setRemark(remark);
		financeHotelAccountsDao.save(ftaB);
		
		//添加支付记录
		FinanceHotelAccounts ftaN = new FinanceHotelAccounts(getNextKey("Finance_Hotel_Accounts", 1));
		ftaN.setAccountsType(ftaO.getAccountsType());
		ftaN.setInputTime(thisTime);
		ftaN.setPaymentType(FinanceHotelAccounts.PAYMENTTYPE_01);
		ftaN.setHotelRechargeRecId(ftaO.getHotelRechargeRecId());
		ftaN.setTranRecId(ftaO.getTranRecId());
		ftaN.setHotelIndentId(hotelIndent.getHotelIndentId().toString());
		ftaN.setHotelIndentCode(ftaO.getHotelIndentCode());
		ftaN.setHotelIndentDetailId(ftaO.getHotelIndentDetailId());
		ftaN.setRoomTypeId(ftaO.getRoomTypeId());
		ftaN.setTypeName(ftaO.getTypeName());
		ftaN.setMoney((ftaO.getMoney()==null ? 0 : ftaO.getMoney()));
		ftaN.setAmount((ftaO.getAmount()==null ? 0 : ftaO.getAmount()));
		ftaN.setRemark(remark);
		financeHotelAccountsDao.save(ftaN);
	}
	
	
}

@Override
public void savePaidToExpend(String hotelIndentId){
	Timestamp thisTime = CommonMethod.getTimeStamp();
	this.savePaidToExpend(hotelIndentId, thisTime,"");
}

private void savePaidToExpend(String hotelIndentId,Timestamp thisTime,String remark) {
	
	DetachedCriteria dc = DetachedCriteria.forClass(FinanceHotelAccounts.class);
	dc.add(Property.forName("hotelIndentId").eq(hotelIndentId));
	List<FinanceHotelAccounts> ftaList =  financeHotelAccountsDao.findByCriteria(dc);
	
	
	//排除冲退和已被冲退的记录
	List<FinanceHotelAccounts> ftaListRe =  excludeReturnBackInfo(ftaList);
	
	String accountsType = "";
	Long rechargeRecId = null;
	String tranRecId = "";
	String indentCode = "";
	String pType = "";
	for(FinanceHotelAccounts fha : ftaListRe){
		if(FinanceHotelAccounts.PAYMENTTYPE_01.equals(fha.getPaymentType()) || FinanceHotelAccounts.PAYMENTTYPE_02.equals(fha.getPaymentType())){
			accountsType = fha.getAccountsType();
			rechargeRecId = fha.getHotelRechargeRecId();
			tranRecId = fha.getTranRecId();
			indentCode = fha.getHotelIndentCode();
			pType = fha.getPaymentType();
			break;
		}
	}
	//根据订单记录计算已消费金额及数量
	DetachedCriteria dcHi = DetachedCriteria.forClass(HotelIndentDetail.class);
	dcHi.createAlias("hotelRoomType", "hotelRoomType");
	//dc.createAlias("hotelRoomType.hotel", "hotel");
	dcHi.add(Property.forName("hotelIndent.hotelIndentId").eq(Long.valueOf(hotelIndentId)));
	dcHi.addOrder(Order.asc("hotelIndentDetailId"));//按订单明细排序保证同意订单每次进来查询的顺序一致
	List<HotelIndentDetail> tidList =  hotelIndentDetailDao.findByCriteria(dcHi);
	
	for (HotelIndentDetail detail : tidList) {
		
		int amount = detail.getAmount();
		double uPrice = detail.getUnitPrice()==null ? 0 : detail.getUnitPrice();
		double money = uPrice*amount;
		
		//先冲退
		FinanceHotelAccounts ftaB = new FinanceHotelAccounts(getNextKey("Finance_Hotel_Accounts", 1));
		ftaB.setAccountsType(accountsType);
		ftaB.setInputTime(thisTime);
		ftaB.setPaymentType(FinanceHotelAccounts.PAYMENTTYPE_10);
		ftaB.setHotelRechargeRecId(rechargeRecId);
		ftaB.setTranRecId(tranRecId);
		ftaB.setHotelIndentId(hotelIndentId);
		ftaB.setHotelIndentCode(indentCode);
		ftaB.setHotelIndentDetailId(detail.getHotelIndentDetailId());
		ftaB.setRoomTypeId(detail.getHotelRoomType().getRoomTypeId());
		ftaB.setTypeName(detail.getHotelRoomType().getTypeName());
		ftaB.setMoney(money);
		ftaB.setAmount(amount);
		ftaB.setRemark(remark);
		financeHotelAccountsDao.save(ftaB);
		
		String nType = "";
		if(FinanceHotelAccounts.PAYMENTTYPE_01.equals(pType)){
			nType = FinanceHotelAccounts.PAYMENTTYPE_04;
		}else if(FinanceHotelAccounts.PAYMENTTYPE_02.equals(pType)){
			nType = FinanceHotelAccounts.PAYMENTTYPE_05;
		}else{
			throw new BusinessException("错误的支付类型:"+hotelIndentId);
		}
		
		//添加支付记录
		FinanceHotelAccounts ftaN = new FinanceHotelAccounts(getNextKey("Finance_Hotel_Accounts", 1));
		ftaN.setAccountsType(accountsType);
		ftaN.setInputTime(thisTime);
		ftaN.setPaymentType(nType);
		ftaN.setHotelRechargeRecId(rechargeRecId);
		ftaN.setTranRecId(tranRecId);
		ftaN.setHotelIndentId(hotelIndentId);
		ftaN.setHotelIndentCode(indentCode);
		ftaN.setHotelIndentDetailId(detail.getHotelIndentDetailId());
		ftaN.setRoomTypeId(detail.getHotelRoomType().getRoomTypeId());
		ftaN.setTypeName(detail.getHotelRoomType().getTypeName());
		ftaN.setMoney(money);
		ftaN.setAmount(amount);
		ftaN.setRemark(remark);
		financeHotelAccountsDao.save(ftaN);
	}
	
}
@Override
public void savePayInfo(HotelIndent hotelIndent,BaseTranRec baseTranRec,HotelRechargeRec hotelRechargeRec){
	Timestamp thisTime = CommonMethod.getTimeStamp();
	DetachedCriteria dc = DetachedCriteria.forClass(HotelIndentDetail.class);
	dc.createAlias("hotelRoomType", "hotelRoomType");
	//dc.createAlias("hotelRoomType.hotel", "hotel");
	dc.add(Property.forName("hotelIndent.hotelIndentId").eq(hotelIndent.getHotelIndentId()));
	dc.addOrder(Order.asc("hotelIndentDetailId"));//按订单明细排序保证同意订单每次进来查询的顺序一致
	List<HotelIndentDetail> tidList =  hotelIndentDetailDao.findByCriteria(dc);
	this.savePayInfo(hotelIndent, baseTranRec, hotelRechargeRec, thisTime,tidList,"");
}


private void savePayInfo(HotelIndent hotelIndent,BaseTranRec baseTranRec,HotelRechargeRec hotelRechargeRec,Timestamp thisTime,List<HotelIndentDetail> tidList,String remark) {
	
	List<FinanceHotelAccounts> hotelAccounts = new ArrayList<FinanceHotelAccounts>();
	
	//判断账目类型
	String accountsType = "";
	
	String iPtype = hotelIndent.getPayType();
	String origin = hotelIndent.getOrigin();
	
	//判断支付类型
	String pType = "";
	
	if(!"00".equals(origin)){//市场部订单
		pType = FinanceHotelAccounts.PAYMENTTYPE_00;
		accountsType="03";
	}else if("01".equals(iPtype)){//奖励款
		pType = FinanceHotelAccounts.PAYMENTTYPE_02;
		accountsType="01";
	}else if("9".equals(iPtype)){//现金网银支付
		accountsType="02";
		pType = FinanceHotelAccounts.PAYMENTTYPE_01;
	}else{
		throw new BusinessException("错误的付款方式");
	}
	
	Long rechargeRecId = hotelRechargeRec == null ? null : hotelRechargeRec.getHotelRechargeRecId();
	String tranRecId = baseTranRec == null ? null : baseTranRec.getTranRecId();
	
	
	for (HotelIndentDetail detail : tidList) {
		
		int amount = detail.getAmount();
		
		double uPrice = detail.getUnitPrice()==null ? 0 : detail.getUnitPrice();
		
		double money = uPrice*amount;
		
		FinanceHotelAccounts financeHotelAccounts = new FinanceHotelAccounts(getNextKey("Finance_Hotel_Accounts", 1));
		financeHotelAccounts.setAccountsType(accountsType);
		financeHotelAccounts.setInputTime(thisTime);
		financeHotelAccounts.setPaymentType(pType);
		financeHotelAccounts.setHotelRechargeRecId(rechargeRecId);
		financeHotelAccounts.setTranRecId(tranRecId);
		financeHotelAccounts.setHotelIndentId(hotelIndent.getHotelIndentId().toString());
		financeHotelAccounts.setHotelIndentCode(hotelIndent.getIndentCode());
		financeHotelAccounts.setHotelIndentDetailId(detail.getHotelIndentDetailId());
		financeHotelAccounts.setRoomTypeId(detail.getHotelRoomType().getRoomTypeId());
		financeHotelAccounts.setTypeName(detail.getHotelRoomType().getTypeName());
		financeHotelAccounts.setMoney(money);
		financeHotelAccounts.setAmount(amount);
		financeHotelAccounts.setRemark(remark);
		hotelAccounts.add(financeHotelAccounts);
		
	}
	
	for(FinanceHotelAccounts fta : hotelAccounts){
		financeHotelAccountsDao.save(fta);
	}
	
}

@Override
public void saveReturnInfo(HotelIndent hotelIndent, List<HotelIndentDetail> hotelIndentDetails, BaseTranRec baseTranRec, HotelRechargeRec hotelRechargeRec){
	Timestamp thisTime = CommonMethod.getTimeStamp();
	
	//判断是否全退
	boolean allRet = true;
	Map<String,HotelIndentDetail> hidMap = new HashMap<String,HotelIndentDetail>();
	for(HotelIndentDetail hid : hotelIndentDetails){
		if(hid.getAmount()!=hid.getAmountret()){//部分退
			allRet = false;
		}
		hidMap.put("date_"+hid.getBookingDate(), hid);
		hidMap.put("delId_"+hid.getHotelIndentDetailId(), hid);
	}
	
//	if(allRet){//全退
//		this.saveReturnInfo(hotelIndent, baseTranRec, hotelRechargeRec, thisTime,"");
//	}else{//部分退
		this.saveReturnInfo(hotelIndent, hidMap, baseTranRec, hotelRechargeRec, thisTime, "");
//	}
}

/**
 * 订单退部分
 * @param hotelIndent
 * @param hotelIndentDetails
 * @param baseTranRec
 * @param hotelRechargeRec
 * @param thisTime
 * @param remark
 */
private void saveReturnInfo(HotelIndent hotelIndent, Map<String,HotelIndentDetail> hotelIndentDetails, BaseTranRec baseTranRec, HotelRechargeRec hotelRechargeRec,Timestamp thisTime,String remark){
	List<FinanceHotelAccounts> hotelAccounts = new ArrayList<FinanceHotelAccounts>();
	
	Long rechargeRecId = hotelRechargeRec == null ? null :hotelRechargeRec.getHotelRechargeRecId();
	String tranRecId = baseTranRec == null ? null : baseTranRec.getTranRecId();
	String returnPayType = baseTranRec == null ? null : baseTranRec.getPayType();
	
	//查询订单历史记录用于匹配支付记录
	DetachedCriteria dc = DetachedCriteria.forClass(HotelDetailHis.class);
	dc.createAlias("hotelRoomType", "hotelRoomType");
	//dc.createAlias("hotelRoomType.hotel", "hotel");
	dc.add(Property.forName("hotelIndent.hotelIndentId").eq(hotelIndent.getHotelIndentId()));
	dc.add(Property.forName("time").eq(1));//仅需要第一组记录
	dc.addOrder(Order.asc("hotelIndentDetailId"));//按订单明细排序保证同意订单每次进来查询的顺序一致
	List<HotelDetailHis> tdhList =  hotelDetailHisDao.findByCriteria(dc);
	Map<String,HotelDetailHis> tdhMap = new HashMap<String,HotelDetailHis>();
	for(HotelDetailHis tdh : tdhList){
		tdhMap.put("delId_"+tdh.getHotelIndentDetailId(), tdh);
	}
	
	DetachedCriteria dcFTA = DetachedCriteria.forClass(FinanceHotelAccounts.class);
	dcFTA.add(Property.forName("hotelIndentId").eq(hotelIndent.getHotelIndentId().toString()));
	dcFTA.addOrder(Order.asc("paymentType"));//排序将网银支付记录排在前面
	List<FinanceHotelAccounts> ftaList =  financeHotelAccountsDao.findByCriteria(dcFTA);
	
	//排除冲退和已被冲退的记录
	List<FinanceHotelAccounts> ftaListRe =  excludeReturnBackInfo(ftaList);
	
	for(FinanceHotelAccounts ftaO : ftaListRe){
		
		String paymentType = ftaO.getPaymentType();
		//如果是退款记录则跳过
		if(FinanceHotelAccounts.PAYMENTTYPE_06.equals(paymentType) || FinanceHotelAccounts.PAYMENTTYPE_07.equals(paymentType)){
			continue;
		}
		
		HotelIndentDetail hid;
		HotelDetailHis tdh = tdhMap.get("delId_"+ftaO.getHotelIndentDetailId());
		if(tdh==null){
			hid = hotelIndentDetails.get("delId_"+ftaO.getHotelIndentDetailId());
		}else{
			hid = hotelIndentDetails.get("date_"+tdh.getBookingDate());
		}
		
		int amountFtaOld = (hid.getAmountret() == null ? 0 : hid.getAmountret());
		//退款金额为=退款-扣款
		double moneyFtaOld = amountFtaOld * hid.getUnitPrice()-(hid.getCutPrice() == null ? 0 : hid.getCutPrice());
		
		FinanceHotelAccounts financeHotelAccounts = new FinanceHotelAccounts(getNextKey("Finance_Hotel_Accounts", 1));
		
		String paymentTypeReturn="";
		if(FinanceHotelAccounts.PAYMENTTYPE_00.equals(paymentType) || 
			(FinanceHotelAccounts.ACCOUNTSTYPE_03.equals(ftaO.getAccountsType()) && FinanceHotelAccounts.PAYMENTTYPE_01.equals(paymentType))){//订单预扣余额和市场部订单支付未消费改为冲退
			paymentTypeReturn = FinanceHotelAccounts.PAYMENTTYPE_09;
			financeHotelAccounts.setBackAccountsId(ftaO.getHotelAccountsId());
		}else if(FinanceHotelAccounts.PAYMENTTYPE_01.equals(paymentType)){//网银已支付未消费金额改为网银付款退款到卡上
			paymentTypeReturn = FinanceHotelAccounts.PAYMENTTYPE_06;
			financeHotelAccounts.setReturnAccountsId(ftaO.getHotelAccountsId());
		}else if(FinanceHotelAccounts.PAYMENTTYPE_02.equals(paymentType)){//余额已支付未消费金额改为余额付款退款到余额
			paymentTypeReturn = FinanceHotelAccounts.PAYMENTTYPE_07;
			financeHotelAccounts.setReturnAccountsId(ftaO.getHotelAccountsId());
		}else{
			throw new BusinessException("不支持的付款类型","");
		}
		
		financeHotelAccounts.setAccountsType(ftaO.getAccountsType());
		financeHotelAccounts.setInputTime(thisTime);
		financeHotelAccounts.setPaymentType(paymentTypeReturn);
		financeHotelAccounts.setHotelRechargeRecId(rechargeRecId);
		financeHotelAccounts.setTranRecId(tranRecId);
		financeHotelAccounts.setHotelIndentId(hotelIndent.getHotelIndentId().toString());
		financeHotelAccounts.setHotelIndentCode(hotelIndent.getIndentCode());
		financeHotelAccounts.setHotelIndentDetailId(hid.getHotelIndentDetailId());
		financeHotelAccounts.setRoomTypeId(ftaO.getRoomTypeId());
		financeHotelAccounts.setTypeName(ftaO.getTypeName());
		financeHotelAccounts.setMoney(0-moneyFtaOld);
		financeHotelAccounts.setAmount(0-amountFtaOld);
		financeHotelAccounts.setRemark(remark);
		hotelAccounts.add(financeHotelAccounts);
		
	}
	
	for(FinanceHotelAccounts fta : hotelAccounts){
		financeHotelAccountsDao.save(fta);
	}
	
}

/**
 * 订单全退
 * @param hotelIndent
 * @param baseTranRec
 * @param hotelRechargeRec
 * @param thisTime
 * @param remark
 */
private void saveReturnInfo(HotelIndent hotelIndent, BaseTranRec baseTranRec, HotelRechargeRec hotelRechargeRec,Timestamp thisTime,String remark) {
	List<FinanceHotelAccounts> hotelAccounts = new ArrayList<FinanceHotelAccounts>();
	
	Long rechargeRecId = hotelRechargeRec == null ? null :hotelRechargeRec.getHotelRechargeRecId();
	String tranRecId = baseTranRec == null ? null : baseTranRec.getTranRecId();
	String returnPayType = baseTranRec == null ? null : baseTranRec.getPayType();
	
	DetachedCriteria dcFTA = DetachedCriteria.forClass(FinanceHotelAccounts.class);
	dcFTA.add(Property.forName("hotelIndentId").eq(hotelIndent.getHotelIndentId().toString()));
	dcFTA.addOrder(Order.asc("paymentType"));//排序将网银支付记录排在前面
	List<FinanceHotelAccounts> ftaList =  financeHotelAccountsDao.findByCriteria(dcFTA);
	
	//排除冲退和已被冲退的记录
	List<FinanceHotelAccounts> ftaListRe =  excludeReturnBackInfo(ftaList);
	
	for(FinanceHotelAccounts ftaO : ftaListRe){
		
		int amountFtaOld = (ftaO.getAmount() == null ? 0 : ftaO.getAmount());
		double moneyFtaOld = (ftaO.getMoney() == null ? 0 : ftaO.getMoney());
		
		String paymentType = ftaO.getPaymentType();
		
		FinanceHotelAccounts financeHotelAccounts = new FinanceHotelAccounts(getNextKey("Finance_Hotel_Accounts", 1));
		
		String paymentTypeReturn="";
		if(FinanceHotelAccounts.PAYMENTTYPE_00.equals(paymentType) || 
			(FinanceHotelAccounts.ACCOUNTSTYPE_03.equals(ftaO.getAccountsType()) && FinanceHotelAccounts.PAYMENTTYPE_01.equals(paymentType))){//订单预扣余额和市场部订单支付未消费改为冲退
			paymentTypeReturn = FinanceHotelAccounts.PAYMENTTYPE_09;
			financeHotelAccounts.setBackAccountsId(ftaO.getHotelAccountsId());
		}else if(FinanceHotelAccounts.PAYMENTTYPE_01.equals(paymentType)){//网银已支付未消费金额改为网银付款退款到卡上
			paymentTypeReturn = FinanceHotelAccounts.PAYMENTTYPE_06;
		}else if(FinanceHotelAccounts.PAYMENTTYPE_02.equals(paymentType)){//余额已支付未消费金额改为余额付款退款到余额
			paymentTypeReturn = FinanceHotelAccounts.PAYMENTTYPE_07;
		}else{
			throw new BusinessException("不支持的付款类型","");
		}
		
		financeHotelAccounts.setAccountsType(ftaO.getAccountsType());
		financeHotelAccounts.setInputTime(thisTime);
		financeHotelAccounts.setPaymentType(paymentTypeReturn);
		financeHotelAccounts.setHotelRechargeRecId(rechargeRecId);
		financeHotelAccounts.setTranRecId(tranRecId);
		financeHotelAccounts.setHotelIndentId(hotelIndent.getHotelIndentId().toString());
		financeHotelAccounts.setHotelIndentCode(hotelIndent.getIndentCode());
		financeHotelAccounts.setHotelIndentDetailId(ftaO.getHotelIndentDetailId());
		financeHotelAccounts.setRoomTypeId(ftaO.getRoomTypeId());
		financeHotelAccounts.setTypeName(ftaO.getTypeName());
		financeHotelAccounts.setMoney(0-moneyFtaOld);
		financeHotelAccounts.setAmount(0-amountFtaOld);
		financeHotelAccounts.setRemark(remark);
		hotelAccounts.add(financeHotelAccounts);
		
	}
	
	for(FinanceHotelAccounts fta : hotelAccounts){
		financeHotelAccountsDao.save(fta);
	}
	
}

@Override
public void saveRewardOrCashTopUpInfo(HotelRechargeRec hotelRechargeRec){
	this.saveRewardOrCashTopUpInfo(hotelRechargeRec, "");
}

private void saveRewardOrCashTopUpInfo(HotelRechargeRec hotelRechargeRec,String remark) {
	FinanceHotelAccounts financeHotelAccounts = new FinanceHotelAccounts(getNextKey("FINANCE_HOTEL_ACCOUNTS", 1));
	//判断账目类型
	String accountsType = "01";
	String paymentType = "03";
	
	double money = hotelRechargeRec.getRewardMoney() == null ? 0D : hotelRechargeRec.getRewardMoney();
	financeHotelAccounts.setAccountsType(accountsType);
	financeHotelAccounts.setPaymentType(paymentType);
	financeHotelAccounts.setMoney(money);
	financeHotelAccounts.setInputTime(hotelRechargeRec.getInputTime());
	financeHotelAccounts.setHotelRechargeRecId(hotelRechargeRec.getHotelRechargeRecId());
	financeHotelAccounts.setRemark(remark);
	financeHotelAccountsDao.save(financeHotelAccounts);
}

@Override
public void saveCutInfo(HotelIndent hotelIndent, HotelCutNotes hotelCutNotes, HotelRechargeRec hotelRechargeRec){
	Timestamp thisTime = CommonMethod.getTimeStamp();
	this.saveCutInfo(hotelIndent, hotelCutNotes, hotelRechargeRec, thisTime,"");
}

private void saveCutInfo(HotelIndent hotelIndent, HotelCutNotes hotelCutNotes, HotelRechargeRec hotelRechargeRec,Timestamp thisTime,String remark) {
	
	//判断账目类型
	String accountsType = "";
	
	String iPtype = hotelIndent.getPayType();
	String origin = hotelIndent.getOrigin();
	
	//判断支付类型
	String pType = FinanceHotelAccounts.PAYMENTTYPE_08;
	
	if(!"00".equals(origin)){//市场部订单
		accountsType="03";
	}else if("01".equals(iPtype)){//奖励款
		accountsType="01";
	}else if("9".equals(iPtype)){//现金网银支付
		accountsType="02";
	}else{
		throw new BusinessException("错误的付款方式");
	}
	
	FinanceHotelAccounts financeHotelAccounts = new FinanceHotelAccounts(getNextKey("Finance_Hotel_Accounts", 1));
	financeHotelAccounts.setAccountsType(accountsType);
	financeHotelAccounts.setInputTime(thisTime);
	if(hotelRechargeRec!=null){
		financeHotelAccounts.setHotelRechargeRecId(hotelRechargeRec.getHotelRechargeRecId());
	}
	financeHotelAccounts.setPaymentType(pType);
	financeHotelAccounts.setCutNotesId(hotelCutNotes.getCutNotesId());
	financeHotelAccounts.setHotelIndentId(hotelIndent.getHotelIndentId().toString());
	financeHotelAccounts.setHotelIndentCode(hotelIndent.getIndentCode());
	financeHotelAccounts.setMoney(hotelCutNotes.getCutPrice());
	financeHotelAccounts.setRemark(remark);
	
	financeHotelAccountsDao.save(financeHotelAccounts);
	
}


@Override
public List<AllStatementObject> findAllStatement(StatementObject fta){
	return financeHotelAccountsDao.findAllStatement(fta);
}

@Override
public void initFinance(){
	//查询2014-07-15后的所有充值
	
	DetachedCriteria dcHRR = DetachedCriteria.forClass(HotelRechargeRec.class);
	dcHRR.add(Property.forName("paymentType").eq("03"));
	dcHRR.add(Property.forName("inputTime").ge(CommonMethod.string2Time1("2014-07-15 00:00:00")));
	List<HotelRechargeRec> hrrList =  hotelRechargeRecDao.findByCriteria(dcHRR);
	System.out.println("初始化奖励款充值:"+hrrList.size());
	for(HotelRechargeRec hrr : hrrList){
		
		DetachedCriteria dcFTA = DetachedCriteria.forClass(FinanceHotelAccounts.class);
		dcFTA.add(Property.forName("hotelRechargeRecId").eq(hrr.getHotelRechargeRecId()));
		dcFTA.addOrder(Order.asc("paymentType"));//排序将网银支付记录排在前面
		List<FinanceHotelAccounts> ftaList =  financeHotelAccountsDao.findByCriteria(dcFTA);
		
		if(ftaList==null || ftaList.size()<1){//没有财务记录
			saveRewardOrCashTopUpInfo(hrr,"初始化酒店财务数据");
		}
		
	}
	
	
	
	//查询2014-07-15后的所有订单
	List<HotelIndent> hList = hotelIndentDao.findHotelIndentListForFinance();
	System.out.println(hList.size());
	
	//市场部订单
	List<HotelIndent> scbList = new ArrayList<HotelIndent>();
	//旅行社订单
	List<HotelIndent> lxsList = new ArrayList<HotelIndent>();
	//散客订单
	List<HotelIndent> skList = new ArrayList<HotelIndent>();
	
	for(HotelIndent hi : hList){
		String origin = hi.getOrigin();
		if("00".equals(origin)){
			String cus = hi.getCustomerType();
			
			if("02".equals(cus)){
				skList.add(hi);
			}else{
				lxsList.add(hi);
			}
		}else{
			scbList.add(hi);
		}
	}
	
	this.initSKFinance(skList);
	this.initLXSFinance(lxsList);
	this.initSCBinance(scbList);
	
}

/**
 * 初始化散客订单财务数据
 * @param skList
 */
private void initSKFinance(List<HotelIndent> skList) {
	System.out.println("初始化散客:"+skList.size());
	for(HotelIndent hi : skList){
		String status = hi.getStatus();
		
		if(!"01".equals(status) && !"06".equals(status)){//排除未支付和已取消的订单
			DetachedCriteria dc = DetachedCriteria.forClass(BaseTranRec.class);
			dc.add(Property.forName("hotelIndent.hotelIndentId").eq(hi.getHotelIndentId()));
			List<BaseTranRec> ftaList =  baseTranRecDao.findByCriteria(dc);
			
			BaseTranRec zfBtr = null;//支付记录
			BaseTranRec tdBtr = null;//退订记录
			
			for(BaseTranRec btr : ftaList){
				String pType = btr.getPaymentType();
				
				if("01".equals(pType)){
					zfBtr = btr;
				}else if("02".equals(pType)){
					tdBtr = btr;
				}
			}
			
			List<HotelIndentDetail> tidList = null;
			
			if("05".equals(status)){//已退订订单没有订单明细
				tidList = new ArrayList<HotelIndentDetail>();
				
				DetachedCriteria dcHD = DetachedCriteria.forClass(HotelDetailHis.class);
				dcHD.createAlias("hotelRoomType", "hotelRoomType");
				//dc.createAlias("hotelRoomType.hotel", "hotel");
				dcHD.add(Property.forName("hotelIndent.hotelIndentId").eq(hi.getHotelIndentId()));
				List<HotelDetailHis> tidHisList = hotelDetailHisDao.findByCriteria(dcHD);
				
				for(HotelDetailHis hdh : tidHisList){
					
					HotelIndentDetail hid = new HotelIndentDetail();
					
					hid.setHotelIndent(hi);
					hid.setHotelRoomType(hdh.getHotelRoomType());
					hid.setHotelIndentDetailCode(hdh.getHotelIndentDetailCode());
					hid.setBookingDate(hdh.getBookingDate());
					hid.setUnitPrice(hdh.getUnitPrice());
					hid.setAmount(hdh.getAmount());
					hid.setSubtotal(hdh.getSubtotal());
					hid.setRemark(hdh.getRemark());
					hid.setInputer(hi.getInputer());
					hid.setInputTime(hi.getInputTime());
					hid.setUpdater(hdh.getUpdater());
					hid.setUpdateTime(hdh.getUpdateTime());
					
					tidList.add(hid);
				}
				
			}else{
				DetachedCriteria dcHD = DetachedCriteria.forClass(HotelIndentDetail.class);
				dcHD.createAlias("hotelRoomType", "hotelRoomType");
				//dc.createAlias("hotelRoomType.hotel", "hotel");
				dcHD.add(Property.forName("hotelIndent.hotelIndentId").eq(hi.getHotelIndentId()));
				dcHD.addOrder(Order.asc("hotelIndentDetailId"));//按订单明细排序保证同意订单每次进来查询的顺序一致
				tidList =  hotelIndentDetailDao.findByCriteria(dcHD);
			}
			
			
			//添加支付财务记录
			this.savePayInfo(hi, zfBtr, null, Timestamp.valueOf(hi.getInputTime().toString()), tidList,"初始化酒店财务数据");
			
			if("03".equals(status) || "04".equals(status)){//已排房和已入住 添加已消费记录
				this.savePaidToExpend(hi.getHotelIndentId().toString(), Timestamp.valueOf(hi.getStartDate().toString()), "初始化酒店财务数据");
			}
			
			if("05".equals(status)){//已退订   添加退款和扣款记录
				this.saveReturnInfo(hi, tdBtr, null, Timestamp.valueOf(tdBtr.getPaymentTime().toString()), "初始化酒店财务数据");
				
				DetachedCriteria dcHCN = DetachedCriteria.forClass(HotelCutNotes.class);
				dcHCN.add(Property.forName("hotelIndent.hotelIndentId").eq(hi.getHotelIndentId()));
				List<HotelCutNotes> hcnList =  hotelCutNotesDao.findByCriteria(dcHCN);
				
				if(hcnList!=null && hcnList.size()>0){//有扣款则添加扣款记录
					this.saveCutInfo(hi, hcnList.get(0), null, Timestamp.valueOf(hcnList.get(0).getInputTime().toString()), "初始化酒店财务数据");
				}
			}
		}
	}
	
}

/**
 * 初始化旅行社订单财务数据
 * @param lxsList
 */
private void initLXSFinance(List<HotelIndent> lxsList) {
	System.out.println("初始化旅行社:"+lxsList.size());
	
	for(HotelIndent hi : lxsList){
		String status = hi.getStatus();
		
		if(!"01".equals(status) && !"06".equals(status)){//排除未支付和已取消的订单
			DetachedCriteria dc = DetachedCriteria.forClass(BaseTranRec.class);
			dc.add(Property.forName("hotelIndent.hotelIndentId").eq(hi.getHotelIndentId()));
			List<BaseTranRec> ftaList =  baseTranRecDao.findByCriteria(dc);
			
			BaseTranRec zfBtr = null;//支付记录
			BaseTranRec tdBtr = null;//退订记录
			
			for(BaseTranRec btr : ftaList){
				String pType = btr.getPaymentType();
				
				if("01".equals(pType)){
					zfBtr = btr;
				}else if("02".equals(pType)){
					tdBtr = btr;
				}
			}
			
			DetachedCriteria dcHRR = DetachedCriteria.forClass(HotelRechargeRec.class);
			dcHRR.add(Property.forName("hotelIndent.hotelIndentId").eq(hi.getHotelIndentId()));
			List<HotelRechargeRec> hrrList =  hotelRechargeRecDao.findByCriteria(dcHRR);
			
			HotelRechargeRec zfHrr = null;//支付记录
			HotelRechargeRec tdHrr = null;//退订记录
			
			for(HotelRechargeRec hrr : hrrList){
				String pType = hrr.getPaymentType();
				
				if("01".equals(pType)){
					zfHrr = hrr;
				}else if("02".equals(pType)){
					tdHrr = hrr;
				}
			}
			
			
			List<HotelIndentDetail> tidList = null;
			
			if("05".equals(status)){//已退订订单没有订单明细
				tidList = new ArrayList<HotelIndentDetail>();
				
				DetachedCriteria dcHD = DetachedCriteria.forClass(HotelDetailHis.class);
				dcHD.createAlias("hotelRoomType", "hotelRoomType");
				//dc.createAlias("hotelRoomType.hotel", "hotel");
				dcHD.add(Property.forName("hotelIndent.hotelIndentId").eq(hi.getHotelIndentId()));
				List<HotelDetailHis> tidHisList = hotelDetailHisDao.findByCriteria(dcHD);
				
				for(HotelDetailHis hdh : tidHisList){
					
					HotelIndentDetail hid = new HotelIndentDetail();
					
					hid.setHotelIndent(hi);
					hid.setHotelRoomType(hdh.getHotelRoomType());
					hid.setHotelIndentDetailCode(hdh.getHotelIndentDetailCode());
					hid.setBookingDate(hdh.getBookingDate());
					hid.setUnitPrice(hdh.getUnitPrice());
					hid.setAmount(hdh.getAmount());
					hid.setSubtotal(hdh.getSubtotal());
					hid.setRemark(hdh.getRemark());
					hid.setInputer(hi.getInputer());
					hid.setInputTime(hi.getInputTime());
					hid.setUpdater(hdh.getUpdater());
					hid.setUpdateTime(hdh.getUpdateTime());
					
					tidList.add(hid);
				}
				
			}else{
				DetachedCriteria dcHD = DetachedCriteria.forClass(HotelIndentDetail.class);
				dcHD.createAlias("hotelRoomType", "hotelRoomType");
				//dc.createAlias("hotelRoomType.hotel", "hotel");
				dcHD.add(Property.forName("hotelIndent.hotelIndentId").eq(hi.getHotelIndentId()));
				dcHD.addOrder(Order.asc("hotelIndentDetailId"));//按订单明细排序保证同意订单每次进来查询的顺序一致
				tidList =  hotelIndentDetailDao.findByCriteria(dcHD);
			}
			
			
			//添加支付财务记录
			this.savePayInfo(hi, zfBtr, zfHrr, Timestamp.valueOf(hi.getInputTime().toString()), tidList,"初始化酒店财务数据");
			
			if("03".equals(status) || "04".equals(status)){//已排房和已入住 添加已消费记录
				this.savePaidToExpend(hi.getHotelIndentId().toString(), Timestamp.valueOf(hi.getStartDate().toString()), "初始化酒店财务数据");
			}
			
			if("05".equals(status)){//已退订   添加退款和扣款记录
				this.saveReturnInfo(hi, tdBtr, tdHrr,tdBtr == null ? Timestamp.valueOf(tdHrr.getInputTime().toString()) 
									: Timestamp.valueOf(tdBtr.getPaymentTime().toString()), "初始化酒店财务数据");
				
				DetachedCriteria dcHCN = DetachedCriteria.forClass(HotelCutNotes.class);
				dcHCN.add(Property.forName("hotelIndent.hotelIndentId").eq(hi.getHotelIndentId()));
				List<HotelCutNotes> hcnList =  hotelCutNotesDao.findByCriteria(dcHCN);
				
				if(hcnList!=null && hcnList.size()>0){//有扣款则添加扣款记录
					this.saveCutInfo(hi, hcnList.get(0), tdHrr, Timestamp.valueOf(hcnList.get(0).getInputTime().toString()), "初始化酒店财务数据");
				}
			}
		}
	}
	
	
}

/**
 * 初始化市场部订单财务数据
 * @param scbList
 */
private void initSCBinance(List<HotelIndent> scbList) {
	System.out.println("初始化市场部:"+scbList.size());
	
	for(HotelIndent hi : scbList){
		String status = hi.getStatus();
		
		List<HotelIndentDetail> tidList = null;
		
		Timestamp returnTim = CommonMethod.getTimeStamp();
		
		if("05".equals(status)||"06".equals(status)){//已退订和已取消订单没有订单明细
			tidList = new ArrayList<HotelIndentDetail>();
			
			DetachedCriteria dcHD = DetachedCriteria.forClass(HotelDetailHis.class);
			dcHD.createAlias("hotelRoomType", "hotelRoomType");
			//dc.createAlias("hotelRoomType.hotel", "hotel");
			dcHD.add(Property.forName("hotelIndent.hotelIndentId").eq(hi.getHotelIndentId()));
			List<HotelDetailHis> tidHisList = hotelDetailHisDao.findByCriteria(dcHD);
			
			for(HotelDetailHis hdh : tidHisList){
				
				HotelIndentDetail hid = new HotelIndentDetail();
				
				hid.setHotelIndent(hi);
				hid.setHotelRoomType(hdh.getHotelRoomType());
				hid.setHotelIndentDetailCode(hdh.getHotelIndentDetailCode());
				hid.setBookingDate(hdh.getBookingDate());
				hid.setUnitPrice(hdh.getUnitPrice());
				hid.setAmount(hdh.getAmount());
				hid.setSubtotal(hdh.getSubtotal());
				hid.setRemark(hdh.getRemark());
				hid.setInputer(hi.getInputer());
				hid.setInputTime(hi.getInputTime());
				hid.setUpdater(hdh.getUpdater());
				hid.setUpdateTime(hdh.getUpdateTime());
				
				returnTim = Timestamp.valueOf(hdh.getInputTime().toString());
				
				tidList.add(hid);
			}
			
		}else{
			DetachedCriteria dcHD = DetachedCriteria.forClass(HotelIndentDetail.class);
			dcHD.createAlias("hotelRoomType", "hotelRoomType");
			//dc.createAlias("hotelRoomType.hotel", "hotel");
			dcHD.add(Property.forName("hotelIndent.hotelIndentId").eq(hi.getHotelIndentId()));
			dcHD.addOrder(Order.asc("hotelIndentDetailId"));//按订单明细排序保证同意订单每次进来查询的顺序一致
			tidList =  hotelIndentDetailDao.findByCriteria(dcHD);
		}
		
		//添加支付财务记录
		this.savePayInfo(hi, null, null, Timestamp.valueOf(hi.getInputTime().toString()), tidList, "初始化酒店财务数据");
		
		if("06".equals(status)){
			this.saveReturnInfo(hi, null, null, Timestamp.valueOf(hi.getUpdateTime().toString()), "初始化酒店财务数据");
		}else if(!"01".equals(status)){//排除未支付和已取消的订单
			
			//添加支付财务记录
			this.savePayToPaid(hi, Timestamp.valueOf(hi.getInputTime().toString()), "初始化酒店财务数据");
			
			if("03".equals(status) || "04".equals(status)){//已排房和已入住 添加已消费记录
				this.savePaidToExpend(hi.getHotelIndentId().toString(), Timestamp.valueOf(hi.getStartDate().toString()), "初始化酒店财务数据");
			}
			
			if("05".equals(status)){//已退订   添加退款和扣款记录
				this.saveReturnInfo(hi, null, null,returnTim,"初始化酒店财务数据");
				
				DetachedCriteria dcHCN = DetachedCriteria.forClass(HotelCutNotes.class);
				dcHCN.add(Property.forName("hotelIndent.hotelIndentId").eq(hi.getHotelIndentId()));
				List<HotelCutNotes> hcnList =  hotelCutNotesDao.findByCriteria(dcHCN);
				
				if(hcnList!=null && hcnList.size()>0){//有扣款则添加扣款记录
					this.saveCutInfo(hi, hcnList.get(0), null, Timestamp.valueOf(hcnList.get(0).getInputTime().toString()), "初始化酒店财务数据");
				}
			}
		}
	}
	
}

}