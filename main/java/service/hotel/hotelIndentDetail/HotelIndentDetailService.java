package service.hotel.hotelIndentDetail;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.xlem.dao.HotelIndent;
import com.xlem.dao.HotelIndentDetail;
import com.xlem.dao.HotelPriceLimitDetail;

import common.pay.PayParams;
import common.service.BaseService;

public interface HotelIndentDetailService extends BaseService<HotelIndentDetail> {
	
	/**
	 * 市场部订单检测是否有重复下单
	 * @param coll
	 * @param hotelIndentDetail
	 */
	List<Object> checkRepeating(HotelIndentDetail hotelIndentDetail);
	
	/**
	 * 保存订单及订单明细
	 * @param coll
	 * @param hotelIndentDetail
	 */
	PayParams save(Collection<HotelIndentDetail> coll,HotelIndentDetail hotelIndentDetail);
	
	/**
	 * 保存订单及订单明细(奖励款支付)
	 * @param coll
	 * @param hotelIndentDetail
	 */
	void saveByReward(Collection<HotelIndentDetail> coll,HotelIndentDetail hotelIndentDetail);

	void saveCheckOutIndent(Collection<HotelIndentDetail> coll,HotelIndentDetail hotelIndentDetail);
	
	void saveCheckOutIndent(Collection<HotelIndentDetail> coll,HotelIndentDetail hotelIndentDetail,String node);
	
	Map<String,Object> findCalcTotal(Collection<HotelIndentDetail> coll,HotelIndentDetail hotelIndentDetail);
	
	/**
	 * 修改酒店订单
	 * @param coll
	 * @param hotelIndentDetail
	 * @return
	 */
	PayParams update(Collection<HotelIndentDetail> coll,HotelIndentDetail hotelIndentDetail);
	
	/**
	 * 改签酒店订单
	 * @param coll
	 * @param hotelIndentDetail
	 * @return
	 */
	Map<String,Object> updateChange(Collection<HotelIndentDetail> coll,HotelIndentDetail hotelIndentDetail, String cutJson);
	
	/**
	 * 退款
	 * @param hotelIndent
	 * @param tkSums
	 * @return
	 */
	boolean saveRefund(HotelIndent hotelIndent, List<HotelIndentDetail> hotelIndentDetails, double tkSums, double charge, double unitRatio,String cutRulesType);
	
	/**
	 * 计算退款金额(扣除房损)
	 * @param hotelIndent
	 * @param cutRulesType  00退房规则，11改签规则
	 * @return
	 */
	Map<String,Object> findCountRefundMoney(HotelIndent hotelIndent,String cutRulesType);
	/**
	 * 计算退款金额和扣除房损金额
	 * @param hotelIndent
	 * @param cutRulesType  00退房规则，11改签规则
	 * @return
	 */
	Map<String,Object> findCountRefundMoney(Collection<HotelIndentDetail> coll,String cutRulesType);
	/**
	 * 查询退款金额和扣款金额
	 * @param hotelIndentDetail
	 * @return
	 */
	HashMap<String,Object> findRefundAndChargeAmount(HotelIndentDetail hotelIndentDetail);
	
	/**
	 * 判断当前房型是否为特价房,且当前时间段能否预订
	 * @param hotelPriceLimitDetail
	 * @return
	 */
	boolean findBookedSpecialPrice(HotelPriceLimitDetail hotelPriceLimitDetail);
}