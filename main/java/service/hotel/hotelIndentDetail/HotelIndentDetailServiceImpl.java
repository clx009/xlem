package service.hotel.hotelIndentDetail;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import service.comm.baseRechargeRec.BaseRechargeRecService;
import service.comm.baseRewardAccount.BaseRewardAccountService;
import service.comm.baseTranRec.BaseTranRecService;
import service.comm.commActivity.CommActivityService;
import service.hotel.financeHotelAccounts.FinanceHotelAccountsService;
import service.ticket.ticketIndentDetail.TicketIndentDetailService;
import ws.ReturnInfo;
import ws.hotel.client.HotelIndentClient;
import ws.hotel.comm.HotelCode;

import com.thoughtworks.xstream.XStream;
import com.xlem.dao.BaseRewardAccount;
import com.xlem.dao.BaseTranRec;
import com.xlem.dao.CommActivity;
import com.xlem.dao.Hotel;
import com.xlem.dao.HotelActivity;
import com.xlem.dao.HotelActivityDetail;
import com.xlem.dao.HotelActivityLimit;
import com.xlem.dao.HotelCutNotes;
import com.xlem.dao.HotelCutRules;
import com.xlem.dao.HotelDetailHis;
import com.xlem.dao.HotelIndent;
import com.xlem.dao.HotelIndentDetail;
import com.xlem.dao.HotelPriceLimitDetail;
import com.xlem.dao.HotelRechargeRec;
import com.xlem.dao.HotelRoomType;
import com.xlem.dao.SysUser;
import com.xlem.dao.Travservice;
import com.xlem.dao.TravserviceHome;

import common.BusinessException;
import common.CommonMethod;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.jsonProcessor.CommonJsonConfig;
import common.pay.PayFactoryService;
import common.pay.PayParams;
import common.pay.RefundParams;
import common.service.BaseServiceImpl;

import dao.comm.baseMessageRec.BaseMessageRecDao;
import dao.comm.baseRewardAccount.BaseRewardAccountDao;
import dao.comm.baseTranRec.BaseTranRecDao;
import dao.comm.reg.baseTravelAgency.BaseTravelAgencyDao;
import dao.hotel.hotel.HotelDao;
import dao.hotel.hotelActivity.HotelActivityDao;
import dao.hotel.hotelActivityDetail.HotelActivityDetailDao;
import dao.hotel.hotelActivityLimit.HotelActivityLimitDao;
import dao.hotel.hotelCutNotes.HotelCutNotesDao;
import dao.hotel.hotelCutRules.HotelCutRulesDao;
import dao.hotel.hotelDetailHis.HotelDetailHisDao;
import dao.hotel.hotelIndent.HotelIndentDao;
import dao.hotel.hotelIndentDetail.HotelIndentDetailDao;
import dao.hotel.hotelPriceLimitDetail.HotelPriceLimitDetailDao;
import dao.hotel.hotelRechargeRec.HotelRechargeRecDao;
import dao.hotel.roomType.RoomTypeDao;
import dao.sys.sysParam.SysParamDao;
import dao.sys.sysUser.SysUserDao;

@Service("hotelIndentDetailService")
public class HotelIndentDetailServiceImpl extends
		BaseServiceImpl<HotelIndentDetail> implements HotelIndentDetailService {
	@Autowired
	private HotelIndentDetailDao hotelIndentDetailDao;
	@Autowired
	private HotelIndentDao hotelIndentDao;
	@Autowired
	private HotelPriceLimitDetailDao hotelPriceLimitDetailDao;
	@Autowired
	private HotelActivityDao hotelActivityDao;
	@Autowired
	private HotelActivityDetailDao hotelActivityDetailDao;
	@Autowired
	private HotelActivityLimitDao hotelActivityLimitDao;
	@Autowired
	private TicketIndentDetailService ticketIndentDetailService;
	@Autowired
	private SysUserDao sysUserDao;
	@Autowired
	private SysParamDao sysParamDao;
	@Autowired
	private BaseTravelAgencyDao baseTravelAgencyDao;
	@Autowired
	private BaseTranRecService baseTranRecService;
	@Autowired
	private BaseRechargeRecService baseRechargeRecService;
	@Autowired
	private BaseTranRecDao baseTranRecDao;
	@Autowired
	private HotelDetailHisDao hotelDetailHisDao;
	@Autowired
	private HotelCutNotesDao hotelCutNotesDao;
	@Autowired
	private HotelCutRulesDao hotelCutRulesDao;
	@Autowired
	private RoomTypeDao roomTypeDao;
	@Autowired
	private HotelDao hotelDao;
	@Autowired
	private BaseMessageRecDao baseMessageRecDao;
	@Autowired
	private BaseRewardAccountDao baseRewardAccountDao;
	@Autowired
	private HotelRechargeRecDao hotelRechargeRecDao;
	@Autowired
	private CommActivityService commActivityService;
	@Autowired
	private PayFactoryService payFactoryService;
	@Autowired
	private BaseRewardAccountService baseRewardAccountService;
	@Autowired
	private FinanceHotelAccountsService financeHotelAccountsService;
	
	
	private static Logger logger = Logger.getLogger("bookingService");
	
	@Override
	@Resource(name = "hotelIndentDetailDao")
	protected void initBaseDAO(BaseDao<HotelIndentDetail> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<HotelIndentDetail> findPageByCriteria(
			PaginationSupport<HotelIndentDetail> ps, HotelIndentDetail t) {
		DetachedCriteria dc = DetachedCriteria
				.forClass(HotelIndentDetail.class);
		return hotelIndentDetailDao.findPageByCriteria(ps,
				Order.desc("hotelIndentDetailId"), dc);
	}

	@Override
	public void save(HotelIndentDetail t) {
		t.setHotelIndentDetailId(getNextKey(
				"Hotel_Indent_Detail".toUpperCase(), 1));
		hotelIndentDetailDao.save(t);
	}

	@Override
	public JsonPager<HotelIndentDetail> findJsonPageByCriteria(
			JsonPager<HotelIndentDetail> jp, HotelIndentDetail t) {
		DetachedCriteria dc = DetachedCriteria.forClass(HotelIndentDetail.class);
		dc.createAlias("hotelRoomType", "hotelRoomType");
		dc.createAlias("hotelRoomType.hotel", "hotel");
		dc.createAlias("hotelIndent", "hotelIndent");
		if(t.getHotelIndent()!=null && t.getHotelIndent().getHotelIndentId()!=null){
			dc.add(Property.forName("hotelIndent.hotelIndentId").eq(t.getHotelIndent().getHotelIndentId()));
		}
		dc.addOrder(Order.asc("bookingDate"));
		JsonPager<HotelIndentDetail> jpH = hotelIndentDetailDao.findJsonPageByCriteria(jp, dc);
		for(HotelIndentDetail hid : jpH.getRoot()){
			if(hid.getAmountret()==null){
				hid.setAmountret(hid.getAmount());
			}
		}
		return jpH;
	}

	@Override
	public List<Object> checkRepeating(HotelIndentDetail t) {
		
		return hotelIndentDao.findRepeating(t.getHotelIndent());
		
	}
	
	@Override
	public PayParams save(Collection<HotelIndentDetail> coll, HotelIndentDetail t) {
		Timestamp ctimestap = CommonMethod.getTimeStamp();
		HotelIndent hotelIndent = t.getHotelIndent();

		//取得客户类型
		String customerType = "01";
		SysUser sysUser = getSysUser();
		if(hotelIndent.getCustomerType()!=null && !hotelIndent.getCustomerType().equals("")){//如无客户类型设置类型
			customerType = hotelIndent.getCustomerType();
		}
		else{
			if(sysUser.getUserType().equals("03")){
				customerType = "01";
			}
			else{
				customerType = "02";
			}
		}
		//订单信息
		if(hotelIndent.getOrigin()==null || "".equals(hotelIndent.getOrigin())){//如果不是市场部订单，即记为00网络订单
			hotelIndent.setOrigin("00");
		}
		hotelIndent.setHotelIndentId(getNextKey("Hotel_Indent", 1));
		hotelIndent.setInputer(getCurrentUserId());
		hotelIndent.setInputTime(ctimestap);
		hotelIndent.setCheckCode(findCheckCode(hotelIndent));//加验证码
		hotelIndent.setStatus("01");
		hotelIndent.setSynchroState("00");
		hotelIndent.setIndentCode(getNextHotelIndentCode(CommonMethod.getCurrentYMD()+"000001",CommonMethod.getCurrentYMD()));
		hotelIndent.setCustomerType(customerType);
		if(hotelIndent.getCustomerName()==null || hotelIndent.getCustomerName().equals("")){//客户名称
			hotelIndent.setCustomerName(sysUser.getUserName());
		}
		//订单明细信息
//		for (HotelIndentDetail hotelIndentDetail : coll) {
//			if(hotelIndentDetail.getAmount()!=null && hotelIndentDetail.getAmount()!=0){
//				//取得时间价格清单
//				DetachedCriteria dc = DetachedCriteria.forClass(HotelPriceLimitDetail.class);
//				dc.createAlias("hotelRoomType", "hotelRoomType");
//				dc.add(Property.forName("hotelRoomType.roomTypeId").eq(hotelIndentDetail.getHotelRoomType().getRoomTypeId()));
//				dc.add(Property.forName("useDate").ge(hotelIndent.getStartDate()));
//				dc.add(Property.forName("useDate").lt(hotelIndent.getEndDate()));
//				dc.addOrder(Order.asc("useDate"));
//				List<HotelPriceLimitDetail> hotelPriceLimitDetails = hotelPriceLimitDetailDao.findByCriteria(dc);
//				long startPk = getNextKey("Hotel_Indent_Detail", hotelPriceLimitDetails.size());
//				
//				/**2014-02-13 市场部从页面修改的价格 
//				 * 从页面传回时在hotelIndentDetail.remark中
//				 * 格式为   2014-02-13:900.00,2014-02-14:920.00,
//				 */
//				String priceStr = hotelIndentDetail.getRemark();
//				Map<Timestamp,Double>  priceMap = new HashMap<Timestamp,Double>();
//				if(!"00".equals(hotelIndent.getOrigin())){//如是市场部订单,获取修改后单价
//					String[] prStrs = priceStr.split(",");
//					
//					for(String pr : prStrs){
//						if(!"".equals(pr)){
//							String[] pStrs = pr.split(":");
//							priceMap.put(CommonMethod.string2Time1(pStrs[0]), Double.valueOf(pStrs[1]));
//						}
//					}
//				}
//				
//				for (HotelPriceLimitDetail hotelPriceLimitDetail : hotelPriceLimitDetails) {
//					HotelIndentDetail tempDetail = new HotelIndentDetail();
//					tempDetail.setHotelIndentDetailId(startPk);
//					tempDetail.setInputer(getCurrentUserId());
//					tempDetail.setInputTime(ctimestap);
//					tempDetail.setHotelIndent(hotelIndent);
//					tempDetail.setHotelRoomType(hotelIndentDetail.getHotelRoomType());
//					tempDetail.setAmount(hotelIndentDetail.getAmount());
//					//通过价格清单设置明细信息
//					hotelPriceLimitDetail.setCustomerType(customerType);
//					tempDetail.setBookingDate(hotelPriceLimitDetail.getUseDate());
//					if(!"00".equals(hotelIndent.getOrigin())){//如是市场部订单,获取修改后单价
//						double unPrice = priceMap.get(hotelPriceLimitDetail.getUseDate());
//						tempDetail.setUnitPrice(unPrice);
//						tempDetail.setSubtotal(hotelIndentDetail.getAmount()*unPrice);
//					}
//					else{
//						tempDetail.setUnitPrice(hotelPriceLimitDetail.getPrice());
//						tempDetail.setSubtotal(hotelIndentDetail.getAmount()*hotelPriceLimitDetail.getPrice());
//					}
//					
//					if("00".equals(hotelIndent.getOrigin())){//网络订单
//						//查询并计算活动
//						CommActivity commActivity = new CommActivity();
//						commActivity.setActivityTime(hotelIndent.getStartDate());
//						commActivity.setActivityEndTime(hotelIndent.getEndDate());
//						commActivity.setRemark(hotelIndent.getHotel().getHotelId().toString());
//						
//						CommActivity commA = commActivityService.findEnabledActBuyGet(commActivity,"1");
//						if(commA!=null){//有活动
//							
//							int aL = commA.getActivityLimit();//买X
//							int aA = commA.getActivityAmount();//送X
//							
//							//应送数量
//							int fA = (tempDetail.getAmount() / aL) * aA;
//							
//							tempDetail.setAmount(tempDetail.getAmount()+fA);
//							
//							tempDetail.setUnitPrice(tempDetail.getSubtotal()/tempDetail.getAmount());
//							hotelIndentDetail.setAmount(tempDetail.getAmount());
//						}
//					}
//					
//					list.add(tempDetail);
//					//计算总价
//					totalPrice = totalPrice + tempDetail.getSubtotal();
//					startPk = startPk+1;
//					//更新剩余数量
//					if(hotelPriceLimitDetail.getRemainAmount() >= hotelIndentDetail.getAmount()){
//						hotelPriceLimitDetail.setRemainAmount(hotelPriceLimitDetail.getRemainAmount()-hotelIndentDetail.getAmount());
//						hotelPriceLimitDetailDao.update(hotelPriceLimitDetail);
//					}
//					else{
//						throw new BusinessException("房间已订完,不能再预订!");
//					}
//				}
//				//计算订单总数量			
//				totalAmount = totalAmount + hotelIndentDetail.getAmount();
//			}
//		}
		
		//订单明细信息
		Map<String,Object> tempMap = null;
		
		if(hotelIndent.getHotelActivityId()!=null){//活动房
			tempMap = this.creatDetailListForAct(coll, hotelIndent, ctimestap, customerType);
		}else{//普通房
			tempMap = this.creatDetailList(coll, hotelIndent, ctimestap, customerType);
		}
		
		
		int totalAmount = tempMap.get("totalAmount") == null ? 0 : Integer.valueOf(tempMap.get("totalAmount").toString());
		
		if(totalAmount<1){
			throw new BusinessException("您还未填写订房数量！","");
		}
		
		double totalPrice = tempMap.get("totalPrice") == null ? 0d : Double.valueOf(tempMap.get("totalPrice").toString());
		List<HotelIndentDetail> list = tempMap.get("list") == null ? new ArrayList<HotelIndentDetail>() : (List<HotelIndentDetail>)tempMap.get("list");
		
		//保存订单
		if(totalPrice==0){//0元特价
			hotelIndent.setStatus("02");
			hotelIndent.setPayType("9");
		}
		hotelIndent.setTotalAmount(totalAmount);
		hotelIndent.setTotalPrice(totalPrice);
		hotelIndentDao.save(hotelIndent);
		//保存明细
		for (HotelIndentDetail hotelIndentDetail : list) {
			hotelIndentDetailDao.save(hotelIndentDetail);
		}
		//生成易宝交易信息
		PayParams payParams = new PayParams();
		if(totalPrice==0){
			Hotel hotel = hotelDao.findById(hotelIndent.getHotel().getHotelId());
			
			//添加财务记录
			financeHotelAccountsService.savePayInfo(hotelIndent, null, null);
			
			String content = "您已成功预订"+hotel.getHotelName()+"房间,订单号为："+hotelIndent.getIndentCode()+",请携带入住人身份证办理入住手续!";
			CommonMethod.sendSms(hotelIndent.getPhoneNumber(), content,sysParamDao.findSysParam("USE_NEW_PLATFROM"));
		}
		else if("00".equals(hotelIndent.getOrigin())){//网络订单需要去易宝支付
//			String keyValue = CommonMethod.formatString(findSysParam("KEY_VALUE"));
//			payParams.setP0_Cmd("Buy");
//			payParams.setP1_MerId(findSysParam("P1_MERID"));
//			payParams.setP2_Order(hotelIndent.getIndentCode());
//			if(hotelIndent.getReceivedMoney()!=null && !hotelIndent.getReceivedMoney().isNaN() && hotelIndent.getReceivedMoney()>0){
//				payParams.setP3_Amt(CommonMethod.format2db(hotelIndent.getTotalPrice()-hotelIndent.getReceivedMoney()));//减去已付款金额
//			}
//			else{
//				payParams.setP3_Amt(CommonMethod.format2db(hotelIndent.getTotalPrice()));
//			}
//			payParams.setP4_Cur("CNY");
//			payParams.setP5_Pid("");
//			payParams.setP6_Pcat("酒店");
//			payParams.setP7_Pdesc("");
//			payParams.setP8_Url(Configuration.getInstance().getValue("resposeUrlForHotel"));
//			payParams.setP9_SAF("0");
//			payParams.setPa_MP("");
//			payParams.setPd_FrpId("");
//			payParams.setPm_Period("20");//20分钟不支付即过期
//			payParams.setPn_Unit("minute");//20分钟不支付即过期
//			payParams.setPr_NeedResponse("1");
//			payParams.setNodeAuthorizationURL(CommonMethod.formatString(findSysParam("YEEPAY_COMMON_REQURL")));
//			payParams.setHmac(PaymentForOnlineService
//					.getReqMd5HmacForOnlinePayment(payParams.getP0_Cmd(),
//							payParams.getP1_MerId(), payParams.getP2_Order(),
//							payParams.getP3_Amt(), payParams.getP4_Cur(),
//							CommonMethod.getNewString(payParams.getP5_Pid(),"GBK"), CommonMethod.getNewString(payParams.getP6_Pcat(),"GBK"),
//							CommonMethod.getNewString(payParams.getP7_Pdesc(),"GBK"), payParams.getP8_Url(),
//							payParams.getP9_SAF(), CommonMethod.getNewString(payParams.getPa_MP(),"GBK"),
//							payParams.getPd_FrpId(),payParams.getPm_Period(),payParams.getPn_Unit(),
//							payParams.getPr_NeedResponse(), keyValue));
			double moneyNeedPay = 0;
			if(hotelIndent.getReceivedMoney()!=null && !hotelIndent.getReceivedMoney().isNaN() && hotelIndent.getReceivedMoney()>0){
				moneyNeedPay = hotelIndent.getTotalPrice()-hotelIndent.getReceivedMoney();//减去已付款金额
			}
			else{
				moneyNeedPay = hotelIndent.getTotalPrice();
			}
			
			//冲退交易记录
			baseTranRecService.saveReturnBackInfo(hotelIndent);
			//生成交易记录
			BaseTranRec baseTranRec = new BaseTranRec();
			baseTranRec.setHotelIndent(hotelIndent);
			baseTranRec.setMoney(moneyNeedPay);
			baseTranRec.setPaymentType("01");//支付类型支付
			baseTranRec.setPayType("01");//交易类型,通过易宝交易
			baseTranRec.setPaymentAccount("02");
			baseTranRecService.save(baseTranRec);
			
			payFactoryService.init();
			payParams = payFactoryService.initPayParams(hotelIndent,moneyNeedPay);
			payParams.setPay_test(sysParamDao.findSysParam("PAY_TEST"));
		}
		else{//市场部订单直接发送成功提示
			Hotel hotel = hotelDao.findById(hotelIndent.getHotel().getHotelId());
			
			//添加财务记录
			financeHotelAccountsService.savePayInfo(hotelIndent, null, null);
			
			String content = "您已成功预订"+hotel.getHotelName()+"房间,订单号为"+hotelIndent.getIndentCode()+"，预留时间至:"+CommonMethod.timeToString2(Timestamp.valueOf(hotelIndent.getPayTime().toString()))+",请及时汇 款或转 账";
			CommonMethod.sendSms(hotelIndent.getPhoneNumber(), content,sysParamDao.findSysParam("USE_NEW_PLATFROM"));
		}
		return payParams;
	}
	
	@Override
	public void saveByReward(Collection<HotelIndentDetail> coll, HotelIndentDetail t) {
		Timestamp ctimestap = CommonMethod.getTimeStamp();
		HotelIndent hotelIndent = t.getHotelIndent();
		if(hotelIndent.getInputTime()!=null){//没有订单时间表示是正在做订单
			//检查订单是否超时时间
			Long mi = Long.parseLong(sysParamDao.findSysParam("HOTEL_INDENT_AUTO_CANCEL_TIME"));//酒店订单自动取消时间（分钟）
			//实际时间比配置时间减少2分钟
			mi = mi - 2;
			//计算剩余时间
			//下订单到现在已过多长时间
			Long myhour = CommonMethod.diffMin( Timestamp.valueOf(hotelIndent.getInputTime().toString()), CommonMethod.getTimeStamp());
			//还剩余多少时间
			mi = mi - myhour;
			
			if(mi<0L){
				throw new BusinessException("订单超过支付时间未支付已被系统取消！");
			}
		}
		
		
		int totalAmount = 0;
		double totalPrice = 0d;
		//取得客户类型
		String customerType = "01";
		SysUser sysUser = getSysUser();
		if(hotelIndent.getCustomerType()!=null && !hotelIndent.getCustomerType().equals("")){//如无客户类型设置类型
			customerType = hotelIndent.getCustomerType();
		}
		else{
			if(sysUser.getUserType().equals("03")){
				customerType = "01";
			}
			else{
				customerType = "02";
			}
		}
		//订单信息
		if(hotelIndent.getOrigin()==null || "".equals(hotelIndent.getOrigin())){//如果不是市场部订单，即记为00网络订单
			hotelIndent.setOrigin("00");
		}
		hotelIndent.setHotelIndentId(getNextKey("Hotel_Indent", 1));
		hotelIndent.setInputer(getCurrentUserId());
		hotelIndent.setInputTime(ctimestap);
		hotelIndent.setCheckCode(findCheckCode(hotelIndent));//加验证码
		hotelIndent.setStatus("01");
		hotelIndent.setSynchroState("00");
		hotelIndent.setIndentCode(getNextHotelIndentCode(CommonMethod.getCurrentYMD()+"000001",CommonMethod.getCurrentYMD()));
		hotelIndent.setCustomerType(customerType);
		if(hotelIndent.getCustomerName()==null || hotelIndent.getCustomerName().equals("")){//客户名称
			hotelIndent.setCustomerName(sysUser.getUserName());
		}
		//订单明细信息
		List<HotelIndentDetail> list = new ArrayList<HotelIndentDetail>();
		for (HotelIndentDetail hotelIndentDetail : coll) {
			if(hotelIndentDetail.getAmount()!=null && hotelIndentDetail.getAmount()!=0){
				//取得时间价格清单
				DetachedCriteria dc = DetachedCriteria.forClass(HotelPriceLimitDetail.class);
				dc.createAlias("hotelRoomType", "hotelRoomType");
				dc.add(Property.forName("hotelRoomType.roomTypeId").eq(hotelIndentDetail.getHotelRoomType().getRoomTypeId()));
				dc.add(Property.forName("useDate").ge(hotelIndent.getStartDate()));
				dc.add(Property.forName("useDate").lt(hotelIndent.getEndDate()));
				dc.addOrder(Order.asc("useDate"));
				List<HotelPriceLimitDetail> hotelPriceLimitDetails = hotelPriceLimitDetailDao.findByCriteria(dc);
				long startPk = getNextKey("Hotel_Indent_Detail", hotelPriceLimitDetails.size());
				for (HotelPriceLimitDetail hotelPriceLimitDetail : hotelPriceLimitDetails) {
					HotelIndentDetail tempDetail = new HotelIndentDetail();
					tempDetail.setHotelIndentDetailId(startPk);
					tempDetail.setInputer(getCurrentUserId());
					tempDetail.setInputTime(ctimestap);
					tempDetail.setHotelIndent(hotelIndent);
					tempDetail.setHotelRoomType(hotelIndentDetail.getHotelRoomType());
					tempDetail.setAmount(hotelIndentDetail.getAmount());
					//通过价格清单设置明细信息
					hotelPriceLimitDetail.setCustomerType(customerType);
					tempDetail.setBookingDate(hotelPriceLimitDetail.getUseDate());
					if(!"00".equals(hotelIndent.getOrigin())){//如是市场部订单,获取修改后单价
						tempDetail.setUnitPrice(hotelIndentDetail.getUnitPrice());
						tempDetail.setSubtotal(hotelIndentDetail.getAmount()*hotelIndentDetail.getUnitPrice());
					}
					else{
						tempDetail.setUnitPrice(hotelPriceLimitDetail.getPrice());
						tempDetail.setSubtotal(hotelIndentDetail.getAmount()*hotelPriceLimitDetail.getPrice());
					}
					
					if("00".equals(hotelIndent.getOrigin())){//网络订单
						//查询并计算活动
						CommActivity commActivity = new CommActivity();
						commActivity.setActivityTime(hotelIndent.getStartDate());
						commActivity.setActivityEndTime(hotelIndent.getEndDate());
						commActivity.setRemark(hotelIndent.getHotel().getHotelId().toString());
						
						CommActivity commA = commActivityService.findEnabledActBuyGet(commActivity,"1");
						if(commA!=null){//有活动
							
							int aL = commA.getActivityLimit();//买X
							int aA = commA.getActivityAmount();//送X
							
							//应送数量
							int fA = (tempDetail.getAmount() / aL) * aA;
							
							tempDetail.setAmount(tempDetail.getAmount()+fA);
							
							tempDetail.setUnitPrice(tempDetail.getSubtotal()/tempDetail.getAmount());
						}
						hotelIndentDetail.setAmount(tempDetail.getAmount());
					}
					
					
					list.add(tempDetail);
					//计算总价
					totalPrice = totalPrice + tempDetail.getSubtotal();
					startPk = startPk+1;
					//更新剩余数量
					if(hotelPriceLimitDetail.getRemainAmount() >= hotelIndentDetail.getAmount()){
						hotelPriceLimitDetail.setRemainAmount(hotelPriceLimitDetail.getRemainAmount()-hotelIndentDetail.getAmount());
						hotelPriceLimitDetailDao.update(hotelPriceLimitDetail);
					}
					else{
						throw new BusinessException("房间已订完,不能再预订!");
					}
				}
				//计算订单总数量			
				totalAmount = totalAmount + hotelIndentDetail.getAmount();
			}
		}
		
		if(totalAmount<1){
			throw new BusinessException("您还未填写订房数量！","");
		}
		
		//保存订单
		if(totalPrice==0){//0元特价
			hotelIndent.setPayType("9");
		}else{
			hotelIndent.setPayType("01");
		}
		hotelIndent.setStatus("02");
		hotelIndent.setTotalAmount(totalAmount);
		hotelIndent.setTotalPrice(totalPrice);
		hotelIndentDao.save(hotelIndent);
		//保存明细
		for (HotelIndentDetail hotelIndentDetail : list) {
			hotelIndentDetailDao.save(hotelIndentDetail);
		}
		//扣除奖励款
		BaseRewardAccount baseRewardAccount =baseRewardAccountDao.findById(t.getRewardAccountId());
		baseRewardAccount.setRewardBalance(baseRewardAccount.getRewardBalance()-totalPrice);
		baseRewardAccountDao.update(baseRewardAccount);
		//奖励款记录
		Travservice baseTravelAgency = baseTravelAgencyDao.findById(sysUser.getBaseTravelAgency().getTravelAgencyId());
		HotelRechargeRec hotelRechargeRec = new HotelRechargeRec();
		hotelRechargeRec.setHotelIndent(hotelIndent);
		hotelRechargeRec.setHotelRechargeRecId(getNextKey("HOTEL_RECHARGE_REC", 1));
		hotelRechargeRec.setBaseTravelAgency(baseTravelAgency);
		hotelRechargeRec.setBaseRewardAccount(baseRewardAccount);
		hotelRechargeRec.setSourceDescription("订房付款");
		hotelRechargeRec.setRewardMoney(0-hotelIndent.getTotalPrice());
		hotelRechargeRec.setRewardBalance(baseRewardAccount.getRewardBalance());
		hotelRechargeRec.setPaymentType("01");
		hotelRechargeRec.setInputer(getCurrentUserId());
		hotelRechargeRec.setInputTime(CommonMethod.getTimeStamp());
		hotelRechargeRecDao.save(hotelRechargeRec);
		
		//添加财务记录
		financeHotelAccountsService.savePayInfo(hotelIndent, null, hotelRechargeRec);
		
		Hotel hotel = hotelDao.findById(hotelIndent.getHotel().getHotelId());
		String content = "您已成功预订"+hotel.getHotelName()+"房间,订单号为"+hotelIndent.getIndentCode()+"！";
		CommonMethod.sendSms(hotelIndent.getPhoneNumber(), content,sysParamDao.findSysParam("USE_NEW_PLATFROM"));
	}
	
	/**
	 * 保存订单时组合订单明细
	 * @return
	 */
	private Map<String,Object> creatDetailList(Collection<HotelIndentDetail> coll,HotelIndent hotelIndent,Timestamp ctimestap,String customerType) {
		
		Map<String,Object> returnMap = new HashMap<String,Object>();
		
		int totalAmount = 0;
		double totalPrice = 0d;
		
		//订单明细信息
		List<HotelIndentDetail> list = new ArrayList<HotelIndentDetail>();
		for (HotelIndentDetail hotelIndentDetail : coll) {
			if(hotelIndentDetail.getAmount()!=null && hotelIndentDetail.getAmount()!=0){
				//取得时间价格清单
				DetachedCriteria dc = DetachedCriteria.forClass(HotelPriceLimitDetail.class);
				dc.createAlias("hotelRoomType", "hotelRoomType");
				dc.add(Property.forName("hotelRoomType.roomTypeId").eq(hotelIndentDetail.getHotelRoomType().getRoomTypeId()));
				dc.add(Property.forName("useDate").ge(hotelIndent.getStartDate()));
				dc.add(Property.forName("useDate").lt(hotelIndent.getEndDate()));
				dc.addOrder(Order.asc("useDate"));
				List<HotelPriceLimitDetail> hotelPriceLimitDetails = hotelPriceLimitDetailDao.findByCriteria(dc);
				long startPk = getNextKey("Hotel_Indent_Detail", hotelPriceLimitDetails.size());
				
				/**2014-02-13 市场部从页面修改的价格 
				 * 从页面传回时在hotelIndentDetail.remark中
				 * 格式为   2014-02-13:900.00,2014-02-14:920.00,
				 */
				String priceStr = hotelIndentDetail.getRemark();
				Map<Timestamp,Double>  priceMap = new HashMap<Timestamp,Double>();
				if(!"00".equals(hotelIndent.getOrigin())){//如是市场部订单,获取修改后单价
					String[] prStrs = priceStr.split(",");
					
					for(String pr : prStrs){
						if(!"".equals(pr)){
							String[] pStrs = pr.split(":");
							priceMap.put(CommonMethod.string2Time1(pStrs[0]), Double.valueOf(pStrs[1]));
						}
					}
				}
				
				for (HotelPriceLimitDetail hotelPriceLimitDetail : hotelPriceLimitDetails) {
					
					hotelPriceLimitDetail.setStartDate(Timestamp.valueOf(hotelPriceLimitDetail.getUseDate().toString()));
					hotelPriceLimitDetail.setEndDate(Timestamp.valueOf(hotelPriceLimitDetail.getUseDate().toString()));
					
					List<HotelPriceLimitDetail> hpList =  hotelIndentDetailDao.findUsedRoomEveryday(hotelPriceLimitDetail);
					
					for(HotelPriceLimitDetail hpTemp : hpList){
						if(hpTemp.getUseDate().equals(hotelPriceLimitDetail.getUseDate())){
							hotelPriceLimitDetail.setRemainAmount(hotelPriceLimitDetail.getAmount()-hpTemp.getBookingAmount());
						}
					}
					
					HotelIndentDetail tempDetail = new HotelIndentDetail();
					tempDetail.setHotelIndentDetailId(startPk);
					tempDetail.setInputer(getCurrentUserId());
					tempDetail.setInputTime(ctimestap);
					tempDetail.setHotelIndent(hotelIndent);
					tempDetail.setHotelRoomType(hotelIndentDetail.getHotelRoomType());
					tempDetail.setAmount(hotelIndentDetail.getAmount());
					//通过价格清单设置明细信息
					hotelPriceLimitDetail.setCustomerType(customerType);
					tempDetail.setBookingDate(hotelPriceLimitDetail.getUseDate());
					if(!"00".equals(hotelIndent.getOrigin())){//如是市场部订单,获取修改后单价
						double unPrice = priceMap.get(hotelPriceLimitDetail.getUseDate());
						tempDetail.setUnitPrice(unPrice);
						tempDetail.setSubtotal(hotelIndentDetail.getAmount()*unPrice);
					}
					else{
						tempDetail.setUnitPrice(hotelPriceLimitDetail.getPrice());
						tempDetail.setSubtotal(hotelIndentDetail.getAmount()*hotelPriceLimitDetail.getPrice());
					}
					
					if("00".equals(hotelIndent.getOrigin())){//网络订单
						//查询并计算活动
						CommActivity commActivity = new CommActivity();
						commActivity.setActivityTime(hotelIndent.getStartDate());
						commActivity.setActivityEndTime(hotelIndent.getEndDate());
						commActivity.setRemark(hotelIndent.getHotel().getHotelId().toString());
						
						CommActivity commA = commActivityService.findEnabledActBuyGet(commActivity,"1");
						if(commA!=null){//有活动
							
							int aL = commA.getActivityLimit();//买X
							int aA = commA.getActivityAmount();//送X
							
							//应送数量
							int fA = (tempDetail.getAmount() / aL) * aA;
							
							tempDetail.setAmount(tempDetail.getAmount()+fA);
							
							tempDetail.setUnitPrice(tempDetail.getSubtotal()/tempDetail.getAmount());
							hotelIndentDetail.setAmount(tempDetail.getAmount());
						}
					}
					
					list.add(tempDetail);
					//计算总价
					totalPrice = totalPrice + tempDetail.getSubtotal();
					startPk = startPk+1;
					//更新剩余数量
					if(hotelPriceLimitDetail.getRemainAmount() >= hotelIndentDetail.getAmount()){
						hotelPriceLimitDetail.setRemainAmount(hotelPriceLimitDetail.getRemainAmount()-hotelIndentDetail.getAmount());
						hotelPriceLimitDetailDao.update(hotelPriceLimitDetail);
					}
					else{
						throw new BusinessException("房间已订完,不能再预订!");
					}
				}
				//计算订单总数量			
				totalAmount = totalAmount + hotelIndentDetail.getAmount();
			}
		}
		
		returnMap.put("totalAmount", totalAmount);
		returnMap.put("totalPrice", totalPrice);
		returnMap.put("list", list);
		
		return returnMap;
		
	}
	
	/**
	 * 保存活动订单时组合订单明细
	 * @return
	 */
	private Map<String,Object> creatDetailListForAct(Collection<HotelIndentDetail> coll,HotelIndent hotelIndent,Timestamp ctimestap,String customerType) {
		
		Map<String,Object> returnMap = new HashMap<String,Object>();
		
		int totalAmount = 0;
		double totalPrice = 0d;
		long hotelActivityId=hotelIndent.getHotelActivityId();
		
		HotelActivity ha = hotelActivityDao.findById(hotelActivityId);
		ha.setIsUse("11");
		hotelActivityDao.update(ha);
		
		List<Timestamp> timestamps = CommonMethod.getTimeStampList(Timestamp.valueOf(hotelIndent.getStartDate().toString()), Timestamp.valueOf(hotelIndent.getEndDate().toString()));
		
		//订单明细信息
		List<HotelIndentDetail> list = new ArrayList<HotelIndentDetail>();
		for (HotelIndentDetail hotelIndentDetail : coll) {
			if(hotelIndentDetail.getAmount()!=null && hotelIndentDetail.getAmount()!=0){
				//取得时间价格清单
				DetachedCriteria dc = DetachedCriteria.forClass(HotelActivityLimit.class);
				dc.createAlias("hotelRoomType", "hotelRoomType");
				dc.add(Property.forName("hotelRoomType.roomTypeId").eq(hotelIndentDetail.getHotelRoomType().getRoomTypeId()));
				dc.add(Property.forName("hotelActivity.hotelActivityId").eq(hotelActivityId));
				dc.add(Property.forName("useDate").ge(hotelIndent.getStartDate()));
				dc.add(Property.forName("useDate").lt(hotelIndent.getEndDate()));
				dc.addOrder(Order.asc("useDate"));
				List<HotelActivityLimit> hotelActivityLimits = hotelActivityLimitDao.findByCriteria(dc);
				long startPk = getNextKey("Hotel_Indent_Detail", hotelActivityLimits.size());
				
				/**2014-02-13 市场部从页面修改的价格 
				 * 从页面传回时在hotelIndentDetail.remark中
				 * 格式为   2014-02-13:900.00,2014-02-14:920.00,
				 */
				String priceStr = hotelIndentDetail.getRemark();
				Map<Timestamp,Double>  priceMap = new HashMap<Timestamp,Double>();
				if(!"00".equals(hotelIndent.getOrigin())){//如是市场部订单,获取修改后单价
					String[] prStrs = priceStr.split(",");
					
					for(String pr : prStrs){
						if(!"".equals(pr)){
							String[] pStrs = pr.split(":");
							priceMap.put(CommonMethod.string2Time1(pStrs[0]), Double.valueOf(pStrs[1]));
						}
					}
				}
				
				for (HotelActivityLimit hotelActivityLimit : hotelActivityLimits) {
					double actPrice = checkActivtyAndGetPrice(timestamps, hotelActivityLimit, customerType);
					
					HotelIndentDetail tempDetail = new HotelIndentDetail();
					tempDetail.setHotelIndentDetailId(startPk);
					tempDetail.setInputer(getCurrentUserId());
					tempDetail.setInputTime(ctimestap);
					tempDetail.setHotelIndent(hotelIndent);
					tempDetail.setHotelRoomType(hotelIndentDetail.getHotelRoomType());
					tempDetail.setAmount(hotelIndentDetail.getAmount());
					//通过价格清单设置明细信息
					hotelActivityLimit.setCustomerType(customerType);
					tempDetail.setBookingDate(hotelActivityLimit.getUseDate());
//					if(!"00".equals(hotelIndent.getOrigin())){//如是市场部订单,获取修改后单价
//						double unPrice = priceMap.get(hotelActivityLimit.getUseDate());
//						tempDetail.setUnitPrice(unPrice);
//						tempDetail.setSubtotal(hotelIndentDetail.getAmount()*unPrice);
//					}
//					else{
						tempDetail.setUnitPrice(actPrice);
						tempDetail.setSubtotal(hotelIndentDetail.getAmount()*actPrice);
//					}
					
					if("00".equals(hotelIndent.getOrigin())){//网络订单
						//查询并计算活动
						CommActivity commActivity = new CommActivity();
						commActivity.setActivityTime(hotelIndent.getStartDate());
						commActivity.setActivityEndTime(hotelIndent.getEndDate());
						commActivity.setRemark(hotelIndent.getHotel().getHotelId().toString());
						
						CommActivity commA = commActivityService.findEnabledActBuyGet(commActivity,"1");
						if(commA!=null){//有活动
							
							int aL = commA.getActivityLimit();//买X
							int aA = commA.getActivityAmount();//送X
							
							//应送数量
							int fA = (tempDetail.getAmount() / aL) * aA;
							
							tempDetail.setAmount(tempDetail.getAmount()+fA);
							
							tempDetail.setUnitPrice(tempDetail.getSubtotal()/tempDetail.getAmount());
							hotelIndentDetail.setAmount(tempDetail.getAmount());
						}
					}
					
					list.add(tempDetail);
					//计算总价
					totalPrice = totalPrice + tempDetail.getSubtotal();
					startPk = startPk+1;
					//更新剩余数量
					if(hotelActivityLimit.getRemainAmount() >= hotelIndentDetail.getAmount()){
						hotelActivityLimit.setRemainAmount(hotelActivityLimit.getRemainAmount()-hotelIndentDetail.getAmount());
						hotelActivityLimitDao.update(hotelActivityLimit);
					}
					else{
						throw new BusinessException("房间已订完,不能再预订!","");
					}
				}
				//计算订单总数量			
				totalAmount = totalAmount + hotelIndentDetail.getAmount();
			}
		}
		
		returnMap.put("totalAmount", totalAmount);
		returnMap.put("totalPrice", totalPrice);
		returnMap.put("list", list);
		
		return returnMap;
		
	}
	
	/**
	 * 验证活动是否有效并查询相关房间金额
	 * @param timestamps
	 * @return
	 */
	private double checkActivtyAndGetPrice(List<Timestamp> timestamps, HotelActivityLimit t,String customerType) {
		
		//查询活动日期
		DetachedCriteria dcAct = DetachedCriteria.forClass(HotelActivity.class);
		if(t.getHotelActivity()!=null && t.getHotelActivity().getHotelActivityId() !=null){
			dcAct.add(Property.forName("hotelActivityId").eq(t.getHotelActivity().getHotelActivityId()));
		}else{//没有传活动ID不查询出数据
			dcAct.add(Property.forName("hotelActivityId").eq(-1L));
		}
		if(t.getStartDate()!=null){
			dcAct.add(Property.forName("startDate").le(t.getStartDate()));
		}
		if(t.getEndDate()!=null){
			dcAct.add(Property.forName("endDate").ge(t.getEndDate()));
		}
		
		List<HotelActivity> haList = hotelActivityDao.findByCriteria(dcAct);
		if(haList==null || haList.size()<1){//没有符合条件的活动
			throw new BusinessException("没有符合条件的酒店活动！请重新确认订单！","");
		}
		
		//查询符合条件的活动明细
		DetachedCriteria dcActDet = DetachedCriteria.forClass(HotelActivityDetail.class);
		dcActDet.createAlias("hotelRoomType", "hotelRoomType");
		if(t.getHotelActivity()!=null && t.getHotelActivity().getHotelActivityId() !=null){
			dcActDet.add(Property.forName("hotelActivity.hotelActivityId").eq(t.getHotelActivity().getHotelActivityId()));
		}else{//没有传活动ID不查询出数据
			dcActDet.add(Property.forName("hotelActivity.hotelActivityId").eq(-1L));
		}
		dcActDet.add(Property.forName("customerType").eq(customerType));
		
		if(t.getHotelRoomType()!=null && t.getHotelRoomType().getRoomTypeId() !=null){
			dcActDet.add(Property.forName("hotelRoomType.roomTypeId").eq(t.getHotelRoomType().getRoomTypeId()));
		}
		List<HotelActivityDetail> hadList = hotelActivityDetailDao.findByCriteria(dcActDet);
		
		double actPrice = -1d;
		int days = timestamps.size()-1;
		
		for(HotelActivityDetail had : hadList){
			
			int minDays = had.getMinDays() == null ? 0 : had.getMinDays();
			int maxDays = had.getMaxDays() == null ? 0 : had.getMaxDays();
			
			if((minDays<=days && days<=maxDays) || (maxDays==0 && minDays<=days)){//符合条件
				actPrice = had.getRoomPrice() == null ? 0d : had.getRoomPrice();
			}
			
		}
		
		if(actPrice==-1d){//没有符合条件的活动
			throw new BusinessException("没有符合条件的酒店活动！请重新确认订单！","");
		}
		
		return actPrice;
		
	}
	
	/**
	 * 生成验证码
	 * @return
	 */
	private String findCheckCode(HotelIndent t){
		String returnStr = "";
		DetachedCriteria dc = DetachedCriteria.forClass(HotelIndent.class);
		dc.add(Property.forName("phoneNumber").eq(t.getPhoneNumber()));
		List<HotelIndent> list = hotelIndentDao.findByCriteria(dc);
		
		for (int i = 0; i < 6; i++) {//产生六位随机数
			returnStr = returnStr+""+(int)(1+Math.random()*9);
		}
		
		for (HotelIndent hotelIndent : list) {
			if(returnStr.equals(hotelIndent.getCheckCode())){//如果相同就继续产生下个随机数
				return findCheckCode(t);
			}
		}
		return returnStr;
	}
	
	/**
	 * 申请退订酒店
	 */
	@Override
	public void saveCheckOutIndent(Collection<HotelIndentDetail> coll,HotelIndentDetail hotelIndentDetail) {
		HotelIndent hotelIndent=this.hotelIndentDao.findById(hotelIndentDetail.getHotelIndent().getHotelIndentId());
		//03为已打印状态
		if(!"02".equals(hotelIndent.getStatus())){
			return;
		}
		
		Map<String,Integer> retDetail = new HashMap<String,Integer>();
		for (HotelIndentDetail hid : coll) {
			retDetail.put("del_"+hid.getHotelIndentDetailId(), hid.getAmountret());
		}
		List<HotelIndentDetail> oldDetails = hotelIndentDetailDao.findByProperty("hotelIndent.hotelIndentId", hotelIndent.getHotelIndentId());
		for (HotelIndentDetail hid : oldDetails) {
			hid.setAmountret(retDetail.get("del_"+hid.getHotelIndentDetailId())==null?0:retDetail.get("del_"+hid.getHotelIndentDetailId()));
			hotelIndentDetailDao.update(hid);
		}
		
		//修改订单状态(07为申请退订)和同步状态(04为退票未同步)
		hotelIndent.setStatus("07");
		hotelIndent.setSynchroState("04");
		hotelIndent.setUpdater(this.getCurrentUserId());
		hotelIndent.setUpdateTime(CommonMethod.getTimeStamp());
		
		this.hotelIndentDao.update(hotelIndent);
	}
	
	
	/**
	 * 免房损退订
	 */
	@Override
	public void saveCheckOutIndent(Collection<HotelIndentDetail> coll,HotelIndentDetail hotelIndentDetail,String node) {
		HotelIndent hotelIndent=this.hotelIndentDao.findById(hotelIndentDetail.getHotelIndent().getHotelIndentId());
		//03为已打印状态
		if(!"02".equals(hotelIndent.getStatus())){
			return;
		}
		List<HotelIndentDetail> oldDetails = hotelIndentDetailDao.findByProperty("hotelIndent.hotelIndentId", hotelIndent.getHotelIndentId());
		double charge  =  0d;
		double tkmoney = 0d;
		boolean allRefund = true;
		int totalAmount = 0;
		double totalPrice = 0d;
		List<HotelIndentDetail> tidList = new ArrayList<HotelIndentDetail>();
		Map<String,HotelIndentDetail> retDetail = new HashMap<String,HotelIndentDetail>();
		for(HotelIndentDetail hid : coll){
			for (HotelIndentDetail hidO : oldDetails) {
				if(hidO.getHotelIndentDetailId().equals(hid.getHotelIndentDetailId())){
					hid.setAmount(hidO.getAmount());
				}
			}
			double cutPrice = hid.getCutPrice() == null ? 0d : hid.getCutPrice();
			int retAmo = hid.getAmountret()==null?0:hid.getAmountret();
			charge += cutPrice;
			tkmoney += (retAmo*hid.getUnitPrice()-cutPrice);
			tidList.add(hid);
			retDetail.put("del_"+hid.getHotelIndentDetailId(), hid);
			int surplus = hid.getAmount()-retAmo;
			if(surplus>totalAmount){//数量取最大值
				totalAmount = surplus;
			}
			totalPrice += surplus*hid.getUnitPrice();
			if(surplus > 0){
				allRefund = false;
			}
		}
		double unitRatio = 0d;
		if((charge+tkmoney)>0d){
			unitRatio = (charge /(charge+tkmoney))*100;
		}
		
		if(saveRefund(hotelIndent, tidList, tkmoney,charge,unitRatio,"00")){//退款成功
			//if(allRefund){//全部退订
				hotelIndent.setSynchroState("07");//退款已同步
				hotelIndent.setStatus("05");//已退订
//			}else{//部分退订
//				hotelIndent.setSynchroState("00");
//				hotelIndent.setStatus("02");
//			}
			hotelIndent.setTotalAmount(totalAmount);
			hotelIndent.setTotalPrice(totalPrice);
			hotelIndent.setUpdater(this.getCurrentUserId());
			hotelIndent.setUpdateTime(CommonMethod.getTimeStamp());
			this.hotelIndentDao.update(hotelIndent);
		}
		for (HotelIndentDetail hid : oldDetails) {
			HotelIndentDetail hidN = retDetail.get("del_"+hid.getHotelIndentDetailId());
			if(hidN==null){
				continue;
			}
			hid.setAmountret(hidN.getAmountret());
			hid.setCutPrice(hidN.getCutPrice());
			hotelIndentDetailDao.update(hid);
		}
	}

	@Override
	public PayParams update(Collection<HotelIndentDetail> coll, HotelIndentDetail t) {
		Timestamp ctimestap = CommonMethod.getTimeStamp();
		HotelIndent hotelIndent = t.getHotelIndent();
		int totalAmount = 0;
		double totalPrice = 0d;
		//取得客户类型
		String customerType = "01";
		SysUser sysUser = getSysUser();
		if(hotelIndent.getCustomerType()!=null && !hotelIndent.getCustomerType().equals("")){//如无客户类型设置类型
			customerType = hotelIndent.getCustomerType();
		}
		else{
			if(sysUser.getUserType().equals("03")){
				customerType = "01";
			}
			else{
				customerType = "02";
			}
		}
		//订单信息
		if(hotelIndent.getOrigin()==null || "".equals(hotelIndent.getOrigin())){//如果不是市场部订单，即记为00网络订单
			hotelIndent.setOrigin("00");
		}
		hotelIndent.setStatus("01");
		hotelIndent.setSynchroState("00");
		hotelIndent.setCustomerType(customerType);
		if(hotelIndent.getCustomerName()==null || hotelIndent.getCustomerName().equals("")){//客户名称
			hotelIndent.setCustomerName(sysUser.getUserName());
		}
		//删除旧明细并存入历史明细表
		Map<String,Map<Timestamp,Double>> oldDet = new HashMap<String,Map<Timestamp,Double>>();
		List<HotelIndentDetail> oldDetails = hotelIndentDetailDao.findByProperty("hotelIndent.hotelIndentId", hotelIndent.getHotelIndentId());
		for (HotelIndentDetail ohid : oldDetails) {
			
			Map<Timestamp,Double> temp = oldDet.get("roomTypeId_"+ohid.getHotelRoomType().getRoomTypeId());
			if(temp==null){
				temp = new HashMap<Timestamp,Double>();
			}
			
			temp.put(Timestamp.valueOf(ohid.getBookingDate().toString()), ohid.getUnitPrice());
			
			oldDet.put("roomTypeId_"+ohid.getHotelRoomType().getRoomTypeId(),temp);
			
			HotelDetailHis hotelDetailHis = new HotelDetailHis();
			hotelDetailHis.setHotelDetailHisId(ohid.getHotelIndentDetailId());
			hotelDetailHis.setAmount(ohid.getAmount());
			hotelDetailHis.setBookingDate(ohid.getBookingDate());
			hotelDetailHis.setHotelIndent(hotelIndent);
			hotelDetailHis.setHotelIndentDetailCode(ohid.getHotelIndentDetailCode());
			hotelDetailHis.setHotelRoomType(ohid.getHotelRoomType());
			hotelDetailHis.setInputer(ohid.getInputer());
			hotelDetailHis.setInputTime(ohid.getInputTime());
			hotelDetailHis.setRemark(ohid.getRemark());
			hotelDetailHis.setSubtotal(ohid.getSubtotal());
			hotelDetailHis.setUnitPrice(ohid.getUnitPrice());
			hotelDetailHis.setHotelIndentDetailId(ohid.getHotelIndentDetailId());
			hotelDetailHisDao.save(hotelDetailHis);
			hotelIndentDetailDao.delete(ohid);
			//更新剩余数量
			DetachedCriteria dc = DetachedCriteria.forClass(HotelPriceLimitDetail.class);
			dc.add(Property.forName("hotelRoomType.roomTypeId").eq(ohid.getHotelRoomType().getRoomTypeId()));
			dc.add(Property.forName("useDate").eq(ohid.getBookingDate()));
			List<HotelPriceLimitDetail> hotelPriceLimitDetails = hotelPriceLimitDetailDao.findByCriteria(dc);
			if(hotelPriceLimitDetails!=null && !hotelPriceLimitDetails.isEmpty()){
				HotelPriceLimitDetail tempHpld = hotelPriceLimitDetails.get(0);
				tempHpld.setRemainAmount(tempHpld.getRemainAmount()+ohid.getAmount());
				hotelPriceLimitDetailDao.update(tempHpld);
			}
		}
		//订单明细信息
		List<HotelIndentDetail> list = new ArrayList<HotelIndentDetail>();
		for (HotelIndentDetail hotelIndentDetail : coll) {
			if(hotelIndentDetail.getAmount()!=null && hotelIndentDetail.getAmount()!=0){
				//取得时间价格清单
				DetachedCriteria dc = DetachedCriteria.forClass(HotelPriceLimitDetail.class);
				dc.createAlias("hotelRoomType", "hotelRoomType");
				dc.add(Property.forName("hotelRoomType.roomTypeId").eq(hotelIndentDetail.getHotelRoomType().getRoomTypeId()));
				dc.add(Property.forName("useDate").ge(hotelIndent.getStartDate()));
				dc.add(Property.forName("useDate").lt(hotelIndent.getEndDate()));
				dc.addOrder(Order.asc("useDate"));
				List<HotelPriceLimitDetail> hotelPriceLimitDetails = hotelPriceLimitDetailDao.findByCriteria(dc);
				long startPk = getNextKey("Hotel_Indent_Detail", hotelPriceLimitDetails.size());
				
				/**2014-02-13 市场部从页面修改的价格 
				 * 从页面传回时在hotelIndentDetail.remark中
				 * 格式为   2014-02-13:900.00,2014-02-14:920.00,
				 */
				String priceStr = hotelIndentDetail.getRemark();
				Map<Timestamp,Double>  priceMap = new HashMap<Timestamp,Double>();
				
				if(priceStr.trim().length() > 0 && priceStr.indexOf(",")>-1){//有价格改动则修改
					String[] prStrs = priceStr.split(",");
					
					for(String pr : prStrs){
						if(!"".equals(pr)){
							String[] pStrs = pr.split(":");
							priceMap.put(CommonMethod.string2Time1(pStrs[0]), Double.valueOf(pStrs[1]));
						}
					}
				}else{//没有价格改动用原价格
					priceMap = oldDet.get("roomTypeId_"+hotelIndentDetail.getHotelRoomType().getRoomTypeId());
				}
				
				
				for (HotelPriceLimitDetail hotelPriceLimitDetail : hotelPriceLimitDetails) {
					HotelIndentDetail tempDetail = new HotelIndentDetail();
					tempDetail.setHotelIndentDetailId(startPk);
					tempDetail.setInputer(getCurrentUserId());
					tempDetail.setInputTime(ctimestap);
					tempDetail.setHotelIndent(hotelIndent);
					tempDetail.setHotelRoomType(hotelIndentDetail.getHotelRoomType());
					tempDetail.setAmount(hotelIndentDetail.getAmount());
					//通过价格清单设置明细信息
					hotelPriceLimitDetail.setCustomerType(customerType);
					tempDetail.setBookingDate(hotelPriceLimitDetail.getUseDate());
					//if(!"00".equals(hotelIndent.getOrigin())){//如是市场部订单,获取修改后单价
						double unPrice = priceMap.get(hotelPriceLimitDetail.getUseDate());
						tempDetail.setUnitPrice(unPrice);
						tempDetail.setSubtotal(hotelIndentDetail.getAmount()*unPrice);
//					}
//					else{
//						tempDetail.setUnitPrice(hotelIndentDetail.getUnitPrice());
//						tempDetail.setSubtotal(hotelIndentDetail.getAmount()*hotelIndentDetail.getUnitPrice());
//					}
					list.add(tempDetail);
					//计算总价
					totalPrice = totalPrice + tempDetail.getSubtotal();
					startPk = startPk+1;
					//更新剩余数量
					if(hotelPriceLimitDetail.getRemainAmount() >= hotelIndentDetail.getAmount()){
						hotelPriceLimitDetail.setRemainAmount(hotelPriceLimitDetail.getRemainAmount()-hotelIndentDetail.getAmount());
						hotelPriceLimitDetailDao.update(hotelPriceLimitDetail);
					}
					else{
						throw new BusinessException("房间已订完,不能再预订!");
					}
				}
				//计算订单总数量			
				totalAmount = totalAmount + hotelIndentDetail.getAmount();
			}
		}
		if(totalAmount<1){
			throw new BusinessException("您还未填写订房数量！","");
		}
		//修改订单
		hotelIndent.setTotalAmount(totalAmount);
		hotelIndent.setTotalPrice(totalPrice);
		hotelIndent.setUpdater(getCurrentUserId());
		hotelIndent.setUpdateTime(ctimestap);
		hotelIndentDao.update(hotelIndent);
		
		//保存明细
		for (HotelIndentDetail hotelIndentDetail : list) {
			hotelIndentDetailDao.save(hotelIndentDetail);
		}
		
		if(!"00".equals(hotelIndent.getOrigin())){//市场部订单
			
			financeHotelAccountsService.saveReturnBackInfo(hotelIndent, null, null);
			//添加财务记录
			financeHotelAccountsService.savePayInfo(hotelIndent, null, null);
		}
		
		
		//生成易宝交易信息
		PayParams payParams = new PayParams();
		if("00".equals(hotelIndent.getOrigin())){//网络订单需要去易宝支付
//			String keyValue = CommonMethod.formatString(findSysParam("KEY_VALUE"));
//			payParams.setP0_Cmd("Buy");
//			payParams.setP1_MerId(findSysParam("P1_MERID"));
//			payParams.setP2_Order(hotelIndent.getIndentCode());
//			if(hotelIndent.getReceivedMoney()!=null && !hotelIndent.getReceivedMoney().isNaN() && hotelIndent.getReceivedMoney()>0){
//				payParams.setP3_Amt(CommonMethod.format2db(hotelIndent.getTotalPrice()-hotelIndent.getReceivedMoney()));//减去已付款金额
//			}
//			else{
//				payParams.setP3_Amt(CommonMethod.format2db(hotelIndent.getTotalPrice()));
//			}
//			payParams.setP4_Cur("CNY");
//			payParams.setP5_Pid("");
//			payParams.setP6_Pcat("酒店");
//			payParams.setP7_Pdesc("");
//			payParams.setP8_Url(Configuration.getInstance().getValue("resposeUrlForHotel"));
//			payParams.setP9_SAF("0");
//			payParams.setPa_MP("");
//			payParams.setPd_FrpId("");
//			payParams.setPm_Period("20");//20分钟不支付即过期
//			payParams.setPn_Unit("minute");//20分钟不支付即过期
//			payParams.setPr_NeedResponse("1");
//			payParams.setNodeAuthorizationURL(CommonMethod.formatString(findSysParam("YEEPAY_COMMON_REQURL")));
//			payParams.setHmac(PaymentForOnlineService
//					.getReqMd5HmacForOnlinePayment(payParams.getP0_Cmd(),
//							payParams.getP1_MerId(), payParams.getP2_Order(),
//							payParams.getP3_Amt(), payParams.getP4_Cur(),
//							CommonMethod.getNewString(payParams.getP5_Pid(),"GBK"), CommonMethod.getNewString(payParams.getP6_Pcat(),"GBK"),
//							CommonMethod.getNewString(payParams.getP7_Pdesc(),"GBK"), payParams.getP8_Url(),
//							payParams.getP9_SAF(), CommonMethod.getNewString(payParams.getPa_MP(),"GBK"),
//							payParams.getPd_FrpId(),payParams.getPm_Period(),payParams.getPn_Unit(),
//							payParams.getPr_NeedResponse(), keyValue));

			double moneyNeedPay = 0;
			if(hotelIndent.getReceivedMoney()!=null && !hotelIndent.getReceivedMoney().isNaN() && hotelIndent.getReceivedMoney()>0){
				moneyNeedPay = hotelIndent.getTotalPrice()-hotelIndent.getReceivedMoney();//减去已付款金额
			}
			else{
				moneyNeedPay = hotelIndent.getTotalPrice();
			}
			payFactoryService.init();
			payParams = payFactoryService.initPayParams(hotelIndent,moneyNeedPay);
		}
		return payParams;
	}
	
	/**
	 * 改签时计算金额
	 */
	@Override
	public Map<String,Object> findCalcTotal(Collection<HotelIndentDetail> coll, HotelIndentDetail t) {
		
		HotelIndent hotelIndent = t.getHotelIndent();
		
		//取得客户类型
		String customerType = "01";
		SysUser sysUser = getSysUser();
		if(hotelIndent.getCustomerType()!=null && !hotelIndent.getCustomerType().equals("")){//如无客户类型设置类型
			customerType = hotelIndent.getCustomerType();
		}
		else{
			if(sysUser.getUserType().equals("03")){
				customerType = "01";
			}
			else{
				customerType = "02";
			}
		}
		
		List<HotelIndentDetail> oldDetails = hotelIndentDetailDao.findByProperty("hotelIndent.hotelIndentId", hotelIndent.getHotelIndentId());
		
		//组合时间段
		Timestamp startDate = Timestamp.valueOf(hotelIndent.getStartDate().toString());
		Timestamp endDate = Timestamp.valueOf(hotelIndent.getEndDate().toString());
		
		HotelIndent hotelIndentOld = hotelIndentDao.findById(hotelIndent.getHotelIndentId());
		
		if(startDate.after(hotelIndentOld.getStartDate())){
			startDate = Timestamp.valueOf(hotelIndentOld.getStartDate().toString());
		}
		if(endDate.before(hotelIndentOld.getEndDate())){
			endDate = Timestamp.valueOf(hotelIndentOld.getEndDate().toString());
		}
		
		//将订单按日期和房型组合成map
		Map<Timestamp,Map<Long,HotelIndentDetail>> newDetailMap = new HashMap<Timestamp,Map<Long,HotelIndentDetail>>();
		for (HotelIndentDetail hotelIndentDetail : coll) {
			if(hotelIndentDetail.getAmount()!=null && hotelIndentDetail.getAmount()!=0){
				//取得时间价格清单
				Long rt = hotelIndentDetail.getHotelRoomType().getRoomTypeId();
				DetachedCriteria dc = DetachedCriteria.forClass(HotelPriceLimitDetail.class);
				dc.createAlias("hotelRoomType", "hotelRoomType");
				dc.add(Property.forName("hotelRoomType.roomTypeId").eq(rt));
				dc.add(Property.forName("useDate").ge(hotelIndent.getStartDate()));
				dc.add(Property.forName("useDate").lt(hotelIndent.getEndDate()));
				dc.addOrder(Order.asc("useDate"));
				List<HotelPriceLimitDetail> hotelPriceLimitDetails = hotelPriceLimitDetailDao.findByCriteria(dc);
				for (HotelPriceLimitDetail hotelPriceLimitDetail : hotelPriceLimitDetails) {
					Timestamp ht = Timestamp.valueOf(hotelPriceLimitDetail.getUseDate().toString());
					HotelIndentDetail tempDetail = new HotelIndentDetail();
					tempDetail.setAmount(hotelIndentDetail.getAmount());
					//通过价格清单设置明细信息
					hotelPriceLimitDetail.setCustomerType(customerType);
					tempDetail.setBookingDate(hotelPriceLimitDetail.getUseDate());
					tempDetail.setUnitPrice(hotelPriceLimitDetail.getPrice());
					tempDetail.setSubtotal(hotelIndentDetail.getAmount()*hotelPriceLimitDetail.getPrice());
					
					Map<Long,HotelIndentDetail> tempMap = newDetailMap.get(ht);
					if(tempMap==null){
						tempMap = new HashMap<Long,HotelIndentDetail>();
					}
					tempMap.put(rt, tempDetail);
					ht = CommonMethod.toStartTime(ht);
					newDetailMap.put(ht, tempMap);
				}
				
			}
		}
		
		Map<Timestamp,Map<Long,HotelIndentDetail>> oldDetailMap = new HashMap<Timestamp,Map<Long,HotelIndentDetail>>();
		for (HotelIndentDetail hotelIndentDetail : oldDetails) {
			Timestamp ht = Timestamp.valueOf(hotelIndentDetail.getBookingDate().toString());
			Long rt = hotelIndentDetail.getHotelRoomType().getRoomTypeId();
			Map<Long,HotelIndentDetail> tempMap = oldDetailMap.get(ht);
			if(tempMap==null){
				tempMap = new HashMap<Long,HotelIndentDetail>();
			}
			tempMap.put(rt, hotelIndentDetail);
			ht = CommonMethod.toStartTime(ht);
			oldDetailMap.put(ht, tempMap);
		}
		
		List<Timestamp> tList = CommonMethod.getTimeStampList(startDate, endDate);
		
		List<HotelRoomType> rtList = roomTypeDao.findByProperty("hotel.hotelId", hotelIndentOld.getHotel().getHotelId());
		
		/*{'日期'：{'房型':{'old':HotelIndentDetail,'new':HotelIndentDetail}}}*/
		Map<Timestamp,Map<Long,Map<String,HotelIndentDetail>>> detailMap = new HashMap<Timestamp,Map<Long,Map<String,HotelIndentDetail>>>();
		
		for (Timestamp ts : tList) {
			
			for (HotelRoomType hotelRoomType : rtList) {
				
				Long rt = hotelRoomType.getRoomTypeId();
				
				Map<Long,HotelIndentDetail> newM = newDetailMap.get(ts);
				HotelIndentDetail newH = null;
				if(newM!=null){
					newH = newM.get(rt);
				}
				
				Map<Long,HotelIndentDetail> oldM = oldDetailMap.get(ts);
				HotelIndentDetail oldH = null;
				if(oldM!=null){
					oldH = oldM.get(rt);
				}
				
				if(newH!=null || oldH!=null){
					Map<Long,Map<String,HotelIndentDetail>> tMap = detailMap.get(ts);
					if(tMap==null){
						tMap = new HashMap<Long,Map<String,HotelIndentDetail>>();
					}
					Map<String,HotelIndentDetail> hMap = tMap.get(rt);
					if(hMap==null){
						hMap = new HashMap<String,HotelIndentDetail>();
					}
					hMap.put("new", newH);
					hMap.put("old", oldH);
					
					tMap.put(rt, hMap);
					
					detailMap.put(ts, tMap);
					
				}
				
			}
			
		}
		
		//先查询扣款规则
		DetachedCriteria hcrDc = DetachedCriteria.forClass(HotelCutRules.class);
		hcrDc.add(Property.forName("cutRulesType").eq("11"));
		hcrDc.addOrder(Order.desc("updateTime"));
		List<HotelCutRules> hcrList = hotelCutRulesDao.findByCriteria(hcrDc);
		Map<Long,Double> hcrMap = new HashMap<Long,Double>();
		String hcrMsg = "";
		for (HotelCutRules hotelCutRules : hcrList) {
			//换算成分钟来对比，因为用小时的话误差有可能达到59分钟
			hcrMap.put((Long.valueOf(hotelCutRules.getLimitHour().toString())*60), hotelCutRules.getUnitRatio());
			
			hcrMsg += hotelCutRules.getLimitHour()+"小时之内退房将扣除"+hotelCutRules.getUnitRatio()+"%的房损费<br>";
		}
		
		//获取当前时间用于计算扣款
		Timestamp todayTime = CommonMethod.getTimeStamp();
		
		//计算金额变动
		double zjSums = 0;//追加金额
		double tkSums = 0;//退款金额
		double sxSums = 0;//退款手续费
		double kkSums = 0;//按规则扣款金额
		
		List<HotelIndentDetail> oldList = new ArrayList<HotelIndentDetail>();
		
		for (Timestamp ts : detailMap.keySet()) {
			
			Map<Long,Map<String,HotelIndentDetail>> tMap = detailMap.get(ts);
			
			for (Long rt : tMap.keySet()) {
				Map<String,HotelIndentDetail> hMap = tMap.get(rt);
				
				HotelIndentDetail newH = hMap.get("new");
				HotelIndentDetail oldH = hMap.get("old");
				
				if(oldH==null){//添加的房间
					double newT = newH.getSubtotal();
					zjSums += newT;
				}else if(newH==null){//减少的房间
					
					double ur = 0;
					if(hcrMap!=null && !hcrMap.isEmpty()){
						String timeStr = CommonMethod.timeToString(ts)+" 12:00:00";//从当天12点计算扣款时间段
						Timestamp indentTime = CommonMethod.string2Time1(timeStr);
						long diffHour = CommonMethod.diffMin(todayTime, indentTime);//换算成分钟来对比，因为用小时的话误差有可能达到59分钟
						long gzH = 0;
						for (long hcrKey : hcrMap.keySet()) {
							if(diffHour<=hcrKey){
								if(gzH==0){
									gzH = hcrKey;
								}else if(hcrKey<gzH){
									gzH = hcrKey;
								}
							}
						}
						if(gzH>0){
							ur = hcrMap.get(gzH);
						}
						
					}
					if(ur > 0){
						oldH.setCutPrice((oldH.getSubtotal() * ur /100));
						oldList.add(oldH);
						kkSums = kkSums + oldH.getCutPrice();
					}
					tkSums = tkSums + oldH.getSubtotal();
				}else{//修改的房间
					
					int newA = newH.getAmount();
					int oldA = oldH.getAmount();
					
					double newUP = newH.getUnitPrice();
					double oldUP = oldH.getUnitPrice();
					
					double newTP = newUP * newA;
					double oldTP = oldUP * oldA;
					
					if(oldA>newA){//减少的房间
						
						double ur = 0;
						if(hcrMap!=null && !hcrMap.isEmpty()){
							String timeStr = CommonMethod.timeToString(ts)+" 12:00:00";//从当天12点计算扣款时间段
							Timestamp indentTime = CommonMethod.string2Time1(timeStr);
							long diffHour = CommonMethod.diffMin(todayTime, indentTime);//换算成分钟来对比，因为用小时的话误差有可能达到59分钟
							long gzH = 0;
							for (long hcrKey : hcrMap.keySet()) {
								if(diffHour<=hcrKey){
									if(gzH==0){
										gzH = hcrKey;
									}else if(hcrKey<gzH){
										gzH = hcrKey;
									}
								}
							}
							if(gzH>0){
								ur = hcrMap.get(gzH);
							}
							
						}
						if(ur > 0){
							oldH.setCutPrice(((oldA - newA) * oldUP * ur /100));
							oldList.add(oldH);
							kkSums = kkSums + oldH.getCutPrice();
						}
						
						//价格可能变动所以有可能退款也有可能补款
						if(oldTP>newTP){//需退款
							tkSums = tkSums + oldTP - newTP;
						}else if(oldTP<newTP){//需要补款
							zjSums = zjSums + newTP - oldTP;
						}
					}else if(oldA<=newA){//添加的房间或不变
						//数量不变时价格可能变动所以有可能退款也有可能补款
						if(oldTP>newTP){//需退款
							tkSums = tkSums + oldTP - newTP;
						}else if(oldTP<newTP){//需要补款
							zjSums = zjSums + newTP - oldTP;
						}
						
					}
					
				}
				
			}
			
		}
		
		
		if(zjSums > tkSums && tkSums >0){
			zjSums = zjSums - tkSums;
			tkSums = 0;
		}else if(tkSums > zjSums && zjSums >0){
			tkSums = tkSums - zjSums;
			zjSums = 0;
		}else if(tkSums == zjSums && zjSums >0){
			tkSums = 0;
			zjSums = 0;
		}
		
		
		String msg = "";
		
		DecimalFormat df = new DecimalFormat("#.00");
		
		//拼凑提示信息
		if(kkSums>0){
			kkSums = Double.valueOf(df.format(kkSums));
			msg += "根据酒店改签扣款规则：<br>"+hcrMsg+
					"您将被扣除房损费："+kkSums+"<br>";
		}
		if(tkSums>0){
			tkSums = tkSums - kkSums;
			//获取手续费比例
			double per_fee = Double.valueOf(findSysParam("PER_FEE"));
			sxSums = tkSums * per_fee;
			sxSums = Double.valueOf(df.format(sxSums));
			tkSums = Double.valueOf(df.format(tkSums-sxSums));
			msg += "将按比例"+(per_fee*100)+"%扣除手续费："+sxSums+"<br>"+
					"改签后将退回金额：<span style='color:red;'>"+tkSums+"</span><br>";
		}
		if(zjSums>0){
			zjSums = zjSums + kkSums;
			zjSums = Double.valueOf(df.format(zjSums));
			msg += "改签后需补交金额：<span style='color:red;'>"+zjSums+"</span><br>";
		}
		
		Map<String,Object> reMap = new HashMap<String,Object>();
		
		
		reMap.put("zjSums", zjSums);
		reMap.put("tkSums", tkSums);
		reMap.put("sxSums", sxSums);
		reMap.put("kkSums", kkSums);
		reMap.put("msg", msg);
		reMap.put("oldList", oldList);
		
		return reMap;
		
	}
	
	@Override
	public Map<String,Object> updateChange(Collection<HotelIndentDetail> coll, HotelIndentDetail t, String cutJson) {
		//生成易宝交易信息
		Map<String,Object> te = new HashMap<String,Object>();
		Timestamp ctimestap = CommonMethod.getTimeStamp();
		HotelIndent hotelIndent = t.getHotelIndent();
		//改签前先退订,检查是否已入住
		HotelIndentClient client = new HotelIndentClient();
		//调用接口发送
		List<HotelIndent> hiList = new ArrayList<HotelIndent>();
		hiList.add(hotelIndent);
		Hotel hotel = hotelDao.findById(hotelIndent.getHotel().getHotelId());
		ReturnInfo ri = client.CancelHotelIndent(hiList,HotelCode.valueOf(hotel.getHotelCode()));
		//处理返回结果
		if("-1".equals(ri.getResult())){
			throw new BusinessException("改签失败,失败原因:"+ri.getError());
		}
		else{
			XStream xsStream = new XStream();
			xsStream.alias("obj", HotelIndent.class);
			xsStream.alias("root", List.class);
			List<HotelIndent> tempList = (List<HotelIndent>)xsStream.fromXML(ri.getResult());
			if(tempList!=null && !tempList.isEmpty()){
				HotelIndent tempHi = tempList.get(0);
				if("03".equals(tempHi.getStatus())){//已入住
					hotelIndent = hotelIndentDao.findById(hotelIndent.getHotelIndentId());
					hotelIndent.setStatus("03");
					hotelIndentDao.update(hotelIndent);
					te.put("exception", "当前订单已入住,无法改签");
					return te;
				}
				else if("05".equals(tempHi.getStatus())){//可改签
					
				}
				else{
					throw new BusinessException("因订单状态异常,改签失败");
				}
			}
			else{
				throw new BusinessException("因网络异常,改签失败");
			}
		}
		
		//以下处理改签
		int totalAmount = 0;
		double totalPrice = 0d;
		//取得客户类型
		String customerType = "01";
		SysUser sysUser = getSysUser();
		if(hotelIndent.getCustomerType()!=null && !hotelIndent.getCustomerType().equals("")){//如无客户类型设置类型
			customerType = hotelIndent.getCustomerType();
		}
		else{
			if(sysUser.getUserType().equals("03")){
				customerType = "01";
			}
			else{
				customerType = "02";
			}
		}
		//订单信息
		if(hotelIndent.getOrigin()==null || "".equals(hotelIndent.getOrigin())){//如果不是市场部订单，即记为00网络订单
			hotelIndent.setOrigin("00");
		}
		hotelIndent.setSynchroState("00");
		hotelIndent.setCustomerType(customerType);
		if(hotelIndent.getCustomerName()==null || hotelIndent.getCustomerName().equals("")){//客户名称
			hotelIndent.setCustomerName(sysUser.getUserName());
		}
		//转化JSON判断是否进行支付或退款
		double zjSums = 0d;//追加付款数
		double tkSums = 0d;//退款数
		double charge = 0d;//扣款数
		JSONArray oldList = new JSONArray();
		if(cutJson!=null && !"".equals(cutJson)){
			CommonJsonConfig jsonConfig = new CommonJsonConfig();
			JSONObject jsonObj = JSONObject.fromObject(cutJson, jsonConfig);
			oldList = jsonObj.getJSONArray("oldList");
			zjSums = jsonObj.getDouble("zjSums");
			tkSums = jsonObj.getDouble("tkSums");
		}
		//删除旧明细并存入历史明细表
		List<HotelIndentDetail> oldDetails = hotelIndentDetailDao.findByProperty("hotelIndent.hotelIndentId", hotelIndent.getHotelIndentId());
		for (HotelIndentDetail ohid : oldDetails) {
			HotelDetailHis hotelDetailHis = new HotelDetailHis();
			hotelDetailHis.setHotelDetailHisId(ohid.getHotelIndentDetailId());
			hotelDetailHis.setAmount(ohid.getAmount());
			hotelDetailHis.setBookingDate(ohid.getBookingDate());
			hotelDetailHis.setHotelIndent(hotelIndent);
			hotelDetailHis.setHotelIndentDetailCode(ohid.getHotelIndentDetailCode());
			hotelDetailHis.setHotelRoomType(ohid.getHotelRoomType());
			hotelDetailHis.setInputer(ohid.getInputer());
			hotelDetailHis.setInputTime(ohid.getInputTime());
			hotelDetailHis.setRemark(ohid.getRemark());
			hotelDetailHis.setSubtotal(ohid.getSubtotal());
			hotelDetailHis.setUnitPrice(ohid.getUnitPrice());
			hotelDetailHis.setHotelIndentDetailId(ohid.getHotelIndentDetailId());
			//保存扣款信息
			if(oldList!=null && !oldList.isEmpty()){
				for (int i = 0; i < oldList.size(); i++) {
					JSONObject object = oldList.getJSONObject(i);
					Long temId = object.getLong("hotelIndentDetailId");
					if(object!=null && temId!=null && temId.equals(ohid.getHotelIndentDetailId())){
						hotelDetailHis.setCutPrice(object.getDouble("cutPrice"));
						charge += hotelDetailHis.getCutPrice()==null?0d:hotelDetailHis.getCutPrice();
					}
				}
			}
			hotelDetailHisDao.save(hotelDetailHis);
			hotelIndentDetailDao.delete(ohid);
			//更新剩余数量
			DetachedCriteria dc = DetachedCriteria.forClass(HotelPriceLimitDetail.class);
			dc.createAlias("hotelRoomType", "hotelRoomType");
			dc.add(Property.forName("hotelRoomType.roomTypeId").eq(ohid.getHotelRoomType().getRoomTypeId()));
			dc.add(Property.forName("useDate").eq(ohid.getBookingDate()));
			List<HotelPriceLimitDetail> hotelPriceLimitDetails = hotelPriceLimitDetailDao.findByCriteria(dc);
			if(hotelPriceLimitDetails!=null && !hotelPriceLimitDetails.isEmpty()){
				HotelPriceLimitDetail tempHpld = hotelPriceLimitDetails.get(0);
				tempHpld.setRemainAmount(tempHpld.getRemainAmount()+ohid.getAmount());
				hotelPriceLimitDetailDao.update(tempHpld);
			}
		}
		//订单明细信息
		List<HotelIndentDetail> list = new ArrayList<HotelIndentDetail>();
		for (HotelIndentDetail hotelIndentDetail : coll) {
			if(hotelIndentDetail.getAmount()!=null && hotelIndentDetail.getAmount()!=0){
				//取得时间价格清单
				DetachedCriteria dc = DetachedCriteria.forClass(HotelPriceLimitDetail.class);
				dc.createAlias("hotelRoomType", "hotelRoomType");
				dc.add(Property.forName("hotelRoomType.roomTypeId").eq(hotelIndentDetail.getHotelRoomType().getRoomTypeId()));
				dc.add(Property.forName("useDate").ge(hotelIndent.getStartDate()));
				dc.add(Property.forName("useDate").lt(hotelIndent.getEndDate()));
				dc.addOrder(Order.asc("useDate"));
				List<HotelPriceLimitDetail> hotelPriceLimitDetails = hotelPriceLimitDetailDao.findByCriteria(dc);
				long startPk = getNextKey("Hotel_Indent_Detail", hotelPriceLimitDetails.size());
				for (HotelPriceLimitDetail hotelPriceLimitDetail : hotelPriceLimitDetails) {
					HotelIndentDetail tempDetail = new HotelIndentDetail();
					tempDetail.setHotelIndentDetailId(startPk);
					tempDetail.setInputer(getCurrentUserId());
					tempDetail.setInputTime(ctimestap);
					tempDetail.setHotelIndent(hotelIndent);
					tempDetail.setHotelRoomType(hotelIndentDetail.getHotelRoomType());
					tempDetail.setAmount(hotelIndentDetail.getAmount());
					//通过价格清单设置明细信息
					hotelPriceLimitDetail.setCustomerType(customerType);
					tempDetail.setBookingDate(hotelPriceLimitDetail.getUseDate());
					tempDetail.setUnitPrice(hotelPriceLimitDetail.getPrice());
					tempDetail.setSubtotal(hotelIndentDetail.getAmount()*hotelPriceLimitDetail.getPrice());
					list.add(tempDetail);
					//计算总价
					totalPrice = totalPrice + tempDetail.getSubtotal();
					startPk = startPk+1;
					//更新剩余数量
					if(hotelPriceLimitDetail.getRemainAmount() >= hotelIndentDetail.getAmount()){
						hotelPriceLimitDetail.setRemainAmount(hotelPriceLimitDetail.getRemainAmount()-hotelIndentDetail.getAmount());
						hotelPriceLimitDetailDao.update(hotelPriceLimitDetail);
					}
					else{
						throw new BusinessException("房间已订完,不能再预订!");
					}
				}
				//计算订单总数量			
				totalAmount = totalAmount + hotelIndentDetail.getAmount();
			}
		}
		
		//修改订单
		hotelIndent.setTotalAmount(totalAmount);
		hotelIndent.setTotalPrice(totalPrice);
		hotelIndent.setUpdater(getCurrentUserId());
		hotelIndent.setUpdateTime(ctimestap);
		if(tkSums==0){
			hotelIndent.setStatus("01");
		}else{
			hotelIndent.setStatus("02");
			hotelIndent.setReceivedMoney(hotelIndent.getTotalPrice());
			hotelIndent.setPayType("9");
		}
		if(zjSums>0){
			hotelIndent.setFrontMoney(totalPrice-zjSums);
		}
		hotelIndentDao.update(hotelIndent);
		//保存明细
		for (HotelIndentDetail hotelIndentDetail : list) {
			hotelIndentDetailDao.save(hotelIndentDetail);
		}
		
		PayParams payParams = new PayParams();
		if(zjSums>0){//有追加时转到易宝支付
			if("00".equals(hotelIndent.getOrigin())){//网络订单需要去易宝支付
//				String keyValue = CommonMethod.formatString(findSysParam("KEY_VALUE"));
//				payParams.setP0_Cmd("Buy");
//				payParams.setP1_MerId(findSysParam("P1_MERID"));
//				payParams.setP2_Order(hotelIndent.getIndentCode());
//				payParams.setP3_Amt(CommonMethod.format2db(zjSums));
//				payParams.setP4_Cur("CNY");
//				payParams.setP5_Pid("");
//				payParams.setP6_Pcat("酒店");
//				payParams.setP7_Pdesc("");
//				payParams.setP8_Url(Configuration.getInstance().getValue("resposeUrlForHotel"));
//				payParams.setP9_SAF("0");
//				payParams.setPa_MP("");
//				payParams.setPd_FrpId("");
//				payParams.setPm_Period("20");//20分钟不支付即过期
//				payParams.setPn_Unit("minute");//20分钟不支付即过期
//				payParams.setPr_NeedResponse("1");
//				payParams.setNodeAuthorizationURL(CommonMethod.formatString(findSysParam("YEEPAY_COMMON_REQURL")));
//				payParams.setHmac(PaymentForOnlineService
//						.getReqMd5HmacForOnlinePayment(payParams.getP0_Cmd(),
//								payParams.getP1_MerId(), payParams.getP2_Order(),
//								payParams.getP3_Amt(), payParams.getP4_Cur(),
//								CommonMethod.getNewString(payParams.getP5_Pid(),"GBK"), CommonMethod.getNewString(payParams.getP6_Pcat(),"GBK"),
//								CommonMethod.getNewString(payParams.getP7_Pdesc(),"GBK"), payParams.getP8_Url(),
//								payParams.getP9_SAF(), CommonMethod.getNewString(payParams.getPa_MP(),"GBK"),
//								payParams.getPd_FrpId(),payParams.getPm_Period(),payParams.getPn_Unit(),
//								payParams.getPr_NeedResponse(), keyValue));
//	
				payFactoryService.init();
				payParams = payFactoryService.initPayParams(hotelIndent,zjSums);
			}
			
			te.put("payParams", payParams);
		}
		else if(tkSums>0){//有退款时,通知易宝退款
			
			if(saveRefund(hotelIndent,list,tkSums,charge,0d,"11")){//退款成功
				te.put("refund", "退款成功！");
			}else{
				throw new BusinessException("退款失败！");
			}
		}else{
			te.put("success", "改签成功！");
		}
		
		
		return te;
	}
	
	/**
	 * 退款
	 * @param hotelIndent
	 * @param tkSums 
	 * @return
	 */
	public boolean saveRefund(HotelIndent hotelIndent, List<HotelIndentDetail> hotelIndentDetails, double tkSums, double charge, double unitRatio,String cutRulesType){
		boolean isrf = false;
		DetachedCriteria dc = DetachedCriteria.forClass(BaseTranRec.class);//查询有交易流水号的付款记录
		//dc.add(Property.forName("trxId").isNotEmpty());
		dc.add(Property.forName("trxId").isNotNull());
		dc.add(Property.forName("hotelIndent.hotelIndentId").eq(hotelIndent.getHotelIndentId()));
		dc.addOrder(Order.asc("tranRecId"));
		List<BaseTranRec> btsList = baseTranRecDao.findByCriteria(dc);
		List<BaseTranRec> baseTranRecs = baseTranRecService.excludeReturnBackInfo(btsList);
		if(baseTranRecs!=null && !baseTranRecs.isEmpty()){
			BaseTranRec baseTranRec = baseTranRecs.get(0);
			System.out.println("退款流水号:"+baseTranRec.getTrxId());
			System.out.println("退款金额:"+tkSums);
			payFactoryService.init();
			RefundParams refundParams = payFactoryService.initRefundParams(baseTranRec,CommonMethod.format2db(tkSums-(tkSums*(hotelIndent.getPerFee()==null?0:hotelIndent.getPerFee()))),PayFactoryService.HOTEL);
			if(payFactoryService.refund(refundParams)){//退款成功
				try{
					BaseTranRec rec = new BaseTranRec();
					rec.setTrxId(baseTranRec.getTrxId());
					rec.setHotelIndent(hotelIndent);
					rec.setMoney(0-tkSums);//负数
					rec.setPerFee(baseTranRec.getPerFee());
					rec.setPayFee(baseTranRec.getPayFee());
					rec.setPaymentType("02");//退款
					rec.setPayType("01");//交易类型,通过易宝交易
					rec.setPaymentTime(CommonMethod.getTimeStamp());
					rec.setPaymentAccount("02");
					baseTranRecService.save(rec);
					
					//添加财务记录
					financeHotelAccountsService.saveReturnInfo(hotelIndent, hotelIndentDetails, rec, null);
					
					if(charge > 0){//扣款大于0则添加扣款记录
						HotelCutNotes hcn = new HotelCutNotes();
						hcn.setCutNotesId(getNextKey("HOTEL_CUT_NOTES".toUpperCase(), 1));
						hcn.setHotelIndent(hotelIndent);
						hcn.setUnitRatio(unitRatio);
						hcn.setCutRulesType(cutRulesType);
						hcn.setCutPrice(charge);
						hcn.setPayType("00");
						hcn.setInputer(hotelIndent.getInputer());
						hcn.setInputTime(CommonMethod.getTimeStamp());
						hotelCutNotesDao.save(hcn);
						//添加财务记录
						financeHotelAccountsService.saveCutInfo(hotelIndent, hcn,null);
						
					}
					
					
				}
				catch (Exception e) {
					e.printStackTrace();
					logger.error("退款成功,但保存退款记录出错，订单:"+hotelIndent.getIndentCode()+"，错误原因："+e.getMessage());
				}
				isrf = true;
			}
		}else{//奖励款退款
			System.out.println("奖励款退款");
			SysUser sysUser = sysUserDao.findById(hotelIndent.getInputer());
			Travservice bta = baseTravelAgencyDao.findById(sysUser.getBaseTravelAgency().getTravelAgencyId());
			
			DetachedCriteria bra = DetachedCriteria.forClass(BaseRewardAccount.class);
			bra.createAlias("baseRewardType", "baseRewardType");
			bra.add(Property.forName("baseTravelAgency.travelAgencyId").eq(bta.getTravelAgencyId()));
			bra.add(Property.forName("baseRewardType.rewardTypeCode").eq("01"));
			bra.add(Property.forName("baseRewardType.startDate").le(CommonMethod.toStartTime(Timestamp.valueOf(hotelIndent.getStartDate().toString()))));
			bra.add(Property.forName("baseRewardType.endDate").ge(CommonMethod.toEndTime(Timestamp.valueOf(hotelIndent.getEndDate().toString()))));
			List<BaseRewardAccount> list = baseRewardAccountService.findByCriteria(bra);
			BaseRewardAccount sr = new BaseRewardAccount();
			if(list!=null && list.size()>0){
				sr = list.get(0);
			}
			double tS = sr.getRewardBalance()+tkSums;
			sr.setRewardBalance(tS);
			baseRewardAccountService.update(sr);
			//酒店充值记录
			HotelRechargeRec hotelRechargeRec = new HotelRechargeRec();
			hotelRechargeRec.setHotelRechargeRecId(getNextKey("HOTEL_RECHARGE_REC".toUpperCase(), 1));
			hotelRechargeRec.setHotelIndent(hotelIndent);
			hotelRechargeRec.setBaseTravelAgency(bta);
			hotelRechargeRec.setBaseRewardAccount(sr);
			hotelRechargeRec.setSourceDescription("奖励款退款");
			hotelRechargeRec.setRewardMoney(tkSums);
			hotelRechargeRec.setRewardBalance(tS);
			hotelRechargeRec.setPaymentType("02");
			hotelRechargeRec.setInputer(hotelIndent.getInputer());
			hotelRechargeRec.setInputTime(CommonMethod.getTimeStamp());
			hotelRechargeRecDao.save(hotelRechargeRec);
			
			//添加财务记录
			financeHotelAccountsService.saveReturnInfo(hotelIndent, hotelIndentDetails, null, hotelRechargeRec);
			
			if(charge > 0){//扣款大于0则添加扣款记录
				HotelCutNotes hcn = new HotelCutNotes();
				hcn.setCutNotesId(getNextKey("HOTEL_CUT_NOTES".toUpperCase(), 1));
				hcn.setHotelIndent(hotelIndent);
				hcn.setUnitRatio(unitRatio);
				hcn.setCutRulesType(cutRulesType);
				hcn.setCutPrice(charge);
				hcn.setPayType("01");
				hcn.setInputer(hotelIndent.getInputer());
				hcn.setInputTime(CommonMethod.getTimeStamp());
				hotelCutNotesDao.save(hcn);
				
				//添加财务记录
				financeHotelAccountsService.saveCutInfo(hotelIndent, hcn,hotelRechargeRec);
			}
			
			
			isrf = true;
		}
		return isrf;
	}

	@Override
	public Map<String,Object> findCountRefundMoney(HotelIndent hotelIndent,String cutRulesType) {
		//先查询扣款规则
		DetachedCriteria hcrDc = DetachedCriteria.forClass(HotelCutRules.class);
		hcrDc.add(Property.forName("cutRulesType").eq(cutRulesType));
		hcrDc.addOrder(Order.desc("updateTime"));
		List<HotelCutRules> hcrList = hotelCutRulesDao.findByCriteria(hcrDc);
		Map<Long,Double> hcrMap = new HashMap<Long,Double>();
		for (HotelCutRules hotelCutRules : hcrList) {
			//换算成分钟来对比，因为用小时的话误差有可能达到59分钟
			hcrMap.put((Long.valueOf(hotelCutRules.getLimitHour().toString())*60), hotelCutRules.getUnitRatio());
		}
		//获取当前时间用于计算扣款
		Timestamp todayTime = CommonMethod.getTimeStamp();
		double ur = 0;
		long diffHour = CommonMethod.diffMin(todayTime, Timestamp.valueOf(hotelIndent.getStartDate().toString()));//换算成分钟来对比，因为用小时的话误差有可能达到59分钟
		long gzH = 0;
		for (long hcrKey : hcrMap.keySet()) {
			if(diffHour<=hcrKey){
				if(gzH==0){
					gzH = hcrKey;
				}else if(hcrKey<gzH){
					gzH = hcrKey;
				}
			}
		}
		if(gzH>0){
			ur = hcrMap.get(gzH);
		}
		
		Map<String,Object> map = new HashMap<String,Object>();
		
		if(ur > 0){
			map.put("unitRatio", ur);
			map.put("totalPrice", hotelIndent.getTotalPrice()-(hotelIndent.getTotalPrice()*ur/100));
			map.put("charge",hotelIndent.getTotalPrice()*ur/100);
		}else{
			map.put("unitRatio", 0d);
			map.put("totalPrice", hotelIndent.getTotalPrice());
			map.put("charge",0d);
		}
		return map;
	}
	
	@Override
	public Map<String,Object> findCountRefundMoney(Collection<HotelIndentDetail> coll,String cutRulesType) {
		//先查询扣款规则
		DetachedCriteria hcrDc = DetachedCriteria.forClass(HotelCutRules.class);
		hcrDc.add(Property.forName("cutRulesType").eq(cutRulesType));
		hcrDc.addOrder(Order.desc("updateTime"));
		List<HotelCutRules> hcrList = hotelCutRulesDao.findByCriteria(hcrDc);
		Map<Long,Double> hcrMap = new HashMap<Long,Double>();
		for (HotelCutRules hotelCutRules : hcrList) {
			//换算成分钟来对比，因为用小时的话误差有可能达到59分钟
			hcrMap.put((Long.valueOf(hotelCutRules.getLimitHour().toString())*60), hotelCutRules.getUnitRatio());
		}
		//获取当前时间用于计算扣款
		Timestamp todayTime = CommonMethod.getTimeStamp();
		
		double refund = 0d;
		double charge = 0d;
		List<Double> ratList = new ArrayList<Double>();
		
		for (HotelIndentDetail hotelIndentDetail : coll) {
			double ur = 0;
			double refundSub = hotelIndentDetail.getAmountret() * hotelIndentDetail.getUnitPrice();
			long diffHour = CommonMethod.diffMin(todayTime, Timestamp.valueOf(hotelIndentDetail.getBookingDate().toString()));//换算成分钟来对比，因为用小时的话误差有可能达到59分钟
			long gzH = 0;
			for (long hcrKey : hcrMap.keySet()) {
				if(diffHour<=hcrKey){
					if(gzH==0){
						gzH = hcrKey;
					}else if(hcrKey<gzH){
						gzH = hcrKey;
					}
				}
			}
			if(gzH>0){
				ur = hcrMap.get(gzH);
			}
			if(ur > 0){
				hotelIndentDetail.setCutPrice(refundSub*ur/100);
				charge += refundSub*ur/100;
				refund += refundSub - (refundSub*ur/100);
				ratList.add(ur);
			}else{
				hotelIndentDetail.setCutPrice(0d);
				refund += refundSub;
			}
		}
		double unitRatio = 0d;
		if(ratList.size()>0){
			for(Double rat : ratList){
				unitRatio += rat;
			}
			unitRatio = unitRatio/ratList.size();
		}
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("refund",refund);
		map.put("unitRatio",unitRatio);
		map.put("charge",charge);
		map.put("delList", coll);
		return map;
	}

	@Override
	public HashMap<String,Object> findRefundAndChargeAmount(HotelIndentDetail hotelIndentDetail) {
		HashMap<String,Object> refundAndChargeAttr=new HashMap<String,Object>();
		HotelIndent hotelIndent=this.hotelIndentDao.findById(hotelIndentDetail.getHotelIndent().getHotelIndentId());
		if(hotelIndent!=null){
			Map<String,Object> map = this.findCountRefundMoney(hotelIndent,"00");
			Double refundAmount= Double.valueOf(map.get("totalPrice").toString());
			refundAndChargeAttr.put("refund",refundAmount);
			refundAndChargeAttr.put("unitRatio",map.get("unitRatio").toString());
			refundAndChargeAttr.put("charge",hotelIndent.getTotalPrice()-refundAmount);
		}
		return refundAndChargeAttr;
	}

	@Override
	public boolean findBookedSpecialPrice(
			HotelPriceLimitDetail t) {
		boolean isBooked = false;
		Timestamp et = CommonMethod.addDay(t.getEndDate(), -1);
		HotelRoomType hrt = roomTypeDao.findById(t.getHotelRoomType().getRoomTypeId());
		if("01".equals(hrt.getRemark())){//特价房
			DetachedCriteria dc = DetachedCriteria.forClass(HotelIndentDetail.class);
			dc.add(Property.forName("bookingDate").ge(t.getStartDate()));
			dc.add(Property.forName("bookingDate").le(et));
			dc.add(Property.forName("hotelRoomType.roomTypeId").eq(t.getHotelRoomType().getRoomTypeId()));
			dc.add(Property.forName("inputer").eq(getCurrentUserId()));
			List<HotelIndentDetail> list = hotelIndentDetailDao.findByCriteria(dc);
			if(list!=null && !list.isEmpty()){
				isBooked = true;
			}
		}
		return isBooked;
	}
}