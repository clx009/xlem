package service.hotel.hotelActivity;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.HotelActivity;

import common.CommonMethod;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.hotel.hotelActivity.HotelActivityDao;
@Service("hotelActivityService")
public class HotelActivityServiceImpl extends BaseServiceImpl<HotelActivity> implements HotelActivityService {
@Autowired
private HotelActivityDao hotelActivityDao;
	@Override
	@Resource(name="hotelActivityDao")
	protected void initBaseDAO(BaseDao<HotelActivity> baseDao) {
		setBaseDao(baseDao);
	}
@Override
public PaginationSupport<HotelActivity> findPageByCriteria(PaginationSupport<HotelActivity> ps, HotelActivity t) {
	DetachedCriteria dc = DetachedCriteria.forClass(HotelActivity.class);
	return hotelActivityDao.findPageByCriteria(ps, Order.desc("hotelActivityId"), dc);
}
@Override
public void save(HotelActivity t) {
	t.setHotelActivityId(getNextKey("HotelActivity".toUpperCase(), 1));
	hotelActivityDao.save(t);
}
@Override
public JsonPager<HotelActivity> findJsonPageByCriteria(
		JsonPager<HotelActivity> jp, HotelActivity t) {
	DetachedCriteria dc = DetachedCriteria.forClass(HotelActivity.class);
	dc.addOrder(Order.desc("endDate"));
	return hotelActivityDao.findJsonPageByCriteria(jp, dc);
}

@Override
public void save(Collection<HotelActivity> coll, HotelActivity t) {
	for (HotelActivity act : coll) {
		if (act.getHotelActivityId() == null) {
			act.setHotelActivityId(getNextKey("HOTEL_ACTIVITY".toUpperCase(), 1));
			act.setInputer(getCurrentUserId());
			act.setInputTime(CommonMethod.getTimeStamp());
			act.setEndDate(CommonMethod.toEndTime(Timestamp.valueOf(act.getEndDate().toString())));
			act.setStatus("11");
			act.setIsUse("00");
			hotelActivityDao.save(act);
		} else {
			act.setEndDate(CommonMethod.toEndTime(Timestamp.valueOf(act.getEndDate().toString())));
			act.setUpdater(getCurrentUserId());
			act.setUpdateTime(CommonMethod.getTimeStamp());
			hotelActivityDao.update(act);
		}
	}
}

@Override
public List<HotelActivity> findHotelActivityList(HotelActivity t) {
	DetachedCriteria dc = DetachedCriteria.forClass(HotelActivity.class);
	if(t !=null && t.getHotelActivityId() !=null){
		dc.add(Property.forName("hotelActivityId").eq(t.getHotelActivityId()));
	}
	dc.add(Property.forName("status").eq("11"));
	if(t.getStartDate()!=null){
		dc.add(Property.forName("startDate").le(t.getStartDate()));
	}
	if(t.getEndDate()!=null){
		dc.add(Property.forName("endDate").ge(t.getEndDate()));
	}
	dc.add(Property.forName("endDate").ge(CommonMethod.getTimeStamp()));
	dc.addOrder(Order.desc("endDate"));
	return hotelActivityDao.findByCriteria(dc);
}

}