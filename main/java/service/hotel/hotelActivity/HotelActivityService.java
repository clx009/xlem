package service.hotel.hotelActivity;
import java.util.Collection;
import java.util.List;

import com.xlem.dao.HotelActivity;

import common.service.BaseService;

public interface HotelActivityService extends BaseService<HotelActivity> {

	void save(Collection<HotelActivity> coll, HotelActivity t);
	
	/**
	 * 查询有效活动列表
	 * @return
	 */
	public List<HotelActivity> findHotelActivityList(HotelActivity t);
}