package service.hotel.hotelBookingLimit;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.xlem.dao.HotelBookingLimit;
import com.xlem.dao.HotelRoomType;

import ws.pojo.request.AvailableRoomInfoRequest;
import ws.pojo.response.HotelBookingLimitResponse;
import common.FullCalBean;
import common.service.BaseService;

public interface HotelBookingLimitService extends BaseService<HotelBookingLimit> {

	void save(Collection<HotelBookingLimit> coll);

	List<FullCalBean> findByFull(Date startDate, Date endDate, HotelBookingLimit hotelBookingLimit);

	void saveFull(FullCalBean fullCalBean, HotelBookingLimit hotelBookingLimit);
	
	List<HotelBookingLimitResponse> updateFromXrws(String ip,AvailableRoomInfoRequest arir);

	public void updateLockRoomFromXrws(FullCalBean fullCalBean,HotelRoomType hotelRoomType);
}