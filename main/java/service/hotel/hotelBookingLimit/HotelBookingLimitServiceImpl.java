package service.hotel.hotelBookingLimit;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.AvailableRoomInfo;
import com.xlem.dao.Hotel;
import com.xlem.dao.HotelActivityLimit;
import com.xlem.dao.HotelBookingLimit;
import com.xlem.dao.HotelPriceLimitDetail;
import com.xlem.dao.HotelRoomType;

import ws.ReturnInfo;
import ws.hotel.client.HotelIndentClient;
import ws.hotel.comm.HotelCode;
import ws.hotel.comm.IpMap;
import ws.pojo.request.AvailableRoomInfoRequest;
import ws.pojo.response.HotelBookingLimitResponse;

import common.BusinessException;
import common.CommonMethod;
import common.FullCalBean;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.hotel.hotelActivityLimit.HotelActivityLimitDao;
import dao.hotel.hotelBookingLimit.HotelBookingLimitDao;
import dao.hotel.hotelPriceLimitDetail.HotelPriceLimitDetailDao;
import dao.hotel.roomType.RoomTypeDao;

@Service("hotelBookingLimitService")
public class HotelBookingLimitServiceImpl extends BaseServiceImpl<HotelBookingLimit>
		implements HotelBookingLimitService {
	@Autowired
	private HotelBookingLimitDao hotelBookingLimitDao;
	@Autowired
	private HotelPriceLimitDetailDao hotelPriceLimitDetailDao;
	@Autowired
	private RoomTypeDao roomTypeDao;
	@Autowired
	private HotelActivityLimitDao hotelActivityLimitDao;
	
	@Override
	@Resource(name = "hotelBookingLimitDao")
	protected void initBaseDAO(BaseDao<HotelBookingLimit> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<HotelBookingLimit> findPageByCriteria(
			PaginationSupport<HotelBookingLimit> ps, HotelBookingLimit t) {
		DetachedCriteria dc = DetachedCriteria.forClass(HotelBookingLimit.class);
		return hotelBookingLimitDao.findPageByCriteria(ps,Order.desc("limitId"), dc);
	}

	@Override
	public void save(HotelBookingLimit t) {
		t.setLimitId(getNextKey("Hotel_Booking_Limit".toUpperCase(), 1));
		hotelBookingLimitDao.save(t);
	}

	@Override
	public JsonPager<HotelBookingLimit> findJsonPageByCriteria(
			JsonPager<HotelBookingLimit> jp, HotelBookingLimit t) {
		DetachedCriteria dc = DetachedCriteria.forClass(HotelBookingLimit.class);
		dc.createAlias("hotelRoomType", "hotelRoomType");
		dc.add(Property.forName("hotelRoomType.roomTypeId").eq(t.getHotelRoomType().getRoomTypeId()));
		dc.addOrder(Order.desc("startDate"));
		return hotelBookingLimitDao.findJsonPageByCriteria(jp, dc);
	}

	@Override
	public List<FullCalBean> findByFull(Date startDate, Date endDate,HotelBookingLimit t) {
		DetachedCriteria dc = DetachedCriteria.forClass(HotelPriceLimitDetail.class);
//		dc.createAlias("hotelRoomType", "hotelRoomType");
		dc.add(Property.forName("hotelRoomType.roomTypeId").eq(t.getHotelRoomType().getRoomTypeId()));
		dc.add(Property.forName("useDate").ge(startDate));
		dc.add(Property.forName("useDate").le(endDate));
		dc.addOrder(Order.desc("useDate"));
		List<HotelPriceLimitDetail> limitDetails  =hotelPriceLimitDetailDao.findByCriteria(dc);
		
		Map<Timestamp,HotelPriceLimitDetail> hpldMap = new HashMap<Timestamp,HotelPriceLimitDetail>();
		if(!limitDetails.isEmpty()){
			for(HotelPriceLimitDetail hp : limitDetails){
				hpldMap.put(Timestamp.valueOf(hp.getUseDate().toString()), hp);
			}
		}
		
		List<Timestamp> timeList = CommonMethod.getTimeStampList(CommonMethod.string2Time1(CommonMethod.dateToString(startDate)), 
					CommonMethod.string2Time1(CommonMethod.dateToString(endDate)));
		
		List<FullCalBean>  calList = new ArrayList<FullCalBean>();
		for(Timestamp ti : timeList){
			HotelPriceLimitDetail limit = hpldMap.get(ti);
			FullCalBean cal = new FullCalBean();
			if(limit!=null){
				cal.setId(limit.getDetailId().toString());
				cal.setTitle(limit.getAmount()==null?"-":limit.getAmount().toString());
				cal.setStart(CommonMethod.toStartTime(Timestamp.valueOf(limit.getUseDate().toString())));
			}else{
				cal.setId("");
				cal.setTitle("-");
				cal.setStart(CommonMethod.toStartTime(ti));
			}
			calList.add(cal);
		}
		
//		for (HotelPriceLimitDetail limit : limitDetails) {
//			FullCalBean cal = new FullCalBean();
//			if(limit.getAmount() != null){
//				cal.setId(limit.getDetailId().toString());
//				cal.setTitle(limit.getAmount()==null?null:limit.getAmount().toString());
//				cal.setStart(CommonMethod.toStartTime(limit.getUseDate()));
////			cal.setEnd(CommonMethod.toEndTime(limit.getEndDate()));
//				calList.add(cal);
//			}
//		}
		return calList;
	}
	

	@Override
	public void saveFull(FullCalBean fullCalBean,HotelBookingLimit hotelBookingLimit) {
		
		DetachedCriteria dc = DetachedCriteria.forClass(HotelPriceLimitDetail.class);
		dc.add(Property.forName("useDate").ge(CommonMethod.toStartTime(fullCalBean.getStart())));
		dc.add(Property.forName("useDate").le(CommonMethod.toEndTime(fullCalBean.getEnd())));
		dc.add(Property.forName("hotelRoomType.roomTypeId").eq(hotelBookingLimit.getHotelRoomType().getRoomTypeId()));
		dc.addOrder(Order.asc("useDate"));
		List<HotelPriceLimitDetail> hpldList = hotelPriceLimitDetailDao.findByCriteria(dc);
		
		Map<Timestamp,HotelPriceLimitDetail> hpldMap = new HashMap<Timestamp,HotelPriceLimitDetail>();
		if(!hpldList.isEmpty()){
			for(HotelPriceLimitDetail hp : hpldList){
				hpldMap.put(Timestamp.valueOf(hp.getUseDate().toString()), hp);
			}
		}
		
		List<Timestamp> tList = CommonMethod.getTimeStampList(fullCalBean.getStart(), fullCalBean.getEnd());
		
		//写酒店接口start
		
		//写酒店接口end
		
		for(Timestamp t : tList){
			
			HotelPriceLimitDetail hp = hpldMap.get(t);
			
			int remainAmount = Integer.parseInt(fullCalBean.getTitle()) - hotelPriceLimitDetailDao.findBookingAmount(t,hotelBookingLimit.getHotelRoomType(),"",null);
			if(remainAmount<0){
				remainAmount = 0;
			}
			
			if (hp!=null) {
				hp.setAmount(Integer.parseInt(fullCalBean.getTitle()));
				hp.setRemainAmount(remainAmount);
				hotelPriceLimitDetailDao.update(hp);
				
			}else{
				HotelPriceLimitDetail limit = new HotelPriceLimitDetail();
				limit.setDetailId(getNextKey("HOTEL_PRICE_LIMIT_DETAIL", 1));
				limit.setHotelRoomType(hotelBookingLimit.getHotelRoomType());
				limit.setAmount(Integer.parseInt(fullCalBean.getTitle()));
				limit.setRemainAmount(remainAmount);
				limit.setUseDate(t);
				hotelPriceLimitDetailDao.save(limit);
			}
		}
		
		//写酒店接口start
		this.updateLockRoomFromXrws(fullCalBean, hotelBookingLimit.getHotelRoomType());
		//写酒店接口end
		
	}
	
	@Override
	public void updateLockRoomFromXrws(FullCalBean fullCalBean,HotelRoomType hotelRoomType){
		
		//房上限
		DetachedCriteria dc = DetachedCriteria.forClass(HotelPriceLimitDetail.class);
		dc.add(Property.forName("useDate").ge(CommonMethod.toStartTime(fullCalBean.getStart())));
		dc.add(Property.forName("useDate").le(CommonMethod.toEndTime(fullCalBean.getEnd())));
		dc.add(Property.forName("hotelRoomType.roomTypeId").eq(hotelRoomType.getRoomTypeId()));
		dc.addOrder(Order.asc("useDate"));
		List<HotelPriceLimitDetail> hpldList = hotelPriceLimitDetailDao.findByCriteria(dc);
		
		Map<Timestamp,HotelPriceLimitDetail> hpldMap = new HashMap<Timestamp,HotelPriceLimitDetail>();
		if(!hpldList.isEmpty()){
			for(HotelPriceLimitDetail hp : hpldList){
				hpldMap.put(Timestamp.valueOf(hp.getUseDate().toString()), hp);
			}
		}
		
		//活动房上限
		DetachedCriteria dcAct = DetachedCriteria.forClass(HotelActivityLimit.class);
		dcAct.add(Property.forName("useDate").ge(CommonMethod.toStartTime(fullCalBean.getStart())));
		dcAct.add(Property.forName("useDate").le(CommonMethod.toEndTime(fullCalBean.getEnd())));
		dcAct.add(Property.forName("hotelRoomType.roomTypeId").eq(hotelRoomType.getRoomTypeId()));
		dcAct.addOrder(Order.asc("useDate"));
		List<HotelActivityLimit> hpldActList = hotelActivityLimitDao.findByCriteria(dcAct);
		
		Map<Timestamp,HotelActivityLimit> hpldActMap = new HashMap<Timestamp,HotelActivityLimit>();
		if(!hpldActList.isEmpty()){
			for(HotelActivityLimit hp : hpldActList){
				
				HotelActivityLimit hpTemp = hpldActMap.get(hp.getUseDate());
				if(hpTemp==null){
					hpldActMap.put(Timestamp.valueOf(hp.getUseDate().toString()), hp);
				}else{
					hp.setAmount(hp.getAmount()+hpTemp.getAmount());
					hpldActMap.put(Timestamp.valueOf(hp.getUseDate().toString()), hp);
				}
				
			}
		}
		
		List<Timestamp> tList = CommonMethod.getTimeStampList(fullCalBean.getStart(), fullCalBean.getEnd());
		
		List<HotelBookingLimit> bookingLimits = new ArrayList<HotelBookingLimit>();
		
		DetachedCriteria dc1 = DetachedCriteria.forClass(HotelRoomType.class);
		dc1.createAlias("hotel", "hotel");
		dc1.add(Property.forName("roomTypeId").eq(hotelRoomType.getRoomTypeId()));
		List<HotelRoomType> hotelRoomTypes = roomTypeDao.findByCriteria(dc1);
		HotelRoomType tHotelRoomType1 = null;
		if(hotelRoomTypes!=null && !hotelRoomTypes.isEmpty()){
			tHotelRoomType1 = hotelRoomTypes.get(0);
		}
		else{
			throw new BusinessException("写酒店订单出错:无房型数据!");
		}
		
		for(Timestamp t : tList){
			
			HotelPriceLimitDetail hp = hpldMap.get(t);
			HotelActivityLimit hpAct = hpldActMap.get(t);
			
			if (hp!=null) {
				//写酒店接口start
				HotelBookingLimit tHotelBookingLimit = new HotelBookingLimit();
				tHotelBookingLimit.setLimitId(hp.getDetailId());
				tHotelBookingLimit.setStatus("11");
				tHotelBookingLimit.setAmount(hp.getAmount());
				tHotelBookingLimit.setSynchroState("02");
				HotelRoomType tHotelRoomType = new HotelRoomType();
				tHotelRoomType.setHotel(tHotelRoomType1.getHotel());
				tHotelRoomType.setRoomTypeCode(tHotelRoomType1.getRoomTypeCode());
				tHotelBookingLimit.setHotelRoomType(tHotelRoomType);
				tHotelBookingLimit.setStartDate(hp.getUseDate());
				Timestamp ts = new Timestamp(hp.getUseDate().getTime());
				ts = CommonMethod.toEndTime(ts);
				tHotelBookingLimit.setEndDate(ts);
				bookingLimits.add(tHotelBookingLimit);
				//写酒店接口end
				
			}
			if (hpAct!=null) {
//				HotelPriceLimitDetail limit = new HotelPriceLimitDetail();
//				limit.setDetailId(getNextKey("HOTEL_PRICE_LIMIT_DETAIL", 1));
//				limit.setHotelRoomType(hotelRoomType);
//				limit.setAmount(0);
//				limit.setRemainAmount(0);
//				limit.setUseDate(t);
//				hotelPriceLimitDetailDao.save(limit);
				//写酒店接口start
				HotelBookingLimit tHotelBookingLimit = new HotelBookingLimit();
				tHotelBookingLimit.setLimitId(hpAct.getLimitId());
				tHotelBookingLimit.setStatus("11");
				tHotelBookingLimit.setAmount(hpAct.getAmount());
				tHotelBookingLimit.setSynchroState("02");
				HotelRoomType tHotelRoomType = new HotelRoomType();
				tHotelRoomType.setHotel(tHotelRoomType1.getHotel());
				tHotelRoomType.setRoomTypeCode(tHotelRoomType1.getRoomTypeCode());
				tHotelBookingLimit.setHotelRoomType(tHotelRoomType);
				tHotelBookingLimit.setStartDate(hpAct.getUseDate());
				Timestamp ts = new Timestamp(hpAct.getUseDate().getTime());
				ts = CommonMethod.toEndTime(ts);
				tHotelBookingLimit.setEndDate(ts);
				tHotelBookingLimit.setRemark("ACT");//表示活动房
				
				bookingLimits.add(tHotelBookingLimit);
				//写酒店接口end
				
			}
		}
		
		String hotelCode = tHotelRoomType1.getHotel().getHotelCode();
		HotelIndentClient client = new HotelIndentClient();
		ReturnInfo ri = client.bookingLimit(bookingLimits, HotelCode.valueOf(hotelCode));
		if("-1".equals(ri.getResult())){//失败
			throw new BusinessException(ri.getError());
		}
		else{//成功
			
		}
		
	}
	
	
	
	@Override
	public List<HotelBookingLimitResponse> updateFromXrws(String ip,AvailableRoomInfoRequest arir){
		
		List<String> hotelCode = IpMap.findHotelForIp(ip);
		List<AvailableRoomInfo> aris = arir.getDetails();
		List<HotelBookingLimitResponse> bookingLimits = new ArrayList<HotelBookingLimitResponse>();
		
		//订房上限改为平台锁房和可用房之和
		for(AvailableRoomInfo ari : aris){
			//可用数量
			int avlquan = ari.getAvlquan();
			if(avlquan>=0){
				continue;
			}
			
			int limitNums = 0;
			
			DetachedCriteria dcAct = DetachedCriteria.forClass(HotelActivityLimit.class);
			dcAct.createAlias("hotelRoomType", "hotelRoomType");
			dcAct.createAlias("hotelRoomType.hotel", "h");
			dcAct.add(Property.forName("useDate").eq(CommonMethod.toStartTime(ari.getCurrentDate())));
			dcAct.add(Property.forName("hotelRoomType.roomTypeCode").eq(ari.getRoomTypeCode().trim()));
			dcAct.add(Restrictions.in("h.hotelCode", hotelCode));
			dcAct.addOrder(Order.asc("useDate"));//按时间排序
			List<HotelActivityLimit> halList = hotelActivityLimitDao.findByCriteria(dcAct);
			
			for(HotelActivityLimit hal : halList){
				limitNums += hal.getAmount();
			}
			
			
			DetachedCriteria dc = DetachedCriteria.forClass(HotelPriceLimitDetail.class);
			dc.createAlias("hotelRoomType", "hotelRoomType");
			dc.createAlias("hotelRoomType.hotel", "h");
			dc.add(Property.forName("useDate").eq(CommonMethod.toStartTime(ari.getCurrentDate())));
			dc.add(Property.forName("hotelRoomType.roomTypeCode").eq(ari.getRoomTypeCode().trim()));
			dc.add(Restrictions.in("h.hotelCode", hotelCode));
			dc.addOrder(Order.asc("hotelRoomType.inputer"));//按特殊房优先排序
			List<HotelPriceLimitDetail> hpldList = hotelPriceLimitDetailDao.findByCriteria(dc);
//			System.out.print(ari.getRoomTypeCode()+";");
			
//			System.out.print("开始：");
			for(HotelPriceLimitDetail hpld : hpldList){
				limitNums += hpld.getAmount();
			}
			
			if(limitNums==0){
				continue;
			}
			
			
			//如果房间被占，优先减活动房数量
			for(HotelActivityLimit hal : halList){
				
				if(avlquan==0){
					break;
				}
				
				int oldAmount = hal.getAmount();
				if(oldAmount==0){
					continue;
				}
				
				int amount = oldAmount + avlquan;

				if(amount<0){
					avlquan = amount;
					amount = 0;
				}else{
					avlquan = 0;
				}
				
				int remainAmount = amount - hotelPriceLimitDetailDao.findBookingAmount(Timestamp.valueOf(hal.getUseDate().toString()), hal.getHotelRoomType(),"act",hal.getHotelActivity().getHotelActivityId());
				if(remainAmount<0){
					remainAmount = 0;
				}
				
				hal.setAmount(amount);
				hal.setRemainAmount(remainAmount);
				hotelActivityLimitDao.update(hal);
				
				HotelBookingLimitResponse tHotelBookingLimit = new HotelBookingLimitResponse();
				tHotelBookingLimit.setLimitId(hal.getLimitId());
				tHotelBookingLimit.setStatus("11");
				tHotelBookingLimit.setAmount(hal.getAmount());
				tHotelBookingLimit.setSynchroState("02");
				HotelRoomType tHotelRoomType = new HotelRoomType();
				tHotelRoomType.setHotel(hal.getHotelRoomType().getHotel());
				tHotelRoomType.setRoomTypeCode(hal.getHotelRoomType().getRoomTypeCode());
				tHotelRoomType.setHotelPriceLimitDetails(null);
				tHotelBookingLimit.setHotelRoomType(tHotelRoomType);
				tHotelBookingLimit.setStartDate(Timestamp.valueOf(hal.getUseDate().toString()));
				Timestamp ts = new Timestamp(hal.getUseDate().getTime());
				ts = CommonMethod.toEndTime(ts);
				tHotelBookingLimit.setEndDate(ts);
				tHotelBookingLimit.setRemark("ACT");//表示活动房
				bookingLimits.add(tHotelBookingLimit);
			}
			
			
			for(HotelPriceLimitDetail hpld : hpldList){
				
				if(avlquan==0){
					break;
				}
				
				int oldAmount = hpld.getAmount();
				if(oldAmount==0){
					continue;
				}
//				System.out.print("hpld.getAmount():"+hpld.getAmount()+";");
//				System.out.print("avlquan:"+avlquan+";");
				int amount = oldAmount + avlquan;
//				System.out.print("amount:"+amount+";");
				if(amount<0){
					avlquan = amount;
					amount = 0;
				}else{
					avlquan = 0;
				}
//				System.out.print("amount:"+amount+";");
				int remainAmount = amount - hotelPriceLimitDetailDao.findBookingAmount(Timestamp.valueOf(hpld.getUseDate().toString()), hpld.getHotelRoomType(),"",null);
				if(remainAmount<0){
					remainAmount = 0;
				}
//				System.out.println("完");
				hpld.setAmount(amount);
				hpld.setRemainAmount(remainAmount);
				hotelPriceLimitDetailDao.update(hpld);
				
				HotelBookingLimitResponse tHotelBookingLimit = new HotelBookingLimitResponse();
				tHotelBookingLimit.setLimitId(hpld.getDetailId());
				tHotelBookingLimit.setStatus("11");
				tHotelBookingLimit.setAmount(hpld.getAmount());
				tHotelBookingLimit.setSynchroState("02");
				HotelRoomType tHotelRoomType = new HotelRoomType();
				tHotelRoomType.setHotel(hpld.getHotelRoomType().getHotel());
				tHotelRoomType.setRoomTypeCode(hpld.getHotelRoomType().getRoomTypeCode());
				tHotelRoomType.setHotelPriceLimitDetails(null);
				tHotelBookingLimit.setHotelRoomType(tHotelRoomType);
				tHotelBookingLimit.setStartDate(Timestamp.valueOf(hpld.getUseDate().toString()));
				Timestamp ts = new Timestamp(hpld.getUseDate().getTime());
				ts = CommonMethod.toEndTime(ts);
				tHotelBookingLimit.setEndDate(ts);
				bookingLimits.add(tHotelBookingLimit);
			}
		}
		
		return bookingLimits;
	}
	
	@Override
	public void save(Collection<HotelBookingLimit> coll) {
		int i = 0;
		Hotel hotel = null;
		HotelRoomType hrt = null;
		List<HotelBookingLimit> bookingLimits = new ArrayList<HotelBookingLimit>();
		for (HotelBookingLimit HotelBookingLimit : coll) {
			if(i==0){
				DetachedCriteria dc = DetachedCriteria.forClass(HotelRoomType.class);
				dc.createAlias("hotel", "hotel");
				dc.add(Property.forName("roomTypeId").eq(HotelBookingLimit.getHotelRoomType().getRoomTypeId()));
				List<HotelRoomType> hrts = roomTypeDao.findByCriteria(dc);
				if(hrts!=null && !hrts.isEmpty()){
					hrt = hrts.get(0);
					hotel = hrts.get(0).getHotel();
				}
				if(hotel==null || hotel.getHotelCode()==null){
					throw new BusinessException("无当前酒店信息！");
				}
				i++;
			}
			
			Timestamp startDate = Timestamp.valueOf(HotelBookingLimit.getStartDate().toString());
			Timestamp endDate = Timestamp.valueOf(HotelBookingLimit.getEndDate().toString());
			
			if (HotelBookingLimit.getLimitId() == null) {
				HotelBookingLimit.setLimitId(getNextKey("Hotel_Booking_Limit", 1));
				HotelBookingLimit.setInputer(getCurrentUserId());
				HotelBookingLimit.setInputTime(CommonMethod.getTimeStamp());
				HotelBookingLimit.setSynchroState("00");
				hotelBookingLimitDao.save(HotelBookingLimit);
				HotelBookingLimit.setHotelRoomType(hrt);
				bookingLimits.add(HotelBookingLimit);
			} else {
				HotelBookingLimit h = hotelBookingLimitDao.findById(HotelBookingLimit.getLimitId());
				if(startDate.after(h.getStartDate())){
					startDate = Timestamp.valueOf(h.getStartDate().toString());
				}
				if(endDate.before(h.getEndDate())){
					endDate = Timestamp.valueOf(h.getEndDate().toString());
				}
				
				h.setStartDate(HotelBookingLimit.getStartDate());
				h.setEndDate(HotelBookingLimit.getEndDate());
				h.setAmount(HotelBookingLimit.getAmount());
				h.setStatus(HotelBookingLimit.getStatus());
				h.setRemark(HotelBookingLimit.getRemark());
				
				h.setUpdater(getCurrentUserId()); 
				h.setUpdateTime(CommonMethod.getTimeStamp());
				HotelBookingLimit.setSynchroState("02");
				h.setSynchroState("02");
				hotelBookingLimitDao.update(h);
				h.setHotelRoomType(hrt);
				bookingLimits.add(h);
			}
			
			DetachedCriteria dc = DetachedCriteria.forClass(HotelPriceLimitDetail.class);
			dc.add(Property.forName("useDate").ge(CommonMethod.toStartTime(startDate)));
			dc.add(Property.forName("useDate").le(CommonMethod.toEndTime(endDate)));
			dc.add(Property.forName("hotelRoomType.roomTypeId").eq(HotelBookingLimit.getHotelRoomType().getRoomTypeId()));
			dc.addOrder(Order.asc("useDate"));
			List<HotelPriceLimitDetail> hpldList = hotelPriceLimitDetailDao.findByCriteria(dc);
			
			Map<Timestamp,HotelPriceLimitDetail> hpldMap = new HashMap<Timestamp,HotelPriceLimitDetail>();
			if(!hpldList.isEmpty()){
				for(HotelPriceLimitDetail hp : hpldList){
					hpldMap.put(Timestamp.valueOf(hp.getUseDate().toString()), hp);
				}
			}
			
			List<Timestamp> tList = CommonMethod.getTimeStampList(startDate, endDate);
			
			for(Timestamp t : tList){
				
				HotelPriceLimitDetail hp = hpldMap.get(t);
				
				Timestamp sDate = CommonMethod.toStartTime(Timestamp.valueOf(HotelBookingLimit.getStartDate().toString()));
				Timestamp eDate = CommonMethod.toEndTime(Timestamp.valueOf(HotelBookingLimit.getEndDate().toString()));
				
				if(!((sDate.before(t) || sDate.equals(t)) && (eDate.after(t) || eDate.equals(t)))){
					if(hp!=null){
						hp.setAmount(null);
						hotelPriceLimitDetailDao.update(hp);
					}
					continue;
				}
				
				String sta = HotelBookingLimit.getStatus();
				
				int amount = HotelBookingLimit.getAmount();
				
				int remainAmount = amount - hotelPriceLimitDetailDao.findBookingAmount(t,HotelBookingLimit.getHotelRoomType(),"",null);
				if(remainAmount<0){
					remainAmount = 0;
				}

				if(hp==null){
					if("11".equals(sta)){//需要添加
						hp=new HotelPriceLimitDetail(getNextKey("HOTEL_PRICE_LIMIT_DETAIL", 1), 
													HotelBookingLimit.getHotelRoomType(), null, amount, t, null, remainAmount, "11", null,null, null,null);
						hotelPriceLimitDetailDao.save(hp);
					}
				}else{
					if("11".equals(sta)){//需要修改值
						hp.setRemainAmount(remainAmount);
						hp.setAmount(amount);
						
					}else{//需要将值修改为null
						hp.setRemainAmount(0);
						hp.setAmount(null);
					}
					hotelPriceLimitDetailDao.update(hp);
				}
				
			}
			
		}
		
//		HotelIndentClient client = new HotelIndentClient();
//		ReturnInfo ri = client.bookingLimit(bookingLimits, HotelCode.valueOf(hotel.getHotelCode()));
//		if("-1".equals(ri.getResult())){//失败
//			throw new BusinessException(ri.getError());
//		}
//		else{//成功
//		}
	}


}