package service.hotel.hotelPrice;
import java.util.Collection;

import com.xlem.dao.HotelPrice;

import common.service.BaseService;

public interface HotelPriceService extends BaseService<HotelPrice> {

	void save(Collection<HotelPrice> coll);

}