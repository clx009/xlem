package service.hotel.hotelPrice;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.Hotel;
import com.xlem.dao.HotelBookingLimit;
import com.xlem.dao.HotelPrice;
import com.xlem.dao.HotelPriceLimitDetail;
import com.xlem.dao.HotelRoomType;

import ws.ReturnInfo;
import ws.hotel.client.HotelIndentClient;
import ws.hotel.comm.HotelCode;

import common.BusinessException;
import common.CommonMethod;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.hotel.hotel.HotelDao;
import dao.hotel.hotelPrice.HotelPriceDao;
import dao.hotel.hotelPriceLimitDetail.HotelPriceLimitDetailDao;
import dao.hotel.roomType.RoomTypeDao;

@Service("hotelPriceService")
public class HotelPriceServiceImpl extends BaseServiceImpl<HotelPrice>
		implements HotelPriceService {
	@Autowired
	private HotelPriceDao hotelPriceDao;
	@Autowired
	private HotelPriceLimitDetailDao hotelPriceLimitDetailDao;
	@Autowired
	private HotelDao hotelDao;
	@Autowired
	private RoomTypeDao roomTypeDao;
	
	@Override
	@Resource(name = "hotelPriceDao")
	protected void initBaseDAO(BaseDao<HotelPrice> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<HotelPrice> findPageByCriteria(
			PaginationSupport<HotelPrice> ps, HotelPrice t) {
		DetachedCriteria dc = DetachedCriteria.forClass(HotelPrice.class);
		return hotelPriceDao.findPageByCriteria(ps,Order.desc("hotelPriceId"), dc);
	}

	@Override
	public void save(HotelPrice t) {
		t.setHotelPriceId(getNextKey("Hotel_Price".toUpperCase(), 1));
		hotelPriceDao.save(t);
	}

	@Override
	public JsonPager<HotelPrice> findJsonPageByCriteria(
			JsonPager<HotelPrice> jp, HotelPrice t) {
		DetachedCriteria dc = DetachedCriteria.forClass(HotelPrice.class);
		dc.createAlias("hotelRoomType", "hotelRoomType");
		dc.add(Property.forName("hotelRoomType.roomTypeId").eq(t.getHotelRoomType().getRoomTypeId()));
		dc.addOrder(Order.desc("startDate"));
		return hotelPriceDao.findJsonPageByCriteria(jp, dc);
	}

	@Override
	public void save(Collection<HotelPrice> coll) {
		System.out.println(coll);
		for (HotelPrice HotelPrice : coll) {
			
			Timestamp startDate = Timestamp.valueOf(HotelPrice.getStartDate().toString());
			Timestamp endDate = Timestamp.valueOf(HotelPrice.getEndDate().toString());
			
			if (HotelPrice.getHotelPriceId() == null) {
				HotelPrice.setHotelPriceId(getNextKey("Hotel_Price", 1));
				HotelPrice.setInputer(getCurrentUserId());
				HotelPrice.setInputTime(CommonMethod.getTimeStamp());
				hotelPriceDao.save(HotelPrice);
			} else {
				HotelPrice h = hotelPriceDao.findById(HotelPrice.getHotelPriceId());
				
				if(startDate.after(h.getStartDate())){
					startDate = Timestamp.valueOf(h.getStartDate().toString());
				}
				if(endDate.before(h.getEndDate())){
					endDate = Timestamp.valueOf(h.getEndDate().toString());
				}
				
				h.setStartDate(HotelPrice.getStartDate());
				h.setEndDate(HotelPrice.getEndDate());
				h.setUnitPriceGroup(HotelPrice.getUnitPriceGroup());
				h.setWeekendFriPriceGroup(HotelPrice.getWeekendFriPriceGroup());
				h.setWeekendSatPriceGroup(HotelPrice.getWeekendSatPriceGroup());
				h.setUnitPrice(HotelPrice.getUnitPrice());
				h.setWeekendFriPrice(HotelPrice.getWeekendFriPrice());
				h.setWeekendSatPrice(HotelPrice.getWeekendSatPrice());
				h.setStatus(HotelPrice.getStatus());
				h.setRemark(HotelPrice.getRemark());
				
				h.setUpdater(getCurrentUserId());
				h.setUpdateTime(CommonMethod.getTimeStamp());
				hotelPriceDao.update(h);
			}
			
			DetachedCriteria dc = DetachedCriteria.forClass(HotelPriceLimitDetail.class);
			dc.add(Property.forName("useDate").ge(CommonMethod.toStartTime(startDate)));
			dc.add(Property.forName("useDate").le(CommonMethod.toEndTime(endDate)));
			dc.add(Property.forName("hotelRoomType.roomTypeId").eq(HotelPrice.getHotelRoomType().getRoomTypeId()));
			dc.addOrder(Order.asc("useDate"));
			List<HotelPriceLimitDetail> hpldList = hotelPriceLimitDetailDao.findByCriteria(dc);
			
			Map<Timestamp,HotelPriceLimitDetail> hpldMap = new HashMap<Timestamp,HotelPriceLimitDetail>();
			if(!hpldList.isEmpty()){
				for(HotelPriceLimitDetail hp : hpldList){
					hpldMap.put(Timestamp.valueOf(hp.getUseDate().toString()), hp);
				}
			}
			
			List<Timestamp> tList = CommonMethod.getTimeStampList(startDate, endDate);
			List<HotelBookingLimit> tHBLs = new ArrayList<HotelBookingLimit>();
			HotelRoomType tHRT = roomTypeDao.findById(HotelPrice.getHotelRoomType().getRoomTypeId());
			Hotel tHP = hotelDao.findById(tHRT.getHotel().getHotelId());
			String hotelCode = tHP.getHotelCode();
			
			for(Timestamp t : tList){
				
				HotelPriceLimitDetail hp = hpldMap.get(t);
				
				Timestamp sDate = CommonMethod.toStartTime(Timestamp.valueOf(HotelPrice.getStartDate().toString()));
				Timestamp eDate = CommonMethod.toEndTime(Timestamp.valueOf(HotelPrice.getEndDate().toString()));
				if(!((sDate.before(t) || sDate.equals(t)) && (eDate.after(t) || eDate.equals(t)))){
					if(hp!=null){
						hp.setUnitPrice(null);
						hp.setUnitPriceGroup(null);
						hotelPriceLimitDetailDao.update(hp);
					}
					continue;
				}
				String sta = HotelPrice.getStatus();
				
				double price = HotelPrice.getUnitPrice()==null?0:HotelPrice.getUnitPrice();
				double priceGroup = HotelPrice.getUnitPriceGroup()==null?0:HotelPrice.getUnitPriceGroup();
				
				int week = CommonMethod.getWeekByTimeStamp(t);
				if(week==6){//星期五
					price = HotelPrice.getWeekendFriPrice()==null?0:HotelPrice.getWeekendFriPrice();
					priceGroup = HotelPrice.getWeekendFriPriceGroup()==null?0:HotelPrice.getWeekendFriPriceGroup();
				}else if(week==7){//星期六
					price = HotelPrice.getWeekendSatPrice()==null?0:HotelPrice.getWeekendSatPrice();
					priceGroup = HotelPrice.getWeekendSatPriceGroup()==null?0:HotelPrice.getWeekendSatPriceGroup();
				}
				
				if(hp==null){
					HotelRoomType hrtTemp = HotelPrice.getHotelRoomType();
					HotelRoomType hrtTemp1 = HotelPrice.getHotelRoomType();
					if(hrtTemp.getRoomTypeCode()==null){
						hrtTemp = roomTypeDao.findById(hrtTemp.getRoomTypeId());
						hrtTemp1.setRoomTypeCode(hrtTemp.getRoomTypeCode());
						HotelPrice.setHotelRoomType(hrtTemp1);
					}
					if(hrtTemp1.getHotel()==null || hrtTemp1.getHotel().getHotelId()==null){
						hrtTemp1.setHotel(hrtTemp.getHotel());
					}
					if("11".equals(sta)){//需要添加
						hp=new HotelPriceLimitDetail(getNextKey("HOTEL_PRICE_LIMIT_DETAIL", 1), 
														HotelPrice.getHotelRoomType(), price, null, t, priceGroup, null,"11",null,null,null,null);
						hotelPriceLimitDetailDao.save(hp);
						
						//start
						HotelBookingLimit tHotelBookingLimit = new HotelBookingLimit();
						tHotelBookingLimit.setLimitId(hp.getDetailId());
						tHotelBookingLimit.setStatus("11");
						int amount = 0;
						if(hp.getAmount()==null || "".equals(hp.getAmount())){
							amount = 0;
						}
						else{
							amount = hp.getAmount();
						}
						tHotelBookingLimit.setAmount(amount);
						tHotelBookingLimit.setSynchroState("00");
						tHotelBookingLimit.setHotelRoomType(hp.getHotelRoomType());
						tHotelBookingLimit.setStartDate(hp.getUseDate());
						Timestamp ts = new Timestamp(hp.getUseDate().getTime());
						ts = CommonMethod.toEndTime(ts);
						tHotelBookingLimit.setEndDate(ts);
						tHBLs.add(tHotelBookingLimit);
						//end
					}
				}else{
					if("11".equals(sta)){//需要修改值
						hp.setUnitPrice(price);
						hp.setUnitPriceGroup(priceGroup);
					}else{//需要将值修改为null
						hp.setUnitPrice(null);
						hp.setUnitPriceGroup(null);
					}
					hotelPriceLimitDetailDao.update(hp);
				}
				
			}
			
			if(tHBLs!=null && !tHBLs.isEmpty()){
				HotelIndentClient client = new HotelIndentClient();
				ReturnInfo ri = client.bookingLimit(tHBLs,HotelCode.valueOf(hotelCode));
				if("-1".equals(ri.getResult())){//失败
					throw new BusinessException(ri.getError());
				}
				else{//成功
					
				}
			}
		}
	}

}