package service.hotel.hotelDetailHis;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.HotelDetailHis;

import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.hotel.hotelDetailHis.HotelDetailHisDao;

@Service("hotelDetailHisService")
public class HotelDetailHisServiceImpl extends BaseServiceImpl<HotelDetailHis>
		implements HotelDetailHisService {
	@Autowired
	private HotelDetailHisDao hotelDetailHisDao;

	@Override
	@Resource(name = "hotelDetailHisDao")
	protected void initBaseDAO(BaseDao<HotelDetailHis> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<HotelDetailHis> findPageByCriteria(
			PaginationSupport<HotelDetailHis> ps, HotelDetailHis t) {
		DetachedCriteria dc = DetachedCriteria.forClass(HotelDetailHis.class);
		return hotelDetailHisDao.findPageByCriteria(ps,
				Order.desc("hotelDetailHisId"), dc);
	}

	@Override
	public void save(HotelDetailHis t) {
		t.setHotelDetailHisId(getNextKey("Hotel_Detail_His".toUpperCase(), 1));
		hotelDetailHisDao.save(t);
	}

	@Override
	public JsonPager<HotelDetailHis> findJsonPageByCriteria(
			JsonPager<HotelDetailHis> jp, HotelDetailHis t) {
		DetachedCriteria dc = DetachedCriteria.forClass(HotelDetailHis.class);
		dc.createAlias("hotelRoomType", "hotelRoomType");
		if(t.getHotelIndent()!=null && t.getHotelIndent().getHotelIndentId()!=null){
			dc.add(Property.forName("hotelIndent.hotelIndentId").eq(t.getHotelIndent().getHotelIndentId()));
		}
		return hotelDetailHisDao.findJsonPageByCriteria(jp, dc);
	}
}