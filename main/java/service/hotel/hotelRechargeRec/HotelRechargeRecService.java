package service.hotel.hotelRechargeRec;
import com.xlem.dao.HotelRechargeRec;
import com.xlem.dao.TicketRechargeRec;

import common.action.JsonPager;
import common.service.BaseService;

public interface HotelRechargeRecService extends BaseService<HotelRechargeRec> {

	public JsonPager<TicketRechargeRec> findJsonPageByCriteriaByFin(
			JsonPager<TicketRechargeRec> jp, HotelRechargeRec t);
	
}