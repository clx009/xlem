package service.hotel.hotelRechargeRec;

import java.sql.Timestamp;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.HotelRechargeRec;
import com.xlem.dao.TicketRechargeRec;
import com.xlem.dao.TravserviceHome;

import common.CommonMethod;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;
import dao.comm.reg.baseTravelAgency.BaseTravelAgencyDao;
import dao.hotel.hotelRechargeRec.HotelRechargeRecDao;

@Service("hotelRechargeRecService")
public class HotelRechargeRecServiceImpl extends
		BaseServiceImpl<HotelRechargeRec> implements HotelRechargeRecService {
	@Autowired
	private HotelRechargeRecDao hotelRechargeRecDao;
	@Autowired
	private BaseTravelAgencyDao baseTravelAgencyDao;

	@Override
	@Resource(name = "hotelRechargeRecDao")
	protected void initBaseDAO(BaseDao<HotelRechargeRec> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<HotelRechargeRec> findPageByCriteria(
			PaginationSupport<HotelRechargeRec> ps, HotelRechargeRec t) {
		DetachedCriteria dc = DetachedCriteria.forClass(HotelRechargeRec.class);
		return hotelRechargeRecDao.findPageByCriteria(ps,
				Order.desc("hotelRechargeRecId"), dc);
	}

	@Override
	public void save(HotelRechargeRec t) {
		t.setHotelRechargeRecId(getNextKey("HotelRechargeRec".toUpperCase(), 1));
		hotelRechargeRecDao.save(t);
	}

	@Override
	public JsonPager<HotelRechargeRec> findJsonPageByCriteria(
			JsonPager<HotelRechargeRec> jp, HotelRechargeRec t) {
		DetachedCriteria dc = DetachedCriteria.forClass(HotelRechargeRec.class);
		dc.createAlias("hotelIndent.hotel", "hotelIndent.hotel",Criteria.LEFT_JOIN);
		dc.createAlias("hotelIndent", "hotelIndent",Criteria.LEFT_JOIN);
		dc.createAlias("baseRewardAccount", "baseRewardAccount",Criteria.LEFT_JOIN);
		dc.createAlias("baseRewardAccount.baseRewardType", "baseRewardAccount.baseRewardType",Criteria.LEFT_JOIN);
		if(t.getBaseRewardAccount()!=null && t.getBaseRewardAccount().getBaseRewardType()!=null && t.getBaseRewardAccount().getBaseRewardType().getRewardTypeId()!=null){
			dc.add(Property.forName("baseRewardAccount.baseRewardType").eq(t.getBaseRewardAccount().getBaseRewardType()));
		}
		if(t.getStartDate()!=null){
			dc.add(Property.forName("inputTime").ge(CommonMethod.toStartTime(t.getStartDate())));//开始时间>=当前时间
		}
		if(t.getEndDate()!=null){
			dc.add(Property.forName("inputTime").le(CommonMethod.toEndTime(Timestamp.valueOf(t.getEndDate().toString()))));//结束时间<=当前时间
		}
		if(t.getHotelIndent()!=null && t.getHotelIndent().getIndentCode()!=null && !t.getHotelIndent().getIndentCode().trim().isEmpty()){
			dc.add(Property.forName("hotelIndent.indentCode").like(t.getHotelIndent().getIndentCode(),MatchMode.ANYWHERE));
		}
		dc.add(Property.forName("baseTravelAgency").eq(baseTravelAgencyDao.findTravelAgencyByUser(getCurrentUserId())));
		dc.addOrder(Order.desc("inputTime"));
		return hotelRechargeRecDao.findJsonPageByCriteria(jp, dc);
	}
	
	@Override
	public JsonPager<TicketRechargeRec> findJsonPageByCriteriaByFin(
			JsonPager<TicketRechargeRec> jp, HotelRechargeRec t) {
		return hotelRechargeRecDao.findTicketRechargeRec(jp, t);
	}
}