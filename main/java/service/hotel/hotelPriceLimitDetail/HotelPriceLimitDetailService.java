package service.hotel.hotelPriceLimitDetail;
import java.util.Date;
import java.util.List;

import com.xlem.dao.HotelPriceLimitDetail;

import common.FullCalBean;
import common.service.BaseService;

public interface HotelPriceLimitDetailService extends BaseService<HotelPriceLimitDetail> {
	public List<FullCalBean> findByFull(Date start, Date end, HotelPriceLimitDetail t);
	
	public void saveFull(FullCalBean fullCalBean, HotelPriceLimitDetail hotelPriceLimitDetail);
	
	public void saveFullByDay(FullCalBean fullCalBean, HotelPriceLimitDetail hotelPriceLimitDetail);
	
	public void savePriceAudit(FullCalBean fullCalBean, HotelPriceLimitDetail hotelPriceLimitDetail);
}