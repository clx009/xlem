package service.hotel.hotelPriceLimitDetail;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.HotelPriceLimitDetail;
import com.xlem.dao.SysUser;

import common.CommonMethod;
import common.FullCalBean;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.hotel.hotelIndentDetail.HotelIndentDetailDao;
import dao.hotel.hotelPriceLimitDetail.HotelPriceLimitDetailDao;

@Service("hotelPriceLimitDetailService")
public class HotelPriceLimitDetailServiceImpl extends
		BaseServiceImpl<HotelPriceLimitDetail> implements
		HotelPriceLimitDetailService {
	@Autowired
	private HotelPriceLimitDetailDao hotelPriceLimitDetailDao;
	@Autowired
	private HotelIndentDetailDao hotelIndentDetailDao;
	
	@Override
	@Resource(name = "hotelPriceLimitDetailDao")
	protected void initBaseDAO(BaseDao<HotelPriceLimitDetail> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<HotelPriceLimitDetail> findPageByCriteria(
			PaginationSupport<HotelPriceLimitDetail> ps, HotelPriceLimitDetail t) {
		DetachedCriteria dc = DetachedCriteria
				.forClass(HotelPriceLimitDetail.class);
		return hotelPriceLimitDetailDao.findPageByCriteria(ps,
				Order.desc("hotelPriceLimitDetailId"), dc);
	}

	@Override
	public void save(HotelPriceLimitDetail t) {
		t.setDetailId(getNextKey("Hotel_Price_Limit_Detail".toUpperCase(), 1));
		hotelPriceLimitDetailDao.save(t);
	}

	@Override
	public JsonPager<HotelPriceLimitDetail> findJsonPageByCriteria(
			JsonPager<HotelPriceLimitDetail> jp, HotelPriceLimitDetail t) {
		String customerType = "01";
		t.setEndDate(CommonMethod.addDay(t.getEndDate(), -1));
		if(t.getCustomerType()!=null && !t.getCustomerType().equals("")){//如无客户类型设置类型
			customerType = t.getCustomerType();
		}
		else{
			SysUser sysUser = getSysUser();
			if("03".equals(sysUser.getUserType())){
				customerType = "01";
			}
			else{
				customerType = "02";
			}
		}
		
		
		DetachedCriteria dc = DetachedCriteria.forClass(HotelPriceLimitDetail.class);
		dc.createAlias("hotelRoomType", "hotelRoomType");
		if(t.getHotelRoomType()!=null && t.getHotelRoomType().getRoomTypeId()!=null){
			dc.add(Property.forName("hotelRoomType.roomTypeId").eq(t.getHotelRoomType().getRoomTypeId()));
		}
		if(t.getStartDate()!=null){
			dc.add(Property.forName("useDate").ge(t.getStartDate()));
		}
		if(t.getEndDate()!=null){
			dc.add(Property.forName("useDate").le(t.getEndDate()));
		}
		//价格审核过的
		dc.add(Property.forName("status").eq("11"));
//		if("01".equals(customerType)){//团客
//			dc.add(Property.forName("unitPriceGroup").isNotNull());
//		}
//		else{
//			dc.add(Property.forName("unitPrice").isNotNull());
//		}
		dc.addOrder(Order.asc("useDate"));
		jp = hotelPriceLimitDetailDao.findJsonPageByCriteria(jp, dc);
		
		//补充缺少的日期信息
		List<HotelPriceLimitDetail> details = jp.getRoot();
		Map<Timestamp, HotelPriceLimitDetail> detailsMap = new HashMap<Timestamp, HotelPriceLimitDetail>();
		List<Timestamp> timestamps = CommonMethod.getTimeStampList(t.getStartDate(), t.getEndDate());
		for (int i = 0; i < details.size(); i++) {
			HotelPriceLimitDetail hotelPriceLimitDetail = details.get(i);
			if(hotelPriceLimitDetail.getAmount()==null){
				hotelPriceLimitDetail.setAmount(0);
			}
			detailsMap.put(Timestamp.valueOf(hotelPriceLimitDetail.getUseDate().toString()), hotelPriceLimitDetail);
		}
		for (int i = 0; i < timestamps.size(); i++) {
			Timestamp timestamp = timestamps.get(i);
			HotelPriceLimitDetail hpld = detailsMap.get(timestamp);
			if(hpld==null){//不存在该日期的设置时,添加此项
				HotelPriceLimitDetail detail = new HotelPriceLimitDetail();
				detail.setHotelRoomType(t.getHotelRoomType());
				detail.setUnitPrice(0d);
				detail.setUnitPriceGroup(0d);
				detail.setUseDate(timestamp);
				detail.setBookingAmount(0);////初始化暂订和剩余房间数
				detail.setSurplusAmount(detail.getAmount());
				detail.setCustomerType(customerType);
				details.add(i, detail);
				detailsMap.put(timestamp, detail);
			}
			else{//初始化暂订和剩余房间数
				hpld.setBookingAmount(0);
				hpld.setCustomerType(customerType);
				hpld.setSurplusAmount(hpld.getAmount());
			}
		}
		
		//查询每天房间预订情况
		List<HotelPriceLimitDetail> details2 = hotelIndentDetailDao.findUsedRoomEveryday(t);
		for (int i = 0; i < details2.size(); i++) {
			HotelPriceLimitDetail roomDetail = details2.get(i);
			HotelPriceLimitDetail limitDetail = detailsMap.get(roomDetail.getUseDate());
			if(limitDetail!=null){
				limitDetail.setBookingAmount(roomDetail.getBookingAmount());
				
				int amount = limitDetail.getAmount() == null ? 0 : limitDetail.getAmount();
				int bAmount = limitDetail.getBookingAmount() == null ? 0 : limitDetail.getBookingAmount();
				
				limitDetail.setSurplusAmount((amount-bAmount)<0?0:(amount-bAmount));
			}
		}
		
		return jp;
	}
	
	@Override
	public List<FullCalBean> findByFull(Date start, Date end, HotelPriceLimitDetail t) {
		DetachedCriteria dc = DetachedCriteria.forClass(HotelPriceLimitDetail.class);
		dc.add(Property.forName("hotelRoomType.roomTypeId").eq(t.getHotelRoomType().getRoomTypeId()));
		dc.add(Property.forName("useDate").ge(start));
		dc.add(Property.forName("useDate").le(end));
		dc.addOrder(Order.desc("useDate"));
		List<HotelPriceLimitDetail> hotelPriceLimitDetail  = hotelPriceLimitDetailDao.findByCriteria(dc);
		
		Map<Timestamp,HotelPriceLimitDetail> tpMap = new HashMap<Timestamp,HotelPriceLimitDetail>();
		if(!hotelPriceLimitDetail.isEmpty()){
			for(HotelPriceLimitDetail tp : hotelPriceLimitDetail){
				tpMap.put(Timestamp.valueOf(tp.getUseDate().toString()), tp);
			}
		}
		
		List<Timestamp> timeList = CommonMethod.getTimeStampList(CommonMethod.string2Time1(CommonMethod.dateToString(start)), 
					CommonMethod.string2Time1(CommonMethod.dateToString(end)));
		
		List<FullCalBean>  calList = new ArrayList<FullCalBean>();
		for(Timestamp ti : timeList){
			HotelPriceLimitDetail hpld = tpMap.get(ti);
			FullCalBean cal = new FullCalBean();
			if(hpld!=null){
				cal.setId(hpld.getDetailId().toString());
				cal.setTitle(hpld.getUnitPrice()==null?"-":hpld.getUnitPrice().toString());
				cal.setTitle1(hpld.getUnitPriceGroup()==null?"-":hpld.getUnitPriceGroup().toString());
				String sText = hpld.getStatusText();
				if("-".equals(cal.getTitle())){
					sText = "未设置";
				}
				cal.setTitle2(sText);
				cal.setStart(CommonMethod.toStartTime(Timestamp.valueOf(hpld.getUseDate().toString())));
			}else{
				cal.setId("");
				cal.setTitle("-");
				cal.setTitle1("-");
				cal.setTitle2("未设置");
				cal.setStart(CommonMethod.toStartTime(ti));
			}
			calList.add(cal);
		}
		
		return calList;
	
	}
	
	@Override
	public void saveFull(FullCalBean fullCalBean, HotelPriceLimitDetail t) {
		DetachedCriteria dc = DetachedCriteria.forClass(HotelPriceLimitDetail.class);
		dc.add(Property.forName("useDate").ge(CommonMethod.toStartTime(fullCalBean.getStart())));
		dc.add(Property.forName("useDate").le(CommonMethod.toEndTime(fullCalBean.getEnd())));
		dc.add(Property.forName("hotelRoomType.roomTypeId").eq(t.getHotelRoomType().getRoomTypeId()));
		dc.addOrder(Order.asc("useDate"));
		List<HotelPriceLimitDetail> hotelPriceLimitDetail  = hotelPriceLimitDetailDao.findByCriteria(dc);
		
		Map<Timestamp,HotelPriceLimitDetail> tpMap = new HashMap<Timestamp,HotelPriceLimitDetail>();
		if(!hotelPriceLimitDetail.isEmpty()){
			for(HotelPriceLimitDetail tp : hotelPriceLimitDetail){
				tpMap.put(Timestamp.valueOf(tp.getUseDate().toString()), tp);
			}
		}
		
		List<Timestamp> tList = CommonMethod.getTimeStampList(fullCalBean.getStart(), fullCalBean.getEnd());
		
		Timestamp nowtime = CommonMethod.getTimeStamp();
		for(Timestamp tT : tList){
			HotelPriceLimitDetail tempTp = tpMap.get(tT);
			int week = CommonMethod.getWeekByTimeStamp(tT);
			double price = fullCalBean.getTitle_def()==null?0:Double.valueOf(fullCalBean.getTitle_def());
			double priceGroup = fullCalBean.getTitle1_def()==null?0:Double.valueOf(fullCalBean.getTitle1_def());
			if(week==6){//星期五
				price = fullCalBean.getTitle_fri()==null?0:Double.valueOf(fullCalBean.getTitle_fri());
				priceGroup = fullCalBean.getTitle1_fri()==null?0:Double.valueOf(fullCalBean.getTitle1_fri());
			}else if(week==7){//星期六
				price = fullCalBean.getTitle_sat()==null?0:Double.valueOf(fullCalBean.getTitle_sat());
				priceGroup = fullCalBean.getTitle1_sat()==null?0:Double.valueOf(fullCalBean.getTitle1_sat());
			}
			
			if (tempTp!=null) {
				
				double uP = tempTp.getUnitPrice()==null?0d:tempTp.getUnitPrice();
				double uPG = tempTp.getUnitPriceGroup()==null?0d:tempTp.getUnitPriceGroup();
				
				if(price<uP || priceGroup<uPG || tempTp.getStatus() == null){
					tempTp.setStatus("00");//改为未审核
				}
				
				tempTp.setUnitPrice(price);
				tempTp.setUnitPriceGroup(priceGroup);
				tempTp.setUpdater(getCurrentUserId());
				tempTp.setUpdateTime(nowtime);
				hotelPriceLimitDetailDao.update(tempTp);
			}else{
				HotelPriceLimitDetail tempTicketPrice = new HotelPriceLimitDetail();
				tempTicketPrice.setDetailId(getNextKey("HOTEL_PRICE_LIMIT_DETAIL", 1));
				tempTicketPrice.setHotelRoomType(t.getHotelRoomType());
				tempTicketPrice.setUnitPrice(price);
				tempTicketPrice.setUnitPriceGroup(priceGroup);
				tempTicketPrice.setUseDate(tT);
				tempTicketPrice.setStatus("00");//改为未审核
				tempTicketPrice.setInputer(getCurrentUserId());
				tempTicketPrice.setInputTime(nowtime);
				hotelPriceLimitDetailDao.save(tempTicketPrice);
			}
		}
		
	}
	
	@Override
	public void saveFullByDay(FullCalBean fullCalBean, HotelPriceLimitDetail t) {
		
		HotelPriceLimitDetail tempTp;
		if(fullCalBean.getId() == null || "".equals(fullCalBean.getId())){
			tempTp = null;
		}else{
			tempTp = hotelPriceLimitDetailDao.findById(Long.valueOf(fullCalBean.getId()));
		}
		
		Timestamp nowtime = CommonMethod.getTimeStamp();
		if (tempTp!=null) {
			double uPrice = Double.parseDouble(fullCalBean.getTitle());
			double uPriceGroup = Double.parseDouble(fullCalBean.getTitle1());
			
			double uP = tempTp.getUnitPrice()==null?0d:tempTp.getUnitPrice();
			double uPG = tempTp.getUnitPriceGroup()==null?0d:tempTp.getUnitPriceGroup();
			
			if(uPrice<uP|| uPriceGroup<uPG || tempTp.getStatus() == null){
				tempTp.setStatus("00");//改为未审核
			}
			
			tempTp.setUnitPrice(uPrice);
			tempTp.setUnitPriceGroup(uPriceGroup);
			tempTp.setUpdater(getCurrentUserId());
			tempTp.setUpdateTime(nowtime);
			hotelPriceLimitDetailDao.update(tempTp);
		}else{
			HotelPriceLimitDetail tempTicketPrice = new HotelPriceLimitDetail();
			tempTicketPrice.setDetailId(getNextKey("HOTEL_PRICE_LIMIT_DETAIL", 1));
			tempTicketPrice.setHotelRoomType(t.getHotelRoomType());
			tempTicketPrice.setUnitPrice(Double.parseDouble(fullCalBean.getTitle()));
			tempTicketPrice.setUnitPriceGroup(Double.parseDouble(fullCalBean.getTitle1()));
			tempTicketPrice.setUseDate(fullCalBean.getStart());
			tempTicketPrice.setStatus("00");//改为未审核
			tempTicketPrice.setInputer(getCurrentUserId());
			tempTicketPrice.setInputTime(nowtime);
			hotelPriceLimitDetailDao.save(tempTicketPrice);
		}
		
	}
	
	@Override
	public void savePriceAudit(FullCalBean fullCalBean, HotelPriceLimitDetail t) {
		DetachedCriteria dc = DetachedCriteria.forClass(HotelPriceLimitDetail.class);
		dc.add(Property.forName("useDate").ge(CommonMethod.toStartTime(fullCalBean.getStart())));
		dc.add(Property.forName("useDate").le(CommonMethod.toEndTime(fullCalBean.getEnd())));
		dc.add(Property.forName("hotelRoomType.roomTypeId").eq(t.getHotelRoomType().getRoomTypeId()));
		dc.addOrder(Order.asc("useDate"));
		List<HotelPriceLimitDetail> hotelPriceLimitDetail  = hotelPriceLimitDetailDao.findByCriteria(dc);
		
		Timestamp nowtime = CommonMethod.getTimeStamp();
		if(!hotelPriceLimitDetail.isEmpty()){
			for(HotelPriceLimitDetail tp : hotelPriceLimitDetail){
				tp.setStatus(t.getStatus());//改为未审核
				tp.setUpdater(getCurrentUserId());
				tp.setUpdateTime(nowtime);
				hotelPriceLimitDetailDao.update(tp);
			}
		}
		
	}
}