package service.hotel.hotelActivityDetail;
import java.util.Collection;
import java.util.List;

import com.xlem.dao.HotelActivityDetail;
import com.xlem.dao.HotelRoomType;

import common.service.BaseService;

public interface HotelActivityDetailService extends BaseService<HotelActivityDetail> {

	void save(Collection<HotelActivityDetail> coll, HotelActivityDetail t);
	
	/**
	 * 酒店活动房型列表查询		
	 */
	public List<HotelRoomType> findHotelActivityRoomList(HotelActivityDetail t);
}