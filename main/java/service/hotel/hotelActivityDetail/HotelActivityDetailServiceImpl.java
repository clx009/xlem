package service.hotel.hotelActivityDetail;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.HotelActivityDetail;
import com.xlem.dao.HotelRoomType;
import com.xlem.dao.SysUser;

import common.BusinessException;
import common.CommonMethod;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.hotel.hotelActivityDetail.HotelActivityDetailDao;
import dao.hotel.roomType.RoomTypeDao;
@Service("hotelActivityDetailService")
public class HotelActivityDetailServiceImpl extends BaseServiceImpl<HotelActivityDetail> implements HotelActivityDetailService {
@Autowired
private HotelActivityDetailDao hotelActivityDetailDao;
@Autowired
private RoomTypeDao roomTypeDao;
	@Override
	@Resource(name="hotelActivityDetailDao")
	protected void initBaseDAO(BaseDao<HotelActivityDetail> baseDao) {
		setBaseDao(baseDao);
	}
@Override
public PaginationSupport<HotelActivityDetail> findPageByCriteria(PaginationSupport<HotelActivityDetail> ps, HotelActivityDetail t) {
	DetachedCriteria dc = DetachedCriteria.forClass(HotelActivityDetail.class);
	return hotelActivityDetailDao.findPageByCriteria(ps, Order.desc("hotelActivityDetailId"), dc);
}
@Override
public void save(HotelActivityDetail t) {
	t.setHotelActivityDetailId(getNextKey("HotelActivityDetail".toUpperCase(), 1));
	hotelActivityDetailDao.save(t);
}
@Override
public JsonPager<HotelActivityDetail> findJsonPageByCriteria(
		JsonPager<HotelActivityDetail> jp, HotelActivityDetail t) {
	DetachedCriteria dc = DetachedCriteria.forClass(HotelActivityDetail.class);
	dc.createAlias("hotelActivity", "hotelActivity");
	dc.createAlias("hotelRoomType", "hotelRoomType");
	dc.createAlias("hotelRoomType.hotel", "hotel");
	if(t.getHotelActivity()!=null && t.getHotelActivity().getHotelActivityId() !=null){
		dc.add(Property.forName("hotelActivity.hotelActivityId").eq(t.getHotelActivity().getHotelActivityId()));
	}else{//没有传活动ID不查询出数据
		dc.add(Property.forName("hotelActivity.hotelActivityId").eq(-1L));
	}
	if("cType".equals(t.getCustomerType())){//需要分团散客查询
		SysUser su  = getSysUser();
		
		if(su.getBaseTravelAgency()!=null){//旅行社
			dc.add(Property.forName("customerType").eq("01"));
		}else if(su.getBaseTourist()!=null){//散客
			dc.add(Property.forName("customerType").eq("02"));
		}
		
	}else if(t.getCustomerType()!=null){
		dc.add(Property.forName("customerType").eq(t.getCustomerType()));
	}
	
	if(t.getHotelRoomType()!=null && t.getHotelRoomType().getHotel() !=null && t.getHotelRoomType().getHotel().getHotelId() !=null){
		dc.add(Property.forName("hotelRoomType.hotel.hotelId").eq(t.getHotelRoomType().getHotel().getHotelId()));
	}
	dc.addOrder(Order.desc("customerType"));
	dc.addOrder(Order.desc("hotel.hotelId"));
	dc.addOrder(Order.asc("hotelRoomType.roomTypeCode"));
	return hotelActivityDetailDao.findJsonPageByCriteria(jp, dc);
}

@Override
public void save(Collection<HotelActivityDetail> coll, HotelActivityDetail t) {
	
	long hotelActivityId = 0L;
	//验证订房天数是否有交叉
	List<HotelActivityDetail> actList = new ArrayList<HotelActivityDetail>();
	for (HotelActivityDetail act : coll) {
		
		int peopleNum = act.getPeopleNum() == null ? 0 : act.getPeopleNum();
		
		if(peopleNum==0){
			throw new BusinessException("房间人数必须大于0！请重新设置！","");
		}
		
		double peoplePrice = act.getPeoplePrice() == null ? 0D : act.getPeoplePrice();
		if(peoplePrice==0D){
			throw new BusinessException("单价必须大于0！请重新设置！","");
		}
		
		double roomPrice = act.getRoomPrice() == null ? 0D : act.getRoomPrice();
		if(roomPrice==0D){
			throw new BusinessException("单价必须大于0！请重新设置！","");
		}
		hotelActivityId = act.getHotelActivity().getHotelActivityId();
		actList.add(act);
	}
	
	DetachedCriteria dcHAD = DetachedCriteria.forClass(HotelActivityDetail.class);
	dcHAD.add(Property.forName("hotelActivity.hotelActivityId").eq(hotelActivityId));
	List<HotelActivityDetail> hadList = hotelActivityDetailDao.findByCriteria(dcHAD);
	
	List<HotelActivityDetail> actListTemp = new ArrayList<HotelActivityDetail>();
	Map<String,Long> havNewMap = new HashMap<String,Long>();
	for(int i=0;i<actList.size();i++){
		HotelActivityDetail actI = actList.get(i);
		if (actI.getHotelActivityDetailId() == null){
			actListTemp.add(actI);
		}else{
			long hadId = actI.getHotelActivityDetailId();
			for(HotelActivityDetail act : hadList){
				long hadIdT = act.getHotelActivityDetailId();
				if(hadId==hadIdT){
					
					act.setHotelRoomType(actI.getHotelRoomType());
					act.setCustomerType(actI.getCustomerType());
					act.setIndentMaxAmount(actI.getIndentMaxAmount());
					act.setIndentMinAmount(actI.getIndentMinAmount());
					act.setMaxDays(actI.getMaxDays());
					act.setMinDays(actI.getMinDays());
					act.setPeopleNum(actI.getPeopleNum());
					act.setPeoplePrice(actI.getPeoplePrice());
					act.setRoomPrice(actI.getRoomPrice());
					act.setUserMaxAmount(actI.getUserMaxAmount());
					act.setNew(true);
					
					actListTemp.add(act);
					havNewMap.put("id_"+hadId, hadId);
				}
			}
		}
			
	}
	
	for(HotelActivityDetail act : hadList){
		long hadIdT = act.getHotelActivityDetailId();
			
		if(havNewMap.get("id_"+hadIdT)==null){
			act.setNew(false);
			actListTemp.add(act);
		}	
			
	}
	
	
	for(int i=0;i<actListTemp.size();i++){
		
		HotelActivityDetail actI = actListTemp.get(i);
		
		long rtIdI = actI.getHotelRoomType().getRoomTypeId();
		String cusTypeI = actI.getCustomerType();
		int minDaysI = actI.getMinDays() == null ? 0 : actI.getMinDays();
		int maxDaysI = actI.getMaxDays() == null ? 0 : actI.getMaxDays();
		
		if(minDaysI>maxDaysI && maxDaysI>0){
			throw new BusinessException("订房最大天数必须大于或等于最小天数！请重新设置！","");
		}
		
		for(int j=0;j<actListTemp.size();j++){
			
			if(i==j){
				continue;
			}
			HotelActivityDetail actJ = actListTemp.get(j);
			long rtIdJ = actJ.getHotelRoomType().getRoomTypeId();
			String cusTypeJ = actJ.getCustomerType();
			int minDaysJ = actJ.getMinDays() == null ? 0 : actJ.getMinDays();
			int maxDaysJ = actJ.getMaxDays() == null ? 0 : actJ.getMaxDays();
			
			if(rtIdI==rtIdJ && cusTypeI.equals(cusTypeJ)){//同房型并且同客户类型
				
				if((minDaysJ<=minDaysI && minDaysI<=maxDaysJ) || (maxDaysJ==0 && minDaysJ<=minDaysI) 
						|| (minDaysJ<=maxDaysI && maxDaysI<=maxDaysJ) || (maxDaysJ==0 && minDaysJ<=maxDaysI)){//有交叉
					
					DetachedCriteria dc = DetachedCriteria.forClass(HotelRoomType.class);
					dc.createAlias("hotel", "hotel");
					dc.add(Property.forName("roomTypeId").eq(rtIdJ));
					List<HotelRoomType> hrtList = roomTypeDao.findByCriteria(dc);
					HotelRoomType hrt;
					if(hrtList!=null && hrtList.size()>0){
						hrt = hrtList.get(0);
					}else{
						hrt = new HotelRoomType();
					}
					
					throw new BusinessException("您设置的"+actJ.getCustomerTypeText()+"  "+hrt.getSelectShowName()+"订房天数出现交叉！请重新设置！","");
				}
				
			}
			
		}
		
	}
	
	for (HotelActivityDetail act : actListTemp) {
		if (act.getHotelActivityDetailId() == null) {
			act.setHotelActivityDetailId(getNextKey("HOTEL_ACTIVITY_DETAIL".toUpperCase(), 1));
			act.setInputer(getCurrentUserId());
			act.setInputTime(CommonMethod.getTimeStamp());
			act.setStatus("11");
			hotelActivityDetailDao.save(act);
		} else if(act.isNew()){
			act.setUpdater(getCurrentUserId());
			act.setUpdateTime(CommonMethod.getTimeStamp());
			hotelActivityDetailDao.update(act);
		}
	}
}

@Override
public List<HotelRoomType> findHotelActivityRoomList(HotelActivityDetail t) {
	List<HotelRoomType> hrt = new ArrayList<HotelRoomType>();
	
	DetachedCriteria dc = DetachedCriteria.forClass(HotelActivityDetail.class);
	dc.createAlias("hotelRoomType", "hotelRoomType");
	dc.createAlias("hotelRoomType.hotel", "hotel");
	if(t.getHotelActivity()!=null && t.getHotelActivity().getHotelActivityId() !=null){
		dc.add(Property.forName("hotelActivity.hotelActivityId").eq(t.getHotelActivity().getHotelActivityId()));
	}else{//没有传活动ID不查询出数据
		return hrt;
	}
	
	if("cType".equals(t.getCustomerType())){//需要分团散客查询
		SysUser su  = getSysUser();
		
		if(su.getBaseTravelAgency()!=null){//旅行社
			dc.add(Property.forName("customerType").eq("01"));
		}else if(su.getBaseTourist()!=null){//散客
			dc.add(Property.forName("customerType").eq("02"));
		}
		
	}
	
	if(t.getHotelRoomType()!=null && t.getHotelRoomType().getHotel() !=null && t.getHotelRoomType().getHotel().getHotelId() !=null){
		dc.add(Property.forName("hotelRoomType.hotel.hotelId").eq(t.getHotelRoomType().getHotel().getHotelId()));
	}
	
	dc.addOrder(Order.desc("customerType"));
	dc.addOrder(Order.desc("hotel.hotelId"));
	dc.addOrder(Order.asc("hotelRoomType.roomTypeCode"));
	
	List<HotelActivityDetail> hadList = hotelActivityDetailDao.findByCriteria(dc);
	
	Map<String,HotelRoomType> hrtMap = new HashMap<String,HotelRoomType>();
	for(HotelActivityDetail had : hadList){
		hrtMap.put(had.getHotelRoomType().getRoomTypeId().toString(), had.getHotelRoomType());
	}
	
	for(String key : hrtMap.keySet()){
		hrt.add(hrtMap.get(key));
	}
	
	return hrt;
}


}