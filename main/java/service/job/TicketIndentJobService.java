package service.job;

import java.util.List;

import com.xlem.dao.TicketBarcodeRec;
import com.xlem.dao.TicketIndent;

import common.service.BaseService;

public interface TicketIndentJobService extends BaseService<TicketIndent>{


	/**
	 * 向缓存写入订单信息
	 * @param indents 
	 */
	public void saveTicketWrite(List<TicketIndent> indents);

	/**
	 * 订单消费完毕检测
	 */
	public void saveTicketUse();
	
	
	/**
	 * 检索退款示成功的订单,向易宝发出退款请求,处理返回信息
	 */
	public void updateRefundIndents();
	
	/**
	 * 栓索在现场进行退票的记录，进行处理
	 * @param ticketBarcodeRec 
	 */
	public void saveRefundTicketBarcodeRec(TicketBarcodeRec ticketBarcodeRec);

	/**
	 * 查询申请退订订单
	 * @return
	 */
	public List<TicketIndent> findReturnIndents();

	/**
	 * 发出退订同步，并处理返回信息，完成退订同步
	 */
	public void updateReturnIndent(TicketIndent indent);
}
