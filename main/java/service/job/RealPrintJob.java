/**
 * 
 */
package service.job;

import java.sql.Timestamp;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import service.ticket.ticketBarcodeRec.TicketBarcodeRecService;

import common.CodeListener;
import common.CommonMethod;

/**
 * @author 
 *实时出票查询，仅当天
 */
@Service("realPrintJob")
public class RealPrintJob {
	
	private static ReentrantLock lock = null;
	private static Logger logger = Logger.getLogger("bookingService");
	@Autowired
	private TicketBarcodeRecService ticketBarcodeRecService;
	
	/**
	 * 实时出票查询job
	 */
	public void findRealPrintJob(){
		if(lock == null){
			 lock = new ReentrantLock();
		}
		if (lock.tryLock()) {
			try{
				Timestamp thisTime = CommonMethod.getTimeStamp();
				CodeListener.setTicketList(ticketBarcodeRecService.findRealPrint(thisTime, thisTime));
			}
			catch (Exception e) {
				e.printStackTrace();
				logger.error("实时出票查询job出错", e);
			}
			finally{
				lock.unlock();
			}
		}
	}
	
}
