package service.job;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import service.comm.baseTranRec.BaseTranRecService;
import service.hotel.financeHotelAccounts.FinanceHotelAccountsService;
import service.hotel.hotelDetailHis.HotelDetailHisService;
import service.hotel.hotelIndent.HotelIndentService;
import service.hotel.hotelIndentDetail.HotelIndentDetailService;
import ws.ReturnInfo;
import ws.hotel.client.HotelIndentClient;
import ws.hotel.comm.HotelCode;
import ws.hotel.pojo.RoomType;

import com.thoughtworks.xstream.XStream;
import com.xlem.dao.BaseMessageRec;
import com.xlem.dao.Hotel;
import com.xlem.dao.HotelBookingLimit;
import com.xlem.dao.HotelIndent;
import com.xlem.dao.HotelIndentDetail;
import com.xlem.dao.HotelPriceLimitDetail;
import com.xlem.dao.HotelRoomType;
import com.xlem.dao.TicketIndent;

import common.BusinessException;
import common.CodeListener;
import common.CommonMethod;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.comm.baseMessageRec.BaseMessageRecDao;
import dao.hotel.hotel.HotelDao;
import dao.hotel.hotelBookingLimit.HotelBookingLimitDao;
import dao.hotel.hotelIndent.HotelIndentDao;
import dao.hotel.hotelIndentDetail.HotelIndentDetailDao;
import dao.hotel.hotelPriceLimitDetail.HotelPriceLimitDetailDao;
import dao.hotel.roomType.RoomTypeDao;
import dao.sys.sysParam.SysParamDao;
@Service("hotelIndentJobService")
public class HotelIndentJobServiceImlp extends BaseServiceImpl<TicketIndent> implements HotelIndentJobService{
	@Autowired
	private HotelIndentDao hotelIndentDao;
	@Autowired
	private HotelIndentDetailService hotelIndentDetailService;
	@Autowired
	private HotelIndentDetailDao hotelIndentDetailDao;
	@Autowired
	private RoomTypeDao roomTypeDao;
	@Autowired
	private HotelDao hotelDao;
	@Autowired
	private HotelBookingLimitDao hotelBookingLimitDao;
	@Autowired
	private BaseMessageRecDao baseMessageRecDao;
	@Autowired
	private HotelDetailHisService hotelDetailHisService;
	@Autowired
	private HotelIndentService hotelIndentService;
	@Autowired
	private HotelPriceLimitDetailDao hotelPriceLimitDetailDao;
	@Autowired
	private FinanceHotelAccountsService financeHotelAccountsService;
	@Autowired
	private BaseTranRecService baseTranRecService;
	@Autowired
	private SysParamDao sysParamDao;
	
	@Override
	@Resource(name = "hotelIndentDao")
	protected void initBaseDAO(BaseDao<TicketIndent> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public void saveHotelWrite(HotelCode hotelCode) {
		HotelIndentClient client = new HotelIndentClient();
		DetachedCriteria dc = DetachedCriteria.forClass(HotelIndent.class);
		dc.createAlias("hotel", "hotel");
		dc.add(Property.forName("synchroState").eq("00"));
		dc.add(Property.forName("status").eq("02"));//已支付
		dc.add(Property.forName("hotel.hotelCode").eq(hotelCode.name()));
		dc.addOrder(Order.asc("hotelIndentId"));
		List<HotelIndent> list = hotelIndentDao.findByCriteria(dc);
		List<HotelIndent> list1 = new ArrayList<HotelIndent>();
		if(list==null || list.isEmpty()){//无数据时不需调用结口
			return;
		}
		if(list != null && !list.isEmpty()){
			for (HotelIndent hotelIndent : list) {
				
				//查询创建人
				String origin = hotelIndent.getOrigin();
				String cType = hotelIndent.getCustomerType();
				
				if(!"00".equals(origin)){
					hotelIndent.setInputerName(CodeListener.getNameByUserId(hotelIndent.getInputer()));
				}
				
				DetachedCriteria dc1 = DetachedCriteria.forClass(HotelIndentDetail.class);
				dc1.createAlias("hotelRoomType", "hotelRoomType");
				dc1.createAlias("hotelRoomType.hotel", "hotel");
				dc1.add(Property.forName("hotelIndent.hotelIndentId").eq(hotelIndent.getHotelIndentId()));
				dc1.addOrder(Order.asc("bookingDate"));
				List<HotelIndentDetail> setList = hotelIndentDetailDao.findByCriteria(dc1);
				Set<HotelIndentDetail> set = new HashSet<HotelIndentDetail>();
				set.addAll(setList);
				for (HotelIndentDetail hotelIndentDetail : set) {
					hotelIndentDetail.setRoomTypeId(hotelIndentDetail.getHotelRoomType().getRoomTypeId());
					hotelIndentDetail.setHotelIndent(hotelIndent);
				}
				
				hotelIndent.setHotelIndentDetails(set);
				hotelIndent.setBaseTranRecs(null);
				hotelIndent.setHotelDetailHises(null);
				
				if(hotelIndent.getHotel()!=null && hotelIndent.getHotel().getHotelCode()!=null && !"".equals(hotelIndent.getHotel().getHotelCode())){
					//设置同步占用
					hotelIndent.setSynchroState("80");
					hotelIndentDao.update(hotelIndent);
					list1.add(hotelIndent);
				}
			}
		}
		if(list1==null || list1.isEmpty()){//无数据发送时不需调用结口
			return;
		}
		//调用接口发送
		ReturnInfo ri = client.writeHotelIndent(list1,hotelCode);
		//处理返回结果
		if("-1".equals(ri.getResult())){
			List ids = new ArrayList();
			for (HotelIndent hotelIndent : list1) {
				//失败后更改同步状态
				hotelIndent.setSynchroState("00");
				hotelIndentDao.update(hotelIndent);
				ids.add(hotelIndent.getIndentCode());
			}
			
			String content = "酒店订单发送失败，酒店编号:"+hotelCode+"!";
			
			DetachedCriteria dc1 = DetachedCriteria.forClass(BaseMessageRec.class);
			dc1.add(Property.forName("phoneNumber").eq(sysParamDao.findSysParam("HOTEL_INDENT_ERROR_MOBILE")));
			dc1.add(Property.forName("sendTime").ge(CommonMethod.string2Time1(CommonMethod.addHour(CommonMethod.timeToString2(CommonMethod.getTimeStamp()), -1))));
			List<BaseMessageRec> setList = baseMessageRecDao.findByCriteria(dc1);
			if(setList.size()<3){
				CommonMethod.sendSms(sysParamDao.findSysParam("HOTEL_INDENT_ERROR_MOBILE"), content,sysParamDao.findSysParam("USE_NEW_PLATFROM"));
				baseMessageRecDao.saveMess(sysParamDao.findSysParam("HOTEL_INDENT_ERROR_MOBILE"),content);
				CommonMethod.sendMail(sysParamDao.findSysParam("HOTEL_INDENT_ERROR_EMAIL"),
						"管理员", 
						"向缓存写入订单信息出错,订单号:"+ids+",错误原因："+ri.getError());
			}
		}
		else{
			for (HotelIndent hotelIndent : list1) {
				//成功后更改同步状态
				hotelIndent.setSynchroState("01");
				hotelIndentDao.update(hotelIndent);
			}
		}
	}

	@Override
	public void saveCancelHotelIndent(HotelCode hotelCode) {
		HotelIndentClient client = new HotelIndentClient();
		DetachedCriteria dc = DetachedCriteria.forClass(HotelIndent.class);
		dc.createAlias("hotel", "hotel");
//		dc.createAlias("hotelIndentDetails", "hotelIndentDetails");
		dc.add(Property.forName("synchroState").eq("04"));//退订未同步
		dc.add(Property.forName("status").eq("07"));//申请退订
		dc.add(Property.forName("hotel.hotelCode").eq(hotelCode.name()));
		dc.addOrder(Order.asc("hotelIndentId"));
		List<HotelIndent> list = hotelIndentDao.findByCriteria(dc);
		List<HotelIndent> list1 = new ArrayList<HotelIndent>();
		if(list==null || list.isEmpty()){//无数据时不需调用结口
			return;
		}
		if(list != null && !list.isEmpty()){
			for (HotelIndent hotelIndent : list) {
				hotelIndent.setHotelIndentDetails(null);
				hotelIndent.setBaseTranRecs(null);
				hotelIndent.setHotelDetailHises(null);
				if(hotelIndent.getHotel()!=null && hotelIndent.getHotel().getHotelCode()!=null && !"".equals(hotelIndent.getHotel().getHotelCode())){
					//设置同步占用
					hotelIndent.setSynchroState("80");
					hotelIndentDao.update(hotelIndent);
					list1.add(hotelIndent);
				}
			}
		}
		if(list1==null || list1.isEmpty()){//无数据发送时不需调用结口
			return;
		}
		//调用接口发送
		ReturnInfo ri = client.CancelHotelIndent(list1,hotelCode);
		//处理返回结果
		if("-1".equals(ri.getResult())){
			for (HotelIndent hotelIndent : list1) {
				//失败后更改同步状态
				hotelIndent.setSynchroState("04");
				hotelIndentDao.update(hotelIndent);
			}
		}
		else{
			XStream xsStream = new XStream();
			xsStream.alias("obj", HotelIndent.class);
			xsStream.alias("root", List.class);
			List<HotelIndent> tempList = (List<HotelIndent>)xsStream.fromXML(ri.getResult());
			HashMap<Long, String> hms = new HashMap<Long, String>();
			for (HotelIndent temHi : tempList) {
				hms.put(temHi.getHotelIndentId(), temHi.getStatus());
			}
			for (HotelIndent hotelIndent : list1) {
				if("05".equals(hms.get(hotelIndent.getHotelIndentId()))){
					List<HotelIndentDetail> hotelIndentDetails = hotelIndentDetailDao.findByProperty("hotelIndent.hotelIndentId", hotelIndent.getHotelIndentId());
					Map<String,Object> map = hotelIndentDetailService.findCountRefundMoney(hotelIndentDetails,"00");
					Double returnMoney = Double.valueOf(map.get("refund").toString());//退款金额
					Double unitRatio = Double.valueOf(map.get("unitRatio").toString());
					Double charge = Double.valueOf(map.get("charge").toString());
					if(hotelIndentDetailService.saveRefund(hotelIndent, hotelIndentDetails, returnMoney, charge, unitRatio,"00")){
						
						boolean allRefund = true;
						int totalAmount = 0;
						double totalPrice = 0d;
						for (int i = 0; i < hotelIndentDetails.size(); i++) {//计算订单是否全部退订
							HotelIndentDetail hid = hotelIndentDetails.get(i);
							int retAmo = hid.getAmountret()==null?0:hid.getAmountret();
							int surplus = hid.getAmount()-retAmo;
							if(surplus>totalAmount){//数量取最大值
								totalAmount = surplus;
							}
							totalPrice += surplus*hid.getUnitPrice();
							if(surplus > 0){
								allRefund = false;
							}
						}
						
						//成功后更改同步状态
						if(allRefund){//全部退订
							hotelIndent.setSynchroState("05");
							hotelIndent.setStatus("05");
						}else{//部分退订
							hotelIndent.setSynchroState("00");
							hotelIndent.setStatus("02");
						}
						hotelIndent.setTotalAmount(totalAmount);
						hotelIndent.setTotalPrice(totalPrice);
						hotelIndentDao.update(hotelIndent);
						hotelIndentService.saveReleaseRoom(hotelIndentDetails,hotelIndent);
					}
					else{
						//失败后更改同步状态
						hotelIndent.setSynchroState("06");//退款未同步
						hotelIndent.setStatus("07");//申请退订
						hotelIndentDao.update(hotelIndent);
					}
				}
				else if("03".equals(hms.get(hotelIndent.getHotelIndentId()))){
					hotelIndent.setSynchroState("05");
					hotelIndent.setStatus("03");
					hotelIndentDao.update(hotelIndent);
				}
			}
		}
	}
	
	@Override
	public void saveCancelHotelIndent(HotelCode hotelCode,List<HotelIndent> list) {
		HotelIndentClient client = new HotelIndentClient();
		List<HotelIndent> list1 = new ArrayList<HotelIndent>();
		if(list==null || list.isEmpty()){//无数据时不需调用结口
			return;
		}
		if(list != null && !list.isEmpty()){
			for (HotelIndent hotelIndent : list) {
				hotelIndent.setHotelIndentDetails(null);
				hotelIndent.setBaseTranRecs(null);
				hotelIndent.setHotelDetailHises(null);
				if(hotelIndent.getHotel()!=null && hotelIndent.getHotel().getHotelCode()!=null && !"".equals(hotelIndent.getHotel().getHotelCode())){
					//设置同步占用
					hotelIndent.setSynchroState("80");
					hotelIndentDao.update(hotelIndent);
					list1.add(hotelIndent);
				}
			}
		}
		if(list1==null || list1.isEmpty()){//无数据发送时不需调用结口
			return;
		}
		//调用接口发送
		ReturnInfo ri = client.CancelHotelIndent(list1,hotelCode);
		//处理返回结果
		if("-1".equals(ri.getResult())){
			for (HotelIndent hotelIndent : list1) {
				//失败后更改同步状态
				hotelIndent.setSynchroState("04");
				hotelIndentDao.update(hotelIndent);
			}
		}
		else{
			XStream xsStream = new XStream();
			xsStream.alias("obj", HotelIndent.class);
			xsStream.alias("root", List.class);
			List<HotelIndent> tempList = (List<HotelIndent>)xsStream.fromXML(ri.getResult());
			HashMap<Long, String> hms = new HashMap<Long, String>();
			for (HotelIndent temHi : tempList) {
				hms.put(temHi.getHotelIndentId(), temHi.getStatus());
			}
			for (HotelIndent hotelIndent : list1) {
				//成功后更改同步状态
				List<HotelIndentDetail> hotelIndentDetails = hotelIndentDetailDao.findByProperty("hotelIndent.hotelIndentId", hotelIndent.getHotelIndentId());
				if(!"03".equals(hotelIndent.getStatus())){//已排房订单
					hotelIndentService.saveReleaseRoom(hotelIndentDetails,hotelIndent);
					boolean allRefund = true;
					for(HotelIndentDetail hid : hotelIndentDetails){
						int retAmo = hid.getAmountret()==null?0:hid.getAmountret();
						int surplus = hid.getAmount()-retAmo;
						if(surplus > 0){
							allRefund = false;
						}
					}
					
					if(allRefund){//全部退订
						hotelIndent.setSynchroState("05");
						hotelIndent.setStatus("05");
					}else{//部分退订
						hotelIndent.setSynchroState("00");
						hotelIndent.setStatus("02");
					}
				}
				hotelIndentDao.update(hotelIndent);
			}
		}
	}

	@Override
	public void saveSynRoomType(HotelCode hotelCode) {
		List<Hotel> hotels = hotelDao.findByProperty("hotelCode", hotelCode.name());//获取当前酒店信息
		Hotel hotel = null;
		if(hotels==null || hotels.isEmpty()){
			return;
		}
		else{
			hotel = hotels.get(0);
		}
		//调用接口发送
		HotelIndentClient client = new HotelIndentClient();
		ReturnInfo ri = client.synRoomType(hotelCode);
		if("-1".equals(ri.getResult())){
			//失败
		}
		else{
			XStream xsRoomTypes=new XStream();
			xsRoomTypes.alias("obj",RoomType.class);
			xsRoomTypes.alias("root",List.class);
			List<RoomType> roomTypes=(List<RoomType>) xsRoomTypes.fromXML(ri.getResult());
			Timestamp nowtime = CommonMethod.getTimeStamp();//当前时间
			for (RoomType roomType : roomTypes) {
				DetachedCriteria dc = DetachedCriteria.forClass(HotelRoomType.class);
				dc.add(Property.forName("roomTypeCode").eq(roomType.getType()));
				dc.add(Property.forName("hotel.hotelId").eq(hotel.getHotelId()));
				List<HotelRoomType> hotelRoomTypes = roomTypeDao.findByCriteria(dc);
				if(hotelRoomTypes==null || hotelRoomTypes.isEmpty()){//如不存在则新增
					HotelRoomType hotelRoomType = new HotelRoomType();
					hotelRoomType.setRoomTypeId(getNextKey("hotel_room_type", 1));
					hotelRoomType.setRoomTypeCode(roomType.getType());
					hotelRoomType.setInputTime(nowtime);
					hotelRoomType.setStatus("11");
					hotelRoomType.setTypeName(roomType.getDescript());
					hotelRoomType.setHotel(hotel);
					roomTypeDao.save(hotelRoomType);
				}
				else{//否则修改
					HotelRoomType hotelRoomType = hotelRoomTypes.get(0);
					hotelRoomType.setRoomTypeCode(roomType.getType());
					hotelRoomType.setUpdateTime(nowtime);
					hotelRoomType.setTypeName(roomType.getDescript());
					hotelRoomType.setHotel(hotel);
					roomTypeDao.update(hotelRoomType);
				}
			}
		}
		
	}

	@Override
	public void saveHotelBookingLimit(HotelCode hotelCode) {
		DetachedCriteria dc = DetachedCriteria.forClass(HotelBookingLimit.class);
		dc.createAlias("hotelRoomType", "hotelRoomType");
		dc.createAlias("hotelRoomType.hotel", "hotel");
		dc.add(Property.forName("hotel.hotelCode").eq(hotelCode.name()));
		dc.add(Property.forName("synchroState").eq("00"));
		List<HotelBookingLimit> bookingLimits = hotelBookingLimitDao.findByCriteria(dc);
		if(bookingLimits==null || bookingLimits.isEmpty()){
			return;
		}
		HotelIndentClient client = new HotelIndentClient();
		ReturnInfo ri = client.bookingLimit(bookingLimits,hotelCode);
		if("-1".equals(ri.getResult())){//失败
			throw new BusinessException(ri.getError());
		}
		else{//成功
			for (HotelBookingLimit hotelBookingLimit : bookingLimits) {
				hotelBookingLimit.setSynchroState("01");
				hotelBookingLimitDao.update(hotelBookingLimit);
			}
		}
	}

	@Override
	public void saveRefundIndent(HotelIndent hi) {
		
		List<HotelIndentDetail> hotelIndentDetails = hotelIndentDetailDao.findByProperty("hotelIndent.hotelIndentId", hi.getHotelIndentId());
		double charge  =  0d;
		double tkmoney = 0d;
		
		List<HotelIndentDetail> tidList = new ArrayList<HotelIndentDetail>();
		Map<String,HotelIndentDetail> retDetail = new HashMap<String,HotelIndentDetail>();
		for(HotelIndentDetail hid : hotelIndentDetails){
			double cutPrice = hid.getCutPrice() == null ? 0d : hid.getCutPrice();
			charge += cutPrice;
			int amountRet = hid.getAmountret() == null ? 0 : hid.getAmountret();
			tkmoney += (amountRet*hid.getUnitPrice()-cutPrice);
			tidList.add(hid);
			retDetail.put("del_"+hid.getHotelIndentDetailId(), hid);
		}
		double unitRatio = 0d;
		if((charge+tkmoney)>0d){
			unitRatio = (charge /(charge+tkmoney))*100;
		}
		if(hotelIndentDetailService.saveRefund(hi, hotelIndentDetails, tkmoney,charge,unitRatio,"00")){//退款成功
			//成功后更改同步状态
				hi.setSynchroState("07");//退款已同步
				hi.setStatus("05");//已退订
		}
		hotelIndentDao.update(hi);
	}
	
	@Override
	public void saveHotelIndentCheck() {
		DetachedCriteria dc = DetachedCriteria.forClass(HotelIndent.class);
		dc.add(Property.forName("origin").ne("00"));//市场部订单
		dc.add(Property.forName("status").eq("01"));//未支付
		List<HotelIndent> hiList = hotelIndentDao.findByCriteria(dc);
		for (HotelIndent hi : hiList) {
			Long h = Long.parseLong(findSysParam("HOTEL_INDENT_NOTIFY_TIME"));//市场部代订订单通知时间间隔单位（小时）
			Long myhour = CommonMethod.diffHour(CommonMethod.getTimeStamp(), Timestamp.valueOf(hi.getPayTime().toString()));
			if(myhour<=h){
				DetachedCriteria dcMes = DetachedCriteria.forClass(BaseMessageRec.class);
				dcMes.add(Property.forName("functionModule").eq(hi.getHotelIndentId().toString()));
				dcMes.add(Property.forName("functionType").eq("001"));//001市场部代订订单通知
				if(baseMessageRecDao.findByCriteria(dcMes).isEmpty()){
					String content = "您的酒店订房订单"+hi.getIndentCode()+"预留时间为"+hi.getPayTime()+"，即将到期，请及时支付！";
					BaseMessageRec mes = new BaseMessageRec(getNextKey("Base_Message_Rec", 1));
					mes.setFunctionModule(hi.getHotelIndentId().toString());
					mes.setFunctionType("001");
					mes.setMessageContent(content);
					mes.setSendTime(CommonMethod.getTimeStamp());
					mes.setPhoneNumber(hi.getPhoneNumber());
					baseMessageRecDao.save(mes);
					CommonMethod.sendSms(hi.getPhoneNumber(), content,sysParamDao.findSysParam("USE_NEW_PLATFROM"));
				}
			}
			if(myhour<0 && -myhour>Long.parseLong(findSysParam("HOTEL_INDENT_CANCEL_TIME"))){
				//取消订单
				hi.setStatus("06");//
				hi.setUpdateTime(CommonMethod.getTimeStamp());
				hotelIndentDao.update(hi);
				List<HotelIndentDetail> hotelIndentDetails = hotelIndentDetailDao.findByProperty("hotelIndent.hotelIndentId", hi.getHotelIndentId());
				for(HotelIndentDetail hid : hotelIndentDetails){
					hid.setAmountret(hid.getAmount());
				}
				hotelIndentService.saveReleaseRoom(hotelIndentDetails,hi);
				String content = "您的酒店订房订单"+hi.getIndentCode()+"预留时间为"+hi.getPayTime()+"，已经到期，系统已经取消该订单！";
				BaseMessageRec mess = new BaseMessageRec(getNextKey("BASE_MESSAGE_REC", 1));
				mess.setPhoneNumber(hi.getPhoneNumber());
				mess.setMessageContent(content);
				mess.setSendTime(CommonMethod.getTimeStamp());
				baseMessageRecDao.save(mess);
				
				//市场部订单取消时需冲退
				financeHotelAccountsService.saveReturnBackInfo(hi, null, null);
//				baseMessageRecDao.saveMess(hi.getPhoneNumber(),content);
				CommonMethod.sendSms(hi.getPhoneNumber(), content,sysParamDao.findSysParam("USE_NEW_PLATFROM"));
			}
		}
	}

	//大于25分钟取消团客和散客的自动取消订单
	public void saveAutoCancelNopayIndent() {
		DetachedCriteria dc = DetachedCriteria.forClass(HotelIndent.class);
		dc.add(Property.forName("origin").eq("00"));//散客、团客订单
		dc.add(Property.forName("status").eq("01"));//未支付
		List<HotelIndent> hiList = hotelIndentDao.findByCriteria(dc);
		Long mi = Long.parseLong(findSysParam("HOTEL_INDENT_AUTO_CANCEL_TIME"));//酒店订单自动取消时间（分钟）
		for (HotelIndent hi : hiList) {
			Long myhour = CommonMethod.diffMin( Timestamp.valueOf(hi.getInputTime().toString()), CommonMethod.getTimeStamp());
			if(myhour>=mi){
				//取消订单
				hi.setStatus("06");//
				hi.setUpdateTime(CommonMethod.getTimeStamp());
				hotelIndentDao.update(hi);
				List<HotelIndentDetail> hotelIndentDetails = hotelIndentDetailDao.findByProperty("hotelIndent.hotelIndentId", hi.getHotelIndentId());
				for(HotelIndentDetail hid : hotelIndentDetails){
					hid.setAmountret(hid.getAmount());
				}
				hotelIndentService.saveReleaseRoom(hotelIndentDetails,hi);
				//提示时间比实际时间少5分钟
				String content = "您的酒店订房订单"+hi.getIndentCode()+"超过"+(mi-5)+"分钟未支付，系统已经取消该订单！";
				baseMessageRecDao.saveMess(hi.getPhoneNumber(),content);
				CommonMethod.sendSms(hi.getPhoneNumber(), content,sysParamDao.findSysParam("USE_NEW_PLATFROM"));
				
				//冲退交易记录
				baseTranRecService.saveReturnBackInfo(hi);
				//冲退财务记录
				financeHotelAccountsService.saveReturnBackInfo(hi,null,null);
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void saveSettleAccountsHotel(HotelCode hotelCode) {
		//从西软获取消费信息
		HotelIndentClient client = new HotelIndentClient();
		ReturnInfo ri = client.synDetails(hotelCode);
		if("-1".equals(ri.getResult())){
			//失败
		}
		else{
			XStream xsRoomTypes=new XStream();
			xsRoomTypes.alias("obj",HotelIndentDetail.class);
			xsRoomTypes.alias("root",List.class);
			List<HotelIndentDetail> hids=(List<HotelIndentDetail>) xsRoomTypes.fromXML(ri.getResult());
			HashMap<String, List<HotelIndentDetail>> hashMap = new HashMap<String, List<HotelIndentDetail>>();
			for (int i = 0; i < hids.size(); i++) {
				HotelIndentDetail hid = hids.get(i);
				List<HotelIndentDetail> tempHids = hashMap.get(hid.getHotelIndent().getIndentCode());
				if(tempHids==null){
					tempHids = new ArrayList<HotelIndentDetail>();
					hashMap.put(hid.getHotelIndent().getIndentCode(), tempHids);
				}
				tempHids.add(hid);
			}
			Set<String> set = hashMap.keySet();
			for (String key : set) {
				List<HotelIndentDetail> tempHids = hashMap.get(key);//实际消费明细
				DetachedCriteria dc = DetachedCriteria.forClass(HotelIndentDetail.class);
				dc.createAlias("hotelIndent", "hotelIndent");
				dc.add(Property.forName("hotelIndent.indentCode").eq(key));
				List<HotelIndentDetail> ohids = hotelIndentDetailDao.findByCriteria(dc);//现在数据库中对应订单的明细
				for (int i = 0; i < ohids.size(); i++) {
					HotelIndentDetail ohid = ohids.get(i);
					for (int j = 0; j < tempHids.size(); j++) {
						HotelIndentDetail hid = tempHids.get(j);
					}
				}
			}
		}
	}

	@Override
	public void saveHotelBookingLimits(HotelCode hotelCode) {
		DetachedCriteria dc = DetachedCriteria.forClass(HotelPriceLimitDetail.class);
		dc.createAlias("hotelRoomType", "hotelRoomType");
		dc.createAlias("hotelRoomType.hotel", "hotel");
		dc.add(Property.forName("hotel.hotelCode").eq(hotelCode.name()));
		dc.add(Property.forName("useDate").ge(CommonMethod.string2Time1("2013-04-01 00:00:00")));
		List<HotelPriceLimitDetail> bookingLimits = hotelPriceLimitDetailDao.findByCriteria(dc);
		if(bookingLimits==null || bookingLimits.isEmpty()){
			return;
		}
		List<HotelBookingLimit> tHBLs = new ArrayList<HotelBookingLimit>();
		for (HotelPriceLimitDetail limit : bookingLimits) {
			HotelBookingLimit tHotelBookingLimit = new HotelBookingLimit();
			tHotelBookingLimit.setLimitId(limit.getDetailId());
			tHotelBookingLimit.setStatus("11");
			int amount = 0;
			if(limit.getAmount()==null || "".equals(limit.getAmount())){
				amount = 0;
			}
			else{
				amount = limit.getAmount();
			}
			tHotelBookingLimit.setAmount(amount);
			tHotelBookingLimit.setSynchroState("00");
			tHotelBookingLimit.setHotelRoomType(limit.getHotelRoomType());
			tHotelBookingLimit.setStartDate(limit.getUseDate());
			Timestamp ts = new Timestamp(limit.getUseDate().getTime());
			ts = CommonMethod.toEndTime(ts);
			tHotelBookingLimit.setEndDate(ts);
			tHBLs.add(tHotelBookingLimit);
		}
		HotelIndentClient client = new HotelIndentClient();
		ReturnInfo ri = client.bookingLimit(tHBLs,hotelCode);
		if("-1".equals(ri.getResult())){//失败
			throw new BusinessException(ri.getError());
		}
		else{//成功
			
		}
	}

	@Override
	public void saveAutoStatus(HotelIndent hotelIndent) {
//		DetachedCriteria dc = DetachedCriteria.forClass(HotelIndent.class);
//		dc.add(Property.forName("status").eq("02"));
//		dc.add(Property.forName("startDate").le(CommonMethod.addDay(CommonMethod.getTimeStamp(),-1)));
//		List<HotelIndent> list = hotelIndentDao.findByCriteria(dc);
//		
//		List<String> hotelIndentIds = new ArrayList<String>();
//		for (HotelIndent hotelIndent : list) {
			
			hotelIndent.setStatus("04");
			hotelIndentDao.update(hotelIndent);
			
//			hotelIndentIds.add(hotelIndent.getHotelIndentId().toString());
//		}
		
//		if(hotelIndentIds.size()>0){
			//添加财务记录
			financeHotelAccountsService.savePaidToExpend(hotelIndent.getHotelIndentId().toString());
//		}
		
		
	}
	
}
