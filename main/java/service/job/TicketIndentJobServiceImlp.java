package service.job;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import service.comm.baseCashAccount.BaseCashAccountService;
import service.comm.baseRechargeRec.BaseRechargeRecService;
import service.comm.baseTranRec.BaseTranRecService;
import service.comm.reg.baseTravelAgency.BaseTravelAgencyService;
import service.ticket.financeTicketAccounts.FinanceTicketAccountsService;
import service.ticket.ticketIndent.TicketIndentService;
import service.ticket.ticketRoleType.TicketRoleTypeService;
import service.ticket.ticketUserType.TicketUserTypeService;
import ws.ReturnInfo;
import ws.client.ticketIndent.TicketIndentClient;

import com.thoughtworks.xstream.XStream;
import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.BaseRewardAccount;
import com.xlem.dao.BaseTranRec;
import com.xlem.dao.SysRole;
import com.xlem.dao.SysUser;
import com.xlem.dao.SysUserRole;
import com.xlem.dao.TicketBarcodeRec;
import com.xlem.dao.TicketIndent;
import com.xlem.dao.TicketIndentDetail;
import com.xlem.dao.TicketMemberList;
import com.xlem.dao.TicketPrice;
import com.xlem.dao.Travservice;
import com.xlem.dao.TravserviceHome;

import common.BusinessException;
import common.CommonMethod;
import common.dao.BaseDao;
import common.pay.PayFactoryService;
import common.pay.RefundParams;
import common.service.BaseServiceImpl;

import dao.comm.baseRechargeRec.BaseRechargeRecDao;
import dao.comm.baseRewardAccount.BaseRewardAccountDao;
import dao.comm.baseRewardType.BaseRewardTypeDao;
import dao.comm.baseTranRec.BaseTranRecDao;
import dao.comm.reg.baseTravelAgency.BaseTravelAgencyDao;
import dao.sys.sysRole.SysRoleDao;
import dao.sys.sysUser.SysUserDao;
import dao.sys.sysUserRole.SysUserRoleDao;
import dao.ticket.ticketBarcodeRec.TicketBarcodeRecDao;
import dao.ticket.ticketIndent.TicketIndentDao;
import dao.ticket.ticketIndentDetail.TicketIndentDetailDao;
import dao.ticket.ticketMemberList.TicketMemberListDao;
import dao.ticket.ticketPrice.TicketPriceDao;
@Service("ticketIndentJobService")
public class TicketIndentJobServiceImlp extends BaseServiceImpl<TicketIndent> implements TicketIndentJobService{
	@Autowired
	private TicketIndentDao ticketIndentDao;
	@Autowired
	private TicketIndentDetailDao ticketIndentDetailDao;
	@Autowired
	private TicketBarcodeRecDao ticketBarcodeRecDao;
	@Autowired
	private SysUserDao sysUserDao;
	@Autowired
	private BaseTravelAgencyDao baseTravelAgencyDao;
	@Autowired
	private BaseTranRecService baseTranRecService;
	@Autowired
	private BaseRechargeRecService baseRechargeRecService;
	@Autowired
	private PayFactoryService payFactoryService;
	@Autowired
	private BaseCashAccountService baseCashAccountService;
	@Autowired
	private BaseTranRecDao baseTranRecDao;
	@Autowired
	private TicketMemberListDao ticketMemberListDao;
	@Autowired
	private BaseRewardAccountDao baseRewardAccountDao;
	@Autowired
	private BaseRewardTypeDao baseRewardTypeDao;
	@Autowired
	private BaseRechargeRecDao baseRechargeRecDao;
	@Autowired
	private BaseTravelAgencyService baseTravelAgencyService;
	@Autowired
	private SysUserRoleDao sysUserRoleDao;
	@Autowired
	private SysRoleDao sysRoleDao;
	@Autowired
	private FinanceTicketAccountsService financeTicketAccountsService;
	@Autowired
	private TicketIndentService ticketIndentService;
	@Autowired
	private TicketUserTypeService ticketUserTypeService;
	@Autowired
	private TicketRoleTypeService ticketRoleTypeService;
	@Autowired
	private TicketPriceDao ticketPriceDao;
	
	private static Logger logger = Logger.getLogger("bookingService");
	
	@Override
	public void saveTicketWrite(List<TicketIndent> indents) {
//		DetachedCriteria dcIndent = DetachedCriteria.forClass(TicketIndent.class);
//		dcIndent.add(Property.forName("status").in(new String[]{"02","03"}));//已支付和待审核;
//		dcIndent.add(Property.forName("synchroState").eq("00"));
//		List<TicketIndent> indents = ticketIndentDao.findByCriteria(dcIndent);
		if(indents==null || indents.size()==0){
			return;
		}
		for (TicketIndent ticketIndent : indents) {//修改状态
			ticketIndent.setSynchroState("80");//轮询同步占用
			ticketIndentDao.update(ticketIndent);
			ticketIndent.setSynchroState("00");
			//外地旅行社订单增加游客名单
			if(ticketIndent.getDiscount()!=null && !ticketIndent.getDiscount().trim().isEmpty()){
				List<TicketMemberList> tmlList = ticketMemberListDao.findByProperty("ticketIndent.indentId", ticketIndent.getIndentId());
				ticketIndent.setTmlList(tmlList);
			}
			//订单明细 1.24增加
			List<TicketIndentDetail> details = ticketIndentDetailDao.findByProperty("ticketIndent.indentId", ticketIndent.getIndentId());
			
			for(TicketIndentDetail ticketIndentDetail : details){
				TicketPrice tp = new TicketPrice(ticketIndentDetail.getTicketPrice().getTicketPriceId());
				tp.setTicketDetailHises(null);
				tp.setTicketIndentDetails(null);
				ticketIndentDetail.setTicketPrice(tp);
			}
//			DetachedCriteria dc = DetachedCriteria.forClass(TicketIndentDetail.class);
//			dc.add(Property.forName("ticketIndent.indentId").eq(ticketIndent.getIndentId()));
//			dc.createAlias("ticketPrice", "ticketPrice");
//			List<TicketIndentDetail> details = ticketIndentDetailDao.findByCriteria(dc);
			ticketIndent.setDetails(details);//2013-12-24 把set改为list
//			ticketIndent.setTicketIndentDetails(new HashSet<TicketIndentDetail>(details));
		}
		TicketIndentClient client = new TicketIndentClient();
		ReturnInfo ri = client.writeTicketIndents(indents);
		if(ri.getResult().trim().equals("0")){//接口调用成功
			for (TicketIndent ticketIndent : indents) {//修改状态
				ticketIndent.setSynchroState("01");//订单写入现场库
				ticketIndentDao.update(ticketIndent);
			}
		}else{
			for (TicketIndent ticketIndent : indents) {//接口调用失败
				ticketIndent.setSynchroState("00");//
				ticketIndentDao.update(ticketIndent);
			}
		}
	}
	
	@Override
	public void saveTicketUse() {
		List<TicketIndent> indentList = ticketIndentDao.findByProperty("status", "04");//已打印
		//1.查找已打印的订单
		for (TicketIndent ticketIndent : indentList) {
			DetachedCriteria dcDetail = DetachedCriteria.forClass(TicketIndentDetail.class);
			dcDetail.createAlias("ticketIndent", "ticketIndent");
			dcDetail.add(Property.forName("ticketIndent").eq(ticketIndent));
			List<TicketIndentDetail> detailList = ticketIndentDetailDao.findByCriteria(dcDetail);
			Boolean alluse = true;
			boolean isReturn = false;
			for (TicketIndentDetail detail : detailList) {
				DetachedCriteria dcTBR = DetachedCriteria.forClass(TicketBarcodeRec.class);
				dcTBR.createAlias("ticketIndentDetail", "ticketIndentDetail");
				dcTBR.add(Property.forName("ticketIndentDetail").eq(detail));
				dcTBR.add(Property.forName("status").in(new String[]{"01","02","03","05"}));//01已使用, 02已作废,03已过期,05现场退票退款已成功
				List<TicketBarcodeRec> tbrList = ticketBarcodeRecDao.findByCriteria(dcTBR);
				if(tbrList.size()!= detail.getAmount()){//订单明细所有票均已经消费
					alluse =false;
					break;
				}
				//订单为自助购票机，处理退订单 2013-10-31 lzp
				if("03".equals(ticketIndent.getPayType())){
					for (TicketBarcodeRec tbr : tbrList) {
						if(!"05".equals(tbr.getStatus())){
							isReturn = false;
							break;
						}else{
							isReturn = true;
						}
					}
				}
			}
			if(alluse){
				ticketIndent.setStatus("05");//05已消费
				ticketIndent.setSynchroState("03");//03完成消费同步；
				//处理退订单 2013-10-31 lzp
				if(isReturn){
					ticketIndent.setStatus("06");//06已退订 
					ticketIndent.setSynchroState("07");//07完成退款同步；
				}
			}
		}
	}
	@Override
	@Resource(name = "ticketIndentDao")
	protected void initBaseDAO(BaseDao<TicketIndent> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public List<TicketIndent> findReturnIndents() {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketIndent.class);
		dc.add(Property.forName("status").eq("08"));//申请退订
		dc.add(Property.forName("synchroState").eq("04"));//退票未同步
		return ticketIndentDao.findByCriteria(dc);
	}
	
	@Override
	public void updateReturnIndent(TicketIndent indent) {
		//退订检测
		if(!checkReturn(indent)){
			return;
		}
		TicketIndentClient client = new TicketIndentClient();
		List<TicketIndent> indents = new ArrayList<TicketIndent>();
		indents.add(indent);
		ReturnInfo ri = client.returnTickets(indents);
		if("-1".equals(ri.getResult().trim())){//接口发生错误
			for (TicketIndent ticketIndent : indents) {//接口调用失败
				ticketIndent.setSynchroState("04");//
				ticketIndentDao.update(ticketIndent);
			}
		}
		else{
			XStream xsStream = new XStream();
			xsStream.alias("obj", TicketIndent.class);
			xsStream.alias("root", List.class);
			List<TicketIndent> returnIndents = (List<TicketIndent>)xsStream.fromXML(ri.getResult());
			for (TicketIndent returnTicketIndent : returnIndents) {//保存退票同步结果
				TicketIndent ticketIndent = ticketIndentDao.findById(returnTicketIndent.getIndentId());
				ticketIndent.setSynchroState("05");
				ticketIndent.setStatus(returnTicketIndent.getStatus());
				Timestamp ts = CommonMethod.getTimeStamp();
				ticketIndent.setUpdateTime(ts);
				//计算两个公司分润
				Map<String,Double> map = ticketIndentService.calcPrice(returnTicketIndent.getIndentId());
				if("06".equals(ticketIndent.getStatus()) && "01".equals(ticketIndent.getCustomerType())){//团客退款
					if("01".equals(ticketIndent.getPayType().trim())){//01奖励款退款
						rewardReturn(ticketIndent);
					}else{//02、04
						//修改旅行社余额
						SysUser sysUser = sysUserDao.findById(ticketIndent.getInputer());
						Travservice baseTravelAgency = baseTravelAgencyDao.findById(sysUser.getBaseTravelAgency().getTravelAgencyId());
						
						String fundType = "";
						String pType = ticketIndent.getPayType().trim();
						
						//股份公司
						double rm = map.get("joint");//需退款金额
						//雪山公司
						double rmSnow = map.get("snow");//需退款金额
						
						double cBalance = 0d;
						double cBalanceSnow = 0d;
						
						if("02".equals(pType)){//现金支付
							baseTravelAgency.setCashBalance((baseTravelAgency.getCashBalance()==null?0:baseTravelAgency.getCashBalance())+rm);
							baseTravelAgency.setCashBalanceSnow((baseTravelAgency.getCashBalanceSnow()==null?0:baseTravelAgency.getCashBalanceSnow())+rmSnow);
							
							cBalance = baseTravelAgency.getCashBalance();
							cBalanceSnow =  baseTravelAgency.getCashBalanceSnow();
							
							fundType = "01";
						}
						if("04".equals(pType)){//预付款支付
							baseTravelAgency.setCashTopupBalance((baseTravelAgency.getCashTopupBalance()==null?0:baseTravelAgency.getCashTopupBalance())+rm);
							baseTravelAgency.setCashTopupBalanceSnow((baseTravelAgency.getCashTopupBalanceSnow()==null?0:baseTravelAgency.getCashTopupBalanceSnow())+rmSnow);
						
							cBalance = baseTravelAgency.getCashTopupBalance();
							cBalanceSnow =  baseTravelAgency.getCashTopupBalanceSnow();
							
							fundType = "03";
						}
						
//						baseTravelAgency.setUpdater(getCurrentUserId());
						baseTravelAgency.setUpdateTime(ts);
						baseTravelAgencyDao.update(baseTravelAgency);
						
						//添加交易记录
						BaseTranRec baseTranRec = new BaseTranRec();
						baseTranRec.setTicketIndent(ticketIndent);
						baseTranRec.setMoney((0-ticketIndent.getTotalPrice()));
						baseTranRec.setPaymentType("02");//支付类型:退款
						if("04".equals(pType)){
							baseTranRec.setPayType("03");//交易类型,通过预付款交易
						}else{
							baseTranRec.setPayType("02");//交易类型,通过余额交易
						}
						baseTranRec.setPaymentTime(ts);
						baseTranRec.setMoneyJoint(0-rm);//股份公司
						baseTranRec.setMoneySnow(0-rmSnow);//雪山公司
						baseTranRecService.save(baseTranRec);
						
						//添加充值记录
						BaseRechargeRec baseRechargeRec = new BaseRechargeRec();
						baseRechargeRec.setRechargeRecId(getNextKey("BASE_RECHARGE_REC", 1));
						baseRechargeRec.setBaseTravelAgency(baseTravelAgency);
						baseRechargeRec.setTicketIndent(ticketIndent);
						baseRechargeRec.setSourceDescription("退票退款");
						//股份公司
						baseRechargeRec.setCashMoney(rm);
						baseRechargeRec.setCashBalance(cBalance);
						//雪山公司
						baseRechargeRec.setCashMoneySnow(rmSnow);
						baseRechargeRec.setCashBalanceSnow(cBalanceSnow);
						baseRechargeRec.setPaymentType("02");//退款
						baseRechargeRec.setFundType(fundType);
//						baseRechargeRec.setInputer(getCurrentUserId());
						baseRechargeRec.setInputTime(ts);
						baseRechargeRecService.save(baseRechargeRec);
						
						//添加财务记录
						financeTicketAccountsService.saveReturnInfo(ticketIndent,baseTranRec,baseRechargeRec);
					}
				}
				else if("06".equals(ticketIndent.getStatus()) && "02".equals(ticketIndent.getCustomerType())){//散客直接退现金
					List<BaseTranRec> baseTranRecs = baseTranRecDao.findByProperty("ticketIndent.indentId", ticketIndent.getIndentId());
					if(baseTranRecs!=null && !baseTranRecs.isEmpty() ){
						
						BaseTranRec baseTranRec = new BaseTranRec();
						
						for(BaseTranRec rec : baseTranRecs){
							if(rec.getTrxId()!=null && !"".equals(rec.getTrxId()) &&
									//rec.getRoyaltyParameters()!=null && !"".equals(rec.getRoyaltyParameters()) && 
									"01".equals(rec.getPaymentType()) && "01".equals(rec.getPayType())){
								baseTranRec = rec;
							}
						}
						
						payFactoryService.init();
						RefundParams refundParams = payFactoryService.initRefundParams(baseTranRec,CommonMethod.format2db(ticketIndent.getTotalPrice()-ticketIndent.getPayFee()),PayFactoryService.TICKET);
						if(payFactoryService.refund(refundParams)){//退款成功
							try{
								BaseTranRec rec = new BaseTranRec();
								rec.setTrxId(baseTranRec.getTrxId());
								rec.setTicketIndent(ticketIndent);
								rec.setMoney(0-baseTranRec.getMoney());//负数
								rec.setMoneyJoint(0-baseTranRec.getMoneyJoint());
								rec.setMoneySnow(0-baseTranRec.getMoneySnow());
								rec.setPerFee(baseTranRec.getPerFee());
								rec.setPayFee(baseTranRec.getPayFee());
								rec.setPaymentType("02");//退款
								rec.setPayType("01");//交易类型,通过易宝交易
								rec.setPaymentTime(CommonMethod.getTimeStamp());
								rec.setPaymentAccount("01");
								rec.setRoyaltyParameters(baseTranRec.getRemark());
								baseTranRecService.save(rec);
								//添加财务记录
								financeTicketAccountsService.saveReturnInfo(ticketIndent,baseTranRec,null);
							}
							catch (Exception e) {
								e.printStackTrace();
								logger.error("保存退款记录出错，订单:"+ticketIndent.getIndentCode()+"，错误原因："+e.getMessage());
							}
						}
						else{//退款失败
							ticketIndent.setStatus("08");//申请退订
							ticketIndent.setSynchroState("06");//退款未同步
						}
					}
				}
				
				
				
				ticketIndentDao.merge(ticketIndent);//保存回传的退订信息
				
				//修改剩余票数
				Long userId= ticketIndent.getInputer();
				Long roleId = 0L;
				if(userId==null){
					userId = 0L;
					DetachedCriteria dcSU = DetachedCriteria.forClass(SysRole.class);
					dcSU.add(Property.forName("roleCode").eq("003"));
					List<SysRole> listSR = sysRoleDao.findByCriteria(dcSU);
					for(SysRole sr : listSR){
						roleId = sr.getRoleId();
					}
					
				}else{
					DetachedCriteria dcSU = DetachedCriteria.forClass(SysUserRole.class);
					dcSU.add(Property.forName("sysUser.userId").eq(userId));
					List<SysUserRole> listSU = sysUserRoleDao.findByCriteria(dcSU);
					for(SysUserRole sr : listSU){
						roleId = sr.getSysRole().getRoleId();
					}
				}
				DetachedCriteria dcTI = DetachedCriteria.forClass(TicketIndentDetail.class);
				dcTI.add(Property.forName("ticketIndent.indentId").eq(ticketIndent.getIndentId()));
				List<TicketIndentDetail> tidList =  ticketIndentDetailDao.findByCriteria(dcTI);
				for (TicketIndentDetail ticketIndentDetail : tidList) {
						TicketPrice ticketPrice = ticketPriceDao.findById(ticketIndentDetail.getTicketPrice().getTicketPriceId());
						if(ticketPrice.getRemainToplimit() != null && ticketPrice.getToplimit() !=null){
							int remainToplimit = ticketPrice.getRemainToplimit() == null ? 0 : ticketPrice.getRemainToplimit();
							remainToplimit = remainToplimit + ticketIndentDetail.getAmount();
							ticketPrice.setRemainToplimit(remainToplimit);
							ticketPriceDao.update(ticketPrice);
						}
						
						int updateNums = 0 - ticketIndentDetail.getAmount();
						ticketUserTypeService.updateReminTopLimit(userId, ticketPrice.getTicketType().getTicketId(), updateNums);
						ticketRoleTypeService.updateReminTopLimit(roleId, ticketPrice.getTicketType().getTicketId(), updateNums);
				}
			}
		}
	}

	private boolean checkReturn(TicketIndent indent) {
		if("02".equals(indent.getPayType()) || "04".equals(indent.getPayType())){//网银支付、预付款支付
			//1.退款金额不能大于交易金额
			List<BaseTranRec> baseTranRecs = baseTranRecDao.findByProperty("ticketIndent.indentId", indent.getIndentId());
			double returnMoney = indent.getTotalPrice();
			double payMoney = 0d;
			for (BaseTranRec tran : baseTranRecs) {
				if("01".equals(tran.getPaymentType())){//支付
					payMoney += tran.getMoney();
				}else if("02".equals(tran.getPaymentType()) || "03".equals(tran.getPaymentType())){//退款 或者冲退
					returnMoney+= tran.getMoney();
				}else{
					throw new BusinessException("错误的支付类型");
				}
			}
			if(Math.abs(returnMoney)>Math.abs(payMoney)){
				return false;
			}else{
				return true;
			}
		}else if("01".equals(indent.getPayType())){//奖励款支付
			List<BaseRechargeRec> baseRechargeRecs = baseRechargeRecDao.findByProperty("ticketIndent.indentId", indent.getIndentId());
			double returnMoney = 0d;
			double returnMoneySnow = 0d;
			double payMoney = 0d;
			double payMoneySnow = 0d;
			for (BaseRechargeRec btr : baseRechargeRecs) {
				if("01".equals(btr.getPaymentType())){//支付
					payMoney += btr.getRewardMoney();
					payMoneySnow +=btr.getRewardMoneySnow();
				}else if("02".equals(btr.getPaymentType()) || "03".equals(btr.getPaymentType())){//退款 或者冲退
					returnMoney += btr.getRewardMoney();
					returnMoneySnow += btr.getRewardMoneySnow();
				}else{
					throw new BusinessException("错误的支付类型");
				}
			}
			if(Math.abs(returnMoney)>Math.abs(payMoney) || Math.abs(returnMoneySnow)>Math.abs(returnMoney)){
				return false;
			}else{
				return true;
			}
		}else{
			throw new BusinessException("不支持的订单支付类型");
		}
		
	}

	/**
	 * 奖励款退票，整个订单退款
	 * @param ticketIndent
	 */
	private void rewardReturn(TicketIndent ticketIndent) {
		Travservice baseTravelAgency = baseTravelAgencyService.findTravelAgencyByUser(ticketIndent.getInputer());
		//1.修改奖励款余额
		BaseRewardAccount baseRewardAccount= baseRewardAccountDao.findAccount(baseTravelAgency.getTravelAgencyId(), "30", Timestamp.valueOf(ticketIndent.getUseDate().toString()), Timestamp.valueOf(ticketIndent.getUseDate().toString()));//票暂时固定30
		//计算两个公司分润
		Map<String,Double> map = ticketIndentService.calcPrice(ticketIndent.getIndentId());
		//股份公司
		double rm = map.get("joint");//需退款金额
		//雪山公司
		double rmSnow = map.get("snow");//需退款金额
		if(baseRewardAccount==null){//退票时，奖励款已过期
			return;
		}else{
			baseRewardAccount.setRewardBalance((baseRewardAccount.getRewardBalance()==null?0:baseRewardAccount.getRewardBalance()+rm));
			baseRewardAccount.setRewardBalanceSnow((baseRewardAccount.getRewardBalanceSnow()==null?0:baseRewardAccount.getRewardBalanceSnow())+rmSnow);
			
//			baseRewardAccount.setRewardBalance(baseRewardAccount.getRewardBalance()+rm);
			baseRewardAccountDao.update(baseRewardAccount);
		}
		//2.添加奖励款充值记录
		BaseRechargeRec brr = new BaseRechargeRec(getNextKey("Base_Recharge_Rec", 1));
		brr.setBaseRewardAccount(baseRewardAccount);
		brr.setBaseTravelAgency(baseTravelAgency);
		brr.setInputTime(CommonMethod.getTimeStamp());
		brr.setFundType("02");
		brr.setPaymentType("02");//02退款
		brr.setSourceDescription("奖励款退款");
		brr.setRewardBalance(baseRewardAccount.getRewardBalance());
		brr.setRewardBalanceSnow(baseRewardAccount.getRewardBalanceSnow());
		brr.setRewardMoney(rm);
		brr.setRewardMoneySnow(rmSnow);
		brr.setTicketIndent(ticketIndent);
		baseRechargeRecDao.save(brr);
		//3.奖励款退款写入财务记录
		financeTicketAccountsService.saveReturnInfo(ticketIndent,null,brr);
	}

	@Override
	public void updateRefundIndents() {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketIndent.class);
		dc.add(Property.forName("status").eq("08"));//申请退订
		dc.add(Property.forName("synchroState").eq("06"));//退款未同步
		List<TicketIndent> indents = ticketIndentDao.findByCriteria(dc);
		for (TicketIndent ticketIndent : indents) {//保存退款同步结果
			ticketIndent.setStatus("06");
			ticketIndent.setSynchroState("07");
			ticketIndent.setUpdateTime(CommonMethod.getTimeStamp());
			
			List<BaseTranRec> baseTranRecs = baseTranRecDao.findByProperty("ticketIndent.indentId", ticketIndent.getIndentId());
			if(baseTranRecs!=null && !baseTranRecs.isEmpty() ){
				BaseTranRec baseTranRec = new BaseTranRec();
				BaseTranRec baseTranRecRefund = new BaseTranRec();
				
				for(BaseTranRec rec : baseTranRecs){
					if(rec.getTrxId()!=null && !"".equals(rec.getTrxId()) &&
							rec.getRoyaltyParameters()!=null && !"".equals(rec.getRoyaltyParameters()) && 
							"01".equals(rec.getPaymentType()) && "01".equals(rec.getPayType())){
						baseTranRec = rec;
					}
					if("02".equals(rec.getPaymentType()) && "01".equals(rec.getPayType())){
						baseTranRecRefund = rec;
					}
				}
				payFactoryService.init();
				RefundParams refundParams = payFactoryService.initRefundParams(baseTranRec,CommonMethod.format2db(ticketIndent.getTotalPrice()-ticketIndent.getPayFee()),PayFactoryService.TICKET);
				if(payFactoryService.refund(refundParams)){//退款成功
					if(baseTranRecRefund.getTranRecId() == null || "".equals(baseTranRecRefund.getTranRecId())){
						BaseTranRec rec = new BaseTranRec();
						rec.setTrxId(baseTranRec.getTrxId());
						rec.setTicketIndent(ticketIndent);
						rec.setMoney(0-baseTranRec.getMoney());//负数
						rec.setMoneyJoint(0-baseTranRec.getMoneyJoint());
						rec.setMoneySnow(0-baseTranRec.getMoneySnow());
						rec.setPerFee(baseTranRec.getPerFee());
						rec.setPayFee(baseTranRec.getPayFee());
						rec.setPaymentType("02");//退款
						rec.setPayType("01");//交易类型,通过易宝交易
						rec.setPaymentTime(CommonMethod.getTimeStamp());
						rec.setPaymentAccount("01");
						rec.setRoyaltyParameters(baseTranRec.getRemark());
						baseTranRecService.save(rec);
					}else{
						baseTranRecRefund.setTrxId(baseTranRec.getTrxId());
						baseTranRecRefund.setTicketIndent(ticketIndent);
						baseTranRecRefund.setMoney(0-baseTranRec.getMoney());//负数
						baseTranRecRefund.setMoneyJoint(0-baseTranRec.getMoneyJoint());
						baseTranRecRefund.setMoneySnow(0-baseTranRec.getMoneySnow());
						baseTranRecRefund.setPerFee(baseTranRec.getPerFee());
						baseTranRecRefund.setPayFee(baseTranRec.getPayFee());
						baseTranRecRefund.setPaymentType("02");//退款
						baseTranRecRefund.setPayType("01");//交易类型,通过易宝交易
						baseTranRecRefund.setPaymentTime(CommonMethod.getTimeStamp());
						baseTranRecRefund.setPaymentAccount("01");
						baseTranRecRefund.setRoyaltyParameters(baseTranRec.getRemark());
						baseTranRecService.update(baseTranRecRefund);
					}
				}
				else{//退款失败
					ticketIndent.setStatus("08");//申请退订
					ticketIndent.setSynchroState("06");//退款未同步
				}
			}
			
			ticketIndentDao.merge(ticketIndent);//保存回传的退订信息
		}
	}

	@Override
	public void saveRefundTicketBarcodeRec(TicketBarcodeRec ticketBarcodeRec) {
		DetachedCriteria dc = DetachedCriteria.forClass(TicketIndentDetail.class);
		dc.createAlias("ticketIndent", "ticketIndent");
		dc.add(Property.forName("indentDetailId").eq(ticketBarcodeRec.getTicketIndentDetail().getIndentDetailId()));
		List<TicketIndentDetail> details = ticketIndentDetailDao.findByCriteria(dc);
		if(details!=null && !details.isEmpty()){
			TicketIndentDetail ticketIndentDetail 	= details.get(0);
			TicketIndent ticketIndent 				= ticketIndentDetail.getTicketIndent();//取得订单和对应的订单明细
			if("01".equals(ticketIndent.getCustomerType())){//团客退款
				saveRefundGroup(ticketIndent,ticketIndentDetail.getUnitPrice(),"团客现场退票退款");//团客退款
			}
			else if("02".equals(ticketIndent.getCustomerType())){
				saveRefund(ticketIndent,ticketIndentDetail.getUnitPrice(),ticketIndentDetail.getPerFee(),"散客现场退票退款",ticketBarcodeRec);
			}
			else{
				throw new BusinessException("票订单:"+ticketIndent.getIndentCode()+"客户类型未知");
			}
			ticketBarcodeRec.setStatus("05");//退票成功
			ticketBarcodeRecDao.update(ticketBarcodeRec);
		}
	}
	
	/**
	 * 散客退款
	 * @param ticketIndent 订单
	 * @param rm 退款金额
	 * @param payFee 手续费比例
	 * @param des 描述
	 * @param ticketBarcodeRec 
	 */
	public void saveRefund(TicketIndent ticketIndent, Double rm,
			Double payFee, String des, TicketBarcodeRec ticketBarcodeRec) {
		List<BaseTranRec> baseTranRecs = baseTranRecDao.findByProperty("ticketIndent.indentId", ticketIndent.getIndentId());
		if(baseTranRecs!=null && !baseTranRecs.isEmpty() ){
			BaseTranRec baseTranRec = new BaseTranRec();
			
			for(BaseTranRec rec : baseTranRecs){
				if(rec.getTrxId()!=null && !"".equals(rec.getTrxId())){
					baseTranRec = rec;
				}
			}
			payFactoryService.init();
			RefundParams refundParams = payFactoryService.initRefundParams(baseTranRec,CommonMethod.format2db(rm-(rm*payFee)),PayFactoryService.TICKET);
			if(payFactoryService.refund(refundParams)){//退款成功
				try{
					BaseTranRec rec = new BaseTranRec();	
					rec.setTrxId(baseTranRec.getTrxId());
					rec.setTicketIndent(ticketIndent);
					rec.setMoney(0-rm);//负数
					rec.setPerFee(baseTranRec.getPerFee());
					rec.setPayFee(baseTranRec.getPayFee());
					rec.setPaymentType("02");//退款
					rec.setPayType("01");//交易类型,通过易宝交易
					rec.setPaymentTime(CommonMethod.getTimeStamp());
					rec.setRemark(ticketBarcodeRec.getOriginalCode());//退票退款加入条码
					rec.setPaymentAccount("01");
					baseTranRecService.save(rec);
				}
				catch (Exception e) {
					logger.error("易宝退款成功后,保存交易记录出错,订单号:"+ticketIndent.getIndentCode());
				}
			}
			else{//退款失败
				logger.error("易宝退款失败,订单号:"+ticketIndent.getIndentCode());
				throw new BusinessException("退款失败");
			}
		}
	}
	
	/**
	 * 团客退款
	 * @param ticketIndent 订单
	 * @param rm 退款金额
	 * @param des 描述
	 */
	public void saveRefundGroup(TicketIndent ticketIndent, Double rm, String des) {
		if("01".equals(ticketIndent.getPayType().trim())){//01奖励款退款
			rewardReturnAlone( ticketIndent,  rm,  des);
		}else{//02现金
		//修改旅行社余额
		Timestamp nowTime = CommonMethod.getTimeStamp();
		SysUser sysUser = sysUserDao.findById(ticketIndent.getInputer());
		Travservice baseTravelAgency = baseTravelAgencyDao.findById(sysUser.getBaseTravelAgency().getTravelAgencyId());
		
		baseTravelAgency.setCashBalance(baseTravelAgency.getCashBalance()+rm);
//		baseTravelAgency.setUpdater(getCurrentUserId());
		baseTravelAgency.setUpdateTime(nowTime);
		baseTravelAgencyDao.update(baseTravelAgency);
		
		//添加交易记录
		BaseTranRec baseTranRec = new BaseTranRec();
		baseTranRec.setTicketIndent(ticketIndent);
		baseTranRec.setMoney((0-rm));
		baseTranRec.setPaymentType("02");//支付类型:退款
		baseTranRec.setPayType("02");//交易类型,通过余额交易
		baseTranRec.setPaymentTime(nowTime);
		baseTranRecService.save(baseTranRec);
		
		//添加充值记录
		BaseRechargeRec baseRechargeRec = new BaseRechargeRec();
		baseRechargeRec.setRechargeRecId(getNextKey("BASE_RECHARGE_REC", 1));
		baseRechargeRec.setBaseTravelAgency(baseTravelAgency);
		baseRechargeRec.setTicketIndent(ticketIndent);
		baseRechargeRec.setSourceDescription(des);
		baseRechargeRec.setCashMoney(rm);
		baseRechargeRec.setCashBalance(baseTravelAgency.getCashBalance());
		baseRechargeRec.setPaymentType("02");//退款
//		baseRechargeRec.setInputer(getCurrentUserId());
		baseRechargeRec.setInputTime(nowTime);
		baseRechargeRecService.save(baseRechargeRec);
		}
	}

	/**
	 * 单张票奖励款退款
	 * @param ticketIndent
	 * @param rm 退款金额
	 * @param des 退款描述
	 */
	private void rewardReturnAlone(TicketIndent ticketIndent, Double rm,
			String des) {
		des="单张票奖励款退款";
		Travservice baseTravelAgency = baseTravelAgencyService.findTravelAgencyByUser(ticketIndent.getInputer());
		//1.修改奖励款余额
		BaseRewardAccount baseRewardAccount= baseRewardAccountDao.findAccount(baseTravelAgency.getTravelAgencyId(), "30", Timestamp.valueOf(ticketIndent.getUseDate().toString()), Timestamp.valueOf(ticketIndent.getUseDate().toString()));//票暂时固定30
		if(baseRewardAccount==null){//退票时，奖励款已过期
			return;
		}else{
			baseRewardAccount.setRewardBalance(baseRewardAccount.getRewardBalance()+rm);
			baseRewardAccountDao.update(baseRewardAccount);
		}
		//2.添加奖励款充值记录
		BaseRechargeRec brr = new BaseRechargeRec(getNextKey("Base_Recharge_Rec", 1));
		brr.setBaseRewardAccount(baseRewardAccount);
		brr.setBaseTravelAgency(baseTravelAgency);
		brr.setInputTime(CommonMethod.getTimeStamp());
		brr.setPaymentType("02");//02退款
		brr.setSourceDescription(des);
		brr.setRewardBalance(baseRewardAccount.getRewardBalance());
		brr.setRewardMoney(rm);
		brr.setTicketIndent(ticketIndent);
		baseRechargeRecDao.save(brr);
		
	}
	
}
