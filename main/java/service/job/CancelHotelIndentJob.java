package service.job;

import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ws.hotel.comm.HotelCode;

@Service("cancelHotelIndentJob")
public class CancelHotelIndentJob {
	@Autowired
	private HotelIndentJobService hotelIndentJobService;

	private static ReentrantLock lock = null;

	private static Logger logger = Logger.getLogger("bookingService");
	/**
	 * 酒店订单申请退订检测
	 */
	public void cancelHotel() {
		if (lock == null) {
			lock = new ReentrantLock();
		}
		if (lock.tryLock()) {
			try {
				HotelCode[] hcs = HotelCode.values();
				for(HotelCode hc : hcs){
					hotelIndentJobService.saveCancelHotelIndent(hc);
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("酒店付款过期检测", e);
			} finally {
				lock.unlock();
			}
		}
	}
}
