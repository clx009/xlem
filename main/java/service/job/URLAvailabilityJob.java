package service.job;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 * 检测网络情况并写入日志
 * @author show
 *
 */
@Service("urlAvailabilityJob")
public class URLAvailabilityJob {

	private static Logger logger = Logger.getLogger("networking");
	public void networkingDetection() {
		
		Map<String,String> ipMap = new HashMap<String,String>();
		ipMap.put("192.168.2.171", "景区山门服务器");
		ipMap.put("192.168.4.160", "映雪酒店服务器");
		ipMap.put("10.0.73.198", "名人温泉票服务器");
		
		for(String ip : ipMap.keySet()){
			String name = ipMap.get(ip)+":"+ip;
			try {
				boolean tag = java.net.InetAddress.getByName(ip).isReachable(3000);
				if(tag){
					logger.info(name+" 网络正常！");
				}else{
					logger.error(name+" 网络断开！");
				}
			} catch (UnknownHostException e) {//无法找到主机
				logger.error(name+" 网络断开！");
			} catch (IOException e) {
				logger.error(name+" 网络断开！");
			}
		}
		
	}
}
