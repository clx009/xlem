package service.job;

import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.sys.sysParam.SysParamDao;

import ws.hotel.comm.HotelCode;

@Service("synRoomTypeJob")
public class SynRoomTypeJob {
	@Autowired
	private HotelIndentJobService hotelIndentJobService;
	@Autowired
	private SysParamDao sysParamDao;
	private static ReentrantLock lock = null;
	private static Logger logger = Logger.getLogger("bookingService");
	/**
	 * 同步房型信息
	 */
	public void synRoomType() {
		if (lock == null) {
			lock = new ReentrantLock();
		}
		if (lock.tryLock()) {
			try {
				String hCode = sysParamDao.findSysParam("HOTEL_SYNROOMTYPE");//查询需要同步房型的酒店
				HotelCode[] hcs = HotelCode.values();
				for(HotelCode hc : hcs){
					if(hc.name().equals(hCode)){
						hotelIndentJobService.saveSynRoomType(hc);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("同步房型信息出错", e);
			} finally {
				lock.unlock();
			}
		}
	}
}
