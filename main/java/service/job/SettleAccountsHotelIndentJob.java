package service.job;

import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ws.hotel.comm.HotelCode;

@Service("settleAccountsHotelIndentJob")
public class SettleAccountsHotelIndentJob {
	@Autowired
	private HotelIndentJobService hotelIndentJobService;

	private static ReentrantLock lock = null;

	private static Logger logger = Logger.getLogger("bookingService");
	/**
	 * 从西软同步入住后结帐房间信息
	 */
	public void settleAccountsHotel() {
		if (lock == null) {
			lock = new ReentrantLock();
		}
		if (lock.tryLock()) {
			try {
				hotelIndentJobService.saveSettleAccountsHotel(HotelCode.SCDN);
				hotelIndentJobService.saveSettleAccountsHotel(HotelCode.SD);
				hotelIndentJobService.saveSettleAccountsHotel(HotelCode.YX);
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("向西软写入酒店订单信息", e);
			} finally {
				lock.unlock();
			}
		}
	}
}
