package service.job;

import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("hotelIndentCheckJob")
public class HotelIndentCheckJob {
	@Autowired
	private HotelIndentJobService hotelIndentJobService;

	private static ReentrantLock lock = null;

	private static Logger logger = Logger.getLogger("bookingService");
	/**
	 * 检查市场部建立的订房订单，逾期自动取消订单，在取消订单前向游客和市场部订房人员发送短信
	 */
	public void hotelIndentChechk() {
		if (lock == null) {
			lock = new ReentrantLock();
		}
		if (lock.tryLock()) {
			try {
				hotelIndentJobService.saveHotelIndentCheck();
				//大于25分钟取消团客和散客的自动取消订单
				hotelIndentJobService.saveAutoCancelNopayIndent();
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("订单检测job", e);
			} finally {
				lock.unlock();
			}
		}
	}
}
