package service.job;

import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ws.hotel.comm.HotelCode;

@Service("hotelBookingLimitJob")
public class HotelBookingLimitJob {
	@Autowired
	private HotelIndentJobService hotelIndentJobService;

	private static ReentrantLock lock = null;
	private static Logger logger = Logger.getLogger("bookingService");

	/**
	 * 同步房型信息
	 */
	public void hotelBookingLimit() {
		if (lock == null) {
			lock = new ReentrantLock();
		}
		if (lock.tryLock()) {
			try {
//				HotelCode[] hcs = HotelCode.values();
//				for(HotelCode hc : hcs){
//					hotelIndentJobService.saveHotelBookingLimits(hc);
//				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("向西软写入酒店订单信息", e);
			} finally {
				lock.unlock();
			}
		}
	}
}
