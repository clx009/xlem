/**
 * 
 */
package service.job;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.HotelIndent;

import service.hotel.hotelIndent.HotelIndentService;
import ws.hotel.comm.HotelCode;

/**
 * @author 黄聪
 *
 */
@Service("refundHotelIndentJob")
public class RefundHotelIndentJob {
	@Autowired
	private HotelIndentJobService hotelIndentJobService;
	@Autowired
	private HotelIndentService hotelIndentService;
	
	private static ReentrantLock lock = null;
	
	private static Logger logger = Logger.getLogger("bookingService");
	
	public void refundIndentJob(){
		if(lock == null){
			 lock = new ReentrantLock();
		}
		if(lock.tryLock()){
			try{
				//循还逐个处理退款
				DetachedCriteria dc = DetachedCriteria.forClass(HotelIndent.class);
				dc.add(Property.forName("synchroState").eq("06"));//退款未同步
				dc.add(Property.forName("status").eq("07"));//申请退房
				List<HotelIndent> hotelIndents = hotelIndentService.findByCriteria(dc);//获取需要退款的订单
				if(hotelIndents!=null && !hotelIndents.isEmpty()){
					for (int i = 0; i < hotelIndents.size(); i++){
						HotelIndent hi = hotelIndents.get(i);
						try{
							hotelIndentJobService.saveRefundIndent(hi);
						}
						catch (Exception e) {
							logger.error("酒店订单:"+hi.getIndentCode()+"退款出错,错误原因"+e.getMessage());
						}
					}
				}
				
				//处理取消已退款订单
				DetachedCriteria dc2 = DetachedCriteria.forClass(HotelIndent.class);
				dc2.createAlias("hotel", "hotel");
				dc2.add(Property.forName("synchroState").eq("07"));//退款同步同步
				dc2.add(Property.forName("status").eq("05"));//已退订
				List<HotelIndent> hotelIndents2 = hotelIndentService.findByCriteria(dc2);//获取需要退款的订单
				Map<String,List<HotelIndent>> hiMap = new HashMap<String,List<HotelIndent>>();
				for(HotelIndent hi : hotelIndents2){
					String key = hi.getHotel().getHotelCode();
					List<HotelIndent> tempList = hiMap.get(key);
					if(tempList==null){
						tempList = new ArrayList<HotelIndent>();
					}
					tempList.add(hi);
					hiMap.put(key, tempList);
				}
				
				HotelCode[] hcs = HotelCode.values();
				for(HotelCode hc : hcs){
					hotelIndentJobService.saveCancelHotelIndent(hc,hiMap.get(hc.name()));
				}
				
			}
			catch (Exception e) {
				e.printStackTrace();
				logger.error("进行退款任务出错,错误原因:"+e.getMessage());
			}
			finally{
				lock.unlock();
			}
		}
	}
	
}
