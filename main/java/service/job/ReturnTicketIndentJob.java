/**
 * 
 */
package service.job;

import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.TicketIndent;

/**
 * @author 黄聪
 *
 */
@Service("returnTicketIndentJob")
public class ReturnTicketIndentJob {
	
	private static ReentrantLock lock = null;
	private static Logger logger = Logger.getLogger("bookingService");
	@Autowired
	private TicketIndentJobService ticketIndentJobService;
	
	/**
	 * 申请退订job
	 */
	public void returnIndentJob(){
		if(lock == null){
			 lock = new ReentrantLock();
		}
		if (lock.tryLock()) {
			try{
				List<TicketIndent> indents = ticketIndentJobService.findReturnIndents();
				if(indents==null || indents.size()==0){
					return;
				}
				for (TicketIndent indent : indents) {
					ticketIndentJobService.updateReturnIndent(indent);
				}
			}
			catch (Exception e) {
				e.printStackTrace();
				logger.error("申请退订job出错", e);
			}
			finally{
				lock.unlock();
			}
		}
	}
	
}
