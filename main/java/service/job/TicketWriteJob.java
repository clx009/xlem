package service.job;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.BaseMessageRec;
import com.xlem.dao.TicketIndent;

import common.CommonMethod;

import service.comm.baseMessageRec.BaseMessageRecService;
import service.ticket.ticketIndent.TicketIndentService;

import dao.sys.sysParam.SysParamDao;

@Service("ticketWriteJob")
public class TicketWriteJob {
	@Autowired
	private TicketIndentJobService ticketIndentJobService;
	@Autowired
	private TicketIndentService ticketIndentService;
	@Autowired
	private BaseMessageRecService baseMessageRecService;
	@Autowired
	private SysParamDao sysParamDao;
	private static ReentrantLock lock = null;
	private static Logger logger = Logger.getLogger("bookingService");
	/**
	 * 向缓存写入订单信息
	 */
	public void ticketWrite(){
		if(lock == null){
			 lock = new ReentrantLock();
		}
		if (lock.tryLock()) {
			try {
				DetachedCriteria dcIndent = DetachedCriteria.forClass(TicketIndent.class);
				dcIndent.add(Property.forName("status").in(new String[]{"02","03"}));//已支付和待审核;
				dcIndent.add(Property.forName("synchroState").eq("00"));
				List<TicketIndent> indents = ticketIndentService.findByCriteria(dcIndent);
				for (int i = 0; i < indents.size(); i++) {
					List<TicketIndent> indents2 = new ArrayList<TicketIndent>();
					TicketIndent indent = indents.get(i);
					indents2.add(indent);
					try{
						ticketIndentJobService.saveTicketWrite(indents2);
					}
					catch (Exception e) {
						
						String content = "向缓存写入单个订单信息出错,订单号:"+indent.getIndentCode();
						DetachedCriteria dc1 = DetachedCriteria.forClass(BaseMessageRec.class);
						dc1.add(Property.forName("phoneNumber").eq(sysParamDao.findSysParam("HOTEL_INDENT_ERROR_MOBILE")));
						dc1.add(Property.forName("sendTime").ge(CommonMethod.string2Time1(CommonMethod.addHour(CommonMethod.timeToString2(CommonMethod.getTimeStamp()), -1))));
						List<BaseMessageRec> setList = baseMessageRecService.findByCriteria(dc1);
						if(setList.size()<3){
							CommonMethod.sendSms(sysParamDao.findSysParam("HOTEL_INDENT_ERROR_MOBILE"), content,sysParamDao.findSysParam("USE_NEW_PLATFROM"));
							baseMessageRecService.saveMess(sysParamDao.findSysParam("HOTEL_INDENT_ERROR_MOBILE"),content);
							CommonMethod.sendMail(sysParamDao.findSysParam("HOTEL_INDENT_ERROR_EMAIL"),
									"管理员", 
									"向缓存写入单个订单信息出错,订单号:"+indent.getIndentCode()+",错误原因："+e.getMessage());
						}
						
						logger.error("向缓存写入单个订单信息出错,订单号:"+indent.getIndentCode(), e);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("向缓存写入订单信息", e);
			}
			finally{
				lock.unlock();
			}
		}
	}
}
