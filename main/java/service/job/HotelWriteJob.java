package service.job;

import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.BaseMessageRec;

import common.CommonMethod;

import dao.sys.sysParam.SysParamDao;

import service.comm.baseMessageRec.BaseMessageRecService;
import ws.hotel.comm.HotelCode;

@Service("hotelWriteJob")
public class HotelWriteJob {
	@Autowired
	private HotelIndentJobService hotelIndentJobService;
	@Autowired
	private BaseMessageRecService baseMessageRecService;
	@Autowired
	private SysParamDao sysParamDao;

	private static ReentrantLock lock = null;

	/**
	 * 向西软写入酒店订单信息
	 */
	public void ticketWrite() {
		if (lock == null) {
			lock = new ReentrantLock();
		}
		if (lock.tryLock()) {
			try {
				HotelCode[] hcs = HotelCode.values();
				for(HotelCode hc : hcs){
					hotelIndentJobService.saveHotelWrite(hc);
				}
			} catch (Exception e) {
				e.printStackTrace();
				
				String content = "酒店订单接口出错!";
				String phoneNumber = sysParamDao.findSysParam("HOTEL_INDENT_ERROR_MOBILE");
				String email = sysParamDao.findSysParam("HOTEL_INDENT_ERROR_EMAIL");
				DetachedCriteria dc1 = DetachedCriteria.forClass(BaseMessageRec.class);
				dc1.add(Property.forName("phoneNumber").eq(phoneNumber));
				dc1.add(Property.forName("sendTime").ge(CommonMethod.string2Time1(CommonMethod.addHour(CommonMethod.timeToString2(CommonMethod.getTimeStamp()), -1))));
				List<BaseMessageRec> setList = baseMessageRecService.findByCriteria(dc1);
				if(setList.size()<3){
					CommonMethod.sendSms(phoneNumber, content,sysParamDao.findSysParam("USE_NEW_PLATFROM"));
					baseMessageRecService.saveMess(phoneNumber,content);
					CommonMethod.sendMail(email,
							"管理员", 
							"酒店订单接口出错!错误原因："+e.getMessage());
				}
				
			} finally {
				lock.unlock();
			}
		}
	}
}
