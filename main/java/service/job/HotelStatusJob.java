package service.job;

import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.HotelIndent;

import service.hotel.hotelIndent.HotelIndentService;

import common.CommonMethod;

@Service("hotelStatusJob")
public class HotelStatusJob {
	@Autowired
	private HotelIndentJobService hotelIndentJobService;
	@Autowired
	private HotelIndentService hotelIndentService;

	private static ReentrantLock lock = null;

	private static Logger logger = Logger.getLogger("bookingService");
	/**
	 * 检查市场部建立的订房订单，逾期自动取消订单，在取消订单前向游客和市场部订房人员发送短信
	 */
	public void hotelStatus() {
		if (lock == null) {
			lock = new ReentrantLock();
		}
		if (lock.tryLock()) {
			try {
				System.out.println("HotelStatusJob");
				//大于并已写入现场库当天自动更改订单状态
				DetachedCriteria dc = DetachedCriteria.forClass(HotelIndent.class);
				dc.add(Property.forName("status").eq("02"));
				dc.add(Property.forName("synchroState").eq("01"));
				dc.add(Property.forName("startDate").le(CommonMethod.addDay(CommonMethod.getTimeStamp(),-1)));
				List<HotelIndent> list = hotelIndentService.findByCriteria(dc);
				
				for (HotelIndent hotelIndent : list) {
					hotelIndentJobService.saveAutoStatus(hotelIndent);
				}
				
				
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("订单状态修改job", e);
			} finally {
				lock.unlock();
			}
		}
	}
}
