/**
 * 
 */
package service.job;

import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.TicketBarcodeRec;

import service.ticket.ticketBarcodeRec.TicketBarcodeRecService;

/**
 * @author 黄聪
 *
 */
@Service("refundTicketIndentJob")
public class RefundTicketIndentJob {
	private static ReentrantLock lock = null;
	
	@Autowired
	private TicketIndentJobService ticketIndentJobService;
	@Autowired
	private TicketBarcodeRecService ticketBarcodeRecService;
	
	private static Logger logger = Logger.getLogger("bookingService");
	
	public void refundIndentJob(){
		if(lock == null){
			 lock = new ReentrantLock();
		}
		if(lock.tryLock()){
			try{
				ticketIndentJobService.updateRefundIndents();
				List<TicketBarcodeRec> ticketBarcodeRecs = ticketBarcodeRecService.findByProperty("status", "04");//退票
				if(ticketBarcodeRecs!=null && !ticketBarcodeRecs.isEmpty()){
					for (TicketBarcodeRec ticketBarcodeRec : ticketBarcodeRecs) {
						try{
							ticketIndentJobService.saveRefundTicketBarcodeRec(ticketBarcodeRec);
						}
						catch (Exception e) {
							logger.error("进行现场退票退款出错,条码:"+ticketBarcodeRec.getOriginalCode()+",错误原因:"+e.getMessage());
						}
					}
				}
			}
			catch (Exception e) {
				e.printStackTrace();
				logger.error("进行退款任务出错,错误原因:"+e.getMessage());
			}
			finally{
				lock.unlock();
			}
		}
	}
	
}
