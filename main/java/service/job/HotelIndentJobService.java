package service.job;

import java.util.List;

import com.xlem.dao.HotelIndent;
import com.xlem.dao.TicketIndent;

import ws.hotel.comm.HotelCode;

import common.service.BaseService;

public interface HotelIndentJobService extends BaseService<TicketIndent>{
	
	/**
	 * 向西软写入订单信息
	 */
	void saveHotelWrite(HotelCode hotelCode);
	
	/**
	 * 退订已付款订单
	 * @param hotelCode
	 */
	void saveCancelHotelIndent(HotelCode hotelCode);
	
	/**
	 * 退订已退款订单
	 * @param hotelCode
	 */
	void saveCancelHotelIndent(HotelCode hotelCode,List<HotelIndent> list1);
	
	/**
	 * 同步西软房型信息
	 * @param scdn
	 */
	void saveSynRoomType(HotelCode scdn);
	
	/**
	 * 同步订房上限
	 * @param scdn
	 */
	void saveHotelBookingLimit(HotelCode scdn);
	
	/**
	 * 同步退款
	 * @param hi 酒店订单
	 */
	void saveRefundIndent(HotelIndent hi);
	

	/**
	 * 检查市场部建立的订房订单，逾期自动取消订单，在取消订单前向游客和市场部订房人员发送短信
	 */
	void saveHotelIndentCheck();
	
	/**
	 * 结算入住后结清房价定单
	 * @param scdn
	 */
	void saveSettleAccountsHotel(HotelCode scdn);

	void saveAutoCancelNopayIndent();
	
	/**
	 * 同步房间上限
	 * @param scdn
	 */
	void saveHotelBookingLimits(HotelCode scdn);
	
	/**
	 * 自动修改入住日期为今天以前的订单状态
	 */
	void saveAutoStatus(HotelIndent hotelIndent);

}
