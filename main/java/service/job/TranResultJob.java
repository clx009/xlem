package service.job;

import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import service.comm.baseTranResult.BaseTranResultService;

import ws.hotel.comm.HotelCode;

@Service("tranResultJob")
public class TranResultJob {
	@Autowired
	private BaseTranResultService baseTranResultService;

	private static ReentrantLock lock = null;

	private static Logger logger = Logger.getLogger("bookingService");
	/**
	 * 同步当天交易结果数据，从支付宝读回数据
	 */
	public void readAliPay() {
		if (lock == null) {
			lock = new ReentrantLock();
		}
		if (lock.tryLock()) {
			try {
				 baseTranResultService.saveReadData();
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("从支付宝读回数据出错", e);
			} finally {
				lock.unlock();
			}
		}
	}
}
