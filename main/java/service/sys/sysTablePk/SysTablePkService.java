package service.sys.sysTablePk;

public  interface SysTablePkService {
	public String trueGetNextIndentCode(String baseCode, String prefixCode);
	
	public String trueGetNextHotelIndentCode(String baseCode, String prefixCode);

	public Long trueGetNextKey(final String upperCase, final int count);
}
