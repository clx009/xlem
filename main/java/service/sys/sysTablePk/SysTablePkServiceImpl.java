package service.sys.sysTablePk;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.sys.sysTablePk.SysTablePkDao;

@Transactional
@Service("sysTablePkService")
public  class SysTablePkServiceImpl implements SysTablePkService {
	@Autowired
	private SysTablePkDao sysTablePkDao;
	@Override
	public Long trueGetNextKey(String tablename, int count) {
		return sysTablePkDao.getNextKey(tablename, count);
	}
	@Override
	public String trueGetNextIndentCode(String baseCode, String prefixCode) {
		return sysTablePkDao.getNextIndentCode( baseCode,  prefixCode);
	}
	@Override
	public String trueGetNextHotelIndentCode(String baseCode, String prefixCode) {
		return sysTablePkDao.getNextHotelIndentCode( baseCode,  prefixCode);
	}

}
