package service.sys.sysLog;

import java.lang.reflect.Method;

import javax.annotation.Resource;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.springframework.aop.MethodBeforeAdvice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.SysLog;

import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;
import dao.sys.sysLog.SysLogDao;

@Service("sysLogService")
public class SysLogServiceImpl extends BaseServiceImpl<SysLog> implements
		SysLogService,MethodBeforeAdvice {
	@Autowired
	private SysLogDao sysLogDao;

	@Override
	@Resource(name = "sysLogDao")
	protected void initBaseDAO(BaseDao<SysLog> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<SysLog> findPageByCriteria(
			PaginationSupport<SysLog> ps, SysLog t) {
		DetachedCriteria dc = DetachedCriteria.forClass(SysLog.class);
		return sysLogDao.findPageByCriteria(ps, Order.desc("sysLogId"), dc);
	}

	@Override
	public void save(SysLog t) {
//		t.setSysLogId(getNextKey("SysLog".toUpperCase(), 1));
		sysLogDao.save(t);
	}

	@Override
	public JsonPager<SysLog> findJsonPageByCriteria(JsonPager<SysLog> jp,
			SysLog t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void before(Method method, Object[] args, Object target)
			throws Throwable {
		// TODO Auto-generated method stub
		
	}
}