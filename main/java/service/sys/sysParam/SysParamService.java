package service.sys.sysParam;
import java.util.Collection;

import com.xlem.dao.SysParam;

import common.action.EasyPager;
import common.service.BaseService;

public interface SysParamService extends BaseService<SysParam> {

	void saves(Collection<SysParam> coll);

	EasyPager<SysParam> findEasyPageByCriteria(EasyPager<SysParam> ep,
			SysParam sysParam);
	
	public String findSysParam(String paramName);
}