package service.sys.sysParam;

import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.SysParam;

import common.action.EasyPager;
import common.action.JsonPager;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.sys.sysParam.SysParamDao;

@Service("sysParamService")
public class SysParamServiceImpl extends BaseServiceImpl<SysParam> implements
		SysParamService {
	@Autowired 
	private SysParamDao sysParamDao;

	@Override
	@Resource(name = "sysParamDao")
	protected void initBaseDAO(BaseDao<SysParam> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public JsonPager<SysParam> findJsonPageByCriteria(JsonPager<SysParam> jp,
			SysParam t) {
		DetachedCriteria dc = DetachedCriteria.forClass(SysParam.class);
		dc.add(Property.forName("paramName").like(t.getParamName(), MatchMode.ANYWHERE));
		dc.add(Property.forName("paramValue").like(t.getParamValue(), MatchMode.ANYWHERE));
		dc.add(Property.forName("status").eq(t.getStatus()));
		return sysParamDao.findJsonPageByCriteria(jp, dc);
	}

	@Override
	public EasyPager<SysParam> findEasyPageByCriteria(EasyPager<SysParam> ep,
			SysParam t) {
		DetachedCriteria dc = DetachedCriteria.forClass(SysParam.class);
		dc.addOrder(Order.asc("paramName"));
		dc.addOrder(Order.asc("paramValue"));
		if(t.getParamName()!=null && !t.getParamName().trim().equals("")){
			dc.add(Property.forName("paramName").like(t.getParamName(), MatchMode.ANYWHERE));
		}
		if(t.getParamValue()!=null && !t.getParamValue().trim().equals("")){
			dc.add(Property.forName("paramValue").like(t.getParamValue(), MatchMode.ANYWHERE));
		}
		if(t.getStatus()!=null && !t.getStatus().trim().equals("")){
			dc.add(Property.forName("status").eq(t.getStatus()));
		}
		return sysParamDao.findEasyPageByCriteria(ep,dc);
	}
	
	@Override
	public void saves(Collection<SysParam> coll) {
		for (SysParam sysParam : coll) {
			if(sysParam.getParamId()==null){
				sysParam.setParamId(getNextKey("Sys_Param", 1));
				sysParamDao.save(sysParam);
			}else{
				sysParamDao.update(sysParam);
			}
		}
	}
	public String findSysParam(String paramName){
		DetachedCriteria dc = DetachedCriteria.forClass(SysParam.class);
		dc.add(Property.forName("paramName").eq(paramName.toUpperCase()));
		dc.add(Property.forName("status").eq("11"));
		List<SysParam> list = sysParamDao.findByCriteria(dc);
		if(list!=null && list.size()>0){
			return list.get(0).getParamValue();
		}
		else{
			return "";
		}
	}
}