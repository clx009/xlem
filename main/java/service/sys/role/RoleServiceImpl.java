package service.sys.role;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.Role;
import com.xlem.dao.TicketAddPay;
import com.xlem.dao.User;

import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;
import dao.sys.role.RoleDao;
import dao.sys.user.UserDao;
import dao.ticket.ticketAddPay.TicketAddPayDao;

@Transactional
@Service("roleService")
public class RoleServiceImpl extends BaseServiceImpl<Role> implements RoleService {
	@Autowired
	private RoleDao roleDao;

	@Override
	public JsonPager<Role> findJsonPageByCriteria(JsonPager<Role> jp, Role t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Autowired
	@Qualifier("roleDao")
	protected void initBaseDAO(BaseDao<Role> baseDao) {
		// TODO Auto-generated method stub
		setBaseDao(baseDao);
	}

	@Override
	public Long getRoleIdByName(String roleName) {
		// TODO Auto-generated method stub
		List<Role> roles = (List<Role>) roleDao.findByProperty("name", roleName); 
		if (roles == null) {
			return (long) -1;
		}
		else {
			return roles.get(0).getRoleId();
		}
	}



}