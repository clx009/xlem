package service.sys.role;
import java.util.List;

import com.xlem.dao.Role;

import common.service.BaseService;
public interface RoleService extends BaseService<Role> {

	Long getRoleIdByName(String roleName);

}