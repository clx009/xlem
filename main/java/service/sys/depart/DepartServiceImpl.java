package service.sys.depart;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.Depart;
import com.xlem.dao.TicketAddPay;

import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;
import dao.sys.user.UserDao;
import dao.ticket.ticketAddPay.TicketAddPayDao;

@Transactional
@Service("departService")
public class DepartServiceImpl extends BaseServiceImpl<Depart> implements DepartService {
	@Override
	@Autowired
	@Qualifier("departDao")
	protected void initBaseDAO(BaseDao<Depart> baseDao) {
		// TODO Auto-generated method stub
		setBaseDao(baseDao);
	}


	@Override
	public JsonPager<Depart> findJsonPageByCriteria(JsonPager<Depart> jp, Depart t) {
		// TODO Auto-generated method stub
		return null;
	}

}