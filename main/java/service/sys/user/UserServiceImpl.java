package service.sys.user;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.TicketAddPay;
import com.xlem.dao.User;

import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;
import dao.sys.user.UserDao;
import dao.ticket.ticketAddPay.TicketAddPayDao;

@Transactional
@Service("userService")
public class UserServiceImpl extends BaseServiceImpl<User> implements UserService {
	@Override
	@Autowired
	@Qualifier("userDao")
	protected void initBaseDAO(BaseDao<User> baseDao) {
		// TODO Auto-generated method stub
		setBaseDao(baseDao);
	}


	@Override
	public JsonPager<User> findJsonPageByCriteria(JsonPager<User> jp, User t) {
		// TODO Auto-generated method stub
		return null;
	}

}