package service.comm.baseMessageRec;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.BaseMessageRec;

import common.action.JsonPager;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.comm.baseMessageRec.BaseMessageRecDao;

@Service("baseMessageRecService")
public class BaseMessageRecServiceImpl extends
		BaseServiceImpl<BaseMessageRec> implements BaseMessageRecService {
	@Autowired
	private BaseMessageRecDao baseMessageRecDao;

	@Override
	@Resource(name = "baseMessageRecDao")
	protected void initBaseDAO(BaseDao<BaseMessageRec> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public JsonPager<BaseMessageRec> findJsonPageByCriteria(
			JsonPager<BaseMessageRec> jp, BaseMessageRec t) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @param phoneNumber 发送号码
	 * @param content 发送内容
	 */
	public void saveMess(String phoneNumber, String content){
		baseMessageRecDao.saveMess(phoneNumber, content);
	}
}