package service.comm.baseMessageRec;
import com.xlem.dao.BaseMessageRec;

import common.service.BaseService;

public interface BaseMessageRecService extends BaseService<BaseMessageRec> {
	
	Long getNextKey(String string, int i);
	
	/**
	 * @param phoneNumber 发送号码
	 * @param content 发送内容
	 */
	void saveMess(String phoneNumber, String content);
	
}