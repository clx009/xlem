package service.comm.baseTranRec;
import java.util.List;

import com.xlem.dao.BaseTranRec;
import com.xlem.dao.HotelIndent;
import com.xlem.dao.TicketCheckRec;
import com.xlem.dao.TicketIndent;

import common.action.JsonPager;
import common.service.BaseService;

public interface BaseTranRecService extends BaseService<BaseTranRec> {
	
	/**
	 * 查询需退款金额
	 * @param ticketIndent
	 * @return
	 */
	BaseTranRec findRefundMoney(TicketIndent ticketIndent);
	
	/**
	 * 退现
	 * @param baseTranRec
	 */
	void saveRefundMoney(BaseTranRec baseTranRec,TicketIndent ticketIndent);
	
	/**
	 * 查询是否已退现
	 * @param ticketIndent
	 * @return
	 */
	boolean findIsRefund(TicketIndent ticketIndent);

	/**
	 * 保存回传交易记录
	 * @param btrs
	 */
	void saveTranBack(List<BaseTranRec> btrs);
	
	/**
	 * 查询网银收益数据
	 */
	public JsonPager<TicketCheckRec> findGridByAccounts(TicketCheckRec ticketCheckRec);
	
	/**
	 * 查询酒店网银收益数据
	 */
	public JsonPager<TicketCheckRec> findHotelGridByAccounts(TicketCheckRec ticketCheckRec);

	/**
	 * 排除冲退和已被冲退的记录
	 * @return
	 */
	public List<BaseTranRec> excludeReturnBackInfo(List<BaseTranRec> ftaList);
	
	/**
	 * 保存冲退记录，有记录则冲退，没有则不做操作
	 * @param ticketIndent 订单信息
	 */
	public void saveReturnBackInfo(TicketIndent ticketIndent);
	/**
	 * 保存冲退记录，有记录则冲退，没有则不做操作
	 * @param hotelIndent 订单信息
	 */
	public void saveReturnBackInfo(HotelIndent hotelIndent);
	/**
	 * 退现信息确认页面查询明细
	 * @param t
	 * @return
	 */
	public List<BaseTranRec> findTranRecList(BaseTranRec t);
}