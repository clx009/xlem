package service.comm.baseTranRec;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.BaseTranRec;
import com.xlem.dao.HotelIndent;
import com.xlem.dao.SysUser;
import com.xlem.dao.TicketCheckRec;
import com.xlem.dao.TicketIndent;
import com.xlem.dao.TicketIndentDetail;
import com.xlem.dao.Travservice;
import com.xlem.dao.TravserviceHome;

import service.ticket.financeTicketAccounts.FinanceTicketAccountsService;

import common.BusinessException;
import common.CommonMethod;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.pay.PayFactoryService;
import common.pay.RefundParams;
import common.service.BaseServiceImpl;

import dao.comm.baseRechargeRec.BaseRechargeRecDao;
import dao.comm.baseTranRec.BaseTranRecDao;
import dao.comm.reg.baseTravelAgency.BaseTravelAgencyDao;
import dao.sys.sysUser.SysUserDao;
import dao.ticket.ticketIndent.TicketIndentDao;
import dao.ticket.ticketIndentDetail.TicketIndentDetailDao;

@Service("baseTranRecService")
public class BaseTranRecServiceImpl extends BaseServiceImpl<BaseTranRec>
		implements BaseTranRecService {
	@Autowired
	private BaseTranRecDao baseTranRecDao;
	@Autowired
	private TicketIndentDao ticketIndentDao;
	@Autowired
	private TicketIndentDetailDao ticketIndentDetailDao;
	@Autowired
	private BaseTravelAgencyDao baseTravelAgencyDao;
	@Autowired
	private BaseRechargeRecDao baseRechargeRecDao;
	@Autowired
	private PayFactoryService payFactoryService;
	@Autowired
	private SysUserDao sysUserDao;
	@Autowired
	private FinanceTicketAccountsService financeTicketAccountsService;
	
	@Override
	@Resource(name = "baseTranRecDao")
	protected void initBaseDAO(BaseDao<BaseTranRec> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<BaseTranRec> findPageByCriteria(
			PaginationSupport<BaseTranRec> ps, BaseTranRec t) {
		DetachedCriteria dc = DetachedCriteria.forClass(BaseTranRec.class);
		return baseTranRecDao.findPageByCriteria(ps,
				Order.desc("baseTranRecId"), dc);
	}
	
	@Override
	public JsonPager<TicketCheckRec> findGridByAccounts(TicketCheckRec ticketCheckRec) {
		JsonPager<TicketCheckRec> jp = new JsonPager<TicketCheckRec>();
		List<TicketCheckRec> taList = baseTranRecDao.findGridByAccounts(ticketCheckRec);
		jp.setRoot(taList);
		jp.setTotal(taList.size());
		return jp;
	}
	
	@Override
	public JsonPager<TicketCheckRec> findHotelGridByAccounts(TicketCheckRec ticketCheckRec) {
		JsonPager<TicketCheckRec> jp = new JsonPager<TicketCheckRec>();
		List<TicketCheckRec> taList = baseTranRecDao.findHotelGridByAccounts(ticketCheckRec);
		jp.setRoot(taList);
		jp.setTotal(taList.size());
		return jp;
	}

	@Override
	public void save(BaseTranRec t) {
		t.setTranRecId(getNextKey("Base_Tran_Rec".toUpperCase(), 1).toString());
		baseTranRecDao.save(t);
	}

	@Override
	public JsonPager<BaseTranRec> findJsonPageByCriteria(
			JsonPager<BaseTranRec> jp, BaseTranRec t) {
		return null;
	}

	@Override
	public BaseTranRec findRefundMoney(TicketIndent ticketIndent) {
		DetachedCriteria dc = DetachedCriteria.forClass(BaseTranRec.class);
		dc.add(Property.forName("ticketIndent.indentId").eq(ticketIndent.getIndentId()));
		dc.add(Property.forName("paymentType").eq("02"));//退款
		dc.add(Property.forName("payType").eq("02"));//退余额
		List<BaseTranRec> baseTranRecs = baseTranRecDao.findByCriteria(dc);
		return baseTranRecs.get(0);
	}

	@Override
	public void saveRefundMoney(BaseTranRec baseTranRec,TicketIndent ticketIndent) {
		DetachedCriteria dc = DetachedCriteria.forClass(BaseTranRec.class);
		dc.add(Property.forName("ticketIndent.indentId").eq(ticketIndent.getIndentId()));
		dc.add(Property.forName("paymentType").eq("01"));//支付
		dc.add(Property.forName("payType").eq("01"));//易宝交易
		List<BaseTranRec> baseTranRecs = baseTranRecDao.findByCriteria(dc);
		
		SysUser sysUser = sysUserDao.findById(ticketIndent.getInputer());
		Travservice baseTravelAgency = baseTravelAgencyDao.findById(sysUser.getBaseTravelAgency().getTravelAgencyId());
		if(findIsRefund(ticketIndent)){//查询是否已退款
			throw new BusinessException("当前订单已退现,无法重复操作!","");
		}
		if(baseTranRecs!=null && !baseTranRecs.isEmpty() ){
			BaseTranRec btr = baseTranRecs.get(0);
			baseTranRec.setTrxId(btr.getTrxId());
			//baseTranRec.setPayFee(btr.getPayFee());
			if(baseTranRec.getMoney()<0){
				baseTranRec.setMoney(0-baseTranRec.getMoney());
			}
			//现金余额不足时,无法退现
			if(baseTravelAgency.getCashBalance()<baseTranRec.getMoney()){
				throw new BusinessException("当前现金余额不足,无法退现!","");
			}
			payFactoryService.init();
//			YeePayRefundParams refundParams = new YeePayRefundParams();
//			refundParams.setPb_TrxId(btr.getTrxId());
//			refundParams.setP3_Amt(CommonMethod.format2db(baseTranRec.getMoney()-baseTranRec.getPayFee()));
//			refundParams.setP4_Cur("CNY");
//			refundParams.setP5_Desc("退票退款!");
//			RefundResult rr = PaymentForOnlineService.refundByTrxId(refundParams.getPb_TrxId(),refundParams.getP3_Amt(),refundParams.getP4_Cur(),refundParams.getP5_Desc());
			RefundParams refundParams = payFactoryService.initRefundParams(baseTranRec,CommonMethod.format2db(baseTranRec.getMoney()-baseTranRec.getPayFee()),PayFactoryService.TICKET);
//			if(rr.getR1_Code().equals("1")){//退款成功
			if(payFactoryService.refund(refundParams)){//退款成功
				Timestamp ctime = CommonMethod.getTimeStamp();
				//增加一余退款交易记录
				BaseTranRec rec = new BaseTranRec();
				rec.setTranRecId(getNextKey("Base_Tran_Rec", 1).toString());
				rec.setTrxId(btr.getTrxId());
				rec.setTicketIndent(ticketIndent);
				rec.setMoney(0-baseTranRec.getMoney());//负数
				rec.setPerFee(baseTranRec.getPerFee());
				rec.setPayFee(baseTranRec.getPayFee());
				rec.setPaymentType("02");//退款
				rec.setPayType("01");//交易类型,通过易宝交易
				rec.setRemark("退现");
				rec.setPaymentTime(ctime);
				baseTranRecDao.save(rec);
				//从当前旅行社余额中扣除
				double rm = baseTranRec.getMoney();//需扣除款金额
				baseTravelAgency.setCashBalance(baseTravelAgency.getCashBalance()-rm);
				baseTravelAgency.setUpdater(sysUser.getUserId());
				baseTravelAgency.setUpdateTime(ctime);
				baseTravelAgencyDao.update(baseTravelAgency);
				//添加充值记录
				BaseRechargeRec baseRechargeRec = new BaseRechargeRec();
				baseRechargeRec.setRechargeRecId(getNextKey("BASE_RECHARGE_REC", 1));
				baseRechargeRec.setBaseTravelAgency(baseTravelAgency);
				baseRechargeRec.setTicketIndent(ticketIndent);
				baseRechargeRec.setSourceDescription("团客退现");
				baseRechargeRec.setCashMoney(0-rm);
				baseRechargeRec.setCashBalance(baseTravelAgency.getCashBalance());
				baseRechargeRec.setPaymentType("01");//退款
				baseRechargeRec.setInputer(sysUser.getUserId());
				baseRechargeRec.setInputTime(ctime);
				baseRechargeRecDao.save(baseRechargeRec);
			}
			else{
				throw new BusinessException("退现失败,请稍后再试或联系管理员!");
			}
		}
		else{
			throw new BusinessException("当前订单不允许退现!");
		}
	}

	@Override
	public void saveTranBack(List<BaseTranRec> btrs) {
		for (BaseTranRec btr : btrs) {
			TicketIndent ti = ticketIndentDao.findById(btr.getTicketIndent().getIndentId());
			DetachedCriteria dc = DetachedCriteria.forClass(TicketIndentDetail.class);
			dc.createAlias("ticketPrice", "ticketPrice");
			dc.createAlias("ticketPrice.ticketType", "ticketType");
			dc.add(Property.forName("ticketIndent.indentId").eq(ti.getIndentId()));
			dc.addOrder(Order.asc("indentDetailId"));//按订单明细排序保证同意订单每次进来查询的顺序一致
			List<TicketIndentDetail> tidList =  ticketIndentDetailDao.findByCriteria(dc);
			
			double mSown = 0D;
			double mJoin = 0D;
			for(TicketIndentDetail tid : tidList){
				mSown += tid.getEntranceUnitPrice()*tid.getAmount();
				mJoin += tid.getRopewayUnitPrice()*tid.getAmount();
			}
			btr.setMoneySnow(mSown);
			btr.setMoneyJoint(mJoin);
			btr.setTicketIndent(ti);
			baseTranRecDao.save(btr);
			ti.setDetails(tidList);
			financeTicketAccountsService.savePayInfo(ti, btr);
		}
	}
	@Override
	public boolean findIsRefund(TicketIndent ticketIndent) {
		DetachedCriteria dc = DetachedCriteria.forClass(BaseTranRec.class);
		dc.add(Property.forName("ticketIndent.indentId").eq(ticketIndent.getIndentId()));
		dc.add(Property.forName("paymentType").eq("02"));//退款
		dc.add(Property.forName("payType").eq("01"));//易宝退余额
		List<BaseTranRec> baseTranRecs = baseTranRecDao.findByCriteria(dc);
		if(baseTranRecs!=null && !baseTranRecs.isEmpty()){
			return true;//已退款
		}
		else{
			return false;
		}
	}
	
	/**
	 * 排除冲退和已被冲退的记录
	 * @return
	 */
	@Override
	public List<BaseTranRec> excludeReturnBackInfo(List<BaseTranRec> ftaList) {
		
		List<BaseTranRec> ftaListRet = new ArrayList<BaseTranRec>();
		
		//先找出冲退和已被冲退的记录Id
		Map<String,String> excludeIds = new HashMap<String,String>();
		for(BaseTranRec fta : ftaList){
			String bId = fta.getBackTranId();
			if(bId!=null){//是冲退记录
				excludeIds.put("id_"+bId, bId);//被冲退记录ID
				String id = fta.getTranRecId();
				excludeIds.put("id_"+id, id);//冲退记录ID
			}
		}
		
		//将其他记录放到新List
		for(BaseTranRec fta : ftaList){
			String id = fta.getTranRecId();
			
			String excludeId = excludeIds.get("id_"+id);
			if(excludeId==null){
				ftaListRet.add(fta);
			}
		}
		
		return ftaListRet;
	}
	
	@Override
	public void saveReturnBackInfo(TicketIndent ticketIndent) {
		
		DetachedCriteria dc = DetachedCriteria.forClass(BaseTranRec.class);
		dc.add(Property.forName("ticketIndent.indentId").eq(ticketIndent.getIndentId()));
		List<BaseTranRec> ftaList =  baseTranRecDao.findByCriteria(dc);
		
		//排除冲退和已被冲退的记录
		List<BaseTranRec> ftaListRe =  excludeReturnBackInfo(ftaList);
		
		for(BaseTranRec ftaO : ftaListRe){
			
			BaseTranRec ftaN = new BaseTranRec(getNextKey("Base_Tran_Rec".toUpperCase(), 1).toString());
			
			ftaN.setTrxId(ftaO.getTrxId());
			ftaN.setTicketIndent(ticketIndent);
			
			ftaN.setMoney(0-ftaO.getMoney());
			ftaN.setMoneyJoint(0-ftaO.getMoneyJoint());
			ftaN.setMoneySnow(0-ftaO.getMoneySnow());
			
			ftaN.setPayFee(ftaO.getPayFee());
			ftaN.setPerFee(ftaO.getPerFee());
			
			ftaN.setRoyaltyParameters(ftaO.getRoyaltyParameters());
			
			ftaN.setCardNumber(ftaO.getCardNumber());
			ftaN.setBank(ftaO.getBank());
			ftaN.setAccountName(ftaO.getAccountName());
			
			ftaN.setPayType(ftaO.getPayType());
			
			ftaN.setPaymentType("03");
			ftaN.setPaymentAccount(ftaO.getPaymentAccount());
			ftaN.setRemark(ftaO.getRemark());
			
			ftaN.setPaymentTime(ticketIndent.getUpdateTime());
			ftaN.setTerminalNo(ftaO.getTerminalNo());
			ftaN.setBatchNo(ftaO.getBatchNo());
			ftaN.setPosSerialNo(ftaO.getPosSerialNo());
			ftaN.setOriginalCardNumber(ftaO.getOriginalCardNumber());
			
			ftaN.setBackTranId(ftaO.getTranRecId());
			
			baseTranRecDao.save(ftaN);
			
		}
		
		
	}
	
	
	@Override
	public void saveReturnBackInfo(HotelIndent hotelIndent) {
		
		DetachedCriteria dc = DetachedCriteria.forClass(BaseTranRec.class);
		dc.add(Property.forName("hotelIndent.hotelIndentId").eq(hotelIndent.getHotelIndentId()));
		List<BaseTranRec> ftaList =  baseTranRecDao.findByCriteria(dc);
		
		//排除冲退和已被冲退的记录
		List<BaseTranRec> ftaListRe =  excludeReturnBackInfo(ftaList);
		
		for(BaseTranRec ftaO : ftaListRe){
			
			BaseTranRec ftaN = new BaseTranRec(getNextKey("Base_Tran_Rec".toUpperCase(), 1).toString());
			
			ftaN.setTrxId(ftaO.getTrxId());
			ftaN.setHotelIndent(hotelIndent);
			
			ftaN.setMoney(0-ftaO.getMoney());
			
			ftaN.setPayFee(ftaO.getPayFee());
			ftaN.setPerFee(ftaO.getPerFee());
			
			ftaN.setRoyaltyParameters(ftaO.getRoyaltyParameters());
			
			ftaN.setCardNumber(ftaO.getCardNumber());
			ftaN.setBank(ftaO.getBank());
			ftaN.setAccountName(ftaO.getAccountName());
			
			ftaN.setPayType(ftaO.getPayType());
			
			ftaN.setPaymentType("03");
			ftaN.setPaymentAccount(ftaO.getPaymentAccount());
			ftaN.setRemark(ftaO.getRemark());
			
			ftaN.setPaymentTime(hotelIndent.getUpdateTime());
			ftaN.setTerminalNo(ftaO.getTerminalNo());
			ftaN.setBatchNo(ftaO.getBatchNo());
			ftaN.setPosSerialNo(ftaO.getPosSerialNo());
			ftaN.setOriginalCardNumber(ftaO.getOriginalCardNumber());
			
			ftaN.setBackTranId(ftaO.getTranRecId());
			
			baseTranRecDao.save(ftaN);
			
		}
		
	}
	
	public List<BaseTranRec> findTranRecList(BaseTranRec t){
		DetachedCriteria dc = DetachedCriteria.forClass(BaseTranRec.class);
		dc.createAlias("ticketIndent", "ticketIndent");
		dc.add(Property.forName("ticketIndent.inputer").eq(t.getTicketIndent().getInputer()));
		dc.addOrder(Order.desc("ticketIndent.inputTime"));
		dc.addOrder(Order.desc("ticketIndent.indentCode"));
		dc.addOrder(Order.desc("paymentType"));
		dc.addOrder(Order.asc("payType"));
		
		List<BaseTranRec> btrList = baseTranRecDao.findByCriteria(dc);
		
		return excludeReturnBackInfo(btrList);
	}
}