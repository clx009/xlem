package service.comm.commActivity;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.CommActivity;
import com.xlem.dao.TicketIndent;

import common.CommonMethod;
import common.action.JsonPager;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.comm.commActivity.CommActivityDao;
import dao.ticket.ticketIndent.TicketIndentDao;

@Service("commActivityService")
public class CommActivityServiceImpl extends
		BaseServiceImpl<CommActivity> implements CommActivityService {
	@Autowired
	private CommActivityDao commActivityDao;
	@Autowired
	private TicketIndentDao ticketIndentDao;
	
	@Override
	@Resource(name = "commActivityDao")
	protected void initBaseDAO(BaseDao<CommActivity> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public JsonPager<CommActivity> findJsonPageByCriteria(
			JsonPager<CommActivity> jp, CommActivity t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CommActivity findEnabledAct() {
		DetachedCriteria dc = DetachedCriteria.forClass(CommActivity.class);
		dc.add(Property.forName("status").eq("11"));
		dc.add(Property.forName("activityType").in(new String[]{"01","11"}));
		dc.add(Property.forName("userId").isNull());
		dc.addOrder(Property.forName("activityId").desc());
		List<CommActivity> commActivity = commActivityDao.findByCriteria(dc);
		if(commActivity!=null && commActivity.size()>0){
			String allLimit = "a";
			for(CommActivity cAct:commActivity){
				allLimit = allLimit + "," + cAct.getActivityLimit();
			}
			
			CommActivity ca = commActivity.get(0);
			
			DetachedCriteria dcd = DetachedCriteria.forClass(TicketIndent.class);
			dcd.add(Property.forName("inputer").eq(getCurrentUserId()));
			dcd.add(Restrictions.or(Restrictions.or(Property.forName("activityId").eq(Long.valueOf(3)),
					Property.forName("activityId").eq(Long.valueOf(4))),Property.forName("activityId").eq(Long.valueOf(5))));
			List<TicketIndent> tList = ticketIndentDao.findByCriteria(dcd);
			if(tList!=null&&tList.size()>0){//已参加活动
				ca.setRemark("感谢您参与"+ca.getActivityName()+"!您已成功抢购本次活动票不能再次抢购!");
			}else{
				ca.setRemark("");
			}
			ca.setAllLimit(allLimit);
			return ca;
		}else{
			return null;
		}
	}
	
	
	@Override
	public CommActivity findTravelAct(CommActivity cact) {
		DetachedCriteria dc = DetachedCriteria.forClass(CommActivity.class);
		dc.add(Property.forName("status").eq("11"));
		dc.add(Property.forName("activityType").in(new String[]{"01","11"}));
		dc.add(Property.forName("userId").eq(cact.getUserId()));
		dc.addOrder(Property.forName("activityId").desc());
		List<CommActivity> commActivity = commActivityDao.findByCriteria(dc);
		if(commActivity!=null && commActivity.size()>0){
			String allLimit = "a";
			for(CommActivity cAct:commActivity){
				allLimit = allLimit + "," + cAct.getActivityLimit();
			}
			
			CommActivity ca = commActivity.get(0);
			//ca.setAllLimit(allLimit);
			return ca;
		}else{
			return null;
		}
	}
	
	@Override
	public List<CommActivity> findAllTravelAct(CommActivity cact) {
		DetachedCriteria dc = DetachedCriteria.forClass(CommActivity.class);
		dc.add(Property.forName("status").eq("11"));
		dc.add(Property.forName("activityType").in(new String[]{"01","11"}));
		dc.add(Property.forName("userId").eq(cact.getUserId()));
		dc.add(Property.forName("ticketPriceId").isNotNull());
		dc.addOrder(Property.forName("activityId").asc());
		List<CommActivity> commActivityList = commActivityDao.findByCriteria(dc);

		return commActivityList;

	}
	
	@Override
	public CommActivity findEnabledActBuyGet(CommActivity commActivity,String type){
		DetachedCriteria dc = DetachedCriteria.forClass(CommActivity.class);
		dc.add(Property.forName("status").eq("11"));
		String activityType = type+"2";
		dc.add(Property.forName("activityType").eq(activityType));
		dc.add(Property.forName("activityTime").eq(CommonMethod.toStartTime(Timestamp.valueOf(commActivity.getActivityTime().toString()))));
//		dc.add(Property.forName("activityTime").ge(CommonMethod.toStartTime(commActivity.getActivityTime())));
//		
		if(commActivity.getActivityEndTime()!=null){
			dc.add(Property.forName("activityTime").eq(CommonMethod.toStartTime(CommonMethod.addDay(Timestamp.valueOf(commActivity.getActivityEndTime().toString()), -1))));
//			dc.add(Property.forName("activityTime").le(CommonMethod.toStartTime(commActivity.getActivityEndTime())));//不包含最后一天
		}
//		else{
//			dc.add(Property.forName("activityTime").le(CommonMethod.toStartTime(commActivity.getActivityTime())));
//		}
		
		if(commActivity.getRemark()!=null && !"".equals(commActivity.getRemark())){
			dc.add(Property.forName("remark").eq(commActivity.getRemark()));//酒店活动这里存酒店ID，如没有酒店ID则是所有酒店
		}
		List<CommActivity> commActivityList = commActivityDao.findByCriteria(dc);
		if(commActivityList!=null && commActivityList.size()>0){
			CommActivity ca = commActivityList.get(0);
			
			return ca;
		}else{
			return null;
		}
	}

}