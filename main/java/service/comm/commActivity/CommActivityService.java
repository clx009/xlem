package service.comm.commActivity;
import java.util.List;

import com.xlem.dao.CommActivity;

import common.service.BaseService;

public interface CommActivityService extends BaseService<CommActivity> {
	
	/**
	 * 查询当前启用抢购活动
	 * @param ticketIndent
	 * @return
	 */
	CommActivity findEnabledAct();
	
	/**
	 * 查询当前启用抢购活动(接口旅行社)
	 * @param commActivity
	 * @return
	 */
	CommActivity findTravelAct(CommActivity commActivity);
	
	/**
	 * 查询当前启用所有抢购活动(接口旅行社)
	 * @param commActivity
	 * @return
	 */
	List<CommActivity> findAllTravelAct(CommActivity commActivity);
	
	/**
	 * 查询当前启用买x送x活动
	 * @param type : 0.票、1.酒店；
	 * @return
	 */
	CommActivity findEnabledActBuyGet(CommActivity commActivity,String type);
	
}