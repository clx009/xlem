package service.comm.reg.baseTravelAgency;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.SysRole;
import com.xlem.dao.SysUser;
import com.xlem.dao.SysUserRole;
import com.xlem.dao.Travservice;
import com.xlem.dao.TravserviceHome;

import ws.client.ticketIndent.TicketIndentClient;

import common.CommonMethod;
import common.MD5;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.comm.baseMessageRec.BaseMessageRecDao;
import dao.comm.baseRechargeRec.BaseRechargeRecDao;
import dao.comm.reg.baseTravelAgency.BaseTravelAgencyDao;
import dao.sys.sysParam.SysParamDao;
import dao.sys.sysRole.SysRoleDao;
import dao.sys.sysUser.SysUserDao;
import dao.sys.sysUserRole.SysUserRoleDao;

@Service("baseTravelAgencyService")
public class BaseTravelAgencyServiceImpl extends
		BaseServiceImpl<Travservice> implements BaseTravelAgencyService {
	@Autowired
	private BaseTravelAgencyDao baseTravelAgencyDao;
	@Autowired
	private SysUserDao sysUserDao;
	@Autowired
	private SysUserRoleDao sysUserRoleDao;
	@Autowired
	private SysRoleDao sysRoleDao;
	@Autowired
	private BaseMessageRecDao baseMessageRecDao;
	@Autowired
	private BaseRechargeRecDao baseRechargeRecDao;
	@Autowired
	private SysParamDao sysParamDao;
	
	
	@Override
	@Resource(name = "baseTravelAgencyDao")
	protected void initBaseDAO(BaseDao<Travservice> baseDao) {
		setBaseDao(baseDao);
	}
	
	@Override
	public PaginationSupport<Travservice> findPageByCriteria(
			PaginationSupport<Travservice> ps, Travservice t) {
		DetachedCriteria dc = DetachedCriteria.forClass(Travservice.class);
		return baseTravelAgencyDao.findPageByCriteria(ps,
				Order.desc("baseTravelAgencyId"), dc);
	}

	@Override
	public void save(Travservice t) {
		t.setTravelAgencyId(getNextKey("Base_Travel_Agency".toUpperCase(), 1));
		t.setInputTime(CommonMethod.getTimeStamp());
		t.setCashBalance(0d);
		t.setRewardBalance(0d);
		/* SysUser sysUser = new SysUser();
		//sysUser.setUserCode(t.getUserCode());
		sysUser.setPassword(String.valueOf(Math.round(Math.random()*(999999-100000)+100000)));
		sysUser.setMailbox(t.getMailbox());
		sysUser.setBaseTravelAgency(t);
		sysUser.setUserName(t.getTravelAgencyName());
		sysUser.setUserType("03");
		sysUser.setIsAudit("00");
		sysUser.setStatus("11");
		sysUser.setUserId(getNextKey("SYS_USER".toUpperCase(), 1));
		SysUserRole sysUserRole = new SysUserRole();
		sysUserRole.setSysUser(sysUser);
		List<SysRole> srList = sysRoleDao.findByProperty("roleCode", findSysParam("ROLE_UNTRAVEL_CODE"));
		SysRole sysRole = srList.get(0);
		sysUserRole.setSysRole(sysRole);
		sysUserRole.setUserRoleId(getNextKey("SYS_USER_ROLE".toUpperCase(), 1));*/
		
		baseTravelAgencyDao.save(t);
		// sysUserDao.save(sysUser);
		// sysUserRoleDao.save(sysUserRole);
		
//		CommonMethod.sendSms(t.getPhoneNumber(), "尊敬的客户您好，您已成功申请西岭雪山电子商务平台的旅行社账号！账户号为："+
//				t.getUserCode()+"，登录网站为：***。请将打印申请表加盖公章和公司营业执照以及税务登记副本传真至028-87397568。",sysParamDao.findSysParam("USE_NEW_PLATFROM"));
		
		/*CommonMethod.sendSms(t.getPhoneNumber(), "尊敬的客户您好，您已成功申请西岭雪山电子商务平台的旅行社账号！" +
						"请将打印申请表加盖公章和公司营业执照以及税务登记副本传真至028-87397568。",sysParamDao.findSysParam("USE_NEW_PLATFROM"));
		if(t.getMailbox() != null && !t.getMailbox().isEmpty()){
			String mailCon = "您已成功申请西岭雪山电子商务平台的旅行社账号！" +
					"请将打印申请表加盖公章和公司营业执照以及税务登记副本传真至028-87397568。";
//			BaseMessageRec mes = new BaseMessageRec(getNextKey("Base_Message_Rec", 1));
//			mes.setFunctionModule(t.getTravelAgencyId().toString());
//			mes.setMessageContent(mailCon);
//			mes.setSendTime(CommonMethod.getTimeStamp());
//			mes.setPhoneNumber(t.getMailbox());
//			baseMessageRecDao.save(mes);
			CommonMethod.sendMail(t.getMailbox(),
					t.getTravelAgencyName(), 
					mailCon);
		}	*/
	}
	
	@Override
	public Map<String,Object> saveAndReturn(Travservice t) {
		long userId = 0;
		long taId = 0;
		t.setInputTime(CommonMethod.getTimeStamp());
		if (t.getUser() == null) {
			taId = getNextKey("Base_Travel_Agency".toUpperCase(), 1);
			t.setTravelAgencyId(taId);
			t.setCashBalance(0d);
			t.setRewardBalance(0d);
			SysUser sysUser = new SysUser();
			//sysUser.setUserCode(t.getUserCode());
			sysUser.setPassword(String.valueOf(Math.round(Math.random()*(999999-100000)+100000)));
			sysUser.setMailbox(t.getMailbox());
			sysUser.setBaseTravelAgency(t);
			sysUser.setUserType("03");
			sysUser.setIsAudit("00");
			sysUser.setStatus("11");
			sysUser.setUserName(t.getTravelAgencyName());
			sysUser.setPhoneNumber(t.getPhoneNumber());
			userId = getNextKey("SYS_USER".toUpperCase(), 1);
			sysUser.setUserId(userId);
			SysUserRole sysUserRole = new SysUserRole();
			sysUserRole.setSysUser(sysUser);
			List<SysRole> srList = sysRoleDao.findByProperty("roleCode", findSysParam("ROLE_UNTRAVEL_CODE"));
			SysRole sysRole = srList.get(0);
			sysUserRole.setSysRole(sysRole);
			sysUserRole.setUserRoleId(getNextKey("SYS_USER_ROLE".toUpperCase(), 1));
			
			baseTravelAgencyDao.save(t);
			sysUserDao.save(sysUser);
			sysUserRoleDao.save(sysUserRole);
			
			CommonMethod.sendSms(t.getPhoneNumber(), "尊敬的客户您好，您已成功申请西岭雪山电子商务平台的旅行社账号！" +
					"请将打印申请表加盖公章和公司营业执照以及税务登记副本传真至028-87397568。",sysParamDao.findSysParam("USE_NEW_PLATFROM"));
			try {
				if(t.getMailbox() != null && !t.getMailbox().isEmpty()){
					String mailCon = "您已成功申请西岭雪山电子商务平台的旅行社账号！" +
							"请将打印申请表加盖公章和公司营业执照以及税务登记副本传真至028-87397568。";
//					BaseMessageRec mes = new BaseMessageRec();
//					mes.setMessageRecId(getNextKey("Base_Message_Rec", 1));
//					mes.setFunctionModule(t.getTravelAgencyId().toString());
//					mes.setMessageContent(mailCon);
//					mes.setSendTime(CommonMethod.getTimeStamp());
//					mes.setPhoneNumber(t.getMailbox());
//					baseMessageRecDao.save(mes);
					CommonMethod.sendMail(t.getMailbox(),
							t.getTravelAgencyName(), mailCon);
				}	
			} catch (Exception e) {
				e.printStackTrace();
			}
			
				
		}else{
			userId = t.getUser().getUserId();
			taId = t.getTravelAgencyId();
			
			SysUser sysUser = sysUserDao.findById(userId);
			sysUser.setPassword(String.valueOf(Math.round(Math.random()*(999999-100000)+100000)));
			sysUser.setMailbox(t.getMailbox());
			sysUser.setBaseTravelAgency(t);
			sysUser.setUserType("03");
			sysUser.setIsAudit("00");
			sysUser.setStatus("11");
			sysUser.setUserName(t.getTravelAgencyName());
			sysUser.setPhoneNumber(t.getPhoneNumber());
			Travservice bta = baseTravelAgencyDao.findById(taId);
			t.setVersion(bta.getVersion());
			
			sysUserDao.update(sysUser);
			baseTravelAgencyDao.merge(t);
			
		}
		
		
		
//		CommonMethod.sendSms(t.getPhoneNumber(), "尊敬的客户您好，您已成功申请西岭雪山电子商务平台的旅行社账号！账户号为："+
//				t.getUserCode()+"，登录网站为：***。请将打印申请表加盖公章和公司营业执照以及税务登记副本传真至028-87397568。",sysParamDao.findSysParam("USE_NEW_PLATFROM"));
		
		Map<String,Object> te = new HashMap<String,Object>();
		te.put("userId", userId);
		te.put("travelAgencyId", taId);
		return te;
	}
	
	@Override
	public void deleteTravel(Travservice t) {
		if (t.getUser() != null) {
			SysUser sysUser = sysUserDao.findById(t.getUser().getUserId());
			Travservice bta = baseTravelAgencyDao.findById(sysUser.getBaseTravelAgency().getTravelAgencyId());
			List<SysUserRole> surList = sysUserRoleDao.findByProperty("sysUser.userId", t.getUser().getUserId());
			baseTravelAgencyDao.delete(bta);
			sysUserDao.delete(sysUser);
			for(SysUserRole sur : surList){
				sysUserRoleDao.delete(sur);
			}
			
			//CommonMethod.sendSms(bta.getPhoneNumber(), "尊敬的客户您好，您申请的西岭雪山电子商务平台的旅行社账号未通过审核！ ",sysParamDao.findSysParam("USE_NEW_PLATFROM"));
		}
		
		
		
//		CommonMethod.sendSms(t.getPhoneNumber(), "尊敬的客户您好，您已成功申请西岭雪山电子商务平台的旅行社账号！账户号为："+
//				t.getUserCode()+"，登录网站为：***。请将打印申请表加盖公章和公司营业执照以及税务登记副本传真至028-87397568。",sysParamDao.findSysParam("USE_NEW_PLATFROM"));
		
	}
	

	@Override
	public JsonPager<Travservice> findJsonPageByCriteria(
			JsonPager<Travservice> jp, Travservice t) {
		DetachedCriteria dc = DetachedCriteria.forClass(Travservice.class);
		dc.addOrder(Order.asc("travelAgencyId"));
		return baseTravelAgencyDao.findJsonPageByCriteria(jp, dc);
	}
	
	@Override
	public Travservice findTravelAgencyByUser(long userId) {
		return baseTravelAgencyDao.findTravelAgencyByUser(userId);
	}
	
	@Override
	public String updateApproveTravelAgency(long userId,Travservice t) {
		
			SysUser sysUser = sysUserDao.findById(userId);
			sysUser.setIsAudit("11");
			sysUser.setUserCode(baseTravelAgencyDao.getNextUserCode());
			String password = String.valueOf(Math.round(Math.random()*(999999-100000)+100000));
			sysUser.setPassword(MD5.getMD5(password));
			sysUserDao.update(sysUser);
		
			
			List<SysRole> srList = sysRoleDao.findByProperty("roleCode", findSysParam("ROLE_TRAVEL_CODE"));
			if(srList==null || srList.size()<1){
				return "false";
			}
			SysRole sysRole = srList.get(0);
			List<SysRole> srUnList = sysRoleDao.findByProperty("roleCode", findSysParam("ROLE_UNTRAVEL_CODE"));
			if(srUnList==null || srUnList.size()<1){
				return "false";
			}
			SysRole sysRoleUn = srUnList.get(0);
			
			DetachedCriteria dc = DetachedCriteria.forClass(SysUserRole.class);
			dc.add(Property.forName("sysUser.userId").eq(userId));
			dc.add(Property.forName("sysRole.roleId").eq(sysRoleUn.getRoleId()));
			
			List<SysUserRole> surList = sysUserRoleDao.findByCriteria(dc);
			if(surList==null || surList.size()<1){
				return "false";
			}
			SysUserRole sysUserRole = surList.get(0);
			sysUserRole.setSysRole(sysRole);
			sysUserRoleDao.update(sysUserRole);
			
			Travservice baseTravelAgency = baseTravelAgencyDao.findById(sysUser.getBaseTravelAgency().getTravelAgencyId());
					
			baseTravelAgency.setIsAgreement(t.getIsAgreement());
			baseTravelAgency.setDiscount(t.getDiscount());
			baseTravelAgencyDao.update(baseTravelAgency);
			
			//调用接口同步缓存数据
			TicketIndentClient client = new TicketIndentClient();
			sysUser.setBaseTravelAgency(baseTravelAgency);
			client.updateTravelAgency(sysUser);
			CommonMethod.sendSms(baseTravelAgency.getPhoneNumber(), "尊敬的客户您好，您申请的西岭雪山电子商务平台账号已成功开通！账号为："+
					sysUser.getUserCode()+"；登录初始密码为："+password+";请第一次登录后修改您的初始密码，并妥善保管账号和密码，登录的网址为：http://www.xilingxueshan.cn/booking。",sysParamDao.findSysParam("USE_NEW_PLATFROM"));
			if(sysUser.getMailbox() != null && !sysUser.getMailbox().isEmpty()){
				String mailCon = "您申请的西岭雪山电子商务平台账号已成功开通！账号为："+
						sysUser.getUserCode()+"；登录初始密码为："+password+";请第一次登录后修改您的初始密码，并妥善保管账号和密码，登录的网址为：http://www.xilingxueshan.cn/booking。";
//				BaseMessageRec mes = new BaseMessageRec(getNextKey("Base_Message_Rec", 1));
//				mes.setFunctionModule(baseTravelAgency.getTravelAgencyId().toString());
//				mes.setMessageContent(mailCon);
//				mes.setSendTime(CommonMethod.getTimeStamp());
//				mes.setPhoneNumber(sysUser.getMailbox());
//				baseMessageRecDao.save(mes);
				CommonMethod.sendMail(sysUser.getMailbox(),
						sysUser.getUserName(),mailCon );
			}		
			
		return "true";
	}
	
	@Override
	public String updateManageTravelAgency(long userId) {
		SysUser sysUser = sysUserDao.findById(userId);
		
		sysUser.setStatus("11".equals(sysUser.getStatus()) ? "00" : "11");
		sysUserDao.update(sysUser);
		
		return "true";
	}
	
	@Override
	public void update(Travservice t) {
		
		/*Travservice baseTravelAgency = baseTravelAgencyDao.findById(t.getTravelAgencyId());
		
		baseTravelAgency.setTravelAgencyName(t.getTravelAgencyName());
		baseTravelAgency.setBranchOffice(t.getBranchOffice());
		baseTravelAgency.setDepartments(t.getDepartments());
		baseTravelAgency.setAreaCode(t.getAreaCode());
		baseTravelAgency.setAddress(t.getAddress());
		baseTravelAgency.setLinkman(t.getLinkman());
		baseTravelAgency.setIdCardNumber(t.getIdCardNumber());
		baseTravelAgency.setTelNumber(t.getTelNumber());
		baseTravelAgency.setPhoneNumber(t.getPhoneNumber());
		baseTravelAgency.setRemark(t.getRemark());
		
		baseTravelAgency.setUpdateTime(CommonMethod.getTimeStamp());
		baseTravelAgency.setUpdater(getCurrentUserId());
		
		SysUser sysUser = sysUserDao.findById(getCurrentUserId());
		sysUser.setMailbox(t.getMailbox());
		sysUser.setPhoneNumber(t.getPhoneNumber());
		sysUser.setBaseTravelAgency(baseTravelAgency);*/
		
		// baseTravelAgencyDao.update(baseTravelAgency);
		// sysUserDao.update(sysUser);
		
		baseTravelAgencyDao.update(t);
		
		//调用接口同步缓存数据
		// TicketIndentClient client = new TicketIndentClient();
		// client.updateTravelAgency(sysUser);
	}
	
	@Override
	public String findByUserPhone(Travservice t) {
		DetachedCriteria dc = DetachedCriteria.forClass(SysUser.class);
		dc.add(Restrictions.or(Property.forName("baseTravelAgency.travelAgencyId").ne(t.getTravelAgencyId()),
				Property.forName("baseTravelAgency.travelAgencyId").isNull()));
//		dc.add(Restrictions.sqlRestriction(""));
		dc.add(Property.forName("phoneNumber").eq(t.getPhoneNumber()));
		if(sysUserDao.findByCriteria(dc).isEmpty()){
			return "ok";
		}else{
			return "";
		}
	}

	@Override
	public void updateType(long parseLong, Travservice baseTravelAgency) {
		Travservice tempBta = baseTravelAgencyDao.findById(parseLong);
//		tempBta.setTravelAgencyType(travelAgencyType);
		tempBta.setDiscount(baseTravelAgency.getDiscount());
		tempBta.setIsAgreement(baseTravelAgency.getIsAgreement());
		tempBta.setUpdater(getCurrentUserId());
		tempBta.setUpdateTime(CommonMethod.getTimeStamp());
		baseTravelAgencyDao.update(tempBta);
		//调用接口同步缓存数据
		TicketIndentClient client = new TicketIndentClient();
		SysUser sysUser = new SysUser();
		sysUser.setBaseTravelAgency(tempBta);
		client.updateTravelAgency(sysUser);
	}

	@Override
	public void saveRecharge(String node, Travservice t) {
		//增加充值金额到奖励款余额
		Travservice bta = baseTravelAgencyDao.findById(t.getTravelAgencyId());
		if(bta!=null && bta.getPhoneNumber()!=null){
			Timestamp ct = CommonMethod.getTimeStamp();
			bta.setRewardBalance((bta.getRewardBalance()==null?0:bta.getRewardBalance())+Double.valueOf(node));
			bta.setUpdater(getCurrentUserId());
			bta.setUpdateTime(ct);
			baseTravelAgencyDao.update(bta);
			//保存充值记录
			BaseRechargeRec baseRechargeRec = new BaseRechargeRec();
			baseRechargeRec.setRechargeRecId(getNextKey("Base_recharge_Rec", 1));
			baseRechargeRec.setBaseTravelAgency(bta);
			baseRechargeRec.setSourceDescription("奖励款充值");
			baseRechargeRec.setRewardMoney(Double.valueOf(node));
			baseRechargeRec.setRewardBalance(bta.getRewardBalance());
			baseRechargeRec.setPaymentType("03");
			baseRechargeRec.setInputer(getCurrentUserId());
			baseRechargeRec.setInputTime(ct);
			baseRechargeRecDao.save(baseRechargeRec);
		}
	}
}