package service.comm.reg.baseTravelAgency;
import java.util.Map;

import com.xlem.dao.Travservice;

import common.service.BaseService;

public interface BaseTravelAgencyService extends BaseService<Travservice> {

	public Travservice findTravelAgencyByUser(long userId);
	
	public String updateApproveTravelAgency(long userId,Travservice baseTravelAgency);
	
	public String updateManageTravelAgency(long userId);
	
	public Map<String,Object> saveAndReturn(Travservice t);

	public void deleteTravel(Travservice t);
	
	public String findByUserPhone(Travservice baseTravelAgency);
	
	/**
	 * 修改类型
	 * @param parseLong
	 * @param travelAgencyType
	 */
	public void updateType(long parseLong, Travservice baseTravelAgency);
	
	/**
	 * 保存充值额及充值记录
	 * @param node
	 * @param baseTravelAgency
	 */
	public void saveRecharge(String node, Travservice baseTravelAgency);

}