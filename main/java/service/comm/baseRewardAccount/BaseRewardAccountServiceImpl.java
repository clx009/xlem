package service.comm.baseRewardAccount;

import java.sql.Timestamp;
import java.util.Collection;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.BaseReward;
import com.xlem.dao.BaseRewardAccount;
import com.xlem.dao.BaseRewardType;
import com.xlem.dao.HotelRechargeRec;
import com.xlem.dao.Travservice;
import com.xlem.dao.TravserviceHome;

import service.hotel.financeHotelAccounts.FinanceHotelAccountsService;
import service.ticket.financeTicketAccounts.FinanceTicketAccountsService;

import common.CommonMethod;
import common.action.JsonPager;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.comm.baseRechargeRec.BaseRechargeRecDao;
import dao.comm.baseRewardAccount.BaseRewardAccountDao;
import dao.comm.baseRewardType.BaseRewardTypeDao;
import dao.comm.reg.baseTravelAgency.BaseTravelAgencyDao;
import dao.hotel.hotelRechargeRec.HotelRechargeRecDao;

@Service("baseRewardAccountService")
public class BaseRewardAccountServiceImpl extends
		BaseServiceImpl<BaseRewardAccount> implements BaseRewardAccountService {
	@Autowired
	private BaseRewardAccountDao baseRewardAccountDao;
	@Autowired
	private BaseTravelAgencyDao baseTravelAgencyDao;
	@Autowired
	private BaseRewardTypeDao baseRewardTypeDao;
	@Autowired
	private BaseRechargeRecDao baseRechargeRecDao;
	@Autowired
	private HotelRechargeRecDao hotelRechargeRecDao;
	@Autowired
	private FinanceTicketAccountsService financeTicketAccountsService;
	@Autowired
	private FinanceHotelAccountsService financeHotelAccountsService;
	
	@Override
	@Resource(name = "baseRewardAccountDao")
	protected void initBaseDAO(BaseDao<BaseRewardAccount> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public JsonPager<BaseRewardAccount> findJsonPageByCriteria(
			JsonPager<BaseRewardAccount> jp, BaseRewardAccount t) {
		DetachedCriteria dc = DetachedCriteria.forClass(BaseRewardAccount.class);
		dc.createAlias("baseRewardType", "baseRewardType");
		dc.add(Property.forName("baseTravelAgency.travelAgencyId").eq(t.getBaseTravelAgency().getTravelAgencyId()));
		dc.add(Property.forName("baseRewardType.status").eq("11"));//有效期内
		return baseRewardAccountDao.findJsonPageByCriteria(jp, dc);
	}

	@Override
	public JsonPager<BaseReward> findReward(
			JsonPager<BaseReward> jp, BaseRewardAccount baseRewardAccount) {
		return baseRewardAccountDao.findReward(jp,baseRewardAccount);
	}

	@Override
	public void saveRecharge(Collection<BaseReward> coll){
		for (BaseReward baseReward : coll) {
			Double ewardMoney = baseReward.getEwardMoney();
			Double ewardMoneySnow = baseReward.getEwardMoneySnow();
			
			if((ewardMoney!=null && ewardMoney!=0d) || (ewardMoneySnow!=null && ewardMoneySnow!=0d) ){//填写了充值金额的记录
				
				//旅行社
				Long travelAgencyId = baseReward.getTravelAgencyId();
				Travservice baseTravelAgency = baseTravelAgencyDao.findById(travelAgencyId);
				//奖励款类型
				Long rewardTypeId = baseReward.getBaseRewardType().getRewardTypeId();
				BaseRewardType baseRewardType = baseRewardTypeDao.findById(rewardTypeId);
				
				//余额
				double rewardBalance = ewardMoney==null ? 0D : ewardMoney;
				double rewardBalanceSnow = ewardMoneySnow == null ? 0D : ewardMoneySnow;
				//设置奖励款账户余额
				Long rewardAccountId = baseReward.getBaseRewardAccount().getRewardAccountId();
				BaseRewardAccount baseRewardAccount;
				if (rewardAccountId == null) {
					baseRewardAccount = new BaseRewardAccount();
					baseRewardAccount.setRewardAccountId(getNextKey("BASE_REWARD_ACCOUNT".toUpperCase(), 1));
					baseRewardAccount.setBaseTravelAgency(baseTravelAgency);
					baseRewardAccount.setBaseRewardType(baseRewardType);
					baseRewardAccount.setRewardBalance(rewardBalance);
					baseRewardAccount.setRewardBalanceSnow(rewardBalanceSnow);
					baseRewardAccountDao.save(baseRewardAccount);
					
				} else {
					baseRewardAccount = baseRewardAccountDao.findById(rewardAccountId);
					rewardBalance = baseRewardAccount.getRewardBalance()==null ? 0D : baseRewardAccount.getRewardBalance() +rewardBalance;
					rewardBalanceSnow += baseRewardAccount.getRewardBalanceSnow() == null ? 0D : baseRewardAccount.getRewardBalanceSnow();
					baseRewardAccount.setRewardBalance(rewardBalance);
					baseRewardAccount.setRewardBalanceSnow(rewardBalanceSnow);
					baseRewardAccountDao.update(baseRewardAccount);
				}
				String rewardTypeCode = baseRewardType.getRewardTypeCode();
				
				if("01".equals(rewardTypeCode)){//酒店充值记录
					HotelRechargeRec hotelRechargeRec = new HotelRechargeRec();
					hotelRechargeRec.setHotelRechargeRecId(getNextKey("HOTEL_RECHARGE_REC".toUpperCase(), 1));
					hotelRechargeRec.setBaseTravelAgency(baseTravelAgency);
					hotelRechargeRec.setBaseRewardAccount(baseRewardAccount);
					hotelRechargeRec.setSourceDescription("奖励款充值");
					hotelRechargeRec.setRewardMoney(ewardMoney);
					hotelRechargeRec.setRewardBalance(rewardBalance);
					hotelRechargeRec.setPaymentType("03");
					hotelRechargeRec.setInputer(getCurrentUserId());
					hotelRechargeRec.setInputTime(CommonMethod.getTimeStamp());
					hotelRechargeRecDao.save(hotelRechargeRec);
					
					financeHotelAccountsService.saveRewardOrCashTopUpInfo(hotelRechargeRec);
				}else{//票充值记录
					BaseRechargeRec baseRechargeRec = new BaseRechargeRec();
					baseRechargeRec.setRechargeRecId(getNextKey("BASE_RECHARGE_REC".toUpperCase(), 1));
					baseRechargeRec.setBaseTravelAgency(baseTravelAgency);
					baseRechargeRec.setBaseRewardAccount(baseRewardAccount);
					baseRechargeRec.setSourceDescription("奖励款充值");
					baseRechargeRec.setRewardMoney(ewardMoney);
					baseRechargeRec.setRewardMoneySnow(ewardMoneySnow);
					baseRechargeRec.setRewardBalance(rewardBalance);
					baseRechargeRec.setRewardBalanceSnow(rewardBalanceSnow);
					baseRechargeRec.setPaymentType("03");
					//设定资金类型    20140705  tt  start
					baseRechargeRec.setFundType("02");//02奖励款
					//设定资金类型    20140705  tt  end
					baseRechargeRec.setInputer(getCurrentUserId());
					baseRechargeRec.setInputTime(CommonMethod.getTimeStamp());
					baseRechargeRecDao.save(baseRechargeRec);
					
					financeTicketAccountsService.saveRewardOrCashTopUpInfo(baseRechargeRec);
				}
				
				baseRewardType.setIsUse("11");
				baseRewardTypeDao.update(baseRewardType);
			}
			
		}
	}
	
	@Override
	public BaseRewardAccount findAccount(Long travelAgencyId, String rewardTypeCode,Timestamp startDate,Timestamp endDate) {
		return baseRewardAccountDao.findAccount(travelAgencyId, rewardTypeCode, startDate, endDate);
	}
	
	
}