package service.comm.baseRewardAccount;
import java.sql.Timestamp;
import java.util.Collection;

import com.xlem.dao.BaseReward;
import com.xlem.dao.BaseRewardAccount;

import common.action.JsonPager;
import common.service.BaseService;

public interface BaseRewardAccountService extends BaseService<BaseRewardAccount> {

	JsonPager<BaseReward> findReward(JsonPager<BaseReward> jp,
			BaseRewardAccount baseRewardAccount);

	/**
	 * 查询旅行社对应奖励帐户
	 * @param travelAgencyId
	 * @param string
	 * @return
	 */
	BaseRewardAccount findAccount(Long travelAgencyId, String string,Timestamp startDate,Timestamp endDate);
	
	void saveRecharge(Collection<BaseReward> coll);
	
}