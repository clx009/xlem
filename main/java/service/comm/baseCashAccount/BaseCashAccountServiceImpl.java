package service.comm.baseCashAccount;
import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.BaseCashAccount;
import com.xlem.dao.BaseRechargeRec;

import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.pay.PayFactoryService;
import common.pay.PayParams;
import common.service.BaseServiceImpl;

import dao.comm.baseCashAccount.BaseCashAccountDao;
import dao.sys.sysParam.SysParamDao;
@Service("baseCashAccountService")
public class BaseCashAccountServiceImpl extends BaseServiceImpl<BaseCashAccount> implements BaseCashAccountService {
	@Autowired
	private BaseCashAccountDao baseCashAccountDao;
	@Autowired
	private PayFactoryService payFactoryService;
	@Autowired
	private SysParamDao sysParamDao;	

	@Override
	@Resource(name="baseCashAccountDao")
	protected void initBaseDAO(BaseDao<BaseCashAccount> baseDao) {
		setBaseDao(baseDao);
	}
	@Override
	public PaginationSupport<BaseCashAccount> findPageByCriteria(PaginationSupport<BaseCashAccount> ps, BaseCashAccount t) {
		DetachedCriteria dc = DetachedCriteria.forClass(BaseCashAccount.class);
		return baseCashAccountDao.findPageByCriteria(ps, Order.desc("baseCashAccountId"), dc);
	}
	@Override
	public void save(BaseCashAccount t) {
		t.setCashAccountId(getNextKey("Base_Cash_Account".toUpperCase(), 1));
		baseCashAccountDao.save(t);
	}
	@Override
	public JsonPager<BaseCashAccount> findJsonPageByCriteria(
			JsonPager<BaseCashAccount> jp, BaseCashAccount t) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public PayParams savePayParamsForRecharge(String rechargeKey, Double rechargeCash) {
		PayParams payParams = new PayParams();
		Double moneyNeedPay = rechargeCash;//需要支付金额
		
		//生成支付宝交易信息
		payFactoryService.init();
		payParams = payFactoryService.initPayParams(rechargeKey, moneyNeedPay);
		payParams.setPay_test(sysParamDao.findSysParam("PAY_TEST"));
		//生成现金交易记录
	//	DetachedCriteria dcBt = DetachedCriteria.forClass(BaseTranRec.class);
	//	dcBt.add(Property.forName("ticketIndent.indentId").eq(ticketIndent.getIndentId()));
	//	dcBt.add(Property.forName("paymentType").eq("01"));
	//	dcBt.add(Property.forName("payType").eq("01"));
	//	List<BaseTranRec> btList = baseTranRecService.findByCriteria(dcBt);
	//	for(BaseTranRec bt : btList){
	//		baseTranRecService.delete(bt);
	//	}
		
		return payParams;
	}
}