package service.comm.baseCashAccount;
import com.xlem.dao.BaseCashAccount;
import com.xlem.dao.BaseRechargeRec;

import common.pay.PayParams;
import common.service.BaseService;
public interface BaseCashAccountService extends BaseService<BaseCashAccount> {
	/**
	 * 生成充值支付参数
	 * @param rechageCash 
	 * @return
	 */
	public PayParams savePayParamsForRecharge(String rechargeKey, Double rechargeCash);

}