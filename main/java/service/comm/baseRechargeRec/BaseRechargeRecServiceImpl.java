package service.comm.baseRechargeRec;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.BaseRewardAccount;
import com.xlem.dao.TicketIndent;
import com.xlem.dao.TicketRechargeRec;
import com.xlem.dao.Travservice;
import com.xlem.dao.TravserviceHome;

import service.ticket.financeTicketAccounts.FinanceTicketAccountsService;

import common.CommonMethod;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;
import dao.comm.baseRechargeRec.BaseRechargeRecDao;
import dao.comm.reg.baseTravelAgency.BaseTravelAgencyDao;

@Service("baseRechargeRecService")
public class BaseRechargeRecServiceImpl extends
		BaseServiceImpl<BaseRechargeRec> implements BaseRechargeRecService {
	@Autowired
	private BaseRechargeRecDao baseRechargeRecDao;
	@Autowired
	private BaseTravelAgencyDao baseTravelAgencyDao;
	@Autowired
	private FinanceTicketAccountsService financeTicketAccountsService;
	
	@Override
	@Resource(name = "baseRechargeRecDao")
	protected void initBaseDAO(BaseDao<BaseRechargeRec> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<BaseRechargeRec> findPageByCriteria(
			PaginationSupport<BaseRechargeRec> ps, BaseRechargeRec t) {
		DetachedCriteria dc = DetachedCriteria.forClass(BaseRechargeRec.class);
		return baseRechargeRecDao.findPageByCriteria(ps,
				Order.desc("baseRechargeRecId"), dc);
	}

	@Override
	public void save(BaseRechargeRec t) {
		// t.setRechargeRecId(getNextKey("Base_Recharge_Rec".toUpperCase(), 1));
		baseRechargeRecDao.save(t);
	}

	public void saveTopUp(BaseRechargeRec brr){
		Double cashMoney = brr.getCashMoney();
		Double cashMoneySnow = brr.getCashMoneySnow();
		if((cashMoney!=null && cashMoney!=0d) || (cashMoneySnow!=null && cashMoneySnow!=0d)){//填写了充值金额的记录
			
			//旅行社
			Long travelAgencyId = brr.getBaseTravelAgency().getTravelAgencyId();
			Travservice baseTravelAgency = baseTravelAgencyDao.findById(travelAgencyId);
			
			//余额
			double balance = (cashMoney == null ? 0 : cashMoney) + (baseTravelAgency.getCashTopupBalance()==null?0:baseTravelAgency.getCashTopupBalance());
			double balanceSnow = (cashMoneySnow == null ? 0 : cashMoneySnow) + (baseTravelAgency.getCashTopupBalanceSnow()==null?0:baseTravelAgency.getCashTopupBalanceSnow());
			
			baseTravelAgency.setCashTopupBalance(balance);
			baseTravelAgency.setCashTopupBalanceSnow(balanceSnow);
			
			baseTravelAgencyDao.update(baseTravelAgency);
			
			BaseRechargeRec baseRechargeRec = new BaseRechargeRec();
			baseRechargeRec.setRechargeRecId(getNextKey("BASE_RECHARGE_REC".toUpperCase(), 1));
			baseRechargeRec.setBaseTravelAgency(baseTravelAgency);
			baseRechargeRec.setSourceDescription("现金充值");
			baseRechargeRec.setCashMoney(cashMoney);
			baseRechargeRec.setCashBalance(balance);
			baseRechargeRec.setCashMoneySnow(cashMoneySnow);
			baseRechargeRec.setCashBalanceSnow(balanceSnow);
			baseRechargeRec.setPaymentType("03");
			baseRechargeRec.setInputer(getCurrentUserId());
			baseRechargeRec.setInputTime(CommonMethod.getTimeStamp());
			//设定资金类型    20140704  tt  start
			baseRechargeRec.setFundType("03");//03转账（现金充值）
			//设定资金类型    20140704  tt  end
			baseRechargeRecDao.save(baseRechargeRec);
			
			//添加财务记录
			financeTicketAccountsService.saveRewardOrCashTopUpInfo(baseRechargeRec);
		}	
	/*	if(cashMoneySnow!=null && cashMoneySnow!=0d){
			balance = 0D;
			balance = cashMoneySnow + (baseTravelAgency.getCashBalanceSnow()==null?0:baseTravelAgency.getCashBalanceSnow());
			baseTravelAgency.setCashBalanceSnow(balance);
			baseTravelAgencyDao.update(baseTravelAgency);
			
			
			baseRechargeRec = new BaseRechargeRec();
			baseRechargeRec.setRechargeRecId(getNextKey("BASE_RECHARGE_REC".toUpperCase(), 1));
			baseRechargeRec.setBaseTravelAgency(baseTravelAgency);
			baseRechargeRec.setSourceDescription("现金充值");
			baseRechargeRec.setCashMoneySnow(cashMoneySnow);
			baseRechargeRec.setCashBalanceSnow(balance);
			baseRechargeRec.setPaymentType("03");
			baseRechargeRec.setInputer(getCurrentUserId());
			baseRechargeRec.setInputTime(CommonMethod.getTimeStamp());
			baseRechargeRecDao.save(baseRechargeRec);
			//添加对应的财务记录
			financeTicketAccountsService.saveRewardOrCashTopUpInfo(baseRechargeRec);
		}	*/
		
	}
	
	@Override
	public JsonPager<BaseRechargeRec> findJsonPageByCriteria(
			JsonPager<BaseRechargeRec> jp, BaseRechargeRec t) {
		DetachedCriteria dc = DetachedCriteria.forClass(BaseRechargeRec.class);
		dc.createAlias("baseTravelAgency", "baseTravelAgency");
		dc.createAlias("ticketIndent", "ticketIndent",Criteria.LEFT_JOIN);
		dc.createAlias("baseRewardAccount", "baseRewardAccount",Criteria.LEFT_JOIN);
		dc.createAlias("baseRewardAccount.baseRewardType", "baseRewardAccount.baseRewardType",Criteria.LEFT_JOIN);
		if(t.getRechargeType()!=null && !t.getRechargeType().isEmpty()){
			if(t.getRechargeType().equals("01")){//现金
			//根据资金类型查询数据    20140705  tt  start
				dc.add(Property.forName("fundType").eq("01"));
				//排除雪山公司与股份公司现金金额同时为0的数据   20150520  tt start
				DetachedCriteria dc1 = DetachedCriteria.forClass(BaseRechargeRec.class);
				dc1.setProjection(Property.forName("rechargeRecId"));
				dc1.add(Restrictions.eq("cashMoney", 0d));
				dc1.add(Restrictions.eq("cashMoneySnow", 0d));
				dc.add(Property.forName("rechargeRecId").notIn(dc1));
				//排除雪山公司与股份公司现金金额同时为0的数据   20150520  tt end
//				dc.add(Property.forName("rewardMoney").isNull());
//				dc.add(Property.forName("rewardBalance").isNull());
			}else if(t.getRechargeType().equals("02")){//奖励款
				dc.add(Property.forName("fundType").eq("02"));
//				dc.add(Property.forName("cashMoney").isNull());
//				dc.add(Property.forName("cashBalance").isNull());
			}else if(t.getRechargeType().equals("03")){//预付款
				dc.add(Property.forName("fundType").eq("03"));
			}
			
			//根据资金类型查询数据    20140705  tt  end
		}
		if(t.getBaseRewardAccount()!=null && t.getBaseRewardAccount().getBaseRewardType()!=null && t.getBaseRewardAccount().getBaseRewardType().getRewardTypeId()!=null){
			dc.add(Property.forName("baseRewardAccount.baseRewardType").eq(t.getBaseRewardAccount().getBaseRewardType()));
		}
		if(t.getStartDate()!=null){
			dc.add(Property.forName("inputTime").ge(CommonMethod.toStartTime(Timestamp.valueOf(t.getStartDate().toString()))));//开始时间>=当前时间
		}
		if(t.getEndDate()!=null){
			dc.add(Property.forName("inputTime").le(CommonMethod.toEndTime(Timestamp.valueOf(t.getEndDate().toString()))));//结束时间<=当前时间
		}
		if(t.getTicketIndent()!=null && t.getTicketIndent().getIndentCode()!=null && !t.getTicketIndent().getIndentCode().trim().isEmpty()){
			dc.add(Property.forName("ticketIndent.indentCode").like(t.getTicketIndent().getIndentCode(),MatchMode.ANYWHERE));
		}
		
		if(t.getPaymentType()!=null && !t.getPaymentType().trim().isEmpty()){
			dc.add(Property.forName("paymentType").eq(t.getPaymentType().trim()));
		}
		
		if(!"all".equals(t.getRemark())){
			dc.add(Property.forName("baseTravelAgency").eq(baseTravelAgencyDao.findTravelAgencyByUser(getCurrentUserId())));
		}else{
			if(t.getBaseTravelAgency()!=null && t.getBaseTravelAgency().getTravelAgencyId()!=-1){
				dc.add(Property.forName("baseTravelAgency").eq(baseTravelAgencyDao.findById(t.getBaseTravelAgency().getTravelAgencyId())));
			}
			
			if(t.getBaseTravelAgency()!=null && t.getBaseTravelAgency().getTravelAgencyName()!=null && !t.getBaseTravelAgency().getTravelAgencyName().trim().isEmpty()){
				dc.add(Property.forName("baseTravelAgency.travelAgencyName").like(t.getBaseTravelAgency().getTravelAgencyName().trim(),MatchMode.ANYWHERE));
			}
		}
		
		
		dc.addOrder(Order.desc("inputTime"));
		dc.addOrder(Order.desc("rechargeRecId"));
		
		return baseRechargeRecDao.findJsonPageByCriteria(jp, dc);
	}

	@Override
	public JsonPager<TicketRechargeRec> findJsonPageByCriteriaByFin(
			JsonPager<TicketRechargeRec> jp, BaseRechargeRec t) {
		return baseRechargeRecDao.findTicketRechargeRec(jp, t);
	}

	/**
	 * 重新计算奖励款记录
	 * @return
	 */
	@Override
	public void calcAgain() {
		
		//baseTravelAgencyDao
		
		DetachedCriteria dc = DetachedCriteria.forClass(BaseRechargeRec.class);
		dc.createAlias("baseTravelAgency", "baseTravelAgency");
		dc.createAlias("ticketIndent", "ticketIndent",Criteria.LEFT_JOIN);
		dc.createAlias("baseRewardAccount", "baseRewardAccount",Criteria.LEFT_JOIN);
		dc.createAlias("baseRewardAccount.baseRewardType", "baseRewardAccount.baseRewardType",Criteria.LEFT_JOIN);
		dc.add(Property.forName("cashMoney").isNull());
		dc.add(Property.forName("cashBalance").isNull());
		dc.addOrder(Order.asc("baseTravelAgency.travelAgencyId"));
		dc.addOrder(Order.asc("inputTime"));
		List<BaseRechargeRec> brrList = baseRechargeRecDao.findByCriteria(dc);
		
		long btaIdQ = 0L;
		double rew = 0D;
		for(BaseRechargeRec brr:brrList){
			
			long btaId = brr.getBaseTravelAgency().getTravelAgencyId();
			
			if(btaId!=btaIdQ){//开始下一个旅行社
				rew = 0D;
				btaIdQ = btaId;
				brr.setRewardBalance(brr.getRewardMoney());
			}else{
				brr.setRewardBalance(rew+brr.getRewardMoney());
			}
			rew = brr.getRewardBalance();
			baseRechargeRecDao.update(brr);
		}
		
		
	}
	
	/**
	 * 排除冲退和已被冲退的记录
	 * @return
	 */
	@Override
	public List<BaseRechargeRec> excludeReturnBackInfo(List<BaseRechargeRec> ftaList) {
		
		List<BaseRechargeRec> ftaListRet = new ArrayList<BaseRechargeRec>();
		
		//先找出冲退和已被冲退的记录Id
		Map<String,Long> excludeIds = new HashMap<String,Long>();
		for(BaseRechargeRec fta : ftaList){
			Long bId = fta.getBackRechargeId();
			if(bId!=null){//是冲退记录
				excludeIds.put("id_"+bId, bId);//被冲退记录ID
				Long id = fta.getRechargeRecId();
				excludeIds.put("id_"+id, id);//冲退记录ID
			}
		}
		
		//将其他记录放到新List
		for(BaseRechargeRec fta : ftaList){
			Long id = fta.getRechargeRecId();
			
			Long excludeId = excludeIds.get("id_"+id);
			if(excludeId==null){
				ftaListRet.add(fta);
			}
		}
		
		return ftaListRet;
	}
	
	@Override
	public void saveReturnBackInfo(TicketIndent ticketIndent, Travservice baseTravelAgency, BaseRewardAccount baseRewardAccount) {
		
		DetachedCriteria dc = DetachedCriteria.forClass(BaseRechargeRec.class);
		dc.add(Property.forName("ticketIndent.indentId").eq(ticketIndent.getIndentId()));
		List<BaseRechargeRec> ftaList =  baseRechargeRecDao.findByCriteria(dc);
		
		//排除冲退和已被冲退的记录
		List<BaseRechargeRec> ftaListRe =  excludeReturnBackInfo(ftaList);
		
		for(BaseRechargeRec ftaO : ftaListRe){
			
			BaseRechargeRec ftaN = new BaseRechargeRec(getNextKey("BASE_RECHARGE_REC".toUpperCase(), 1));
			
			ftaN.setBaseTravelAgency(ftaO.getBaseTravelAgency());
			ftaN.setTicketIndent(ticketIndent);
			ftaN.setBaseRewardAccount(ftaO.getBaseRewardAccount());
			ftaN.setBaseCashAccount(ftaO.getBaseCashAccount());
			ftaN.setSourceDescription(ftaO.getSourceDescription());
			ftaN.setEndDate(ftaO.getEndDate());
			
			ftaN.setCashMoney(0-ftaO.getCashMoney());
			ftaN.setCashBalance((baseTravelAgency.getCashBalance()==null?0:baseTravelAgency.getCashBalance())-ftaO.getCashMoney());
			ftaN.setCashMoneySnow(0-ftaO.getCashMoneySnow());
			ftaN.setCashBalanceSnow((baseTravelAgency.getCashBalanceSnow()==null?0:baseTravelAgency.getCashBalanceSnow())-ftaO.getCashMoneySnow());
			
			if(baseRewardAccount!=null){
				ftaN.setRewardMoney(0-ftaO.getRewardMoney());
				ftaN.setRewardBalance((baseRewardAccount.getRewardBalance()==null?0:baseRewardAccount.getRewardBalance())-ftaO.getRewardMoney());
				ftaN.setRewardMoneySnow(0-ftaO.getRewardMoneySnow());
				ftaN.setRewardBalanceSnow((baseRewardAccount.getRewardBalanceSnow()==null?0:baseRewardAccount.getRewardBalanceSnow())-ftaO.getRewardMoneySnow());
			}
			
			ftaN.setFundType(ftaO.getFundType());
			
			ftaN.setPaymentType("04");
			
			ftaN.setInputTime(ticketIndent.getUpdateTime());
			
			ftaN.setBackRechargeId(ftaO.getRechargeRecId());
			
			baseRechargeRecDao.save(ftaN);
			
		}
		
		
	}
	
	public List<BaseRechargeRec> findRechargeRecList(BaseRechargeRec t){
		DetachedCriteria dc = DetachedCriteria.forClass(BaseRechargeRec.class);
		dc.createAlias("ticketIndent", "ticketIndent",dc.LEFT_JOIN);
		//dc.createAlias("baseTravelAgency", "baseTravelAgency");
		dc.add(Property.forName("baseTravelAgency.travelAgencyId").eq(t.getBaseTravelAgency().getTravelAgencyId()));
		dc.add(Property.forName("fundType").eq("01"));
		dc.add(Property.forName("rechargeRecId").ge(1000L));
		dc.addOrder(Order.desc("inputTime"));
//		dc.addOrder(Order.desc("ticketIndent.indentCode"));
//		dc.addOrder(Order.asc("paymentType"));
//		dc.addOrder(Order.asc("payType"));
		
		List<BaseRechargeRec> btrList = baseRechargeRecDao.findByCriteria(dc);
		
		return excludeReturnBackInfo(btrList);
	}
	
	public Long getNextKey(String tableName, int i) {
		return super.getNextKey(tableName, i);
	}
}