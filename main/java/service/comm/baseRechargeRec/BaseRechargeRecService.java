package service.comm.baseRechargeRec;
import java.util.List;

import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.BaseRewardAccount;
import com.xlem.dao.TicketIndent;
import com.xlem.dao.TicketRechargeRec;
import com.xlem.dao.Travservice;

import common.action.JsonPager;
import common.service.BaseService;

public interface BaseRechargeRecService extends BaseService<BaseRechargeRec> {

	public JsonPager<TicketRechargeRec> findJsonPageByCriteriaByFin(
			JsonPager<TicketRechargeRec> jp, BaseRechargeRec t);

	public void saveTopUp(BaseRechargeRec coll);
//	public void saveTopUp(Collection<BaseRechargeRec> coll);
	
	/**
	 * 重新计算奖励款记录
	 * @return
	 */
	public void calcAgain();
	
	/**
	 * 排除冲退和已被冲退的记录
	 * @return
	 */
	public List<BaseRechargeRec> excludeReturnBackInfo(List<BaseRechargeRec> ftaList);
	
	/**
	 * 保存冲退记录，有记录则冲退，没有则不做操作
	 * @param ticketIndent 订单信息
	 */
	public void saveReturnBackInfo(TicketIndent ticketIndent, Travservice baseTravelAgency,BaseRewardAccount baseRewardAccount);
	
	/**
	 * 退现信息确认页面查询明细
	 * @param t
	 * @return
	 */
	public List<BaseRechargeRec> findRechargeRecList(BaseRechargeRec t);

	public Long getNextKey(String upperCase, int i);
}