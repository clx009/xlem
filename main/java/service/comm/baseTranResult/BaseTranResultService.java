package service.comm.baseTranResult;

import com.xlem.dao.BaseTranResult;

import common.service.BaseService;

public interface BaseTranResultService extends BaseService<BaseTranResult> {

	/**
	 * 保存从支付宝取回财务数据
	 */
	void saveReadData();

}