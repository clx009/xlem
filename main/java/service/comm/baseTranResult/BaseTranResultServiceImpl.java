package service.comm.baseTranResult;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.BaseTranResult;

import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.pay.AccountService;
import common.pay.alipay.AliAccountPager;
import common.pay.alipay.AliAccountParams;
import common.service.BaseServiceImpl;

import dao.comm.baseTranResult.BaseTranResultDao;

@Service("baseTranResultService")
public class BaseTranResultServiceImpl extends BaseServiceImpl<BaseTranResult>
		implements BaseTranResultService {
	@Autowired
	private BaseTranResultDao baseTranResultDao;
	@Autowired
	private AccountService aliAccountService;
	@Override
	@Resource(name = "baseTranResultDao")
	protected void initBaseDAO(BaseDao<BaseTranResult> baseDao) {
		setBaseDao(baseDao);
	}



	@Override
	public JsonPager<BaseTranResult> findJsonPageByCriteria(
			JsonPager<BaseTranResult> jp, BaseTranResult t) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public void saveReadData() {
		AliAccountParams params = new AliAccountParams();
		params.setGmt_start_time("2013-08-30 00:00:00");
		params.setGmt_end_time("2013-08-30 23:59:59");
		Integer pageNo = 1;
		params.setPage_no(pageNo.toString());
		AliAccountPager pager =  aliAccountService.readData(params);
		saveTranResults(pager.getAccount_page_query_result().getAccount_log_list());
		while("T".equals(pager.getIs_success()) && "T".equals(pager.getAccount_page_query_result().getHas_next_page())){
			pageNo++;
			params.setPage_no(pageNo.toString());
			pager =  aliAccountService.readData(params);
			saveTranResults(pager.getAccount_page_query_result().getAccount_log_list());
		}
	}



	private void saveTranResults(List<BaseTranResult> account_log_list) {
		long key = getNextKey("Base_Tran_Result", account_log_list.size());
		for (BaseTranResult baseTranResult : account_log_list) {
			if(baseTranResultDao.findByExample(baseTranResult).isEmpty()){
				baseTranResult.setTranResultId(key++);
				baseTranResultDao.save(baseTranResult);
			}
		}
	}
}