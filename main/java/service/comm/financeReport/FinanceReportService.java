package service.comm.financeReport;
import java.util.Collection;
import java.util.List;

import com.xlem.dao.BaseNotice;
import com.xlem.dao.FinanceReport;

import common.service.BaseService;

public interface FinanceReportService extends BaseService<FinanceReport> {
}