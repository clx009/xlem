package service.comm.financeReport;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.BaseNotice;
import com.xlem.dao.FinanceReport;

import common.CommonMethod;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.comm.baseNotice.BaseNoticeDao;
import dao.comm.financeReport.FinanceReportDao;

@Service("financeReportService")
public class FinanceReportServiceImpl extends
		BaseServiceImpl<FinanceReport> implements FinanceReportService {
	@Autowired
	private FinanceReportDao financeReportDao;

	@Override
	@Resource(name = "financeReportDao")	
	protected void initBaseDAO(BaseDao<FinanceReport> baseDao) {
		// TODO Auto-generated method stub
		setBaseDao(baseDao);
	}

	@Override
	public JsonPager<FinanceReport> findJsonPageByCriteria(JsonPager<FinanceReport> jp, FinanceReport t) {
		// TODO Auto-generated method stub
		return null;
	}

}