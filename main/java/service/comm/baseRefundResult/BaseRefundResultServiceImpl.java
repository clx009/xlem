package service.comm.baseRefundResult;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.BaseRefundResult;
import com.xlem.dao.BaseTranRec;
import com.xlem.dao.HotelIndent;
import com.xlem.dao.TicketIndent;

import service.comm.baseTranRec.BaseTranRecService;

import common.BusinessException;
import common.CommonMethod;
import common.action.JsonPager;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.comm.baseRefundResult.BaseRefundResultDao;
import dao.comm.baseTranRec.BaseTranRecDao;
import dao.hotel.hotelIndent.HotelIndentDao;
import dao.ticket.ticketIndent.TicketIndentDao;

@Service("baseRefundResultService")
public class BaseRefundResultServiceImpl extends
		BaseServiceImpl<BaseRefundResult> implements BaseRefundResultService {
	@Autowired
	private BaseRefundResultDao baseRefundResultDao;
	@Autowired
	private HotelIndentDao hotelIndentDao;
	@Autowired
	private TicketIndentDao ticketIndentDao;
	@Autowired
	private BaseTranRecDao baseTranRecDao;

	@Override
	@Resource(name = "baseRefundResultDao")
	protected void initBaseDAO(BaseDao<BaseRefundResult> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public JsonPager<BaseRefundResult> findJsonPageByCriteria(
			JsonPager<BaseRefundResult> jp, BaseRefundResult t) {
		return null;
	}

	@Override
	public void saveRefund(BaseRefundResult rr) {
		String trxId = rr.getResultDetails();
		trxId = trxId.substring(0, trxId.indexOf("^"));
		rr.setTrxId(trxId);
		rr.setRefundResultId(getNextKey("Base_Refund_Result", 1).toString());
		rr.setInputTime(CommonMethod.getTimeStamp());
		baseRefundResultDao.save(rr);
	}

	@Override
	public List<BaseRefundResult> findByIndentCode(String indentCode) {
		long hId = 0L;
		String tId = "";
    	DetachedCriteria dcTi = DetachedCriteria.forClass(TicketIndent.class);
    	dcTi.add(Property.forName("indentCode").eq(indentCode));
		List<TicketIndent> tiList = ticketIndentDao.findByCriteria(dcTi);
		if(tiList==null || tiList.size()<1){
			DetachedCriteria dcHi = DetachedCriteria.forClass(HotelIndent.class);
			dcHi.add(Property.forName("indentCode").eq(indentCode));
			List<HotelIndent> hiList = hotelIndentDao.findByCriteria(dcHi);
			for(HotelIndent hi : hiList){
				hId = hi.getHotelIndentId();
			}
		}else{
			for(TicketIndent ti : tiList){
				tId = ti.getIndentId();
			}
		}
    	
		DetachedCriteria dcBtr = DetachedCriteria.forClass(BaseTranRec.class);
		dcBtr.add(Property.forName("trxId").isNotNull());
		dcBtr.add(Property.forName("paymentType").eq("01"));
		if(!"".equals(tId)){
			dcBtr.add(Property.forName("ticketIndent.indentId").eq(tId));
		}else if(hId != 0L){
			dcBtr.add(Property.forName("hotelIndent.hotelIndentId").eq(hId));
		}else{
			throw new BusinessException("订单不存在！","");
		}
		List<BaseTranRec>  btrList = baseTranRecDao.findByCriteria(dcBtr);
		
		String trxId = "";
		for(BaseTranRec btr : btrList){
			trxId = btr.getTrxId();
		}
		
		DetachedCriteria dcBrr = DetachedCriteria.forClass(BaseRefundResult.class);
		dcBrr.add(Property.forName("trxId").eq(trxId));
		dcBrr.addOrder(Order.asc("inputTime"));
		return baseRefundResultDao.findByCriteria(dcBrr);
	}
}