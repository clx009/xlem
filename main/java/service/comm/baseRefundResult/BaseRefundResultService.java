package service.comm.baseRefundResult;
import java.util.List;

import com.xlem.dao.BaseRefundResult;

import common.service.BaseService;
public interface BaseRefundResultService extends BaseService<BaseRefundResult> {

	void saveRefund(BaseRefundResult rr);

	public List<BaseRefundResult> findByIndentCode(String indentCode);
}