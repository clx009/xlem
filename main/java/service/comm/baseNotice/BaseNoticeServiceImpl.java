package service.comm.baseNotice;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.BaseNotice;

import common.CommonMethod;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.service.BaseServiceImpl;

import dao.comm.baseNotice.BaseNoticeDao;

@Service("baseNoticeService")
public class BaseNoticeServiceImpl extends
		BaseServiceImpl<BaseNotice> implements BaseNoticeService {
	@Autowired
	private BaseNoticeDao baseNoticeDao;

	@Override
	@Resource(name = "baseNoticeDao")
	protected void initBaseDAO(BaseDao<BaseNotice> baseDao) {
		setBaseDao(baseDao);
	}

	@Override
	public PaginationSupport<BaseNotice> findPageByCriteria(
			PaginationSupport<BaseNotice> ps, BaseNotice t) {
		DetachedCriteria dc = DetachedCriteria.forClass(BaseNotice.class);
		return baseNoticeDao.findPageByCriteria(ps,
				Order.desc("noticeId"), dc);
	}

	@Override
	public void save(BaseNotice t) {
		t.setNoticeId(getNextKey("Base_Notice".toUpperCase(), 1));
		baseNoticeDao.save(t);
	}

	@Override
	public JsonPager<BaseNotice> findJsonPageByCriteria(
			JsonPager<BaseNotice> jp, BaseNotice t) {
		DetachedCriteria dc = DetachedCriteria.forClass(BaseNotice.class);
		
		if(t.getStartDate()!=null){
			dc.add(Property.forName("startDate").ge(CommonMethod.toStartTime(new Timestamp(t.getStartDate().getTime()))));//开始时间>=当前时间
		}
		if(t.getEndDate()!=null){
			dc.add(Property.forName("endDate").le(CommonMethod.toEndTime(new Timestamp(t.getEndDate().getTime()))));//结束时间<=当前时间
		}
		if(t.getNoticeTitle()!=null && t.getNoticeTitle()!=null && !t.getNoticeTitle().trim().isEmpty()){
			dc.add(Property.forName("noticeTitle").like(t.getNoticeTitle(),MatchMode.ANYWHERE));
		}
		
		dc.addOrder(Order.desc("noticeId"));
		return baseNoticeDao.findJsonPageByCriteria(jp, dc);
	}
	
	@Override
	public List<BaseNotice> findGridAll() {
		DetachedCriteria dc = DetachedCriteria.forClass(BaseNotice.class);
		
		dc.add(Property.forName("startDate").le(CommonMethod.toEndTime(CommonMethod.getTimeStamp())));
		dc.add(Property.forName("endDate").ge(CommonMethod.toStartTime(CommonMethod.getTimeStamp())));
		
		dc.addOrder(Order.desc("noticeId"));
		return baseNoticeDao.findByCriteria(dc);
	}
	
	@Override
	public void save(Collection<BaseNotice> coll) {
		for (BaseNotice baseNotice : coll) {
			if (baseNotice.getNoticeId() == null) {
				baseNotice.setNoticeId(getNextKey("base_Notice", 1));
				baseNotice.setInputer(getCurrentUserId());
				baseNotice.setInputTime(CommonMethod.getTimeStamp());
				baseNoticeDao.save(baseNotice);
			} else {
				baseNotice.setUpdater(getCurrentUserId());
				baseNotice.setUpdateTime(CommonMethod.getTimeStamp());
				baseNoticeDao.update(baseNotice);
			}
		}
	}
	
}