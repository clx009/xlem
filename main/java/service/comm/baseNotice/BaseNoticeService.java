package service.comm.baseNotice;
import java.util.Collection;
import java.util.List;

import com.xlem.dao.BaseNotice;

import common.service.BaseService;

public interface BaseNoticeService extends BaseService<BaseNotice> {

	void save(Collection<BaseNotice> coll);
	
	/**
	 * 获取有效公告
	 */
	List<BaseNotice> findGridAll();
	
}