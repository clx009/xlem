package common.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.opensymphony.xwork2.ActionContext;
import com.xlem.dao.SysUser;
import com.xlem.dao.TicketIndent;

import service.sys.sysTablePk.SysTablePkService;

import common.CusSession;
import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import dao.sys.sysParam.SysParamDao;
import dao.sys.sysUser.SysUserDao;
/**
 * @author Administrator
 *
 * @param <T>
 */

@Service
public abstract  class BaseServiceImpl<T> implements BaseService<T> {
	private BaseDao<T> baseDao;
	@Autowired
	private SysTablePkService sysTablePkService;
	@Autowired
	private SysUserDao sysUserDao;
	@Autowired
	private SysParamDao sysParamDao;
	
	@Autowired
	private HttpSession session;
	
	/**
	 * 初始化basedao
	 * @param baseDao
	 */
	abstract protected void initBaseDAO(BaseDao<T> baseDao); 
	
	public String findSysParam(String paramName){
		return sysParamDao.findSysParam(paramName);
	}
	
	public Long getNextKey(final String tablename,final int count){
		synchronized (sysTablePkService) {
			return sysTablePkService.trueGetNextKey(tablename.toUpperCase(), count);
		}
	}
		
	public String getNextIndentCode(String baseCode, String prefixCode){
		synchronized (sysTablePkService) {
			return sysTablePkService.trueGetNextIndentCode(baseCode,prefixCode);
		}
	}
	
	public String getNextHotelIndentCode(String baseCode, String prefixCode){
		synchronized (sysTablePkService) {
			return sysTablePkService.trueGetNextHotelIndentCode(baseCode,prefixCode);
		}
	}
	
	public Long getCurrentUserId() {
		return (Long)session.getAttribute("userId");
	}
	
	public Long getCurrentDeptId() {
		return (Long)session.getAttribute("deptId");
	}
	
	public Long getCurrentCompanyId() {
		return (Long)session.getAttribute("companyId");
	}
	
	public SysUser getSysUser(){
		return sysUserDao.findById(getCurrentUserId());
	}
	
	public void deletes(String[] IDS) {
		// TODO Auto-generated method stub
		for (String id : IDS) {
			baseDao.delete(baseDao.findById(Long.parseLong(id)));
		}
	}

	public List<T> findAll() {
		// TODO Auto-generated method stub
		return baseDao.findAll();
	}

	public List<T> findByExample(T t) {
		// TODO Auto-generated method stub
		return baseDao.findByExample(t);
	}

	public T findById(Long id) {
		// TODO Auto-generated method stub
		return baseDao.findById(id);
	}
	
	public T findById(String id) {
		// TODO Auto-generated method stub
		return baseDao.findById(id);
	}
	
	public List<T> findByProperty(String propertyName, Object propertyValue) {
		// TODO Auto-generated method stub
		return baseDao.findByProperty(propertyName, propertyValue);
	}
	
	public List<T> findByProperty(String propertyName, Object propertyValue,String orderPropertyName) {
		return baseDao.findByProperty(propertyName, propertyValue,orderPropertyName);
	}

	public PaginationSupport<T> findPageByCriteria(PaginationSupport<T> ps, T t) {
		throw new RuntimeException("请重写findPageByCriteria方法");
	}
	
	public List<T> findByCriteria(DetachedCriteria detachedCriteria){
		return baseDao.findByCriteria(detachedCriteria);
	}
	public List<T> findByCriteria(DetachedCriteria detachedCriteria,int firstResult,int maxResults){
		return baseDao.findByCriteria(detachedCriteria,firstResult,maxResults);
	}
	public JsonPager<T> findJsonPageByCriteria(
			JsonPager<T> jp, TicketIndent t) {
		return null;
	}
	public void save(T t) {
		baseDao.save(t);
	}
	public void saveOrUpdate(T t) {
		baseDao.saveOrUpdate(t);
	}
	public void update(T t) {
		baseDao.update(t);
	}
	public void delete(T t) {
		baseDao.delete(t);
	}
	
	public void initList(T t){
		throw new RuntimeException("请重写initList方法");
	}
	

	public BaseDao<T> getBaseDao() {
		return baseDao;
	}

	public void setBaseDao(BaseDao<T> baseDao) {
		this.baseDao = baseDao;
	}

	public boolean findPayResult(String hmac, String p1_MerId, String r0_Cmd,
			String r1_Code, String r2_TrxId, String r3_Amt, String r4_Cur,
			String r5_Pid, String r6_Order, String r7_Uid, String r8_MP,
			String r9_BType) {
		// TODO Auto-generated method stub
		return false;
	}

}
