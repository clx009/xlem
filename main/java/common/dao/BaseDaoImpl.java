package common.dao;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.List;

import javax.inject.Qualifier;
import javax.naming.InitialContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.internal.CriteriaImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import common.action.EasyPager;
import common.action.JsonPager;
import common.action.PaginationSupport;

@Transactional
public class BaseDaoImpl<T> implements BaseDao<T>{
	private Class<T> persistentClass;
	@SuppressWarnings("unchecked")
	public BaseDaoImpl() {
        Type genericSuperClass = getClass().getGenericSuperclass();

        // Get the generic super class of the super class if it's a cglib proxy
        if (getClass().getName().contains("$$EnhancerByCGLIB$$")) {
            genericSuperClass = getClass().getSuperclass().getGenericSuperclass();
        }

        Class<T> myClass = (Class<T>) ((ParameterizedType) genericSuperClass).getActualTypeArguments()[0];		
		
		// Class<T> myClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		this.persistentClass = myClass;
	}

	public Class<T> getPersistentClass() {
		return persistentClass;
	}
	
	protected final Log log = LogFactory.getLog("BaseDAO");
	protected SessionFactory sessionFactory;

	@Autowired  
	protected void initSessionFactory(SessionFactory sessionFactory){
	  this.sessionFactory = sessionFactory;   
	}  
	
	/** 带有分页查询以及排序功能的条件查询 */
	
	@SuppressWarnings("unchecked")
	public PaginationSupport<T> findPageByCriteria(final PaginationSupport<T> page, final Order order,
			final DetachedCriteria detachedCriteria) {
		Session session = sessionFactory.getCurrentSession();
		try {
			Criteria criteria = detachedCriteria.getExecutableCriteria(session);
			//查询条件
			CriteriaImpl impl = (CriteriaImpl) criteria;
			//先把Projection和OrderBy条件取出来,清空两者来执行Count操作
			Projection projection = impl.getProjection();
			criteria.setProjection(Projections.rowCount());
			int totalCount = ((Integer) criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
			criteria.setProjection(projection);
			if (projection == null) {
			    criteria.setResultTransformer(CriteriaSpecification.ROOT_ENTITY);
			}
			page.setTotalRows(totalCount);
			if (page.getTotalRows() % page.getPageSize() == 0){
				page.setTotalPages(page.getTotalRows() / page.getPageSize());
			}
			else{
				page.setTotalPages(page.getTotalRows() / page.getPageSize()+ 1);
			}
			if(page.getViewPage()>page.getTotalPages()){
				page.setCurrentPage(1);
			}else{
				page.setCurrentPage(page.getViewPage());
			}
			if (page.getTotalPages() > page.getCurrentPage()) {
				page.setHasNext(true);
			} else {
				page.setHasNext(false);
			}
			if (page.getCurrentPage() > 1) {
				page.setHasPrevious(true);
			} else {
				page.setHasPrevious(false);
			}
			if (order != null) {
				criteria.addOrder(order);
			}
			criteria.setFirstResult((page.getCurrentPage() - 1)* page.getPageSize());
			criteria.setMaxResults(page.getPageSize());
			page.setResults(criteria.list());
			return page;
		} 
		catch (HibernateException e) {
			e.printStackTrace();
			throw e;
		} 
	}

//	@Override
//	public JsonPager<T> findJsonPageByCriteria(JsonPager<T> jp,
//			DetachedCriteria detachedCriteria) {
//		// TODO Auto-generated method stub
//		return null;
//	}
/** 带有分页查询以及排序功能的条件查询 */
	
	@Override
	@SuppressWarnings("unchecked")
	public JsonPager<T> findJsonPageByCriteria(final JsonPager<T> page,final DetachedCriteria detachedCriteria) {
		Session session = sessionFactory.getCurrentSession();
		try {
			Criteria criteria = detachedCriteria.getExecutableCriteria(session);
			//查询条件
			CriteriaImpl impl = (CriteriaImpl) criteria;
			//先把Projection和OrderBy条件取出来,清空两者来执行Count操作
			Projection projection = impl.getProjection();
			criteria.setProjection(Projections.rowCount());
			int totalCount = ((Integer) criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
			criteria.setProjection(projection);
			if (projection == null) {
			    criteria.setResultTransformer(CriteriaSpecification.ROOT_ENTITY);
			}
			page.setTotal(totalCount);
			criteria.setFirstResult(Integer.parseInt(page.getStart()));
			criteria.setMaxResults(Integer.parseInt(page.getLimit()));
			page.setRoot(criteria.list());
			return page;
		} 
		catch (HibernateException e) {
			e.printStackTrace();
			throw e;
		} 
	}

	@SuppressWarnings("unchecked")
	@Override
	public EasyPager<T> findEasyPageByCriteria(final EasyPager<T> page,
			final DetachedCriteria detachedCriteria) {
		Session session = sessionFactory.getCurrentSession();
		try {
			Criteria criteria = detachedCriteria.getExecutableCriteria(session);
			//查询条件
			CriteriaImpl impl = (CriteriaImpl) criteria;
			//先把Projection和OrderBy条件取出来,清空两者来执行Count操作
			Projection projection = impl.getProjection();
			criteria.setProjection(Projections.rowCount());
			int totalCount = ((Integer) criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
			criteria.setProjection(projection);
			if (projection == null) {
			    criteria.setResultTransformer(CriteriaSpecification.ROOT_ENTITY);
			}
			page.setTotal(totalCount);
			criteria.setFirstResult(page.getPageSize()*(page.getPageNumber()-1));
			criteria.setMaxResults(page.getPageSize());

			page.setRows(criteria.list());
			return page;
		} 
		catch (HibernateException e) {
			e.printStackTrace();
			throw e;
		} 
	}
	
	public void delete(T t) {
		try {
			sessionFactory.getCurrentSession().delete(t);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		log.debug("finding all "+persistentClass.getSimpleName()+" instances");
		try {
			String queryString = "from "+persistentClass.getSimpleName();
			Query queryObject = sessionFactory.getCurrentSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<T> findByExample(T t) {
		log.debug("finding "+persistentClass.getSimpleName()+" instance by example");
		try {
			List<T> results = sessionFactory.getCurrentSession()
						.createCriteria(this.persistentClass)
						.add(Example.create(t)).list();
				log.debug("find by example successful, result size: "
						+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public T findById(Long id) {
		log.debug("getting "+persistentClass.getSimpleName()+" instance with id: " + id);
		try {
			T instance = (T) sessionFactory.getCurrentSession()
					.get(getPersistentClass(), id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public T findById(String id) {
		log.debug("getting "+persistentClass.getSimpleName()+" instance with id: " + id);
		try {
			T instance = (T) sessionFactory.getCurrentSession()
					.get(getPersistentClass(), id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<T> findByProperty(String propertyName, Object value) {
		log.debug("finding "+persistentClass.getSimpleName()+" instance with property: "
				+ propertyName + ", value: " + value);
		try {
			String queryString = "from "+persistentClass.getSimpleName()+" as model where model."
					+ propertyName + "= ?";
			Query queryObject = sessionFactory.getCurrentSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<T> findByProperty(String propertyName, Object value,String orderName) {
		log.debug("finding "+persistentClass.getSimpleName()+" instance with property: "
				+ propertyName + ", value: " + value + " order by "+orderName);
		try {
			String queryString = "from "+persistentClass.getSimpleName()+" as model where model."
					+ propertyName + "= ?"+" order by "+orderName;
			Query queryObject = sessionFactory.getCurrentSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}
	@SuppressWarnings("unchecked")
	public T merge(T t) {
		log.debug("merging "+persistentClass.getSimpleName()+" instance");
		try {
			T result = (T) sessionFactory.getCurrentSession().merge(t);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void save(T t) {
		log.debug("saving "+persistentClass.getSimpleName()+" instance");
		try {
			sessionFactory.getCurrentSession().save(t);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}
	
	public void saveOrUpdate(T t) {
		log.debug("saveOrUpdate "+t.getClass().getSimpleName()+" instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(t);
			log.debug("saveOrUpdate successful");
		} catch (RuntimeException re) {
			log.error("saveOrUpdate failed", re);
			throw re;
		}
	}
	
	public void update(T t){
		log.debug("update "+t.getClass().getSimpleName()+" update");
		try {
			sessionFactory.getCurrentSession().update(t);
			log.debug("update successful");
		} catch (RuntimeException re) {
			log.error("update failed", re);
			re.printStackTrace();
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<T> findByCriteria(DetachedCriteria detachedCriteria){
		Session session = sessionFactory.getCurrentSession();
        Criteria criteria = detachedCriteria.getExecutableCriteria(session);
		return (List<T>)criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<T> findByCriteria(DetachedCriteria detachedCriteria, int firstResult, int maxResults){
		Session session = sessionFactory.getCurrentSession();
        Criteria criteria = detachedCriteria.getExecutableCriteria(session);
        criteria.setFirstResult(firstResult);
        criteria.setMaxResults(maxResults);
		return (List<T>)criteria.list();
	}

	@Override
	public String getNextCode(String tablename, String columName,String baseCode,String where) {
		String hql = "select max("+columName+") from "+tablename;
		if(where!=null&&!where.trim().equals("")){
			hql+=" where " + where;
		}
		
		Query queryObject = sessionFactory.getCurrentSession().createQuery(hql);
		List<String> list = queryObject.list(); 
		if(list==null || list.get(0)==null){
			return baseCode;
		}else{
			String code = list.get(0);
			String str = String.format("%0"+baseCode.length()+"d", Integer.parseInt(code)+1);
			return str;
		}
	}
}
