package common.pay;

import common.dao.BaseBean;

/**
 * @author STKJ08
 *
 */
public class PayResult extends BaseBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String notify_id;

	public String getNotify_id() {
		return notify_id;
	}

	public void setNotify_id(String notify_id) {
		this.notify_id = notify_id;
	}
}
