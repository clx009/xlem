package common.pay.alipay;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.BaseRefundResult;
import com.xlem.dao.BaseTranRec;
import com.xlem.dao.HotelIndent;
import com.xlem.dao.SysTablePk;
import com.xlem.dao.TicketIndent;

// import com.yeepay.Configuration;
import common.BeanToMapUtil;
import common.BusinessException;
import common.CommonMethod;
import common.MD5;
import common.pay.PayParams;
import common.pay.PayService;
import common.pay.RefundParams;

import dao.sys.sysParam.SysParamDao;
import dao.sys.sysTablePk.SysTablePkDao;

@Transactional
@Service("aliBankPayService")
public class AliBankPayServiceImpl implements PayService {

	@Autowired
	private SysParamDao sysParamDao;
	@Autowired
	private SysTablePkDao sysTablePkDao;
	
	private static Logger logger = Logger.getLogger("bookingService");
	
	@Override
	public PayParams initPayParams(TicketIndent ticketIndent,
			Double moneyNeedPay, Double moneyNeedPaySub) {
		return initPayParams(ticketIndent.getIndentCode(), moneyNeedPay,1,ticketIndent.getDefaultbank(),ticketIndent, moneyNeedPaySub);
	}


	@Override
	public PayParams initPayParams(HotelIndent hotelIndent, Double moneyNeedPay) {
		return initPayParams(hotelIndent.getIndentCode(), moneyNeedPay,2,hotelIndent.getDefaultbank(),hotelIndent, 0D);
	}
	
	private PayParams initPayParams(String indentCode, Double moneyNeedPay,int type,String defaultbank,Object indent, Double moneyNeedPaySub) {
		String keyValue = CommonMethod.formatString(sysParamDao.findSysParam("ALIPAY_KEY_VALUE",type==2 ? "02" : "01"));
		AliBankPayParams alipayParams = new AliBankPayParams();
		//支付宝网关手续费费率
		String alipayPerFee = CommonMethod.formatString(sysParamDao.findSysParam("ALIPAY_PER_FEE"));
		alipayParams.setService("create_direct_pay_by_user");
		alipayParams.setPartner(sysParamDao.findSysParam("partner",type==2 ? "02" : "01"));
		alipayParams.set_input_charset("utf-8");
		alipayParams.setSign_type("MD5");		
		alipayParams.setNotify_url(Configuration.getInstance().getValue(type==2 ? "notify_urlForHotel" : "notify_url"));
		
		alipayParams.setReturn_url(Configuration.getInstance().getValue(type==2 ? "return_urlForHotel" : "return_url"));		
		
		/**2014-02-11解决进入支付宝页面未支付，金额改变后再次进入支付宝页面时报“交易金额与交易中不一致”的错误
		 * Out_trade_no改为  ‘indentCode’ 加 ‘_’ 加 ‘时间戳’
		 */
		//String Out_trade_no = indentCode + "_" + CommonMethod.getCurrentSystemDate();
		/**
		 * 2014-03-03避免重复支付的情况Out_trade_no的后缀改为支付金额(去掉小数点)。
		 */
		String moneyNeedPayStr = moneyNeedPay.toString().replace(".", "");
		String Out_trade_no = indentCode + "_" + moneyNeedPayStr;
		
		alipayParams.setOut_trade_no(Out_trade_no);
		alipayParams.setSubject(type==2 ? "hotel" : "ticket");
		alipayParams.setPayment_type("1");
		if(type==2){//酒店订单设置超时时间
			HotelIndent hi = (HotelIndent)indent;
			Long mi = Long.parseLong(sysParamDao.findSysParam("HOTEL_INDENT_AUTO_CANCEL_TIME"));//酒店订单自动取消时间（分钟）
			//实际时间比配置时间减少2分钟
			mi = mi - 2;
			//计算剩余时间
			//下订单到现在已过多长时间
			Long myhour = CommonMethod.diffMin(Timestamp.valueOf(hi.getInputTime().toString()), CommonMethod.getTimeStamp());
			//还剩余多少时间
			mi = mi - myhour;
			if(mi>30L){
				mi = 30L;
			}
			alipayParams.setIt_b_pay(mi+"m");
		}
		
		//测试账号：alipay-test12@alipay.com 正式账号：xlxs123456@126.com
		alipayParams.setSeller_email(sysParamDao.findSysParam("seller_email",type==2 ? "02" : "01"));		
		alipayParams.setTotal_fee(moneyNeedPay.toString());
		
		//在选择银行的情况下设置银行参数
		if(defaultbank!=null && !defaultbank.isEmpty()){
			alipayParams.setPaymethod("bankPay");
			alipayParams.setDefaultbank(defaultbank);
		}
		
		if(moneyNeedPaySub!=null && moneyNeedPaySub>0D){//有需要分润的金额
			
			alipayParams.setRoyalty_type("10");
			
			double paySub = moneyNeedPaySub*(1-Double.valueOf(alipayPerFee));
			
			String royalty_parameters = sysParamDao.findSysParam("seller_email", "03")+"^"
			+CommonMethod.format2db(paySub).toString()+"^"+(type==2 ? "hotel" : "ticket");
			alipayParams.setRoyalty_parameters(royalty_parameters);
			
		}
		
		
		Map<String,String> sArray = new HashMap<String, String>();
		try {
			sArray =BeanToMapUtil.convertBean(alipayParams);
		}  catch (Exception e) {
			throw new BusinessException(e);
		}
		alipayParams.setSign(buildMysign(sArray,keyValue));
		alipayParams.setIsUsePay(true);
		
		return alipayParams;
	}
	
 
   
    @Override
    public RefundParams initRefundParams(BaseTranRec baseTranRec, String amt,int type) {
    	String keyValue = CommonMethod.formatString(sysParamDao.findSysParam("ALIPAY_KEY_VALUE",type==2 ? "02" : "01"));
    	AliPayRefundParams params = new AliPayRefundParams();
    	params.setService("refund_fastpay_by_platform_nopwd");
    	params.setPartner(sysParamDao.findSysParam("partner",type==2 ? "02" : "01"));
    	params.set_input_charset("utf-8");
    	params.setSign_type("MD5");	
//    	params.setNotify_url(Configuration.getInstance().getValue("refund_notify_url"));
//    	params.setNotify_url("http://192.168.5.210:8080/booking/aliRefundResultAction!saveRefund.action");
    	params.setBatch_no(getNextBatchNo(CommonMethod.getCurrentSystemDate().substring(0, 8)+"001", CommonMethod.getCurrentSystemDate().substring(0, 8)));
    	params.setRefund_date(CommonMethod.dateToString2(CommonMethod.getTime()));
    	params.setBatch_num("1");
    	if(type == 1){//票
//    		params.setDback_notify_url(Configuration.getInstance().getValue("dback_notify_url"));
    		//支付宝网关手续费费率
    		String alipayPerFee = CommonMethod.formatString(sysParamDao.findSysParam("ALIPAY_PER_FEE"));
    		
    		double m = baseTranRec.getMoney();
    		if(m<0d){
    			m = 0 - m;
    		}
    		
    		double amtSub = m*(1-Double.valueOf(alipayPerFee));
    		amt = CommonMethod.format2db(amtSub).toString();
    		//交易退款数据集
    		String refundStr = baseTranRec.getTrxId()+"^"+amt+"^refundTicket";
    		
    		String rpStr = baseTranRec.getRoyaltyParameters();
    		String refundSubStr = "";
    		if(rpStr!=null && !"".equals(rpStr)){
    			String[] date = rpStr.split("\\^");
        		//分润退款数据集
        		refundSubStr = "|"+sysParamDao.findSysParam("seller_email", "03")+"^"+sysParamDao.findSysParam("partner", "03")+"^"+
        								sysParamDao.findSysParam("seller_email", "01")+"^"+sysParamDao.findSysParam("partner", "01")+"^"+
        								date[1]+"^refundTicket";
    		}
    		
    		params.setDetail_data(refundStr+refundSubStr);
    		params.setNotify_url(Configuration.getInstance().getValue("refund_notify_url"));
    	}else if(type == 2){//酒店
    		params.setDetail_data(baseTranRec.getTrxId()+"^"+amt+"^refundHotel");
    		params.setNotify_url(Configuration.getInstance().getValue("refund_notify_urlForHotel"));
    	}
    	Map<String,String> sArray = new HashMap<String, String>();
    	try {
			sArray =BeanToMapUtil.convertBean(params);
		}  catch (Exception e) {
			throw new BusinessException(e);
		}
		params.setSign(buildMysign(sArray,keyValue));
		params.setRefund_date(URLEncoder.encode(params.getRefund_date()));
		baseTranRec.setRemark(params.getDetail_data());
//		params.setDetail_data(baseTranRec.getTrxId()+"^"+amt+"^"+URLEncoder.encode("退票退款"));
    	return params;
    }
	@Override
	public boolean refund(RefundParams refundParams) {
		URL url = null;
		try {
			String payTest = sysParamDao.findSysParam("PAY_TEST");
			if("true".equals(payTest)){
				url = new URL("http://192.168.5.210:8080/simulationPay/simulatePayAction!refundTicket.action");
				return true;
			} else {
				url = new URL("https://mapi.alipay.com/gateway.do?"+createLinkString(paraFilterEmpty(BeanToMapUtil.convertBean(refundParams))));
			}
			BufferedReader in = null;
			in = new BufferedReader(new InputStreamReader(url.openStream()));
			String line = "";
			System.out.println(url);
			while ((line=in.readLine())!=null ){
				logger.debug(line);
				System.out.println(line);
				if(line!=null && line.trim().contains("<is_success>T</is_success>")){
					return true;
				}else if(line!=null && line.trim().contains("<is_success>F</is_success>")){
					logger.error("退款出错："+(line=in.readLine()));
					return false;
				}
			}
		} 
		catch (Exception e1) {
			e1.printStackTrace();
			return false;
		}
		return false;
	}
	
	
	/**
     * 生成签名结果
     * @param sArray 要签名的数组
     * @return 签名结果字符串
     */
    public static String buildMysign(Map<String,String>sArray,String prestrKey) { 
        return MD5.getMD5(createLinkString(paraFilter(sArray)) + prestrKey);
    }
 
    /** 
     * 除去数组中的空值和签名参数
     * @param sArray 签名参数组
     * @return 去掉空值与签名参数后的新签名参数组
     */
    private static Map<String,String> paraFilterEmpty(Map<String,String> sArray) {
        Map<String,String> result = new HashMap<String,String>();
        if (sArray == null || sArray.size() <= 0) {
            return result;
        }
        for (String key : sArray.keySet()) {
            String value = sArray.get(key);
            if (value == null || value.equals("") ) {
                continue;
            }
            result.put(key, value);
        }
        return result;
    }
    /** 
     * 除去数组中的空值和签名参数
     * @param sArray 签名参数组
     * @return 去掉空值与签名参数后的新签名参数组
     */
    private static Map<String,String> encodePara(Map<String,String> sArray) {
        Map<String,String> result = new HashMap<String,String>();
        if (sArray == null || sArray.size() <= 0) {
            return result;
        }
        for (String key : sArray.keySet()) {
            String value = sArray.get(key);
            if (value != null && !value.equals("") ) {
            	value = URLEncoder.encode(value);
            	result.put(key, value);
            }
        }
        return result;
    }
    /** 
     * 除去数组中的空值和签名参数
     * @param sArray 签名参数组
     * @return 去掉空值与签名参数后的新签名参数组
     */
    public static Map<String,String> paraFilter(Map<String,String> sArray) {
        Map<String,String> result = new HashMap<String,String>();
        if (sArray == null || sArray.size() <= 0) {
            return result;
        }
        for (String key : sArray.keySet()) {
            String value = sArray.get(key);
            if (value == null || value.equals("") || key.equalsIgnoreCase("sign")
                || key.equalsIgnoreCase("sign_type") || key.equalsIgnoreCase("payType")) {
                continue;
            }
            result.put(key, value);
        }
        return result;
    }
    
    /** 
     * 把数组所有元素排序，并按照“参数=参数值”的模式用“&”字符拼接成字符串
     * @param params 需要排序并参与字符拼接的参数组
     * @return 拼接后字符串
     */
    public static String createLinkString(Map<String,String> params) {
        List<String> keys = new ArrayList<String>(params.keySet());
        Collections.sort(keys);
 
        //String prestr = "";
        StringBuffer sb=new StringBuffer(500);
        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
           // String value = params.get(key);
 
            if (i == keys.size() - 1) {//拼接时，不包括最后一个&字符
                sb.append(key).append("=").append(params.get(key));
                //prestr = prestr + key + "=" + value;
            } else {
                //prestr = prestr + key + "=" + value + "&";
                sb.append(key).append("=").append(params.get(key)).append("&");
            }
        }
        return sb.toString();
    }
    
	private synchronized String getNextBatchNo(String baseCode,String prefixCode) {
//		SysTablePk sysTablepk = sysTablePkDao.findById("BATCH_NO");
//		if(sysTablepk==null || sysTablepk.getTableName() == null){//无数据时，新增
//			sysTablepk = new SysTablePk();
//			sysTablepk.setTableName("BATCH_NO");
//			sysTablepk.setCurrentPk(prefixCode+baseCode);
//			sysTablePkDao.save(sysTablepk);
//			return prefixCode+baseCode;
//		}
//		else{
//			if(sysTablepk.getCurrentPk().startsWith(prefixCode)){//
//				String code = sysTablepk.getCurrentPk();
//				String str = prefixCode+String.format("%0"+3+"d", Integer.parseInt(code.substring(7, code.length()))+1);
//				sysTablepk.setTableName("BATCH_NO");
//				sysTablepk.setCurrentPk(str);
//				sysTablePkDao.update(sysTablepk);
//				return str;
//			}
//			else{
//				sysTablepk.setTableName("BATCH_NO");
//				sysTablepk.setCurrentPk(baseCode+prefixCode);
//				sysTablePkDao.update(sysTablepk);
//				return prefixCode+baseCode;
//			}
//		}
		SysTablePk sysTablepk = sysTablePkDao.findById("BATCH_NO");
		if(sysTablepk==null || sysTablepk.getTableName() == null){//无数据时，新增
			sysTablepk = new SysTablePk();
			sysTablepk.setTableName("BATCH_NO");
			sysTablepk.setCurrentPk(baseCode);
			sysTablePkDao.save(sysTablepk);
			return baseCode;
		}
		else{
			if(sysTablepk.getCurrentPk().startsWith(prefixCode)){//
				String code = sysTablepk.getCurrentPk();
				String str = baseCode.substring(0, 8)+String.format("%0"+3+"d", Integer.parseInt(code.substring(8, code.length()))+1);
				sysTablepk.setTableName("BATCH_NO");
				sysTablepk.setCurrentPk(str);
				sysTablePkDao.update(sysTablepk);
				return str;
			}
			else{
				sysTablepk.setTableName("BATCH_NO");
				sysTablepk.setCurrentPk(baseCode);
				sysTablePkDao.update(sysTablepk);
				return baseCode;
			}
		}
	}


	@Override
	public boolean saveRefund(BaseRefundResult refundResult) {
		throw new BusinessException("为实现");
	}


	@Override
	public PayParams initPayParams(String rechargeKey, Double moneyNeedPay) {
		// TODO Auto-generated method stub
		return null;
	}



}
