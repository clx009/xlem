package common.pay.alipay;

public class AliAccountParams {
	private String service;
	private String partner;
	private String input_charset;
	private String sign_type;
	private String sign;
	private String page_no;
	private String gmt_start_time;
	private String gmt_end_time;
	private String logon_id;
	private String iw_account_log_id;
	private String trade_no;
	private String merchant_out_order_no;
	private String deposit_bank_no;
	private String page_size;
	private String trans_code;

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getPartner() {
		return partner;
	}

	public void setPartner(String partner) {
		this.partner = partner;
	}

	public String getInput_charset() {
		return input_charset;
	}

	public void setInput_charset(String input_charset) {
		this.input_charset = input_charset;
	}

	public String getSign_type() {
		return sign_type;
	}

	public void setSign_type(String sign_type) {
		this.sign_type = sign_type;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getPage_no() {
		return page_no;
	}

	public void setPage_no(String page_no) {
		this.page_no = page_no;
	}

	public String getGmt_start_time() {
		return gmt_start_time;
	}

	public void setGmt_start_time(String gmt_start_time) {
		this.gmt_start_time = gmt_start_time;
	}

	public String getGmt_end_time() {
		return gmt_end_time;
	}

	public void setGmt_end_time(String gmt_end_time) {
		this.gmt_end_time = gmt_end_time;
	}

	public String getLogon_id() {
		return logon_id;
	}

	public void setLogon_id(String logon_id) {
		this.logon_id = logon_id;
	}

	public String getIw_account_log_id() {
		return iw_account_log_id;
	}

	public void setIw_account_log_id(String iw_account_log_id) {
		this.iw_account_log_id = iw_account_log_id;
	}

	public String getTrade_no() {
		return trade_no;
	}

	public void setTrade_no(String trade_no) {
		this.trade_no = trade_no;
	}

	public String getMerchant_out_order_no() {
		return merchant_out_order_no;
	}

	public void setMerchant_out_order_no(String merchant_out_order_no) {
		this.merchant_out_order_no = merchant_out_order_no;
	}

	public String getDeposit_bank_no() {
		return deposit_bank_no;
	}

	public void setDeposit_bank_no(String deposit_bank_no) {
		this.deposit_bank_no = deposit_bank_no;
	}

	public String getPage_size() {
		return page_size;
	}

	public void setPage_size(String page_size) {
		this.page_size = page_size;
	}

	public String getTrans_code() {
		return trans_code;
	}

	public void setTrans_code(String trans_code) {
		this.trans_code = trans_code;
	}
}
