/**
 * 
 */
package common.pay.alipay;

import common.pay.PayParams;


/**
 * @author 黄聪
 *
 */
public class AliBankPayParams extends PayParams {
	
	//基本参数
	private String service;
	private String partner;
	private String _input_charset;
	private String sign_type;
	private String sign;
	private String notify_url;
	private String return_url;
	private String error_notify_url;
	//业务参数
	private String out_trade_no;
	private String subject;
	private String payment_type;
	private String defaultbank;
	private String seller_email;
	private String seller_id;
	private String seller_account_name;
	private String price;
	private String total_fee;
	private String quantity;
	private String body;
	private String show_url;
	private String paymethod;
	private String enable_paymethod;
	private String need_ctu_check;
	private String royalty_type;
	private String royalty_parameters;
	private String anti_phishing_key;
	private String exter_invoke_ip;
	private String extra_common_param;
	private String extend_param;
	private String it_b_pay;
	private String default_login;
	private String product_type;
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getPartner() {
		return partner;
	}
	public void setPartner(String partner) {
		this.partner = partner;
	}
	public String get_input_charset() {
		return _input_charset;
	}
	public void set_input_charset(String _input_charset) {
		this._input_charset = _input_charset;
	}
	public String getSign_type() {
		return sign_type;
	}
	public void setSign_type(String sign_type) {
		this.sign_type = sign_type;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getNotify_url() {
		return notify_url;
	}
	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}
	public String getReturn_url() {
		return return_url;
	}
	public void setReturn_url(String return_url) {
		this.return_url = return_url;
	}
	public String getError_notify_url() {
		return error_notify_url;
	}
	public void setError_notify_url(String error_notify_url) {
		this.error_notify_url = error_notify_url;
	}
	public String getOut_trade_no() {
		return out_trade_no;
	}
	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getPayment_type() {
		return payment_type;
	}
	public void setPayment_type(String payment_type) {
		this.payment_type = payment_type;
	}
	public String getDefaultbank() {
		return defaultbank;
	}
	public void setDefaultbank(String defaultbank) {
		this.defaultbank = defaultbank;
	}
	public String getSeller_email() {
		return seller_email;
	}
	public void setSeller_email(String seller_email) {
		this.seller_email = seller_email;
	}
	public String getSeller_id() {
		return seller_id;
	}
	public void setSeller_id(String seller_id) {
		this.seller_id = seller_id;
	}
	public String getSeller_account_name() {
		return seller_account_name;
	}
	public void setSeller_account_name(String seller_account_name) {
		this.seller_account_name = seller_account_name;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getTotal_fee() {
		return total_fee;
	}
	public void setTotal_fee(String total_fee) {
		this.total_fee = total_fee;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getShow_url() {
		return show_url;
	}
	public void setShow_url(String show_url) {
		this.show_url = show_url;
	}
	public String getPaymethod() {
		return paymethod;
	}
	public void setPaymethod(String paymethod) {
		this.paymethod = paymethod;
	}
	public String getEnable_paymethod() {
		return enable_paymethod;
	}
	public void setEnable_paymethod(String enable_paymethod) {
		this.enable_paymethod = enable_paymethod;
	}
	public String getNeed_ctu_check() {
		return need_ctu_check;
	}
	public void setNeed_ctu_check(String need_ctu_check) {
		this.need_ctu_check = need_ctu_check;
	}
	public String getRoyalty_type() {
		return royalty_type;
	}
	public void setRoyalty_type(String royalty_type) {
		this.royalty_type = royalty_type;
	}
	public String getRoyalty_parameters() {
		return royalty_parameters;
	}
	public void setRoyalty_parameters(String royalty_parameters) {
		this.royalty_parameters = royalty_parameters;
		super.setRoyalty_parameters(royalty_parameters);
	}
	public String getAnti_phishing_key() {
		return anti_phishing_key;
	}
	public void setAnti_phishing_key(String anti_phishing_key) {
		this.anti_phishing_key = anti_phishing_key;
	}
	public String getExter_invoke_ip() {
		return exter_invoke_ip;
	}
	public void setExter_invoke_ip(String exter_invoke_ip) {
		this.exter_invoke_ip = exter_invoke_ip;
	}
	public String getExtra_common_param() {
		return extra_common_param;
	}
	public void setExtra_common_param(String extra_common_param) {
		this.extra_common_param = extra_common_param;
	}
	public String getExtend_param() {
		return extend_param;
	}
	public void setExtend_param(String extend_param) {
		this.extend_param = extend_param;
	}
	public String getIt_b_pay() {
		return it_b_pay;
	}
	public void setIt_b_pay(String it_b_pay) {
		this.it_b_pay = it_b_pay;
	}
	public String getDefault_login() {
		return default_login;
	}
	public void setDefault_login(String default_login) {
		this.default_login = default_login;
	}
	public String getProduct_type() {
		return product_type;
	}
	public void setProduct_type(String product_type) {
		this.product_type = product_type;
	}
	
}
