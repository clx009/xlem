package common.pay.alipay;

import java.util.List;

import com.xlem.dao.BaseTranResult;


public class AliPage {
	
	private List<BaseTranResult> account_log_list;
	private String has_next_page;
	private String page_no;
	private String page_size;
	
	public void setParam() {
		for (BaseTranResult btr : account_log_list) {
			btr.setParam();
		}
	}
	
	public List<BaseTranResult> getAccount_log_list() {
		return account_log_list;
	}

	public void setAccount_log_list(List<BaseTranResult> account_log_list) {
		this.account_log_list = account_log_list;
	}

	public String getHas_next_page() {
		return has_next_page;
	}

	public void setHas_next_page(String has_next_page) {
		this.has_next_page = has_next_page;
	}

	public String getPage_no() {
		return page_no;
	}

	public void setPage_no(String page_no) {
		this.page_no = page_no;
	}

	public String getPage_size() {
		return page_size;
	}

	public void setPage_size(String page_size) {
		this.page_size = page_size;
	}

	
}
