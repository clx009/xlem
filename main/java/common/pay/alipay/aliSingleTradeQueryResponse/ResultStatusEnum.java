package common.pay.alipay.aliSingleTradeQueryResponse;

public  enum ResultStatusEnum {
	ILLEGAL_SIGN("签名不正确"),
	ILLEGAL_DYN_MD5_KEY("动态密钥信息错误"),
	ILLEGAL_ENCRYPT("加密不正确"),
	ILLEGAL_ARGUMENT("参数不正确"),
	ILLEGAL_SERVICE("Service参数不正确"),
	ILLEGAL_USER("用户ID不正确"),
	ILLEGAL_PARTNER("合作伙伴ID不正确"),
	ILLEGAL_EXTERFACE("接口配置不正确"),
	ILLEGAL_PARTNER_EXTERFACE("合作伙伴接口信息不正确"),
	ILLEGAL_SECURITY_PROFILE("未找到匹配的密钥配置"),
	ILLEGAL_AGENT("代理ID不正确"),
	ILLEGAL_SIGN_TYPE("签名类型不正确"),
	ILLEGAL_CHARSET("字符集不合法"),
	ILLEGAL_CLIENT_IP("客户端IP地址无权访问服务"),
	HAS_NO_PRIVILEGE("无权访问"),
	ILLEGAL_DIGEST_TYPE("摘要类型不正确"),
	ILLEGAL_DIGEST("文件摘要不正确"),
	ILLEGAL_FILE_FORMAT("文件格式不正确"),
	ILLEGAL_ENCODING("不支持该编码类型"),
	ILLEGAL_REQUEST_REFERER("防钓鱼检查不支持该请求来源"),
	ILLEGAL_ANTI_PHISHING_KEY("防钓鱼检查非法时间戳参数"),
	ANTI_PHISHING_KEY_TIMEOUT("防钓鱼检查时间戳超时"),
	ILLEGAL_EXTER_INVOKE_IP("防钓鱼检查非法调用IP"),
	BATCH_NUM_EXCEED_LIMIT("总笔数大于1000"),
	REFUND_DATE_ERROR("错误的退款时间"),
	BATCH_NUM_ERROR("传入的总笔数格式错误"),
	DUBL_ROYALTY_IN_DETAIL("同一条明细中存在两条转入转出账户相同的分润信息"),
	BATCH_NUM_NOT_EQUAL_TOTAL("传入的退款条数不等于数据集解析出的退款条数"),
	SINGLE_DETAIL_DATA_EXCEED_LIMIT("单笔退款明细超出限制"),
	DUBL_TRADE_NO_IN_SAME_BATCH("同一批退款中存在两条相同的退款记录"),
	DUPLICATE_BATCH_NO("重复的批次号"),
	TRADE_STATUS_ERROR("交易状态不允许退款"),
	BATCH_NO_FORMAT_ERROR("批次号格式错误"),
	PARTNER_NOT_SIGN_PROTOCOL("平台商未签署协议"),
	NOT_THIS_PARTNERS_TRADE("退款明细非本合作伙伴的交易"),
	DETAIL_DATA_FORMAT_ERROR("数据集参数格式错误"),
	SELLER_NOT_SIGN_PROTOCOL("卖家未签署协议"),
	INVALID_CHARACTER_SET("字符集无效"),
	ACCOUNT_NOT_EXISTS("账号不存在"),
	EMAIL_USERID_NOT_MATCH("Email和用户ID不匹配"),
	REFUND_ROYALTY_FEE_ERROR("退分润金额不合法"),
	ROYALTYER_NOT_SIGN_PROTOCOL("分润方未签署三方协议"),
	RESULT_AMOUNT_NOT_VALID("退收费、退分润或者退款的金额错误"),
	REASON_REFUND_ROYALTY_ERROR("退分润错误"),
	TRADE_NOT_EXISTS("交易不存在"),
	WHOLE_DETAIL_FORBID_REFUND("整条退款明细都禁止退款"),
	TRADE_HAS_CLOSED("交易已关闭，不允许退款"),
	TRADE_HAS_FINISHED("交易已结束，不允许退款"),
	NO_REFUND_CHARGE_PRIVILEDGE("没有退收费的权限"),
	RESULT_BATCH_NO_FORMAT_ERROR("批次号格式错误"),
	BATCH_MEMO_LENGTH_EXCEED_LIMIT("备注长度超过256字节"),
	REFUND_CHARGE_FEE_GREATER_THAN_LIMIT("退收费金额超过限制"),
	REFUND_TRADE_FEE_ERROR("退交易金额不合法"),
	SELLER_STATUS_NOT_ALLOW("卖家状态不正常"),
	SINGLE_DETAIL_DATA_ENCODING_NOT_SUPPORT("单条数据集编码集不支持"),
	TXN_RESULT_ACCOUNT_STATUS_NOT_VALID("卖家账户状态无效或被冻结"),
	TXN_RESULT_ACCOUNT_BALANCE_NOT_ENOUGH("卖家账户余额不足"),
	CA_USER_NOT_USE_CA("数字证书用户但未使用数字证书登录"),
	BATCH_REFUND_LOCK_ERROR("同一时间不允许进行多笔并发退款"),
	REFUND_SUBTRADE_FEE_ERROR("退子交易金额不合法"),
	NANHANG_REFUND_CHARGE_AMOUNT_ERROR("退票面价金额不合法"),
	REFUND_AMOUNT_NOT_VALID("退款金额不合法"),
	TRADE_PRODUCT_TYPE_NOT_ALLOW_REFUND("交易类型不允许退交易"),
	RESULT_FACE_AMOUNT_NOT_VALID("退款票面价不能大于支付票面价"),
	REFUND_CHARGE_FEE_ERROR("退收费金额不合法"),
	REASON_REFUND_CHARGE_ERR("退收费失败"),
	DUP_ROYALTY_REFUND_ITEM("重复的退分润条目"),
	RESULT_ACCOUNT_NO_NOT_VALID("账号无效"),
	REASON_TRADE_REFUND_FEE_ERR("退款金额错误"),
	REASON_HAS_REFUND_FEE_NOT_MATCH("已退款金额错误"),
	REASON_REFUND_AMOUNT_LESS_THAN_COUPON_FEE("红包无法部分退款"),
	BATCH_REFUND_STATUS_ERROR("退款记录状态错误"),
	BATCH_REFUND_DATA_ERROR("批量退款后数据检查错误"),
	REFUND_TRADE_FAILED("不存在退交易，但是退收费和退分润失败"),
	REFUND_FAIL("退款失败（该结果码只会出现在做意外数据恢复时，找不到结果码的情况）"),
	SUCCESS("交易成功");
	
	// 成员变量  
    private String name;  
    // 构造方法  
    private ResultStatusEnum(String name) {  
        this.name = name;  
    }
    
 // get set 方法  
    public String getName() {  
        return name;  
    }  
    public void setName(String name) {  
        this.name = name;  
    }  
}
