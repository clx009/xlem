/**
 * 
 */
package common.pay.alipay.aliSingleTradeQueryResponse;



/**
 * @author 单笔交易查询
 *
 */
public class ASTQResponse{
	
	private ASTQTrade trade;

	public ASTQTrade getTrade() {
		return trade;
	}

	public void setTrade(ASTQTrade trade) {
		this.trade = trade;
	}

}
