/**
 * 
 */
package common.pay.alipay.aliSingleTradeQueryResponse;

import java.util.List;

import com.xlem.dao.BaseRefundResult;

import common.dao.BaseBean;

/**
 * @author 单笔交易查询
 *
 */
public class AliSingleTradeQueryResponse extends BaseBean{
	
	private String is_success;
	private String sign_type;
	private String sign;
	private String error;
	
	private ASTQResponse response;
	
	private List<BaseRefundResult>  brrList;

	
	public String getIs_success() {
		return is_success;
	}

	public void setIs_success(String is_success) {
		this.is_success = is_success;
	}

	public String getSign_type() {
		return sign_type;
	}

	public void setSign_type(String sign_type) {
		this.sign_type = sign_type;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	public String getErrorText() {
		if(this.error!=null && !this.error.isEmpty()){
			return ResponseErroeEnum.valueOf(this.error).getName();
		}else{
			return "";
		}
	}

	public ASTQResponse getResponse() {
		return response;
	}

	public void setResponse(ASTQResponse response) {
		this.response = response;
	}

	public List<BaseRefundResult> getBrrList() {
		return brrList;
	}

	public void setBrrList(List<BaseRefundResult> brrList) {
		this.brrList = brrList;
	}
	
	
}
