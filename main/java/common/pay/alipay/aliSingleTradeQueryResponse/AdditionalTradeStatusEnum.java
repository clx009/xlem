package common.pay.alipay.aliSingleTradeQueryResponse;

public  enum AdditionalTradeStatusEnum {
	ZHIFUBAO_CONFIRM("客服代买家确认收货"), 
	ZHIFUBAO_CANCEL_FP("客服代付款方取消快速支付"), 
	DAEMON_CONFIRM_CANCEL_PRE_AUTH("超时程序取消预授权"), 
	DAEMON_CONFIRM_CLOSE("超时程序因买家不付款关闭交易");
	
	// 成员变量  
    private String name;  
    // 构造方法  
    private AdditionalTradeStatusEnum(String name) {  
        this.name = name;  
    }
    
 // get set 方法  
    public String getName() {  
        return name;  
    }  
    public void setName(String name) {  
        this.name = name;  
    }  
}
