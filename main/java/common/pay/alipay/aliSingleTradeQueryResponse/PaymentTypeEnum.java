package common.pay.alipay.aliSingleTradeQueryResponse;

public  enum PaymentTypeEnum {
	type_01("红包结算金预收款","01"),
	type_02("红包结算金","02"),
	type_03("提醒收款","03"),
	type_04("自动发货商品","04"),
	type_1("商品购买","1"),
	type_2("服务购买","2"),
	type_3("网络拍卖","3"),
	type_4("捐赠","4"),
	type_5("邮费补偿","5"),
	type_6("奖金","6"),
	type_7("基金购买","7"),
	type_8("机票购买","8"),
	type_9("收AA款","9"),
	type_10("团购","10"),
	type_11("电子客票","11"),
	type_12("彩票","12"),
	type_13("拍卖","13"),
	type_14("手机支付类型","14"),
	type_15("鲜花礼品","15"),
	type_16("代理商电子客票","16"),
	type_17("党费","17"),
	type_18("外汇","18"),
	type_19("自动直充","19"),
	type_20("境外收单退款","20"),
	type_21("即时到账退款","21"),
	type_22("业务保证金","22"),
	type_24("送礼金","24"),
	type_25("交房租","25"),
	type_26("motopay类型","26"),
	type_23("购物车支付","23"),
	type_27("团购担保交易付款","27");
	
	// 成员变量  
    private String name;  
    private String code;
    // 构造方法  
    private PaymentTypeEnum(String name,String code) {  
        this.name = name;  
        this.code = code;  
    }
 // 普通方法  
    public static String getName(String code) {  
        for (PaymentTypeEnum c : PaymentTypeEnum.values()) {  
            if (code.equals(c.getCode())) {  
                return c.name;  
            }  
        }  
        return null;  
    } 
    
 // get set 方法  
    public String getName() {  
        return name;  
    }  
    public void setName(String name) {  
        this.name = name;  
    }

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}  
    
    
}
