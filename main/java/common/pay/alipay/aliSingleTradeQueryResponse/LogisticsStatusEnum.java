package common.pay.alipay.aliSingleTradeQueryResponse;

public  enum LogisticsStatusEnum {
	INITIAL_STATUS("初始状态"), 
	WAIT_LOGISTICS_FETCH_GOODS("等待物流取货"), 
	LOGISTICS_FAILURE("物流失败（包括发送消息失败、取货失败）"), 
	GOODS_RECEIVED("货物收到了"), 
	WAIT_RECEIVER_CONFIRM_GOODS("等待收货人确认收货"), 
	LOGISTICS_SENDING("物流发货中");
	
	// 成员变量  
    private String name;  
    // 构造方法  
    private LogisticsStatusEnum(String name) {  
        this.name = name;  
    }
    
 // get set 方法  
    public String getName() {  
        return name;  
    }  
    public void setName(String name) {  
        this.name = name;  
    }  
}
