package common.pay.alipay.aliSingleTradeQueryResponse;

public  enum RefundStatusEnum {
	WAIT_SELLER_AGREE("退款协议等待卖家确认中"), 
	SELLER_REFUSE_BUYER("卖家不同意协议，等待买家修改"), 
	WAIT_BUYER_RETURN_GOODS("退款协议达成，等待买家退货"), 
	WAIT_SELLER_CONFIRM_GOODS("等待卖家收货"), 
	REFUND_SUCCESS("退款成功"), 
	REFUND_CLOSED("退款关闭"), 
	WAIT_ALIPAY_REFUND("等待支付宝退款"), 
	ACTIVE_REFUND("进行中的退款，供查询"), 
	OVERED_REFUND("结束的退款"), 
	ALL_REFUND_STATUS("所有退款，供查询");
	
	// 成员变量  
    private String name;  
    // 构造方法  
    private RefundStatusEnum(String name) {  
        this.name = name;  
    }
    
 // get set 方法  
    public String getName() {  
        return name;  
    }  
    public void setName(String name) {  
        this.name = name;  
    }  
}
