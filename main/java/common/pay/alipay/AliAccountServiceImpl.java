package common.pay.alipay;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import service.comm.baseTranRec.BaseTranRecService;

import com.thoughtworks.xstream.XStream;
import com.xlem.dao.BaseTranRec;
import com.xlem.dao.BaseTranResult;
import com.xlem.dao.HotelIndent;
import com.xlem.dao.SysParam;
import com.xlem.dao.TicketIndent;

import common.BeanToMapUtil;
import common.BusinessException;
import common.CommonMethod;
import common.MD5;
import common.pay.AccountService;
import common.pay.alipay.aliSingleTradeQueryResponse.ASTQResponse;
import common.pay.alipay.aliSingleTradeQueryResponse.ASTQTrade;
import common.pay.alipay.aliSingleTradeQueryResponse.AliSingleTradeQueryResponse;
import dao.hotel.hotelIndent.HotelIndentDao;
import dao.sys.sysParam.SysParamDao;
import dao.ticket.ticketIndent.TicketIndentDao;

@Service("aliAccountService")
public class AliAccountServiceImpl implements AccountService {
	private static Logger logger = Logger.getLogger("bookingService");
	@Autowired
	private SysParamDao sysParamDao;
	@Autowired
	private HotelIndentDao hotelIndentDao;
	@Autowired
	private TicketIndentDao ticketIndentDao;
	@Autowired
	private BaseTranRecService baseTranRecService;
	
	@Override
	public AliAccountPager readData(AliAccountParams params) {
		initParams(params);
		XStream xStream = new XStream();
		xStream.alias("alipay", AliAccountPager.class);
		xStream.alias("account_page_query_result", AliPage.class);
		xStream.alias("AccountQueryAccountLogVO", BaseTranResult.class);
		AliAccountPager pager = (AliAccountPager) xStream.fromXML(read(params));
		pager.setParam();
		return pager;
	}

	public static void main(String[] args) {
		AliAccountParams params = new AliAccountParams();
		params.setGmt_start_time("2013-08-30 00:00:00");
		params.setGmt_end_time("2013-08-30 23:59:59");
		Integer pageNo = 1;
		params.setPage_no(pageNo.toString());
		AliAccountServiceImpl i = new AliAccountServiceImpl();
		AliAccountPager p = i.readData(params);
		AliPage ap = p.getAccount_page_query_result();
		List<BaseTranResult> brts = ap.getAccount_log_list();
		for (BaseTranResult btr : brts) {
			System.out.println(btr.getBalance());
			System.out.println(btr.getTransOutOrderNo());
			System.out.println(btr.getTrans_out_order_no());
		}
	}
	
	private void initParams(AliAccountParams params) {
		String keyValue = CommonMethod.formatString(sysParamDao.findSysParam("ALIPAY_KEY_VALUE"));
//		String keyValue ="sawyiccrfhqulsfgmvg423je8nh6z6vh";//正式：sawyiccrfhqulsfgmvg423je8nh6z6vh，测试：umz4aea6g97skeect0jtxigvjkrimd0o
		params.setService("account.page.query");
		params.setPartner(sysParamDao.findSysParam("partner"));
//		params.setPartner("2088901621834291");//正式：2088901621834291 测试：2088201564809153
		params.setInput_charset("utf-8");
		params.setSign_type("MD5");
		params.setPage_size("50");
		Map<String,String> sArray = new HashMap<String, String>();
		try {
			sArray =BeanToMapUtil.convertBean(params);
		}  catch (Exception e) {
			throw new BusinessException(e);
		}
		params.setSign(buildMysign(sArray,keyValue));
		params.setGmt_start_time(URLEncoder.encode(params.getGmt_start_time()));
		params.setGmt_end_time(URLEncoder.encode(params.getGmt_end_time()));
	}

	private String read(AliAccountParams params) {
			String url;
			try {
				url = "https://mapi.alipay.com/gateway.do?"+createLinkString(paraFilterEmpty(BeanToMapUtil.convertBean(params)));
				System.out.println(url);
				StringBuffer xml =CommonMethod.readUrl(url);
				if(xml.indexOf("<is_success>F</is_success>")==-1){
					xml.replace(xml.indexOf("<request>"), xml.indexOf("</request>")+"</request>".length(), "");
					xml.replace(xml.indexOf("<response>"), xml.indexOf("<response>")+"<response>".length(), "");
					xml.replace(xml.indexOf("</response>"), xml.indexOf("</response>")+"</response>".length(), "");
				}else{
					throw new BusinessException("查询参数有误"+xml);
				}
				return xml.toString();
			} catch (Exception e) {
				e.printStackTrace();
				throw new BusinessException("convertBean 出错");
			} 
	}
	
	/**
     * 生成签名结果
     * @param sArray 要签名的数组
     * @return 签名结果字符串
     */
    public static String buildMysign(Map<String,String>sArray,String prestrKey) { 
        return MD5.getMD5(createLinkString(paraFilter(sArray)) + prestrKey);
    }
	 /** 
     * 除去数组中的空值和签名参数
     * @param sArray 签名参数组
     * @return 去掉空值与签名参数后的新签名参数组
     */
    private static Map<String,String> paraFilterEmpty(Map<String,String> sArray) {
        Map<String,String> result = new HashMap<String,String>();
        if (sArray == null || sArray.size() <= 0) {
            return result;
        }
        for (String key : sArray.keySet()) {
            String value = sArray.get(key);
            if (value == null || value.equals("") ) {
                continue;
            }
            result.put(key, value);
        }
        return result;
    }
	/** 
     * 除去数组中的空值和签名参数
     * @param sArray 签名参数组
     * @return 去掉空值与签名参数后的新签名参数组
     */
    public static Map<String,String> paraFilter(Map<String,String> sArray) {
        Map<String,String> result = new HashMap<String,String>();
        if (sArray == null || sArray.size() <= 0) {
            return result;
        }
        for (String key : sArray.keySet()) {
            String value = sArray.get(key);
            if (value == null || value.equals("") || key.equalsIgnoreCase("sign")
                || key.equalsIgnoreCase("sign_type") || key.equalsIgnoreCase("payType")) {
                continue;
            }
            result.put(key, value);
        }
        return result;
    }
    
    /** 
     * 把数组所有元素排序，并按照“参数=参数值”的模式用“&”字符拼接成字符串
     * @param params 需要排序并参与字符拼接的参数组
     * @return 拼接后字符串
     */
    public static String createLinkString(Map<String,String> params) {
    	System.out.println(params);
        List<String> keys = new ArrayList<String>(params.keySet());
        Collections.sort(keys);
 
        //String prestr = "";
        StringBuffer sb=new StringBuffer(500);
        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
           // String value = params.get(key);
 
            if (i == keys.size() - 1) {//拼接时，不包括最后一个&字符
                sb.append(key).append("=").append(params.get(key));
                //prestr = prestr + key + "=" + value;
            } else {
                //prestr = prestr + key + "=" + value + "&";
                sb.append(key).append("=").append(params.get(key)).append("&");
            }
        }
        return sb.toString();
    }

    @Override
	public AliSingleTradeQueryParams initSingleTradeQueryParams(String outTradeNo) {
    	
    	String otn = "";
    	String paymentAccount = "01";
    	long hId = 0L;
    	String tId = "";
    	DetachedCriteria dcTi = DetachedCriteria.forClass(TicketIndent.class);
    	dcTi.add(Property.forName("indentCode").eq(outTradeNo));
		List<TicketIndent> tiList = ticketIndentDao.findByCriteria(dcTi);
		if(tiList==null || tiList.size()<1){
			DetachedCriteria dcHi = DetachedCriteria.forClass(HotelIndent.class);
			dcHi.add(Property.forName("indentCode").eq(outTradeNo));
			List<HotelIndent> hiList = hotelIndentDao.findByCriteria(dcHi);
			for(HotelIndent hi : hiList){
				hId = hi.getHotelIndentId();
			}
		}else{
			for(TicketIndent ti : tiList){
				tId = ti.getIndentId();
			}
		}
    	
		DetachedCriteria dcBtr = DetachedCriteria.forClass(BaseTranRec.class);
		dcBtr.add(Property.forName("trxId").isNotNull());
		dcBtr.add(Property.forName("paymentType").eq("01"));
		dcBtr.add(Property.forName("paymentAccount").isNotNull());
		if(!"".equals(tId)){
			dcBtr.add(Property.forName("ticketIndent.indentId").eq(tId));
		}else if(hId != 0L){
			dcBtr.add(Property.forName("hotelIndent.hotelIndentId").eq(hId));
		}else{
			throw new BusinessException("订单不存在！","");
		}
		List<BaseTranRec>  btrsList = baseTranRecService.findByCriteria(dcBtr);
		
		List<BaseTranRec>  btrList = baseTranRecService.excludeReturnBackInfo(btrsList);
		
		for(BaseTranRec btr : btrList){
			otn = btr.getBatchNo();
			paymentAccount = btr.getPaymentAccount();
		}
    	
		if("".equals(otn)){
			//throw new BusinessException("订单无交易记录！","");
			otn = outTradeNo;
		}
		
		String keyValue = CommonMethod.formatString(sysParamDao.findSysParam("ALIPAY_KEY_VALUE",paymentAccount));
//		String keyValue = CommonMethod.formatString("sawyiccrfhqulsfgmvg423je8nh6z6vh");//正式账号
		
		AliSingleTradeQueryParams params = new AliSingleTradeQueryParams();
    	params.setService("single_trade_query");
    	params.setPartner(sysParamDao.findSysParam("partner",paymentAccount));
//    	params.setPartner("2088901621834291");//正式账号
    	params.set_input_charset("utf-8");
    	params.setSign_type("MD5");	
	
    	params.setOut_trade_no(otn);
    	Map<String,String> sArray = new HashMap<String, String>();
    	try {
			sArray =BeanToMapUtil.convertBean(params);
		}  catch (Exception e) {
			throw new BusinessException(e);
		}
		params.setSign(buildMysign(sArray,keyValue));
    	return params;
	}
    
    @Override
	public AliSignProtocolWithPartnerParams initSignProtocolWithPartnerParams(String email) {
    	
    	DetachedCriteria dcEm = DetachedCriteria.forClass(SysParam.class);
    	dcEm.add(Property.forName("paramName").like("SELLER_EMAIL", MatchMode.START));
    	List<SysParam> spList = sysParamDao.findByCriteria(dcEm);
    	
    	boolean notE = true;
    	for(SysParam sp : spList){
    		
    		String pEmail = sp.getParamValue();
    		String pName = sp.getParamName();
    		if(email!=null && email.equals(pEmail)){
    			if("SELLER_EMAIL".equals(pName)){
    				throw new BusinessException("该账户不需支付圈签约！","");
    			}else{
    				notE = false;
    			}
    		}
    		
    	}
    	
    	if(notE){
    		throw new BusinessException("你录入的支付宝账号没有合作关系，请确认后重新录入或联系管理员！","");
    	}
    	
		String keyValue = CommonMethod.formatString(sysParamDao.findSysParam("ALIPAY_KEY_VALUE","01"));
//		String keyValue = CommonMethod.formatString("sawyiccrfhqulsfgmvg423je8nh6z6vh");//正式账号
		
		AliSignProtocolWithPartnerParams params = new AliSignProtocolWithPartnerParams();
    	params.setService("sign_protocol_with_partner");
    	params.setPartner(sysParamDao.findSysParam("partner","01"));
//    	params.setPartner("2088901621834291");//正式账号
    	params.set_input_charset("utf-8");
    	params.setSign_type("MD5");	
	
    	params.setEmail(email);
    	params.setSign_channel("NORMAL");
    	Map<String,String> sArray = new HashMap<String, String>();
    	try {
			sArray =BeanToMapUtil.convertBean(params);
		}  catch (Exception e) {
			throw new BusinessException(e);
		}
		params.setSign(buildMysign(sArray,keyValue));
    	return params;
	}
    
    @Override
	public AliSingleTradeQueryResponse findSingleTradeQuery(AliSingleTradeQueryParams payParams) {
		try {
			String url = "https://mapi.alipay.com/gateway.do?service="+payParams.getService()+
				"&partner="+payParams.getPartner()+
				"&_input_charset="+payParams.get_input_charset()+
				"&sign_type="+payParams.getSign_type()+
				"&sign="+payParams.getSign()+
				"&out_trade_no="+payParams.getOut_trade_no();
			StringBuffer xml =CommonMethod.readUrl(url);
			
			if(xml.indexOf("<request>")>0){
				xml.replace(xml.indexOf("<request>"), xml.indexOf("</request>")+10, "");
			}
			
			XStream xStream = new XStream();
			xStream.alias("alipay", AliSingleTradeQueryResponse.class);
			xStream.alias("response", ASTQResponse.class);
			xStream.alias("trade", ASTQTrade.class);
			AliSingleTradeQueryResponse pager = (AliSingleTradeQueryResponse) xStream.fromXML(xml.toString());
			
			return pager;
		} 
		catch (Exception e1) {
			e1.printStackTrace();
			throw new BusinessException("查询出错！","");
		}
	}
    
}
