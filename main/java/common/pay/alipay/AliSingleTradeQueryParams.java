/**
 * 
 */
package common.pay.alipay;

import common.pay.PayParams;


/**
 * @author 单笔交易查询
 *
 */
public class AliSingleTradeQueryParams extends PayParams {
	
	//基本参数
	private String service;
	private String partner;
	private String _input_charset;
	private String sign_type;
	private String sign;
	//业务参数
	private String out_trade_no;
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getPartner() {
		return partner;
	}
	public void setPartner(String partner) {
		this.partner = partner;
	}
	public String get_input_charset() {
		return _input_charset;
	}
	public void set_input_charset(String _input_charset) {
		this._input_charset = _input_charset;
	}
	public String getSign_type() {
		return sign_type;
	}
	public void setSign_type(String sign_type) {
		this.sign_type = sign_type;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getOut_trade_no() {
		return out_trade_no;
	}
	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}
	
	
	
}
