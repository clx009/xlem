package common.pay.alipay;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.BaseRefundResult;
import com.xlem.dao.BaseTranRec;
import com.xlem.dao.HotelIndent;
import com.xlem.dao.SysTablePk;
import com.xlem.dao.TicketIndent;
// import com.yeepay.Configuration;
import common.BeanToMapUtil;
import common.BusinessException;
import common.CommonMethod;
import common.MD5;
import common.pay.PayFactoryService;
import common.pay.PayParams;
import common.pay.PayService;
import common.pay.RefundParams;

import dao.sys.sysParam.SysParamDao;
import dao.sys.sysTablePk.SysTablePkDao;

@Transactional
@Service("alipayService")
public class AlipayServiceImpl implements PayService {

	@Autowired
	private SysParamDao sysParamDao;
	@Autowired
	private SysTablePkDao sysTablePkDao;
	
	private static Logger logger = Logger.getLogger("bookingService");
	
	@Override
	public PayParams initPayParams(TicketIndent ticketIndent,
			Double moneyNeedPay, Double moneyNeedPaySub) {
		return initPayParams( ticketIndent.getIndentCode(), moneyNeedPay,1, moneyNeedPaySub);
	}


	@Override
	public PayParams initPayParams(HotelIndent hotelIndent, Double moneyNeedPay) {
		return initPayParams( hotelIndent.getIndentCode(), moneyNeedPay,2,0D);
	}
	
	@Override
	public PayParams initPayParams(String rechargeKey, Double moneyNeedPay) {
		String keyValue = CommonMethod.formatString(sysParamDao.findSysParam("ALIPAY_KEY_VALUE", "01"));
		AlipayParams alipayParams = new AlipayParams();
		                         
		alipayParams.setService("create_direct_pay_by_user");
		alipayParams.setPartner(sysParamDao.findSysParam("partner", "01"));
		alipayParams.set_input_charset("utf-8");
		alipayParams.setSign_type("MD5");		
		alipayParams.setNotify_url(Configuration.getInstance().getValue("notify_urlForRecharge"));
		
		alipayParams.setReturn_url(Configuration.getInstance().getValue("return_urlForRecharge"));
		String trade_no = rechargeKey;
		alipayParams.setOut_trade_no(trade_no);
		alipayParams.setSubject("recharge");
		alipayParams.setPayment_type("1");
		//测试账号：alipay-test12@alipay.com
		alipayParams.setSeller_email(sysParamDao.findSysParam("seller_email", "01"));		
		alipayParams.setTotal_fee(moneyNeedPay.toString());
		alipayParams.setPaymethod("bankPay");
		Map<String,String> sArray = new HashMap<String, String>();
		try {
			sArray =BeanToMapUtil.convertBean(alipayParams);
		}  catch (Exception e) {
			throw new BusinessException(e);
		}
		alipayParams.setSign(buildMysign(sArray,keyValue));
		alipayParams.setIsUsePay(true);
		
		return alipayParams;	
	}
	
	private PayParams initPayParams(String indentCode, Double moneyNeedPay,int type, Double moneyNeedPaySub) {
		String keyValue = CommonMethod.formatString(sysParamDao.findSysParam("ALIPAY_KEY_VALUE",type==2 ? "02" : "01"));
		AlipayParams alipayParams = new AlipayParams();
		                         
		alipayParams.setService("create_direct_pay_by_user");
		alipayParams.setPartner(sysParamDao.findSysParam("partner",type==2 ? "02" : "01"));
		alipayParams.set_input_charset("utf-8");
		alipayParams.setSign_type("MD5");		
		alipayParams.setNotify_url(Configuration.getInstance().getValue(type==2 ? "notify_urlForHotel" : "notify_url"));
		
		alipayParams.setReturn_url(Configuration.getInstance().getValue(type==2 ? "return_urlForHotel" : "return_url"));		
		alipayParams.setOut_trade_no(indentCode);
		alipayParams.setSubject(type==2 ? "hotel" : "ticket");
		alipayParams.setPayment_type("1");
		//测试账号：alipay-test12@alipay.com
		alipayParams.setSeller_email(sysParamDao.findSysParam("seller_email",type==2 ? "02" : "01"));		
		alipayParams.setTotal_fee(moneyNeedPay.toString());
		alipayParams.setPaymethod("bankPay");
		Map<String,String> sArray = new HashMap<String, String>();
		try {
			sArray =BeanToMapUtil.convertBean(alipayParams);
		}  catch (Exception e) {
			throw new BusinessException(e);
		}
		alipayParams.setSign(buildMysign(sArray,keyValue));
		alipayParams.setIsUsePay(true);
		
		return alipayParams;
	}
	
    @Override
    public RefundParams initRefundParams(BaseTranRec baseTranRec, String amt, int type) {
    	String keyValue = CommonMethod.formatString(sysParamDao.findSysParam("ALIPAY_KEY_VALUE",type==2 ? "02" : "01"));
    	AliPayRefundParams params = new AliPayRefundParams();
    	// params.setService("refund_fastpay_by_platform_nopwd");
    	params.setService("refund_fastpay_by_platform_nopwd");
    	params.setPartner(sysParamDao.findSysParam("partner",type==2 ? "02" : "01"));
    	params.set_input_charset("utf-8");
    	params.setSign_type("MD5");	
//    	params.setNotify_url("");
    	params.setBatch_no(getNextBatchNo(CommonMethod.getCurrentSystemDate().substring(0, 8)+"001", CommonMethod.getCurrentSystemDate().substring(0, 8)));
    	params.setRefund_date(CommonMethod.dateToString2(CommonMethod.getTime()));
    	params.setBatch_num("1");
    	if(type == 1){//票
//    		params.setDback_notify_url(Configuration.getInstance().getValue("dback_notify_url"));
    		params.setDetail_data(baseTranRec.getTrxId()+"^"+amt+"^refundTicket");
    	}else if(type == 2){//酒店
    		params.setDetail_data(baseTranRec.getTrxId()+"^"+amt+"^refundHotel");
    	}
    	Map<String,String> sArray = new HashMap<String, String>();
    	try {
			sArray =BeanToMapUtil.convertBean(params);
		}  catch (Exception e) {
			throw new BusinessException(e);
		}
		params.setSign(buildMysign(sArray,keyValue));
		params.setRefund_date(URLEncoder.encode(params.getRefund_date()));
//		params.setDetail_data(baseTranRec.getTrxId()+"^"+amt+"^"+URLEncoder.encode("退票退款"));
    	return params;
    }
	@Override
	public boolean refund(RefundParams refundParams) {
		URL url = null;
		try {
			url = new URL("https://mapi.alipay.com/gateway.do?"+createLinkString(paraFilterEmpty(BeanToMapUtil.convertBean(refundParams))));
			System.out.println(url);
			BufferedReader in = null;
			in = new BufferedReader(new InputStreamReader(url.openStream()));
			String line = "";
			while ((line=in.readLine())!=null ){
				System.out.println(line);
				logger.debug(line);
				if(line!=null && line.trim().contains("<is_success>T</is_success>")){
					return true;
				}else if(line!=null && line.trim().contains("<is_success>F</is_success>")){
					logger.error("退款出错："+(line=in.readLine()));
					System.out.println(line);
					return false;
				}
			}
		} 
		catch (Exception e1) {
			e1.printStackTrace();
			return false;
		}
		return false;
	}
	
	
	/**
     * 生成签名结果
     * @param sArray 要签名的数组
     * @return 签名结果字符串
     */
    public static String buildMysign(Map<String,String>sArray,String prestrKey) { 
        return MD5.getMD5(createLinkString(paraFilter(sArray)) + prestrKey);
    }
 
    /** 
     * 除去数组中的空值和签名参数
     * @param sArray 签名参数组
     * @return 去掉空值与签名参数后的新签名参数组
     */
    private static Map<String,String> paraFilterEmpty(Map<String,String> sArray) {
        Map<String,String> result = new HashMap<String,String>();
        if (sArray == null || sArray.size() <= 0) {
            return result;
        }
        for (String key : sArray.keySet()) {
            String value = sArray.get(key);
            if (value == null || value.equals("") ) {
                continue;
            }
            result.put(key, value);
        }
        return result;
    }
    /** 
     * 除去数组中的空值和签名参数
     * @param sArray 签名参数组
     * @return 去掉空值与签名参数后的新签名参数组
     */
    private static Map<String,String> encodePara(Map<String,String> sArray) {
        Map<String,String> result = new HashMap<String,String>();
        if (sArray == null || sArray.size() <= 0) {
            return result;
        }
        for (String key : sArray.keySet()) {
            String value = sArray.get(key);
            if (value != null && !value.equals("") ) {
            	value = URLEncoder.encode(value);
            	result.put(key, value);
            }
        }
        return result;
    }
    /** 
     * 除去数组中的空值和签名参数
     * @param sArray 签名参数组
     * @return 去掉空值与签名参数后的新签名参数组
     */
    public static Map<String,String> paraFilter(Map<String,String> sArray) {
        Map<String,String> result = new HashMap<String,String>();
        if (sArray == null || sArray.size() <= 0) {
            return result;
        }
        for (String key : sArray.keySet()) {
            String value = sArray.get(key);
            if (value == null || value.equals("") || key.equalsIgnoreCase("sign")
                || key.equalsIgnoreCase("sign_type") || key.equalsIgnoreCase("payType")) {
                continue;
            }
            result.put(key, value);
        }
        return result;
    }
    
    /** 
     * 把数组所有元素排序，并按照“参数=参数值”的模式用“&”字符拼接成字符串
     * @param params 需要排序并参与字符拼接的参数组
     * @return 拼接后字符串
     */
    public static String createLinkString(Map<String,String> params) {
 
        List<String> keys = new ArrayList<String>(params.keySet());
        Collections.sort(keys);
 
        //String prestr = "";
        StringBuffer sb=new StringBuffer(500);
        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
           // String value = params.get(key);
 
            if (i == keys.size() - 1) {//拼接时，不包括最后一个&字符
                sb.append(key).append("=").append(params.get(key));
                //prestr = prestr + key + "=" + value;
            } else {
                //prestr = prestr + key + "=" + value + "&";
                sb.append(key).append("=").append(params.get(key)).append("&");
            }
        }
        return sb.toString();
    }
    
	private synchronized String getNextBatchNo(String baseCode,String prefixCode) {
//		SysTablePk sysTablepk = sysTablePkDao.findById("BATCH_NO");
//		if(sysTablepk==null || sysTablepk.getTableName() == null){//无数据时，新增
//			sysTablepk = new SysTablePk();
//			sysTablepk.setTableName("BATCH_NO");
//			sysTablepk.setCurrentPk(prefixCode+baseCode);
//			sysTablePkDao.save(sysTablepk);
//			return prefixCode+baseCode;
//		}
//		else{
//			if(sysTablepk.getCurrentPk().startsWith(prefixCode)){//
//				String code = sysTablepk.getCurrentPk();
//				String str = prefixCode+String.format("%0"+3+"d", Integer.parseInt(code.substring(7, code.length()))+1);
//				sysTablepk.setTableName("BATCH_NO");
//				sysTablepk.setCurrentPk(str);
//				sysTablePkDao.update(sysTablepk);
//				return str;
//			}
//			else{
//				sysTablepk.setTableName("BATCH_NO");
//				sysTablepk.setCurrentPk(baseCode+prefixCode);
//				sysTablePkDao.update(sysTablepk);
//				return prefixCode+baseCode;
//			}
//		}
		SysTablePk sysTablepk = sysTablePkDao.findById("BATCH_NO");
		if(sysTablepk==null || sysTablepk.getTableName() == null){//无数据时，新增
			sysTablepk = new SysTablePk();
			sysTablepk.setTableName("BATCH_NO");
			sysTablepk.setCurrentPk(baseCode);
			sysTablePkDao.save(sysTablepk);
			return baseCode;
		}
		else{
			if(sysTablepk.getCurrentPk().startsWith(prefixCode)){//
				String code = sysTablepk.getCurrentPk();
				String str = baseCode.substring(0, 8)+String.format("%0"+3+"d", Integer.parseInt(code.substring(8, code.length()))+1);
				sysTablepk.setTableName("BATCH_NO");
				sysTablepk.setCurrentPk(str);
				sysTablePkDao.update(sysTablepk);
				return str;
			}
			else{
				sysTablepk.setTableName("BATCH_NO");
				sysTablepk.setCurrentPk(baseCode);
				sysTablePkDao.update(sysTablepk);
				return baseCode;
			}
		}
	}


	@Override
	public boolean saveRefund(BaseRefundResult refundResult) {
		throw new BusinessException("未实现");
	}

}
