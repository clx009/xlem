package common.pay.alipay;

public class AliAccountPager {
	private String is_success;
	private AliPage account_page_query_result = new AliPage();
	private String sign;
	private String sign_type;

	public void setParam(){
		account_page_query_result.setParam();
	}
	
	public String getIs_success() {
		return is_success;
	}

	public void setIs_success(String is_success) {
		this.is_success = is_success;
	}

	public String getSign_type() {
		return sign_type;
	}

	public void setSign_type(String sign_type) {
		this.sign_type = sign_type;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public AliPage getAccount_page_query_result() {
		return account_page_query_result;
	}

	public void setAccount_page_query_result(AliPage account_page_query_result) {
		this.account_page_query_result = account_page_query_result;
	}

}
