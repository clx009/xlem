/**
 * 
 */
package common.pay.alipay;

import common.pay.RefundParams;

/**
 * @author STKJ-12
 * 
 */
public class AliPayRefundParams extends RefundParams {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String service;
	private String partner;
	private String _input_charset;
	private String sign_type;
	private String sign;
	private String notify_url;
	private String dback_notify_url;
	private String batch_no;
	private String refund_date;
	private String batch_num;
	private String detail_data;
	private String use_freeze_amount;
	private String return_type;

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getPartner() {
		return partner;
	}

	public void setPartner(String partner) {
		this.partner = partner;
	}

	public String get_input_charset() {
		return _input_charset;
	}

	public void set_input_charset(String _input_charset) {
		this._input_charset = _input_charset;
	}

	public String getSign_type() {
		return sign_type;
	}

	public void setSign_type(String sign_type) {
		this.sign_type = sign_type;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getNotify_url() {
		return notify_url;
	}

	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}
	
	public String getDback_notify_url() {
		return dback_notify_url;
	}

	public void setDback_notify_url(String dback_notify_url) {
		this.dback_notify_url = dback_notify_url;
	}

	public String getBatch_no() {
		return batch_no;
	}

	public void setBatch_no(String batch_no) {
		this.batch_no = batch_no;
	}

	public String getRefund_date() {
		return refund_date;
	}

	public void setRefund_date(String refund_date) {
		this.refund_date = refund_date;
	}

	public String getBatch_num() {
		return batch_num;
	}

	public void setBatch_num(String batch_num) {
		this.batch_num = batch_num;
	}

	public String getDetail_data() {
		return detail_data;
	}

	public void setDetail_data(String detail_data) {
		this.detail_data = detail_data;
	}

	public String getUse_freeze_amount() {
		return use_freeze_amount;
	}

	public void setUse_freeze_amount(String use_freeze_amount) {
		this.use_freeze_amount = use_freeze_amount;
	}

	public String getReturn_type() {
		return return_type;
	}

	public void setReturn_type(String return_type) {
		this.return_type = return_type;
	}

}
