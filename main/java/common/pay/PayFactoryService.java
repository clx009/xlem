package common.pay;

import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.BaseRefundResult;
import com.xlem.dao.BaseTranRec;
import com.xlem.dao.HotelIndent;
import com.xlem.dao.TicketIndent;

public interface PayFactoryService {
	
	public final static int ALIPAY = 1;
	public final static int YEEPAY = 2;
	public final static int ALIBANKPAY = 3;
	public final static int TICKET = 1;
	public final static int HOTEL = 2;

	/**
	 * @param payType
	 *            支付类型：1alipay，2yeepay
	 */
	void init(int payType);
	
	/**
	 * 默认支付宝或易宝
	 */
	void init();

	
	/**
	 * @param ticketIndent 票订单
	 * @param moneyNeedPay 须支付金额
	 * @param moneyNeedPaySub 分润金额
	 * @return
	 */
	PayParams initPayParams(TicketIndent ticketIndent, Double moneyNeedPay, Double moneyNeedPaySub);

	/**
	 * @param hotelIndent 酒店订单
	 * @param moneyNeedPay 须支付金额
	 * @return
	 */
	PayParams initPayParams(HotelIndent hotelIndent, Double moneyNeedPay);

	/**
	 * @param baseRechargeRec 现金充值记录
	 * @param moneyNeedPay 须支付金额
	 * @return
	 */
	PayParams initPayParams(String rechargeKey, Double moneyNeedPay);	
	
	/**
	 * @param payResult
	 * @param type ticket:票、hotel：酒店
	 * @return 校验结果 true表示成功
	 */
//	boolean verifyPayResult(PayResult payResult);

	<T extends PayResult>  boolean verifyPayResult(T payResult,String Type);
	
	/**
	 * 初始化退款参数
	 * @param baseTranRec
	 * @param amt 退款金额
	 * @param type 1:票、2：酒店
	 * @return
	 */
	RefundParams initRefundParams(BaseTranRec baseTranRec, String amt,int type);
	/**
	 * 向第三方申请退款，申请后易宝无退款结果返回，支付宝通过异步端口告知退款结果
	 * @param refundParams
	 * @return true成功申请退款，false申请退款失败
	 */
	boolean refund(RefundParams refundParams);

	/**
	 * 保存退款处理情况
	 * @param refundResult
	 * @param type ticket:票、hotel：酒店
	 * @return
	 */
	boolean saveRefund(BaseRefundResult refundResult,String Type);
	
}
