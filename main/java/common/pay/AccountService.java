package common.pay;

import common.pay.alipay.AliAccountPager;
import common.pay.alipay.AliAccountParams;
import common.pay.alipay.AliSignProtocolWithPartnerParams;
import common.pay.alipay.AliSingleTradeQueryParams;
import common.pay.alipay.aliSingleTradeQueryResponse.AliSingleTradeQueryResponse;

public interface AccountService {
	AliAccountPager readData(AliAccountParams params);
	
	AliSingleTradeQueryParams initSingleTradeQueryParams(String outTradeNo);
	
	AliSignProtocolWithPartnerParams initSignProtocolWithPartnerParams(String email);
	
	public AliSingleTradeQueryResponse findSingleTradeQuery(AliSingleTradeQueryParams payParams);
}
