package common.pay;

import common.dao.BaseBean;

/**
 * @author STKJ08
 *
 */
public class PayParams extends BaseBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Boolean isUsePay;
	private int payType;
	private String payMsg;
	
	private String royalty_parameters;
	
	private String pay_test;
	
	/**
	 * @return 是否使用网银支付
	 */
	public Boolean getIsUsePay() {
		return isUsePay;
	}

	public void setIsUsePay(Boolean isUsePay) {
		this.isUsePay = isUsePay;
	}

	public int getPayType() {
		return payType;
	}

	public void setPayType(int payType) {
		this.payType = payType;
	}

	/**
	 * 需在页面提示的信息
	 * @return
	 */
	public String getPayMsg() {
		return payMsg;
	}

	public void setPayMsg(String payMsg) {
		this.payMsg = payMsg;
	}

	/**
	 * 分润规则
	 * @return
	 */
	public String getRoyalty_parameters() {
		return royalty_parameters;
	}

	public void setRoyalty_parameters(String royalty_parameters) {
		this.royalty_parameters = royalty_parameters;
	}

	/**
	 * true:测试，false:正式支付
	 * @return
	 */
	public String getPay_test() {
		return pay_test;
	}

	public void setPay_test(String pay_test) {
		this.pay_test = pay_test;
	}

	
	
}
