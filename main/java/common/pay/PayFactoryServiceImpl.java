package common.pay;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.BaseRefundResult;
import com.xlem.dao.BaseTranRec;
import com.xlem.dao.HotelIndent;
import com.xlem.dao.TicketIndent;

import service.comm.baseRefundResult.BaseRefundResultService;

import common.BusinessException;
import dao.comm.baseRefundResult.BaseRefundResultDao;
import dao.sys.sysParam.SysParamDao;

@Service("payFactoryService")
public class PayFactoryServiceImpl implements PayFactoryService{
	@Resource(name="alipayService")
	private PayService alipayService;
	@Resource(name="aliBankPayService")
	private PayService aliBankPayService;
	/*@Resource(name="yeePayService")
	private PayService yeePayService;*/
	private PayService payService;
	@Autowired
	private BaseRefundResultService baseRefundResultService;
	@Autowired
	private SysParamDao sysParamDao;
	@Autowired
    private BaseRefundResultDao baseRefundResultDao;
	
	private int payType;

	@Override
	public void init(int payType) {
		this.payType = payType;
		if(payType == ALIPAY){
			payService= alipayService;
		/*}else if(payType == YEEPAY){
			payService= yeePayService;*/
		}else if(payType == ALIBANKPAY){
			payService= aliBankPayService;
		}else{
			throw new BusinessException("工厂初始化出错");
		}
	}
	
	@Override
	public void init() {
		this.payType = ALIPAY;
		payService= alipayService;
	}
	
	@Override
	public PayParams initPayParams(TicketIndent ticketIndent, Double moneyNeedPay, Double moneyNeedPaySub) {
		PayParams pp = payService.initPayParams(ticketIndent, moneyNeedPay, moneyNeedPaySub);
		pp.setPayType(payType);
		return pp;
	}
	
	@Override
	public PayParams initPayParams(HotelIndent hotelIndent, Double moneyNeedPay){
		PayParams pp = payService.initPayParams(hotelIndent, moneyNeedPay);
		pp.setPayType(payType);
		return pp;
	}
	
	@Override
	public PayParams initPayParams(String rechargeKey, Double moneyNeedPay){
		PayParams pp = payService.initPayParams(rechargeKey, moneyNeedPay);
		pp.setPayType(payType);
		return pp;
	}
	
	@Override
	public boolean verifyPayResult(PayResult payResult,String Type) {
		return verifyNotifyId(payResult.getNotify_id(),Type);
	}

	private boolean verifyNotifyId(String notifyId,String Type){
		URL url = null;
//		String send_content = URLEncoder.encode(Content.replaceAll("<br/>", " "), "UTF-8");
		
		String PAY_TEST = sysParamDao.findSysParam("PAY_TEST");
		String partner = "";
		if("ticket".equals(Type)){
			partner = "PARTNER";
		}else if("hotel".equals(Type)){
			partner = "PARTNER_HOTEL";
		}else{
			throw new BusinessException("不支持的校验类型，仅有ticket,hotel");
		}
		
		if("true".equals(PAY_TEST)){
			return true;
		}else{
			try {
				url = new URL("https://mapi.alipay.com/gateway.do?service=notify_verify&partner="+sysParamDao.findSysParam(partner)+"&notify_id="+notifyId);
				BufferedReader in = null;
				in = new BufferedReader(new InputStreamReader(url.openStream()));
				String line = in.readLine();
				if(line!=null && line.trim().equals("true")){
					return true;
				}
				return false;
			} 
			catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
		
	}

	@Override
	public common.pay.RefundParams initRefundParams(BaseTranRec baseTranRec, String amt,int type) {
		return payService.initRefundParams(baseTranRec, amt,type);
	}

	@Override
	public boolean refund(RefundParams refundParams) {
		return payService.refund(refundParams);
	}
	
	@Override
	public boolean saveRefund(BaseRefundResult refundResult,String Type) {
		if(verifyNotifyId(refundResult.getNotifyId(),Type)){//验证notifyid
			List<BaseRefundResult> temp = baseRefundResultDao.findByProperty("batchNo", refundResult.getBatchNo()); 
			if(temp.isEmpty()){
				baseRefundResultService.saveRefund(refundResult);
			}else{
				BaseRefundResult rr = temp.get(0);
				fillRR(rr,refundResult);
				baseRefundResultDao.update(rr);
			}
			return true;
		}else{
			return false;
		}
	}

	private void fillRR(BaseRefundResult rr, BaseRefundResult refundResult) {
		rr.setBatchNo(refundResult.getBatchNo());
		rr.setNotifyId(refundResult.getNotifyId());
		rr.setNotifyTime(refundResult.getNotifyTime());
		rr.setNotifyType(refundResult.getNotifyType());
		rr.setResultDetails(refundResult.getResultDetails());
		rr.setSign(refundResult.getSign());
		rr.setSignType(refundResult.getSignType());
		rr.setSuccessNum(refundResult.getSuccessNum());
		rr.setUnfreezedDeta(refundResult.getUnfreezedDeta());
	}
	
}
