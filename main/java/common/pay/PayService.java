package common.pay;

import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.BaseRefundResult;
import com.xlem.dao.BaseTranRec;
import com.xlem.dao.HotelIndent;
import com.xlem.dao.TicketIndent;

public interface PayService {

	/**
	 * 
	 * @param indentCode 订单号
	 * @param moneyNeedPay 交易金额
	 * @param moneyNeedPaySub 分润金额
	 * @return
	 */
	PayParams initPayParams(TicketIndent ticketIndent, Double moneyNeedPay, Double moneyNeedPaySub);

	/**
	 * 
	 * @param indentCode 订单号
	 * @param moneyNeedPay 交易金额
	 * @return
	 */
	PayParams initPayParams(HotelIndent hotelIndent, Double moneyNeedPay);
	
	/**
	 * 
	 * @param indentCode 订单号
	 * @param moneyNeedPay 交易金额
	 * @return
	 */
	PayParams initPayParams(String rechargeKey, Double moneyNeedPay);	
	
	boolean refund(RefundParams refundParams);

	/**
	 * 
	 * @param baseTranRec 
	 * @param amt 退款金额
	 * @param type 1:票、2：酒店
	 * @return
	 */
	RefundParams initRefundParams(BaseTranRec baseTranRec, String amt,int type);

	/**
	 * 保存退款处理情况
	 * @param refundResult
	 * @return
	 */
	boolean saveRefund(BaseRefundResult refundResult);


}
