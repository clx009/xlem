package common;

import java.sql.Timestamp;

public class FullCalBean {
	private String id;
	private String title;
	private Timestamp start;
	private Timestamp end;
	private String url;
	private String title1;
	private String title2;
	
	
	private String title_def;
	private String title1_def;
	private String title_fri;
	private String title1_fri;
	private String title_sat;
	private String title1_sat;
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Timestamp getStart() {
		return start;
	}

	public void setStart(Timestamp start) {
		this.start = start;
	}

	public Timestamp getEnd() {
		return end;
	}

	public void setEnd(Timestamp end) {
		this.end = end;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle1() {
		return title1;
	}

	public void setTitle1(String title1) {
		this.title1 = title1;
	}

	public String getTitle2() {
		return title2;
	}

	public void setTitle2(String title2) {
		this.title2 = title2;
	}

	public String getTitle_def() {
		return title_def;
	}

	public void setTitle_def(String title_def) {
		this.title_def = title_def;
	}

	public String getTitle1_def() {
		return title1_def;
	}

	public void setTitle1_def(String title1_def) {
		this.title1_def = title1_def;
	}

	public String getTitle_fri() {
		return title_fri;
	}

	public void setTitle_fri(String title_fri) {
		this.title_fri = title_fri;
	}

	public String getTitle1_fri() {
		return title1_fri;
	}

	public void setTitle1_fri(String title1_fri) {
		this.title1_fri = title1_fri;
	}

	public String getTitle_sat() {
		return title_sat;
	}

	public void setTitle_sat(String title_sat) {
		this.title_sat = title_sat;
	}

	public String getTitle1_sat() {
		return title1_sat;
	}

	public void setTitle1_sat(String title1_sat) {
		this.title1_sat = title1_sat;
	}
	
	
}
