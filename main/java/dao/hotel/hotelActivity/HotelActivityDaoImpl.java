package dao.hotel.hotelActivity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.HotelActivity;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("hotelActivityDao")
public class HotelActivityDaoImpl extends BaseDaoImpl<HotelActivity> implements HotelActivityDao {

}