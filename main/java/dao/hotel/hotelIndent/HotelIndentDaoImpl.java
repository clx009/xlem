package dao.hotel.hotelIndent;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.Hotel;
import com.xlem.dao.HotelCheckRec;
import com.xlem.dao.HotelIndent;
import com.xlem.dao.HotelIndentDetail;
import com.xlem.dao.HotelRoomType;

import common.CommonMethod;
import common.action.JsonPager;
import common.dao.BaseDaoImpl;

@Transactional
@Repository("hotelIndentDao")
public class HotelIndentDaoImpl extends BaseDaoImpl<HotelIndent> implements HotelIndentDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<HotelIndent> findHotelIndentList(final HotelIndent t,final long userId,
			final JsonPager<HotelIndent> jp, final String forPage) {
		Session session = sessionFactory.getCurrentSession();
		StringBuilder sqlBuilder=new StringBuilder();
		sqlBuilder.append("SELECT t_pager_order.* FROM ");
		sqlBuilder.append("(SELECT ROWNUM row_num,t_pager.* FROM");
		sqlBuilder.append("(SELECT ");
		sqlBuilder.append("t.HOTEL_INDENT_ID,t.INDENT_CODE,t.TOTAL_AMOUNT,t.TOTAL_PRICE,");					
		sqlBuilder.append("t.START_DATE, t.END_DATE,t.LINKMAN,t.ID_CARD_NUMBER,t.PHONE_NUMBER,");
		sqlBuilder.append("t.CHECK_CODE,t.TEL_NUMBER,t.FAX_NUMBER,t.PAY_TYPE,t.FRONT_MONEY,t.PAY_TIME,");
		sqlBuilder.append("t.CUSTOMER_TYPE,t.CUSTOMER_NAME,t.PEOPLE_NUM,t.BED_MONEY,t.MEAL_SCALE,");	
		sqlBuilder.append("t.ARRIVE_TIME,t.COUNTRY,t.VERIFIER,t.ORIGIN,t.SYNCHRO_STATE,t.TRX_ID,");
		sqlBuilder.append("CAST(t.STATUS AS VARCHAR2(2)) AS STATUS,");		
		sqlBuilder.append("t.REMARK,t.INPUTER,t.INPUT_TIME,t.UPDATER,t.UPDATE_TIME,t.VERSION,t.HOTEL_NAME,");		
		sqlBuilder.append("to_char(WMSYS.WM_CONCAT(t.TYPE_NAME)) TYPE_NAMES ");		
		sqlBuilder.append("FROM ");	
		sqlBuilder.append("(select hIndent.*,typeName.Type_Name,typeName.hotel_name from ");	 
		sqlBuilder.append("HOTEL_INDENT hIndent,(select hIndentDetail.Hotel_Indent_Id,hRoomType.Type_Name,h.hotel_name ");	
		sqlBuilder.append("from HOTEL_INDENT_DETAIL hIndentDetail,HOTEL_ROOM_TYPE hRoomType,HOTEL h ");	
		sqlBuilder.append("where hIndentDetail.ROOM_TYPE_ID=hRoomType.ROOM_TYPE_ID and HROOMTYPE.HOTEL_ID=h.HOTEL_ID ");	
		sqlBuilder.append("group by hIndentDetail.Hotel_Indent_Id,hRoomType.Type_Name,h.hotel_name) typeName ");	
		sqlBuilder.append("where hIndent.Hotel_Indent_Id=typeName.Hotel_Indent_Id ");//以上sql测试正确
		
		if(!"freeRoomLoss".equals(forPage)){
			sqlBuilder.append("AND hIndent.INPUTER=:inputer ");
		}else{
			sqlBuilder.append("AND hIndent.ORIGIN = '00' ");
		}
		
		//动态增加查询条件
		if("returnPage".equals(forPage) || "freeRoomLoss".equals(forPage)){
			sqlBuilder.append("AND hIndent.STATUS = '02' ");
		}
		if(t.getStatus()!=null && !t.getStatus().trim().isEmpty()){
			sqlBuilder.append("AND hIndent.STATUS = :status ");
		}
		if(t.getStartDate()!=null){
			sqlBuilder.append("AND hIndent.START_DATE >= :startDate ");
		}
		if(t.getEndDate()!=null){
			sqlBuilder.append("AND hIndent.END_DATE <= :endDate ");
		}
		if(t.getIndentCode()!=null && !t.getIndentCode().trim().isEmpty()){
			sqlBuilder.append("AND hIndent.INDENT_CODE LIKE :indentCode ");
		}
		if(t.getPhoneNumber()!=null && !t.getPhoneNumber().trim().isEmpty()){
			sqlBuilder.append("AND hIndent.PHONE_NUMBER LIKE :phoneNumber ");
		}
		if(t.getIdCardNumber()!=null && !t.getIdCardNumber().trim().isEmpty()){
			sqlBuilder.append("AND hIndent.ID_CARD_NUMBER LIKE :idCardNumber ");
		}
		sqlBuilder.append("ORDER BY hIndent.INPUT_TIME) t ");
		sqlBuilder.append("GROUP BY t.HOTEL_INDENT_ID,t.INDENT_CODE,t.TOTAL_AMOUNT,t.TOTAL_PRICE,");
		sqlBuilder.append("t.START_DATE, t.END_DATE,t.LINKMAN,t.ID_CARD_NUMBER,t.PHONE_NUMBER,");
		sqlBuilder.append("t.CHECK_CODE,t.TEL_NUMBER,t.FAX_NUMBER,t.PAY_TYPE,t.FRONT_MONEY,t.PAY_TIME,");
		sqlBuilder.append("t.CUSTOMER_TYPE,t.CUSTOMER_NAME,t.PEOPLE_NUM,t.BED_MONEY,t.MEAL_SCALE,");
		sqlBuilder.append("t.ARRIVE_TIME,t.COUNTRY,t.VERIFIER,t.ORIGIN,t.SYNCHRO_STATE,t.TRX_ID,t.STATUS,t.REMARK,");
		sqlBuilder.append("t.INPUTER,t.INPUT_TIME,t.UPDATER,t.UPDATE_TIME,t.VERSION,t.HOTEL_NAME ORDER BY t.INPUT_TIME DESC");
		sqlBuilder.append(") t_pager WHERE ROWNUM< :endIndex");
		sqlBuilder.append(") t_pager_order WHERE t_pager_order.row_num>= :beginIndex");//分页查询记录的初始索引
	
		
		Query query=session.createSQLQuery(sqlBuilder.toString());
		
		if(!"freeRoomLoss".equals(forPage)){
			query.setLong("inputer",userId);
		}
		
		//设置查询条件
		if(t.getStatus()!=null && !t.getStatus().trim().isEmpty()){
			query.setString("status",t.getStatus().trim());
		}
		if(t.getStartDate()!=null){
			query.setTimestamp("startDate",CommonMethod.toStartTime(Timestamp.valueOf(t.getStartDate().toString())));
		}
		if(t.getEndDate()!=null){
			query.setTimestamp("endDate",CommonMethod.toEndTime(Timestamp.valueOf(t.getEndDate().toString())));
		}
		if(t.getIndentCode()!=null && !t.getIndentCode().trim().isEmpty()){
			query.setString("indentCode","%"+t.getIndentCode().trim()+"%");
		}
		if(t.getPhoneNumber()!=null && !t.getPhoneNumber().trim().isEmpty()){
			query.setString("phoneNumber","%"+t.getPhoneNumber().trim()+"%");
		}
		if(t.getIdCardNumber()!=null && !t.getIdCardNumber().trim().isEmpty()){
			query.setString("idCardNumber","%"+t.getIdCardNumber().trim()+"%");
		}
		
		int beginIndex=Integer.valueOf(jp.getStart().trim());
		int limit=Integer.valueOf(jp.getLimit().trim());
		query.setInteger("beginIndex",beginIndex);
		query.setInteger("endIndex",beginIndex+limit);
	
		@SuppressWarnings("rawtypes")
		List indentList=query.list();
		List<HotelIndent> hotelIndentList=new ArrayList<HotelIndent>();
		for(Object o:indentList){
			HotelIndent hotelIndent=new HotelIndent();
			Object[] tmp=(Object[])o;
			if(tmp[1]!=null){
				hotelIndent.setHotelIndentId(Long.valueOf(tmp[1].toString().trim()));
			}
			if(tmp[2]!=null){
				hotelIndent.setIndentCode(tmp[2].toString());
			}
			if(tmp[3]!=null){
				hotelIndent.setTotalAmount(Integer.valueOf(tmp[3].toString()));
			}
			if(tmp[4]!=null){
				hotelIndent.setTotalPrice(Double.valueOf(tmp[4].toString()));
			}
			if(tmp[5]!=null){
				hotelIndent.setStartDate(CommonMethod.string2Time1(tmp[5].toString().trim()));
			}
			if(tmp[6]!=null){
				hotelIndent.setEndDate(CommonMethod.string2Time1(tmp[6].toString().trim()));
			}
			if(tmp[7]!=null){
				hotelIndent.setLinkman(tmp[7].toString());
			}
			if(tmp[8]!=null){
				hotelIndent.setIdCardNumber(tmp[8].toString());
			}
			if(tmp[9]!=null){
				hotelIndent.setPhoneNumber(tmp[9].toString());
			}
			if(tmp[10]!=null){
				hotelIndent.setCheckCode(tmp[10].toString());
			}
			if(tmp[11]!=null){
				hotelIndent.setTelNumber(tmp[11].toString());
			}
			if(tmp[12]!=null){
				hotelIndent.setFaxNumber(tmp[12].toString());
			}
			if(tmp[13]!=null){
				hotelIndent.setPayType(tmp[13].toString());
			}
			if(tmp[14]!=null){
				hotelIndent.setFrontMoney(Double.valueOf(tmp[14].toString().trim()));
			}
			if(tmp[15]!=null){
				hotelIndent.setPayTime(CommonMethod.string2Time1(tmp[15].toString().trim()));
			}
			if(tmp[16]!=null){
				hotelIndent.setCustomerType(tmp[16].toString());
			}
			if(tmp[17]!=null){
				hotelIndent.setCustomerName(tmp[17].toString());
			}
			if(tmp[18]!=null){
				hotelIndent.setPeopleNum(Integer.valueOf(tmp[18].toString().trim()));
			}
			if(tmp[19]!=null){
				hotelIndent.setBedMoney(Double.valueOf(tmp[19].toString().trim()));
			}
			if(tmp[20]!=null){
				hotelIndent.setMealScale(Double.valueOf(tmp[20].toString().trim()));
			}
			if(tmp[21]!=null){
				hotelIndent.setArriveTime(CommonMethod.string2Time1(tmp[21].toString()));
			}
			if(tmp[22]!=null){
				hotelIndent.setCountry(tmp[22].toString());
			}
			if(tmp[23]!=null){
				hotelIndent.setVerifier(Long.valueOf(tmp[23].toString().trim()));
			}
			if(tmp[24]!=null){
				hotelIndent.setOrigin(tmp[24].toString());
			}
			if(tmp[25]!=null){
				hotelIndent.setSynchroState(tmp[25].toString());
			}
			if(tmp[26]!=null){
				hotelIndent.setTrxId(tmp[26].toString());
			}
			if(tmp[27]!=null){
				hotelIndent.setStatus(tmp[27].toString());
			}
			if(tmp[28]!=null){
				hotelIndent.setRemark(tmp[28].toString());
			}
			if(tmp[29]!=null){
				hotelIndent.setInputer(Long.valueOf(tmp[29].toString().trim()));
			}
			if(tmp[30]!=null){
				hotelIndent.setInputTime(CommonMethod.string2Time1(tmp[30].toString().trim()));
			}
			if(tmp[31]!=null){
				hotelIndent.setUpdater(Long.valueOf(tmp[31].toString().trim()));
			}
			if(tmp[32]!=null){
				hotelIndent.setUpdateTime(CommonMethod.string2Time1(tmp[32].toString().trim()));
			}
			if(tmp[33]!=null){
				hotelIndent.setVersion(Long.valueOf(Long.valueOf(tmp[33].toString().trim())));
			}
			if(tmp[34]!=null){
				hotelIndent.setHotelName(tmp[34].toString());
			}
			if(tmp[35]!=null){
				String roomTypes=tmp[35].toString().trim();
				if(roomTypes.indexOf(",")>=0){
					roomTypes=roomTypes.replaceAll(",","<br/>");
				}
				hotelIndent.setRoomTypes(roomTypes);
			}
			hotelIndentList.add(hotelIndent);
		}
		return hotelIndentList;
	}
	
	@Override
	public int findIndentListCount(final HotelIndent t,final  long userId,
			final JsonPager<HotelIndent> jp, final String forPage) {
		Session session = sessionFactory.getCurrentSession();
		StringBuilder sqlBuilder=new StringBuilder();
		sqlBuilder.append("SELECT COUNT(distinct hIndent.HOTEL_INDENT_ID) AS TOTAL_AMOUNT ");
		sqlBuilder.append("FROM HOTEL_INDENT hIndent,HOTEL_INDENT_DETAIL hIndentDetail ");
		sqlBuilder.append("WHERE hIndent.HOTEL_INDENT_ID=hIndentDetail.HOTEL_INDENT_ID ");
		
		if(!"freeRoomLoss".equals(forPage)){
			sqlBuilder.append("AND hIndent.INPUTER=:inputer ");
		}else{
			sqlBuilder.append("AND hIndent.ORIGIN = '00' ");
		}
		
		if("returnPage".equals(forPage) || "freeRoomLoss".equals(forPage)){
			sqlBuilder.append("AND hIndent.STATUS = '02' ");
		}
		if(t.getStatus()!=null && !t.getStatus().trim().isEmpty()){
			sqlBuilder.append("AND hIndent.STATUS = :status ");
		}
		if(t.getStartDate()!=null){
			sqlBuilder.append("AND hIndent.START_DATE >= :startDate ");
		}
		if(t.getEndDate()!=null){
			sqlBuilder.append("AND hIndent.START_DATE <= :endDate ");
		}
		if(t.getIndentCode()!=null && !t.getIndentCode().trim().isEmpty()){
			sqlBuilder.append("AND hIndent.INDENT_CODE LIKE :indentCode ");
		}
		if(t.getPhoneNumber()!=null && !t.getPhoneNumber().trim().isEmpty()){
			sqlBuilder.append("AND hIndent.PHONE_NUMBER LIKE :phoneNumber ");
		}
		if(t.getIdCardNumber()!=null && !t.getIdCardNumber().trim().isEmpty()){
			sqlBuilder.append("AND hIndent.ID_CARD_NUMBER LIKE :idCardNumber ");
		}
		
		Query query=session.createSQLQuery(sqlBuilder.toString());
		if(!"freeRoomLoss".equals(forPage)){
			query.setLong("inputer",userId);
		}
		
		if(t.getStatus()!=null && !t.getStatus().trim().isEmpty()){
			query.setString("status",t.getStatus().trim());
		}
		
		if(t.getStartDate()!=null){
			query.setTimestamp("startDate", CommonMethod.toStartTime(Timestamp.valueOf(t.getStartDate().toString())));
		}
		
		if(t.getEndDate()!=null){
			query.setTimestamp("endDate", CommonMethod.toEndTime(Timestamp.valueOf(t.getEndDate().toString())));
		}
		if(t.getIndentCode()!=null && !t.getIndentCode().trim().isEmpty()){
			query.setString("indentCode", "%"+t.getIndentCode().trim()+"%");
		}
		
		if(t.getPhoneNumber()!=null && !t.getPhoneNumber().trim().isEmpty()){
			query.setString("phoneNumber", "%"+t.getPhoneNumber().trim()+"%");
		}
		
		if(t.getIdCardNumber()!=null && !t.getIdCardNumber().trim().isEmpty()){
			query.setString("idCardNumber", "%"+t.getIdCardNumber().trim()+"%");
		}
		@SuppressWarnings("rawtypes")
		List result=query.list();
		int count=Integer.valueOf(result.get(0).toString().trim());
		return count;
	}
	
	@Override
	public int findIndentListCountForMKT(final HotelIndent t, final Long userId,
			JsonPager<HotelIndent> jp, final String forPage) {
		Session session = sessionFactory.getCurrentSession();
		StringBuilder sqlBuilder=new StringBuilder();
		sqlBuilder.append("SELECT COUNT(distinct hIndent.HOTEL_INDENT_ID) AS TOTAL_AMOUNT ");
		
		if("cancelAll".equals(forPage)){//查询取消和退订订单
			sqlBuilder.append("FROM HOTEL_INDENT hIndent,HOTEL_DETAIL_HIS hIndentDetail ");
		}else{
			sqlBuilder.append("FROM HOTEL_INDENT hIndent,HOTEL_INDENT_DETAIL hIndentDetail ");
		}
		sqlBuilder.append("WHERE hIndent.HOTEL_INDENT_ID=hIndentDetail.HOTEL_INDENT_ID ");
		
		if(!"queryAll".equals(forPage) && !"cancelAll".equals(forPage)){
			if("11".equals(t.getIsQuery())){
				sqlBuilder.append("AND (hIndent.ORIGIN <> :origin OR (hIndent.CUSTOMER_TYPE = '01' AND hIndent.status = '01')) ");
			}
			else{
				sqlBuilder.append("AND hIndent.ORIGIN <> :origin ");
			}
		}
		if("returnPage".equals(forPage)||"freeRoomLoss".equals(forPage)){
			sqlBuilder.append("AND hIndent.STATUS = '02' ");
		}if("cancelAll".equals(forPage)){//查询取消和退订订单
			sqlBuilder.append("AND hIndent.STATUS in ('05','06')  ");
		}
		if(t.getStatus()!=null && !t.getStatus().trim().isEmpty()){
			sqlBuilder.append("AND hIndent.STATUS = :status ");
		}
		if(t.getPayType()!=null && !t.getPayType().trim().isEmpty()){
			sqlBuilder.append("AND hIndent.PAY_TYPE = '"+t.getPayType()+"' ");
		}
		if(t.getStartDate()!=null){
			sqlBuilder.append("AND hIndent.START_DATE >= :startDate ");
		}
		if(t.getEndDate()!=null){
			sqlBuilder.append("AND hIndent.START_DATE <= :endDate ");
		}
		if(t.getIndentCode()!=null && !t.getIndentCode().trim().isEmpty()){
			sqlBuilder.append("AND hIndent.INDENT_CODE LIKE :indentCode ");
		}
		if(t.getPhoneNumber()!=null && !t.getPhoneNumber().trim().isEmpty()){
			sqlBuilder.append("AND hIndent.PHONE_NUMBER LIKE :phoneNumber ");
		}
		if(t.getIdCardNumber()!=null && !t.getIdCardNumber().trim().isEmpty()){
			sqlBuilder.append("AND hIndent.ID_CARD_NUMBER LIKE :idCardNumber ");
		}
		
		if(t.getInputer()!=null){
			sqlBuilder.append("AND hIndent.INPUTER = :inputer ");
		}
		if(t.getHotel()!=null && t.getHotel().getHotelId() !=null){
			sqlBuilder.append("AND hIndent.hotel_Id = :hotelId ");
		}
		
		if(t.getInputerStartDate()!=null){
			sqlBuilder.append("AND hIndent.INPUT_TIME >= :inputerStartDate ");
		}
		if(t.getInputerEndDate()!=null){
			sqlBuilder.append("AND hIndent.INPUT_TIME <= :inputerEndDate ");
		}
		
		Query query=session.createSQLQuery(sqlBuilder.toString());
		if(!"queryAll".equals(forPage) && !"cancelAll".equals(forPage)){
			query.setString("origin","00");//所有市场部订单
		}
		
		if(t.getStatus()!=null && !t.getStatus().trim().isEmpty()){
			query.setString("status",t.getStatus());
		}
		
//					if(t.getPayType()!=null && !t.getPayType().trim().isEmpty()){
//						query.setString("payType",t.getPayType());
//					}
		
		if(t.getStartDate()!=null){
			query.setTimestamp("startDate", CommonMethod.toStartTime(Timestamp.valueOf(t.getStartDate().toString())));
		}
		
		if(t.getEndDate()!=null){
			query.setTimestamp("endDate", CommonMethod.toEndTime(Timestamp.valueOf(t.getEndDate().toString())));
		}
		if(t.getIndentCode()!=null && !t.getIndentCode().trim().isEmpty()){
			query.setString("indentCode", "%"+t.getIndentCode().trim()+"%");
		}
		
		if(t.getPhoneNumber()!=null && !t.getPhoneNumber().trim().isEmpty()){
			query.setString("phoneNumber", "%"+t.getPhoneNumber().trim()+"%");
		}
		
		if(t.getIdCardNumber()!=null && !t.getIdCardNumber().trim().isEmpty()){
			query.setString("idCardNumber", "%"+t.getIdCardNumber().trim()+"%");
		}
		if(t.getInputer()!=null){
			query.setLong("inputer", t.getInputer());
		}
		if(t.getHotel()!=null && t.getHotel().getHotelId() !=null){
			query.setLong("hotelId", t.getHotel().getHotelId());
		}
		if(t.getInputerStartDate()!=null){
			query.setTimestamp("inputerStartDate", CommonMethod.toStartTime(t.getInputerStartDate()));
		}
		if(t.getInputerEndDate()!=null){
			query.setTimestamp("inputerEndDate", CommonMethod.toEndTime(t.getInputerEndDate()));
		}
		
		@SuppressWarnings("rawtypes")
		List result=query.list();
		int count=Integer.valueOf(result.get(0).toString().trim());
		return count;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<HotelIndent> findHotelIndentListForMKT(final HotelIndent t,
			final Long userId, final JsonPager<HotelIndent> jp, final String forPage) {
		Session session = sessionFactory.getCurrentSession();
		StringBuilder sqlBuilder=new StringBuilder();
		sqlBuilder.append("SELECT t_pager_order.* FROM ");
		sqlBuilder.append("(SELECT ROWNUM row_num,t_pager.* FROM");
		sqlBuilder.append("(SELECT ");
		sqlBuilder.append("t.HOTEL_INDENT_ID,t.INDENT_CODE,t.TOTAL_AMOUNT,t.TOTAL_PRICE,");					
		sqlBuilder.append("t.START_DATE, t.END_DATE,t.LINKMAN,t.ID_CARD_NUMBER,t.PHONE_NUMBER,");
		sqlBuilder.append("t.CHECK_CODE,t.TEL_NUMBER,t.FAX_NUMBER,CAST(t.PAY_TYPE as varchar2(2)),t.FRONT_MONEY,t.PAY_TIME,");
		sqlBuilder.append("t.CUSTOMER_TYPE,t.CUSTOMER_NAME,t.PEOPLE_NUM,t.BED_MONEY,t.MEAL_SCALE,");	
		sqlBuilder.append("t.ARRIVE_TIME,t.COUNTRY,t.VERIFIER,t.ORIGIN,t.SYNCHRO_STATE,t.TRX_ID,");
		sqlBuilder.append("CAST(t.STATUS AS VARCHAR2(2)) AS STATUS,");		
		sqlBuilder.append("t.REMARK,t.INPUTER,t.INPUTERNAME,t.INPUT_TIME,t.UPDATER,t.UPDATERNAME,t.UPDATE_TIME,t.VERSION,t.HOTEL_NAME,");		
		sqlBuilder.append("WMSYS.WM_CONCAT(t.TYPE_NAME) TYPE_NAMES,t.HOTEL_ID,t.HOTEL_ACTIVITY_ID ");		
		sqlBuilder.append("FROM ");	
		sqlBuilder.append("(select hIndent.*,suInputer.user_name as INPUTERNAME,nvl(suUpdater.user_name,'') as UPDATERNAME,typeName.Type_Name,typeName.hotel_name from ");
		
		if("cancelAll".equals(forPage)){//查询取消和退订订单
			sqlBuilder.append("(select hIndentDetail.Hotel_Indent_Id,hRoomType.Type_Name,h.hotel_name ");	
			sqlBuilder.append("from HOTEL_DETAIL_HIS hIndentDetail,HOTEL_ROOM_TYPE hRoomType,HOTEL h ");	
			sqlBuilder.append("where hIndentDetail.ROOM_TYPE_ID=hRoomType.ROOM_TYPE_ID and HROOMTYPE.HOTEL_ID=h.HOTEL_ID ");	
			sqlBuilder.append("group by hIndentDetail.Hotel_Indent_Id,hRoomType.Type_Name,h.hotel_name) typeName, ");
		}else{
			sqlBuilder.append("(select hIndentDetail.Hotel_Indent_Id,hRoomType.Type_Name,h.hotel_name ");	
			sqlBuilder.append("from HOTEL_INDENT_DETAIL hIndentDetail,HOTEL_ROOM_TYPE hRoomType,HOTEL h ");	
			sqlBuilder.append("where hIndentDetail.ROOM_TYPE_ID=hRoomType.ROOM_TYPE_ID and HROOMTYPE.HOTEL_ID=h.HOTEL_ID ");	
			sqlBuilder.append("group by hIndentDetail.Hotel_Indent_Id,hRoomType.Type_Name,h.hotel_name) typeName, ");
		}
		sqlBuilder.append(" SYS_USER suInputer,HOTEL_INDENT hIndent left join SYS_USER suUpdater on hIndent.UPDATER=suUpdater.user_id ");
		
		sqlBuilder.append("where hIndent.Hotel_Indent_Id=typeName.Hotel_Indent_Id ");//以上sql测试正确
		sqlBuilder.append("AND hIndent.INPUTER=suInputer.user_id  ");
		if(!"queryAll".equals(forPage) && !"cancelAll".equals(forPage)){
			if("11".equals(t.getIsQuery())){
				sqlBuilder.append("AND (hIndent.ORIGIN <> :origin OR (hIndent.CUSTOMER_TYPE = '01' AND hIndent.status = '01')) ");
			}
			else{
				sqlBuilder.append("AND hIndent.ORIGIN <> :origin ");
			}
		}
		
		//动态增加查询条件
		if("returnPage".equals(forPage)||"freeRoomLoss".equals(forPage)){
			sqlBuilder.append("AND hIndent.STATUS = '02' ");
		}else if("cancelAll".equals(forPage)){//查询取消和退订订单
			sqlBuilder.append("AND hIndent.STATUS in ('05','06')  ");
		}
		
		if(t.getStatus()!=null && !t.getStatus().trim().isEmpty()){
			sqlBuilder.append("AND hIndent.STATUS = :status ");
		}
		
		if(t.getPayType()!=null && !t.getPayType().trim().isEmpty()){
			sqlBuilder.append("AND hIndent.PAY_TYPE = '"+t.getPayType()+"' ");
		}
		if(t.getStartDate()!=null){
			sqlBuilder.append("AND hIndent.START_DATE >= :startDate ");
		}
		if(t.getEndDate()!=null){
			sqlBuilder.append("AND hIndent.START_DATE <= :endDate ");
		}
		if(t.getIndentCode()!=null && !t.getIndentCode().trim().isEmpty()){
			sqlBuilder.append("AND hIndent.INDENT_CODE LIKE :indentCode ");
		}
		if(t.getPhoneNumber()!=null && !t.getPhoneNumber().trim().isEmpty()){
			sqlBuilder.append("AND hIndent.PHONE_NUMBER LIKE :phoneNumber ");
		}
		if(t.getIdCardNumber()!=null && !t.getIdCardNumber().trim().isEmpty()){
			sqlBuilder.append("AND hIndent.ID_CARD_NUMBER LIKE :idCardNumber ");
		}
		if(t.getInputer()!=null){
			sqlBuilder.append("AND hIndent.INPUTER = :inputer ");
		}
		if(t.getHotel()!=null && t.getHotel().getHotelId() !=null){
			sqlBuilder.append("AND hIndent.Hotel_ID = :hotelId ");
		}
		if(t.getInputerStartDate()!=null){
			sqlBuilder.append("AND hIndent.INPUT_TIME >= :inputerStartDate ");
		}
		if(t.getInputerEndDate()!=null){
			sqlBuilder.append("AND hIndent.INPUT_TIME <= :inputerEndDate ");
		}
		sqlBuilder.append("ORDER BY hIndent.INPUT_TIME) t ");
		sqlBuilder.append("GROUP BY t.HOTEL_INDENT_ID,t.INDENT_CODE,t.TOTAL_AMOUNT,t.TOTAL_PRICE,");
		sqlBuilder.append("t.START_DATE, t.END_DATE,t.LINKMAN,t.ID_CARD_NUMBER,t.PHONE_NUMBER,");
		sqlBuilder.append("t.CHECK_CODE,t.TEL_NUMBER,t.FAX_NUMBER,t.PAY_TYPE,t.FRONT_MONEY,t.PAY_TIME,");
		sqlBuilder.append("t.CUSTOMER_TYPE,t.CUSTOMER_NAME,t.PEOPLE_NUM,t.BED_MONEY,t.MEAL_SCALE,");
		sqlBuilder.append("t.ARRIVE_TIME,t.COUNTRY,t.VERIFIER,t.ORIGIN,t.SYNCHRO_STATE,t.TRX_ID,t.STATUS,t.REMARK,");
		sqlBuilder.append("t.INPUTER,t.INPUTERNAME,t.INPUT_TIME,t.UPDATER,t.UPDATERNAME,t.UPDATE_TIME,t.VERSION,t.HOTEL_NAME,t.HOTEL_ID,t.HOTEL_ACTIVITY_ID ORDER BY t.INPUT_TIME DESC");
		sqlBuilder.append(") t_pager WHERE ROWNUM<= :endIndex");
		sqlBuilder.append(") t_pager_order WHERE t_pager_order.row_num> :beginIndex");//分页查询记录的初始索引
	
		
		Query query=session.createSQLQuery(sqlBuilder.toString());
		if(!"queryAll".equals(forPage) && !"cancelAll".equals(forPage)){
			query.setString("origin","00");//所有市场部订单
		}
		
		//设置查询条件
		if(t.getStatus()!=null && !t.getStatus().trim().isEmpty()){
			query.setString("status",t.getStatus());
		}
//				if(t.getPayType()!=null && !t.getPayType().trim().isEmpty()){
//					query.setString("payType",t.getPayType());
//				}
		if(t.getStartDate()!=null){
			query.setTimestamp("startDate",CommonMethod.toStartTime(Timestamp.valueOf(t.getStartDate().toString())));
		}
		if(t.getEndDate()!=null){
			query.setTimestamp("endDate",CommonMethod.toEndTime(Timestamp.valueOf(t.getEndDate().toString())));
		}
		if(t.getIndentCode()!=null && !t.getIndentCode().trim().isEmpty()){
			query.setString("indentCode","%"+t.getIndentCode().trim()+"%");
		}
		if(t.getPhoneNumber()!=null && !t.getPhoneNumber().trim().isEmpty()){
			query.setString("phoneNumber","%"+t.getPhoneNumber().trim()+"%");
		}
		if(t.getIdCardNumber()!=null && !t.getIdCardNumber().trim().isEmpty()){
			query.setString("idCardNumber","%"+t.getIdCardNumber().trim()+"%");
		}
		
		if(t.getInputer()!=null){
			query.setLong("inputer", t.getInputer());
		}
		if(t.getHotel()!=null && t.getHotel().getHotelId() !=null){
			query.setLong("hotelId", t.getHotel().getHotelId());
		}
		if(t.getInputerStartDate()!=null){
			query.setTimestamp("inputerStartDate", CommonMethod.toStartTime(t.getInputerStartDate()));
		}
		if(t.getInputerEndDate()!=null){
			query.setTimestamp("inputerEndDate", CommonMethod.toEndTime(t.getInputerEndDate()));
		}
		
		int beginIndex=Integer.valueOf(jp.getStart().trim());
		int limit=Integer.valueOf(jp.getLimit().trim());
		query.setInteger("beginIndex",beginIndex);
		query.setInteger("endIndex",beginIndex+limit);
	
		@SuppressWarnings("rawtypes")
		List indentList=query.list();
		List<HotelIndent> hotelIndentList=new ArrayList<HotelIndent>();
		for(Object o:indentList){
			HotelIndent hotelIndent=new HotelIndent();
			Object[] tmp=(Object[])o;
			if(tmp[1]!=null){
				hotelIndent.setHotelIndentId(Long.valueOf(tmp[1].toString().trim()));
			}
			if(tmp[2]!=null){
				hotelIndent.setIndentCode(tmp[2].toString());
			}
			if(tmp[3]!=null){
				hotelIndent.setTotalAmount(Integer.valueOf(tmp[3].toString()));
			}
			if(tmp[4]!=null){
				hotelIndent.setTotalPrice(Double.valueOf(tmp[4].toString()));
			}
			if(tmp[5]!=null){
				hotelIndent.setStartDate(CommonMethod.string2Time1(tmp[5].toString().trim()));
			}
			if(tmp[6]!=null){
				hotelIndent.setEndDate(CommonMethod.string2Time1(tmp[6].toString().trim()));
			}
			if(tmp[7]!=null){
				hotelIndent.setLinkman(tmp[7].toString());
			}
			if(tmp[8]!=null){
				hotelIndent.setIdCardNumber(tmp[8].toString());
			}
			if(tmp[9]!=null){
				hotelIndent.setPhoneNumber(tmp[9].toString());
			}
			if(tmp[10]!=null){
				hotelIndent.setCheckCode(tmp[10].toString());
			}
			if(tmp[11]!=null){
				hotelIndent.setTelNumber(tmp[11].toString());
			}
			if(tmp[12]!=null){
				hotelIndent.setFaxNumber(tmp[12].toString());
			}
			if(tmp[13]!=null){
				hotelIndent.setPayType(tmp[13].toString());
			}
			if(tmp[14]!=null){
				hotelIndent.setFrontMoney(Double.valueOf(tmp[14].toString().trim()));
			}
			if(tmp[15]!=null){
				hotelIndent.setPayTime(CommonMethod.string2Time1(tmp[15].toString().trim()));
			}
			if(tmp[16]!=null){
				hotelIndent.setCustomerType(tmp[16].toString());
			}
			if(tmp[17]!=null){
				hotelIndent.setCustomerName(tmp[17].toString());
			}
			if(tmp[18]!=null){
				hotelIndent.setPeopleNum(Integer.valueOf(tmp[18].toString().trim()));
			}
			if(tmp[19]!=null){
				hotelIndent.setBedMoney(Double.valueOf(tmp[19].toString().trim()));
			}
			if(tmp[20]!=null){
				hotelIndent.setMealScale(Double.valueOf(tmp[20].toString().trim()));
			}
			if(tmp[21]!=null){
				hotelIndent.setArriveTime(CommonMethod.string2Time1(tmp[21].toString()));
			}
			if(tmp[22]!=null){
				hotelIndent.setCountry(tmp[22].toString());
			}
			if(tmp[23]!=null){
				hotelIndent.setVerifier(Long.valueOf(tmp[23].toString().trim()));
			}
			if(tmp[24]!=null){
				hotelIndent.setOrigin(tmp[24].toString());
			}
			if(tmp[25]!=null){
				hotelIndent.setSynchroState(tmp[25].toString());
			}
			if(tmp[26]!=null){
				hotelIndent.setTrxId(tmp[26].toString());
			}
			if(tmp[27]!=null){
				hotelIndent.setStatus(tmp[27].toString());
			}
			if(tmp[28]!=null){
				hotelIndent.setRemark(tmp[28].toString());
			}
			if(tmp[29]!=null){
				hotelIndent.setInputer(Long.valueOf(tmp[29].toString().trim()));
			}
			if(tmp[30]!=null){
				hotelIndent.setInputerName(tmp[30].toString());
			}
			if(tmp[31]!=null){
				hotelIndent.setInputTime(CommonMethod.string2Time1(tmp[31].toString().trim()));
			}
			if(tmp[32]!=null){
				hotelIndent.setUpdater(Long.valueOf(tmp[32].toString().trim()));
			}
			if(tmp[33]!=null){
				hotelIndent.setUpdaterName(tmp[33].toString());
			}
			if(tmp[34]!=null){
				hotelIndent.setUpdateTime(CommonMethod.string2Time1(tmp[34].toString().trim()));
			}
			if(tmp[35]!=null){
				hotelIndent.setVersion(Long.valueOf(Long.valueOf(tmp[35].toString().trim())));
			}
			if(tmp[36]!=null){
				hotelIndent.setHotelName(tmp[36].toString());
			}
			if(tmp[37]!=null){
				String roomTypes=tmp[37].toString().trim();
				if(roomTypes.indexOf(",")>=0){
					roomTypes=roomTypes.replaceAll(",","<br/>");
				}
				hotelIndent.setRoomTypes(roomTypes);
			}
			Hotel hotel = new Hotel();
			hotel.setHotelId(tmp[38]==null?null:(Long.parseLong(tmp[38].toString())));
			hotelIndent.setHotel(hotel);
			if(tmp[39]!=null){
				hotelIndent.setHotelActivityId(Long.valueOf(Long.valueOf(tmp[39].toString().trim())));
			}
			hotelIndentList.add(hotelIndent);
			//hotel_activity_id
		}
		return hotelIndentList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Double> findHotelIndentSums(JsonPager<HotelIndent> jp,
			final HotelIndent t, final String forPage,Long userId) {
		Session session = sessionFactory.getCurrentSession();
		Map<String, Double> map = new HashMap<String, Double>();
		StringBuilder sqlBuilder=new StringBuilder();
		sqlBuilder.append("SELECT sum(nvl(hIndentDetail.subtotal,0)) totalSums ");
		sqlBuilder.append("FROM HOTEL_INDENT hIndent,HOTEL_INDENT_DETAIL hIndentDetail ");
		sqlBuilder.append("WHERE hIndent.HOTEL_INDENT_ID=hIndentDetail.HOTEL_INDENT_ID ");
		if(!"queryAll".equals(forPage)){
			sqlBuilder.append("AND hIndent.ORIGIN <> :origin ");
		}
		
		if("returnPage".equals(forPage)){
			sqlBuilder.append("AND hIndent.STATUS = '02' ");
		}
		if(t.getStatus()!=null && !t.getStatus().trim().isEmpty()){
			sqlBuilder.append("AND hIndent.STATUS = :status ");
		}
		if(t.getPayType()!=null && !t.getPayType().trim().isEmpty()){
			sqlBuilder.append("AND hIndent.PAY_TYPE = '"+t.getPayType()+"' ");
		}
		if(t.getStartDate()!=null){
			sqlBuilder.append("AND hIndent.START_DATE >= :startDate ");
		}
		if(t.getEndDate()!=null){
			sqlBuilder.append("AND hIndent.START_DATE <= :endDate ");
		}
		if(t.getIndentCode()!=null && !t.getIndentCode().trim().isEmpty()){
			sqlBuilder.append("AND hIndent.INDENT_CODE LIKE :indentCode ");
		}
		if(t.getPhoneNumber()!=null && !t.getPhoneNumber().trim().isEmpty()){
			sqlBuilder.append("AND hIndent.PHONE_NUMBER LIKE :phoneNumber ");
		}
		if(t.getIdCardNumber()!=null && !t.getIdCardNumber().trim().isEmpty()){
			sqlBuilder.append("AND hIndent.ID_CARD_NUMBER LIKE :idCardNumber ");
		}
		
		Query query=session.createSQLQuery(sqlBuilder.toString());
		if(!"queryAll".equals(forPage)){
			query.setString("origin","00");//所有市场部订单
		}
		
		if(t.getStatus()!=null && !t.getStatus().trim().isEmpty()){
			query.setString("status",t.getStatus());
		}
//						if(t.getPayType()!=null && !t.getPayType().trim().isEmpty()){
//							query.setString("payType",t.getPayType());
//						}
		if(t.getStartDate()!=null){
			query.setTimestamp("startDate", CommonMethod.toStartTime(Timestamp.valueOf(t.getStartDate().toString())));
		}
		
		if(t.getEndDate()!=null){
			query.setTimestamp("endDate", CommonMethod.toEndTime(Timestamp.valueOf(t.getEndDate().toString())));
		}
		if(t.getIndentCode()!=null && !t.getIndentCode().trim().isEmpty()){
			query.setString("indentCode", "%"+t.getIndentCode().trim()+"%");
		}
		
		if(t.getPhoneNumber()!=null && !t.getPhoneNumber().trim().isEmpty()){
			query.setString("phoneNumber", "%"+t.getPhoneNumber().trim()+"%");
		}
		
		if(t.getIdCardNumber()!=null && !t.getIdCardNumber().trim().isEmpty()){
			query.setString("idCardNumber", "%"+t.getIdCardNumber().trim()+"%");
		}
		@SuppressWarnings("rawtypes")
		List result=query.list();
		Double sums = 0d;
		if(result!=null && !result.isEmpty() && result.get(0)!=null){
			sums=Double.valueOf(result.get(0).toString().trim());
		}
		map.put("sums", sums);
		return map;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public int findHotelIndentTodayNums() {
		Session session = sessionFactory.getCurrentSession();
		int nums = 0;
		StringBuilder sqlBuilder=new StringBuilder();
		sqlBuilder.append("SELECT sum(nvl(hIndentDetail.amount,0)) totalNums ");
		sqlBuilder.append("FROM HOTEL_INDENT hIndent,HOTEL_INDENT_DETAIL hIndentDetail ");
		sqlBuilder.append("WHERE hIndent.HOTEL_INDENT_ID=hIndentDetail.HOTEL_INDENT_ID ");
		sqlBuilder.append("AND hIndent.STATUS in ('02','03','04') ");
		sqlBuilder.append("AND hIndentDetail.booking_Date = to_date('"+CommonMethod.getCurrentDate()+"','yyyy-MM-dd') ");
		Query query=session.createSQLQuery(sqlBuilder.toString());
		@SuppressWarnings("rawtypes")
		List result=query.list();
		if(result!=null && !result.isEmpty() && result.get(0)!=null){
			nums=Integer.valueOf(result.get(0).toString().trim());
		}
		return nums;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<HotelIndent> findHotelIndentListForFinance() {
		Session session = sessionFactory.getCurrentSession();
		StringBuilder sqlBuilder=new StringBuilder();
		sqlBuilder.append("SELECT ");		
		sqlBuilder.append("t.HOTEL_INDENT_ID,t.HOTEL_ID,t.INDENT_CODE, ");		
		sqlBuilder.append("t.TOTAL_AMOUNT,t.TOTAL_PRICE,t.START_DATE,t.END_DATE,t.LINKMAN,t.ID_CARD_NUMBER,t.PHONE_NUMBER,t.CHECK_CODE, ");		
		sqlBuilder.append("t.TEL_NUMBER,t.FAX_NUMBER,cast(t.PAY_TYPE as varchar2(2)) as PAY_TYPE,t.FRONT_MONEY,t.PAY_TIME,cast(t.CUSTOMER_TYPE as varchar2(2)) as CUSTOMER_TYPE,t.CUSTOMER_NAME,t.PEOPLE_NUM, ");		
		sqlBuilder.append("t.BED_MONEY,t.MEAL_SCALE,t.ARRIVE_TIME,t.COUNTRY,t.VERIFIER,cast(t.ORIGIN as varchar2(2)) as ORIGIN,cast(t.SYNCHRO_STATE as varchar2(2)) as SYNCHRO_STATE,t.TRX_ID,cast(t.STATUS as varchar2(2)) as STATUS,t.REMARK, ");		
		sqlBuilder.append("t.INPUTER,t.INPUT_TIME,t.UPDATER,t.UPDATE_TIME,t.VERSION,t.RECEIVED_MONEY,t.IS_CHECK,t.PER_FEE,t.CHECK_REC_ID,t.HOTEL_ACTIVITY_ID ");		
		sqlBuilder.append("FROM hotel_indent t ");		
		sqlBuilder.append("left join finance_hotel_accounts fha ");		
		sqlBuilder.append("on t.hotel_indent_id = fha.hotel_indent_id ");		
		sqlBuilder.append("where t.input_time > to_date('2014-07-15 00:00:00', 'YYYY-MM-DD HH24:MI:SS') ");		
		sqlBuilder.append("and fha.hotel_accounts_id is null ");	
		//sqlBuilder.append("and t.indent_code = '225b5A0021'");	
		
		Query query=session.createSQLQuery(sqlBuilder.toString());
		query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		
		@SuppressWarnings("rawtypes")
		List indentList=query.list();
		List<HotelIndent> hotelIndentList=new ArrayList<HotelIndent>();
		for(Object o:indentList){
			HotelIndent hotelIndent=new HotelIndent();
			
			Map map = (Map)o;
			
			hotelIndent.setHotelIndentId(map.get("HOTEL_INDENT_ID") == null ? 0L : Long.valueOf(map.get("HOTEL_INDENT_ID").toString()));
			hotelIndent.setIndentCode(map.get("INDENT_CODE") == null ? "" : map.get("INDENT_CODE").toString());
			
			hotelIndent.setTotalAmount(map.get("TOTAL_AMOUNT") == null ? 0 : Integer.valueOf(map.get("TOTAL_AMOUNT").toString()));
			hotelIndent.setTotalPrice(map.get("TOTAL_PRICE") == null ? 0D : Double.valueOf(map.get("TOTAL_PRICE").toString()));
			
			hotelIndent.setTotalPrice(map.get("TOTAL_PRICE") == null ? 0D : Double.valueOf(map.get("TOTAL_PRICE").toString()));
			
			hotelIndent.setStartDate(map.get("START_DATE") == null ? null : CommonMethod.string2Time1(map.get("START_DATE").toString()));
			hotelIndent.setEndDate(map.get("END_DATE") == null ? null : CommonMethod.string2Time1(map.get("END_DATE").toString()));
			
			hotelIndent.setLinkman(map.get("LINKMAN") == null ? "" : map.get("LINKMAN").toString());
			hotelIndent.setIdCardNumber(map.get("ID_CARD_NUMBER") == null ? "" : map.get("ID_CARD_NUMBER").toString());
			hotelIndent.setPhoneNumber(map.get("PHONE_NUMBER") == null ? "" : map.get("PHONE_NUMBER").toString());
			hotelIndent.setCheckCode(map.get("CHECK_CODE") == null ? "" : map.get("CHECK_CODE").toString());
			
			hotelIndent.setPayType(map.get("PAY_TYPE") == null ? "" : map.get("PAY_TYPE").toString());
			
			hotelIndent.setPayTime(map.get("PAY_TIME") == null ? null : CommonMethod.string2Time1(map.get("PAY_TIME").toString()));
			
			hotelIndent.setCustomerType(map.get("CUSTOMER_TYPE") == null ? "" : map.get("CUSTOMER_TYPE").toString());
			hotelIndent.setCustomerName(map.get("CUSTOMER_NAME") == null ? "" : map.get("CUSTOMER_NAME").toString());
			
			hotelIndent.setOrigin(map.get("ORIGIN") == null ? "" : map.get("ORIGIN").toString());
			hotelIndent.setSynchroState(map.get("SYNCHRO_STATE") == null ? "" : map.get("SYNCHRO_STATE").toString());
			hotelIndent.setStatus(map.get("STATUS") == null ? "" : map.get("STATUS").toString());
			
			hotelIndent.setInputer(map.get("INPUTER")==null?null:Long.valueOf(map.get("INPUTER").toString()));
			hotelIndent.setInputTime(map.get("INPUT_TIME")==null?null:CommonMethod.string2Time1(map.get("INPUT_TIME").toString()));
			hotelIndent.setUpdater(map.get("UPDATER")==null?null:Long.valueOf(map.get("UPDATER").toString()));
			hotelIndent.setUpdateTime(map.get("UPDATE_TIME")==null?null:CommonMethod.string2Time1(map.get("UPDATE_TIME").toString()));
			
			hotelIndentList.add(hotelIndent);
		}
		return hotelIndentList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List findRepeating(final HotelIndent hi) {
		Session session = sessionFactory.getCurrentSession();
				
		String sDate = CommonMethod.timeToString(Timestamp.valueOf(hi.getStartDate().toString()));
		String eDate = CommonMethod.timeToString(Timestamp.valueOf(hi.getEndDate().toString()));
		
		//查询订单
		StringBuilder sqlBuilder=new StringBuilder();
		sqlBuilder.append(" select ");
		sqlBuilder.append(" t.HOTEL_INDENT_ID,t.HOTEL_ID,t.INDENT_CODE, ");
		sqlBuilder.append(" t.TOTAL_AMOUNT,t.TOTAL_PRICE,t.START_DATE,t.END_DATE,t.LINKMAN,t.ID_CARD_NUMBER,t.PHONE_NUMBER,t.CHECK_CODE, ");
		sqlBuilder.append(" t.TEL_NUMBER,t.FAX_NUMBER,cast(t.PAY_TYPE as varchar2(2)) as PAY_TYPE,t.FRONT_MONEY, ");
		sqlBuilder.append(" t.PAY_TIME,cast(t.CUSTOMER_TYPE as varchar2(2)) as CUSTOMER_TYPE,t.CUSTOMER_NAME,t.PEOPLE_NUM, ");
		sqlBuilder.append(" t.BED_MONEY,t.MEAL_SCALE,t.ARRIVE_TIME,t.COUNTRY,t.VERIFIER,cast(t.ORIGIN as varchar2(2)) as ORIGIN, ");
		sqlBuilder.append(" cast(t.SYNCHRO_STATE as varchar2(2)) as SYNCHRO_STATE,t.TRX_ID,cast(t.STATUS as varchar2(2)) as STATUS,t.REMARK, ");
		sqlBuilder.append(" t.INPUTER,t.INPUT_TIME,t.UPDATER,t.UPDATE_TIME,t.VERSION,t.RECEIVED_MONEY,cast(t.IS_CHECK as varchar2(2)) as IS_CHECK,t.PER_FEE,t.CHECK_REC_ID,t.HOTEL_ACTIVITY_ID,h.HOTEL_NAME ");
		sqlBuilder.append(" from HOTEL_INDENT t,HOTEL h ");
		sqlBuilder.append(" where t.hotel_id = h.hotel_id and t.origin <> '00' and t.status in ('01','02','03','04') ");
		sqlBuilder.append(" and (t.linkman = '"+hi.getLinkman()+"' or t.id_card_number = '"+hi.getIdCardNumber()+"' or t.phone_number = '"+hi.getPhoneNumber()+"') ");
		sqlBuilder.append(" and t.hotel_indent_id in ( ");
		sqlBuilder.append(" select hid.hotel_indent_id from hotel_indent_detail hid where hid.booking_date >= TO_DATE('"+sDate+" 00:00:00','yyyy-mm-dd hh24:mi:ss') ");
		sqlBuilder.append(" and hid.booking_date <= TO_DATE('"+eDate+" 00:00:00','yyyy-mm-dd hh24:mi:ss') ");
		sqlBuilder.append(" )");	
		
		Query query=session.createSQLQuery(sqlBuilder.toString());
		query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		
		@SuppressWarnings("rawtypes")
		List indentList=query.list();
		List<HotelIndent> hotelIndentList=new ArrayList<HotelIndent>();
		for(Object o:indentList){
			HotelIndent hotelIndent=new HotelIndent();
			
			Map map = (Map)o;
			
			hotelIndent.setHotelIndentId(map.get("HOTEL_INDENT_ID") == null ? 0L : Long.valueOf(map.get("HOTEL_INDENT_ID").toString()));
			hotelIndent.setHotelName(map.get("HOTEL_NAME") == null ? "" : map.get("HOTEL_NAME").toString());
			Hotel h = new Hotel(map.get("HOTEL_ID") == null ? 0L : Long.valueOf(map.get("HOTEL_ID").toString()));
			h.setHotelName(hotelIndent.getHotelName());
			hotelIndent.setHotel(h);
			hotelIndent.setIndentCode(map.get("INDENT_CODE") == null ? "" : map.get("INDENT_CODE").toString());
			
			hotelIndent.setTotalAmount(map.get("TOTAL_AMOUNT") == null ? 0 : Integer.valueOf(map.get("TOTAL_AMOUNT").toString()));
			hotelIndent.setTotalPrice(map.get("TOTAL_PRICE") == null ? 0D : Double.valueOf(map.get("TOTAL_PRICE").toString()));
			
			hotelIndent.setTotalPrice(map.get("TOTAL_PRICE") == null ? 0D : Double.valueOf(map.get("TOTAL_PRICE").toString()));
			
			hotelIndent.setStartDate(map.get("START_DATE") == null ? null : CommonMethod.string2Time1(map.get("START_DATE").toString()));
			hotelIndent.setEndDate(map.get("END_DATE") == null ? null : CommonMethod.string2Time1(map.get("END_DATE").toString()));
			
			hotelIndent.setLinkman(map.get("LINKMAN") == null ? "" : map.get("LINKMAN").toString());
			hotelIndent.setIdCardNumber(map.get("ID_CARD_NUMBER") == null ? "" : map.get("ID_CARD_NUMBER").toString());
			hotelIndent.setPhoneNumber(map.get("PHONE_NUMBER") == null ? "" : map.get("PHONE_NUMBER").toString());
			hotelIndent.setCheckCode(map.get("CHECK_CODE") == null ? "" : map.get("CHECK_CODE").toString());
			
			hotelIndent.setTelNumber(map.get("TEL_NUMBER") == null ? "" : map.get("TEL_NUMBER").toString());
			hotelIndent.setFaxNumber(map.get("FAX_NUMBER") == null ? "" : map.get("FAX_NUMBER").toString());
					
			hotelIndent.setPayType(map.get("PAY_TYPE") == null ? "" : map.get("PAY_TYPE").toString());
			hotelIndent.setFrontMoney(map.get("FRONT_MONEY") == null ? 0D : Double.valueOf(map.get("FRONT_MONEY").toString()));
			hotelIndent.setPayTime(map.get("PAY_TIME") == null ? null : CommonMethod.string2Time1(map.get("PAY_TIME").toString()));
			
			hotelIndent.setCustomerType(map.get("CUSTOMER_TYPE") == null ? "" : map.get("CUSTOMER_TYPE").toString());
			hotelIndent.setCustomerName(map.get("CUSTOMER_NAME") == null ? "" : map.get("CUSTOMER_NAME").toString());
			hotelIndent.setPeopleNum(map.get("PEOPLE_NUM") == null ? 0 : Integer.valueOf(map.get("PEOPLE_NUM").toString()));
			
			hotelIndent.setBedMoney(map.get("BED_MONEY") == null ? 0D : Double.valueOf(map.get("BED_MONEY").toString()));
			hotelIndent.setMealScale(map.get("MEAL_SCALE") == null ? 0D : Double.valueOf(map.get("MEAL_SCALE").toString()));
			hotelIndent.setArriveTime(map.get("ARRIVE_TIME") == null ? null : CommonMethod.string2Time1(map.get("ARRIVE_TIME").toString()));
			hotelIndent.setCountry(map.get("COUNTRY") == null ? "" : map.get("COUNTRY").toString());
			hotelIndent.setVerifier(map.get("VERIFIER") == null ? null : Long.valueOf(map.get("VERIFIER").toString()));
					
			hotelIndent.setOrigin(map.get("ORIGIN") == null ? "" : map.get("ORIGIN").toString());
			hotelIndent.setSynchroState(map.get("SYNCHRO_STATE") == null ? "" : map.get("SYNCHRO_STATE").toString());
			hotelIndent.setTrxId(map.get("TRX_ID") == null ? "" : map.get("TRX_ID").toString());
			hotelIndent.setStatus(map.get("STATUS") == null ? "" : map.get("STATUS").toString());
			hotelIndent.setRemark(map.get("REMARK") == null ? "" : map.get("REMARK").toString());
			
			hotelIndent.setInputer(map.get("INPUTER")==null?null:Long.valueOf(map.get("INPUTER").toString()));
			hotelIndent.setInputTime(map.get("INPUT_TIME")==null?null:CommonMethod.string2Time1(map.get("INPUT_TIME").toString()));
			hotelIndent.setUpdater(map.get("UPDATER")==null?null:Long.valueOf(map.get("UPDATER").toString()));
			hotelIndent.setUpdateTime(map.get("UPDATE_TIME")==null?null:CommonMethod.string2Time1(map.get("UPDATE_TIME").toString()));
			
			hotelIndent.setVersion(map.get("VERSION") == null ? 0 : Long.valueOf(map.get("VERSION").toString()));
			hotelIndent.setReceivedMoney(map.get("RECEIVED_MONEY") == null ? 0D : Double.valueOf(map.get("RECEIVED_MONEY").toString()));
			hotelIndent.setIsCheck(map.get("IS_CHECK") == null ? "" : map.get("IS_CHECK").toString());
			hotelIndent.setPerFee(map.get("PER_FEE") == null ? 0D : Double.valueOf(map.get("PER_FEE").toString()));
			hotelIndent.setHotelCheckRec(new HotelCheckRec(map.get("CHECK_REC_ID") == null ? 0 : Long.valueOf(map.get("CHECK_REC_ID").toString())));
			hotelIndent.setHotelActivityId(map.get("HOTEL_ACTIVITY_ID") == null ? 0 : Long.valueOf(map.get("HOTEL_ACTIVITY_ID").toString()));
			
			hotelIndentList.add(hotelIndent);
		}
		
		//查询订单明细
		StringBuilder sqlBuilderSub=new StringBuilder();
		sqlBuilderSub.append(" select ");
		sqlBuilderSub.append(" hid.HOTEL_INDENT_DETAIL_ID,hid.HOTEL_INDENT_ID,hid.ROOM_TYPE_ID,hid.HOTEL_INDENT_DETAIL_CODE, ");
		sqlBuilderSub.append(" hid.BOOKING_DATE,hid.UNIT_PRICE,hid.AMOUNT,hid.SUBTOTAL,hid.REMARK,hid.INPUTER,hid.INPUT_TIME,hid.UPDATER, ");
		sqlBuilderSub.append(" hid.UPDATE_TIME,hid.VERSION,hid.AMOUNTRET,hid.CUT_PRICE,hrt.TYPE_NAME ");
		sqlBuilderSub.append(" from hotel_indent_detail hid,hotel_room_type hrt where hid.room_type_id = hrt.room_type_id ");
		sqlBuilderSub.append(" and hid.hotel_indent_id in (select t.HOTEL_INDENT_ID from HOTEL_INDENT t ");
		sqlBuilderSub.append(" where t.origin <> '00' and t.status in ('01','02','03','04') ");
		sqlBuilderSub.append(" and (t.linkman = '"+hi.getLinkman()+"' or t.id_card_number = '"+hi.getIdCardNumber()+"' or t.phone_number = '"+hi.getPhoneNumber()+"') ");
		sqlBuilderSub.append(" and t.hotel_indent_id in ( ");
		sqlBuilderSub.append(" select hid1.hotel_indent_id from hotel_indent_detail hid1 where hid1.booking_date >= TO_DATE('"+sDate+" 00:00:00','yyyy-mm-dd hh24:mi:ss') ");
		sqlBuilderSub.append(" and hid1.booking_date <= TO_DATE('"+eDate+" 00:00:00','yyyy-mm-dd hh24:mi:ss') ");
		sqlBuilderSub.append(" ))");	
		
		Query querySub=session.createSQLQuery(sqlBuilderSub.toString());
		querySub.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		
		@SuppressWarnings("rawtypes")
		List indentDelList=querySub.list();
		List<HotelIndentDetail> hotelIndentSubList=new ArrayList<HotelIndentDetail>();
		
		for(Object o:indentDelList){
			HotelIndentDetail hotelIndentDetail=new HotelIndentDetail();
			Map map = (Map)o;
			
			hotelIndentDetail.setHotelIndentDetailId(map.get("HOTEL_INDENT_DETAIL_ID") == null ? 0L : Long.valueOf(map.get("HOTEL_INDENT_DETAIL_ID").toString()));
			hotelIndentDetail.setHotelIndent(new HotelIndent(map.get("HOTEL_INDENT_ID") == null ? 0L : Long.valueOf(map.get("HOTEL_INDENT_ID").toString())));
			hotelIndentDetail.setRoomTypeId(map.get("ROOM_TYPE_ID") == null ? 0L : Long.valueOf(map.get("ROOM_TYPE_ID").toString()));
			hotelIndentDetail.setHotelIndentDetailCode(map.get("HOTEL_INDENT_DETAIL_CODE") == null ? "" : map.get("HOTEL_INDENT_DETAIL_CODE").toString());
			
			hotelIndentDetail.setBookingDate(map.get("BOOKING_DATE")==null?null:CommonMethod.string2Time1(map.get("BOOKING_DATE").toString()));
			hotelIndentDetail.setUnitPrice(map.get("UNIT_PRICE") == null ? 0D : Double.valueOf(map.get("UNIT_PRICE").toString()));
			hotelIndentDetail.setAmount(map.get("AMOUNT") == null ? 0 : Integer.valueOf(map.get("AMOUNT").toString()));
			hotelIndentDetail.setSubtotal(map.get("SUBTOTAL") == null ? 0D : Double.valueOf(map.get("SUBTOTAL").toString()));
			hotelIndentDetail.setRemark(map.get("REMARK") == null ? "" : map.get("REMARK").toString());
			hotelIndentDetail.setInputer(map.get("INPUTER")==null?null:Long.valueOf(map.get("INPUTER").toString()));
			hotelIndentDetail.setInputTime(map.get("INPUT_TIME")==null?null:CommonMethod.string2Time1(map.get("INPUT_TIME").toString()));
			hotelIndentDetail.setUpdater(map.get("UPDATER")==null?null:Long.valueOf(map.get("UPDATER").toString()));
			hotelIndentDetail.setUpdateTime(map.get("UPDATE_TIME")==null?null:CommonMethod.string2Time1(map.get("UPDATE_TIME").toString()));
			
			hotelIndentDetail.setVersion(map.get("VERSION") == null ? 0 : Long.valueOf(map.get("VERSION").toString()));
			hotelIndentDetail.setAmountret(map.get("AMOUNTRET") == null ? 0 : Integer.valueOf(map.get("AMOUNTRET").toString()));
			hotelIndentDetail.setCutPrice(map.get("CUT_PRICE") == null ? 0D : Double.valueOf(map.get("CUT_PRICE").toString()));
			HotelRoomType hrt = new HotelRoomType(hotelIndentDetail.getRoomTypeId());
			hrt.setTypeName(map.get("TYPE_NAME") == null ? "" : map.get("TYPE_NAME").toString());
			hotelIndentDetail.setHotelRoomType(hrt);
			
			hotelIndentSubList.add(hotelIndentDetail);
		}
		
		//组合返回数据
		List<Map<String,Object>> reList = new ArrayList<Map<String,Object>>();
		for(HotelIndent hi1 : hotelIndentList){
			
			Map<String,Object> temp = new HashMap<String,Object>();
			
			temp.put("indent", hi1);
			
			long hiId = hi1.getHotelIndentId();
			
			List<HotelIndentDetail> tempListSub=new ArrayList<HotelIndentDetail>();
			for(HotelIndentDetail hid : hotelIndentSubList){
				long hiIdSub = hid.getHotelIndent().getHotelIndentId();
				if(hiIdSub==hiId){
					tempListSub.add(hid);
				}
			}
			
			temp.put("detail", tempListSub);
			reList.add(temp);
		}
		return reList;
	}
	
}