package dao.hotel.hotelIndent;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import com.xlem.dao.HotelIndent;

import common.action.JsonPager;
import common.dao.BaseDao;

public interface HotelIndentDao extends BaseDao<HotelIndent> {
	/**
	 * 
	 * @param t 封装客户端的查询参数
	 * @param userId 当前登录用户的ID
	 * @param jp 分页对象
	 * @param forPage 查询页面，“returnPage”时之查询status为‘02’和‘03’的记录
	 * @return
	 */
	public List<HotelIndent> findHotelIndentList(HotelIndent t,long userId,JsonPager<HotelIndent> jp,String forPage);
	public int findIndentListCount(HotelIndent t,long userId,JsonPager<HotelIndent> jp,String forPage);
	
	/**
	 * 市场部查询订单总数
	 * @param t
	 * @param currentUserId
	 * @param jp
	 * @param forPage
	 * @return
	 */
	public int findIndentListCountForMKT(HotelIndent t, Long currentUserId,
			JsonPager<HotelIndent> jp, String forPage);
	
	/**
	 * 市场部查询订单
	 * @param t
	 * @param currentUserId
	 * @param jp
	 * @param forPage
	 * @return
	 */
	public List<HotelIndent> findHotelIndentListForMKT(HotelIndent t,
			Long currentUserId, JsonPager<HotelIndent> jp, String forPage);
	
	/**
	 * 统计金额
	 * @param jp
	 * @param hotelIndent
	 * @param node
	 * @return
	 */
	public Map<String, Double> findHotelIndentSums(JsonPager<HotelIndent> jp,
			HotelIndent hotelIndent, String node, Long userId);
	
	/**
	 * 统计当天房间数量
	 * @param jp
	 * @param hotelIndent
	 * @param node
	 * @return
	 */
	public int findHotelIndentTodayNums();
	
	
	/**
	 * 查询2014-07-15后的所有没有财务记录的订单
	 * @return
	 */
	public List<HotelIndent> findHotelIndentListForFinance();
	
	/**
	 * 查询市场部是否有重复订单
	 * @param hotelIndent
	 * @return
	 */
	public List<Object> findRepeating(HotelIndent hotelIndent);
	
}