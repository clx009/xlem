package dao.hotel.hotelPrice;
import com.xlem.dao.HotelPrice;

import common.dao.BaseDao;

public interface HotelPriceDao extends BaseDao<HotelPrice> {

}