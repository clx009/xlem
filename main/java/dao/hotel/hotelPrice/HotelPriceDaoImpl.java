package dao.hotel.hotelPrice;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.HotelPrice;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("hotelPriceDao")
public class HotelPriceDaoImpl extends BaseDaoImpl<HotelPrice> implements HotelPriceDao {

}