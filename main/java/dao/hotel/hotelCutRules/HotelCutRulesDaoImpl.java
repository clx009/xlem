package dao.hotel.hotelCutRules;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.HotelCutRules;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("hotelCutRulesDao")
public class HotelCutRulesDaoImpl extends BaseDaoImpl<HotelCutRules> implements HotelCutRulesDao {

}