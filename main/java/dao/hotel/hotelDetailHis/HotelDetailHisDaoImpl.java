package dao.hotel.hotelDetailHis;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.HotelDetailHis;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("hotelDetailHisDao")
public class HotelDetailHisDaoImpl extends BaseDaoImpl<HotelDetailHis> implements HotelDetailHisDao {

}