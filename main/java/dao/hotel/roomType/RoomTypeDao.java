package dao.hotel.roomType;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.TreeMap;

import com.xlem.dao.HotelRoomType;

import common.dao.BaseDao;

public interface RoomTypeDao extends BaseDao<HotelRoomType> {
	TreeMap<String,HashMap<String,String>> findRoomTypeAmountInfo(Timestamp startDate,Timestamp endDate);
}