package dao.hotel.roomType;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.HotelRoomType;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("roomTypeDao")
public class RoomTypeDaoImpl extends BaseDaoImpl<HotelRoomType> implements RoomTypeDao {

	/**
	 * TreeMap<String,HashMap<String,String>>
	 *           |        |
	 *           |        Key表示房型ID,Value表示房型ID对应的订房数量     
	 *           表示订房 时间，使用TreeMap按照升序排序，SQL查询中最后不需要按照订房时间排序
	 *                  
	 */
	@SuppressWarnings("unchecked")
	@Override
	public TreeMap<String,HashMap<String,String>> findRoomTypeAmountInfo(final Timestamp startDate,final Timestamp endDate) {
		Session session = sessionFactory.getCurrentSession();
		if(startDate==null && endDate==null){
			return null;
		}
		StringBuilder sqlBuilder=new StringBuilder();
		sqlBuilder.append(" select t1.use_date, ");
		sqlBuilder.append(" t1.room_type_id, ");
		sqlBuilder.append(" nvl(t.room_amount,0), ");
		sqlBuilder.append(" t1.amount, ");
		sqlBuilder.append(" nvl(t1.amount, 0) - nvl(t.room_amount, 0) as ramount ");
		sqlBuilder.append(" from (SELECT hIndentDetail.BOOKING_DATE, ");
		sqlBuilder.append(" hIndentDetail.ROOM_TYPE_ID, ");
		sqlBuilder.append(" sum(hIndentDetail.AMOUNT) ROOM_AMOUNT ");
		sqlBuilder.append(" FROM HOTEL_INDENT_DETAIL hIndentDetail,hotel_indent hi where hIndentDetail.hotel_indent_id = hi.hotel_indent_id and hi.status not in ('05','06') ");
		sqlBuilder.append(" GROUP BY hIndentDetail.BOOKING_DATE,hIndentDetail.ROOM_TYPE_ID) t ");
		sqlBuilder.append(" right join HOTEL_LIMIT_VIEW t1 ");
		sqlBuilder.append(" on t.room_type_id = t1.room_type_id ");
		sqlBuilder.append(" and t.booking_date = t1.use_date ");
		sqlBuilder.append(" WHERE 1 = 1 ");
		if(startDate!=null){
			sqlBuilder.append(" AND t1.use_date >= :startDate ");
		}
		if(endDate!=null){
			sqlBuilder.append(" AND t1.use_date <= :endDate ");
		}
		//使用TreeMap内部排序,SQL中不需要排序
		//sqlBuilder.append("ORDER BY hIndentDetail.BOOKING_DATE ASC ");
		
		Query query=session.createSQLQuery(sqlBuilder.toString());
		
		if(startDate!=null){
			query.setTimestamp("startDate",startDate);
		}
		if(endDate!=null){
			query.setTimestamp("endDate",endDate);
		}
		
		@SuppressWarnings("rawtypes")
		List dataList=query.list();
	
		TreeMap<String,HashMap<String,String>> roomData=new TreeMap<String,HashMap<String,String>>();
		for(Object o:dataList){
			Object[]tmp=(Object[])o;
			String bookingDate=null;
			String roomTypeId=null;
			String roomAmount=null;
			String ramount = null;
			if(tmp[0]!=null){
				bookingDate=tmp[0].toString();
			}
			if(tmp[1]!=null){
				roomTypeId=tmp[1].toString();
			}
			if(tmp[2]!=null){
				roomAmount=tmp[2].toString();
			}
			if(tmp[4]!=null){
				ramount=tmp[4].toString();
			}
			if(bookingDate!=null){
				HashMap<String,String> currBookingDateInfo=roomData.get(bookingDate);
				if(currBookingDateInfo==null){
					currBookingDateInfo=new HashMap<String,String>();
					currBookingDateInfo.put(roomTypeId, roomAmount);
					currBookingDateInfo.put(roomTypeId+"R", ramount);
					roomData.put(bookingDate, currBookingDateInfo);
				}else{
					currBookingDateInfo.put(roomTypeId, roomAmount);
					currBookingDateInfo.put(roomTypeId+"R", ramount);
				}
			}
		}
		return roomData;
	}

} 