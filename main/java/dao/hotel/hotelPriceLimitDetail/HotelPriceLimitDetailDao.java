package dao.hotel.hotelPriceLimitDetail;
import java.sql.Timestamp;

import com.xlem.dao.HotelPriceLimitDetail;
import com.xlem.dao.HotelRoomType;

import common.dao.BaseDao;
public interface HotelPriceLimitDetailDao extends BaseDao<HotelPriceLimitDetail> {
	
	/**
	 * 查询房型在该日预订情况
	 * @param t
	 * @param hotelRoomType
	 * @param act   "act":活动房
	 * @param hotelActivityId   活动ID
	 * @return
	 */
	int findBookingAmount(Timestamp t, HotelRoomType hotelRoomType,String act,Long hotelActivityId);


}