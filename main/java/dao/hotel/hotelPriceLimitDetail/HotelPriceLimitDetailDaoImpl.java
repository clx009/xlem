package dao.hotel.hotelPriceLimitDetail;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.HotelPriceLimitDetail;
import com.xlem.dao.HotelRoomType;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("hotelPriceLimitDetailDao")
public class HotelPriceLimitDetailDaoImpl extends BaseDaoImpl<HotelPriceLimitDetail> implements HotelPriceLimitDetailDao {

	@SuppressWarnings("unchecked")
	@Override
	public int findBookingAmount(final Timestamp t, final HotelRoomType hotelRoomType,final String act,final Long hotelActivityId) {
		Session session = sessionFactory.getCurrentSession();
		StringBuffer sql = new StringBuffer();
		sql.append(" select hid.booking_date, hid.room_type_id, sum(hid.amount)");
		sql.append(" from hotel_indent_detail hid,hotel_indent hi");
		sql.append(" where hid.booking_date = :bookingDate and hid.room_type_id = :roomTypeId ");
		sql.append(" and hid.hotel_indent_id = hi.hotel_indent_id");
		if("all".equals(act)){//查所有订单
		}else if("act".equals(act)){//活动房
			
			if(hotelActivityId!=null){
				sql.append(" and hi.hotel_activity_id = "+hotelActivityId);
			}else{
				sql.append(" and hi.hotel_activity_id is not null");
			}
		}else{
			sql.append(" and hi.hotel_activity_id is null");
		}
		sql.append(" group by hid.booking_date, hid.room_type_id");
		Query query = session.createSQLQuery(sql.toString());
		query.setTimestamp("bookingDate", t);
		query.setLong("roomTypeId", hotelRoomType.getRoomTypeId());
		List<Object[]> objects = query.list();
		if(objects!=null && !objects.isEmpty()){
			Object[] obj = objects.get(0);
			if(obj!=null){
				if(obj[2]!=null){
					return Integer.parseInt(obj[2].toString());
				}
				else{
					return 0;
				}
			}
			else{
				return 0;
			}
		}
		else{
			return 0;
		}
	}
}