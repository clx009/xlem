package dao.hotel.hotelActivityLimit;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.HotelActivityLimit;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("hotelActivityLimitDao")
public class HotelActivityLimitDaoImpl extends BaseDaoImpl<HotelActivityLimit> implements HotelActivityLimitDao {

}