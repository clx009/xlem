package dao.hotel.hotelActivityLimit;
import com.xlem.dao.HotelActivityLimit;

import common.dao.BaseDao;

public interface HotelActivityLimitDao extends BaseDao<HotelActivityLimit> {

}