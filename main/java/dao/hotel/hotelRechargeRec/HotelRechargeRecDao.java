package dao.hotel.hotelRechargeRec;
import com.xlem.dao.HotelRechargeRec;
import com.xlem.dao.TicketRechargeRec;

import common.action.JsonPager;
import common.dao.BaseDao;

public interface HotelRechargeRecDao extends BaseDao<HotelRechargeRec> {

	public JsonPager<TicketRechargeRec> findTicketRechargeRec(final JsonPager<TicketRechargeRec> jp, final HotelRechargeRec t);
	
}