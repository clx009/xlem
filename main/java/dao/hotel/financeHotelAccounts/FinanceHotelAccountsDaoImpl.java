package dao.hotel.financeHotelAccounts;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.FinanceHotelAccounts;

import common.CommonMethod;
import common.dao.BaseDaoImpl;

import dao.ticket.financeTicketAccounts.AllStatementObject;
import dao.ticket.financeTicketAccounts.StatementObject;

@Transactional
@Repository("financeHotelAccountsDao")
public class FinanceHotelAccountsDaoImpl extends BaseDaoImpl<FinanceHotelAccounts> implements FinanceHotelAccountsDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<AllStatementObject>  findAllStatement(final StatementObject stateObj) {
		Session session = sessionFactory.getCurrentSession();

		List<AllStatementObject> pagerRoot = new ArrayList<AllStatementObject>();

		//查询奖励款类型
		StringBuffer rewardCondition = new StringBuffer();
		StringBuffer rewardConditionSub = new StringBuffer();
		if(stateObj.getRewardTypeId()!=null){
			rewardCondition.append(" and t.hotel_recharge_rec_id in ( ");
			rewardCondition.append(" select brr.hotel_recharge_rec_id from HOTEL_RECHARGE_REC brr where brr.reward_account_id in ( ");
			rewardCondition.append(" select bra.reward_account_id from BASE_REWARD_ACCOUNT bra where bra.reward_type_id = "+stateObj.getRewardTypeId());
			rewardCondition.append(" )) ");
			
			rewardConditionSub.append(" and ftasub.hotel_recharge_rec_id in ( ");
			rewardConditionSub.append(" select brr.hotel_recharge_rec_id from HOTEL_RECHARGE_REC brr where brr.reward_account_id in ( ");
			rewardConditionSub.append(" select bra.reward_account_id from BASE_REWARD_ACCOUNT bra where bra.reward_type_id = "+stateObj.getRewardTypeId());
			rewardConditionSub.append(" )) ");
		}
		
		String accType = "";
		if("01".equals(stateObj.getPayType())){//市场部下单
			accType = "'03'";
			pagerRoot.add(new AllStatementObject(1, "已消费金额",true,true,true,false,false,true));
			pagerRoot.add(new AllStatementObject(4, "已支付未消费金额",true,true,false,true,false,true));
			//pagerRoot.add(new AllStatementObject(6, "充值金额",true,true,false,true,true));
			//pagerRoot.add(new AllStatementObject(10, "合计",true,true,true,true,false));
		}else if("02".equals(stateObj.getPayType())){//奖励款
			accType = "'01'";
			pagerRoot.add(new AllStatementObject(3, "余额支付已消费金额",true,true,true,false,false,true));
			pagerRoot.add(new AllStatementObject(5, "余额已支付未消费金额",true,true,false,true,false,true));
			pagerRoot.add(new AllStatementObject(6, "奖励款充值金额",true,true,false,true,true,true));
			pagerRoot.add(new AllStatementObject(8, "扣除房损",true,true,false,true,false,true));
			pagerRoot.add(new AllStatementObject(10, "合计",true,true,true,true,true,true));
		}else{//现金
			accType = "'02'";
			pagerRoot.add(new AllStatementObject(1, "网银支付已消费金额",true,true,true,false,false,true));
			//pagerRoot.add(new AllStatementObject(2, "易宝支付已消费金额",false,true,true,false,false));
			//pagerRoot.add(new AllStatementObject(3, "余额支付已消费金额",true,true,true,false,false));
			pagerRoot.add(new AllStatementObject(4, "网银已支付未消费金额",true,true,false,true,false,true));
			//pagerRoot.add(new AllStatementObject(5, "余额已支付未消费金额",true,true,false,true,false));
			//pagerRoot.add(new AllStatementObject(6, "现金充值金额",true,true,false,true,true));
			//pagerRoot.add(new AllStatementObject(7, "网银付款退款到余额",true,true,false,true,true));
			pagerRoot.add(new AllStatementObject(8, "扣除房损",true,true,false,true,false,true));
			pagerRoot.add(new AllStatementObject(9, "退款到卡上金额",true,true,false,true,false,true));
			pagerRoot.add(new AllStatementObject(10, "合计",true,true,true,true,false,true));
		}
		
		
		//查询条件（上期）
		StringBuffer timeConditionBefore = new StringBuffer();
		//添加传入的查询条件
		timeConditionBefore.append(" t.input_time < to_date('");
		timeConditionBefore.append(CommonMethod.timeToString(stateObj.getStartDate()));
		timeConditionBefore.append(" 02:00:00','YYYY-MM-DD HH24:MI:SS') ");
		
		timeConditionBefore.append(rewardCondition);
		
		//查询条件（本期）
		StringBuffer timeConditionThis = new StringBuffer();
		//添加传入的查询条件
		timeConditionThis.append(" t.input_time >= to_date('");
		timeConditionThis.append(CommonMethod.timeToString(stateObj.getStartDate()));
		timeConditionThis.append(" 02:00:00','YYYY-MM-DD HH24:MI:SS') and t.input_time < to_date('");
		
		timeConditionThis.append(CommonMethod.timeToString(CommonMethod.addDay(stateObj.getEndDate(),1)));
		timeConditionThis.append(" 02:00:00','YYYY-MM-DD HH24:MI:SS') ");
		
		timeConditionThis.append(rewardCondition);
		
		
		
		StringBuffer sql = new StringBuffer();
		List queryResult;
		sql.append(" select nvl(before.atb,this.att) aType,nvl(before.ptb,this.ptt) pType, ");
		sql.append(" nvl(before.moneyB,0) moneyB, ");
		sql.append(" nvl(this.moneyT,0) moneyT from ");
		sql.append("(select cast(t.accounts_type as varchar2(2)) atb,cast(t.payment_type as varchar2(2)) ptb,sum(t.money+nvl(back.money,0)) moneyB");
		sql.append(" from FINANCE_HOTEL_ACCOUNTS t");
		sql.append(" left join ");
		sql.append("(select tBack.Money,tBack.Back_Accounts_Id ");
		sql.append(" from FINANCE_HOTEL_ACCOUNTS tBack ");
		sql.append(" where tBack.Payment_Type in ('09','10') ");
		sql.append(" ) back ");
		sql.append(" on back.back_accounts_id = t.hotel_accounts_id ");
		sql.append(" where t.accounts_type in ("+accType+") and t.payment_type not in ('09','10') ");
		sql.append(" and ");
		sql.append(timeConditionBefore);
		sql.append(" group by t.accounts_type,t.payment_type ) before");
		
		sql.append(" full join ");
		
		sql.append("(select cast(t.accounts_type as varchar2(2)) att,cast(t.payment_type as varchar2(2)) ptt,sum(t.money+nvl(back.money,0)) moneyT");
		sql.append(" from FINANCE_HOTEL_ACCOUNTS t");
		sql.append(" left join ");
		sql.append("(select tBack.Money,tBack.Back_Accounts_Id ");
		sql.append(" from FINANCE_HOTEL_ACCOUNTS tBack ");
		sql.append(" where tBack.Payment_Type in ('09','10') ");
		sql.append(" and tBack.input_time  < to_date('"+CommonMethod.timeToString(CommonMethod.addDay(stateObj.getEndDate(),1))+" 02:00:00','YYYY-MM-DD HH24:MI:SS') ");
		sql.append(" ) back ");
		sql.append(" on back.back_accounts_id = t.hotel_accounts_id ");
		sql.append(" where t.accounts_type in ("+accType+") and t.payment_type not in ('09','10') ");
		sql.append(" and ");
		sql.append(timeConditionThis);
		sql.append(" group by t.accounts_type,t.payment_type) this ");
		sql.append(" on (before.atb || before.ptb)=(this.att || this.ptt) ");
		SQLQuery query = session.createSQLQuery(sql.toString());
		query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		queryResult = query.list();
		
		//查询本期打印回传的冲退，在本期已消费的交易记录中显示
		
		Map<String,Map> backMap = new HashMap<String,Map>();
		StringBuffer sqlBack = new StringBuffer();
		
		sqlBack.append(" select rBack.accounts_type att,rBack.payment_type ptt,sum(rBack.money-nvl(ret.money,0)) moneyT from ");
		sqlBack.append(" (select back.hotel_indent_id,cast(back.accounts_type as varchar2(2)) accounts_type,cast(t.payment_type as varchar2(2)) payment_type,sum(back.money) money ");
		sqlBack.append(" from FINANCE_HOTEL_ACCOUNTS t,FINANCE_HOTEL_ACCOUNTS back ");
		sqlBack.append(" where back.accounts_type in ("+accType+") and back.payment_type in ('10') ");
		sqlBack.append(" and back.back_accounts_id = t.hotel_accounts_id ");
		sqlBack.append(" and ");
		sqlBack.append(timeConditionThis);
		sqlBack.append(" group by back.hotel_indent_id,back.accounts_type,t.payment_type) rBack ");
		sqlBack.append(" left join ");
		sqlBack.append(" (select cast(t.accounts_type as varchar2(2)) accounts_type,t.hotel_indent_id,case when t.payment_type = '07' then '01' ");
		sqlBack.append(" when t.payment_type = '08' then '02' ");
		sqlBack.append(" end payment_type,sum(t.money) money ");
		sqlBack.append(" from FINANCE_HOTEL_ACCOUNTS t where t.payment_type in ('08','07')  ");
		sqlBack.append(" and ");
		sqlBack.append(timeConditionThis);
		sqlBack.append(" group by t.accounts_type,t.hotel_indent_id,t.payment_type) ret ");
		sqlBack.append(" on rBack.hotel_indent_id = ret.hotel_indent_id and rBack.accounts_type = ret.accounts_type and rBack.payment_type = ret.payment_type ");
		sqlBack.append(" group by rBack.accounts_type,rBack.payment_type ");
		
		
		
		SQLQuery queryBack = session.createSQLQuery(sqlBack.toString());
		queryBack.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		List queryBackResult = queryBack.list();
		for (Object o : queryBackResult) {
			
			Map map = (Map)o;
			
			String aType = "";//accountType
			if(map.get("ATT")!=null){
				aType = map.get("ATT").toString();
			}
			String type = "";//accountType
			if(map.get("PTT")!=null){
				type = map.get("PTT").toString();
			}
			
			backMap.put("back_"+aType+"_"+type, map);
		}
		
		//查询网银支付退款到余额数据，在网银支付未消费中减去
		
		Map<String,Map> bankBanlMap = new HashMap<String,Map>();
		StringBuffer sqlBankBanl = new StringBuffer();
		
		sqlBankBanl.append(" select nvl(before.atb, this.att) att, ");
		sqlBankBanl.append(" nvl(before.ptb, this.ptt) ptt, ");
		sqlBankBanl.append(" nvl(before.moneyB, 0) moneyB, ");
		sqlBankBanl.append(" nvl(this.moneyT, 0) moneyT ");
		sqlBankBanl.append(" from (select cast(t.accounts_type as varchar2(2)) atb,cast(t.payment_type as varchar2(2)) ptb, ");
		sqlBankBanl.append(" sum(t.money) moneyB ");
		sqlBankBanl.append(" from FINANCE_HOTEL_ACCOUNTS t ");
		sqlBankBanl.append(" where t.accounts_type in ("+accType+") and t.payment_type in ('07','08') and t.hotel_indent_id not in ( ");
		sqlBankBanl.append(" select ftasub.hotel_indent_id from FINANCE_HOTEL_ACCOUNTS ftasub where ftasub.payment_type  in ('04','05')) ");
		sqlBankBanl.append(" and ");
		sqlBankBanl.append(timeConditionBefore);
		sqlBankBanl.append(" group by t.accounts_type, t.payment_type) before ");
		sqlBankBanl.append(" full join (select cast(t.accounts_type as varchar2(2)) att,cast(t.payment_type as varchar2(2)) ptt, ");
		sqlBankBanl.append(" sum(t.money) moneyT ");
		sqlBankBanl.append(" from FINANCE_HOTEL_ACCOUNTS t ");
		sqlBankBanl.append(" where t.accounts_type in ("+accType+") and t.payment_type in ('07','08') ");
		//sqlBankBanl.append(" and t.hotel_indent_id not in (select ftasub.hotel_indent_id from FINANCE_HOTEL_ACCOUNTS ftasub where ftasub.payment_type  in ('04','05')) ");
		//排除支付和消费在同一天的
		
		//查询条件（本期）
		StringBuffer timeConditionThisDate = new StringBuffer();
		//添加传入的查询条件
		timeConditionThisDate.append(" ftasub.input_time >= to_date('");
		timeConditionThisDate.append(CommonMethod.timeToString(stateObj.getStartDate()));
		timeConditionThisDate.append(" 02:00:00','YYYY-MM-DD HH24:MI:SS') and ftasub.input_time < to_date('");
		
		timeConditionThisDate.append(CommonMethod.timeToString(CommonMethod.addDay(stateObj.getEndDate(),1)));
		timeConditionThisDate.append(" 02:00:00','YYYY-MM-DD HH24:MI:SS') ");
		timeConditionThisDate.append(rewardConditionSub);
		
		sqlBankBanl.append(" and t.hotel_indent_id not in (select xf.hotel_indent_id from ");
		sqlBankBanl.append(" (select ftasub.hotel_indent_id from FINANCE_HOTEL_ACCOUNTS ftasub where ftasub.payment_type  in ('04','05') and "+timeConditionThisDate+") xf ");
		sqlBankBanl.append(" inner join ");
		sqlBankBanl.append(" (select ftasub.hotel_indent_id from FINANCE_HOTEL_ACCOUNTS ftasub where ftasub.payment_type  in ('01','02') and "+timeConditionThisDate+") zf ");
		sqlBankBanl.append(" on xf.hotel_indent_id = zf.hotel_indent_id) ");
		
		sqlBankBanl.append(" and ");
		sqlBankBanl.append(timeConditionThis);
		sqlBankBanl.append(" group by t.accounts_type, t.payment_type) this ");
		sqlBankBanl.append(" on (before.atb || before.ptb) = (this.att || this.ptt) ");
		
		
		
		SQLQuery queryBankBanl = session.createSQLQuery(sqlBankBanl.toString());
		queryBankBanl.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		List queryBankBanlResult = queryBankBanl.list();
		for (Object o : queryBankBanlResult) {
			
			Map map = (Map)o;
			
			String aType = "";//accountType
			if(map.get("ATT")!=null){
				aType = map.get("ATT").toString();
			}
			String type = "";//accountType
			if(map.get("PTT")!=null){
				type = map.get("PTT").toString();
			}
			
			bankBanlMap.put("bankBanl_"+aType+"_"+type, map);
		}
		
		
		
		Map<String,AllStatementObject> tempMap = new HashMap<String,AllStatementObject>();
		
		for (Object o : queryResult) {
			
			Map map = (Map)o;
			
			String aType = "";//accountType
			if(map.get("ATYPE")!=null){
				aType = map.get("ATYPE").toString();
			}
			
			String type = "";//accountType
			if(map.get("PTYPE")!=null){
				type = map.get("PTYPE").toString();
			}
			
			
			AllStatementObject aso = new AllStatementObject();
			
			//设置顺序和行名
			int colNums=0;
			if("04".equals(type)){
				colNums=1;
			}else if("05".equals(type)){
				colNums=3;
			}else if("01".equals(type)){
				colNums=4;
			}else if("02".equals(type)){
				colNums=5;
			}else if("03".equals(type)){
				colNums=6;
			}else if("07".equals(type) || "-1".equals(type)){
				colNums=7;
			}else if("08".equals(type)){
				colNums=8;
			}else if("06".equals(type)){
				colNums=9;
			}
			
			
			aso.setColNums(colNums);
			
			double previous= (map.get("MONEYB") == null ? new BigDecimal(0) : (BigDecimal)map.get("MONEYB")).doubleValue();
			double money= (map.get("MONEYT") == null ? new BigDecimal(0) : (BigDecimal)map.get("MONEYT")).doubleValue();
			
//					if(!"06".equals(type) && !"07".equals(type)){//除退款到卡上其他都显示正数
//						previous = Math.abs(previous);
//						money = Math.abs(money);
//					}
			
			if("01".equals(type)){//网银支付未消费要减去网银退到余额的金额
				Map baTempMap = bankBanlMap.get("bankBanl_"+aType+"_07");
				if(baTempMap!=null){
					double previousB= (baTempMap.get("MONEYB") == null ? new BigDecimal(0) : (BigDecimal)baTempMap.get("MONEYB")).doubleValue();
					double moneyB= (baTempMap.get("MONEYT") == null ? new BigDecimal(0) : (BigDecimal)baTempMap.get("MONEYT")).doubleValue();
				
					//网银退到余额记录为负数所以用加
					previous += previousB;
					money += moneyB;
					
				}
			}
			
			if("02".equals(type)){//余额支付未消费要减去余额退到余额的金额
				Map baTempMap = bankBanlMap.get("bankBanl_"+aType+"_08");
				if(baTempMap!=null){
					
					double previousB= (baTempMap.get("MONEYB") == null ? new BigDecimal(0) : (BigDecimal)baTempMap.get("MONEYB")).doubleValue();
					double moneyB= (baTempMap.get("MONEYT") == null ? new BigDecimal(0) : (BigDecimal)baTempMap.get("MONEYT")).doubleValue();
				
					//网银退到余额记录为负数所以用加
					previous += previousB;
					money += moneyB;
				}
			}
			
			
			aso.setPrevious(previous);
			
			if("04".equals(type) || "05".equals(type)){//本期消费放到消费栏中
				aso.setExpense(money);
				
				//本期交易金额为打印回传时的冲退金额
				
				String keyType = "";
				if("04".equals(type)){
					keyType = "01";
				}
				if("05".equals(type)){
					keyType = "02";
				}
				//TODO..
				Map baTempMap = backMap.get("back_"+aType+"_"+keyType);
				if(baTempMap!=null){
					double moneyB= Math.abs((baTempMap.get("MONEYT") == null ? new BigDecimal(0) : (BigDecimal)baTempMap.get("MONEYT")).doubleValue());
					aso.setTrade(moneyB);
				}
				
			}else{//其他放到交易栏
				aso.setTrade(money);
			}
			
			
			AllStatementObject asoTemp = tempMap.get("colNums_"+colNums);
			if(asoTemp==null){
				tempMap.put("colNums_"+colNums, aso);
			}else{
				
				
				aso.setPrevious((aso.getPrevious() == null ? 0d : aso.getPrevious())+(asoTemp.getPrevious() == null ? 0d : asoTemp.getPrevious()));
				
				aso.setExpense((aso.getExpense() == null ? 0d : aso.getExpense())+(asoTemp.getExpense() == null ? 0d : asoTemp.getExpense()));
				
				aso.setTrade((aso.getTrade() == null ? 0d : aso.getTrade())+(asoTemp.getTrade() == null ? 0d : asoTemp.getTrade()));
				
				tempMap.put("colNums_"+colNums, aso);
			}
			//pagerRoot.add(aso);
		}
		
		//System.out.println(tempMap);
		for(AllStatementObject aso : pagerRoot){
			
			AllStatementObject tempAso = tempMap.get("colNums_"+aso.getColNums());
			if(tempAso!=null){
				aso.putValues(tempAso);
			}
			
		}
		
		
		
		if("01".equals(stateObj.getPayType())){//市场部
			//3, "已消费金额"
			AllStatementObject aso3 = pagerRoot.get(0);
			//5, "已支付未消费金额"
			AllStatementObject aso5 = pagerRoot.get(1);
			//6, "现金充值金额"
			//AllStatementObject aso6 = pagerRoot.get(2);
			//10, "合计"
			//AllStatementObject aso10 = pagerRoot.get(2);
					
			//计算合计
//					for(AllStatementObject aso : pagerRoot){
//						int cNums = aso.getColNums();
//						if(cNums!=10){//排除合计行
//							
//							if(aso.isPreviousShow()){
//								aso10.setPrevious((aso10.getPrevious()==null ? 0 : aso10.getPrevious())+(aso.getPrevious()==null ? 0 : aso.getPrevious()));
//							}
//							
//							if(aso.isTradeShow()){
//								aso10.setTrade((aso10.getTrade()==null ? 0 : aso10.getTrade())+(aso.getTrade()==null ? 0 : aso.getTrade()));
//							}
//							
//							//本期消费
//							if(aso.isExpenseShow()){
//								aso10.setExpense((aso10.getExpense()==null ? 0 : aso10.getExpense())+(aso.getExpense()==null ? 0 : aso.getExpense()));
//							}
//						}
//					}
			
			
		}else if("02".equals(stateObj.getPayType())){//奖励款
			
			//3, "余额支付已消费金额"
			AllStatementObject aso3 = pagerRoot.get(0);
			//5, "余额已支付未消费金额"
			AllStatementObject aso5 = pagerRoot.get(1);
			//6, "现金充值金额"
			AllStatementObject aso6 = pagerRoot.get(2);
			//8, "扣除房损"
			AllStatementObject aso8 = pagerRoot.get(3);
			//10, "合计"
			AllStatementObject aso10 = pagerRoot.get(4);
					
			//07余额付款退款到余额记录
			AllStatementObject asoT = tempMap.get("colNums_7");
			if(asoT==null){
				asoT = new AllStatementObject();
			}
			
			
			//网银已支付未消费金额减去房损
			//上期余额
			double previous5 = (aso5.getPrevious()==null ? 0 : aso5.getPrevious()) - (aso8.getPrevious()==null ? 0 : aso8.getPrevious()) + (asoT.getPrevious()==null ? 0 : asoT.getPrevious());
			aso5.setPrevious(previous5);
			
			//本期交易
			double trade5 = (aso5.getTrade()==null ? 0 : aso5.getTrade()) - (aso8.getTrade()==null ? 0 : aso8.getTrade()) + (asoT.getTrade()==null ? 0 : asoT.getTrade());
			aso5.setTrade(trade5);
			
			//本期消费
			double expense5 = (aso5.getExpense()==null ? 0 : aso5.getExpense()) - (aso8.getExpense()==null ? 0 : aso8.getExpense()) + (asoT.getExpense()==null ? 0 : asoT.getExpense());
			aso5.setExpense(expense5);
			
			//退款到卡上金额减去房损
			//上期余额
//					double previous6 = (aso6.getPrevious()==null ? 0 : aso6.getPrevious()) + (aso8.getPrevious()==null ? 0 : aso8.getPrevious());
//					aso6.setPrevious(previous6);
//					
//					//本期交易
//					double trade6 = (aso6.getTrade()==null ? 0 : aso6.getTrade()) + (aso8.getTrade()==null ? 0 : aso8.getTrade());
//					aso6.setTrade(trade6);
//					
//					//本期消费
//					double expense6 = (aso6.getExpense()==null ? 0 : aso6.getExpense()) + (aso8.getExpense()==null ? 0 : aso8.getExpense());
//					aso6.setExpense(expense6);
			
			
			//计算合计行
			//上期余额
			double previous10 = (aso6.getPrevious()==null ? 0 : aso6.getPrevious()) - (aso3.getPrevious()==null ? 0 : aso3.getPrevious());
			aso10.setPrevious(previous10);
			
			//本期交易
			double trade10 = (aso6.getTrade()==null ? 0 : aso6.getTrade());
			aso10.setTrade(trade10);
			
			//本期消费
			double expense10 = (aso3.getExpense()==null ? 0 : aso3.getExpense());
			aso10.setExpense(expense10);
			
			//本期余额
			double current10 = (aso6.getPrevious()==null ? 0 : aso6.getPrevious()) - (aso3.getPrevious()==null ? 0 : aso3.getPrevious()) + (aso6.getTrade()==null ? 0 : aso6.getTrade()) - (aso3.getExpense()==null ? 0 : aso3.getExpense());
			aso10.setCurrent(current10);
			
			
		}else{//现金
			//1, "网银支付已消费金额"
			AllStatementObject aso1 = pagerRoot.get(0);
			//2, "易宝支付已消费金额"
			//AllStatementObject aso2 = pagerRoot.get(1);
			//3, "余额支付已消费金额"
			//AllStatementObject aso3 = pagerRoot.get(2);
			//4, "网银已支付未消费金额"
			AllStatementObject aso4 = pagerRoot.get(1);
			//5, "余额已支付未消费金额"
			//AllStatementObject aso5 = pagerRoot.get(4);
			//6, "现金充值金额"
			//AllStatementObject aso6 = pagerRoot.get(5);
			//7, "网银付款退款到余额"
			//AllStatementObject aso7 = pagerRoot.get(5);
			//8, "扣除房损"
			AllStatementObject aso8 = pagerRoot.get(2);
			//9, "退款到卡上金额"
			AllStatementObject aso9 = pagerRoot.get(3);
			//10, "合计"
			AllStatementObject aso10 = pagerRoot.get(4);
					
			
			//网银已支付未消费金额减去房损
			//上期余额
			double previous4 = (aso4.getPrevious()==null ? 0 : aso4.getPrevious()) - (aso8.getPrevious()==null ? 0 : aso8.getPrevious());
			aso4.setPrevious(previous4);
			
			//本期交易
			double trade4 = (aso4.getTrade()==null ? 0 : aso4.getTrade()) - (aso8.getTrade()==null ? 0 : aso8.getTrade());
			aso4.setTrade(trade4);
			
			//本期消费
			double expense4 = (aso4.getExpense()==null ? 0 : aso4.getExpense()) - (aso8.getExpense()==null ? 0 : aso8.getExpense());
			aso4.setExpense(expense4);
			
			//退款到卡上金额减去房损
			//上期余额
			double previous9 = (aso9.getPrevious()==null ? 0 : aso9.getPrevious()) + (aso8.getPrevious()==null ? 0 : aso8.getPrevious());
			aso9.setPrevious(previous9);
			
			//本期交易
			double trade9 = (aso9.getTrade()==null ? 0 : aso9.getTrade()) + (aso8.getTrade()==null ? 0 : aso8.getTrade());
			aso9.setTrade(trade9);
			
			//本期消费
			double expense9 = (aso9.getExpense()==null ? 0 : aso9.getExpense()) + (aso8.getExpense()==null ? 0 : aso8.getExpense());
			aso9.setExpense(expense9);
			
			
			//计算合计
			//上期余额
			double previous10 = (aso1.getExpense()==null ? 0 : aso1.getExpense()) - (aso1.getTrade()==null ? 0 : aso1.getTrade()) + (aso4.getPrevious()==null ? 0 : aso4.getPrevious()) + (aso9.getPrevious()==null ? 0 : aso9.getPrevious()) + (aso8.getPrevious()==null ? 0 : aso8.getPrevious());
			aso10.setPrevious(previous10);
			
			
			//本期交易
			double trade10 = (aso1.getTrade()==null ? 0 : aso1.getTrade()) + (aso4.getTrade()==null ? 0 : aso4.getTrade()) +(aso9.getTrade()==null ? 0 : aso9.getTrade()) + (aso8.getTrade()==null ? 0 : aso8.getTrade());
			aso10.setTrade(trade10);
			
			
			
			for(AllStatementObject aso : pagerRoot){
				int cNums = aso.getColNums();
				if(cNums!=10){//排除合计行
					
					//本期消费
					if(aso.isExpenseShow()){
						aso10.setExpense((aso10.getExpense()==null ? 0 : aso10.getExpense())+(aso.getExpense()==null ? 0 : aso.getExpense()));
					}
				}
			}
			
		}
		
		return pagerRoot;
	}
	
}