package dao.hotel.financeHotelAccounts;
import java.util.List;

import com.xlem.dao.FinanceHotelAccounts;

import common.dao.BaseDao;
import dao.ticket.financeTicketAccounts.AllStatementObject;
import dao.ticket.financeTicketAccounts.StatementObject;
public interface FinanceHotelAccountsDao extends BaseDao<FinanceHotelAccounts> {

	/**
	 * 报表总计表 肖宇翔
	 * @param statementObject
	 * @return
	 */
	List<AllStatementObject>  findAllStatement(final StatementObject stateObj);
	
}