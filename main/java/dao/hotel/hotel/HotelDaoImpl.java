package dao.hotel.hotel;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.Hotel;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("hotelDao")
public class HotelDaoImpl extends BaseDaoImpl<Hotel> implements HotelDao {

} 