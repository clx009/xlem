package dao.hotel.hotel;
import com.xlem.dao.Hotel;

import common.dao.BaseDao;

public interface HotelDao extends BaseDao<Hotel> {

}