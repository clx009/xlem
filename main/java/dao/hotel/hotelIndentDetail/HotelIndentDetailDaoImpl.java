package dao.hotel.hotelIndentDetail;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.HotelActivityLimit;
import com.xlem.dao.HotelIndentDetail;
import com.xlem.dao.HotelPriceLimitDetail;
import com.xlem.dao.HotelRoomType;

import common.CommonMethod;
import common.action.JsonPager;
import common.dao.BaseDaoImpl;

@Transactional
@Repository("hotelIndentDetailDao")
public class HotelIndentDetailDaoImpl extends BaseDaoImpl<HotelIndentDetail>
		implements HotelIndentDetailDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<HotelPriceLimitDetail> findUsedRoomEveryday(final HotelPriceLimitDetail t) {
		Session session = sessionFactory.getCurrentSession();
						
		String sql = "select sum(t.amount),t.bookingDate from HotelIndentDetail t " +
				" where t.hotelIndent.status not in ('05','06') " +
				" and t.bookingDate >= :startDate and t.bookingDate <= :endDate " +
				" and t.hotelRoomType.roomTypeId = :roomTypeId and t.hotelIndent.hotelActivityId is null ";
		if(t.getHotelRoomType().getHotelIndentId()!=null && !"".equals(t.getHotelRoomType().getHotelIndentId())){
			sql += " and t.hotelIndent.hotelIndentId <> :hotelIndentId ";
		}
		sql += " group by bookingDate ";
		Query query = session.createQuery(sql);
		query.setTimestamp("startDate", t.getStartDate());
		query.setTimestamp("endDate", t.getEndDate());
		query.setLong("roomTypeId", t.getHotelRoomType().getRoomTypeId());
		if(t.getHotelRoomType().getHotelIndentId()!=null && !"".equals(t.getHotelRoomType().getHotelIndentId())){
			query.setLong("hotelIndentId", t.getHotelRoomType().getHotelIndentId());
		}
		List<Object[]> objects = query.list();
		List<HotelPriceLimitDetail> details = new ArrayList<HotelPriceLimitDetail>();
		for (int i = 0; i < objects.size(); i++) {
			Object[] objects2 = objects.get(i);
			HotelPriceLimitDetail hotelPriceLimitDetail = new HotelPriceLimitDetail();
			hotelPriceLimitDetail.setBookingAmount(objects2[0]==null?0:(Integer.parseInt(objects2[0].toString())));
			hotelPriceLimitDetail.setUseDate(CommonMethod.string2Time1(objects2[1].toString()));
			details.add(hotelPriceLimitDetail);
		}
		return details;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<HotelActivityLimit> findUsedRoomEveryday(final HotelActivityLimit t) {
		Session session = sessionFactory.getCurrentSession();
		String sql = "select sum(t.amount),t.bookingDate from HotelIndentDetail t where t.bookingDate >= :startDate and t.bookingDate <= :endDate and t.hotelRoomType.roomTypeId = :roomTypeId and t.hotelIndent.hotelActivityId = :hotelActivityId";
		if(t.getHotelRoomType().getHotelIndentId()!=null && !"".equals(t.getHotelRoomType().getHotelIndentId())){
			sql += " and t.hotelIndent.hotelIndentId <> :hotelIndentId ";
		}
		sql += " group by bookingDate ";
		Query query = session.createQuery(sql);
		query.setTimestamp("startDate", t.getStartDate());
		query.setTimestamp("endDate", t.getEndDate());
		query.setLong("roomTypeId", t.getHotelRoomType().getRoomTypeId());
		query.setLong("hotelActivityId", t.getHotelActivity().getHotelActivityId());
		if(t.getHotelRoomType().getHotelIndentId()!=null && !"".equals(t.getHotelRoomType().getHotelIndentId())){
			query.setLong("hotelIndentId", t.getHotelRoomType().getHotelIndentId());
		}
		List<Object[]> objects = query.list();
		List<HotelActivityLimit> details = new ArrayList<HotelActivityLimit>();
		for (int i = 0; i < objects.size(); i++) {
			Object[] objects2 = objects.get(i);
			HotelActivityLimit hotelPriceLimitDetail = new HotelActivityLimit();
			hotelPriceLimitDetail.setBookingAmount(objects2[0]==null?0:(Integer.parseInt(objects2[0].toString())));
			hotelPriceLimitDetail.setUseDate(CommonMethod.string2Time1(objects2[1].toString()));
			details.add(hotelPriceLimitDetail);
		}
		return details;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void findJsonPageForQueryRoom(JsonPager<HotelRoomType> jp,
			final HotelRoomType t) {
		List<HotelRoomType> list = new ArrayList<HotelRoomType>();
		Session session = sessionFactory.getCurrentSession();
		String sql = "select a.room_Type_Id,a.type_Name,b.ta,b.tb,b.up from Hotel_Room_Type a left join (" +
				"select t.amount ta,sum(t.subtotal) tb,sum(t.subtotal)/t.amount/count(t.amount) up,t.hotel_Indent_Id tc,t.room_Type_Id td from Hotel_Indent_Detail t where t.hotel_Indent_Id = :hotelIndentId group by t.hotel_Indent_Id,t.room_Type_Id,t.amount) b " +
				"on a.room_Type_Id = b.td where a.hotel_id = :hotelId and a.status = '11'";
		Query query = session.createSQLQuery(sql);
		query.setLong("hotelIndentId", t.getHotelIndentId());
		query.setLong("hotelId", t.getHotelId());
		List<Object[]> objects = query.list();
		List<HotelRoomType> details = new ArrayList<HotelRoomType>();
		for (int i = 0; i < objects.size(); i++) {
			Object[] objects2 = objects.get(i);
			HotelRoomType hotelRoomType = new HotelRoomType();
			hotelRoomType.setRoomTypeId(objects2[0]==null?0l:(Long.parseLong(objects2[0].toString())));
			hotelRoomType.setTypeName(objects2[1]==null?"":objects2[1].toString());
			hotelRoomType.setAmount(objects2[2]==null?0:(Integer.parseInt(objects2[2].toString())));
			hotelRoomType.setSubtotal(objects2[3]==null?0d:(Double.parseDouble(objects2[3].toString())));
			hotelRoomType.setUnitPrice(objects2[4]==null?0d:(Double.parseDouble(objects2[4].toString())));
			details.add(hotelRoomType);
		}

		list = details;
		jp.setRoot(list);
	}

}