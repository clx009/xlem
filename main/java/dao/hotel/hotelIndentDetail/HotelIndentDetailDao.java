package dao.hotel.hotelIndentDetail;
import java.util.List;

import com.xlem.dao.HotelActivityLimit;
import com.xlem.dao.HotelIndentDetail;
import com.xlem.dao.HotelPriceLimitDetail;
import com.xlem.dao.HotelRoomType;

import common.action.JsonPager;
import common.dao.BaseDao;

public interface HotelIndentDetailDao extends BaseDao<HotelIndentDetail> {
	
	/**
	 * 查询每天房型预订房间数(不含活动房)
	 * @param t
	 * @return 
	 */
	List<HotelPriceLimitDetail> findUsedRoomEveryday(HotelPriceLimitDetail t);
	
	/**
	 * 查询每天房型预订房间数(仅活动房)
	 * @param t
	 * @return 
	 */
	List<HotelActivityLimit> findUsedRoomEveryday(HotelActivityLimit t);
	
	/**
	 * 市场部获取订房明细信息
	 * @param jp
	 * @param hotelIndentDetail
	 */
	void findJsonPageForQueryRoom(JsonPager<HotelRoomType> jp,
			HotelRoomType hotelRoomType);

}