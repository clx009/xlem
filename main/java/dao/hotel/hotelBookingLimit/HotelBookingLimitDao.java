package dao.hotel.hotelBookingLimit;
import com.xlem.dao.HotelBookingLimit;

import common.dao.BaseDao;

public interface HotelBookingLimitDao extends BaseDao<HotelBookingLimit> {

}