package dao.hotel.hotelBookingLimit;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.HotelBookingLimit;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("hotelBookingLimitDao")
public class HotelBookingLimitDaoImpl extends BaseDaoImpl<HotelBookingLimit> implements HotelBookingLimitDao {

}