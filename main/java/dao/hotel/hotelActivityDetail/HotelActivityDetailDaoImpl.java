package dao.hotel.hotelActivityDetail;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.HotelActivityDetail;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("hotelActivityDetailDao")
public class HotelActivityDetailDaoImpl extends BaseDaoImpl<HotelActivityDetail> implements HotelActivityDetailDao {

}