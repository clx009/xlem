package dao.hotel.hotelActivityDetail;
import com.xlem.dao.HotelActivityDetail;

import common.dao.BaseDao;

public interface HotelActivityDetailDao extends BaseDao<HotelActivityDetail> {

}