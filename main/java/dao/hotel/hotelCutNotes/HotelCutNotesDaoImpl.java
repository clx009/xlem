package dao.hotel.hotelCutNotes;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.HotelCutNotes;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("hotelCutNotesDao")
public class HotelCutNotesDaoImpl extends BaseDaoImpl<HotelCutNotes> implements HotelCutNotesDao {

}