package dao.ticket.ticketUserType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.TicketUserType;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("ticketUserTypeDao")
public class TicketUserTypeDaoImpl extends BaseDaoImpl<TicketUserType> implements TicketUserTypeDao {

}