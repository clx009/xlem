package dao.ticket.ticketUserType;
import com.xlem.dao.TicketUserType;

import common.dao.BaseDao;

public interface TicketUserTypeDao extends BaseDao<TicketUserType> {

}