package dao.ticket.ticketCheckRec;
import java.util.List;

import com.xlem.dao.TicketAccounts;
import com.xlem.dao.TicketCheckRec;

import common.action.JsonPager;
import common.dao.BaseDao;

public interface TicketCheckRecDao extends BaseDao<TicketCheckRec> {
	
	/**
	 * 查询结帐明细
	 * @param checkRecId
	 * @return
	 */
	JsonPager<TicketCheckRec> findGridDetail(Long checkRecId);
	
	/**
	 * 查询结帐明细 (分账)
	 * @param checkRecId
	 * @return
	 */
	List<TicketAccounts> findGridDetailSub(Long checkRecId);
	
}