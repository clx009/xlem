package dao.ticket.ticketCheckRec;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.TicketAccounts;
import com.xlem.dao.TicketCheckRec;

import common.action.JsonPager;
import common.dao.BaseDaoImpl;

@Transactional
@Repository("ticketCheckRecDao")
public class TicketCheckRecDaoImpl extends BaseDaoImpl<TicketCheckRec> implements TicketCheckRecDao {

	@SuppressWarnings("unchecked")
	@Override
	public JsonPager<TicketCheckRec> findGridDetail(final Long checkRecId) {
		Session session = sessionFactory.getCurrentSession();
		JsonPager<TicketCheckRec> jpTcr = new JsonPager<TicketCheckRec>();
		String sqlString = 	" select cast(discount as varchar2(2)) discount,money,pay_fee,"+
							" check_rec_id,cast(pay_type as varchar2(2)) pay_type"+
							" from (select nvl(ti.discount, '10') discount,"+
							" sum(tid.unit_price) money,"+
							" sum(tid.unit_price * tid.per_fee) pay_fee,"+
							" tcr.check_rec_id,ti.pay_type"+
							" from ticket_barcode_rec tbr,ticket_check_rec tcr,ticket_indent_detail tid,ticket_indent ti";
		sqlString += 		" where tcr.check_rec_id = :checkRecId"+
							" and tbr.check_rec_id = tcr.check_rec_id"+
							" and tbr.indent_detail_id = tid.indent_detail_id"+
							" and tid.indent_id = ti.indent_id";
		sqlString += 		" group by nvl(ti.discount,'10'), tcr.check_rec_id,ti.pay_type" +
							" order by ti.pay_type desc,nvl(ti.discount,'10') desc)";
		Query query = session.createSQLQuery(sqlString);
		query.setLong("checkRecId", checkRecId);
		List<Object[]> list = query.list();
		if(list!=null && !list.isEmpty()){
			ArrayList<TicketCheckRec> checkRecs = new ArrayList<TicketCheckRec>();
			for (Object[] objects : list) {
				TicketCheckRec ticketCheckRec = new TicketCheckRec();
				ticketCheckRec.setDiscount(objects[0]==null?"10":objects[0].toString());
				ticketCheckRec.setMoney(objects[1]==null?null:Double.parseDouble(objects[1].toString()));
				ticketCheckRec.setPayFee(objects[2]==null?null:Double.parseDouble(objects[2].toString()));
				ticketCheckRec.setCheckRecId(objects[3]==null?null:Long.parseLong(objects[3].toString()));
				ticketCheckRec.setPayType(objects[4]==null?"02":objects[4].toString());
				checkRecs.add(ticketCheckRec);
			}
			jpTcr.setRoot(checkRecs);
		}
		return jpTcr;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TicketAccounts> findGridDetailSub(final Long checkRecId) {
		Session session = sessionFactory.getCurrentSession();
		StringBuffer sql = new StringBuffer("select nvl(w.ticket_name,x.ticket_name) as ticket_name,");
		sql.append(" nvl(w.amountGroup,0) as amountGroup,nvl(w.subtotalGroup,0) as subtotalGroup,nvl(w.subtotalGroupE,0) as subtotalGroupE,nvl(w.subtotalGroupR,0) as subtotalGroupR,nvl(w.payFeeGroup,0) as payFeeGroup,nvl(w.payFeeGroupE,0) as payFeeGroupE,nvl(w.payFeeGroupR,0) as payFeeGroupR,");
		sql.append(" nvl(w.amountGroupD,0) as amountGroupD,nvl(w.subtotalGroupD,0) as subtotalGroupD,nvl(w.subtotalGroupDE,0) as subtotalGroupDE,nvl(w.subtotalGroupDR,0) as subtotalGroupDR,nvl(w.payFeeGroupD,0) as payFeeGroupD,nvl(w.payFeeGroupDE,0) as payFeeGroupDE,nvl(w.payFeeGroupDR,0) as payFeeGroupDR,");
		sql.append(" nvl(w.amount,0) as amount,nvl(w.subtotal,0) as subtotal,nvl(w.subtotalE,0) as subtotalE,nvl(w.subtotalR,0) as subtotalR,nvl(w.payFee,0) as payFee,nvl(w.payFeeE,0) as payFeeE,nvl(w.payFeeR,0) as payFeeR,");
		sql.append(" nvl(x.amount,0) as amountLocale,nvl(x.subtotal,0) as subtotalLocale,nvl(x.subtotalE,0) as subtotalLocaleE,nvl(x.subtotalR,0) as subtotalLocaleR,nvl(x.payFee,0) as payFeeLocale,nvl(x.payFeeE,0) as payFeeLocaleE,nvl(x.payFeeR,0) as payFeeLocaleR,");
		sql.append(" nvl(w.eCompany,x.eCompany) as eCompany,nvl(w.rCompany,x.rCompany) as rCompany,");
		sql.append(" nvl(w.discount,x.discount) as discount,nvl(w.status,x.status) as status from ");
		
		sql.append(" (select nvl(gr.ticket_name,s.ticket_name) as ticket_name,");
		sql.append(" nvl(gr.amountGroup,0) as amountGroup,nvl(gr.subtotalGroup,0) as subtotalGroup,nvl(gr.subtotalGroupE,0) as subtotalGroupE,nvl(gr.subtotalGroupR,0) as subtotalGroupR,nvl(gr.payFeeGroup,0) as payFeeGroup,nvl(gr.payFeeGroupE,0) as payFeeGroupE,nvl(gr.payFeeGroupR,0) as payFeeGroupR,");
		sql.append(" nvl(gr.amountGroupD,0) as amountGroupD,nvl(gr.subtotalGroupD,0) as subtotalGroupD,nvl(gr.subtotalGroupDE,0) as subtotalGroupDE,nvl(gr.subtotalGroupDR,0) as subtotalGroupDR,nvl(gr.payFeeGroupD,0) as payFeeGroupD,nvl(gr.payFeeGroupDE,0) as payFeeGroupDE,nvl(gr.payFeeGroupDR,0) as payFeeGroupDR,");
		sql.append(" nvl(s.amount,0) as amount,nvl(s.subtotal,0) as subtotal,nvl(s.subtotalE,0) as subtotalE,nvl(s.subtotalR,0) as subtotalR,nvl(s.payFee,0) as payFee,nvl(s.payFeeE,0) as payFeeE,nvl(s.payFeeR,0) as payFeeR,");
		sql.append(" nvl(gr.eCompany,s.eCompany) as eCompany,nvl(gr.rCompany,s.rCompany) as rCompany,");
		sql.append(" nvl(gr.discount,s.discount) as discount,nvl(gr.special_type,s.special_type) as special_type,nvl(gr.status,s.status) as status from ");
		
		sql.append(" (select nvl(t.ticket_name,ts.ticket_name) as ticket_name,");
		sql.append(" nvl(t.amount,0) as amountGroup,nvl(t.subtotal,0) as subtotalGroup,nvl(t.subtotalE,0) as subtotalGroupE,nvl(t.subtotalR,0) as subtotalGroupR,nvl(t.payFee,0) as payFeeGroup,nvl(t.payFeeE,0) as payFeeGroupE,nvl(t.payFeeR,0) as payFeeGroupR,");
		sql.append(" nvl(ts.amount,0) as amountGroupD,nvl(ts.subtotal,0) as subtotalGroupD,nvl(ts.subtotalE,0) as subtotalGroupDE,nvl(ts.subtotalR,0) as subtotalGroupDR,nvl(ts.payFee,0) as payFeeGroupD,nvl(ts.payFeeE,0) as payFeeGroupDE,nvl(ts.payFeeR,0) as payFeeGroupDR,");
		sql.append(" nvl(t.eCompany,ts.eCompany) as eCompany,nvl(t.rCompany,ts.rCompany) as rCompany,");
		sql.append(" nvl(t.discount,ts.discount) as discount,nvl(t.special_type,ts.special_type) as special_type,nvl(t.status,ts.status) as status from ");
		sql.append(" (select tt.ticket_name,count(tbr.barcode_rec_id) as amount,");
		sql.append(" sum(tid.unit_price) as subtotal,sum(tid.entrance_unit_price) as subtotalE,sum(tid.ropeway_unit_price) as subtotalR,");
		sql.append(" sum(tid.unit_price*tid.per_fee) as payFee,sum(tid.entrance_unit_price*tid.per_fee) as payFeeE,sum(tid.ropeway_unit_price*tid.per_fee) as payFeeR,");
		sql.append(" tid.entrance_company as eCompany,tid.ropeway_company as rCompany,");
		sql.append(" nvl(ti.discount,'10') as discount, tt.special_type,tbr.status");
		sql.append(" from TICKET_BARCODE_REC tbr,TICKET_INDENT_DETAIL tid,TICKET_INDENT ti,TICKET_PRICE tp,TICKET_TYPE tt");
		sql.append(" where tbr.check_rec_id = :checkRecId");
		sql.append(" and tbr.indent_detail_id=tid.indent_detail_id and tid.indent_id = ti.indent_id");
		sql.append(" and tid.ticket_price_id = tp.ticket_price_id and tp.ticket_id=tt.ticket_id");
		sql.append(" and ti.customer_type = '01' and ti.pay_type = '02' and (ti.tel_number <> '1' or ti.tel_number is null )");
		sql.append(" group by tt.ticket_name,tt.ticket_id,nvl(ti.discount,'10'),tt.special_type,tbr.status,tid.entrance_company,tid.ropeway_company) t");
		sql.append(" full join");
		sql.append(" (select tt.ticket_name,count(tbr.barcode_rec_id) as amount,");
		sql.append(" sum(tid.unit_price) as subtotal,sum(tid.entrance_unit_price) as subtotalE,sum(tid.ropeway_unit_price) as subtotalR,");
		sql.append(" sum(tid.unit_price*tid.per_fee) as payFee,sum(tid.entrance_unit_price*tid.per_fee) as payFeeE,sum(tid.ropeway_unit_price*tid.per_fee) as payFeeR,");
		sql.append(" tid.entrance_company as eCompany,tid.ropeway_company as rCompany,");
		sql.append(" nvl(ti.discount,'10') as discount, tt.special_type,tbr.status");
		sql.append(" from TICKET_BARCODE_REC tbr,TICKET_INDENT_DETAIL tid,TICKET_INDENT ti,TICKET_PRICE tp,TICKET_TYPE tt");
		sql.append(" where tbr.check_rec_id = :checkRecId");
		sql.append(" and tbr.indent_detail_id=tid.indent_detail_id and tid.indent_id = ti.indent_id");
		sql.append(" and tid.ticket_price_id = tp.ticket_price_id and tp.ticket_id=tt.ticket_id");
		sql.append(" and ti.customer_type = '01' and ti.pay_type = '02' and ti.tel_number = '1' ");
		sql.append(" group by tt.ticket_name,tt.ticket_id,nvl(ti.discount,'10'),tt.special_type,tbr.status,tid.entrance_company,tid.ropeway_company) ts");
		sql.append(" on t.ticket_name = ts.ticket_name and t.discount = ts.discount and t.status = ts.status and t.eCompany = ts.eCompany and t.rCompany = ts.rCompany) gr ");
		
		sql.append(" full join");
		sql.append(" (select tt.ticket_name,count(tbr.barcode_rec_id) as amount,");
		sql.append(" sum(tid.unit_price) as subtotal,sum(tid.entrance_unit_price) as subtotalE,sum(tid.ropeway_unit_price) as subtotalR,");
		sql.append(" sum(tid.unit_price*tid.per_fee) as payFee,sum(tid.entrance_unit_price*tid.per_fee) as payFeeE,sum(tid.ropeway_unit_price*tid.per_fee) as payFeeR,");
		sql.append(" tid.entrance_company as eCompany,tid.ropeway_company as rCompany,");
		sql.append(" nvl(ti.discount,'10') as discount, tt.special_type,tbr.status");
		sql.append(" from TICKET_BARCODE_REC tbr,TICKET_INDENT_DETAIL tid,TICKET_INDENT ti,TICKET_PRICE tp,TICKET_TYPE tt");
		sql.append(" where tbr.check_rec_id = :checkRecId");
		sql.append(" and tbr.indent_detail_id=tid.indent_detail_id and tid.indent_id = ti.indent_id");
		sql.append(" and tid.ticket_price_id = tp.ticket_price_id and tp.ticket_id=tt.ticket_id");
		sql.append(" and ti.customer_type = '02' and ti.pay_type = '02'");
		sql.append(" group by tt.ticket_name,tt.ticket_id,nvl(ti.discount,'10'), tt.special_type,tbr.status,tid.entrance_company,tid.ropeway_company) s");
		sql.append(" on gr.ticket_name = s.ticket_name and gr.discount = s.discount and gr.status = s.status and gr.eCompany = s.eCompany and gr.rCompany = s.rCompany) w");
		
		sql.append(" full join");
		sql.append(" (select tt.ticket_name,count(tbr.barcode_rec_id) as amount,");
		sql.append(" sum(tid.unit_price) as subtotal,sum(tid.entrance_unit_price) as subtotalE,sum(tid.ropeway_unit_price) as subtotalR,");
		sql.append(" sum(tid.unit_price*tid.per_fee) as payFee,sum(tid.entrance_unit_price*tid.per_fee) as payFeeE,sum(tid.ropeway_unit_price*tid.per_fee) as payFeeR,");
		sql.append(" tid.entrance_company as eCompany,tid.ropeway_company as rCompany,");
		sql.append(" nvl(ti.discount,'10') as discount, tt.special_type,tbr.status");
		sql.append(" from TICKET_BARCODE_REC tbr,TICKET_INDENT_DETAIL tid,TICKET_INDENT ti,TICKET_PRICE tp,TICKET_TYPE tt");
		sql.append(" where tbr.check_rec_id = :checkRecId");
		sql.append(" and tbr.indent_detail_id=tid.indent_detail_id and tid.indent_id = ti.indent_id");
		sql.append(" and tid.ticket_price_id = tp.ticket_price_id and tp.ticket_id=tt.ticket_id");
		sql.append(" and tbr.CHECKING_MACHINE_NUMBER in ('017','018','019','020')");
		sql.append(" group by tt.ticket_name,tt.ticket_id,nvl(ti.discount,'10'),tt.special_type,tbr.status,tid.entrance_company,tid.ropeway_company) x");
		sql.append(" on w.ticket_name = x.ticket_name and w.discount = x.discount and w.status = x.status and w.eCompany = x.eCompany and w.rCompany = x.rCompany");
		sql.append(" order by nvl(w.discount,x.discount) desc,nvl(w.special_type,x.special_type),nvl(w.status,x.status) ");
				
				
				
				
				
				
				
	//						new StringBuffer("select nvl(w.ticket_name,x.ticket_name) as ticket_name,");
	//				sql.append(" nvl(w.amountGroup,0) as amountGroup,nvl(w.subtotalGroup,0) as subtotalGroup,nvl(w.subtotalGroupE,0) as subtotalGroupE,nvl(w.subtotalGroupR,0) as subtotalGroupR,nvl(w.payFeeGroup,0) as payFeeGroup,nvl(w.payFeeGroupE,0) as payFeeGroupE,nvl(w.payFeeGroupR,0) as payFeeGroupR,");
	//				sql.append(" nvl(w.amountGroupD,0) as amountGroupD,nvl(w.subtotalGroupD,0) as subtotalGroupD,nvl(w.subtotalGroupDE,0) as subtotalGroupDE,nvl(w.subtotalGroupDR,0) as subtotalGroupDR,nvl(w.payFeeGroupD,0) as payFeeGroupD,nvl(w.payFeeGroupDE,0) as payFeeGroupDE,nvl(w.payFeeGroupDR,0) as payFeeGroupDR,");
	//				sql.append(" nvl(w.amount,0) as amount,nvl(w.subtotal,0) as subtotal,nvl(w.subtotalE,0) as subtotalE,nvl(w.subtotalR,0) as subtotalR,nvl(w.payFee,0) as payFee,nvl(w.payFeeE,0) as payFeeE,nvl(w.payFeeR,0) as payFeeR,");
	//				sql.append(" nvl(x.amount,0) as amountLocale,nvl(x.subtotal,0) as subtotalLocale,nvl(x.subtotalE,0) as subtotalLocaleE,nvl(x.subtotalR,0) as subtotalLocaleR,nvl(x.payFee,0) as payFeeLocale,nvl(x.payFeeE,0) as payFeeLocaleE,nvl(x.payFeeR,0) as payFeeLocaleR,");
	//				sql.append(" nvl(w.eCompany,x.eCompany) as eCompany,nvl(w.rCompany,x.rCompany) as rCompany,");
	//				sql.append(" nvl(w.discount,x.discount) as discount,nvl(w.status,x.status) as status from ");
	//				
	//				sql.append(" (select nvl(gr.ticket_name,gr.ticket_name) as ticket_name,");
	//				sql.append(" nvl(gr.amountGroup,0) as amountGroup,nvl(gr.subtotalGroup,0) as subtotalGroup,nvl(gr.subtotalGroupE,0) as subtotalGroupE,nvl(gr.subtotalGroupR,0) as subtotalGroupR,nvl(gr.payFeeGroup,0) as payFeeGroup,nvl(gr.payFeeGroupE,0) as payFeeGroupE,nvl(gr.payFeeGroupR,0) as payFeeGroupR,");
	//				sql.append(" nvl(gr.amountGroupD,0) as amountGroupD,nvl(gr.subtotalGroupD,0) as subtotalGroupD,nvl(gr.subtotalGroupDE,0) as subtotalGroupDE,nvl(gr.subtotalGroupDR,0) as subtotalGroupDR,nvl(gr.payFeeGroupD,0) as payFeeGroupD,nvl(gr.payFeeGroupDE,0) as payFeeGroupDE,nvl(gr.payFeeGroupDR,0) as payFeeGroupDR,");
	//				sql.append(" nvl(s.amount,0) as amount,nvl(s.subtotal,0) as subtotal,nvl(s.subtotalE,0) as subtotalE,nvl(s.subtotalR,0) as subtotalR,nvl(s.payFee,0) as payFee,nvl(s.payFeeE,0) as payFeeE,nvl(s.payFeeR,0) as payFeeR,");
	//				sql.append(" nvl(gr.eCompany,s.eCompany) as eCompany,nvl(gr.rCompany,s.rCompany) as rCompany,");
	//				sql.append(" nvl(gr.discount,s.discount) as discount,nvl(gr.special_type,s.special_type) as special_type,nvl(gr.status,s.status) as status from ");
	//				
	//				sql.append(" (select nvl(t.ticket_name,ts.ticket_name) as ticket_name,");
	//				sql.append(" nvl(t.amount,0) as amountGroup,nvl(t.subtotal,0) as subtotalGroup,nvl(t.subtotalE,0) as subtotalGroupE,nvl(t.subtotalR,0) as subtotalGroupR,nvl(t.payFee,0) as payFeeGroup,nvl(t.payFeeE,0) as payFeeGroupE,nvl(t.payFeeR,0) as payFeeGroupR,");
	//				sql.append(" nvl(ts.amount,0) as amountGroupD,nvl(ts.subtotal,0) as subtotalGroupD,nvl(ts.subtotalE,0) as subtotalGroupDE,nvl(ts.subtotalR,0) as subtotalGroupDR,nvl(ts.payFee,0) as payFeeGroupD,nvl(ts.payFeeE,0) as payFeeGroupDE,nvl(ts.payFeeR,0) as payFeeGroupDR,");
	//				sql.append(" nvl(t.eCompany,ts.eCompany) as eCompany,nvl(t.rCompany,ts.rCompany) as rCompany,");
	//				sql.append(" nvl(t.discount,ts.discount) as discount,nvl(t.special_type,ts.special_type) as special_type,nvl(t.status,ts.status) as status from ");
	//				sql.append(" (select tt.ticket_name,count(tbr.barcode_rec_id) as amount,");
	//				sql.append(" sum(tid.unit_price) as subtotal,sum(tid.entrance_unit_price) as subtotalE,sum(tid.ropeway_unit_price) as subtotalR,");
	//				sql.append(" sum(tid.unit_price*tid.per_fee) as payFee,sum(tid.entrance_unit_price*tid.per_fee) as payFeeE,sum(tid.ropeway_unit_price*tid.per_fee) as payFeeR,");
	//				sql.append(" tid.entrance_company as eCompany,tid.ropeway_company as rCompany,");
	//				sql.append(" nvl(ti.discount,'10') as discount, tt.special_type,tbr.status");
	//				sql.append(" from TICKET_BARCODE_REC tbr,TICKET_INDENT_DETAIL tid,TICKET_INDENT ti,TICKET_PRICE tp,TICKET_TYPE tt");
	//				sql.append(" where tbr.check_rec_id = :checkRecId");
	//				sql.append(" and tbr.indent_detail_id=tid.indent_detail_id and tid.indent_id = ti.indent_id");
	//				sql.append(" and tid.ticket_price_id = tp.ticket_price_id and tp.ticket_id=tt.ticket_id");
	//				sql.append(" and ti.customer_type = '01' and ti.pay_type = '02' and (ti.tel_number <> '1' or ti.tel_number is null )");
	//				sql.append(" group by tt.ticket_name,tt.ticket_id,nvl(ti.discount,'10'),tt.special_type,tbr.status,tid.entrance_company,tid.ropeway_company) t");
	//				sql.append(" full join");
	//				sql.append(" (select tt.ticket_name,count(tbr.barcode_rec_id) as amount,");
	//				sql.append(" sum(tid.unit_price) as subtotal,sum(tid.entrance_unit_price) as subtotalE,sum(tid.ropeway_unit_price) as subtotalR,");
	//				sql.append(" sum(tid.unit_price*tid.per_fee) as payFee,sum(tid.entrance_unit_price*tid.per_fee) as payFeeE,sum(tid.ropeway_unit_price*tid.per_fee) as payFeeR,");
	//				sql.append(" tid.entrance_company as eCompany,tid.ropeway_company as rCompany,");
	//				sql.append(" nvl(ti.discount,'10') as discount, tt.special_type,tbr.status");
	//				sql.append(" from TICKET_BARCODE_REC tbr,TICKET_INDENT_DETAIL tid,TICKET_INDENT ti,TICKET_PRICE tp,TICKET_TYPE tt");
	//				sql.append(" where tbr.check_rec_id = :checkRecId");
	//				sql.append(" and tbr.indent_detail_id=tid.indent_detail_id and tid.indent_id = ti.indent_id");
	//				sql.append(" and tid.ticket_price_id = tp.ticket_price_id and tp.ticket_id=tt.ticket_id");
	//				sql.append(" and ti.customer_type = '01' and ti.pay_type = '02' and ti.tel_number = '1' ");
	//				sql.append(" group by tt.ticket_name,tt.ticket_id,nvl(ti.discount,'10'),tt.special_type,tbr.status,tid.entrance_company,tid.ropeway_company) ts");
	//				sql.append(" on t.ticket_name = ts.ticket_name and t.discount = ts.discount and t.status = ts.status and t.eCompany = ts.eCompany and t.rCompany = ts.rCompany) gr ");
	//				
	//				sql.append(" full join");
	//				sql.append(" (select tt.ticket_name,count(tbr.barcode_rec_id) as amount,");
	//				sql.append(" sum(tid.unit_price) as subtotal,sum(tid.entrance_unit_price) as subtotalE,sum(tid.ropeway_unit_price) as subtotalR,");
	//				sql.append(" sum(tid.unit_price*tid.per_fee) as payFee,sum(tid.entrance_unit_price*tid.per_fee) as payFeeE,sum(tid.ropeway_unit_price*tid.per_fee) as payFeeR,");
	//				sql.append(" tid.entrance_company as eCompany,tid.ropeway_company as rCompany,");
	//				sql.append(" nvl(ti.discount,'10') as discount, tt.special_type,tbr.status");
	//				sql.append(" from TICKET_BARCODE_REC tbr,TICKET_INDENT_DETAIL tid,TICKET_INDENT ti,TICKET_PRICE tp,TICKET_TYPE tt");
	//				sql.append(" where tbr.check_rec_id = :checkRecId");
	//				sql.append(" and tbr.indent_detail_id=tid.indent_detail_id and tid.indent_id = ti.indent_id");
	//				sql.append(" and tid.ticket_price_id = tp.ticket_price_id and tp.ticket_id=tt.ticket_id");
	//				sql.append(" and ti.customer_type = '02' and ti.pay_type = '02'");
	//				sql.append(" group by tt.ticket_name,tt.ticket_id,nvl(ti.discount,'10'), tt.special_type,tbr.status,tid.entrance_company,tid.ropeway_company) s");
	//				sql.append(" on gr.ticket_name = s.ticket_name and gr.discount = s.discount and gr.status = s.status and gr.eCompany = s.eCompany and gr.rCompany = s.rCompany) w");
	//				
	//				sql.append(" full join");
	//				sql.append(" (select tt.ticket_name,count(tbr.barcode_rec_id) as amount,");
	//				sql.append(" sum(tid.unit_price) as subtotal,sum(tid.entrance_unit_price) as subtotalE,sum(tid.ropeway_unit_price) as subtotalR,");
	//				sql.append(" sum(tid.unit_price*tid.per_fee) as payFee,sum(tid.entrance_unit_price*tid.per_fee) as payFeeE,sum(tid.ropeway_unit_price*tid.per_fee) as payFeeR,");
	//				sql.append(" tid.entrance_company as eCompany,tid.ropeway_company as rCompany,");
	//				sql.append(" nvl(ti.discount,'10') as discount, tt.special_type,tbr.status");
	//				sql.append(" from TICKET_BARCODE_REC tbr,TICKET_INDENT_DETAIL tid,TICKET_INDENT ti,TICKET_PRICE tp,TICKET_TYPE tt");
	//				sql.append(" where tbr.check_rec_id = :checkRecId");
	//				sql.append(" and tbr.indent_detail_id=tid.indent_detail_id and tid.indent_id = ti.indent_id");
	//				sql.append(" and tid.ticket_price_id = tp.ticket_price_id and tp.ticket_id=tt.ticket_id");
	//				sql.append(" and tbr.CHECKING_MACHINE_NUMBER in ('017','018','019','020')");
	//				sql.append(" group by tt.ticket_name,tt.ticket_id,nvl(ti.discount,'10'),tt.special_type,tbr.status,tid.entrance_company,tid.ropeway_company) x");
	//				sql.append(" on w.ticket_name = x.ticket_name and w.discount = x.discount and w.status = x.status and w.eCompany = x.eCompany and w.rCompany = x.rCompany");
	//				sql.append(" order by nvl(w.discount,x.discount) desc,nvl(w.special_type,x.special_type),nvl(w.status,x.status) ");
			
		Query query = session.createSQLQuery(sql.toString());
	//				System.out.println(sql.toString());
		query.setLong("checkRecId", checkRecId);
		query.setLong("checkRecId", checkRecId);
		query.setLong("checkRecId", checkRecId);
		query.setLong("checkRecId", checkRecId);
		@SuppressWarnings("rawtypes")
		List l = query.list();
		
		List<TicketAccounts> list = new ArrayList<TicketAccounts>();
		for(Object r : l){
			
			Object[] temp = (Object[])r;
			TicketAccounts ta = new TicketAccounts();
	
			if(temp[0]!=null){
				ta.setTicketName(temp[0].toString());
			}
			if(temp[1]!=null){
				ta.setAmountGroup(Integer.valueOf(temp[1].toString()));
			}
			if(temp[2]!=null){
				ta.setSubtotalGroup(Double.valueOf(temp[2].toString()));
			}
			if(temp[3]!=null){
				ta.setSubtotalGroupE(Double.valueOf(temp[3].toString()));
			}
			if(temp[4]!=null){
				ta.setSubtotalGroupR(Double.valueOf(temp[4].toString()));
			}
			if(temp[5]!=null){
				ta.setPayFeeGroup(Double.valueOf(temp[5].toString()));
			}
			if(temp[6]!=null){
				ta.setPayFeeGroupE(Double.valueOf(temp[6].toString()));
			}
			if(temp[7]!=null){
				ta.setPayFeeGroupR(Double.valueOf(temp[7].toString()));
			}
			if(temp[8]!=null){
				ta.setAmountGroupD(Integer.valueOf(temp[8].toString()));
			}
			if(temp[9]!=null){
				ta.setSubtotalGroupD(Double.valueOf(temp[9].toString()));
			}
			if(temp[10]!=null){
				ta.setSubtotalGroupDE(Double.valueOf(temp[10].toString()));
			}
			if(temp[11]!=null){
				ta.setSubtotalGroupDR(Double.valueOf(temp[11].toString()));
			}
			if(temp[12]!=null){
				ta.setPayFeeGroupD(Double.valueOf(temp[12].toString()));
			}
			if(temp[13]!=null){
				ta.setPayFeeGroupDE(Double.valueOf(temp[13].toString()));
			}
			if(temp[14]!=null){
				ta.setPayFeeGroupDR(Double.valueOf(temp[14].toString()));
			}
			if(temp[15]!=null){
				ta.setAmount(Integer.valueOf(temp[15].toString()));
			}
			if(temp[16]!=null){
				ta.setSubtotal(Double.valueOf(temp[16].toString()));
			}
			if(temp[17]!=null){
				ta.setSubtotalE(Double.valueOf(temp[17].toString()));
			}
			if(temp[18]!=null){
				ta.setSubtotalR(Double.valueOf(temp[18].toString()));
			}
			if(temp[19]!=null){
				ta.setPayFee(Double.valueOf(temp[19].toString()));
			}
			if(temp[20]!=null){
				ta.setPayFeeE(Double.valueOf(temp[20].toString()));
			}
			if(temp[21]!=null){
				ta.setPayFeeR(Double.valueOf(temp[21].toString()));
			}
			if(temp[22]!=null){
				ta.setAmountLocale(Integer.valueOf(temp[22].toString()));
			}
			if(temp[23]!=null){
				ta.setSubtotalLocale(Double.valueOf(temp[23].toString()));
			}
			if(temp[24]!=null){
				ta.setSubtotalLocaleE(Double.valueOf(temp[24].toString()));
			}
			if(temp[25]!=null){
				ta.setSubtotalLocaleR(Double.valueOf(temp[25].toString()));
			}
			if(temp[26]!=null){
				ta.setPayFeeLocale(Double.valueOf(temp[26].toString()));
			}
			if(temp[27]!=null){
				ta.setPayFeeLocaleE(Double.valueOf(temp[27].toString()));
			}
			if(temp[28]!=null){
				ta.setPayFeeLocaleR(Double.valueOf(temp[28].toString()));
			}
			if(temp[29]!=null){
				ta.seteCompany(temp[29].toString());
			}
			if(temp[30]!=null){
				ta.setrCompany(temp[30].toString());
			}
			if(temp[31]!=null){
				ta.setDiscount(temp[31].toString());
			}
			if(temp[32]!=null){
				ta.setStatus(temp[32].toString());
			}
			list.add(ta);
		}
		
		return list;
	}
	
}