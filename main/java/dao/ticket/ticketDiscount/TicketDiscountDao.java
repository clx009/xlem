package dao.ticket.ticketDiscount;
import com.xlem.dao.TicketDiscount;

import common.dao.BaseDao;

public interface TicketDiscountDao extends BaseDao<TicketDiscount> {

}