package dao.ticket.ticketDiscount;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.TicketDiscount;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("ticketDiscountDao")
public class TicketDiscountDaoImpl extends BaseDaoImpl<TicketDiscount> implements TicketDiscountDao {

}