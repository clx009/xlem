package dao.ticket.ticketIndent;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.TicketIndent;

import common.CommonMethod;
import common.action.JsonPager;
import common.dao.BaseDaoImpl;

@Transactional
@Repository("ticketIndentDao")
public class TicketIndentDaoImpl extends BaseDaoImpl<TicketIndent> implements TicketIndentDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<TicketIndent> findIndentList(final TicketIndent t,final long userId,final JsonPager<TicketIndent> jp,final String forPage) {
		Session session = sessionFactory.getCurrentSession();
		String sql = "select * from"+
				" (select b.*,rownum rownum_ from"+
				" (select a.indent_Id,a.indent_Code,a.total_Amount,a.total_Price,a.use_Date,"+
				" a.per_Fee,a.linkman,a.id_Card_Number,a.phone_Number,a.check_Code,"+
				" a.tel_Number,a.fax_Number,a.pay_Type,a.customer_Type, a.status,"+
				" a.remark,a.inputer,a.input_Time,a.updater,a.update_Time,a.synchro_State, to_char(wmsys.wm_concat(a.ticket_name)) as tiket_names,a.discount,a.is_outbound"+
				" from (select ti.indent_Id,ti.indent_Code,ti.total_Amount,ti.total_Price,ti.use_Date,"+
				" ti.per_Fee,ti.linkman,ti.id_Card_Number,ti.phone_Number,ti.check_Code,"+
				" ti.tel_Number,ti.fax_Number,cast(ti.pay_Type as varchar2(2)) as pay_Type,cast(ti.customer_Type as varchar2(2)) as customer_Type,cast(ti.status as varchar2(2)) as status,"+
				" ti.remark,cast(ti.discount as varchar2(2)) as discount,ti.inputer,cast(ti.input_Time as timestamp) as input_Time,ti.updater,cast(ti.update_Time as timestamp) as update_Time,cast(ti.synchro_State as varchar2(2)) as synchro_State,tt.ticket_name, cast(ti.is_outbound as varchar2(2)) as is_outbound"+
				" from TICKET_INDENT ti,TICKET_INDENT_DETAIL tid,TICKET_PRICE tp,TICKET_TYPE tt"+
				" where ti.indent_id = tid.indent_id and tid.ticket_price_id = tp.ticket_price_id " +
				" and tp.ticket_id = tt.ticket_id";
				
				if("queryAll".equals(forPage)){
					
				}
				else if(!"checkPage".equals(forPage)){
					sql = sql+" and ti.inputer = :inputer";
				}
				
				if("returnPage".equals(forPage)){
					sql = sql+" and (ti.status = '02' or ti.status = '03')";
				}
				else if("refundPage".equals(forPage)){//查询可退现金的票订单
					sql = sql+" and (ti.status = '04' or ti.status = '05' or ti.status = '06')";
					sql = sql+" and ti.indent_id in (select ti1.indent_id"+
	                          " from ticket_indent ti1"+
	                          " left join base_tran_rec btr"+
	                          " on ti1.indent_id = btr.indent_id"+
	                          " where btr.payment_type = '02'"+
	                          " and btr.pay_type = '02')"+
	                          " and ti.indent_id in (select ti1.indent_id"+
	                          " from ticket_indent ti1"+
	                          " left join base_tran_rec btr"+
	                          " on ti1.indent_id = btr.indent_id"+
	                          " where btr.payment_type = '01'"+
	                          " and btr.pay_type = '01')"+
	                          " and ti.indent_id not in (select ti2.indent_id"+
                              " from ticket_indent ti2"+
                              " left join base_tran_rec btr"+
                              " on ti2.indent_id = btr.indent_id"+
                              " where btr.payment_type = '02'"+
                              " and btr.pay_type = '01')";
				}
				if(t.getTravelAgencyType()!=null && !t.getTravelAgencyType().isEmpty()){
					sql = sql+" and ti.inputer in (select su.user_id " +
							"from sys_user su,Base_Travel_Agency bta " +
							"where su.travel_Agency_Id = bta.travel_Agency_Id " +
							"and bta.travel_Agency_Type  like :travel_Agency_Type)";
				}
				if(t.getCustomerType()!=null && !t.getCustomerType().isEmpty()){
					sql = sql+" and ti.customer_Type like :customerType";
				}
				if(t.getStatus()!=null && !t.getStatus().isEmpty()){
					sql = sql+" and ti.status like :status";
				}
				if(t.getIndentCode()!=null && !t.getIndentCode().trim().isEmpty()){
					sql = sql+" and ti.indent_Code like :indentCode";
				}
				
				if(t.getPhoneNumber()!=null && !t.getPhoneNumber().trim().isEmpty()){
					sql = sql+" and ti.phone_Number like :phoneNumber";
				}
				
				if(t.getIdCardNumber()!=null && !t.getIdCardNumber().trim().isEmpty()){
					sql = sql+" and ti.id_Card_Number like :idCardNumber";
				}
				
				if(t.getStartDate()!=null){
					sql = sql+" and ti.input_Time >= :startDate";
				}
				
				if(t.getEndDate()!=null){
					sql = sql+" and ti.input_Time <= :endDate";
				}
				
				if(t.getIsOutbound()!=null && !t.getIsOutbound().trim().isEmpty()){
					if(t.getIsOutbound().equals("00")){
						sql = sql+" and (ti.is_outbound = :isOutbound or ti.is_outbound is null)";
					}
					else{
						sql = sql+" and ti.is_outbound = :isOutbound";
					}
				}
				
				if(t.getUseDate()!=null){
					String sDate = CommonMethod.timeToString(Timestamp.valueOf(t.getUseDate().toString()));
					
					String eDate = CommonMethod.timeToString(Timestamp.valueOf(t.getUseDate().toString()));
					sql = sql+" and ti.use_Date >= TO_DATE('"+sDate+" 00:00:00','yyyy-mm-dd hh24:mi:ss') ";
					sql = sql+" and ti.use_Date <= TO_DATE('"+eDate+" 23:59:59','yyyy-mm-dd hh24:mi:ss')";
				}
				
			sql = sql+" order by ti.input_time desc,tt.ticket_id"+
				" ) a group by a.indent_Id,a.indent_Code,a.total_Amount,a.total_Price,a.use_Date,"+
				" a.per_Fee,a.linkman,a.id_Card_Number,a.phone_Number,a.check_Code,"+
				" a.tel_Number,a.fax_Number,a.pay_Type,a.customer_Type,a.status,"+
				" a.remark,a.discount,a.inputer,a.input_Time,a.updater,a.update_Time,a.synchro_State,a.is_outbound " +
				" order by a.input_Time desc) b"+
				" where rownum <= :rownumE ) c where rownum_ > :rownumS";
		
			
		Query query = session.createSQLQuery(sql);

		if("queryAll".equals(forPage)){
			
		}
		else if(!"checkPage".equals(forPage)){
			query.setLong("inputer", userId);
		}
		
		if(t.getTravelAgencyType()!=null && !t.getTravelAgencyType().isEmpty()){
			query.setString("travel_Agency_Type", t.getTravelAgencyType());
		}
		if(t.getCustomerType()!=null && !t.getCustomerType().isEmpty()){
			query.setString("customerType", t.getCustomerType());
		}
		if(t.getStatus()!=null && !t.getStatus().trim().isEmpty()){
			query.setString("status", t.getStatus());
		}
		if(t.getIndentCode()!=null && !t.getIndentCode().trim().isEmpty()){
			query.setString("indentCode", "%"+t.getIndentCode().trim()+"%");
		}
		
		if(t.getPhoneNumber()!=null && !t.getPhoneNumber().trim().isEmpty()){
			query.setString("phoneNumber", "%"+t.getPhoneNumber().trim()+"%");
		}
		
		if(t.getIdCardNumber()!=null && !t.getIdCardNumber().trim().isEmpty()){
			query.setString("idCardNumber", "%"+t.getIdCardNumber().trim()+"%");
		}
		
		if(t.getStartDate()!=null){
			query.setTimestamp("startDate", CommonMethod.toStartTime(t.getStartDate()));
		}
		
		if(t.getEndDate()!=null){
			query.setTimestamp("endDate", CommonMethod.toEndTime(t.getEndDate()));
		}
		
		if(t.getIsOutbound()!=null && !t.getIsOutbound().trim().isEmpty()){
			query.setString("isOutbound", t.getIsOutbound());
		}
		
		int rownumS = Integer.valueOf(jp.getStart());
		int limit = Integer.valueOf(jp.getLimit());
		
		query.setInteger("rownumS", rownumS);
		query.setInteger("rownumE", rownumS+limit);
		
		List l = query.list();
		
		List<TicketIndent> list = new ArrayList<TicketIndent>();
		
		for(Object r : l){
			
			Object[] temp = (Object[])r;
			
			TicketIndent ti = new TicketIndent();
			
			if(temp[0]!=null){
				ti.setIndentId(temp[0].toString());
			}
			if(temp[1]!=null){
				ti.setIndentCode(temp[1].toString());
			}
			if(temp[2]!=null){
				ti.setTotalAmount(Integer.valueOf(temp[2].toString()));
			}
			if(temp[3]!=null){
				ti.setTotalPrice(Double.valueOf(temp[3].toString()));
			}
			if(temp[4]!=null){
				ti.setUseDate(CommonMethod.string2Time1(temp[4].toString()));
			}
			if(temp[5]!=null){
				ti.setPerFee(Double.valueOf(temp[5].toString()));
			}
			if(temp[6]!=null){
				ti.setLinkman(temp[6].toString());
			}
			if(temp[7]!=null){
				ti.setIdCardNumber(temp[7].toString());
			}
			if(temp[8]!=null){
				ti.setPhoneNumber(temp[8].toString());
			}
			if(temp[9]!=null){
				ti.setCheckCode(temp[9].toString());
			}
			if(temp[10]!=null){
				ti.setTelNumber(temp[10].toString());
			}
			if(temp[11]!=null){
				ti.setFaxNumber(temp[11].toString());
			}
			if(temp[12]!=null){
				ti.setPayType(temp[12].toString());
			}
			if(temp[13]!=null){
				ti.setCustomerType(temp[13].toString());
			}
			if(temp[14]!=null){
				ti.setStatus(temp[14].toString());
			}
			if(temp[15]!=null){
				ti.setRemark(temp[15].toString());
			}
			if(temp[16]!=null){
				ti.setInputer(Long.valueOf(temp[16].toString()));
			}
			if(temp[17]!=null){
				ti.setInputTime(CommonMethod.string2Time1(temp[17].toString()));
			}
			if(temp[18]!=null){
				ti.setUpdater(Long.valueOf(temp[18].toString()));
			}
			if(temp[19]!=null){
				ti.setUpdateTime(CommonMethod.string2Time1(temp[19].toString()));
			}
			if(temp[20]!=null){
				ti.setSynchroState(temp[20].toString());
			}
			if(temp[21]!=null){
				String ticketNames = temp[21].toString();
				if(ticketNames.indexOf(",")>=0){
					ticketNames = ticketNames.replaceAll(",", "<br />");
				}
				ti.setTicketNames(ticketNames);
			}
			
			if(temp[22]!=null){
				ti.setDiscount(temp[22].toString());
			}
			if(temp[23]!=null){
				ti.setIsOutbound(temp[23].toString());
			}
			
			list.add(ti);
		}
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public int findIndentListCount(final TicketIndent t,final long userId,final JsonPager<TicketIndent> jp,final String forPage) {
		Session session = sessionFactory.getCurrentSession();
		String sql = " select count(distinct ti.indent_Id) as total "+
				" from TICKET_INDENT ti,TICKET_INDENT_DETAIL tid"+
				" where ti.indent_Id=tid.indent_Id";
				if("queryAll".equals(forPage)){
			
				}
				else if(!"checkPage".equals(forPage)){
					sql = sql+" and ti.inputer = :inputer";
				}
				if("returnPage".equals(forPage)){
					sql = sql+" and (ti.status = '02' or ti.status = '03')";
				}
				else if("refundPage".equals(forPage)){//查询可退现金的票订单
					sql = sql+" and (ti.status = '04' or ti.status = '05' or ti.status = '06')";
					sql = sql+" and ti.indent_id in (select ti1.indent_id"+
	                          " from ticket_indent ti1"+
	                          " left join base_tran_rec btr"+
	                          " on ti1.indent_id = btr.indent_id"+
	                          " where btr.payment_type = '02'"+
	                          " and btr.pay_type = '02')"+
	                          " and ti.indent_id in (select ti1.indent_id"+
	                          " from ticket_indent ti1"+
	                          " left join base_tran_rec btr"+
	                          " on ti1.indent_id = btr.indent_id"+
	                          " where btr.payment_type = '01'"+
	                          " and btr.pay_type = '01')"+
	                          " and ti.indent_id not in (select ti2.indent_id"+
                              " from ticket_indent ti2"+
                              " left join base_tran_rec btr"+
                              " on ti2.indent_id = btr.indent_id"+
                              " where btr.payment_type = '02'"+
                              " and btr.pay_type = '01')";
				}
				if(t.getTravelAgencyType()!=null && !t.getTravelAgencyType().isEmpty()){
					sql = sql+" and ti.inputer in (select su.user_id " +
							"from sys_user su,Base_Travel_Agency bta " +
							"where su.travel_Agency_Id = bta.travel_Agency_Id " +
							"and bta.travel_Agency_Type  like :travel_Agency_Type)";
				}
				if(t.getCustomerType()!=null && !t.getCustomerType().isEmpty()){
					sql = sql+" and ti.customer_Type like :customerType";
				}
				if(t.getStatus()!=null && !t.getStatus().isEmpty()){
					sql = sql+" and ti.status like :status";
				}
				if(t.getIndentCode()!=null && !t.getIndentCode().trim().isEmpty()){
					sql = sql+" and ti.indent_Code like :indentCode";
				}
				
				if(t.getPhoneNumber()!=null && !t.getPhoneNumber().trim().isEmpty()){
					sql = sql+" and ti.phone_Number like :phoneNumber";
				}
				
				if(t.getIdCardNumber()!=null && !t.getIdCardNumber().trim().isEmpty()){
					sql = sql+" and ti.id_Card_Number like :idCardNumber";
				}
				
				if(t.getStartDate()!=null){
					sql = sql+" and ti.input_Time >= :startDate";
				}
				
				if(t.getEndDate()!=null){
					sql = sql+" and ti.input_Time <= :endDate";
				}
				
				if(t.getIsOutbound()!=null && !t.getIsOutbound().trim().isEmpty()){
					if(t.getIsOutbound().equals("00")){
						sql = sql+" and (ti.is_outbound = :isOutbound or ti.is_outbound is null)";
					}
					else{
						sql = sql+" and ti.is_outbound = :isOutbound";
					}
				}
		
				if(t.getUseDate()!=null){
					String sDate = CommonMethod.timeToString(Timestamp.valueOf(t.getUseDate().toString()));
					String eDate = CommonMethod.timeToString(Timestamp.valueOf(t.getUseDate().toString()));
					sql = sql+" and ti.use_Date >= TO_DATE('"+sDate+" 00:00:00','yyyy-mm-dd hh24:mi:ss') ";
					sql = sql+" and ti.use_Date <= TO_DATE('"+eDate+" 23:59:59','yyyy-mm-dd hh24:mi:ss')";
				}
				
		Query query = session.createSQLQuery(sql);
		if("queryAll".equals(forPage)){
			
		}
		else if(!"checkPage".equals(forPage)){
			query.setLong("inputer", userId);
		}
		
		if(t.getTravelAgencyType()!=null && !t.getTravelAgencyType().isEmpty()){
			query.setString("travel_Agency_Type", t.getTravelAgencyType());
		}
		if(t.getCustomerType()!=null && !t.getCustomerType().isEmpty()){
			query.setString("customerType", t.getCustomerType());
		}
		if(t.getStatus()!=null && !t.getStatus().trim().isEmpty()){
			query.setString("status", t.getStatus());
		}
		if(t.getIndentCode()!=null && !t.getIndentCode().trim().isEmpty()){
			query.setString("indentCode", "%"+t.getIndentCode().trim()+"%");
		}
		
		if(t.getPhoneNumber()!=null && !t.getPhoneNumber().trim().isEmpty()){
			query.setString("phoneNumber", "%"+t.getPhoneNumber().trim()+"%");
		}
		
		if(t.getIdCardNumber()!=null && !t.getIdCardNumber().trim().isEmpty()){
			query.setString("idCardNumber", "%"+t.getIdCardNumber().trim()+"%");
		}
		
		if(t.getStartDate()!=null){
			query.setTimestamp("startDate", CommonMethod.toStartTime(t.getStartDate()));
		}
		
		if(t.getEndDate()!=null){
			query.setTimestamp("endDate", CommonMethod.toEndTime(t.getEndDate()));
		}
		
		if(t.getIsOutbound()!=null && !t.getIsOutbound().trim().isEmpty()){
			query.setString("isOutbound", t.getIsOutbound());
		}
		
		List l = query.list();
		
		int total = Integer.valueOf(l.get(0).toString());
		
		return total;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public JsonPager<TicketIndent> findIndent(final TicketIndent t,final long userId,final JsonPager<TicketIndent> jsonPager,final String forPage) {
		Session session = sessionFactory.getCurrentSession();
		//1.count 总记录数
		StringBuffer querySQLCount = new StringBuffer();
		querySQLCount.append(" select count(distinct ti.indent_Id) as total ");
		querySQLCount.append(" from TICKET_INDENT ti,TICKET_INDENT_DETAIL tid ");
		querySQLCount.append(" where ti.indent_Id=tid.indent_Id ");
		
		initConditionSQL( querySQLCount , t, userId, forPage);
		
		Query queryCount = session.createSQLQuery(querySQLCount.toString());
		//1.1查询条件赋值
		initCondition(queryCount,t,userId,forPage);
		jsonPager.setTotal(Integer.parseInt(queryCount.list().get(0).toString()));
		
		StringBuffer querySQL = new StringBuffer();
		querySQL.append(" select a.indent_Id,a.indent_Code,a.total_Amount,a.total_Price,a.use_Date, ");
		querySQL.append(" a.per_Fee,a.linkman,a.id_Card_Number,a.phone_Number,a.check_Code, ");
		querySQL.append(" a.tel_Number,a.fax_Number,a.pay_Type,a.customer_Type, a.status,a.indent_Type, ");
		querySQL.append(" a.remark,a.inputer,a.input_Time,a.updater,a.update_Time,a.synchro_State, to_char(wmsys.wm_concat(a.ticket_name)) as tiket_names,a.discount,a.is_outbound ");
		querySQL.append(" from (select ti.indent_Id,ti.indent_Code,ti.total_Amount,ti.total_Price,ti.use_Date, ");
		querySQL.append(" ti.per_Fee,ti.linkman,ti.id_Card_Number,ti.phone_Number,ti.check_Code,cast(ti.indent_Type as varchar2(2)) as indent_Type, ");
		querySQL.append(" ti.tel_Number,ti.fax_Number,cast(ti.pay_Type as varchar2(2)) as pay_Type,cast(ti.customer_Type as varchar2(2)) as customer_Type,cast(ti.status as varchar2(2)) as status, ");
		querySQL.append(" ti.remark,cast(ti.discount as varchar2(2)) as discount,ti.inputer,cast(ti.input_Time as timestamp) as input_Time,ti.updater,cast(ti.update_Time as timestamp) as update_Time,cast(ti.synchro_State as varchar2(2)) as synchro_State,tt.ticket_name, cast(ti.is_outbound as varchar2(2)) as is_outbound ");
		querySQL.append(" from TICKET_INDENT ti,TICKET_INDENT_DETAIL tid,TICKET_PRICE tp,TICKET_TYPE tt ");
		querySQL.append(" where ti.indent_id = tid.indent_id and tid.ticket_price_id = tp.ticket_price_id ");
		querySQL.append(" and tp.ticket_id = tt.ticket_id ");
				
		initConditionSQL( querySQL , t, userId, forPage);
				
		querySQL.append(" order by ti.input_time desc,tt.ticket_id ");
		querySQL.append(" ) a group by a.indent_Id,a.indent_Code,a.total_Amount,a.total_Price,a.use_Date, ");
		querySQL.append(" a.per_Fee,a.linkman,a.id_Card_Number,a.phone_Number,a.check_Code,a.indent_Type, ");
		querySQL.append(" a.tel_Number,a.fax_Number,a.pay_Type,a.customer_Type,a.status, ");
		querySQL.append(" a.remark,a.discount,a.inputer,a.input_Time,a.updater,a.update_Time,a.synchro_State,a.is_outbound ");
		querySQL.append(" order by a.input_Time desc ");
		
		
		//2.查询分页记录
		Query query = session.createSQLQuery(querySQL.toString());
		//2.1查询条件赋值
		initCondition(query,t,userId,forPage);
		query.setFirstResult(Integer.parseInt(jsonPager.getStart()));
		if(!"rForMKT".equals(forPage)){
			query.setMaxResults(Integer.parseInt(jsonPager.getLimit()));
		}
		query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		//3.循环填充list
		List<TicketIndent> infoList=new ArrayList<TicketIndent>();
		for (Object obj : query.list()) {
			Map map = (Map)obj;
			TicketIndent ti = new TicketIndent();
			
				ti.setIndentId(map.get("INDENT_ID")==null?null:map.get("INDENT_ID").toString());
				ti.setIndentCode(map.get("INDENT_CODE")==null?null:map.get("INDENT_CODE").toString());
				ti.setTotalAmount(map.get("TOTAL_AMOUNT")==null?null:Integer.valueOf(map.get("TOTAL_AMOUNT").toString()));
				ti.setTotalPrice(map.get("TOTAL_PRICE")==null?null:Double.valueOf(map.get("TOTAL_PRICE").toString()));
				ti.setUseDate(map.get("USE_DATE")==null?null:CommonMethod.string2Time1(map.get("USE_DATE").toString()));
				ti.setPerFee(map.get("PER_FEE")==null?null:Double.valueOf(map.get("PER_FEE").toString()));
				ti.setLinkman(map.get("LINKMAN")==null?null:map.get("LINKMAN").toString());
				ti.setIdCardNumber(map.get("ID_CARD_NUMBER")==null?null:map.get("ID_CARD_NUMBER").toString());
				ti.setPhoneNumber(map.get("PHONE_NUMBER")==null?null:map.get("PHONE_NUMBER").toString());
				ti.setCheckCode(map.get("CHECK_CODE")==null?null:map.get("CHECK_CODE").toString());
				ti.setTelNumber(map.get("TEL_NUMBER")==null?null:map.get("TEL_NUMBER").toString());
				ti.setFaxNumber(map.get("FAX_NUMBER")==null?null:map.get("FAX_NUMBER").toString());
				ti.setPayType(map.get("PAY_TYPE")==null?null:map.get("PAY_TYPE").toString());
				ti.setCustomerType(map.get("CUSTOMER_TYPE")==null?null:map.get("CUSTOMER_TYPE").toString());
				ti.setStatus(map.get("STATUS")==null?null:map.get("STATUS").toString());
				ti.setRemark(map.get("REMARK")==null?null:map.get("REMARK").toString());
				ti.setInputer(map.get("INPUTER")==null?null:Long.valueOf(map.get("INPUTER").toString()));
				ti.setInputTime(map.get("INPUT_TIME")==null?null:CommonMethod.string2Time1(map.get("INPUT_TIME").toString()));
				ti.setUpdater(map.get("UPDATER")==null?null:Long.valueOf(map.get("UPDATER").toString()));
				ti.setUpdateTime(map.get("UPDATE_TIME")==null?null:CommonMethod.string2Time1(map.get("UPDATE_TIME").toString()));
				ti.setSynchroState(map.get("SYNCHRO_STATE")==null?null:map.get("SYNCHRO_STATE").toString());
				ti.setIndentType(map.get("INDENT_TYPE")==null?null:map.get("INDENT_TYPE").toString());
			if(map.get("TIKET_NAMES")!=null){
				String ticketNames = map.get("TIKET_NAMES").toString();
				if(ticketNames.indexOf(",")>=0){
					ticketNames = ticketNames.replaceAll(",", "<br />");
				}
				ti.setTicketNames(ticketNames);
			}
			ti.setDiscount(map.get("DISCOUNT")==null?null:map.get("DISCOUNT").toString());
			ti.setIsOutbound(map.get("IS_OUTBOUND")==null?null:map.get("IS_OUTBOUND").toString());
			ti.setIndentType(map.get("INDENT_TYPE")==null?null:map.get("INDENT_TYPE").toString());
			infoList.add(ti);
		}
		jsonPager.setRoot(infoList);
		return jsonPager;
	}
	
	private void initConditionSQL(StringBuffer querySQL ,final TicketIndent t,final long userId,final String forPage){
		if("queryAll".equals(forPage)){
			
		}else if("webService".equals(forPage)){
			querySQL.append(" and ti.pay_type not in ('05','06') and ti.status in ('02','03','04','05') ");
		}
		else if(!"checkPage".equals(forPage)&&!"refundPage".equals(forPage)&&!"returnScbPage".equals(forPage)){
			querySQL.append(" and ti.inputer = :inputer ");
		}
		
		if("returnPage".equals(forPage) || "returnScbPage".equals(forPage)){
			querySQL.append(" and (ti.status = '02' or ti.status = '03') ");
		}
		else if("refundPage".equals(forPage)){//查询可退现金的票订单
			querySQL.append(" and (ti.status = '04' or ti.status = '05' or ti.status = '06') ");
			querySQL.append(" and ti.indent_id in (select ti1.indent_id ");
			querySQL.append(" from ticket_indent ti1 ");
			querySQL.append(" left join base_tran_rec btr ");
			querySQL.append(" on ti1.indent_id = btr.indent_id ");
			querySQL.append(" where btr.payment_type = '02' ");
			querySQL.append(" and btr.pay_type = '02') ");
			querySQL.append(" and ti.indent_id in (select ti1.indent_id ");
			querySQL.append(" from ticket_indent ti1 ");
			querySQL.append(" left join base_tran_rec btr ");
			querySQL.append(" on ti1.indent_id = btr.indent_id ");
			querySQL.append(" where btr.payment_type = '01' ");
			querySQL.append(" and btr.pay_type = '01') ");
			querySQL.append(" and ti.indent_id not in (select ti2.indent_id ");
			querySQL.append(" from ticket_indent ti2 ");
			querySQL.append(" left join base_tran_rec btr ");
			querySQL.append(" on ti2.indent_id = btr.indent_id ");
			querySQL.append(" where btr.payment_type = '02' ");
			querySQL.append(" and btr.pay_type = '01') ");
		}
		if(t.getUserCode()!=null && !t.getUserCode().isEmpty()){
			querySQL.append(" and ti.inputer in (select su.user_id ");
			querySQL.append(" from sys_user su ");
			querySQL.append(" where su.user_code  like :user_code) ");
		}
		if(t.getTravelAgencyName()!=null && !t.getTravelAgencyName().isEmpty()){
			querySQL.append(" and ti.inputer in (select su.user_id ");
			querySQL.append(" from sys_user su,Base_Travel_Agency bta ");
			querySQL.append(" where su.travel_Agency_Id = bta.travel_Agency_Id ");
			querySQL.append(" and bta.travel_Agency_Name  like :travel_Agency_Name) ");
		}
		if(t.getTravelAgencyType()!=null && !t.getTravelAgencyType().isEmpty()){
			querySQL.append(" and ti.inputer in (select su.user_id ");
			querySQL.append(" from sys_user su,Base_Travel_Agency bta ");
			querySQL.append(" where su.travel_Agency_Id = bta.travel_Agency_Id ");
			querySQL.append(" and bta.IS_AGREEMENT  like :travel_Agency_Type) ");
		}
		if(t.getCustomerType()!=null && !t.getCustomerType().isEmpty()){
			if("-1".equals(t.getCustomerType())){//自助购票
				querySQL.append(" and ti.pay_Type like :customerType ");
			}else{
				querySQL.append(" and ti.customer_Type like :customerType ");
			}
		}
		if(t.getStatus()!=null && !t.getStatus().isEmpty()){
			if("indentFind_03".equals(forPage) && "03".equals(t.getStatus())){
				querySQL.append(" and (ti.status = '02' or ti.status = '03') ");
			}else{
				querySQL.append(" and ti.status like :status ");
			}
		}
		if("rForMKT".equals(forPage)){
			querySQL.append(" and ti.status not in ('07') ");
		}
		if(t.getDiscount()!=null && !t.getDiscount().isEmpty()){
			querySQL.append(" and ti.discount like :discount ");
		}
		if(t.getIndentCode()!=null && !t.getIndentCode().trim().isEmpty()){
			querySQL.append(" and ti.indent_Code like :indentCode ");
		}
		
		if(t.getPhoneNumber()!=null && !t.getPhoneNumber().trim().isEmpty()){
			querySQL.append(" and ti.phone_Number like :phoneNumber ");
		}
		
		if(t.getIdCardNumber()!=null && !t.getIdCardNumber().trim().isEmpty()){
			querySQL.append(" and ti.id_Card_Number like :idCardNumber ");
		}
		
		if(t.getStartDate()!=null){
			querySQL.append(" and ti.input_Time >= :startDate ");
		}
		
		if(t.getEndDate()!=null){
			querySQL.append(" and ti.input_Time <= :endDate ");
		}
		
		if(t.getIsOutbound()!=null && !t.getIsOutbound().trim().isEmpty()){
			if(t.getIsOutbound().equals("00")){
				querySQL.append(" and (ti.is_outbound = :isOutbound or ti.is_outbound is null) ");
			}
			else{
				querySQL.append(" and ti.is_outbound = :isOutbound ");
			}
		}
		
		if(t.getUserStartDate()!=null){
			String sDate = CommonMethod.timeToString(t.getUserStartDate());
			querySQL.append(" and ti.use_Date >= TO_DATE('"+sDate+" 00:00:00','yyyy-mm-dd hh24:mi:ss') ");
		}
		
		if(t.getUserEndDate()!=null){
			String eDate = CommonMethod.timeToString(t.getUserEndDate());
			querySQL.append(" and ti.use_Date <= TO_DATE('"+eDate+" 23:59:59','yyyy-mm-dd hh24:mi:ss') ");
		}
		if(t.getUseDate()!=null){
			String sDate = CommonMethod.timeToString(Timestamp.valueOf(t.getUseDate().toString()));
			String eDate = CommonMethod.timeToString(Timestamp.valueOf(t.getUseDate().toString()));
			querySQL.append(" and ti.use_Date >= TO_DATE('"+sDate+" 00:00:00','yyyy-mm-dd hh24:mi:ss') ");
			querySQL.append(" and ti.use_Date <= TO_DATE('"+eDate+" 23:59:59','yyyy-mm-dd hh24:mi:ss') ");
		}
		
		if(t.getPayType()!=null && !t.getPayType().isEmpty()){
			if("mobile".equals(t.getPayType())){//所有移动端购票
				querySQL.append(" and ti.pay_Type in ('05','06') ");
			}else if("00".equals(t.getPayType())){//平台购票
				querySQL.append(" and ti.pay_Type not in ('05','06','03') ");
			}else{
				querySQL.append(" and ti.pay_Type in ('"+t.getPayType()+"') ");
			}
		}
		if(t.getIndentType()!=null && !t.getIndentType().isEmpty()){
			querySQL.append(" and ti.indent_Type = '"+t.getIndentType()+"'");
		}
	}
	
	private void initCondition(Query query ,final TicketIndent t,final long userId,final String forPage){
		if("queryAll".equals(forPage)){
			
		}else if("webService".equals(forPage)){
			
		}
		else if(!"checkPage".equals(forPage)&&!"refundPage".equals(forPage)&&!"returnScbPage".equals(forPage)){
			query.setLong("inputer", userId);
		}
		
		if(t.getUserCode()!=null && !t.getUserCode().isEmpty()){
			query.setString("user_code", "%"+t.getUserCode()+"%");
		}
		if(t.getTravelAgencyName()!=null && !t.getTravelAgencyName().isEmpty()){
			query.setString("travel_Agency_Name", "%"+t.getTravelAgencyName()+"%");
		}
		if(t.getTravelAgencyType()!=null && !t.getTravelAgencyType().isEmpty()){
			query.setString("travel_Agency_Type", t.getTravelAgencyType());
		}
		if(t.getCustomerType()!=null && !t.getCustomerType().isEmpty()){
			if("-1".equals(t.getCustomerType())){//自助购票
				query.setString("customerType", "03");
			}else{
				query.setString("customerType", t.getCustomerType());
			}
		}
		if(t.getStatus()!=null && !t.getStatus().trim().isEmpty()){
			if("indentFind_03".equals(forPage) && "03".equals(t.getStatus())){
			}else{
				query.setString("status", t.getStatus());
			}
		}
		if(t.getDiscount()!=null && !t.getDiscount().isEmpty()){
			query.setString("discount", t.getDiscount());
		}
		if(t.getIndentCode()!=null && !t.getIndentCode().trim().isEmpty()){
			query.setString("indentCode", "%"+t.getIndentCode().trim()+"%");
		}
		
		if(t.getPhoneNumber()!=null && !t.getPhoneNumber().trim().isEmpty()){
			query.setString("phoneNumber", "%"+t.getPhoneNumber().trim()+"%");
		}
		
		if(t.getIdCardNumber()!=null && !t.getIdCardNumber().trim().isEmpty()){
			query.setString("idCardNumber", "%"+t.getIdCardNumber().trim()+"%");
		}
		
		if(t.getStartDate()!=null){
			query.setTimestamp("startDate", CommonMethod.toStartTime(t.getStartDate()));
		}
		
		if(t.getEndDate()!=null){
			query.setTimestamp("endDate", CommonMethod.toEndTime(t.getEndDate()));
		}
		
		if(t.getIsOutbound()!=null && !t.getIsOutbound().trim().isEmpty()){
			query.setString("isOutbound", t.getIsOutbound());
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String,Double> findIndentSums(final TicketIndent t,final long userId,final JsonPager<TicketIndent> jp,final String forPage) {
		Session session = sessionFactory.getCurrentSession();
		StringBuffer querySQL = new StringBuffer();
		
		querySQL.append(" select sum(ti.TOTAL_AMOUNT) as nums,sum(ti.TOTAL_PRICE) as sums ");
		querySQL.append(" from TICKET_INDENT ti ");
//						",TICKET_INDENT_DETAIL tid"+
		querySQL.append(" where 1=1 ");
		
		initConditionSQL( querySQL , t, userId, forPage);
//						if("queryAll".equals(forPage)){
//					
//						}
//						else if(!"checkPage".equals(forPage)){
//							sql = sql+" and ti.inputer = :inputer";
//						}
//						if("returnPage".equals(forPage)){
//							sql = sql+" and (ti.status = '02' or ti.status = '03')";
//						}
//						else if("refundPage".equals(forPage)){//查询可退现金的票订单
//							sql = sql+" and (ti.status = '04' or ti.status = '05' or ti.status = '06')";
//							sql = sql+" and ti.indent_id in (select ti1.indent_id"+
//			                          " from ticket_indent ti1"+
//			                          " left join base_tran_rec btr"+
//			                          " on ti1.indent_id = btr.indent_id"+
//			                          " where btr.payment_type = '02'"+
//			                          " and btr.pay_type = '02')"+
//			                          " and ti.indent_id in (select ti1.indent_id"+
//			                          " from ticket_indent ti1"+
//			                          " left join base_tran_rec btr"+
//			                          " on ti1.indent_id = btr.indent_id"+
//			                          " where btr.payment_type = '01'"+
//			                          " and btr.pay_type = '01')"+
//			                          " and ti.indent_id not in (select ti2.indent_id"+
//		                              " from ticket_indent ti2"+
//		                              " left join base_tran_rec btr"+
//		                              " on ti2.indent_id = btr.indent_id"+
//		                              " where btr.payment_type = '02'"+
//		                              " and btr.pay_type = '01')";
//						}
//						if(t.getTravelAgencyType()!=null && !t.getTravelAgencyType().isEmpty()){
//							sql = sql+" and ti.inputer in (select su.user_id " +
//									"from sys_user su,Base_Travel_Agency bta " +
//									"where su.travel_Agency_Id = bta.travel_Agency_Id " +
//									"and bta.travel_Agency_Type  like :travel_Agency_Type)";
//						}
//						if(t.getCustomerType()!=null && !t.getCustomerType().isEmpty()){
//							sql = sql+" and ti.customer_Type like :customerType";
//						}
//						if(t.getStatus()!=null && !t.getStatus().isEmpty()){
//							if("indentFind_03".equals(forPage) && "02".equals(t.getStatus())){
//								sql = sql+" and (ti.status = '02' or ti.status = '03')";
//							}else{
//								sql = sql+" and ti.status like :status ";
//							}
//						}
//						if(t.getDiscount()!=null && !t.getDiscount().isEmpty()){
//							sql = sql+" and ti.discount like :discount ";
//						}
//						if(t.getIndentCode()!=null && !t.getIndentCode().trim().isEmpty()){
//							sql = sql+" and ti.indent_Code like :indentCode";
//						}
//						
//						if(t.getPhoneNumber()!=null && !t.getPhoneNumber().trim().isEmpty()){
//							sql = sql+" and ti.phone_Number like :phoneNumber";
//						}
//						
//						if(t.getIdCardNumber()!=null && !t.getIdCardNumber().trim().isEmpty()){
//							sql = sql+" and ti.id_Card_Number like :idCardNumber";
//						}
//						
//						if(t.getStartDate()!=null){
//							sql = sql+" and ti.input_Time >= :startDate";
//						}
//						
//						if(t.getEndDate()!=null){
//							sql = sql+" and ti.input_Time <= :endDate";
//						}
//				
//						if(t.getUseDate()!=null){
//							String sDate = CommonMethod.timeToString(t.getUseDate());
//							String eDate = CommonMethod.timeToString(t.getUseDate());
//							sql = sql+" and ti.use_Date >= TO_DATE('"+sDate+" 00:00:00','yyyy-mm-dd hh24:mi:ss') ";
//							sql = sql+" and ti.use_Date <= TO_DATE('"+eDate+" 23:59:59','yyyy-mm-dd hh24:mi:ss')";
//						}
				
		Query query = session.createSQLQuery(querySQL.toString());
		initCondition(query,t,userId,forPage);
//				if("queryAll".equals(forPage)){
//					
//				}
//				else if(!"checkPage".equals(forPage)){
//					query.setLong("inputer", userId);
//				}
//				
//				if(t.getTravelAgencyType()!=null && !t.getTravelAgencyType().isEmpty()){
//					query.setString("travel_Agency_Type", t.getTravelAgencyType());
//				}
//				if(t.getCustomerType()!=null && !t.getCustomerType().isEmpty()){
//					query.setString("customerType", t.getCustomerType());
//				}
//				if(t.getStatus()!=null && !t.getStatus().trim().isEmpty()){
//					if("indentFind_03".equals(forPage) && "02".equals(t.getStatus())){
//					}else{
//						query.setString("status", t.getStatus());
//					}
//				}
//				if(t.getDiscount()!=null && !t.getDiscount().isEmpty()){
//					query.setString("discount", t.getDiscount());
//				}
//				if(t.getIndentCode()!=null && !t.getIndentCode().trim().isEmpty()){
//					query.setString("indentCode", "%"+t.getIndentCode().trim()+"%");
//				}
//				
//				if(t.getPhoneNumber()!=null && !t.getPhoneNumber().trim().isEmpty()){
//					query.setString("phoneNumber", "%"+t.getPhoneNumber().trim()+"%");
//				}
//				
//				if(t.getIdCardNumber()!=null && !t.getIdCardNumber().trim().isEmpty()){
//					query.setString("idCardNumber", "%"+t.getIdCardNumber().trim()+"%");
//				}
//				
//				if(t.getStartDate()!=null){
//					query.setTimestamp("startDate", CommonMethod.toStartTime(t.getStartDate()));
//				}
//				
//				if(t.getEndDate()!=null){
//					query.setTimestamp("endDate", CommonMethod.toEndTime(t.getEndDate()));
//				}
		
		Map<String,Double> teMap = new HashMap<String,Double>();
		List l = query.list();
		
		for(Object r : l){
			
			Object[] temp = (Object[])r;
			if(temp[0]!=null){
				teMap.put("nums", Double.valueOf(temp[0].toString()));
			}
			if(temp[1]!=null){
				teMap.put("sums", Double.valueOf(temp[1].toString()));
			}
			
		}
		
		return teMap;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public int findCountByUsedate(final String useDate) {
		Session session = sessionFactory.getCurrentSession();
		StringBuffer sql = new StringBuffer();
		
		sql.append(" select sum(tid.amount)");
		sql.append(" from ticket_indent_detail  tid");
		sql.append(" inner join  ticket_indent ti on tid.indent_id = ti.indent_id");
		sql.append(" inner join ticket_price tp on tid.ticket_price_id = tp.ticket_price_id");
		sql.append(" inner join ticket_type tt on tp.ticket_id = tt.ticket_id");
		sql.append(" where ti.status in ('02','03','04','05')");
		sql.append(" and tt.ticket_big_type = '01'");
		sql.append(" and to_char(ti.use_date,'yyyy-MM-dd') = :useDate");
		
		Query query = session.createSQLQuery(sql.toString());
		query.setString("useDate", useDate);
		List list = query.list();
		int total =0;
		if(list.get(0)!=null){
			total = Integer.valueOf(list.get(0).toString());
		}
		
		return total;
	}
}