package dao.ticket.ticketIndent;
import java.util.List;
import java.util.Map;

import com.xlem.dao.TicketIndent;

import common.action.JsonPager;
import common.dao.BaseDao;

public interface TicketIndentDao extends BaseDao<TicketIndent> {
	
	/**
	 * 
	 * @param t
	 * @param userId
	 * @param jp
	 * @param forPage 查询页面，“returnPage”时之查询status为‘02’和‘03’的记录
	 * @return
	 */
	public List<TicketIndent> findIndentList(TicketIndent t,long userId,JsonPager<TicketIndent> jp,String forPage);
	public int findIndentListCount(TicketIndent t,long userId,JsonPager<TicketIndent> jp,String forPage);
	
	/**
	 * 查询总数量和总金额
	 * @param t
	 * @param userId
	 * @param jp
	 * @param forPage
	 * @return
	 */
	public Map<String,Double> findIndentSums(final TicketIndent t,final long userId,final JsonPager<TicketIndent> jp,final String forPage);
	
	public JsonPager<TicketIndent> findIndent(TicketIndent t, long userId,
			JsonPager<TicketIndent> jsonPager, String forPage);
	
	
	/**
	 * 查询当日购票总数量
	 * @param useDate
	 * @return
	 */
	public int findCountByUsedate(String useDate);
	
}