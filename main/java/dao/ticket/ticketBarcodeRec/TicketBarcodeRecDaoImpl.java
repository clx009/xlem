package dao.ticket.ticketBarcodeRec;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.ConsumeTicketInfo;
import com.xlem.dao.TicketAccounts;
import com.xlem.dao.TicketBarcodeRec;

import common.CommonMethod;
import common.action.JsonPager;
import common.dao.BaseDaoImpl;

@Transactional
@Repository("ticketBarcodeRecDao")
public class TicketBarcodeRecDaoImpl extends BaseDaoImpl<TicketBarcodeRec> implements TicketBarcodeRecDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<TicketAccounts> findGridByAccounts() {
		Session session = sessionFactory.getCurrentSession();
		StringBuffer sql = new StringBuffer("select nvl(w.ticket_name,x.ticket_name) as ticket_name,");
		sql.append(" nvl(w.amountGroup,0) as amountGroup,nvl(w.subtotalGroup,0) as subtotalGroup,nvl(w.payFeeGroup,0) as payFeeGroup,");
		sql.append(" nvl(w.amount,0) as amount,nvl(w.subtotal,0) as subtotal,nvl(w.payFee,0) as payFee,");
		sql.append(" nvl(x.amount,0) as amountLocale,nvl(x.subtotal,0) as subtotalLocale,nvl(x.payFee,0) as payFeeLocale,");
		sql.append(" nvl(w.discount,x.discount),nvl(w.status,x.status) as status from ");
		sql.append(" (select nvl(t.ticket_name,s.ticket_name) as ticket_name,");
		sql.append(" nvl(t.amount,0) as amountGroup,nvl(t.subtotal,0) as subtotalGroup,nvl(t.payFee,0) as payFeeGroup,");
		sql.append(" nvl(s.amount,0) as amount,nvl(s.subtotal,0) as subtotal,nvl(s.payFee,0) as payFee,");
		sql.append(" nvl(t.discount,s.discount) as discount,nvl(t.special_type,s.special_type) as special_type,nvl(t.status,s.status) as status from ");
		sql.append(" (select tt.ticket_name,count(tbr.barcode_rec_id) as amount,sum(tid.unit_price) as subtotal,");
		sql.append(" sum(tid.unit_price*tid.per_fee) as payFee, nvl(ti.discount,'10') as discount, tt.special_type,tbr.status");
		sql.append(" from TICKET_BARCODE_REC tbr,TICKET_INDENT_DETAIL tid,TICKET_INDENT ti,TICKET_PRICE tp,TICKET_TYPE tt");
		sql.append(" where tbr.status in('01','03') and tbr.is_check = '00'");
		sql.append(" and tbr.indent_detail_id=tid.indent_detail_id and tid.indent_id = ti.indent_id");
		sql.append(" and tid.ticket_price_id = tp.ticket_price_id and tp.ticket_id=tt.ticket_id");
		sql.append(" and ti.customer_type = '01' and ti.pay_type = '02'");
		sql.append(" group by tt.ticket_name,tt.ticket_id,nvl(ti.discount,'10'),tt.special_type,tbr.status) t");
		sql.append(" full join");
		sql.append(" (select tt.ticket_name,count(tbr.barcode_rec_id) as amount,sum(tid.unit_price) as subtotal,");
		sql.append(" sum(tid.unit_price*tid.per_fee) as payFee, nvl(ti.discount,'10') as discount, tt.special_type,tbr.status");
		sql.append(" from TICKET_BARCODE_REC tbr,TICKET_INDENT_DETAIL tid,TICKET_INDENT ti,TICKET_PRICE tp,TICKET_TYPE tt");
		sql.append(" where tbr.status in('01','03') and tbr.is_check = '00'");
		sql.append(" and tbr.indent_detail_id=tid.indent_detail_id and tid.indent_id = ti.indent_id");
		sql.append(" and tid.ticket_price_id = tp.ticket_price_id and tp.ticket_id=tt.ticket_id");
		sql.append(" and ti.customer_type = '02' and ti.pay_type = '02'");
		sql.append(" group by tt.ticket_name,tt.ticket_id,nvl(ti.discount,'10'), tt.special_type,tbr.status) s");
		sql.append(" on t.ticket_name = s.ticket_name and t.discount = s.discount and t.status = s.status) w");
		sql.append(" full join");
		sql.append(" (select tt.ticket_name,count(tbr.barcode_rec_id) as amount,sum(tid.unit_price) as subtotal,");
		sql.append(" sum(tid.unit_price*tid.per_fee) as payFee,nvl(ti.discount,'10') as discount, tt.special_type,tbr.status");
		sql.append(" from TICKET_BARCODE_REC tbr,TICKET_INDENT_DETAIL tid,TICKET_INDENT ti,TICKET_PRICE tp,TICKET_TYPE tt");
		sql.append(" where tbr.status in('01','03') and tbr.is_check = '00'");
		sql.append(" and tbr.indent_detail_id=tid.indent_detail_id and tid.indent_id = ti.indent_id");
		sql.append(" and tid.ticket_price_id = tp.ticket_price_id and tp.ticket_id=tt.ticket_id");
		sql.append(" and tbr.CHECKING_MACHINE_NUMBER in ('017','018','019','020')");
		sql.append(" group by tt.ticket_name,tt.ticket_id,nvl(ti.discount,'10'),tt.special_type,tbr.status) x");
		sql.append(" on w.ticket_name = x.ticket_name and w.discount = x.discount and w.status = x.status ");
		sql.append(" order by nvl(w.discount,x.discount) desc,nvl(w.special_type,x.special_type),nvl(w.status,x.status) ");
			
		Query query = session.createSQLQuery(sql.toString());
//				System.out.println(sql.toString());
		@SuppressWarnings("rawtypes")
		List l = query.list();
		
		List<TicketAccounts> list = new ArrayList<TicketAccounts>();
		
		for(Object r : l){
			
			Object[] temp = (Object[])r;
			
			TicketAccounts ta = new TicketAccounts();

			if(temp[0]!=null){
				ta.setTicketName(temp[0].toString());
			}
			if(temp[1]!=null){
				ta.setAmountGroup(Integer.valueOf(temp[1].toString()));
			}
			if(temp[2]!=null){
				ta.setSubtotalGroup(Double.valueOf(temp[2].toString()));
			}
			if(temp[3]!=null){
				ta.setPayFeeGroup(Double.valueOf(temp[3].toString()));
			}
			if(temp[4]!=null){
				ta.setAmount(Integer.valueOf(temp[4].toString()));
			}
			if(temp[5]!=null){
				ta.setSubtotal(Double.valueOf(temp[5].toString()));
			}
			if(temp[6]!=null){
				ta.setPayFee(Double.valueOf(temp[6].toString()));
			}
			if(temp[7]!=null){
				ta.setAmountLocale(Integer.valueOf(temp[7].toString()));
			}
			if(temp[8]!=null){
				ta.setSubtotalLocale(Double.valueOf(temp[8].toString()));
			}
			if(temp[9]!=null){
				ta.setPayFeeLocale(Double.valueOf(temp[9].toString()));
			}
			if(temp[10]!=null){
				ta.setDiscount(temp[10].toString());
			}
			if(temp[11]!=null){
				ta.setStatus(temp[11].toString());
			}
			list.add(ta);
		}
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TicketAccounts> findGridBySubAccounts() {
		Session session = sessionFactory.getCurrentSession();
		StringBuffer sql = new StringBuffer("select nvl(w.ticket_name,x.ticket_name) as ticket_name,");
		sql.append(" nvl(w.amountGroup,0) as amountGroup,nvl(w.subtotalGroup,0) as subtotalGroup,nvl(w.subtotalGroupE,0) as subtotalGroupE,nvl(w.subtotalGroupR,0) as subtotalGroupR,nvl(w.payFeeGroup,0) as payFeeGroup,nvl(w.payFeeGroupE,0) as payFeeGroupE,nvl(w.payFeeGroupR,0) as payFeeGroupR,");
		sql.append(" nvl(w.amountGroupD,0) as amountGroupD,nvl(w.subtotalGroupD,0) as subtotalGroupD,nvl(w.subtotalGroupDE,0) as subtotalGroupDE,nvl(w.subtotalGroupDR,0) as subtotalGroupDR,nvl(w.payFeeGroupD,0) as payFeeGroupD,nvl(w.payFeeGroupDE,0) as payFeeGroupDE,nvl(w.payFeeGroupDR,0) as payFeeGroupDR,");
		sql.append(" nvl(w.amount,0) as amount,nvl(w.subtotal,0) as subtotal,nvl(w.subtotalE,0) as subtotalE,nvl(w.subtotalR,0) as subtotalR,nvl(w.payFee,0) as payFee,nvl(w.payFeeE,0) as payFeeE,nvl(w.payFeeR,0) as payFeeR,");
		sql.append(" nvl(x.amount,0) as amountLocale,nvl(x.subtotal,0) as subtotalLocale,nvl(x.subtotalE,0) as subtotalLocaleE,nvl(x.subtotalR,0) as subtotalLocaleR,nvl(x.payFee,0) as payFeeLocale,nvl(x.payFeeE,0) as payFeeLocaleE,nvl(x.payFeeR,0) as payFeeLocaleR,");
		sql.append(" nvl(w.eCompany,x.eCompany) as eCompany,nvl(w.rCompany,x.rCompany) as rCompany,");
		sql.append(" nvl(w.discount,x.discount) as discount,nvl(w.status,x.status) as status from ");
		
		sql.append(" (select nvl(gr.ticket_name,s.ticket_name) as ticket_name,");
		sql.append(" nvl(gr.amountGroup,0) as amountGroup,nvl(gr.subtotalGroup,0) as subtotalGroup,nvl(gr.subtotalGroupE,0) as subtotalGroupE,nvl(gr.subtotalGroupR,0) as subtotalGroupR,nvl(gr.payFeeGroup,0) as payFeeGroup,nvl(gr.payFeeGroupE,0) as payFeeGroupE,nvl(gr.payFeeGroupR,0) as payFeeGroupR,");
		sql.append(" nvl(gr.amountGroupD,0) as amountGroupD,nvl(gr.subtotalGroupD,0) as subtotalGroupD,nvl(gr.subtotalGroupDE,0) as subtotalGroupDE,nvl(gr.subtotalGroupDR,0) as subtotalGroupDR,nvl(gr.payFeeGroupD,0) as payFeeGroupD,nvl(gr.payFeeGroupDE,0) as payFeeGroupDE,nvl(gr.payFeeGroupDR,0) as payFeeGroupDR,");
		sql.append(" nvl(s.amount,0) as amount,nvl(s.subtotal,0) as subtotal,nvl(s.subtotalE,0) as subtotalE,nvl(s.subtotalR,0) as subtotalR,nvl(s.payFee,0) as payFee,nvl(s.payFeeE,0) as payFeeE,nvl(s.payFeeR,0) as payFeeR,");
		sql.append(" nvl(gr.eCompany,s.eCompany) as eCompany,nvl(gr.rCompany,s.rCompany) as rCompany,");
		sql.append(" nvl(gr.discount,s.discount) as discount,nvl(gr.special_type,s.special_type) as special_type,nvl(gr.status,s.status) as status from ");
		
		sql.append(" (select nvl(t.ticket_name,ts.ticket_name) as ticket_name,");
		sql.append(" nvl(t.amount,0) as amountGroup,nvl(t.subtotal,0) as subtotalGroup,nvl(t.subtotalE,0) as subtotalGroupE,nvl(t.subtotalR,0) as subtotalGroupR,nvl(t.payFee,0) as payFeeGroup,nvl(t.payFeeE,0) as payFeeGroupE,nvl(t.payFeeR,0) as payFeeGroupR,");
		sql.append(" nvl(ts.amount,0) as amountGroupD,nvl(ts.subtotal,0) as subtotalGroupD,nvl(ts.subtotalE,0) as subtotalGroupDE,nvl(ts.subtotalR,0) as subtotalGroupDR,nvl(ts.payFee,0) as payFeeGroupD,nvl(ts.payFeeE,0) as payFeeGroupDE,nvl(ts.payFeeR,0) as payFeeGroupDR,");
		sql.append(" nvl(t.eCompany,ts.eCompany) as eCompany,nvl(t.rCompany,ts.rCompany) as rCompany,");
		sql.append(" nvl(t.discount,ts.discount) as discount,nvl(t.special_type,ts.special_type) as special_type,nvl(t.status,ts.status) as status from ");
		sql.append(" (select tt.ticket_name,count(tbr.barcode_rec_id) as amount,");
		sql.append(" sum(tid.unit_price) as subtotal,sum(tid.entrance_unit_price) as subtotalE,sum(tid.ropeway_unit_price) as subtotalR,");
		sql.append(" sum(tid.unit_price*tid.per_fee) as payFee,sum(tid.entrance_unit_price*tid.per_fee) as payFeeE,sum(tid.ropeway_unit_price*tid.per_fee) as payFeeR,");
		sql.append(" tid.entrance_company as eCompany,tid.ropeway_company as rCompany,");
		sql.append(" nvl(ti.discount,'10') as discount, tt.special_type,tbr.status");
		sql.append(" from TICKET_BARCODE_REC tbr,TICKET_INDENT_DETAIL tid,TICKET_INDENT ti,TICKET_PRICE tp,TICKET_TYPE tt");
		sql.append(" where tbr.status in('01','03') and tbr.is_check = '00'");
		sql.append(" and tbr.indent_detail_id=tid.indent_detail_id and tid.indent_id = ti.indent_id");
		sql.append(" and tid.ticket_price_id = tp.ticket_price_id and tp.ticket_id=tt.ticket_id");
		sql.append(" and ti.customer_type = '01' and ti.pay_type = '02' and (ti.tel_number <> '1' or ti.tel_number is null )");
		sql.append(" group by tt.ticket_name,tt.ticket_id,nvl(ti.discount,'10'),tt.special_type,tbr.status,tid.entrance_company,tid.ropeway_company) t");
		sql.append(" full join");
		sql.append(" (select tt.ticket_name,count(tbr.barcode_rec_id) as amount,");
		sql.append(" sum(tid.unit_price) as subtotal,sum(tid.entrance_unit_price) as subtotalE,sum(tid.ropeway_unit_price) as subtotalR,");
		sql.append(" sum(tid.unit_price*tid.per_fee) as payFee,sum(tid.entrance_unit_price*tid.per_fee) as payFeeE,sum(tid.ropeway_unit_price*tid.per_fee) as payFeeR,");
		sql.append(" tid.entrance_company as eCompany,tid.ropeway_company as rCompany,");
		sql.append(" nvl(ti.discount,'10') as discount, tt.special_type,tbr.status");
		sql.append(" from TICKET_BARCODE_REC tbr,TICKET_INDENT_DETAIL tid,TICKET_INDENT ti,TICKET_PRICE tp,TICKET_TYPE tt");
		sql.append(" where tbr.status in('01','03') and tbr.is_check = '00'");
		sql.append(" and tbr.indent_detail_id=tid.indent_detail_id and tid.indent_id = ti.indent_id");
		sql.append(" and tid.ticket_price_id = tp.ticket_price_id and tp.ticket_id=tt.ticket_id");
		sql.append(" and ti.customer_type = '01' and ti.pay_type = '02' and ti.tel_number = '1' ");
		sql.append(" group by tt.ticket_name,tt.ticket_id,nvl(ti.discount,'10'),tt.special_type,tbr.status,tid.entrance_company,tid.ropeway_company) ts");
		sql.append(" on t.ticket_name = ts.ticket_name and t.discount = ts.discount and t.status = ts.status and t.eCompany = ts.eCompany and t.rCompany = ts.rCompany) gr ");
		
		sql.append(" full join");
		sql.append(" (select tt.ticket_name,count(tbr.barcode_rec_id) as amount,");
		sql.append(" sum(tid.unit_price) as subtotal,sum(tid.entrance_unit_price) as subtotalE,sum(tid.ropeway_unit_price) as subtotalR,");
		sql.append(" sum(tid.unit_price*tid.per_fee) as payFee,sum(tid.entrance_unit_price*tid.per_fee) as payFeeE,sum(tid.ropeway_unit_price*tid.per_fee) as payFeeR,");
		sql.append(" tid.entrance_company as eCompany,tid.ropeway_company as rCompany,");
		sql.append(" nvl(ti.discount,'10') as discount, tt.special_type,tbr.status");
		sql.append(" from TICKET_BARCODE_REC tbr,TICKET_INDENT_DETAIL tid,TICKET_INDENT ti,TICKET_PRICE tp,TICKET_TYPE tt");
		sql.append(" where tbr.status in('01','03') and tbr.is_check = '00'");
		sql.append(" and tbr.indent_detail_id=tid.indent_detail_id and tid.indent_id = ti.indent_id");
		sql.append(" and tid.ticket_price_id = tp.ticket_price_id and tp.ticket_id=tt.ticket_id");
		sql.append(" and ti.customer_type = '02' and ti.pay_type = '02'");
		sql.append(" group by tt.ticket_name,tt.ticket_id,nvl(ti.discount,'10'), tt.special_type,tbr.status,tid.entrance_company,tid.ropeway_company) s");
		sql.append(" on gr.ticket_name = s.ticket_name and gr.discount = s.discount and gr.status = s.status and gr.eCompany = s.eCompany and gr.rCompany = s.rCompany) w");
		
		sql.append(" full join");
		sql.append(" (select tt.ticket_name,count(tbr.barcode_rec_id) as amount,");
		sql.append(" sum(tid.unit_price) as subtotal,sum(tid.entrance_unit_price) as subtotalE,sum(tid.ropeway_unit_price) as subtotalR,");
		sql.append(" sum(tid.unit_price*tid.per_fee) as payFee,sum(tid.entrance_unit_price*tid.per_fee) as payFeeE,sum(tid.ropeway_unit_price*tid.per_fee) as payFeeR,");
		sql.append(" tid.entrance_company as eCompany,tid.ropeway_company as rCompany,");
		sql.append(" nvl(ti.discount,'10') as discount, tt.special_type,tbr.status");
		sql.append(" from TICKET_BARCODE_REC tbr,TICKET_INDENT_DETAIL tid,TICKET_INDENT ti,TICKET_PRICE tp,TICKET_TYPE tt");
		sql.append(" where tbr.status in('01','03') and tbr.is_check = '00'");
		sql.append(" and tbr.indent_detail_id=tid.indent_detail_id and tid.indent_id = ti.indent_id");
		sql.append(" and tid.ticket_price_id = tp.ticket_price_id and tp.ticket_id=tt.ticket_id");
		sql.append(" and tbr.CHECKING_MACHINE_NUMBER in ('017','018','019','020')");
		sql.append(" group by tt.ticket_name,tt.ticket_id,nvl(ti.discount,'10'),tt.special_type,tbr.status,tid.entrance_company,tid.ropeway_company) x");
		sql.append(" on w.ticket_name = x.ticket_name and w.discount = x.discount and w.status = x.status and w.eCompany = x.eCompany and w.rCompany = x.rCompany");
		sql.append(" order by nvl(w.discount,x.discount) desc,nvl(w.special_type,x.special_type),nvl(w.status,x.status) ");
			
		Query query = session.createSQLQuery(sql.toString());
	//				System.out.println(sql.toString());
		@SuppressWarnings("rawtypes")
		List l = query.list();
		
		List<TicketAccounts> list = new ArrayList<TicketAccounts>();
		
		for(Object r : l){
			
			Object[] temp = (Object[])r;
			TicketAccounts ta = new TicketAccounts();
	
			if(temp[0]!=null){
				ta.setTicketName(temp[0].toString());
			}
			if(temp[1]!=null){
				ta.setAmountGroup(Integer.valueOf(temp[1].toString()));
			}
			if(temp[2]!=null){
				ta.setSubtotalGroup(Double.valueOf(temp[2].toString()));
			}
			if(temp[3]!=null){
				ta.setSubtotalGroupE(Double.valueOf(temp[3].toString()));
			}
			if(temp[4]!=null){
				ta.setSubtotalGroupR(Double.valueOf(temp[4].toString()));
			}
			if(temp[5]!=null){
				ta.setPayFeeGroup(Double.valueOf(temp[5].toString()));
			}
			if(temp[6]!=null){
				ta.setPayFeeGroupE(Double.valueOf(temp[6].toString()));
			}
			if(temp[7]!=null){
				ta.setPayFeeGroupR(Double.valueOf(temp[7].toString()));
			}
			if(temp[8]!=null){
				ta.setAmountGroupD(Integer.valueOf(temp[8].toString()));
			}
			if(temp[9]!=null){
				ta.setSubtotalGroupD(Double.valueOf(temp[9].toString()));
			}
			if(temp[10]!=null){
				ta.setSubtotalGroupDE(Double.valueOf(temp[10].toString()));
			}
			if(temp[11]!=null){
				ta.setSubtotalGroupDR(Double.valueOf(temp[11].toString()));
			}
			if(temp[12]!=null){
				ta.setPayFeeGroupD(Double.valueOf(temp[12].toString()));
			}
			if(temp[13]!=null){
				ta.setPayFeeGroupDE(Double.valueOf(temp[13].toString()));
			}
			if(temp[14]!=null){
				ta.setPayFeeGroupDR(Double.valueOf(temp[14].toString()));
			}
			if(temp[15]!=null){
				ta.setAmount(Integer.valueOf(temp[15].toString()));
			}
			if(temp[16]!=null){
				ta.setSubtotal(Double.valueOf(temp[16].toString()));
			}
			if(temp[17]!=null){
				ta.setSubtotalE(Double.valueOf(temp[17].toString()));
			}
			if(temp[18]!=null){
				ta.setSubtotalR(Double.valueOf(temp[18].toString()));
			}
			if(temp[19]!=null){
				ta.setPayFee(Double.valueOf(temp[19].toString()));
			}
			if(temp[20]!=null){
				ta.setPayFeeE(Double.valueOf(temp[20].toString()));
			}
			if(temp[21]!=null){
				ta.setPayFeeR(Double.valueOf(temp[21].toString()));
			}
			if(temp[22]!=null){
				ta.setAmountLocale(Integer.valueOf(temp[22].toString()));
			}
			if(temp[23]!=null){
				ta.setSubtotalLocale(Double.valueOf(temp[23].toString()));
			}
			if(temp[24]!=null){
				ta.setSubtotalLocaleE(Double.valueOf(temp[24].toString()));
			}
			if(temp[25]!=null){
				ta.setSubtotalLocaleR(Double.valueOf(temp[25].toString()));
			}
			if(temp[26]!=null){
				ta.setPayFeeLocale(Double.valueOf(temp[26].toString()));
			}
			if(temp[27]!=null){
				ta.setPayFeeLocaleE(Double.valueOf(temp[27].toString()));
			}
			if(temp[28]!=null){
				ta.setPayFeeLocaleR(Double.valueOf(temp[28].toString()));
			}
			if(temp[29]!=null){
				ta.seteCompany(temp[29].toString());
			}
			if(temp[30]!=null){
				ta.setrCompany(temp[30].toString());
			}
			if(temp[31]!=null){
				ta.setDiscount(temp[31].toString());
			}
			if(temp[32]!=null){
				ta.setStatus(temp[32].toString());
			}
			list.add(ta);
		}
		
		return list;
	}
	
	@Override
	public void update(final long checkRecId,final long userId) {
		Session session = sessionFactory.getCurrentSession();
		String saveSql = "insert into TICKET_CHECK_REC(CHECK_REC_ID,CHECK_USER_ID,CHECK_TIME) values(:checkRecId,:userId,:checkTime)";
		Query querySave = session.createSQLQuery(saveSql);
		querySave.setLong("checkRecId", checkRecId);
		querySave.setLong("userId", userId);
		querySave.setTimestamp("checkTime", CommonMethod.getTimeStamp());
		querySave.executeUpdate();
		
		String sql = "update TICKET_BARCODE_REC set IS_CHECK = '11',CHECK_REC_ID = :checkRecId"+
				" where (status in('01','03') and is_check = '00'"+
				" and indent_detail_id in(select tid.indent_detail_id from TICKET_INDENT_DETAIL tid where tid.indent_id in "+
				" (select ti.indent_id from TICKET_INDENT ti where ti.pay_type = '02'))) or (" +
				"status in('01','03') and is_check = '00' and CHECKING_MACHINE_NUMBER in ('017','018','019','020')"+
				" and indent_detail_id in(select tid.indent_detail_id from TICKET_INDENT_DETAIL tid where tid.indent_id in "+
				" (select ti.indent_id from TICKET_INDENT ti where ti.pay_type = '03')))";
		Query query = session.createSQLQuery(sql);
		query.setLong("checkRecId", checkRecId);
		query.executeUpdate();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String,Double> updateTotal(final long checkRecId) {
		Session session = sessionFactory.getCurrentSession();
		StringBuffer sqlS = new StringBuffer("select sum(tid.unit_price) as subtotal,");
		sqlS.append(" sum(tid.unit_price*tid.per_fee) as payFee,");
		sqlS.append(" sum(tid.entrance_unit_price) as subtotalE,sum(tid.entrance_unit_price*tid.per_fee) as payFeeE,");
		sqlS.append(" sum(tid.ropeway_unit_price) as subtotalR,sum(tid.ropeway_unit_price*tid.per_fee) as payFeeR");
		sqlS.append(" from TICKET_BARCODE_REC tbr,TICKET_INDENT_DETAIL tid");
		sqlS.append(" where tbr.check_rec_id = :checkRecId");
		sqlS.append(" and tbr.indent_detail_id=tid.indent_detail_id");
			
		Query queryS = session.createSQLQuery(sqlS.toString());
		queryS.setLong("checkRecId", checkRecId);
		@SuppressWarnings("rawtypes")
		List l = queryS.list();
		
		double subtotal = 0;
		double payFee = 0;
		double subtotalE = 0;
		double payFeeE = 0;
		double subtotalR = 0;
		double payFeeR = 0;
		for(Object r : l){
			
			Object[] temp = (Object[])r;
			
			TicketAccounts ta = new TicketAccounts();

			if(temp[0]!=null){
				subtotal = Double.valueOf(temp[0].toString());
			}
			if(temp[1]!=null){
				payFee = Double.valueOf(temp[1].toString());
			}
			if(temp[2]!=null){
				subtotalE = Double.valueOf(temp[2].toString());
			}
			if(temp[3]!=null){
				payFeeE = Double.valueOf(temp[3].toString());
			}
			if(temp[4]!=null){
				subtotalR = Double.valueOf(temp[4].toString());
			}
			if(temp[5]!=null){
				payFeeR = Double.valueOf(temp[5].toString());
			}
			
		}
		Map<String,Double> temp = new HashMap<String, Double>();
		temp.put("subtotal", subtotal);
		temp.put("payFee", payFee);
		temp.put("subtotalE", subtotalE);
		temp.put("payFeeE", payFeeE);
		temp.put("subtotalR", subtotalR);
		temp.put("payFeeR", payFeeR);
		
		String sqlUpdate = "update TICKET_CHECK_REC set MONEY = :subtotal,PAY_FEE = :payFee,"+
				"ENTRANCE_MONEY = :subtotalE,ENTRANCE_PAY_FEE = :payFeeE,"+
				"ROPEWAY_MONEY = :subtotalR,ROPEWAY_PAY_FEE = :payFeeR"+
				" where check_rec_id  = :checkRecId";
		Query queryUpdate = session.createSQLQuery(sqlUpdate);
		queryUpdate.setDouble("subtotal", subtotal);
		queryUpdate.setDouble("payFee", payFee);
		queryUpdate.setDouble("subtotalE", subtotalE);
		queryUpdate.setDouble("payFeeE", payFeeE);
		queryUpdate.setDouble("subtotalR", subtotalR);
		queryUpdate.setDouble("payFeeR", payFeeR);
		queryUpdate.setLong("checkRecId", checkRecId);
		queryUpdate.executeUpdate();
		
		return temp;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ConsumeTicketInfo> findTicketStatisticsDataForMKT(final JsonPager<ConsumeTicketInfo> jsonPager,final Timestamp startTime,final Timestamp endTime) {
		Session session = sessionFactory.getCurrentSession();
		StringBuilder sqlBuilder=new StringBuilder();
		sqlBuilder.append("SELECT T_ORDER_PAGER.ROW_ORDER,T_ORDER_PAGER.TRAVEL_AGENCY_NAME,T_ORDER_PAGER.TICKET_TYPES_NAMES,T_ORDER_PAGER.AMOUNT,T_ORDER_PAGER.TOTAL ");
		sqlBuilder.append("FROM(");
		sqlBuilder.append("SELECT ROWNUM ROW_ORDER,T_PAGER.* FROM(");
		sqlBuilder.append("SELECT ta.TRAVEL_AGENCY_ID,ta.TRAVEL_AGENCY_NAME,'' as TICKET_TYPES_NAMES, COUNT(ticketBarCode.BARCODE_REC_ID) AMOUNT,SUM(ticketIndentDetail.UNIT_PRICE) TOTAL ");
		sqlBuilder.append("FROM ");
		sqlBuilder.append("TICKET_BARCODE_REC ticketBarCode,TICKET_INDENT_DETAIL ticketIndentDetail,");
		sqlBuilder.append("TICKET_PRICE ticketPrice,");
		sqlBuilder.append("TICKET_TYPE ticketType,SYS_USER sysUser,BASE_TRAVEL_AGENCY ta ");	
		sqlBuilder.append("WHERE ticketBarCode.INDENT_DETAIL_ID=ticketIndentDetail.INDENT_DETAIL_ID ");
		sqlBuilder.append("AND ticketIndentDetail.TICKET_PRICE_ID=ticketPrice.TICKET_PRICE_ID ");
		sqlBuilder.append("AND ticketPrice.TICKET_ID=ticketType.TICKET_ID ");
		sqlBuilder.append("AND ticketIndentDetail.INPUTER=sysUser.USER_ID ");
		sqlBuilder.append("AND sysUser.TRAVEL_AGENCY_ID =ta.TRAVEL_AGENCY_ID ");
		sqlBuilder.append("AND ticketBarCode.STATUS IN ('01','03') ");
		if(startTime!=null){
			sqlBuilder.append("AND ticketBarCode.PRINT_TIME>= :startTime ");
		}
		if(endTime!=null){
			sqlBuilder.append("AND ticketBarCode.PRINT_TIME<= :endTime ");
		}
		sqlBuilder.append(" GROUP BY ta.TRAVEL_AGENCY_ID,ta.TRAVEL_AGENCY_NAME ");
		sqlBuilder.append(" ORDER BY TOTAL DESC ");
		sqlBuilder.append("	) T_PAGER ");
		sqlBuilder.append("	WHERE ROWNUM<= :endIndex ");
		sqlBuilder.append(") T_ORDER_PAGER ");
		sqlBuilder.append("WHERE T_ORDER_PAGER.ROW_ORDER> :beginIndex");
		
		Query query=session.createSQLQuery(sqlBuilder.toString());
		if(startTime!=null){
			query.setTimestamp("startTime",startTime);
		}
		if(endTime!=null){
			query.setTimestamp("endTime",endTime);
		}
		int beginIndex=Integer.valueOf(jsonPager.getStart().trim());
		int limit=Integer.valueOf(jsonPager.getLimit().trim());
	
		query.setInteger("beginIndex",beginIndex);
		query.setInteger("endIndex",beginIndex+limit);
		
		List<ConsumeTicketInfo> infoList=new ArrayList<ConsumeTicketInfo>();
		@SuppressWarnings("rawtypes")
		List tmpList=query.list();
		for(Object obj:tmpList){
			ConsumeTicketInfo info=new ConsumeTicketInfo();
			Object[]tmpObj=(Object[])obj;
			if(tmpObj[0]!=null){
				info.setOrder(Integer.valueOf(tmpObj[0].toString().trim()));
			}
			if(tmpObj[1]!=null){
				info.setTravelAgencyName(tmpObj[1].toString());
			}
			if(tmpObj[2]!=null){
				String[]ticketTypeNameArray= tmpObj[2].toString().trim().split(",");
				Set<String> names=new HashSet<String>();
				for(String ticketTypeName:ticketTypeNameArray){
					names.add(ticketTypeName);
				}
				String tmpNames=names.toString();
				info.setTicketTypeNames(tmpNames.substring(1,tmpNames.length()-1).replaceAll(",","<br/>").trim());
				
			}
			if(tmpObj[3]!=null){
				info.setTicketTotalQuantity(Integer.valueOf(tmpObj[3].toString().trim()));
			}
			if(tmpObj[4]!=null){
				info.setTicketTotalAmount(Double.valueOf(tmpObj[4].toString().trim()));
			}
			infoList.add(info);
		}
		return infoList;
	}
	
	@Override
	public int findTicketStatisticRowCountForMKT(final Timestamp startTime,final Timestamp endTime){
		Session session = sessionFactory.getCurrentSession();
		StringBuilder sqlBuilder=new StringBuilder();
		sqlBuilder.append("SELECT COUNT(*) NUM ");
		sqlBuilder.append("FROM ");
		sqlBuilder.append("(SELECT  DISTINCT ta.TRAVEL_AGENCY_ID ");
		sqlBuilder.append("FROM ");
		sqlBuilder.append("TICKET_BARCODE_REC ticketBarCode,");
		sqlBuilder.append("TICKET_INDENT_DETAIL ticketIndentDetail,SYS_USER sysUser,");
		sqlBuilder.append("BASE_TRAVEL_AGENCY ta ");
		sqlBuilder.append("WHERE ticketBarCode.INDENT_DETAIL_ID=ticketIndentDetail.INDENT_DETAIL_ID ");
		sqlBuilder.append("AND ticketIndentDetail.INPUTER=sysUser.USER_ID ");
		sqlBuilder.append("AND sysUser.TRAVEL_AGENCY_ID =ta.TRAVEL_AGENCY_ID ");
		if(startTime!=null){
			sqlBuilder.append("AND ticketBarCode.PRINT_TIME>= :startTime ");
		}
		if(endTime!=null){
			sqlBuilder.append("AND ticketBarCode.PRINT_TIME<= :endTime ");
		}
		sqlBuilder.append(" AND ticketBarCode.STATUS IN ('01','03'))");
		Query query=session.createSQLQuery(sqlBuilder.toString());
		if(startTime!=null){
			query.setTimestamp("startTime",startTime);
		}
		if(endTime!=null){
			query.setTimestamp("endTime",endTime);
		}
		@SuppressWarnings("rawtypes")
		List list=query.list();
		Integer count=Integer.valueOf((list.get(0).toString().trim()));
		return count;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public JsonPager<ConsumeTicketInfo> findTicketStatisticsInfoForMKT(
			final JsonPager<ConsumeTicketInfo> jsonPager, final Timestamp startTime,
			final Timestamp endTime,final String discount,final String ticketTypes,final String eliminateTimes) {
		Session session = sessionFactory.getCurrentSession();
		StringBuffer querySQL = new StringBuffer();
		
		querySQL.append(" select a.inputer,u.user_code,t.travel_agency_name,t.branch_office,t.departments,a.total_price,a.total_amount ");
		querySQL.append(" from ( ");
		querySQL.append(" select ti.inputer,sum(tid.unit_price) total_price,count(tbr.barcode_rec_id) total_amount ");
		querySQL.append(" from ticket_indent_detail tid ");
		querySQL.append(" inner join ticket_indent ti on tid.indent_id = ti.indent_id ");
		querySQL.append(" inner join ticket_price tp on tid.ticket_price_id = tp.ticket_price_id ");
		querySQL.append(" inner join ticket_type tt on tp.ticket_id = tt.ticket_id ");
		querySQL.append(" inner join ticket_barcode_rec tbr on tid.indent_detail_id = tbr.indent_detail_id ");
		querySQL.append(" where ti.status in ('04','05') and ti.customer_type = '01' and tbr.status not in ('02','04','05') ");
		//条件
		initConditionSQL(querySQL, startTime, endTime, discount, ticketTypes, eliminateTimes);
//				querySQL.append(" and ( ");
//				querySQL.append(" ( ");
//				querySQL.append(" tbr.print_time >= to_date('2013-12-21 00:00:00','yyyy-MM-DD hh24:mi:ss') ");
//				querySQL.append(" and tbr.print_time <= to_date('2014-03-31 23:59:59','yyyy-MM-DD hh24:mi:ss') ");
//				querySQL.append(" ) ");
//				querySQL.append(" ) ");
		
		querySQL.append(" group by ti.inputer ");
		querySQL.append(" order by ti.inputer) a left join sys_user u ");
		querySQL.append(" on a.inputer = u.user_id ");
		querySQL.append(" left join base_travel_agency t ");
		querySQL.append(" on u.travel_agency_id = t.travel_agency_id ");
		querySQL.append(" order by a.total_price desc ");
		
		
//				querySQL.append(" select ii.user_code,ii.travel_agency_name,ii.branch_office,ii.departments,ii.total_amount,ii.total_price ");
//				querySQL.append(" from (select u.user_code,t.travel_agency_name,t.branch_office,t.departments, ");
//				querySQL.append(" sum(indent.total_amount) total_amount,sum(indent.total_price) total_price ");
//				querySQL.append(" from ticket_indent indent, sys_user u, base_travel_agency t ");
//				querySQL.append(" where indent.inputer = u.user_id ");
//				querySQL.append(" and u.travel_agency_id = t.travel_agency_id ");
//				querySQL.append(" and indent.status = '05' ");
//				querySQL.append(" and indent.customer_type = '01' ");
//				//查询条件参数化
//				if(startTime!=null){
//					querySQL.append("AND indent.update_time>= :startTime ");
//				}
//				if(endTime!=null){
//					querySQL.append("AND indent.update_time<= :endTime ");
//				}
//				if(discount!=null && !discount.isEmpty()){
//					querySQL.append("AND nvl(indent.discount, '10') = :discount ");
//				}
//				querySQL.append(" group by u.user_code,t.travel_agency_name,t.branch_office,t.departments) ii ");
//				querySQL.append(" order by  total_price desc");
		
		//1.count 总记录数
		StringBuffer countSQL = new StringBuffer(" select count (*) "
			+ CommonMethod.removeSelect(querySQL.toString()));
		Query queryCount = session.createSQLQuery(countSQL.toString());
		//1.1查询条件赋值
		initCondition(queryCount, startTime, endTime, discount, ticketTypes, eliminateTimes);
		jsonPager.setTotal(Integer.parseInt(queryCount.list().get(0).toString()));
		//2.查询分页记录
		Query query = session.createSQLQuery(querySQL.toString());
		//2.1查询条件赋值
		//initCondition(query, startTime, endTime,discount);
		initCondition(query, startTime, endTime, discount, ticketTypes, eliminateTimes);
		query.setFirstResult(Integer.parseInt(jsonPager.getStart()));
		query.setMaxResults(Integer.parseInt(jsonPager.getLimit()));
		query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		//3.循环填充list
		List<ConsumeTicketInfo> infoList=new ArrayList<ConsumeTicketInfo>();
		Map<String,ConsumeTicketInfo> infoMap=new HashMap<String,ConsumeTicketInfo>();
		int order = Integer.parseInt(jsonPager.getStart());
		order++;
		for (Object obj : query.list()) {
			Map map = (Map)obj;
			ConsumeTicketInfo info = new ConsumeTicketInfo();
			info.setOrder(order++);
			info.setUserId(map.get("USER_ID")==null?null:map.get("USER_ID").toString());
			info.setUserCode(map.get("USER_CODE")==null?null:map.get("USER_CODE").toString());
			info.setTravelAgencyName(map.get("TRAVEL_AGENCY_NAME")==null?null:map.get("TRAVEL_AGENCY_NAME").toString());
			info.setBranchOffice(map.get("BRANCH_OFFICE")==null?null:map.get("BRANCH_OFFICE").toString());
			info.setDepartments(map.get("DEPARTMENTS")==null?null:map.get("DEPARTMENTS").toString());
			info.setDiscount(map.get("DISCOUNT")==null?null:map.get("DISCOUNT").toString());
			info.setTotalAmount(map.get("TOTAL_AMOUNT")==null?null:map.get("TOTAL_AMOUNT").toString());
			info.setTotalPrice(map.get("TOTAL_PRICE")==null?null:map.get("TOTAL_PRICE").toString());
			infoList.add(info);
			infoMap.put(info.getUserCode(), info);
		}
		
		StringBuffer querySubSQL = new StringBuffer();

		querySubSQL.append(" select a.inputer,a.discount,u.user_code,t.travel_agency_name,t.branch_office,t.departments,a.total_price,a.total_amount ");
		querySubSQL.append(" from ( ");
		querySubSQL.append(" select ti.inputer, nvl(ti.discount, '10') discount,sum(tid.unit_price) total_price,count(tbr.barcode_rec_id) total_amount ");
		querySubSQL.append(" from ticket_indent_detail tid ");
		querySubSQL.append(" inner join ticket_indent ti on tid.indent_id = ti.indent_id ");
		querySubSQL.append(" inner join ticket_price tp on tid.ticket_price_id = tp.ticket_price_id ");
		querySubSQL.append(" inner join ticket_type tt on tp.ticket_id = tt.ticket_id ");
		querySubSQL.append(" inner join ticket_barcode_rec tbr on tid.indent_detail_id = tbr.indent_detail_id ");
		querySubSQL.append(" where ti.status in ('04','05') and ti.customer_type = '01' and tbr.status not in ('02','04','05') ");
		
		//条件
		initConditionSQL(querySubSQL, startTime, endTime, discount, ticketTypes, eliminateTimes);
//				querySubSQL.append(" and ( ");
//				querySubSQL.append(" ( ");
//				querySubSQL.append(" tbr.print_time >= to_date('2013-12-21 00:00:00','yyyy-MM-DD hh24:mi:ss') ");
//				querySubSQL.append(" and tbr.print_time <= to_date('2014-03-31 23:59:59','yyyy-MM-DD hh24:mi:ss') ");
//				querySubSQL.append(" ) ");
//				querySubSQL.append(" ) ");
		
		querySubSQL.append(" group by ti.inputer, nvl(ti.discount, '10') ");
		querySubSQL.append(" order by ti.inputer) a left join st.sys_user u ");
		querySubSQL.append(" on a.inputer = u.user_id ");
		querySubSQL.append(" left join st.base_travel_agency t ");
		querySubSQL.append(" on u.travel_agency_id = t.travel_agency_id ");
		querySubSQL.append(" order by a.inputer,a.discount ");
		  
		  
//				querySubSQL.append(" select tt.user_id,tt.user_code,tt.travel_agency_name,tt.branch_office,tt.departments,ii.discount,ii.total_amount,ii.total_price ");
//				querySubSQL.append(" from (select inputer, nvl(indent.discount, '10') discount, ");
//				querySubSQL.append(" sum(indent.total_amount) total_amount,sum(indent.total_price) total_price ");
//				querySubSQL.append(" from ticket_indent indent  where indent.status = '05' ");
//				querySubSQL.append(" and indent.customer_type = '01' ");
//				//查询条件参数化
//				if(startTime!=null){
//					querySubSQL.append("AND indent.update_time>= :startTime ");
//				}
//				if(endTime!=null){
//					querySubSQL.append("AND indent.update_time<= :endTime ");
//				}
//				if(discount!=null && !discount.isEmpty()){
//					querySubSQL.append("AND nvl(indent.discount, '10') = :discount ");
//				}
//				querySubSQL.append(" group by indent.inputer, nvl(indent.discount, '10')) ii ");
//				querySubSQL.append(" inner join (   ");           
//				querySubSQL.append(" select u.user_id, t.travel_agency_name,u.user_code,t.branch_office,t.departments ");
//				querySubSQL.append(" from sys_user u ");
//				querySubSQL.append(" inner join base_travel_agency t ");
//				querySubSQL.append(" on u.travel_agency_id = t.travel_agency_id) tt ");
//				querySubSQL.append(" on ii.inputer = tt.user_id order by tt.user_id ");
		
		Query querySub = session.createSQLQuery(querySubSQL.toString());
		//2.1查询条件赋值
		initCondition(querySub, startTime, endTime, discount, ticketTypes, eliminateTimes);
		querySub.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		
		for (Object obj : querySub.list()) {
			Map map = (Map)obj;
			
			String taName = map.get("USER_CODE")==null?null:map.get("USER_CODE").toString();
			
			ConsumeTicketInfo info = infoMap.get(taName);
			if(info==null){
				continue;
			}
			List te = info.getSubList();
			if(te==null){
				te = new ArrayList();
			}
			ConsumeTicketInfo infoSub = new ConsumeTicketInfo();
			infoSub.setDiscount(map.get("DISCOUNT")==null?null:map.get("DISCOUNT").toString());
			infoSub.setTotalAmount(map.get("TOTAL_AMOUNT")==null?null:map.get("TOTAL_AMOUNT").toString());
			infoSub.setTotalPrice(map.get("TOTAL_PRICE")==null?null:map.get("TOTAL_PRICE").toString());
			te.add(infoSub);
			info.setSubList(te);
		}
		
		jsonPager.setRoot(infoList);
		return jsonPager;
	}
	
	private void initConditionSQL(StringBuffer querySQL, final Timestamp startTime,
			final Timestamp endTime,final String discount,final String ticketTypes,final String eliminateTimes){
		
		if(discount!=null && !discount.isEmpty()){
			querySQL.append(" and nvl(ti.discount, '10') = :discount ");
		}
		if(ticketTypes!=null&&!"".equals(ticketTypes)){
			if(ticketTypes.indexOf("all_all")>=0){//出现all_all则查询全部
			}else{
				String[] ts = ticketTypes.split("\\|");
				querySQL.append(" and ( ");
				for(int tsNum = 0 ; tsNum < ts.length ;tsNum++){
					String t = ts[tsNum];
					if(t.indexOf("_")>0){
						String[] tt = t.split("_");
						querySQL.append(" ( ");
						querySQL.append(" tt.special_type = '"+tt[0]+"' ");
						querySQL.append(" and tt.ticket_big_type = '"+tt[1]+"' ");
						querySQL.append(" ) or ");
					}
				}
				querySQL.append(" ( ");
				querySQL.append(" tt.special_type = '-1' ");
				querySQL.append(" and tt.ticket_big_type = '-1' ");
				querySQL.append(" ) ");
				querySQL.append(" ) ");
				
			}
		}
		
		if(eliminateTimes!=null&&!"".equals(eliminateTimes)){
			
			String[] eTimes = eliminateTimes.split("\\|");
			List<Map<String,Timestamp>> tFraList = new ArrayList<Map<String,Timestamp>>();
			for(int tsNum = 0 ; tsNum < eTimes.length ;tsNum++){
				String t = eTimes[tsNum];
				if(t.indexOf(",")>0){
					String[] tFra = t.split(",");
					
					String sta = tFra[0];
					String end = tFra[1];
					
					Timestamp staT = CommonMethod.string2Time1(sta+" 00:00:00");
					Timestamp endT = CommonMethod.string2Time1(end+" 00:00:00");
					
					if(staT.before(endTime) && endT.after(startTime) &&
							staT.after(startTime) && endT.before(endTime)){//排除时间段在查询时间段内才加入条件中
						Map<String,Timestamp> tempMap = new HashMap<String,Timestamp>();
						tempMap.put("startTime", staT);
						tempMap.put("endTime",  endT);
						
						tFraList.add(tempMap);
					}
				}
			}
			//按开始时间排序
			Collections.sort(tFraList, new Comparator() {
		      @Override
		      public int compare(Object o1, Object o2) {
		    	  Map<String,Timestamp> m1 = (Map<String,Timestamp>)o1;
		    	  Map<String,Timestamp> m2 = (Map<String,Timestamp>)o2;
		    	  long l=m1.get("startTime").getTime()-m2.get("startTime").getTime();
		  		  long day=(l/(60*60*1000*24));
		  		  return new Long(day).intValue();
		      }
		    });
			
			//合并交叉时间段
			List<Map<String,Timestamp>> tFraListRe = new ArrayList<Map<String,Timestamp>>();
			
			for(Map<String,Timestamp> tFraMap : tFraList){
				
				if(tFraListRe.size()<1){
					tFraListRe.add(tFraMap);
				}else{
					Map<String,Timestamp> tempMapRe = tFraListRe.get(tFraListRe.size()-1);
					
					if((tempMapRe.get("startTime").after(tFraMap.get("startTime"))&&tempMapRe.get("startTime").before(tFraMap.get("endTime"))) || 
							(tempMapRe.get("endTime").after(tFraMap.get("startTime"))&&tempMapRe.get("endTime").before(tFraMap.get("endTime")))||
							(tFraMap.get("startTime").after(tempMapRe.get("startTime"))&&tFraMap.get("startTime").before(tempMapRe.get("endTime"))) || 
							(tFraMap.get("endTime").after(tempMapRe.get("startTime"))&&tFraMap.get("endTime").before(tempMapRe.get("endTime")))){//时间有交叉
						
						if(tempMapRe.get("startTime").before(tFraMap.get("startTime"))){
							tempMapRe.put("startTime", tempMapRe.get("startTime"));
						}else{
							tempMapRe.put("startTime", tFraMap.get("startTime"));
						}
						
						if(tempMapRe.get("endTime").after(tFraMap.get("endTime"))){
							tempMapRe.put("endTime", tempMapRe.get("endTime"));
						}else{
							tempMapRe.put("endTime", tFraMap.get("endTime"));
						}
					}else{
						tFraListRe.add(tFraMap);
					}
				}
				
			}
			if(tFraListRe.size()<1){
				querySQL.append(" and ( ");
				querySQL.append(" ( ");
				querySQL.append(" tbr.print_time >= to_date('"+CommonMethod.timeToString(startTime)+" 00:00:00','yyyy-MM-DD hh24:mi:ss') ");
				querySQL.append(" and tbr.print_time <= to_date('"+CommonMethod.timeToString(endTime)+" 23:59:59','yyyy-MM-DD hh24:mi:ss') ");
				querySQL.append(" ) ");
				querySQL.append(" ) ");
			}else{
				//组合时间段
				
				List<Map<String,Timestamp>> timeFrameList = new ArrayList<Map<String,Timestamp>>();
				
				List<Timestamp> timeList = new ArrayList<Timestamp>();
				timeList.add(startTime);
				for(int num = 0 ; num<tFraListRe.size();num++){
					
					Map<String,Timestamp> tFraMap = tFraListRe.get(num);
					timeList.add(CommonMethod.addDay(tFraMap.get("startTime"), -1));
					timeList.add(CommonMethod.addDay(tFraMap.get("endTime"), 1));
				}
				timeList.add(endTime);
				
				querySQL.append(" and ( ");
				
				for(int num = 0 ; num < timeList.size() ; num=num+2){
					querySQL.append(" ( ");
					querySQL.append(" tbr.print_time >= to_date('"+CommonMethod.timeToString(timeList.get(num))+" 00:00:00','yyyy-MM-DD hh24:mi:ss') ");
					querySQL.append(" and tbr.print_time <= to_date('"+CommonMethod.timeToString(timeList.get(num+1))+" 23:59:59','yyyy-MM-DD hh24:mi:ss') ");
					querySQL.append(" ) or");
				}
				querySQL.append(" ( 1=2 ) ");
				querySQL.append(" ) ");
			}
			
		}else{
			
			querySQL.append(" and ( ");
			querySQL.append(" ( ");
			querySQL.append(" tbr.print_time >= to_date('"+CommonMethod.timeToString(startTime)+" 00:00:00','yyyy-MM-DD hh24:mi:ss') ");
			querySQL.append(" and tbr.print_time <= to_date('"+CommonMethod.timeToString(endTime)+" 23:59:59','yyyy-MM-DD hh24:mi:ss') ");
			querySQL.append(" ) ");
			querySQL.append(" ) ");
		}
		
	}
	
	
	private void initCondition(Query query, final Timestamp startTime,
			final Timestamp endTime,final String discount,final String ticketTypes,final String eliminateTimes){
		if(discount!=null && !discount.isEmpty()){
			query.setString("discount", discount);
		}
	}
	
}
