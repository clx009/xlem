package dao.ticket.ticketBarcodeRec;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import com.xlem.dao.ConsumeTicketInfo;
import com.xlem.dao.TicketAccounts;
import com.xlem.dao.TicketBarcodeRec;

import common.action.JsonPager;
import common.dao.BaseDao;

public interface TicketBarcodeRecDao extends BaseDao<TicketBarcodeRec> {

	public List<TicketAccounts> findGridByAccounts();
	
	public List<TicketAccounts> findGridBySubAccounts();
	
	public void update(long checkRecId,long userId);
	
	public Map<String,Double> updateTotal(long checkRecId);

	public List<ConsumeTicketInfo> findTicketStatisticsDataForMKT(final JsonPager<ConsumeTicketInfo> jsonPager,Timestamp startTime,Timestamp endTime);

	public int findTicketStatisticRowCountForMKT(Timestamp startTime, Timestamp endTime);

	public JsonPager<ConsumeTicketInfo> findTicketStatisticsInfoForMKT(
			JsonPager<ConsumeTicketInfo> jsonPager, Timestamp startTime,
			Timestamp endTime, String discount,final String ticketTypes,final String eliminateTimes);
}