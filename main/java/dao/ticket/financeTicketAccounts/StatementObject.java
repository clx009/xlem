package dao.ticket.financeTicketAccounts;

import java.sql.Timestamp;

import common.dao.BaseBean;

/**
 * 此类封装 报表一行数据 如果列数量不够，请自行添加
 * 
 * @author li
 * 
 */
public class StatementObject extends BaseBean {

	private Double tableRow1 = 0D;
	private Double tableRow2 = 0D;
	private Double tableRow3 = 0D;
	private Double tableRow4 = 0D;
	private Double tableRow5 = 0D;
	private Double tableRow6 = 0D;
	private Double tableRow7 = 0D;
	private Double tableRow8 = 0D;
	private Double tableRow9 = 0D;
	private Double tableRow10 = 0D;
	private Double tableRow11 = 0D;
	private Double tableRow12 = 0D;
	private String tableColumn;
	private String remark;
	private Timestamp startDate;
	private Timestamp endDate;
	
	//查询类型，00：现金，01：预付款，02：奖励款
	private String payType;
	
	//奖励款类型
	private Long rewardTypeId;

	public StatementObject() {
	}

	public StatementObject(String tableColumn) {
		this.tableColumn = tableColumn;
	}

	public Double getTableRow1() {
		return tableRow1;
	}

	public void setTableRow1(Double tableRow1) {
		this.tableRow1 = tableRow1;
	}

	public Double getTableRow2() {
		return tableRow2;
	}

	public void setTableRow2(Double tableRow2) {
		this.tableRow2 = tableRow2;
	}

	public Double getTableRow3() {
		return tableRow3;
	}

	public void setTableRow3(Double tableRow3) {
		this.tableRow3 = tableRow3;
	}

	public Double getTableRow4() {
		return tableRow4;
	}

	public void setTableRow4(Double tableRow4) {
		this.tableRow4 = tableRow4;
	}

	public Double getTableRow5() {
		return tableRow5;
	}

	public void setTableRow5(Double tableRow5) {
		this.tableRow5 = tableRow5;
	}

	public Double getTableRow6() {
		return tableRow6;
	}

	public void setTableRow6(Double tableRow6) {
		this.tableRow6 = tableRow6;
	}

	public Double getTableRow7() {
		return tableRow7;
	}

	public void setTableRow7(Double tableRow7) {
		this.tableRow7 = tableRow7;
	}

	public Double getTableRow8() {
		return tableRow8;
	}

	public void setTableRow8(Double tableRow8) {
		this.tableRow8 = tableRow8;
	}

	public Double getTableRow9() {
		return tableRow9;
	}

	public void setTableRow9(Double tableRow9) {
		this.tableRow9 = tableRow9;
	}

	public Double getTableRow10() {
		return tableRow10;
	}

	public void setTableRow10(Double tableRow10) {
		this.tableRow10 = tableRow10;
	}

	public Double getTableRow11() {
		return tableRow11;
	}

	public void setTableRow11(Double tableRow11) {
		this.tableRow11 = tableRow11;
	}

	public Double getTableRow12() {
		return tableRow12;
	}

	public void setTableRow12(Double tableRow12) {
		this.tableRow12 = tableRow12;
	}

	public String getTableColumn() {
		return tableColumn;
	}

	public void setTableColumn(String tableColumn) {
		this.tableColumn = tableColumn;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * 查询类型
	 * @return 00：现金，01：预付款，02：奖励款
	 */
	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	
	public Long getRewardTypeId() {
		return rewardTypeId;
	}

	public void setRewardTypeId(Long rewardTypeId) {
		this.rewardTypeId = rewardTypeId;
	}

}
