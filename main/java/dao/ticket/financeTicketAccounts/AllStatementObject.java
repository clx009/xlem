package dao.ticket.financeTicketAccounts;

import java.sql.Timestamp;

import common.CommonMethod;
import common.dao.BaseBean;

/**
 * 财务总报表使用类
 * @author xyx
 * 
 */
public class AllStatementObject extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int scale=1;//显示小数点位数
	
	private int colNums;//顺序
	
	private String objectName;//行名
	
	//上期余额
	private Double previous = 0D;
	private Double previousSnow = 0D;
	private boolean previousShow = true;
	
	//本期交易
	private Double trade = 0D;
	private Double tradeSnow = 0D;
	private boolean tradeShow = true;
	
	//本期消费
	private Double expense = 0D;
	private Double expenseSnow = 0D;
	private boolean expenseShow = true;
	
	//本期余额
	private Double current = 0D;
	private Double currentSnow = 0D;
	private boolean currentShow = true;
	private boolean currentShowNums = false;
	
	//显示行
	private boolean show = true;
	
	private Timestamp startDate;
	private Timestamp endDate;
	
	public AllStatementObject() {
	}
	
	public AllStatementObject(int colNums,String objectName,boolean previousShow,boolean tradeShow,boolean expenseShow,boolean currentShow,boolean currentShowNums,boolean show) {
		scale=0;
		this.colNums = colNums;
		this.objectName = objectName;
		this.previousShow=previousShow;
		this.tradeShow=tradeShow;
		this.expenseShow=expenseShow;
		this.currentShow=currentShow;
		this.currentShowNums=currentShowNums;
		this.show=show;
	}
	
	public void putValues(AllStatementObject aso){
		
		this.previous = aso.getPrevious();
		this.previousSnow = aso.getPreviousSnow();
		
		this.trade = aso.getTrade();
		this.tradeSnow = aso.getTradeSnow();
		
		this.expense = aso.getExpense();
		this.expenseSnow = aso.getExpenseSnow();
		
		this.current = aso.getCurrent();
		this.currentSnow = aso.getCurrentSnow();
		
		this.startDate = aso.getStartDate();
		this.endDate = aso.getEndDate();
		
	}
	
	
	public int getColNums() {
		return colNums;
	}
	public void setColNums(int colNums) {
		this.colNums = colNums;
	}
	public String getObjectName() {
		return objectName;
	}
	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}
	
	/**
	 * 上期余额
	 */
	public Double getPrevious() {
		return previous;
	}
	/**
	 * 上期余额
	 */
	public void setPrevious(Double previous) {
		this.previous = previous;
	}
	/**
	 * 上期余额
	 */
	public String getPreviousText() {
		if(previousShow){
			return CommonMethod.formatdb(previous == null ? 0 :previous,scale);
		}else{
			return "&nbsp;";
		}
	}
	
	/**
	 * 上期余额
	 */
	public Double getPreviousSnow() {
		return previousSnow;
	}
	/**
	 * 上期余额
	 */
	public void setPreviousSnow(Double previousSnow) {
		this.previousSnow = previousSnow;
	}
	/**
	 * 上期余额
	 */
	public String getPreviousSnowText() {
		if(previousShow){
			return CommonMethod.formatdb(previousSnow == null ? 0 :previousSnow,scale);
		}else{
			return "&nbsp;";
		}
	}
	
	/**
	 * 上期余额合计
	 */
	public String getPreviousTotalText() {
		if(previousShow){
			double t = (previous == null ? 0 :previous)+(previousSnow == null ? 0 :previousSnow);
			return CommonMethod.formatdb(t,scale);
		}else{
			return "&nbsp;";
		}
	}
	
	
	/**
	 * 本期交易
	 */
	public Double getTrade() {
		return trade;
	}
	/**
	 * 本期交易
	 */
	public void setTrade(Double trade) {
		this.trade = trade;
	}
	/**
	 * 本期交易
	 */
	public String getTradeText() {
		if(tradeShow){
			return CommonMethod.formatdb(trade == null ? 0 :trade,scale);
		}else{
			return "&nbsp;";
		}
	}
	
	/**
	 * 本期交易
	 */
	public Double getTradeSnow() {
		return tradeSnow;
	}
	/**
	 * 本期交易
	 */
	public void setTradeSnow(Double tradeSnow) {
		this.tradeSnow = tradeSnow;
	}
	/**
	 * 本期交易
	 */
	public String getTradeSnowText() {
		if(tradeShow){
			return CommonMethod.formatdb(tradeSnow == null ? 0 :tradeSnow,scale);
		}else{
			return "&nbsp;";
		}
	}
	/**
	 * 本期交易合计
	 */
	public String getTradeTotalText() {
		if(tradeShow){
			double t = (trade == null ? 0 :trade)+(tradeSnow == null ? 0 :tradeSnow);
			return CommonMethod.formatdb(t,scale);
		}else{
			return "&nbsp;";
		}
	}
	
	
	/**
	 * 本期消费
	 */
	public Double getExpense() {
		return expense;
	}
	/**
	 * 本期消费
	 */
	public void setExpense(Double expense) {
		this.expense = expense;
	}
	/**
	 * 本期消费
	 */
	public String getExpenseText() {
		if(expenseShow){
			return CommonMethod.formatdb(expense == null ? 0 :expense,scale);
		}else{
			return "&nbsp;";
		}
	}
	
	/**
	 * 本期消费
	 */
	public Double getExpenseSnow() {
		return expenseSnow;
	}
	/**
	 * 本期消费
	 */
	public void setExpenseSnow(Double expenseSnow) {
		this.expenseSnow = expenseSnow;
	}
	/**
	 * 本期消费
	 */
	public String getExpenseSnowText() {
		if(expenseShow){
			return CommonMethod.formatdb(expenseSnow == null ? 0 :expenseSnow,scale);
		}else{
			return "&nbsp;";
		}
	}
	/**
	 * 本期消费合计
	 */
	public String getExpenseTotalText() {
		if(expenseShow){
			double t = (expense == null ? 0 :expense)+(expenseSnow == null ? 0 :expenseSnow);
			return CommonMethod.formatdb(t,scale);
		}else{
			return "&nbsp;";
		}
	}
	
	
	/**
	 * 本期余额
	 */
	public Double getCurrent() {
		if(currentShowNums){
			return current;
		}else if(currentShow){
			return (previous == null ? 0 :previous) + (trade == null ? 0 :trade)-(expense == null ? 0 :expense);
		}else{
			return 0d;
		}
	}
	/**
	 * 本期余额
	 */
	public void setCurrent(Double current) {
		this.current = current;
	}
	/**
	 * 本期余额
	 */
	public String getCurrentText() {
		if(currentShowNums){
			return CommonMethod.formatdb(current == null ? 0 :current,scale);
		}else if(currentShow){
			double t = (previous == null ? 0 :previous) + (trade == null ? 0 :trade)-(expense == null ? 0 :expense);
			return CommonMethod.formatdb(t,scale);
		}else{
			return "&nbsp;";
		}
	}
	/**
	 * 本期余额
	 */
	public Double getCurrentSnow() {
		if(currentShowNums){
			return currentSnow;
		}else if(currentShow){
			return (previousSnow == null ? 0 :previousSnow) + (tradeSnow == null ? 0 :tradeSnow)-(expenseSnow == null ? 0 :expenseSnow);
		}else{
			return 0d;
		}
	}
	/**
	 * 本期余额
	 */
	public void setCurrentSnow(Double currentSnow) {
		this.currentSnow = currentSnow;
	}
	/**
	 * 本期余额
	 */
	public String getCurrentSnowText() {
		if(currentShowNums){
			return CommonMethod.formatdb(currentSnow == null ? 0 :currentSnow,scale);
		}else if(currentShow){
			double t = (previousSnow == null ? 0 :previousSnow) + (tradeSnow == null ? 0 :tradeSnow)-(expenseSnow == null ? 0 :expenseSnow);
			return CommonMethod.formatdb(t,scale);
		}else{
			return "&nbsp;";
		}
	}
	/**
	 * 本期余额合计
	 */
	public String getCurrentTotalText() {
		if(currentShowNums){
			double t = (current == null ? 0 :current)+(currentSnow == null ? 0 :currentSnow);
			return CommonMethod.formatdb(t,scale);
		}else if(currentShow){
			double t = (previous == null ? 0 :previous) + (trade == null ? 0 :trade)-(expense == null ? 0 :expense);
			double tSnow = (previousSnow == null ? 0 :previousSnow) + (tradeSnow == null ? 0 :tradeSnow)-(expenseSnow == null ? 0 :expenseSnow);
			double tC = t+tSnow;
			return CommonMethod.formatdb(tC,scale);
		}else{
			return "&nbsp;";
		}
	}
	
	public Timestamp getStartDate() {
		return startDate;
	}
	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}
	public Timestamp getEndDate() {
		return endDate;
	}
	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public boolean isPreviousShow() {
		return previousShow;
	}

	public void setPreviousShow(boolean previousShow) {
		this.previousShow = previousShow;
	}

	public boolean isTradeShow() {
		return tradeShow;
	}

	public void setTradeShow(boolean tradeShow) {
		this.tradeShow = tradeShow;
	}

	public boolean isExpenseShow() {
		return expenseShow;
	}

	public void setExpenseShow(boolean expenseShow) {
		this.expenseShow = expenseShow;
	}

	public boolean isCurrentShow() {
		return currentShow;
	}

	public void setCurrentShow(boolean currentShow) {
		this.currentShow = currentShow;
	}

	public boolean isCurrentShowNums() {
		return currentShowNums;
	}

	public void setCurrentShowNums(boolean currentShowNums) {
		this.currentShowNums = currentShowNums;
	}

	public boolean isShow() {
		return show;
	}

	public void setShow(boolean show) {
		this.show = show;
	}

	
}
