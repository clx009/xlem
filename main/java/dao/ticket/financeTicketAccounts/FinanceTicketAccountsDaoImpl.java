package dao.ticket.financeTicketAccounts;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.FinanceTicketAccounts;
import com.xlem.dao.SysUserRole;

import common.CommonMethod;
import common.action.JsonPager;
import common.dao.BaseDaoImpl;

@Transactional
@Repository("financeTicketAccountsDao")
public class FinanceTicketAccountsDaoImpl extends BaseDaoImpl<FinanceTicketAccounts> implements FinanceTicketAccountsDao {
	/**
	 * 总报表
	 */
	
	//key 报表行
	//value 报表每一行对的   财务票表   里面的  支付类型
	private Map<String, String> statementColumn = new HashMap<String, String>();
	
	
	public FinanceTicketAccountsDaoImpl() {
		statementColumn.put("网银支付已消费金额", "04"); 
		statementColumn.put("易宝支付已消费金额", "x"); 
		statementColumn.put("余额支付已消费金额", "05");
		statementColumn.put("网银已支付未消费金额", "01");
		statementColumn.put("余额已支付未消费金额", "02");
		//statementColumn.put("现金充值金额", "03");
		statementColumn.put("网银付款退款到余额", "07");
		statementColumn.put("余额付款退款到余额", "08");
		//statementColumn.put("退款到余额", "07,08");
		statementColumn.put("退款到卡上金额", "06");
		statementColumn.put("合计", " ");
	}   
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AllStatementObject>  findAllStatement(final StatementObject stateObj) {
		Session session = sessionFactory.getCurrentSession();
		List<AllStatementObject> pagerRoot = new ArrayList<AllStatementObject>();

		//查询奖励款类型
		StringBuffer rewardCondition = new StringBuffer();
		StringBuffer rewardConditionSub = new StringBuffer();
		if(stateObj.getRewardTypeId()!=null){
			rewardCondition.append(" and t.recharge_rec_id in ( ");
			rewardCondition.append(" select brr.recharge_rec_id from BASE_RECHARGE_REC brr where brr.reward_account_id in ( ");
			rewardCondition.append(" select bra.reward_account_id from BASE_REWARD_ACCOUNT bra where bra.reward_type_id = "+stateObj.getRewardTypeId());
			rewardCondition.append(" )) ");
			
			rewardConditionSub.append(" and ftasub.recharge_rec_id in ( ");
			rewardConditionSub.append(" select brr.recharge_rec_id from BASE_RECHARGE_REC brr where brr.reward_account_id in ( ");
			rewardConditionSub.append(" select bra.reward_account_id from BASE_REWARD_ACCOUNT bra where bra.reward_type_id = "+stateObj.getRewardTypeId());
			rewardConditionSub.append(" )) ");
		}
		
		String accType = "";
		if("01".equals(stateObj.getPayType())){//预付款
			accType = "'04'";
			pagerRoot.add(new AllStatementObject(3, "已消费金额",true,true,true,false,false,true));
			pagerRoot.add(new AllStatementObject(5, "已支付未消费金额",true,true,false,true,false,true));
			pagerRoot.add(new AllStatementObject(6, "充值金额",true,true,false,true,true,true));
			pagerRoot.add(new AllStatementObject(10, "合计",true,true,true,true,false,true));
		}else if("02".equals(stateObj.getPayType())){//奖励款
			accType = "'01'";
			pagerRoot.add(new AllStatementObject(3, "余额支付已消费金额",true,true,true,false,false,true));
			pagerRoot.add(new AllStatementObject(5, "余额已支付未消费金额",true,true,false,true,false,true));
			pagerRoot.add(new AllStatementObject(6, "奖励款充值金额",true,true,false,true,true,true));
			pagerRoot.add(new AllStatementObject(10, "合计",true,true,true,true,false,true));
		}else if("mobile".equals(stateObj.getPayType()) || "05".equals(stateObj.getPayType()) || "06".equals(stateObj.getPayType())){//移动终端
			if("mobile".equals(stateObj.getPayType())){
				accType = "'05','06'";
			}else if("05".equals(stateObj.getPayType())){
				accType = "'05'";
			}else if("06".equals(stateObj.getPayType())){
				accType = "'06'";
			}
			pagerRoot.add(new AllStatementObject(1, "支付已消费金额",true,true,true,false,false,true));
			pagerRoot.add(new AllStatementObject(4, "支付未消费金额",true,true,false,true,false,true));
			pagerRoot.add(new AllStatementObject(9, "退款金额",true,true,false,true,false,true));
			pagerRoot.add(new AllStatementObject(10, "合计",true,true,true,true,false,true));
		}else{//现金
			accType = "'02','03'";
			pagerRoot.add(new AllStatementObject(1, "网银支付已消费金额",true,true,true,false,false,true));
			pagerRoot.add(new AllStatementObject(2, "易宝支付已消费金额",false,true,true,false,false,true));
			pagerRoot.add(new AllStatementObject(3, "余额支付已消费金额",true,true,true,false,false,true));
			pagerRoot.add(new AllStatementObject(4, "网银已支付未消费金额",true,true,false,true,false,true));
			pagerRoot.add(new AllStatementObject(5, "余额已支付未消费金额",true,true,false,true,false,true));
			//pagerRoot.add(new AllStatementObject(6, "现金充值金额",true,true,false,true,true));
			pagerRoot.add(new AllStatementObject(7, "网银付款退款到余额",true,true,false,true,true,true));
			//pagerRoot.add(new AllStatementObject(8, "余额付款退款到余额",true,true,false,true,false));
			pagerRoot.add(new AllStatementObject(9, "退款到卡上金额",true,true,false,true,false,true));
			pagerRoot.add(new AllStatementObject(10, "合计",true,true,true,true,false,true));
			//pagerRoot.add(new AllStatementObject(11, "退现抵扣",true,true,true,true,true,false));
		}
		
		
		//查询条件（上期）
		StringBuffer timeConditionBefore = new StringBuffer();
		//添加传入的查询条件
		timeConditionBefore.append(" t.input_time < to_date('");
		timeConditionBefore.append(CommonMethod.timeToString(stateObj.getStartDate()));
		timeConditionBefore.append(" 02:00:00','YYYY-MM-DD HH24:MI:SS') ");
		
		timeConditionBefore.append(rewardCondition);
		
		//查询条件（本期）
		StringBuffer timeConditionThis = new StringBuffer();
		//添加传入的查询条件
		timeConditionThis.append(" t.input_time >= to_date('");
		timeConditionThis.append(CommonMethod.timeToString(stateObj.getStartDate()));
		timeConditionThis.append(" 02:00:00','YYYY-MM-DD HH24:MI:SS') and t.input_time < to_date('");
		
		timeConditionThis.append(CommonMethod.timeToString(CommonMethod.addDay(stateObj.getEndDate(),1)));
		timeConditionThis.append(" 02:00:00','YYYY-MM-DD HH24:MI:SS') ");
		
		timeConditionThis.append(rewardCondition);
		
		
		
		StringBuffer sql = new StringBuffer();
		List queryResult;
		sql.append(" select nvl(before.atb,this.att) aType,nvl(before.ptb,this.ptt) pType, ");
		sql.append(" nvl(before.moneyB,0) moneyB,nvl(before.moneySnowB,0) moneySnowB, ");
		sql.append(" nvl(this.moneyT,0) moneyT,nvl(this.moneySnowT,0) moneySnowT from ");
		sql.append("(select cast(t.accounts_type as varchar2(2)) atb,cast(t.payment_type as varchar2(2)) ptb,sum(t.money+nvl(back.money,0)) moneyB,sum(t.money_snow+nvl(back.money_snow,0)) moneySnowB");
		sql.append(" from FINANCE_TICKET_ACCOUNTS t");
		sql.append(" left join ");
		sql.append("(select tBack.Money,tBack.Money_Snow,tBack.Back_Accounts_Id ");
		sql.append(" from FINANCE_TICKET_ACCOUNTS tBack ");
		sql.append(" where tBack.Payment_Type in ('09','10') ");
		sql.append(" and tBack.input_time  < to_date('"+CommonMethod.timeToString(CommonMethod.addDay(stateObj.getStartDate(),1))+" 02:00:00','YYYY-MM-DD HH24:MI:SS') ");
		sql.append(" ) back ");
		sql.append(" on back.back_accounts_id = t.ticket_accounts_id ");
		sql.append(" where t.accounts_type in ("+accType+") and t.payment_type not in ('09','10') ");
		sql.append(" and ");
		sql.append(timeConditionBefore);
		sql.append(" group by t.accounts_type,t.payment_type ) before");
		
		sql.append(" full join ");
		
		sql.append("(select cast(t.accounts_type as varchar2(2)) att,cast(t.payment_type as varchar2(2)) ptt,sum(t.money+nvl(back.money,0)) moneyT,sum(t.money_snow+nvl(back.money_snow,0)) moneySnowT");
		sql.append(" from FINANCE_TICKET_ACCOUNTS t");
		sql.append(" left join ");
		sql.append("(select tBack.Money,tBack.Money_Snow,tBack.Back_Accounts_Id ");
		sql.append(" from FINANCE_TICKET_ACCOUNTS tBack ");
		sql.append(" where tBack.Payment_Type in ('09','10') ");
		sql.append(" and tBack.input_time  < to_date('"+CommonMethod.timeToString(CommonMethod.addDay(stateObj.getEndDate(),1))+" 02:00:00','YYYY-MM-DD HH24:MI:SS') ");
		sql.append(" ) back ");
		sql.append(" on back.back_accounts_id = t.ticket_accounts_id ");
		sql.append(" where t.accounts_type in ("+accType+") and t.payment_type not in ('09','10') ");
		sql.append(" and ");
		sql.append(timeConditionThis);
		sql.append(" group by t.accounts_type,t.payment_type) this ");
		sql.append(" on (before.atb || before.ptb)=(this.att || this.ptt) ");
		SQLQuery query = session.createSQLQuery(sql.toString());
		query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		queryResult = query.list();
		
		//查询本期打印回传的冲退，在本期已消费的交易记录中显示
		//查询条件（本期）
		StringBuffer timeConditionThisBack = new StringBuffer();
		//添加传入的查询条件
		timeConditionThisBack.append(" and back.input_time >= to_date('");
		timeConditionThisBack.append(CommonMethod.timeToString(stateObj.getStartDate()));
		timeConditionThisBack.append(" 02:00:00','YYYY-MM-DD HH24:MI:SS') and back.input_time < to_date('");
		
		timeConditionThisBack.append(CommonMethod.timeToString(CommonMethod.addDay(stateObj.getEndDate(),1)));
		timeConditionThisBack.append(" 02:00:00','YYYY-MM-DD HH24:MI:SS') ");
		
		Map<String,Map> backMap = new HashMap<String,Map>();
		StringBuffer sqlBack = new StringBuffer();
		
		sqlBack.append(" select rBack.accounts_type att,rBack.payment_type ptt,sum(rBack.money-nvl(ret.money,0)) moneyT, ");
		sqlBack.append(" sum(rBack.money_snow-nvl(ret.money_snow,0)) moneySnowT from ");
		sqlBack.append(" (select back.ticket_indent_id,cast(back.accounts_type as varchar2(2)) accounts_type,cast(t.payment_type as varchar2(2)) payment_type,sum(back.money) money, ");
		sqlBack.append(" sum(back.money_snow) money_snow ");
		sqlBack.append(" from FINANCE_TICKET_ACCOUNTS t,FINANCE_TICKET_ACCOUNTS back ");
		sqlBack.append(" where back.accounts_type in ("+accType+") and back.payment_type in ('10') ");
		sqlBack.append(" and back.back_accounts_id = t.ticket_accounts_id ");
		sqlBack.append(" and ");
		sqlBack.append(timeConditionThis);
		sqlBack.append(timeConditionThisBack);
		sqlBack.append(" group by back.ticket_indent_id,back.accounts_type,t.payment_type) rBack ");
		sqlBack.append(" left join ");
		sqlBack.append(" (select cast(t.accounts_type as varchar2(2)) accounts_type,t.ticket_indent_id,case when t.payment_type = '07' then '01' ");
		sqlBack.append(" when t.payment_type = '08' then '02' ");
		sqlBack.append(" end payment_type,sum(t.money) money,sum(t.money_snow) money_snow ");
		sqlBack.append(" from FINANCE_TICKET_ACCOUNTS t where t.payment_type in ('08','07')  ");
		sqlBack.append(" and ");
		sqlBack.append(timeConditionThis);
		sqlBack.append(" group by t.accounts_type,t.ticket_indent_id,t.payment_type) ret ");
		sqlBack.append(" on rBack.ticket_indent_id = ret.ticket_indent_id and rBack.accounts_type = ret.accounts_type and rBack.payment_type = ret.payment_type ");
		sqlBack.append(" group by rBack.accounts_type,rBack.payment_type ");
		
		
		
		SQLQuery queryBack = session.createSQLQuery(sqlBack.toString());
		queryBack.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		List queryBackResult = queryBack.list();
		for (Object o : queryBackResult) {
			
			Map map = (Map)o;
			
			String aType = "";//accountType
			if(map.get("ATT")!=null){
				aType = map.get("ATT").toString();
			}
			String type = "";//accountType
			if(map.get("PTT")!=null){
				type = map.get("PTT").toString();
			}
			
			backMap.put("back_"+aType+"_"+type, map);
		}
		
		//查询网银支付退款到余额数据，在网银支付未消费中减去
		
		Map<String,Map> bankBanlMap = new HashMap<String,Map>();
		StringBuffer sqlBankBanl = new StringBuffer();
		
		sqlBankBanl.append(" select nvl(before.atb, this.att) att, ");
		sqlBankBanl.append(" nvl(before.ptb, this.ptt) ptt, ");
		sqlBankBanl.append(" nvl(before.moneyB, 0) moneyB, ");
		sqlBankBanl.append(" nvl(before.moneySnowB, 0) moneySnowB, ");
		sqlBankBanl.append(" nvl(this.moneyT, 0) moneyT, ");
		sqlBankBanl.append(" nvl(this.moneySnowT, 0) moneySnowT ");
		sqlBankBanl.append(" from (select cast(t.accounts_type as varchar2(2)) atb,cast(t.payment_type as varchar2(2)) ptb, ");
		sqlBankBanl.append(" sum(t.money) moneyB,sum(t.money_snow) moneySnowB ");
		sqlBankBanl.append(" from finance_ticket_accounts t ");
		sqlBankBanl.append(" where t.accounts_type in ("+accType+") and t.payment_type in ('07','08') ");
		sqlBankBanl.append(" and t.ticket_indent_id not in (select ftasub.ticket_indent_id from finance_ticket_accounts ftasub where ftasub.payment_type  in ('04','05')) ");
		sqlBankBanl.append(" and ");
		sqlBankBanl.append(timeConditionBefore);
		sqlBankBanl.append(" group by t.accounts_type, t.payment_type) before ");
		sqlBankBanl.append(" full join (select cast(t.accounts_type as varchar2(2)) att,cast(t.payment_type as varchar2(2)) ptt, ");
		sqlBankBanl.append(" sum(t.money) moneyT,sum(t.money_snow) moneySnowT ");
		sqlBankBanl.append(" from finance_ticket_accounts t ");
		sqlBankBanl.append(" where t.accounts_type in ("+accType+") and t.payment_type in ('07','08') ");
		//sqlBankBanl.append(" and t.ticket_indent_id not in (select ftasub.ticket_indent_id from finance_ticket_accounts ftasub where ftasub.payment_type  in ('04','05')) ");
		//排除支付和消费在同一天的
		
		//查询条件（本期）
		StringBuffer timeConditionThisDate = new StringBuffer();
		//添加传入的查询条件
		timeConditionThisDate.append(" ftasub.input_time >= to_date('");
		timeConditionThisDate.append(CommonMethod.timeToString(stateObj.getStartDate()));
		timeConditionThisDate.append(" 02:00:00','YYYY-MM-DD HH24:MI:SS') and ftasub.input_time < to_date('");
		
		timeConditionThisDate.append(CommonMethod.timeToString(CommonMethod.addDay(stateObj.getEndDate(),1)));
		timeConditionThisDate.append(" 02:00:00','YYYY-MM-DD HH24:MI:SS') ");
		timeConditionThisDate.append(rewardConditionSub);
		
		sqlBankBanl.append(" and t.ticket_indent_id not in (select xf.ticket_indent_id from ");
		sqlBankBanl.append(" (select ftasub.ticket_indent_id from finance_ticket_accounts ftasub where ftasub.payment_type  in ('04','05') and "+timeConditionThisDate+") xf ");
		sqlBankBanl.append(" inner join ");
		sqlBankBanl.append(" (select ftasub.ticket_indent_id from finance_ticket_accounts ftasub where ftasub.payment_type  in ('01','02') and "+timeConditionThisDate+") zf ");
		sqlBankBanl.append(" on xf.ticket_indent_id = zf.ticket_indent_id) ");
		
		sqlBankBanl.append(" and ");
		sqlBankBanl.append(timeConditionThis);
		sqlBankBanl.append(" group by t.accounts_type, t.payment_type) this ");
		sqlBankBanl.append(" on (before.atb || before.ptb) = (this.att || this.ptt) ");
		
		
		SQLQuery queryBankBanl = session.createSQLQuery(sqlBankBanl.toString());
		queryBankBanl.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		List queryBankBanlResult = queryBankBanl.list();
		for (Object o : queryBankBanlResult) {
			
			Map map = (Map)o;
			
			String aType = "";//accountType
			if(map.get("ATT")!=null){
				aType = map.get("ATT").toString();
			}
			String type = "";//accountType
			if(map.get("PTT")!=null){
				type = map.get("PTT").toString();
			}
			
			bankBanlMap.put("bankBanl_"+aType+"_"+type, map);
		}
		
		//查询初始化无对应订单的支付记录在网银已支付未消费金额中减去
		Map<String,Map> initiafMap = new HashMap<String,Map>();
		StringBuffer sqlInitiaf = new StringBuffer();
		
		sqlInitiaf.append(" select nvl(before.atb, this.att) att, ");
		sqlInitiaf.append(" nvl(before.ptb, this.ptt) ptt, ");
		sqlInitiaf.append(" nvl(before.moneyB, 0) moneyB, ");
		sqlInitiaf.append(" nvl(before.moneySnowB, 0) moneySnowB, ");
		sqlInitiaf.append(" nvl(this.moneyT, 0) moneyT, ");
		sqlInitiaf.append(" nvl(this.moneySnowT, 0) moneySnowT ");
		sqlInitiaf.append(" from (select cast(t.accounts_type as varchar2(2)) atb,cast(t.payment_type as varchar2(2)) ptb, ");
		sqlInitiaf.append(" sum(t.money) moneyB,sum(t.money_snow) moneySnowB ");
		sqlInitiaf.append(" from FINANCE_TICKET_ACCOUNTS t ");
		sqlInitiaf.append(" where t.accounts_type in ('02', '03') ");
		sqlInitiaf.append(" and t.payment_type in ('01') ");
		sqlInitiaf.append(" and t.ticket_indent_id is null ");
		sqlInitiaf.append(" and ");
		sqlInitiaf.append(timeConditionBefore);
		sqlInitiaf.append(" group by t.accounts_type, t.payment_type) before ");
		sqlInitiaf.append(" full join (select cast(t.accounts_type as varchar2(2)) att,cast(t.payment_type as varchar2(2)) ptt, ");
		sqlInitiaf.append(" sum(t.money) moneyT,sum(t.money_snow) moneySnowT ");
		sqlInitiaf.append(" from FINANCE_TICKET_ACCOUNTS t ");
		sqlInitiaf.append(" where t.accounts_type in ('02', '03') ");
		sqlInitiaf.append(" and t.payment_type in ('01') ");
		sqlInitiaf.append(" and t.ticket_indent_id is null ");
		sqlInitiaf.append(" and ");
		sqlInitiaf.append(timeConditionThis);
		sqlInitiaf.append(" group by t.accounts_type, t.payment_type) this ");
		sqlInitiaf.append(" on  (before.atb || before.ptb) = (this.att || this.ptt) ");
	

		
		SQLQuery queryInitiaf = session.createSQLQuery(sqlInitiaf.toString());
		queryInitiaf.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		List queryInitiafResult = queryInitiaf.list();
		for (Object o : queryInitiafResult) {
			
			Map map = (Map)o;
			
			String aType = "";//accountType
			if(map.get("ATT")!=null){
				aType = map.get("ATT").toString();
			}
			String type = "";//accountType
			if(map.get("PTT")!=null){
				type = map.get("PTT").toString();
			}
			
			initiafMap.put("initiaf_"+aType+"_"+type, map);
		}
		
		
		Map<String,AllStatementObject> tempMap = new HashMap<String,AllStatementObject>();
		
		for (Object o : queryResult) {
			
			Map map = (Map)o;
			
			String aType = "";//accountType
			if(map.get("ATYPE")!=null){
				aType = map.get("ATYPE").toString();
			}
			
			String type = "";//accountType
			if(map.get("PTYPE")!=null){
				type = map.get("PTYPE").toString();
			}
			
			
			AllStatementObject aso = new AllStatementObject();
			
			//设置顺序和行名
			int colNums=0;
			if("03".equals(aType)){//易宝
				colNums=2;
			}else if("04".equals(type)){
				colNums=1;
			}else if("05".equals(type)){
				colNums=3;
			}else if("01".equals(type)){
				colNums=4;
			}else if("02".equals(type)){
				colNums=5;
			}else if("03".equals(type)){
				colNums=6;
			}else if("07".equals(type) || "-1".equals(type)){
				colNums=7;
			}else if("08".equals(type)){
				colNums=8;
			}else if("06".equals(type)){
				colNums=9;
			}else if("11".equals(type)){
				colNums=11;
			}
			
			
			aso.setColNums(colNums);
			
			double previous= (map.get("MONEYB") == null ? new BigDecimal(0) : (BigDecimal)map.get("MONEYB")).doubleValue();
			double previousSnow= (map.get("MONEYSNOWB") == null ? new BigDecimal(0) : (BigDecimal)map.get("MONEYSNOWB")).doubleValue();
			double money= (map.get("MONEYT") == null ? new BigDecimal(0) : (BigDecimal)map.get("MONEYT")).doubleValue();
			double moneySnow= (map.get("MONEYSNOWT") == null ? new BigDecimal(0) : (BigDecimal)map.get("MONEYSNOWT")).doubleValue();
			
			if(colNums==7){//网银退款到余额显示为正
				previous = Math.abs(previous);
				previousSnow = Math.abs(previousSnow);
				money = Math.abs(money);
				moneySnow = Math.abs(moneySnow);
			}
			
			if("01".equals(type)){//网银支付未消费要减去网银退到余额的金额
				Map baTempMap = bankBanlMap.get("bankBanl_"+aType+"_07");
				if(baTempMap!=null){
					double previousB= (baTempMap.get("MONEYB") == null ? new BigDecimal(0) : (BigDecimal)baTempMap.get("MONEYB")).doubleValue();
					double previousSnowB= (baTempMap.get("MONEYSNOWB") == null ? new BigDecimal(0) : (BigDecimal)baTempMap.get("MONEYSNOWB")).doubleValue();
					double moneyB= (baTempMap.get("MONEYT") == null ? new BigDecimal(0) : (BigDecimal)baTempMap.get("MONEYT")).doubleValue();
					double moneySnowB= (baTempMap.get("MONEYSNOWT") == null ? new BigDecimal(0) : (BigDecimal)baTempMap.get("MONEYSNOWT")).doubleValue();
				
					//网银退到余额记录为负数所以用加
					previous += previousB;
					previousSnow += previousSnowB;
					money += moneyB;
					moneySnow += moneySnowB;
					
				}
				
				//减去初始化无对应订单数据
				Map inTempMap = initiafMap.get("initiaf_"+aType+"_"+type);
				if(inTempMap!=null){
					
					double previousI= (inTempMap.get("MONEYB") == null ? new BigDecimal(0) : (BigDecimal)inTempMap.get("MONEYB")).doubleValue();
					double previousSnowI= (inTempMap.get("MONEYSNOWB") == null ? new BigDecimal(0) : (BigDecimal)inTempMap.get("MONEYSNOWB")).doubleValue();
					double moneyI= (inTempMap.get("MONEYT") == null ? new BigDecimal(0) : (BigDecimal)inTempMap.get("MONEYT")).doubleValue();
					double moneySnowI= (inTempMap.get("MONEYSNOWT") == null ? new BigDecimal(0) : (BigDecimal)inTempMap.get("MONEYSNOWT")).doubleValue();
				
					previous = previous - previousI;
					previousSnow = previousSnow - previousSnowI;
					money = money - moneyI;
					moneySnow = moneySnow - moneySnowI;
				}
				
			}
			
			if("02".equals(type)){//余额支付未消费要减去余额退到余额的金额
				Map baTempMap = bankBanlMap.get("bankBanl_"+aType+"_08");
				if(baTempMap!=null){
					
					double previousB= (baTempMap.get("MONEYB") == null ? new BigDecimal(0) : (BigDecimal)baTempMap.get("MONEYB")).doubleValue();
					double previousSnowB= (baTempMap.get("MONEYSNOWB") == null ? new BigDecimal(0) : (BigDecimal)baTempMap.get("MONEYSNOWB")).doubleValue();
					double moneyB= (baTempMap.get("MONEYT") == null ? new BigDecimal(0) : (BigDecimal)baTempMap.get("MONEYT")).doubleValue();
					double moneySnowB= (baTempMap.get("MONEYSNOWT") == null ? new BigDecimal(0) : (BigDecimal)baTempMap.get("MONEYSNOWT")).doubleValue();
				
					//网银退到余额记录为负数所以用加
					previous += previousB;
					previousSnow += previousSnowB;
					money += moneyB;
					moneySnow += moneySnowB;
				}
			}
			
			
			aso.setPrevious(previous);
			aso.setPreviousSnow(previousSnow);
			
			if("04".equals(type) || "05".equals(type)){//本期消费放到消费栏中
				aso.setExpense(money);
				aso.setExpenseSnow(moneySnow);
				
				//本期交易金额为打印回传时的冲退金额
				
				String keyType = "";
				if("04".equals(type)){
					keyType = "01";
				}
				if("05".equals(type)){
					keyType = "02";
				}
				//TODO..
				Map baTempMap = backMap.get("back_"+aType+"_"+keyType);
				if(baTempMap!=null){
					double moneyB= Math.abs((baTempMap.get("MONEYT") == null ? new BigDecimal(0) : (BigDecimal)baTempMap.get("MONEYT")).doubleValue());
					double moneySnowB= Math.abs((baTempMap.get("MONEYSNOWT") == null ? new BigDecimal(0) : (BigDecimal)baTempMap.get("MONEYSNOWT")).doubleValue());
					aso.setTrade(moneyB);
					aso.setTradeSnow(moneySnowB);
				}
				
			}else{//其他放到交易栏
				aso.setTrade(money);
				aso.setTradeSnow(moneySnow);
			}
			
			
			AllStatementObject asoTemp = tempMap.get("colNums_"+colNums);
			if(asoTemp==null){
				tempMap.put("colNums_"+colNums, aso);
			}else{
				
				
				aso.setPrevious((aso.getPrevious() == null ? 0d : aso.getPrevious())+(asoTemp.getPrevious() == null ? 0d : asoTemp.getPrevious()));
				aso.setPreviousSnow((aso.getPreviousSnow() == null ? 0d : aso.getPreviousSnow())+(asoTemp.getPreviousSnow() == null ? 0d : asoTemp.getPreviousSnow()));
				
				aso.setExpense((aso.getExpense() == null ? 0d : aso.getExpense())+(asoTemp.getExpense() == null ? 0d : asoTemp.getExpense()));
				aso.setExpenseSnow((aso.getExpenseSnow() == null ? 0d : aso.getExpenseSnow())+(asoTemp.getExpenseSnow() == null ? 0d : asoTemp.getExpenseSnow()));
				
				aso.setTrade((aso.getTrade() == null ? 0d : aso.getTrade())+(asoTemp.getTrade() == null ? 0d : asoTemp.getTrade()));
				aso.setTradeSnow((aso.getTradeSnow() == null ? 0d : aso.getTradeSnow())+(asoTemp.getTradeSnow() == null ? 0d : asoTemp.getTradeSnow()));
				
				tempMap.put("colNums_"+colNums, aso);
			}
			//pagerRoot.add(aso);
		}
		//System.out.println(tempMap);
		for(AllStatementObject aso : pagerRoot){
			
			AllStatementObject tempAso = tempMap.get("colNums_"+aso.getColNums());
			if(tempAso!=null){
				aso.putValues(tempAso);
			}
			
		}
		
		
		
		if("01".equals(stateObj.getPayType())){//预付款
			//3, "余额支付已消费金额"
			AllStatementObject aso3 = pagerRoot.get(0);
			//5, "余额已支付未消费金额"
			AllStatementObject aso5 = pagerRoot.get(1);
			//6, "现金充值金额"
			AllStatementObject aso6 = pagerRoot.get(2);
			//10, "合计"
			AllStatementObject aso10 = pagerRoot.get(3);
					
			//计算合计行
			//上期余额
			double previous10 = (aso6.getPrevious()==null ? 0 : aso6.getPrevious()) - (aso3.getPrevious()==null ? 0 : aso3.getPrevious());
			aso10.setPrevious(previous10);
			
			double previousSnow10 = (aso6.getPreviousSnow()==null ? 0 : aso6.getPreviousSnow()) - (aso3.getPreviousSnow()==null ? 0 : aso3.getPreviousSnow());
			aso10.setPreviousSnow(previousSnow10);
			
			//本期交易
			double trade10 = (aso6.getTrade()==null ? 0 : aso6.getTrade());
			aso10.setTrade(trade10);
			
			double tradeSnow10 = (aso6.getTradeSnow()==null ? 0 : aso6.getTradeSnow());
			aso10.setTradeSnow(tradeSnow10);
			
			//本期消费
			double expense10 = (aso3.getExpense()==null ? 0 : aso3.getExpense());
			aso10.setExpense(expense10);
			
			double expenseSnow10 = (aso3.getExpenseSnow()==null ? 0 : aso3.getExpenseSnow());
			aso10.setExpenseSnow(expenseSnow10);
			
			
			
		}else if("02".equals(stateObj.getPayType())){//奖励款
			
			//3, "余额支付已消费金额"
			AllStatementObject aso3 = pagerRoot.get(0);
			//5, "余额已支付未消费金额"
			AllStatementObject aso5 = pagerRoot.get(1);
			//6, "现金充值金额"
			AllStatementObject aso6 = pagerRoot.get(2);
			//10, "合计"
			AllStatementObject aso10 = pagerRoot.get(3);
					
			//计算合计行
			//上期余额
			double previous10 = (aso6.getPrevious()==null ? 0 : aso6.getPrevious()) - (aso3.getPrevious()==null ? 0 : aso3.getPrevious());
			aso10.setPrevious(previous10);
			
			double previousSnow10 = (aso6.getPreviousSnow()==null ? 0 : aso6.getPreviousSnow()) - (aso3.getPreviousSnow()==null ? 0 : aso3.getPreviousSnow());
			aso10.setPreviousSnow(previousSnow10);
			
			//本期交易
			double trade10 = (aso6.getTrade()==null ? 0 : aso6.getTrade());
			aso10.setTrade(trade10);
			
			double tradeSnow10 = (aso6.getTradeSnow()==null ? 0 : aso6.getTradeSnow());
			aso10.setTradeSnow(tradeSnow10);
			
			//本期消费
			double expense10 = (aso3.getExpense()==null ? 0 : aso3.getExpense());
			aso10.setExpense(expense10);
			
			double expenseSnow10 = (aso3.getExpenseSnow()==null ? 0 : aso3.getExpenseSnow());
			aso10.setExpenseSnow(expenseSnow10);
			
		}else if("mobile".equals(stateObj.getPayType()) || "05".equals(stateObj.getPayType()) || "06".equals(stateObj.getPayType())){//移动终端
			
			//1, "网银支付已消费金额"
			AllStatementObject aso1 = pagerRoot.get(0);
			//4, "网银已支付未消费金额"
			AllStatementObject aso4 = pagerRoot.get(1);
			//9, "退款到卡上金额"
			AllStatementObject aso9 = pagerRoot.get(2);
			//10, "合计"
			AllStatementObject aso10 = pagerRoot.get(3);
			
			for(AllStatementObject aso : pagerRoot){
				int cNums = aso.getColNums();
				if(cNums!=10){//排除合计行
					//上期余额
					if(aso.isPreviousShow()){
						aso10.setPrevious((aso10.getPrevious()==null ? 0 : aso10.getPrevious())+(aso.getPrevious()==null ? 0 : aso.getPrevious()));
						aso10.setPreviousSnow((aso10.getPreviousSnow()==null ? 0 : aso10.getPreviousSnow())+(aso.getPreviousSnow()==null ? 0 : aso.getPreviousSnow()));
					}
					//本期消费
					if(aso.isExpenseShow()){
						aso10.setExpense((aso10.getExpense()==null ? 0 : aso10.getExpense())+(aso.getExpense()==null ? 0 : aso.getExpense()));
						aso10.setExpenseSnow((aso10.getExpenseSnow()==null ? 0 : aso10.getExpenseSnow())+(aso.getExpenseSnow()==null ? 0 : aso.getExpenseSnow()));
					}
					//本期交易
					if(aso.isTradeShow()){
						aso10.setTrade((aso10.getTrade()==null ? 0 : aso10.getTrade())+(aso.getTrade()==null ? 0 : aso.getTrade()));
						aso10.setTradeSnow((aso10.getTradeSnow()==null ? 0 : aso10.getTradeSnow())+(aso.getTradeSnow()==null ? 0 : aso.getTradeSnow()));
					}
					//本期余额
					if(aso.isCurrentShow()){
						aso10.setCurrent((aso10.getCurrent()==null ? 0 : aso10.getCurrent())+(aso.getCurrent()==null ? 0 : aso.getCurrent()));
						aso10.setCurrentSnow((aso10.getCurrentSnow()==null ? 0 : aso10.getCurrentSnow())+(aso.getCurrentSnow()==null ? 0 : aso.getCurrentSnow()));
					}
				}
			}
			
		}else{//现金
			//1, "网银支付已消费金额"
			AllStatementObject aso1 = pagerRoot.get(0);
			//2, "易宝支付已消费金额"
			AllStatementObject aso2 = pagerRoot.get(1);
			//3, "余额支付已消费金额"
			AllStatementObject aso3 = pagerRoot.get(2);
			//4, "网银已支付未消费金额"
			AllStatementObject aso4 = pagerRoot.get(3);
			//5, "余额已支付未消费金额"
			AllStatementObject aso5 = pagerRoot.get(4);
			//6, "现金充值金额"
			//AllStatementObject aso6 = pagerRoot.get(5);
			//7, "网银付款退款到余额"
			AllStatementObject aso7 = pagerRoot.get(5);
			//8, "余额付款退款到余额"
			//AllStatementObject aso8 = pagerRoot.get(6);
			//9, "退款到卡上金额"
			AllStatementObject aso9 = pagerRoot.get(6);
			//10, "合计"
			AllStatementObject aso10 = pagerRoot.get(7);
					
			
			//网银退款到余额本期余额计算
			double current = (aso7.getPrevious()==null ? 0 : aso7.getPrevious())+(aso7.getTrade()==null ? 0 : aso7.getTrade())-(aso3.getPrevious()==null ? 0 : aso3.getPrevious())-(aso3.getExpense()==null ? 0 : aso3.getExpense());
			aso7.setCurrent(current);
			double currentSnow = (aso7.getPreviousSnow()==null ? 0 : aso7.getPreviousSnow())+(aso7.getTradeSnow()==null ? 0 : aso7.getTradeSnow())-(aso3.getPreviousSnow()==null ? 0 : aso3.getPreviousSnow())-(aso3.getExpenseSnow()==null ? 0 : aso3.getExpenseSnow());
			aso7.setCurrentSnow(currentSnow);
			
			
			//计算合计行
			
			//上期余额
			double previous10 = (aso1.getExpense()==null ? 0 : aso1.getExpense()) - (aso1.getTrade()==null ? 0 : aso1.getTrade());
			previous10 += (aso4.getPrevious()==null ? 0 : aso4.getPrevious()) + (aso7.getPrevious()==null ? 0 : aso7.getPrevious())+(aso9.getPrevious()==null ? 0 : aso9.getPrevious());
			previous10 = previous10 - (aso3.getPrevious()==null ? 0 : aso3.getPrevious());
			aso10.setPrevious(previous10);
			
			double previousSnow10 = (aso1.getExpenseSnow()==null ? 0 : aso1.getExpenseSnow()) - (aso1.getTradeSnow()==null ? 0 : aso1.getTradeSnow());
			previousSnow10 += (aso4.getPreviousSnow()==null ? 0 : aso4.getPreviousSnow()) + (aso7.getPreviousSnow()==null ? 0 : aso7.getPreviousSnow())+(aso9.getPreviousSnow()==null ? 0 : aso9.getPreviousSnow());
			previousSnow10 = previousSnow10 - (aso3.getPreviousSnow()==null ? 0 : aso3.getPreviousSnow());
			aso10.setPreviousSnow(previousSnow10);
			
			//本期交易
			double trade10 = (aso1.getTrade()==null ? 0 : aso1.getTrade()) + (aso2.getTrade()==null ? 0 : aso2.getTrade()) +
				(aso4.getTrade()==null ? 0 : aso4.getTrade()) + (aso7.getTrade()==null ? 0 : aso7.getTrade()) +(aso9.getTrade()==null ? 0 : aso9.getTrade());
			aso10.setTrade(trade10);
			
			double tradeSnow10 = (aso1.getTradeSnow()==null ? 0 : aso1.getTradeSnow()) + (aso2.getTradeSnow()==null ? 0 : aso2.getTradeSnow()) +
			(aso4.getTradeSnow()==null ? 0 : aso4.getTradeSnow()) + (aso7.getTradeSnow()==null ? 0 : aso7.getTradeSnow()) +(aso9.getTradeSnow()==null ? 0 : aso9.getTradeSnow());
			aso10.setTradeSnow(tradeSnow10);
			
			//本期消费
			for(AllStatementObject aso : pagerRoot){
				int cNums = aso.getColNums();
				if(cNums!=10){//排除合计行
					//本期消费
					if(aso.isExpenseShow()){
						aso10.setExpense((aso10.getExpense()==null ? 0 : aso10.getExpense())+(aso.getExpense()==null ? 0 : aso.getExpense()));
						aso10.setExpenseSnow((aso10.getExpenseSnow()==null ? 0 : aso10.getExpenseSnow())+(aso.getExpenseSnow()==null ? 0 : aso.getExpenseSnow()));
					}
				}
			}
			
			//本期余额
			double current10 = (aso4.getCurrent()==null ? 0 : aso4.getCurrent()) + (aso7.getCurrent()==null ? 0 : aso7.getCurrent()) +
				(aso9.getCurrent()==null ? 0 : aso9.getCurrent()) - (aso3.getExpense()==null ? 0 : aso3.getExpense());
			aso10.setCurrent(current10);
			
			double currentSnow10 = (aso4.getCurrentSnow()==null ? 0 : aso4.getCurrentSnow()) + (aso7.getCurrentSnow()==null ? 0 : aso7.getCurrentSnow()) +
				(aso9.getCurrentSnow()==null ? 0 : aso9.getCurrentSnow()) - (aso3.getExpenseSnow()==null ? 0 : aso3.getExpenseSnow());
			aso10.setCurrentSnow(currentSnow10);
		}
		
		return pagerRoot;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<StatementObjectForPage>  findTotalStatement(final StatementObject stateObj) {
//		List<StatementObjectForPage>  returnObj = getHibernateTemplate().executeFind(new HibernateCallback() {
//			@Override
//			public List<StatementObjectForPage> doInHibernate(Session session) throws HibernateException, SQLException {
//				List<StatementObject> pagerRoot = new ArrayList<StatementObject>();
//
//				//记录数据存放顺序
//				List<String> dataOrder = new ArrayList<String>();
//				dataOrder.add("网银支付已消费金额");
//				dataOrder.add("易宝支付已消费金额");
//				dataOrder.add("余额支付已消费金额");
//				dataOrder.add("网银已支付未消费金额");
//				dataOrder.add("余额已支付未消费金额");
//				dataOrder.add("现金充值金额");
//				dataOrder.add("网银付款退款到余额");
//				dataOrder.add("余额付款退款到余额");
//				//dataOrder.add("退款到余额");
//				dataOrder.add("退款到卡上金额");
//				dataOrder.add("合计");
//				
//				//查询条件（上期）
//				StringBuffer timeConditionBefore = new StringBuffer();
//				//添加传入的查询条件
//				timeConditionBefore.append(" t.input_time < to_date(substr('");
//				timeConditionBefore.append(CommonMethod.toStartTime(stateObj.getStartDate()));
//				timeConditionBefore.append("',0,19),'YYYY-MM-DD HH24:MI:SS') ");
//				//查询条件（本期）
//				StringBuffer timeConditionThis = new StringBuffer();
//				//添加传入的查询条件
//				timeConditionThis.append(" t.input_time >= to_date(substr('");
//				timeConditionThis.append(CommonMethod.toStartTime(stateObj.getStartDate()));
//				timeConditionThis.append("',0,19),'YYYY-MM-DD HH24:MI:SS') and t.input_time <= to_date(substr('");
//				timeConditionThis.append(CommonMethod.toEndTime(stateObj.getEndDate()));
//				timeConditionThis.append("',0,19),'YYYY-MM-DD HH24:MI:SS') ");
//				
//				//SQL
//				StringBuffer sql = new StringBuffer();
//				List queryResult;
//				sql.append(" select nvl(before.atb,this.att) aType,nvl(before.ptb,this.ptt) pType, ");
//				sql.append(" nvl(before.moneyB,0) moneyB,nvl(before.moneySnowB,0) moneySnowB, ");
//				sql.append(" nvl(this.moneyT,0) moneyT,nvl(this.moneySnowT,0) moneySnowT from ");
//				sql.append("(select t.accounts_type atb,t.payment_type ptb,sum(t.money+nvl(back.money,0)) moneyB,sum(t.money_snow+nvl(back.money_snow,0)) moneySnowB");
//				sql.append(" from FINANCE_TICKET_ACCOUNTS t");
//				sql.append(" left join ");
//				sql.append("(select tBack.Money,tBack.Money_Snow,tBack.Back_Accounts_Id ");
//				sql.append(" from FINANCE_TICKET_ACCOUNTS tBack ");
//				sql.append(" where tBack.Payment_Type = '09' ");
//				sql.append(" ) back ");
//				sql.append(" on back.back_accounts_id = t.ticket_accounts_id ");
//				sql.append(" where t.accounts_type in ('02','03') and t.payment_type not in ('09') ");
//				sql.append(" and ");
//				sql.append(timeConditionBefore);
//				sql.append(" group by t.accounts_type,t.payment_type ) before");
//				
//				sql.append(" full join ");
//				
//				sql.append("(select t.accounts_type att,t.payment_type ptt,sum(t.money+nvl(back.money,0)) moneyT,sum(t.money_snow+nvl(back.money_snow,0)) moneySnowT");
//				sql.append(" from FINANCE_TICKET_ACCOUNTS t");
//				sql.append(" left join ");
//				sql.append("(select tBack.Money,tBack.Money_Snow,tBack.Back_Accounts_Id ");
//				sql.append(" from FINANCE_TICKET_ACCOUNTS tBack ");
//				sql.append(" where tBack.Payment_Type = '09' ");
//				sql.append(" ) back ");
//				sql.append(" on back.back_accounts_id = t.ticket_accounts_id ");
//				sql.append(" where t.accounts_type in ('02','03') and t.payment_type not in ('09') ");
//				sql.append(" and ");
//				sql.append(timeConditionThis);
//				sql.append(" group by t.accounts_type,t.payment_type) this ");
//				sql.append(" on (before.atb || before.ptb)=(this.att || this.ptt) ");
//				SQLQuery query = session.createSQLQuery(sql.toString());
//				query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
//				queryResult = query.list();
//				Map<String, StatementObject> temp = new HashMap<String, StatementObject>();
//				for (Object o : queryResult) {
//					Map map = (Map)o;
//					String type ="";//paymentType
//					
//					String aType = "";//accountType
//					if(map.get("ATYPE")!=null){
//						aType = map.get("ATYPE").toString();;
//					}
//					
//					if("03".equals(aType)){//易宝交易
//						type = "x";
//					}else{
//						if(map.get("PTYPE")!=null){
//							type = map.get("PTYPE").toString();;
//						}
//					}
//					
//					StatementObject statementObject = new StatementObject(type);
//					Double s1 = 0D;// 雪山公司上期余额
//					Double s2 = 0D;//股份公司上期余额
//					Double s3 = 0D;//雪山公司本期交易
//					Double s4 = 0D;//股份公司本期交易
//					Double countOther = 0D;//上期余额合计
//				//1.上期余额
//					//1.1 雪山公司上期余额
//					s1 = (map.get("MONEYSNOWB") == null ? new BigDecimal(0) : (BigDecimal)map.get("MONEYSNOWB")).doubleValue();
//					statementObject.setTableRow1(s1);
//					//1.2 股份公司上期余额
//					s2= (map.get("MONEYB") == null ? new BigDecimal(0) : (BigDecimal)map.get("MONEYB")).doubleValue();
//					statementObject.setTableRow2(s2);
//					//1.3上期余额合计(股份公司上期余额+雪山公司上期余额)
//					countOther = (s1 == null ? 0D : s1) + (s2 == null ? 0D : s2);
//					statementObject.setTableRow3(countOther);
//				//2.本期交易
//					//2.1 雪山公司本期交易
//					s3= (map.get("MONEYSNOWT") == null ? new BigDecimal(0) : (BigDecimal)map.get("MONEYSNOWT")).doubleValue();
//					statementObject.setTableRow4(s3);
//					//2.2 股份公司本期交易
//					s4= (map.get("MONEYT") == null ? new BigDecimal(0) : (BigDecimal)map.get("MONEYT")).doubleValue();
//					statementObject.setTableRow5(s4);
//					//2.3 本期交易合计
//					Double countThis = (s3 == null ? 0D : s3) + (s4 == null ? 0D : s4);
//					statementObject.setTableRow6(countThis);
//				//3.本期消费
//					if("05".equals(type)){
//						s1 = 0D;
//						s2 = 0D;
//					}
//					//3.1雪山公司本期消费
//					statementObject.setTableRow7(s1+s3);
//					//3.2股份公司本期消费
//					statementObject.setTableRow8(s2+s4);
//					//3.3本期消费合计
//					statementObject.setTableRow9(s1+s3+s2+s4);
//				//4.本期余额
//					//4.1雪山公司本期余额
//					statementObject.setTableRow10(s1+s3);
//					//4.2股份公司本期余额
//					statementObject.setTableRow11(s2+s4);
//					//4.3本期余额合计
//					statementObject.setTableRow12(s1+s3+s2+s4);
//					temp.put(type, statementObject);
//				}
//				
//				
//				for (String tbCol : dataOrder) {
//					
//					if("网银已支付未消费金额".trim().equals(tbCol.trim())){
//						StatementObject statementObject01 = temp.get("01");
//						if(statementObject01!=null){
//							statementObject01.setTableColumn(tbCol);
//							statementObject01.setTableRow10(statementObject01.getTableRow1()+statementObject01.getTableRow4());
//							statementObject01.setTableRow11(statementObject01.getTableRow2()+statementObject01.getTableRow5());
//							statementObject01.setTableRow12(statementObject01.getTableRow3()+statementObject01.getTableRow6());
//							pagerRoot.add(statementObject01);
//						}else{
//							pagerRoot.add(new StatementObject(tbCol));
//						}
//						continue;
//					}
//					
//					if("网银付款退款到余额".trim().equals(tbCol.trim())){
//						StatementObject statementObject07 = temp.get("07");
//						if(statementObject07!=null){
//							statementObject07.setTableColumn(tbCol);
//							statementObject07.setTableRow1(abs(statementObject07.getTableRow1()));
//							statementObject07.setTableRow2(abs(statementObject07.getTableRow2()));
//							statementObject07.setTableRow3(abs(statementObject07.getTableRow3()));
//							statementObject07.setTableRow4(abs(statementObject07.getTableRow4()));
//							statementObject07.setTableRow5(abs(statementObject07.getTableRow5()));
//							statementObject07.setTableRow6(abs(statementObject07.getTableRow6()));
//							statementObject07.setTableRow7(abs(statementObject07.getTableRow7()));
//							statementObject07.setTableRow8(abs(statementObject07.getTableRow8()));
//							statementObject07.setTableRow9(abs(statementObject07.getTableRow9()));
//							statementObject07.setTableRow10(abs(statementObject07.getTableRow10()));
//							statementObject07.setTableRow11(abs(statementObject07.getTableRow11()));
//							statementObject07.setTableRow12(abs(statementObject07.getTableRow12()));
//							pagerRoot.add(statementObject07);
//							}else{
//								pagerRoot.add(new StatementObject(tbCol));
//							}
//							continue;
//					}
//					
//					if("余额付款退款到余额".trim().equals(tbCol.trim())){
//						StatementObject statementObject08 = temp.get("08");
//						if(statementObject08!=null){
//							statementObject08.setTableColumn(tbCol);
//							statementObject08.setTableRow1(abs(statementObject08.getTableRow1()));
//							statementObject08.setTableRow2(abs(statementObject08.getTableRow2()));
//							statementObject08.setTableRow3(abs(statementObject08.getTableRow3()));
//							statementObject08.setTableRow4(abs(statementObject08.getTableRow4()));
//							statementObject08.setTableRow5(abs(statementObject08.getTableRow5()));
//							statementObject08.setTableRow6(abs(statementObject08.getTableRow6()));
//							statementObject08.setTableRow7(abs(statementObject08.getTableRow7()));
//							statementObject08.setTableRow8(abs(statementObject08.getTableRow8()));
//							statementObject08.setTableRow9(abs(statementObject08.getTableRow9()));
//							statementObject08.setTableRow10(abs(statementObject08.getTableRow10()));
//							statementObject08.setTableRow11(abs(statementObject08.getTableRow11()));
//							statementObject08.setTableRow12(abs(statementObject08.getTableRow12()));
//							pagerRoot.add(statementObject08);
//						}else{
//							pagerRoot.add(new StatementObject(tbCol));
//						}
//						continue;
//					}
//					
//					if("现金充值金额".trim().equals(tbCol.trim())){
//						StatementObject statementObject = temp.get("03");
//						StatementObject statementObject05 = (temp.get("05") == null ? new StatementObject("05") : temp.get("05"));
//						
//						if(statementObject!=null){
//							statementObject.setTableColumn(tbCol);
//							Double tableRow10 = statementObject.getTableRow1()-statementObject05.getTableRow1()+statementObject.getTableRow4();
//							statementObject.setTableRow10(tableRow10);
//							Double tableRow11 = statementObject.getTableRow2()-statementObject05.getTableRow2()+statementObject.getTableRow5();
//							statementObject.setTableRow11(tableRow11);
//							Double tableRow12 = statementObject.getTableRow3()-statementObject05.getTableRow3()+statementObject.getTableRow6();
//							statementObject.setTableRow12(tableRow12);
//							pagerRoot.add(statementObject);
//						}else{
//							statementObject = new StatementObject(tbCol);
//							pagerRoot.add(statementObject);
//						}
//						continue;
//					}
//					
//					
//					if("合计".equals(tbCol.trim())){//计算合计行
//						StatementObject statementObject29 = (temp.get("03")==null ? new StatementObject() : temp.get("03"));
//						StatementObject statementObject27 = (temp.get("01") == null ? new StatementObject() : temp.get("01"));
//						StatementObject statementObject32 = (temp.get("06")== null ? new StatementObject() : temp.get("06"));
//						StatementObject statementObject26 = (temp.get("05")== null ? new StatementObject() : temp.get("05"));
//						StatementObject statementObject24 = (temp.get("04")== null ? new StatementObject() : temp.get("04"));
//						StatementObject statementObject25 = (temp.get("x")== null ? new StatementObject() : temp.get("x"));
//						
//						StatementObject statementObject = new StatementObject();
//						statementObject.setTableColumn(tbCol);
//						Double tableRow1 = statementObject24.getTableRow4()-statementObject24.getTableRow7()+statementObject29.getTableRow1()+statementObject27.getTableRow1()+statementObject32.getTableRow1()-statementObject26.getTableRow1();
//						statementObject.setTableRow1(tableRow1);
//						Double tableRow2 = statementObject24.getTableRow5()-statementObject24.getTableRow8()+statementObject29.getTableRow2()+statementObject27.getTableRow2()+statementObject32.getTableRow2()-statementObject26.getTableRow2();
//						statementObject.setTableRow2(tableRow2);
//						Double tableRow3 = statementObject24.getTableRow6()-statementObject24.getTableRow9()+statementObject29.getTableRow3()+statementObject27.getTableRow3()+statementObject32.getTableRow3()-statementObject26.getTableRow3();
//						statementObject.setTableRow3(tableRow3);
//						Double tableRow4 = statementObject24.getTableRow4()+statementObject25.getTableRow4()+statementObject27.getTableRow4()+statementObject29.getTableRow4()+statementObject32.getTableRow4();
//						statementObject.setTableRow4(tableRow4);
//						Double tableRow5 = statementObject24.getTableRow5()+statementObject25.getTableRow5()+statementObject27.getTableRow5()+statementObject29.getTableRow5()+statementObject32.getTableRow5();
//						statementObject.setTableRow5(tableRow5);
//						Double tableRow6 = tableRow4+tableRow5;
//						statementObject.setTableRow6(tableRow6);
//						Double tableRow7 = statementObject24.getTableRow7()+statementObject25.getTableRow7()+statementObject26.getTableRow7();
//						statementObject.setTableRow7(tableRow7);
//						Double tableRow8 = statementObject24.getTableRow8()+statementObject25.getTableRow8()+statementObject26.getTableRow8();
//						statementObject.setTableRow8(tableRow8);
//						Double tableRow9 = statementObject24.getTableRow9()+statementObject25.getTableRow9()+statementObject26.getTableRow9();
//						statementObject.setTableRow9(tableRow9);
//						Double tableRow10 = statementObject29.getTableRow10()+statementObject27.getTableRow10()+statementObject32.getTableRow10()-statementObject26.getTableRow7();
//						statementObject.setTableRow10(tableRow10);
//						Double tableRow11 = statementObject29.getTableRow11()+statementObject27.getTableRow11()+statementObject32.getTableRow11()-statementObject26.getTableRow8();
//						statementObject.setTableRow11(tableRow11);
//						Double tableRow12 = statementObject29.getTableRow12()+statementObject27.getTableRow12()+statementObject32.getTableRow12()-statementObject26.getTableRow9();
//						statementObject.setTableRow12(tableRow12);
//						pagerRoot.add(statementObject);
//						continue;
//					}
//					
//					StatementObject statementObject = temp.get(statementColumn.get(tbCol));
//					if(statementObject==null){
//						statementObject = new StatementObject(tbCol);
//					}else{
//						statementObject.setTableColumn(tbCol);
//					}
//					pagerRoot.add(statementObject);
//				}
//				List<StatementObjectForPage> pagedata = new ArrayList<StatementObjectForPage>();
//				
//				//转换StatementObject--->StatementObjectForPage
//				for (StatementObject o : pagerRoot) {
//					pagedata.add(ObjToPageObj(o,false));
//				}
//				
//				//控制显示
//				for (StatementObjectForPage o : pagedata) {
//					String colName = o.getTableColumn().trim();
//					
//					if("易宝支付已消费金额".equals(colName) || "退款到卡上金额".equals(colName)){
//						o.setTableRow1("");
//						o.setTableRow2("");
//						o.setTableRow3("");
//					}
//					
//					if("网银已支付未消费金额".equals(colName) || "余额已支付未消费金额".equals(colName) ||"现金充值金额".equals(colName) 
//						|| "退款到卡上金额".equals(colName) || "网银付款退款到余额".equals(colName) || "余额付款退款到余额".equals(colName)  
//					){
//						//3.1雪山公司本期消费
//						o.setTableRow7("");
//						//3.2股份公司本期消费
//						o.setTableRow8("");
//						//3.3本期消费合计
//						o.setTableRow9("");
//					}
//				//4.本期余额
//					if("易宝支付已消费金额".equals(colName) ||"网银支付已消费金额".equals(colName) || "余额支付已消费金额".equals(colName)){//04网银支付已消费金额  05 余额支付已消费金额
//						
//						//4.1雪山公司本期余额
//						o.setTableRow10("");
//						//4.2股份公司本期余额
//						o.setTableRow11("");
//						//4.3本期余额合计
//						o.setTableRow12("");
//					}
//				}
//				return pagedata;
//			}
//		});
//		return returnObj;
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<StatementObjectForPage> findDeatilStatement(final StatementObject statementObject) {
		final Map<String, String> ticketType = new HashMap<String, String>();
		ticketType.put("01", "门票");
		ticketType.put("02", "索道票");
		ticketType.put("03", "观景索道票");
		ticketType.put("04", "滑雪票");
		ticketType.put("05", "雪地摩托票");
		ticketType.put("06", "滑草票");
		ticketType.put("x", "初始化数据");
		
		Session session = sessionFactory.getCurrentSession();
		List<StatementObject> pagerRoot = new ArrayList<StatementObject>();
		String detailName = statementObject.getTableColumn();//获取明细名称
		if("已消费金额".equals(detailName)){
			detailName="余额支付已消费金额";
		}
		if("已支付未消费金额".equals(detailName)){
			detailName="余额已支付未消费金额";
		}
		if("支付已消费金额".equals(detailName)){
			detailName="网银支付已消费金额";
		}
		if("支付未消费金额".equals(detailName)){
			detailName="网银已支付未消费金额";
		}
		String detailPaymentType = statementColumn.get(detailName);
		String accType;
		if("01".equals(statementObject.getPayType())){//预付款
			accType = "'04'";
		}else if("02".equals(statementObject.getPayType())){//奖励款
			accType = "'01'";
		}else if("mobile".equals(statementObject.getPayType())){
			accType = "'05','06'";
		}else if("05".equals(statementObject.getPayType())){
			accType = "'05'";
		}else if("06".equals(statementObject.getPayType())){
			accType = "'06'";
		}else{//现金
			accType = "'02','03'";
		}
		//查询奖励款类型
		StringBuffer rewardCondition = new StringBuffer();
		StringBuffer rewardConditionSub = new StringBuffer();
		if(statementObject.getRewardTypeId()!=null){
			rewardCondition.append(" and t.recharge_rec_id in ( ");
			rewardCondition.append(" select brr.recharge_rec_id from BASE_RECHARGE_REC brr where brr.reward_account_id in ( ");
			rewardCondition.append(" select bra.reward_account_id from BASE_REWARD_ACCOUNT bra where bra.reward_type_id = "+statementObject.getRewardTypeId());
			rewardCondition.append(" )) ");
			
			rewardConditionSub.append(" and ftasub.recharge_rec_id in ( ");
			rewardConditionSub.append(" select brr.recharge_rec_id from BASE_RECHARGE_REC brr where brr.reward_account_id in ( ");
			rewardConditionSub.append(" select bra.reward_account_id from BASE_REWARD_ACCOUNT bra where bra.reward_type_id = "+statementObject.getRewardTypeId());
			rewardConditionSub.append(" )) ");
		}
		
		//查询条件（上期）
		StringBuffer timeConditionBefore = new StringBuffer();
		//添加传入的查询条件
		timeConditionBefore.append(" t.input_time < to_date('");
		timeConditionBefore.append(CommonMethod.timeToString(statementObject.getStartDate()));
		timeConditionBefore.append(" 02:00:00','YYYY-MM-DD HH24:MI:SS') ");
		
		timeConditionBefore.append(rewardCondition);
		
		//查询条件（本期）
		StringBuffer timeConditionThis = new StringBuffer();
		//添加传入的查询条件
		timeConditionThis.append(" t.input_time >= to_date('");
		timeConditionThis.append(CommonMethod.timeToString(statementObject.getStartDate()));
		timeConditionThis.append(" 02:00:00','YYYY-MM-DD HH24:MI:SS') and t.input_time < to_date('");
		
		timeConditionThis.append(CommonMethod.timeToString(CommonMethod.addDay(statementObject.getEndDate(),1)));
		timeConditionThis.append(" 02:00:00','YYYY-MM-DD HH24:MI:SS') ");
		
		timeConditionThis.append(rewardCondition);
		
		/*查询网银支付退款到余额数据，在网银支付未消费中减去*/
		StringBuffer E_bankBack = new StringBuffer();
		//att   ptt	ttt 	moneyB	moneySnowB	amountB	moneyT	moneySnowT	amountT
		E_bankBack.append(" select nvl(before.atb, this.att) att,");
		E_bankBack.append(" nvl(before.ptb, this.ptt) ptt,");
		E_bankBack.append(" nvl(before.ttb,this.ttt) ttt,");
		E_bankBack.append(" nvl(before.moneyB, 0) moneyB,");
		E_bankBack.append(" nvl(before.moneySnowB, 0) moneySnowB,");
		E_bankBack.append(" nvl(before.amountB,0) amountB,");
		E_bankBack.append(" nvl(this.moneyT, 0) moneyT,");
		E_bankBack.append(" nvl(this.moneySnowT, 0) moneySnowT,");
		E_bankBack.append(" nvl(this.amountT,0) amountT");
		E_bankBack.append(" from (select cast(t.accounts_type as varchar2(2)) atb,");
		E_bankBack.append(" cast(t.payment_type as varchar2(2)) ptb,");
		E_bankBack.append(" cast(t.ticket_type as varchar2(2)) ttb,");
		E_bankBack.append(" sum(t.money) moneyB,");
		E_bankBack.append(" sum(t.money_snow) moneySnowB,");
		E_bankBack.append(" sum(t.amount) amountB");
		E_bankBack.append(" from finance_ticket_accounts t");
		E_bankBack.append(" where t.accounts_type in ("+accType+")");
		E_bankBack.append(" and t.payment_type in ('07', '08')");
		E_bankBack.append(" and t.ticket_indent_id not in");
		E_bankBack.append(" (select ftasub.ticket_indent_id");
		E_bankBack.append(" from finance_ticket_accounts ftasub");
		E_bankBack.append(" where ftasub.payment_type in ('04', '05'))");
		E_bankBack.append(" and");
		E_bankBack.append(timeConditionBefore);
		E_bankBack.append(" group by t.accounts_type, t.payment_type,t.ticket_type) before");
		E_bankBack.append(" full join (select cast(t.accounts_type as varchar2(2)) att,");
		E_bankBack.append(" cast(t.payment_type as varchar2(2)) ptt,");
		E_bankBack.append(" cast(t.ticket_type as varchar2(2)) ttt,");
		E_bankBack.append(" sum(t.money) moneyT,");
		E_bankBack.append(" sum(t.money_snow) moneySnowT,");
		E_bankBack.append(" sum(t.amount) amountT");
		E_bankBack.append(" from finance_ticket_accounts t");
		E_bankBack.append(" where t.accounts_type in ("+accType+")");
		E_bankBack.append(" and t.payment_type in ('07', '08')");
		E_bankBack.append(" and t.ticket_indent_id not in");
		E_bankBack.append(" (select xf.ticket_indent_id");
		E_bankBack.append(" from (select ftasub.ticket_indent_id");
		E_bankBack.append(" from finance_ticket_accounts ftasub");
		E_bankBack.append(" where ftasub.payment_type in ('04', '05')");
		E_bankBack.append(" and ftasub.input_time >= to_date('");
		E_bankBack.append(CommonMethod.timeToString(statementObject.getStartDate()));
		E_bankBack.append(" 02:00:00','YYYY-MM-DD HH24:MI:SS') and ftasub.input_time < to_date('");
		E_bankBack.append(CommonMethod.timeToString(CommonMethod.addDay(statementObject.getEndDate(),1)));
		E_bankBack.append(" 02:00:00','YYYY-MM-DD HH24:MI:SS') ");
		E_bankBack.append(rewardConditionSub);
		E_bankBack.append(" ) xf");
		E_bankBack.append(" inner join (select ftasub.ticket_indent_id");
		E_bankBack.append(" from finance_ticket_accounts ftasub");
		E_bankBack.append(" where ftasub.payment_type in ('01', '02')");
		E_bankBack.append(" and ftasub.input_time >= to_date('");
		E_bankBack.append(CommonMethod.timeToString(statementObject.getStartDate()));
		E_bankBack.append(" 02:00:00','YYYY-MM-DD HH24:MI:SS') and ftasub.input_time < to_date('");
		E_bankBack.append(CommonMethod.timeToString(CommonMethod.addDay(statementObject.getEndDate(),1)));
		E_bankBack.append(" 02:00:00','YYYY-MM-DD HH24:MI:SS') ");
		E_bankBack.append(rewardConditionSub);
		E_bankBack.append(") zf");
		E_bankBack.append(" on xf.ticket_indent_id = zf.ticket_indent_id)");
		E_bankBack.append(" and");
		E_bankBack.append(timeConditionThis);
		E_bankBack.append(" group by t.accounts_type, t.payment_type,t.ticket_type) this");
		E_bankBack.append(" on (before.atb || before.ptb||before.ttb) = (this.att || this.ptt||this.ttt)");
		
		SQLQuery E_bankBack_query = session.createSQLQuery(E_bankBack.toString());
		E_bankBack_query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		List E_bankBack_queryResult = E_bankBack_query.list();
		Map<String,Map> e_bankBack = new HashMap<String, Map>();//网银退款
		for (Object o : E_bankBack_queryResult) {
			Map map = (Map)o;
			//ATT   PTT	TTT 	MONEYB	MONEYSNOWB	AMOUNTB	MONEYT	MONEYSNOWT	AMOUNTT
			String key = "";//temp中的key
			String pType ="";//paymentType
			String tType = "";//tickletType
			String aType = "";//accountType
			if(map.get("ATT")!=null){
				aType = map.get("ATT").toString();
			}
			if("03".equals(aType)){//易宝
				pType="x";
			}else{
				if(map.get("PTT")!=null){
					pType=map.get("PTT").toString();
				}
			}
			
			if(map.get("TTT")!=null){
				tType = map.get("TTT").toString();
			}else{
				tType = "x";//初始化数据   没有对应的票类型记录
			}
			key = "E_BANK_"+aType+pType+tType;
			e_bankBack.put(key, map);
		}
		/*查询本期打印回传的冲退，在本期已消费的交易记录中显示*/
		StringBuffer printBackSql = new StringBuffer();
		// att	ptt	ttt	moneyT	moneySnowT	amountT
		printBackSql.append(" select rBack.accounts_type att,");
		printBackSql.append(" rBack.payment_type ptt,");
		printBackSql.append(" rBack.ticket_type ttt,");
		printBackSql.append(" sum(rBack.money - nvl(ret.money, 0)) moneyT,");
		printBackSql.append(" sum(rBack.money_snow - nvl(ret.money_snow, 0)) moneySnowT,");
		printBackSql.append(" sum(rBack.amount - nvl(ret.amount, 0)) amountT");
		printBackSql.append(" from (select back.ticket_indent_id,");
		printBackSql.append(" cast(back.accounts_type as varchar2(2)) accounts_type,");
		printBackSql.append(" cast(t.payment_type as varchar2(2)) payment_type,");
		printBackSql.append(" cast(t.ticket_type as varchar2(2)) ticket_type,");
		printBackSql.append(" sum(back.money) money,");
		printBackSql.append(" sum(back.money_snow) money_snow,");
		printBackSql.append(" sum(back.amount) amount");
		printBackSql.append(" from FINANCE_TICKET_ACCOUNTS t, FINANCE_TICKET_ACCOUNTS back");
		printBackSql.append(" where back.accounts_type in ("+accType+")");
		printBackSql.append(" and back.payment_type in ('10')");
		printBackSql.append(" and back.back_accounts_id = t.ticket_accounts_id");
		printBackSql.append(" and ");
		printBackSql.append(timeConditionThis);
		printBackSql.append(" group by back.ticket_indent_id,");
		printBackSql.append(" back.accounts_type,");
		printBackSql.append(" t.payment_type,");
		printBackSql.append(" t.ticket_type) rBack");
		printBackSql.append(" left join (select cast(t.accounts_type as varchar2(2)) accounts_type,");
		printBackSql.append(" t.ticket_indent_id,");
		printBackSql.append(" t.ticket_type,");
		printBackSql.append(" case");
		printBackSql.append(" when t.payment_type = '07' then");
		printBackSql.append(" '01'");
		printBackSql.append(" when t.payment_type = '08' then");
		printBackSql.append(" '02'");
		printBackSql.append(" end payment_type,");
		printBackSql.append(" sum(t.money) money,");
		printBackSql.append(" sum(t.money_snow) money_snow,");
		printBackSql.append(" sum(t.amount) amount");
		printBackSql.append(" from FINANCE_TICKET_ACCOUNTS t");
		printBackSql.append(" where t.payment_type in ('08', '07')");
		printBackSql.append(" and ");
		printBackSql.append(timeConditionThis);
		printBackSql.append(" group by t.accounts_type,");
		printBackSql.append(" t.ticket_indent_id,");
		printBackSql.append(" t.payment_type,");
		printBackSql.append(" t.ticket_type) ret");
		printBackSql.append(" on rBack.ticket_indent_id = ret.ticket_indent_id");
		printBackSql.append(" and rBack.accounts_type = ret.accounts_type");
		printBackSql.append(" and rBack.payment_type = ret.payment_type");
		printBackSql.append(" and rBack.ticket_type = ret.ticket_type");
		printBackSql.append(" group by rBack.accounts_type, rBack.payment_type, rBack.ticket_type");
		
		SQLQuery printBackquery = session.createSQLQuery(printBackSql.toString());
		printBackquery.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		List printBackQueryResult = printBackquery.list();
		
		Map<String,Map> printBack = new HashMap<String, Map>();//网银退款
		for (Object o : printBackQueryResult) {
			Map map = (Map)o;
			// ATT	PTT	TTT	MONEYT	MONEYSNOWT	AMOUNTT
			String key = "";
			String pType ="";
			String tType = "";
			String aType = "";
			if(map.get("ATT")!=null){
				aType = map.get("ATT").toString();
			}
			if("03".equals(aType)){//易宝
				pType="x";
			}else{
				if(map.get("PTT")!=null){
					pType=map.get("PTT").toString();
				}
			}
			
			if(map.get("TTT")!=null){
				tType = map.get("TTT").toString();
			}else{
				tType = "x";//初始化数据   没有对应的票类型记录
			}
			key = "PRINT_"+aType+pType+tType;
			printBack.put(key, map);
		}
		//SQL
		StringBuffer sql = new StringBuffer();

		sql.append(" select nvl(before.atb, this.att) aType,");
		sql.append(" nvl(before.ptb, this.ptt) pType,");
		sql.append(" nvl(before.ttb,this.ttt) tType,");
		sql.append(" nvl(before.moneyB, 0) moneyB,");
		sql.append(" nvl(before.moneySnowB, 0) moneySnowB,");
		sql.append(" nvl(before.amountB,0) amountB,");
		sql.append(" nvl(this.moneyT, 0) moneyT,");
		sql.append(" nvl(this.moneySnowT, 0) moneySnowT,");
		sql.append(" nvl(this.amountT,0) amountT");
		sql.append(" from ");
		sql.append(" (select cast(t.accounts_type as varchar2(2)) atb,");
		sql.append(" cast(t.payment_type as varchar2(2)) ptb,");
		sql.append(" cast(t.ticket_type as varchar2(2)) ttb,");
		sql.append(" sum(t.money + nvl(back.money, 0)) moneyB,");
		sql.append(" sum(t.money_snow + nvl(back.money_snow, 0)) moneySnowB,");
		sql.append(" sum(t.amount+nvl(back.Amount,0)) amountB");
		sql.append(" from FINANCE_TICKET_ACCOUNTS t");
		sql.append(" left join (select tBack.Money,");
		sql.append(" tBack.Money_Snow,");
		sql.append(" tBack.Amount,");
		sql.append(" tBack.Back_Accounts_Id");
		sql.append(" from FINANCE_TICKET_ACCOUNTS tBack");
		sql.append(" where tBack.Payment_Type in ('09', '10')) back");
		sql.append(" on back.back_accounts_id = t.ticket_accounts_id");
		sql.append(" where t.accounts_type in ("+accType+")");
		sql.append(" and t.payment_type not in ('09', '10')");
		sql.append(" and");
		sql.append(timeConditionBefore);
		sql.append(" group by t.accounts_type, t.payment_type,t.ticket_type) before");
		
		sql.append(" full join ");
		
		sql.append(" (select cast(t.accounts_type as varchar2(2)) att,");
		sql.append(" cast(t.payment_type as varchar2(2)) ptt,");
		sql.append(" cast(t.ticket_type as varchar2(2)) ttt,");
		sql.append(" sum(t.money + nvl(back.money, 0)) moneyT,");
		sql.append(" sum(t.money_snow + nvl(back.money_snow, 0)) moneySnowT,");
		sql.append(" sum(t.amount+nvl(back.Amount,0)) amountT");
		sql.append(" from FINANCE_TICKET_ACCOUNTS t");
		sql.append(" left join (select tBack.Money,");
		sql.append(" tBack.Money_Snow,");
		sql.append(" tBack.Amount,");
		sql.append(" tBack.Back_Accounts_Id");
		sql.append(" from FINANCE_TICKET_ACCOUNTS tBack");
		sql.append(" where tBack.Payment_Type in ('09', '10')) back");
		sql.append(" on back.back_accounts_id = t.ticket_accounts_id");
		sql.append(" where t.accounts_type in ("+accType+")");
		sql.append(" and t.payment_type not in ('09', '10')");
		sql.append(" and");
		sql.append(timeConditionThis);
		sql.append(" group by t.accounts_type, t.payment_type,t.ticket_type) this");
                     
		sql.append(" on (before.atb || before.ptb || before.ttb) = (this.att || this.ptt||this.ttt)");

		SQLQuery query = session.createSQLQuery(sql.toString());
		query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		List queryResult = query.list();
		
		Map<String,StatementObject> tempMap = new HashMap<String,StatementObject>();
		
		for (Object obj : queryResult) {
			//{MONEYSNOWT=0, AMOUNTB=32, MONEYT=0, MONEYSNOWB=0, PTYPE=01, MONEYB=0, AMOUNTT=8, TTYPE=01, ATYPE=02}
			Map map = (Map)obj;
			String key = "";//temp中的key
			String pType ="";//paymentType
			String tType = "";//tickletType
			String aType = "";//accountType
			if(map.get("ATYPE")!=null){
				aType = map.get("ATYPE").toString();
			}
			if("03".equals(aType)){//易宝
				pType="x";
			}else{
				if(map.get("PTYPE")!=null){
					pType=map.get("PTYPE").toString();
				}
			}
			
			if(map.get("TTYPE")!=null){
				tType = map.get("TTYPE").toString();
			}else{
				tType = "x";//初始化数据   没有对应的票类型记录
			}
			if(detailPaymentType.equals(pType)){
				Double moneyB = map.get("MONEYB")==null ? 0D : ((BigDecimal)map.get("MONEYB")).doubleValue();
				Double moneyT = map.get("MONEYT") == null ? 0D : ((BigDecimal)map.get("MONEYT")).doubleValue();
				Double amountB = map.get("AMOUNTB")==null ? 0D : ((BigDecimal)map.get("AMOUNTB")).doubleValue();
				Double amountT = map.get("AMOUNTT") == null ? 0D : ((BigDecimal)map.get("AMOUNTT")).doubleValue();
				Double moneySnowB = ((BigDecimal)map.get("MONEYSNOWB")).doubleValue();
				Double moneySnowT = ((BigDecimal)map.get("MONEYSNOWT")).doubleValue();
				if("01".equals(pType)){//余额支付未消费要减去余额退到余额的金额
					//ATT   PTT	TTT 	MONEYB	MONEYSNOWB	AMOUNTB	MONEYT	MONEYSNOWT	AMOUNTT
					Map bankB = e_bankBack.get("E_BANK_"+aType+"07"+tType);
					if(bankB!=null){
						moneyB += ((BigDecimal)bankB.get("MONEYB")).doubleValue();	
						amountB += ((BigDecimal)bankB.get("AMOUNTB")).doubleValue();	
						moneyT += ((BigDecimal)bankB.get("MONEYT")).doubleValue();	
						amountT += ((BigDecimal)bankB.get("AMOUNTT")).doubleValue();	
						moneySnowB += ((BigDecimal)bankB.get("MONEYSNOWB")).doubleValue();
						moneySnowT += ((BigDecimal)bankB.get("MONEYSNOWT")).doubleValue();
					}
				}
				
				if("02".equals(pType)){//余额支付未消费要减去余额退到余额的金额
					Map bankB = e_bankBack.get("E_BANK_"+aType+"08"+tType);
					if(bankB!=null){
						moneyB += ((BigDecimal)bankB.get("MONEYB")).doubleValue();	
						amountB += ((BigDecimal)bankB.get("AMOUNTB")).doubleValue();	
						moneyT += ((BigDecimal)bankB.get("MONEYT")).doubleValue();	
						amountT += ((BigDecimal)bankB.get("AMOUNTT")).doubleValue();
						moneySnowB += ((BigDecimal)bankB.get("MONEYSNOWB")).doubleValue();
						moneySnowT += ((BigDecimal)bankB.get("MONEYSNOWT")).doubleValue();
					}
				}
				
				if("04".equals(pType)||"05".equals(pType)){//本期交易金额为打印回传时的冲退金额
					//ATT	PTT	TTT	MONEYT	MONEYSNOWT	AMOUNTT
					Map print_back = printBack.get("PRINT_"+aType+pType+tType);
					if(print_back!=null){
						amountT = ((BigDecimal)print_back.get("MONEYT")).doubleValue();
						moneyT = ((BigDecimal)print_back.get("MONEYT")).doubleValue();
						moneySnowT = ((BigDecimal)print_back.get("MONEYSNOWT")).doubleValue();
					}
				}
				
				if("joint".equals(statementObject.getRemark())&&!"01".equals(tType)){//股份公司明细
					StatementObject object = tempMap.get("tType_"+tType);
					if(object==null){
						object = new StatementObject(ticketType.get(tType));
						object.setTableRow1(amountB);
						object.setTableRow2(moneyB);
						object.setTableRow3(amountT);
						object.setTableRow4(moneyT);
					}else{
						object.setTableRow1(object.getTableRow1()+amountB);
						object.setTableRow2(object.getTableRow2()+moneyB);
						object.setTableRow3(object.getTableRow3()+amountT);
						object.setTableRow4(object.getTableRow4()+moneyT);
					}
					tempMap.put("tType_"+tType,object);
					//pagerRoot.add(object);
				}
				if("snow".equals(statementObject.getRemark())&&("01".equals(tType)||"x".equals(tType))){//雪山公司明细
//							StatementObject object = new StatementObject(ticketType.get(tType));
//							object.setTableRow1(amountB);
//							object.setTableRow2(moneySnowB);
//							object.setTableRow3(amountT);
//							object.setTableRow4(moneySnowT);
//							pagerRoot.add(object);
					StatementObject object = tempMap.get("tType_"+tType);
					if(object==null){
						object = new StatementObject(ticketType.get(tType));
						object.setTableRow1(amountB);
						object.setTableRow2(moneySnowB);
						object.setTableRow3(amountT);
						object.setTableRow4(moneySnowT);
					}else{
						object.setTableRow1(object.getTableRow1()+amountB);
						object.setTableRow2(object.getTableRow2()+moneySnowB);
						object.setTableRow3(object.getTableRow3()+amountT);
						object.setTableRow4(object.getTableRow4()+moneySnowT);
					}
					tempMap.put("tType_"+tType,object);
					//pagerRoot.add(object);
				}
				
			}
		}
		
		for(String key : tempMap.keySet()){
			StatementObject object = tempMap.get(key);
			pagerRoot.add(object);
		}
		
		//将StatementObject转成StatementObjectForPage
		List<StatementObjectForPage> objForPage= new ArrayList<StatementObjectForPage>();
		for (StatementObject o : pagerRoot) {
			objForPage.add(ObjToPageObj(o,true));
		}
		return objForPage;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public JsonPager<StatementObject> findDeatilOfRecharge(int start,int limit,final StatementObject statementObject) {
		final boolean isExport= statementObject.getTableRow1()==1D ? true : false;//是不是导出报表    1 是导出报表
		final int startR = start;//当前页
		final int pagesize = limit;//每页条数
		//查询参数
		final Timestamp startTime = statementObject.getStartDate();
		final Timestamp endTime = statementObject.getEndDate();
		final String remark = statementObject.getRemark();
		final String payType = statementObject.getPayType();
		final String detailName = statementObject.getTableColumn();
		final JsonPager<StatementObject> pager = new JsonPager<StatementObject>();
		
		Session session = sessionFactory.getCurrentSession();
		List<StatementObject> pagerRoot = new ArrayList<StatementObject>();
		String accountType="";
		if("奖励款充值金额".equals(detailName)){
			accountType="'01'";
		}
		if("充值金额".equals(detailName)){
			accountType="'04'";
		}
		if("".equals(accountType)){//导出报表
			if("01".equals(payType)){
				accountType="'04'";
			}
			if("02".equals(payType)){
				accountType="'01'";
			}
		}
		
		//查询奖励款类型
		StringBuffer rewardCondition = new StringBuffer();
		if(statementObject.getRewardTypeId()!=null){
			rewardCondition.append(" and t.recharge_rec_id in ( ");
			rewardCondition.append(" select brr.recharge_rec_id from BASE_RECHARGE_REC brr where brr.reward_account_id in ( ");
			rewardCondition.append(" select bra.reward_account_id from BASE_REWARD_ACCOUNT bra where bra.reward_type_id = "+statementObject.getRewardTypeId());
			rewardCondition.append(" )) ");
		}
		
		//查询条件（本期）
		StringBuffer timeConditionThis = new StringBuffer();
		//添加传入的查询条件
		timeConditionThis.append(" t.input_time >= to_date('");
		timeConditionThis.append(CommonMethod.timeToString(statementObject.getStartDate()));
		timeConditionThis.append(" 02:00:00','YYYY-MM-DD HH24:MI:SS') and t.input_time < to_date('");
		timeConditionThis.append(CommonMethod.timeToString(CommonMethod.addDay(statementObject.getEndDate(),1)));
		timeConditionThis.append(" 02:00:00','YYYY-MM-DD HH24:MI:SS') ");
		timeConditionThis.append(rewardCondition);
		
		//基础查询   没有分页    将查询出所有符合的记录
		StringBuffer baseSql = new StringBuffer();
		baseSql.append("select u.user_code code,ta.travel_agency_name,t.input_time it,t.money,t.money_snow");
		baseSql.append(" from finance_ticket_accounts t ,base_travel_agency ta,base_recharge_rec r ,sys_user u");
		baseSql.append(" where t.recharge_rec_id = r.recharge_rec_id and r.travel_agency_id=ta.travel_agency_id ");
		baseSql.append(" and ta.travel_agency_id=u.travel_agency_id ");
		baseSql.append(" and t.payment_type='03' ");
		baseSql.append(" and t.accounts_type="+accountType+" ");
		if("snow".equals(remark.trim())){
			baseSql.append(" and t.money_snow >0 ");
		}else if("joint".equals(remark.trim())){
			baseSql.append(" and t.money >0 ");
		}
		baseSql.append(" and ");
		baseSql.append(timeConditionThis);
		//如果是导出报表,怎么需要用baseSql 全部查出来
		if(isExport) {
			SQLQuery query = session.createSQLQuery(baseSql.toString());
			List queryResult = query.list();
			for (Object object : queryResult) {
				Object[] res = (Object[])object;
				//[lxszh00321, 四川省中国青年旅行社, 2014-06-23 20:17:47.0, 1, 0, 1]
				String code = (String)res[0];
				String name = (String)res[1];
				Timestamp time = (Timestamp)res[2];
				Double money = ((BigDecimal)res[3] == null ? new BigDecimal(0) : (BigDecimal)res[3]).doubleValue();
				Double money_snow = ((BigDecimal)res[4] == null ? new BigDecimal(0) : (BigDecimal)res[4]).doubleValue();
				StatementObject so = new StatementObject(code);//code是旅行社账号
				so.setRemark(name);//旅行舍名称
				so.setStartDate(time);//充值时间
				if("snow".equals(remark.trim())){
					so.setTableRow1(money_snow);//充值金额雪山公司
					pagerRoot.add(so);
				}
				if("joint".equals(remark.trim())){
					so.setTableRow1(money);//充值金额股份公司
					pagerRoot.add(so);
				}
			}
			
			
			//查询总记录数
			SQLQuery queryCount = session.createSQLQuery("select count(code) from ("+baseSql.toString()+")");
			int countObj = ((BigDecimal)queryCount.uniqueResult()).intValue();
			pager.setTotal(countObj);
			//分页
			StringBuffer sql = new StringBuffer();
			sql.append("select *");
			sql.append(" from (select a.*,rownum rn ");
			sql.append(" from (select *");
			sql.append(" from ");
			sql.append(" ( ");
			sql.append(baseSql);
			sql.append(")");
			sql.append(" order by it desc) a ");
			sql.append(" where rownum <= ");
			sql.append(startR+pagesize);
			sql.append(")");
			sql.append(" where rn > ");
			sql.append(startR);
			
			SQLQuery query1 = session.createSQLQuery(sql.toString());
			List queryResult1 = query1.list();
			for (Object object : queryResult1) {
				Object[] res = (Object[])object;
				//[lxszh00321, 四川省中国青年旅行社, 2014-06-23 20:17:47.0, 1, 0, 1]
				String code = (String)res[0];
				String name = (String)res[1];
				Timestamp time = (Timestamp)res[2];
				Double money = ((BigDecimal)res[3] == null ? new BigDecimal(0) : (BigDecimal)res[3]).doubleValue();
				Double money_snow = ((BigDecimal)res[4] == null ? new BigDecimal(0) : (BigDecimal)res[4]).doubleValue();
				StatementObject so = new StatementObject(code);//code是旅行社账号
				so.setRemark(name);//旅行舍名称
				so.setStartDate(time);//充值时间
				if("snow".equals(remark.trim())){
					so.setTableRow1(money_snow);//充值金额雪山公司
					pagerRoot.add(so);
				}
				if("joint".equals(remark.trim())){
					so.setTableRow1(money);//充值金额股份公司
					pagerRoot.add(so);
				}
			}

			pager.setRoot(pagerRoot);
		}
		
		return pager;
	}
	
	
	/**
	 * 计算合计行
	 * @param list
	 * @return
	 */
	private StatementObject calcuTotalColumn(List<StatementObject> list ,String tableColumn){
		Double count1 = 0D;
		Double count2 = 0d;
		Double count3 = 0D;
		Double count4 = 0d;
		Double count5 = 0d;
		Double count6 = 0D;
		Double count7 = 0D;
		Double count8 = 0D;
		Double count9 = 0D;
		Double count10 = 0D;
		Double count11 = 0D;
		Double count12 = 0D;
		for (StatementObject so : list) {//查找已经放入的行（StatementObject）数据，相同的列求和
			if(so.getTableColumn().trim().equals("合计")){//排除其他的小计(有多个合计行的情况)  其他的合计不纳入最后的合计行
				continue;
			}
			count1 +=so.getTableRow1();
			count2 +=so.getTableRow2();
			count3 +=so.getTableRow3();
			count4 +=so.getTableRow4();
			count5 +=so.getTableRow5();
			count6 +=so.getTableRow6();
			count7 +=so.getTableRow7();
			count8 +=so.getTableRow8();
			count9 +=so.getTableRow9();
			count10 +=so.getTableRow10();
			count11 +=so.getTableRow11();
			count12 +=so.getTableRow12();
		}
		StatementObject countObj = new StatementObject(tableColumn);
		
		countObj.setTableRow1(Double.valueOf(CommonMethod.format2db(count1)));
		countObj.setTableRow2(Double.valueOf(CommonMethod.format2db(count2)));
		countObj.setTableRow3(Double.valueOf(CommonMethod.format2db(count3)));
		countObj.setTableRow4(Double.valueOf(CommonMethod.format2db(count4)));
		countObj.setTableRow5(Double.valueOf(CommonMethod.format2db(count5)));
		countObj.setTableRow6(Double.valueOf(CommonMethod.format2db(count6)));
		countObj.setTableRow7(Double.valueOf(CommonMethod.format2db(count7)));
		countObj.setTableRow8(Double.valueOf(CommonMethod.format2db(count8)));
		countObj.setTableRow9(Double.valueOf(CommonMethod.format2db(count9)));
		countObj.setTableRow10(Double.valueOf(CommonMethod.format2db(count10)));
		countObj.setTableRow11(Double.valueOf(CommonMethod.format2db(count11)));
		countObj.setTableRow12(Double.valueOf(CommonMethod.format2db(count12)));
		//添加合计行的数据
		return countObj;
	}
	
	private StatementObjectForPage ObjToPageObj(StatementObject statementObject,boolean isAmount){
		StatementObjectForPage pageObj = new StatementObjectForPage(statementObject.getTableColumn()); 
		if(isAmount){
			pageObj.setTableRow1(statementObject.getTableRow1().intValue()+"");
		}else{
			pageObj.setTableRow1(statementObject.getTableRow1().toString());
		}
		pageObj.setTableRow2(statementObject.getTableRow2().toString());
		if(isAmount){
			pageObj.setTableRow3(statementObject.getTableRow3().intValue()+"");
		}else{
			pageObj.setTableRow3(statementObject.getTableRow3().toString());
		}
		pageObj.setTableRow4(statementObject.getTableRow4().toString());
		pageObj.setTableRow5(statementObject.getTableRow5().toString());
		pageObj.setTableRow6(statementObject.getTableRow6().toString());
		pageObj.setTableRow7(statementObject.getTableRow7().toString());
		pageObj.setTableRow8(statementObject.getTableRow8().toString());
		pageObj.setTableRow9(statementObject.getTableRow9().toString());
		pageObj.setTableRow10(statementObject.getTableRow10().toString());
		pageObj.setTableRow11(statementObject.getTableRow11().toString());
		pageObj.setTableRow12(statementObject.getTableRow12().toString());
		
		pageObj.setRemark(statementObject.getRemark());
		pageObj.setStartDate(statementObject.getStartDate());
		pageObj.setEndDate(statementObject.getEndDate());
		
		return pageObj;
	}
}