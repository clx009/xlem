package dao.ticket.financeTicketAccounts;
import java.util.List;

import com.xlem.dao.FinanceTicketAccounts;

import common.action.JsonPager;
import common.dao.BaseDao;

public interface FinanceTicketAccountsDao extends BaseDao<FinanceTicketAccounts> {
	
	/**
	 * 报表总计表 肖宇翔
	 * @param statementObject
	 * @return
	 */
	List<AllStatementObject>  findAllStatement(final StatementObject stateObj);
	
	/**
	 * 报表总计表
	 * @param statementObject
	 * @return
	 */
	List<StatementObjectForPage> findTotalStatement(final StatementObject statementObject);
	/**
	 * 报表明细表->>网银支付已消费金额
	 * @param statementObject 传入参数封装   总表中明细行的  名称
	 * @return
	 */
	List<StatementObjectForPage> findDeatilStatement(final StatementObject statementObject);
	/**
	 * 报表明细表->>网银支付已消费金额
	 * @param statementObject 传入参数封装   总表中明细行的  名称
	 * @return
	 */
	/**
	 * 现金充值明细表
	 */
	JsonPager<StatementObject> findDeatilOfRecharge(int start,int limit,final StatementObject statementObject);
	
}