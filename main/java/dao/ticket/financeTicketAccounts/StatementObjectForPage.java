package dao.ticket.financeTicketAccounts;

import java.sql.Timestamp;
import java.util.Date;

import common.dao.BaseBean;

/**
 * 此类封装 报表一行数据 如果列数量不够，请自行添加
 * 
 * @author li
 * 
 */
public class StatementObjectForPage extends BaseBean {

	private String tableRow1;
	private String tableRow2;
	private String tableRow3;
	private String tableRow4;
	private String tableRow5;
	private String tableRow6;
	private String tableRow7;
	private String tableRow8;
	private String tableRow9;
	private String tableRow10;
	private String tableRow11;
	private String tableRow12;
	private String tableColumn;
	private String remark;
	private Timestamp startDate;
	private Timestamp endDate;

	public StatementObjectForPage() {
	}

	public StatementObjectForPage(String tableColumn) {
		this.tableColumn = tableColumn;
	}

	public String getTableRow1() {
		return tableRow1;
	}

	public void setTableRow1(String tableRow1) {
		this.tableRow1 = tableRow1;
	}

	public String getTableRow2() {
		return tableRow2;
	}

	public void setTableRow2(String tableRow2) {
		this.tableRow2 = tableRow2;
	}

	public String getTableRow3() {
		return tableRow3;
	}

	public void setTableRow3(String tableRow3) {
		this.tableRow3 = tableRow3;
	}

	public String getTableRow4() {
		return tableRow4;
	}

	public void setTableRow4(String tableRow4) {
		this.tableRow4 = tableRow4;
	}

	public String getTableRow5() {
		return tableRow5;
	}

	public void setTableRow5(String tableRow5) {
		this.tableRow5 = tableRow5;
	}

	public String getTableRow6() {
		return tableRow6;
	}

	public void setTableRow6(String tableRow6) {
		this.tableRow6 = tableRow6;
	}

	public String getTableRow7() {
		return tableRow7;
	}

	public void setTableRow7(String tableRow7) {
		this.tableRow7 = tableRow7;
	}

	public String getTableRow8() {
		return tableRow8;
	}

	public void setTableRow8(String tableRow8) {
		this.tableRow8 = tableRow8;
	}

	public String getTableRow9() {
		return tableRow9;
	}

	public void setTableRow9(String tableRow9) {
		this.tableRow9 = tableRow9;
	}

	public String getTableRow10() {
		return tableRow10;
	}

	public void setTableRow10(String tableRow10) {
		this.tableRow10 = tableRow10;
	}

	public String getTableRow11() {
		return tableRow11;
	}

	public void setTableRow11(String tableRow11) {
		this.tableRow11 = tableRow11;
	}

	public String getTableRow12() {
		return tableRow12;
	}

	public void setTableRow12(String tableRow12) {
		this.tableRow12 = tableRow12;
	}

	public String getTableColumn() {
		return tableColumn;
	}

	public void setTableColumn(String tableColumn) {
		this.tableColumn = tableColumn;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
