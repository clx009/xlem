package dao.ticket.ticketIndentCheckLog;
import com.xlem.dao.TicketIndentCheckLog;

import common.dao.BaseDao;

public interface TicketIndentCheckLogDao extends BaseDao<TicketIndentCheckLog> {

}