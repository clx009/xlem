package dao.ticket.ticketIndentCheckLog;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.TicketIndentCheckLog;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("ticketIndentCheckLogDao")
public class TicketIndentCheckLogDaoImpl extends BaseDaoImpl<TicketIndentCheckLog> implements TicketIndentCheckLogDao {

}