package dao.ticket.ticketRoleType;
import com.xlem.dao.TicketRoleType;

import common.dao.BaseDao;
public interface TicketRoleTypeDao extends BaseDao<TicketRoleType> {

}