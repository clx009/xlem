package dao.ticket.ticketRoleType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.TicketRoleType;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("ticketRoleTypeDao")
public class TicketRoleTypeDaoImpl extends BaseDaoImpl<TicketRoleType> implements TicketRoleTypeDao {

}