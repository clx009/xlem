package dao.ticket.ticketIndentDetail;
import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.SysTablePk;
import com.xlem.dao.TicketIndentDetail;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("ticketIndentDetailDao")
public class TicketIndentDetailDaoImpl extends BaseDaoImpl<TicketIndentDetail> implements TicketIndentDetailDao {

	@Override
	public void deleteByIndentId(final String indentId) {
		Session session = sessionFactory.getCurrentSession();
		String sql = "delete from TicketIndentDetail t where t.ticketIndent.indentId = :indentId";
		Query query = session.createQuery(sql).setString("indentId", indentId);
		query.executeUpdate();
	}

	@Override
	public synchronized String getNextIndentCode(String baseCode,String prefixCode) {
//		String queryString = "select currentPk from SysTablePk where tableName = 'INDENT_CODE'";// and currentPk like '"+prefixCode+"%'
		SysTablePk sysTablepk = new SysTablePk();
		sysTablepk = (SysTablePk)sessionFactory.getCurrentSession().get(SysTablePk.class, "INDENT_CODE");
		if(sysTablepk==null || sysTablepk.getTableName() == null){//无数据时，新增
			sysTablepk = new SysTablePk();
			sysTablepk.setTableName("INDENT_CODE");
			sysTablepk.setCurrentPk(baseCode);
			sessionFactory.getCurrentSession().save(sysTablepk);
			return baseCode;
		}
		else{
			if(sysTablepk.getCurrentPk().startsWith(prefixCode)){//
				String code = sysTablepk.getCurrentPk();
				String str = baseCode.substring(0, 8) + String.format("%0"+8+"d", Integer.parseInt(code.substring(8, code.length()))+1);
				sysTablepk.setTableName("INDENT_CODE");
				sysTablepk.setCurrentPk(str);
				sessionFactory.getCurrentSession().update(sysTablepk);
				return str;
			}
			else{
				sysTablepk.setTableName("INDENT_CODE");
				sysTablepk.setCurrentPk(baseCode);
				sessionFactory.getCurrentSession().update(sysTablepk);
				return baseCode;
			}
		}
	}

	@Override
	public TicketIndentDetail findById(String indentDetailId) {
		log.debug("getting "+TicketIndentDetail.class.getSimpleName()+" instance with id: " + indentDetailId);
		try {
			TicketIndentDetail instance = (TicketIndentDetail) sessionFactory.openSession()
					.get(getPersistentClass(), indentDetailId);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}