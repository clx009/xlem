package dao.ticket.ticketIndentDetail;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.TicketDetailHis;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("ticketDetailHisDao")
public class TicketDetailHisDaoImpl extends BaseDaoImpl<TicketDetailHis> implements TicketDetailHisDao {

}