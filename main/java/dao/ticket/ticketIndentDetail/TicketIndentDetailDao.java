package dao.ticket.ticketIndentDetail;
import com.xlem.dao.TicketIndentDetail;

import common.dao.BaseDao;

public interface TicketIndentDetailDao extends BaseDao<TicketIndentDetail> {

	void deleteByIndentId(String indentId);

	String getNextIndentCode(String basecode, String prefixCode);

	TicketIndentDetail findById(String indentDetailId);

}