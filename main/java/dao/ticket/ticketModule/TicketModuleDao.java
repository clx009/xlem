package dao.ticket.ticketModule;
import com.xlem.dao.TicketModule;

import common.dao.BaseDao;

public interface TicketModuleDao extends BaseDao<TicketModule> {

}