package dao.ticket.ticketModule;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.TicketModule;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("ticketModuleDao")
public class TicketModuleDaoImpl extends BaseDaoImpl<TicketModule> implements TicketModuleDao {

}