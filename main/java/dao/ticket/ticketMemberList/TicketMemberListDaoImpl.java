package dao.ticket.ticketMemberList;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.TicketMemberList;

import common.CommonMethod;
import common.dao.BaseDaoImpl;

@Transactional
@Repository("ticketMemberListDao")
public class TicketMemberListDaoImpl extends BaseDaoImpl<TicketMemberList> implements TicketMemberListDao {

	@SuppressWarnings("unchecked")
	@Override
	public Map<String,Integer> findFrequency(final String indentId) {
		Session session = sessionFactory.getCurrentSession();
		String sql = " select t.ID_CARD_NUMBER,t.MEMBER_NAME,COUNT(distinct t.MEMBER_LIST_ID) as NUMS from TICKET_MEMBER_LIST t "+
				" where t.MEMBER_LIST_ID in "+
				" (select tm.MEMBER_LIST_ID from TICKET_MEMBER_LIST tm where tm.INDENT_ID = '"+indentId+"') "+
				" or t.INDENT_ID in (select ti.indent_id from ticket_indent ti " +
				" where ti.status in ('02','03','04','05','22','23') and ti.use_date >= TO_DATE('"+CommonMethod.addYear(CommonMethod.getCurrentDate(), -1)+" 00:00:00','yyyy-mm-dd hh24:mi:ss') ) "+
				" group by t.ID_CARD_NUMBER,t.MEMBER_NAME";
			
		Query query = session.createSQLQuery(sql);
		query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		
		Map<String,Integer> teMap = new HashMap<String,Integer>();
		List list = query.list();
		for (Object obj : list) {
			Map map = (Map)obj;
			String idCard = map.get("ID_CARD_NUMBER")==null?null:map.get("ID_CARD_NUMBER").toString();
			String mName = map.get("MEMBER_NAME")==null?null:map.get("MEMBER_NAME").toString();
			int nums = map.get("NUMS")==null?0:Integer.valueOf(map.get("NUMS").toString());
			teMap.put("IC_"+idCard+"_"+mName, nums);
		}
		return teMap;
	}
}