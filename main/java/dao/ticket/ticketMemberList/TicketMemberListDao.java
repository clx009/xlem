package dao.ticket.ticketMemberList;
import java.util.Map;

import com.xlem.dao.TicketMemberList;

import common.dao.BaseDao;
public interface TicketMemberListDao extends BaseDao<TicketMemberList> {
	/**
	 * 查询身份证使用次数
	 * @param indentId
	 * @return
	 */
	public Map<String,Integer> findFrequency(final String indentId);
}