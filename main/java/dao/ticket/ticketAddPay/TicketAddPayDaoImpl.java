package dao.ticket.ticketAddPay;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.TicketAddPay;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("ticketAddPayDao")
public class TicketAddPayDaoImpl extends BaseDaoImpl<TicketAddPay> implements TicketAddPayDao {

}