package dao.ticket.ticketPrice;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.TicketPrice;
import com.xlem.dao.TicketType;

import common.CommonMethod;
import common.dao.BaseDaoImpl;

@Transactional
@Repository("ticketPriceDao")
public class TicketPriceDaoImpl extends BaseDaoImpl<TicketPrice> implements TicketPriceDao {

	@SuppressWarnings("unchecked")
	@Override
	public int findTopLimit(final Timestamp t, final TicketPrice ticketPrice) {
		Session session = sessionFactory.getCurrentSession();
		StringBuffer sql = new StringBuffer();
		sql.append(" select ti.use_date,tid.ticket_price_id,sum(tid.amount)");
		sql.append(" from ticket_indent ti,ticket_indent_detail tid");
		sql.append(" where ti.indent_id = tid.indent_id");
		sql.append(" and ti.use_date = :useDate and tid.ticket_price_id = :priceId");
		sql.append(" group by ti.use_date,tid.ticket_price_id");
		Query query = session.createSQLQuery(sql.toString());
		query.setTimestamp("useDate", t);
		query.setLong("priceId", ticketPrice.getTicketPriceId());
		List<Object[]> objects = query.list();
		if(objects!=null && !objects.isEmpty()){
			Object[] obj = objects.get(0);
			if(obj!=null){
				if(obj[2]!=null){
					return Integer.parseInt(obj[2].toString());
				}
				else{
					return 0;
				}
			}
			else{
				return 0;
			}
		}
		else{
			return 0;
		}
	}

	@Override
	public TicketPrice findPriceAndTypeById(Long ticketPriceId) {
		TicketPrice tp = (TicketPrice) sessionFactory.getCurrentSession()
	     .load(TicketPrice.class, ticketPriceId);
	   // getHibernateTemplate().initialize(tp); // 手工加载每个对象
	   Hibernate.initialize(tp);
	   // getHibernateTemplate().initialize(tp.getTicketType());// 手工加载每个对象
	   Hibernate.initialize(tp.getTicketType());
	   return tp;
	}
	
	@SuppressWarnings("unchecked")
	private List<TicketPrice> findTicketListSQL(final TicketPrice ticketPrice,final Timestamp useDate,final String usePage,final long userId,final long roleId,final String fun) {
		Session session = sessionFactory.getCurrentSession();
		String sql = " select t.ticket_id as ticket_id_t, t.toplimit as toplimit_t,t.indent_toplimit as indent_toplimit_t,t.user_indent_toplimit as user_indent_toplimit_t, "+
			" t.remain_toplimit as remain_toplimit_t,t.use_page as use_page_t, "+

			" tp.ticket_price_id,tp.ticket_id,tp.unit_price,tp.print_price,tp.start_date,tp.end_date,cast(tp.status as varchar2(2)) as status, "+
			" cast(tp.is_use as varchar2(2)) as is_use,tp.remark,tp.inputer,tp.input_time,tp.updater,tp.update_time,tp.entrance_price,tp.ropeway_price, "+
			" tp.toplimit,tp.remain_toplimit,tp.entrance_unit_price,cast(tp.entrance_company as varchar2(2)) as entrance_company,tp.ropeway_unit_price,cast(tp.ropeway_company as varchar2(2)) as ropeway_company, "+

			" tt.ticket_id as ticket_id_type,tt.ticket_code as ticket_code_type,tt.ticket_name as ticket_name_type, "+
			" tt.intro as intro_type,cast(tt.special_type as varchar2(2)) as special_type_type,cast(tt.customer_type as varchar2(2)) as customer_type_type, "+
			" cast(tt.gate as varchar2(2)) as gate_type,cast(tt.status as varchar2(2)) as status_type,cast(tt.is_use as varchar2(2)) as is_use_type, "+
			" tt.remark as remark_type,tt.inputer as inputer_type,tt.input_time as input_time_type, "+
			" tt.updater as updater_type,tt.update_time as update_time_type,tt.ticket_discount_id as ticket_discount_id_type, "+
			" tt.ticket_module_id as ticket_module_id_type,tt.reward_type_code as reward_type_code_type,cast(tt.ticket_big_type as varchar2(2)) as ticket_big_type_type, "+
			" tt.img_address as img_address_type,tt.img_folder as img_folder_type,tt.descride as descride_type,tt.notice as notice_type ,cast(tt.activity as varchar2(2)) as activity_type"+
 
					" from  "+
			" (select nvl(tu.ticket_id,tr.ticket_id) as ticket_id, nvl(nvl(tu.toplimit,tr.toplimit),0) as toplimit, "+ 
			" nvl(nvl(tu.indent_toplimit,tr.indent_toplimit),0) as indent_toplimit,nvl(nvl(tu.user_indent_toplimit,tr.user_indent_toplimit),0) as user_indent_toplimit, " +
			" nvl(tu.remain_toplimit,tr.remain_toplimit) as remain_toplimit, "+ 
			" nvl(tu.use_page,tr.use_page) as use_page "+ 
			" from "+
			" (select tut.ticket_id,tut.toplimit,tut.indent_toplimit,tut.user_indent_toplimit,tut.remain_toplimit,tut.use_page "+ 
			" from TICKET_USER_TYPE tut "+
			" where tut.status ='11' and tut.user_id = :user_id "+
			" and tut.use_page like :use_page_tut "+
			" ) tu "+
			" full join "+
			" (select trt.ticket_id,trt.toplimit,trt.indent_toplimit,trt.user_indent_toplimit,trt.remain_toplimit,trt.use_page "+ 
			" from TICKET_ROLE_TYPE trt "+
			" where trt.status ='11' and trt.role_id = :role_id "+
			" and trt.use_page like :use_page "+
			" ) tr "+
			" on tu.ticket_id = tr.ticket_id) t,TICKET_PRICE tp,TICKET_TYPE tt "+
			" where t.ticket_id=tp.ticket_id "+
			" and tt.ticket_id=tp.ticket_id  and tt.ticket_big_type != '04'  "+
			" and tp.unit_price>0 and tp.status ='11' and tt.status ='11' ";
		
//				if(ticketPrice.getTicketType()!=null && ticketPrice.getTicketType().getTicketBigType()!=null && !"".equals(ticketPrice.getTicketType().getTicketBigType())){
//					sql += " and tt.ticket_big_type = :ticket_big_type ";
			if(ticketPrice.getTicketType()!=null && ticketPrice.getTicketType().getTicketBigType()!=null && ticketPrice.getTicketType().getTicketBigType().equals("02")){//查项目票时判断剩余数量
				if(!"acPayProject".equals(ticketPrice.getRemark())){
					sql += " and tp.remain_toplimit > 0 ";
				}
			}
//				}
			
			if(useDate !=null && 
					!"act".equals(ticketPrice.getRemark())){//查询活动票必须设置唯一启用时间段
				
				String sDate = CommonMethod.timeToString(useDate);
				String eDate = CommonMethod.timeToString(useDate);
				sql = sql+" and tp.end_date >= TO_DATE('"+sDate+" 00:00:00','yyyy-mm-dd hh24:mi:ss') ";
				sql = sql+" and tp.start_date <= TO_DATE('"+eDate+" 23:59:59','yyyy-mm-dd hh24:mi:ss')";
			}
			
			if("BuyTicket".equals(fun)){
				
				if(ticketPrice.getTicketType()!=null && ticketPrice.getTicketType().getTicketBigType()!=null && !"".equals(ticketPrice.getTicketType().getTicketBigType()) && ticketPrice.getTicketType().getTicketBigType().indexOf("_")<0){
					sql += " and tt.ticket_big_type = "+ticketPrice.getTicketType().getTicketBigType();
				}
				
				if("act".equals(ticketPrice.getRemark())){//查询抢购活动票
					sql += " and tt.special_type = '05' and tt.ticket_big_type = '03' ";
				}else if("webService".equals(ticketPrice.getRemark())){//接口查询抢购活动票
					sql += " and tt.special_type = '05' and tt.ticket_big_type = '03' ";
				}else if(!"allTic".equals(ticketPrice.getRemark())){//不查全部票种
//							sql += " and tt.ticket_big_type not in ('03') ";//排除活动票
					
					if(ticketPrice.getTicketType()!=null && ticketPrice.getTicketType().getSpecialType()!=null && ticketPrice.getTicketType().getSpecialType().indexOf("_")<0){
						sql += " and tt.special_type = '"+ticketPrice.getTicketType().getSpecialType()+"'";
					}else if(ticketPrice.getTicketType()!=null && ticketPrice.getTicketType().getSpecialType()!=null && ticketPrice.getTicketType().getSpecialType().equals("02_03")){
						sql += " and tt.special_type in ('02','03')";
					}else if(ticketPrice.getTicketType()!=null && ticketPrice.getTicketType().getSpecialType()!=null && ticketPrice.getTicketType().getSpecialType().equals("01_04")){
						sql += " and tt.special_type in ('01','04')";
					}else{
						sql += " and tt.special_type in ('01','04','06')";
					}
				}
				if(!"buyTicketFlag".equals(ticketPrice.getRemark())){
					sql += " and tt.activity <>'01'";
				}
			}
			
			
			if(ticketPrice.getTicketPriceId()!=null){//查哪一种活动票
				sql += " and tp.ticket_price_id = '" + ticketPrice.getTicketPriceId()+"'";
			}
			//区分观景索道票和项目票:观景索道票
			if("projectFlag".equals(ticketPrice.getRemark()) //团客购票
					|| "fProject".equals(ticketPrice.getRemark())//散客登陆购票
					|| "home".equals(ticketPrice.getRemark())){//首页购票
				sql += " and tt.ticket_code ='05'";
				sql += " and tt.special_type <>'07'";
			}
			//购买观景索道活动票  20150804
			if("acPjFlag".equals(ticketPrice.getRemark())){
				sql += " and tt.ticket_code ='05'";
				sql += " and tt.special_type ='07'";
			}
			//区分观景索道票和项目票:项目票
			if("gamesFlag".equals(ticketPrice.getRemark())//团客购票
					|| "fgames".equals(ticketPrice.getRemark())//散客登陆购票
					|| "gamesHome".equals(ticketPrice.getRemark())){//首页购票
				sql += " and tt.ticket_code ='06'";
			}
			
			if("01".equals(usePage)){//首页购票
				sql += " and tt.img_address is not null";
			}
			
		sql += " order by tt.ticket_big_type asc,tt.ticket_name asc";
		
			
		Query query = session.createSQLQuery(sql);

		query.setString("use_page_tut", "%"+usePage+"%");
		query.setLong("user_id", userId);
		query.setString("use_page", "%"+usePage+"%");
		query.setLong("role_id", roleId);
		
		query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		List l = query.list();
		
		List<TicketPrice> list = new ArrayList<TicketPrice>();
		
		for(Object obj : l){
			
			Map map = (Map)obj;
			
			TicketType tt = new TicketType();
			
			tt.setTicketId(map.get("TICKET_ID_TYPE")==null?null:Long.valueOf(map.get("TICKET_ID_TYPE").toString()));
			tt.setTicketCode(map.get("TICKET_CODE_TYPE")==null?null:map.get("TICKET_CODE_TYPE").toString());
			tt.setTicketName(map.get("TICKET_NAME_TYPE")==null?null:map.get("TICKET_NAME_TYPE").toString());
			tt.setIntro(map.get("INTRO_TYPE")==null?null:map.get("INTRO_TYPE").toString());
			tt.setSpecialType(map.get("SPECIAL_TYPE_TYPE")==null?null:map.get("SPECIAL_TYPE_TYPE").toString());
			tt.setCustomerType(map.get("CUSTOMER_TYPE_TYPE")==null?null:map.get("CUSTOMER_TYPE_TYPE").toString());
			tt.setGate(map.get("GATE_TYPE")==null?null:map.get("GATE_TYPE").toString());
			tt.setStatus(map.get("STATUS_TYPE")==null?null:map.get("STATUS_TYPE").toString());
			tt.setIsUse(map.get("IS_USE_TYPE")==null?null:map.get("IS_USE_TYPE").toString());
			tt.setRemark(map.get("REMARK_TYPE")==null?null:map.get("REMARK_TYPE").toString());
			tt.setInputer(map.get("INPUTER_TYPE")==null?null:Long.valueOf(map.get("INPUTER_TYPE").toString()));
			tt.setInputTime(map.get("INPUT_TIME_TYPE")==null?null:CommonMethod.string2Time1(map.get("INPUT_TIME_TYPE").toString()));
			tt.setUpdater(map.get("UPDATER_TYPE")==null?null:Long.valueOf(map.get("UPDATER_TYPE").toString()));
			tt.setUpdateTime(map.get("UPDATE_TIME_TYPE")==null?null:CommonMethod.string2Time1(map.get("UPDATE_TIME_TYPE").toString()));
			tt.setTicketDiscountId(map.get("TICKET_DISCOUNT_ID_TYPE")==null?null:Long.valueOf(map.get("TICKET_DISCOUNT_ID_TYPE").toString()));
			tt.setTicketModuleId(map.get("TICKET_MODULE_ID_TYPE")==null?null:map.get("TICKET_MODULE_ID_TYPE").toString());
			tt.setRewardTypeCode(map.get("REWARD_TYPE_CODE_TYPE")==null?null:map.get("REWARD_TYPE_CODE_TYPE").toString());
			tt.setTicketBigType(map.get("TICKET_BIG_TYPE_TYPE")==null?null:map.get("TICKET_BIG_TYPE_TYPE").toString());
			tt.setImgAddress(map.get("IMG_ADDRESS_TYPE")==null?null:map.get("IMG_ADDRESS_TYPE").toString());
			tt.setImgFolder(map.get("IMG_FOLDER_TYPE")==null?null:map.get("IMG_FOLDER_TYPE").toString());
			tt.setDescride(map.get("DESCRIDE_TYPE")==null?null:map.get("DESCRIDE_TYPE").toString());
			tt.setNotice(map.get("NOTICE_TYPE")==null?null:map.get("NOTICE_TYPE").toString());
			tt.setActivity(map.get("ACTIVITY_TYPE")==null?null:map.get("ACTIVITY_TYPE").toString());
			TicketPrice tp = new TicketPrice();
			
			tp.setTicketPriceId(map.get("TICKET_PRICE_ID")==null?null:Long.valueOf(map.get("TICKET_PRICE_ID").toString()));
			tp.setUnitPrice(map.get("UNIT_PRICE")==null?null:Double.valueOf(map.get("UNIT_PRICE").toString()));
			tp.setPrintPrice(map.get("PRINT_PRICE")==null?null:Double.valueOf(map.get("PRINT_PRICE").toString()));
			tp.setStartDate(map.get("START_DATE")==null?null:CommonMethod.string2Time1(map.get("START_DATE").toString()));
			tp.setEndDate(map.get("END_DATE")==null?null:CommonMethod.string2Time1(map.get("END_DATE").toString()));
			tp.setStatus(map.get("STATUS")==null?null:map.get("STATUS").toString());
			tp.setIsUse(map.get("IS_USE")==null?null:map.get("IS_USE").toString());
			tp.setRemark(map.get("REMARK")==null?null:map.get("REMARK").toString());
			tp.setInputer(map.get("INPUTER")==null?null:Long.valueOf(map.get("INPUTER").toString()));
			tp.setInputTime(map.get("INPUT_TIME")==null?null:CommonMethod.string2Time1(map.get("INPUT_TIME").toString()));
			tp.setUpdater(map.get("UPDATER")==null?null:Long.valueOf(map.get("UPDATER").toString()));
			tp.setUpdateTime(map.get("UPDATE_TIME")==null?null:CommonMethod.string2Time1(map.get("UPDATE_TIME").toString()));
			tp.setEntrancePrice(map.get("ENTRANCE_PRICE")==null?null:Double.valueOf(map.get("ENTRANCE_PRICE").toString()));
			tp.setRopewayPrice(map.get("ROPEWAY_PRICE")==null?null:Double.valueOf(map.get("ROPEWAY_PRICE").toString()));
			tp.setEntranceUnitPrice(map.get("ENTRANCE_UNIT_PRICE")==null?null:Double.valueOf(map.get("ENTRANCE_UNIT_PRICE").toString()));
			tp.setEntranceCompany(map.get("ENTRANCE_COMPANY")==null?null:map.get("ENTRANCE_COMPANY").toString());
			tp.setRopewayUnitPrice(map.get("ROPEWAY_UNIT_PRICE")==null?null:Double.valueOf(map.get("ROPEWAY_UNIT_PRICE").toString()));
			tp.setRopewayCompany(map.get("ROPEWAY_COMPANY")==null?null:map.get("ROPEWAY_COMPANY").toString());
			
			
			tp.setIndentToplimit(map.get("INDENT_TOPLIMIT_T")==null?null:Integer.valueOf(map.get("INDENT_TOPLIMIT_T").toString()));
			tp.setUserIndentToplimit(map.get("USER_INDENT_TOPLIMIT_T")==null?null:Integer.valueOf(map.get("USER_INDENT_TOPLIMIT_T").toString()));
			
			tp.setToplimit(map.get("TOPLIMIT")==null?null:Integer.valueOf(map.get("TOPLIMIT").toString()));
			tp.setToplimitT(map.get("TOPLIMIT_T")==null?null:Integer.valueOf(map.get("TOPLIMIT_T").toString()));
			
//					Integer Toplimit = map.get("TOPLIMIT")==null?null:Integer.valueOf(map.get("TOPLIMIT").toString());
//					Integer ToplimitT = map.get("TOPLIMIT_T")==null?null:Integer.valueOf(map.get("TOPLIMIT_T").toString());
//					if(Toplimit==ToplimitT){
//						tp.setToplimit(Toplimit);
//					}else if(Toplimit==null && ToplimitT!=null){
//						tp.setToplimit(ToplimitT);
//					}else if(ToplimitT==null && Toplimit!=null){
//						tp.setToplimit(Toplimit);
//					}else if(Toplimit<=ToplimitT){
//						tp.setToplimit(Toplimit);
//					}else{
//						tp.setToplimit(ToplimitT);
//					}
			
			tp.setRemainToplimit(map.get("REMAIN_TOPLIMIT")==null?null:Integer.valueOf(map.get("REMAIN_TOPLIMIT").toString()));
			tp.setRemainToplimitT(map.get("REMAIN_TOPLIMIT_T")==null?null:Integer.valueOf(map.get("REMAIN_TOPLIMIT_T").toString()));
			
//					Integer RemainToplimit = map.get("REMAIN_TOPLIMIT")==null?null:Integer.valueOf(map.get("REMAIN_TOPLIMIT").toString());
//					Integer RemainToplimitT = map.get("REMAIN_TOPLIMIT_T")==null?null:Integer.valueOf(map.get("REMAIN_TOPLIMIT_T").toString());
//					if(RemainToplimit==RemainToplimitT){
//						tp.setRemainToplimit(RemainToplimit);
//					}else if(RemainToplimit==null && RemainToplimitT!=null){
//						tp.setRemainToplimit(RemainToplimitT);
//					}else if(RemainToplimitT==null && RemainToplimit!=null){
//						tp.setRemainToplimit(RemainToplimit);
//					}else if(RemainToplimit<=RemainToplimitT){
//						tp.setRemainToplimit(RemainToplimit);
//					}else{
//						tp.setRemainToplimit(RemainToplimitT);
//					}
			
			tp.setTicketType(tt);
			list.add(tp);
		}
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	private List<TicketPrice> findTicketListSQLByHsw(final TicketPrice ticketPrice,final Timestamp useDate,final String usePage,final long userId,final long roleId,final String fun) {
		Session session = sessionFactory.getCurrentSession();
		String sql = " select tp.ticket_price_id,tp.ticket_id,tp.unit_price,tp.print_price,tp.start_date,tp.end_date,cast(tp.status as varchar2(2)) as status, "+
			" cast(tp.is_use as varchar2(2)) as is_use,tp.remark,tp.inputer,tp.input_time,tp.updater,tp.update_time,tp.entrance_price,tp.ropeway_price, "+
			" tp.toplimit,tp.remain_toplimit,tp.entrance_unit_price,cast(tp.entrance_company as varchar2(2)) as entrance_company,tp.ropeway_unit_price,cast(tp.ropeway_company as varchar2(2)) as ropeway_company, "+

			" tt.ticket_id as ticket_id_type,tt.ticket_code as ticket_code_type,tt.ticket_name as ticket_name_type, "+
			" tt.intro as intro_type,cast(tt.special_type as varchar2(2)) as special_type_type,cast(tt.customer_type as varchar2(2)) as customer_type_type, "+
			" cast(tt.gate as varchar2(2)) as gate_type,cast(tt.status as varchar2(2)) as status_type,cast(tt.is_use as varchar2(2)) as is_use_type, "+
			" tt.remark as remark_type,tt.inputer as inputer_type,tt.input_time as input_time_type, "+
			" tt.updater as updater_type,tt.update_time as update_time_type,tt.ticket_discount_id as ticket_discount_id_type, "+
			" tt.ticket_module_id as ticket_module_id_type,tt.reward_type_code as reward_type_code_type,cast(tt.ticket_big_type as varchar2(2)) as ticket_big_type_type, "+
			" tt.img_address as img_address_type,tt.img_folder as img_folder_type,tt.descride as descride_type,tt.notice as notice_type "+
 
					" from  "+
			" TICKET_PRICE tp,TICKET_TYPE tt "+
			" where tt.ticket_id=tp.ticket_id "+
			" and tp.unit_price>0 and tp.status ='11' and tt.status ='11' and tt.ticket_big_type = '04' ";
			if("home".equals(ticketPrice.getRemark())){
				sql += " and tt.customer_type = '02' ";
			}
//				if(ticketPrice.getTicketType()!=null && ticketPrice.getTicketType().getTicketBigType()!=null && !"".equals(ticketPrice.getTicketType().getTicketBigType())){
//					sql += " and tt.ticket_big_type = :ticket_big_type ";
			if(ticketPrice.getTicketType()!=null && ticketPrice.getTicketType().getTicketBigType()!=null && ticketPrice.getTicketType().getTicketBigType().equals("02")){//查项目票时判断剩余数量
				sql += " and tp.remain_toplimit > 0 ";
			}
//				}
			
			if(useDate !=null && 
					!"act".equals(ticketPrice.getRemark())){//查询活动票必须设置唯一启用时间段
				
				String sDate = CommonMethod.timeToString(useDate);
				String eDate = CommonMethod.timeToString(useDate);
				sql = sql+" and tp.end_date >= TO_DATE('"+sDate+" 00:00:00','yyyy-mm-dd hh24:mi:ss') ";
				sql = sql+" and tp.start_date <= TO_DATE('"+eDate+" 23:59:59','yyyy-mm-dd hh24:mi:ss')";
			}
			
			if("BuyTicket".equals(fun)){
				
				if(ticketPrice.getTicketType()!=null && ticketPrice.getTicketType().getTicketBigType()!=null && !"".equals(ticketPrice.getTicketType().getTicketBigType()) && ticketPrice.getTicketType().getTicketBigType().indexOf("_")<0){
					sql += " and tt.ticket_big_type = "+ticketPrice.getTicketType().getTicketBigType();
				}
				
				if("act".equals(ticketPrice.getRemark())){//查询抢购活动票
					sql += " and tt.special_type = '05' and tt.ticket_big_type = '03' ";
				}else if("webService".equals(ticketPrice.getRemark())){//接口查询抢购活动票
					sql += " and tt.special_type = '05' and tt.ticket_big_type = '03' ";
				}else if(!"allTic".equals(ticketPrice.getRemark())){//不查全部票种
//							sql += " and tt.ticket_big_type not in ('03') ";//排除活动票
					
					if(ticketPrice.getTicketType()!=null && ticketPrice.getTicketType().getSpecialType()!=null && ticketPrice.getTicketType().getSpecialType().indexOf("_")<0){
						sql += " and tt.special_type = '"+ticketPrice.getTicketType().getSpecialType()+"'";
					}else if(ticketPrice.getTicketType()!=null && ticketPrice.getTicketType().getSpecialType()!=null && ticketPrice.getTicketType().getSpecialType().equals("02_03")){
						sql += " and tt.special_type in ('02','03')";
					}else if(ticketPrice.getTicketType()!=null && ticketPrice.getTicketType().getSpecialType()!=null && ticketPrice.getTicketType().getSpecialType().equals("01_04")){
						sql += " and tt.special_type in ('01','04')";
					}else{
						sql += " and tt.special_type in ('01','04','06')";
					}
				}
			}
			
			if(ticketPrice.getTicketPriceId()!=null){//查哪一种活动票
				sql += " and tp.ticket_price_id = '" + ticketPrice.getTicketPriceId()+"'";
			}
			
			
		sql += " order by tt.ticket_big_type asc,tt.ticket_name asc";
		
		Query query = session.createSQLQuery(sql);
		
		query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		List l = query.list();
		
		List<TicketPrice> list = new ArrayList<TicketPrice>();
		
		for(Object obj : l){
			
			Map map = (Map)obj;
			
			TicketType tt = new TicketType();
			
			tt.setTicketId(map.get("TICKET_ID_TYPE")==null?null:Long.valueOf(map.get("TICKET_ID_TYPE").toString()));
			tt.setTicketCode(map.get("TICKET_CODE_TYPE")==null?null:map.get("TICKET_CODE_TYPE").toString());
			tt.setTicketName(map.get("TICKET_NAME_TYPE")==null?null:map.get("TICKET_NAME_TYPE").toString());
			tt.setIntro(map.get("INTRO_TYPE")==null?null:map.get("INTRO_TYPE").toString());
			tt.setSpecialType(map.get("SPECIAL_TYPE_TYPE")==null?null:map.get("SPECIAL_TYPE_TYPE").toString());
			tt.setCustomerType(map.get("CUSTOMER_TYPE_TYPE")==null?null:map.get("CUSTOMER_TYPE_TYPE").toString());
			tt.setGate(map.get("GATE_TYPE")==null?null:map.get("GATE_TYPE").toString());
			tt.setStatus(map.get("STATUS_TYPE")==null?null:map.get("STATUS_TYPE").toString());
			tt.setIsUse(map.get("IS_USE_TYPE")==null?null:map.get("IS_USE_TYPE").toString());
			tt.setRemark(map.get("REMARK_TYPE")==null?null:map.get("REMARK_TYPE").toString());
			tt.setInputer(map.get("INPUTER_TYPE")==null?null:Long.valueOf(map.get("INPUTER_TYPE").toString()));
			tt.setInputTime(map.get("INPUT_TIME_TYPE")==null?null:CommonMethod.string2Time1(map.get("INPUT_TIME_TYPE").toString()));
			tt.setUpdater(map.get("UPDATER_TYPE")==null?null:Long.valueOf(map.get("UPDATER_TYPE").toString()));
			tt.setUpdateTime(map.get("UPDATE_TIME_TYPE")==null?null:CommonMethod.string2Time1(map.get("UPDATE_TIME_TYPE").toString()));
			tt.setTicketDiscountId(map.get("TICKET_DISCOUNT_ID_TYPE")==null?null:Long.valueOf(map.get("TICKET_DISCOUNT_ID_TYPE").toString()));
			tt.setTicketModuleId(map.get("TICKET_MODULE_ID_TYPE")==null?null:map.get("TICKET_MODULE_ID_TYPE").toString());
			tt.setRewardTypeCode(map.get("REWARD_TYPE_CODE_TYPE")==null?null:map.get("REWARD_TYPE_CODE_TYPE").toString());
			tt.setTicketBigType(map.get("TICKET_BIG_TYPE_TYPE")==null?null:map.get("TICKET_BIG_TYPE_TYPE").toString());
			tt.setImgAddress(map.get("IMG_ADDRESS_TYPE")==null?null:map.get("IMG_ADDRESS_TYPE").toString());
			tt.setImgFolder(map.get("IMG_FOLDER_TYPE")==null?null:map.get("IMG_FOLDER_TYPE").toString());
			tt.setDescride(map.get("DESCRIDE_TYPE")==null?null:map.get("DESCRIDE_TYPE").toString());
			tt.setNotice(map.get("NOTICE_TYPE")==null?null:map.get("NOTICE_TYPE").toString());
			
			TicketPrice tp = new TicketPrice();
			
			tp.setTicketPriceId(map.get("TICKET_PRICE_ID")==null?null:Long.valueOf(map.get("TICKET_PRICE_ID").toString()));
			tp.setUnitPrice(map.get("UNIT_PRICE")==null?null:Double.valueOf(map.get("UNIT_PRICE").toString()));
			tp.setPrintPrice(map.get("PRINT_PRICE")==null?null:Double.valueOf(map.get("PRINT_PRICE").toString()));
			tp.setStartDate(map.get("START_DATE")==null?null:CommonMethod.string2Time1(map.get("START_DATE").toString()));
			tp.setEndDate(map.get("END_DATE")==null?null:CommonMethod.string2Time1(map.get("END_DATE").toString()));
			tp.setStatus(map.get("STATUS")==null?null:map.get("STATUS").toString());
			tp.setIsUse(map.get("IS_USE")==null?null:map.get("IS_USE").toString());
			tp.setRemark(map.get("REMARK")==null?null:map.get("REMARK").toString());
			tp.setInputer(map.get("INPUTER")==null?null:Long.valueOf(map.get("INPUTER").toString()));
			tp.setInputTime(map.get("INPUT_TIME")==null?null:CommonMethod.string2Time1(map.get("INPUT_TIME").toString()));
			tp.setUpdater(map.get("UPDATER")==null?null:Long.valueOf(map.get("UPDATER").toString()));
			tp.setUpdateTime(map.get("UPDATE_TIME")==null?null:CommonMethod.string2Time1(map.get("UPDATE_TIME").toString()));
			tp.setEntrancePrice(map.get("ENTRANCE_PRICE")==null?null:Double.valueOf(map.get("ENTRANCE_PRICE").toString()));
			tp.setRopewayPrice(map.get("ROPEWAY_PRICE")==null?null:Double.valueOf(map.get("ROPEWAY_PRICE").toString()));
			tp.setEntranceUnitPrice(map.get("ENTRANCE_UNIT_PRICE")==null?null:Double.valueOf(map.get("ENTRANCE_UNIT_PRICE").toString()));
			tp.setEntranceCompany(map.get("ENTRANCE_COMPANY")==null?null:map.get("ENTRANCE_COMPANY").toString());
			tp.setRopewayUnitPrice(map.get("ROPEWAY_UNIT_PRICE")==null?null:Double.valueOf(map.get("ROPEWAY_UNIT_PRICE").toString()));
			tp.setRopewayCompany(map.get("ROPEWAY_COMPANY")==null?null:map.get("ROPEWAY_COMPANY").toString());
			
			
			tp.setIndentToplimit(map.get("INDENT_TOPLIMIT_T")==null?null:Integer.valueOf(map.get("INDENT_TOPLIMIT_T").toString()));
			tp.setUserIndentToplimit(map.get("USER_INDENT_TOPLIMIT_T")==null?null:Integer.valueOf(map.get("USER_INDENT_TOPLIMIT_T").toString()));
			
			tp.setToplimit(map.get("TOPLIMIT")==null?null:Integer.valueOf(map.get("TOPLIMIT").toString()));
			tp.setToplimitT(map.get("TOPLIMIT_T")==null?null:Integer.valueOf(map.get("TOPLIMIT_T").toString()));
			
//					Integer Toplimit = map.get("TOPLIMIT")==null?null:Integer.valueOf(map.get("TOPLIMIT").toString());
//					Integer ToplimitT = map.get("TOPLIMIT_T")==null?null:Integer.valueOf(map.get("TOPLIMIT_T").toString());
//					if(Toplimit==ToplimitT){
//						tp.setToplimit(Toplimit);
//					}else if(Toplimit==null && ToplimitT!=null){
//						tp.setToplimit(ToplimitT);
//					}else if(ToplimitT==null && Toplimit!=null){
//						tp.setToplimit(Toplimit);
//					}else if(Toplimit<=ToplimitT){
//						tp.setToplimit(Toplimit);
//					}else{
//						tp.setToplimit(ToplimitT);
//					}
			
			tp.setRemainToplimit(map.get("REMAIN_TOPLIMIT")==null?null:Integer.valueOf(map.get("REMAIN_TOPLIMIT").toString()));
			tp.setRemainToplimitT(map.get("REMAIN_TOPLIMIT_T")==null?null:Integer.valueOf(map.get("REMAIN_TOPLIMIT_T").toString()));
			
//					Integer RemainToplimit = map.get("REMAIN_TOPLIMIT")==null?null:Integer.valueOf(map.get("REMAIN_TOPLIMIT").toString());
//					Integer RemainToplimitT = map.get("REMAIN_TOPLIMIT_T")==null?null:Integer.valueOf(map.get("REMAIN_TOPLIMIT_T").toString());
//					if(RemainToplimit==RemainToplimitT){
//						tp.setRemainToplimit(RemainToplimit);
//					}else if(RemainToplimit==null && RemainToplimitT!=null){
//						tp.setRemainToplimit(RemainToplimitT);
//					}else if(RemainToplimitT==null && RemainToplimit!=null){
//						tp.setRemainToplimit(RemainToplimit);
//					}else if(RemainToplimit<=RemainToplimitT){
//						tp.setRemainToplimit(RemainToplimit);
//					}else{
//						tp.setRemainToplimit(RemainToplimitT);
//					}
			
			tp.setTicketType(tt);
			list.add(tp);
		}
		
		return list;
	}
	
	@Override
	public List<TicketPrice> findTicketList(TicketPrice ticketPrice,Timestamp useDate,String usePage,long userId,long roleId,String fun){
		List<TicketPrice> tpList = this.findTicketListSQL(ticketPrice, useDate, usePage, userId, roleId, fun);
		
		for(TicketPrice tp : tpList){
			
			Integer Toplimit = tp.getToplimit()==null?null:tp.getToplimit();
			Integer ToplimitT = tp.getToplimitT()==null?null:tp.getToplimitT();
			if((Toplimit==null || Toplimit==0) && (ToplimitT==null || ToplimitT==0)){
				tp.setToplimit(Toplimit);
			}else if((Toplimit==null || Toplimit==0) && ToplimitT!=null){
				tp.setToplimit(ToplimitT);
			}else if((ToplimitT==null || ToplimitT==0) && Toplimit!=null){
				tp.setToplimit(Toplimit);
			}else if(Toplimit.equals(ToplimitT)){
				tp.setToplimit(Toplimit);
			}else if(Toplimit<=ToplimitT && Toplimit>0){
				tp.setToplimit(Toplimit);
			}else{
				tp.setToplimit(ToplimitT);
			}
			
			
			Integer RemainToplimit = tp.getRemainToplimit()==null?null:tp.getRemainToplimit();
			Integer RemainToplimitT = tp.getRemainToplimitT()==null?null:tp.getRemainToplimitT();
			if((RemainToplimit==null || RemainToplimit==0) && (RemainToplimitT==null || RemainToplimitT==0) ){
				tp.setRemainToplimit(RemainToplimit);
			}else if((RemainToplimit==null || RemainToplimit==0) && RemainToplimitT!=null){
				tp.setRemainToplimit(RemainToplimitT);
			}else if((RemainToplimitT==null || RemainToplimitT==0) && RemainToplimit!=null){
				tp.setRemainToplimit(RemainToplimit);
			}else if(RemainToplimit.equals(RemainToplimitT)){
				tp.setRemainToplimit(RemainToplimit);
			}else if(RemainToplimit<=RemainToplimitT && RemainToplimit>0){
				tp.setRemainToplimit(RemainToplimit);
			}else{
				tp.setRemainToplimit(RemainToplimitT);
			}
		}
		
		return tpList;
	}
	@Override
	public List<TicketPrice> findTicketListByHsw(TicketPrice ticketPrice,Timestamp useDate,String usePage,long userId,long roleId,String fun){
		List<TicketPrice> tpList = this.findTicketListSQLByHsw(ticketPrice, useDate, usePage, userId, roleId, fun);
		
		for(TicketPrice tp : tpList){
			
			Integer Toplimit = tp.getToplimit()==null?null:tp.getToplimit();
			Integer ToplimitT = tp.getToplimitT()==null?null:tp.getToplimitT();
			if((Toplimit==null || Toplimit==0) && (ToplimitT==null || ToplimitT==0)){
				tp.setToplimit(Toplimit);
			}else if((Toplimit==null || Toplimit==0) && ToplimitT!=null){
				tp.setToplimit(ToplimitT);
			}else if((ToplimitT==null || ToplimitT==0) && Toplimit!=null){
				tp.setToplimit(Toplimit);
			}else if(Toplimit.equals(ToplimitT)){
				tp.setToplimit(Toplimit);
			}else if(Toplimit<=ToplimitT && Toplimit>0){
				tp.setToplimit(Toplimit);
			}else{
				tp.setToplimit(ToplimitT);
			}
			
			
			Integer RemainToplimit = tp.getRemainToplimit()==null?null:tp.getRemainToplimit();
			Integer RemainToplimitT = tp.getRemainToplimitT()==null?null:tp.getRemainToplimitT();
			if((RemainToplimit==null || RemainToplimit==0) && (RemainToplimitT==null || RemainToplimitT==0) ){
				tp.setRemainToplimit(RemainToplimit);
			}else if((RemainToplimit==null || RemainToplimit==0) && RemainToplimitT!=null){
				tp.setRemainToplimit(RemainToplimitT);
			}else if((RemainToplimitT==null || RemainToplimitT==0) && RemainToplimit!=null){
				tp.setRemainToplimit(RemainToplimit);
			}else if(RemainToplimit.equals(RemainToplimitT)){
				tp.setRemainToplimit(RemainToplimit);
			}else if(RemainToplimit<=RemainToplimitT && RemainToplimit>0){
				tp.setRemainToplimit(RemainToplimit);
			}else{
				tp.setRemainToplimit(RemainToplimitT);
			}
		}
		
		return tpList;
	}
	
	@Override
	public TicketPrice findTicketByPriceId(TicketPrice ticketPrice,Timestamp useDate,String usePage,long userId,long roleId,String fun,Long ticketPriceId){
		List<TicketPrice> tpList = findTicketListSQL(ticketPrice, useDate, usePage, userId, roleId, fun);
		
		TicketPrice reTp = null;
		for(TicketPrice tp : tpList){
			if(tp.getTicketPriceId().equals(ticketPriceId)){
				reTp = tp;
			}
		}
		return reTp;
	}
	@Override
	public TicketPrice findTicketByPriceIdByHsw(TicketPrice ticketPrice,Timestamp useDate,String usePage,long userId,long roleId,String fun,Long ticketPriceId){
		List<TicketPrice> tpList = findTicketListSQLByHsw(ticketPrice, useDate, usePage, userId, roleId, fun);
		
		TicketPrice reTp = null;
		for(TicketPrice tp : tpList){
			if(tp.getTicketPriceId().equals(ticketPriceId)){
				reTp = tp;
			}
		}
		return reTp;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map findTicketPriceTimeList(final String ticketId) {
		Session session = sessionFactory.getCurrentSession();
		String sql = "select cast(t.start_date as timestamp) as start_date,cast(t.end_date as timestamp) as end_date,min(t.ticket_price_id) from TICKET_PRICE t " +
					" where t.ticket_id = "+ticketId+
					" group by t.start_date,t.end_date order by t.start_date desc";
				
		Query query = session.createSQLQuery(sql);
		List l = query.list();
		
		List timeBList = new ArrayList();
		
		for(Object r : l){
			Object[] tempR = (Object[])r;
			Map tempBMap = new HashMap();
			tempBMap.put("startDate", tempR[0]);
			tempBMap.put("endDate", tempR[1]);
			tempBMap.put("id", tempR[2]);
			timeBList.add(tempBMap);
		}
		Map map = new HashMap();
		
		map.put("timeBList", timeBList);
		return map;
	}
}