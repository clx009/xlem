package dao.ticket.ticketPrice;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import com.xlem.dao.TicketPrice;

import common.dao.BaseDao;

public interface TicketPriceDao extends BaseDao<TicketPrice> {

	/**
	 * 查询项目票在该日预订情况
	 * @param t
	 * @param hotelRoomType
	 * @return
	 */
	int findTopLimit(Timestamp t, TicketPrice TicketPrice);

	/**
	 * 通过价格id查询票价和票类型
	 * @param ticketPriceId
	 * @return
	 */
	TicketPrice findPriceAndTypeById(Long ticketPriceId);
	
	/**
	 * 查询有权限的票，用于售票列表查询
	 * @param ticketPrice
	 * @param useDate
	 * @param usePage
	 * @param userId
	 * @param roleId
	 * @param fun
	 * @return
	 */
	public List<TicketPrice> findTicketList(TicketPrice ticketPrice,Timestamp useDate,String usePage,long userId,long roleId,String fun);
	
	
	/**
	 * 查询有权限的票，用于售票列表查询(花水湾)
	 * @param ticketPrice
	 * @param useDate
	 * @param usePage
	 * @param userId
	 * @param roleId
	 * @param fun
	 * @return
	 */
	public List<TicketPrice> findTicketListByHsw(TicketPrice ticketPrice,Timestamp useDate,String usePage,long userId,long roleId,String fun);
	
	/**
	 * 查询有权限的票，用于验证票权限查询
	 * @param ticketPrice
	 * @param useDate
	 * @param usePage
	 * @param userId
	 * @param roleId
	 * @param fun
	 * @return
	 */
	public TicketPrice findTicketByPriceId(TicketPrice ticketPrice,Timestamp useDate,String usePage,long userId,long roleId,String fun,Long ticketPriceId);
	
	/**
	 * 查询有权限的票，用于验证票权限查询(花水湾)
	 * @param ticketPrice
	 * @param useDate
	 * @param usePage
	 * @param userId
	 * @param roleId
	 * @param fun
	 * @return
	 */
	public TicketPrice findTicketByPriceIdByHsw(TicketPrice ticketPrice,Timestamp useDate,String usePage,long userId,long roleId,String fun,Long ticketPriceId);
	
	/**
	 * 查询票价时间段
	 * @param ticketId
	 * @return
	 */
	public Map findTicketPriceTimeList(final String ticketId);
}