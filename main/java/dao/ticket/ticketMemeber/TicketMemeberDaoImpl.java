package dao.ticket.ticketMemeber;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.TicketMemberList;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("ticketMemeberDao")
public class TicketMemeberDaoImpl extends BaseDaoImpl<TicketMemberList> implements TicketMemeberDao{

	@Override
	public void saveDataToDB(List<TicketMemberList> memebers) {
		for(TicketMemberList memeber:memebers){
			this.save(memeber);
		}
	}

}
