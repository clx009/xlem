package dao.ticket.ticketType;
import com.xlem.dao.TicketType;

import common.dao.BaseDao;
public interface TicketTypeDao extends BaseDao<TicketType> {

}