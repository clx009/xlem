package dao.ticket.ticketType; 
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.TicketType;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("ticketTypeDao")
public class TicketTypeDaoImpl extends BaseDaoImpl<TicketType> implements TicketTypeDao {

}