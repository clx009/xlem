package dao.ticket.ticketYear;
import com.xlem.dao.TicketYear;

import common.dao.BaseDao;

public interface TicketYearDao extends BaseDao<TicketYear> {

}