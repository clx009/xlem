package dao.ticket.ticketYear;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.TicketUseRec;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("ticketUseRecDao")
public class TicketUseRecDaoImpl extends BaseDaoImpl<TicketUseRec> implements TicketUseRecDao {

}