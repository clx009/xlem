package dao.ticket.ticketYear;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.TicketYear;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("ticketYearDao")
public class TicketYearDaoImpl extends BaseDaoImpl<TicketYear> implements TicketYearDao {


}