package dao.ticket.ticketModulePrice;
import java.util.List;
import java.util.Map;

import com.xlem.dao.TicketModulePrice;

import common.action.JsonPager;
import common.dao.BaseDao;

public interface TicketModulePriceDao extends BaseDao<TicketModulePrice> {

	public List<Map> findModulePriceList(TicketModulePrice t,long userId,JsonPager<Map> jp,List timeList);
	public Map findModulePriceListCount(long userId);
	
	public Map findModulePriceTimesCount(final long userId);
}