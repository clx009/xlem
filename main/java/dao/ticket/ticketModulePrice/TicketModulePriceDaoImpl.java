package dao.ticket.ticketModulePrice;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.TicketModulePrice;

import common.CommonMethod;
import common.action.JsonPager;
import common.dao.BaseDaoImpl;

@Transactional
@Repository("ticketModulePriceDao")
public class TicketModulePriceDaoImpl extends BaseDaoImpl<TicketModulePrice> implements TicketModulePriceDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Map> findModulePriceList(final TicketModulePrice t,final long userId,final JsonPager<Map> jp,final List timeList) {
		Session session = sessionFactory.getCurrentSession();
		if(timeList.size()<1){
			return new ArrayList<Map>();
		}
		
		int rownumS = Integer.valueOf(jp.getStart());
		int limit = Integer.valueOf(jp.getLimit());
		
		int rownumE = rownumS+limit;
		
		if(timeList.size()<(rownumE+1)){
			rownumE = timeList.size();
		}
		List<String> subTList = timeList.subList(rownumS, rownumE);
		
		String pageStartDate=null;
		String pageEndDate=null;
		if(subTList.size()>0){
			pageStartDate = subTList.get(subTList.size()-1).toString();
			pageEndDate = subTList.get(0).toString();
		}
		
		String sql = "select t.ticket_module_price_id,t.ticket_module_id,t.unit_price,"+
					"t.unit_price_group,t.print_price,cast(t.start_date as timestamp) as start_date,cast(t.end_date as timestamp) as end_date,"+
					"cast(t.status as varchar2(2)) as status,t.inputer,cast(t.input_time as timestamp) as input_time"+
					" from TICKET_MODULE_PRICE t where 1=1";
				
				if(pageStartDate!=null){
					sql = sql+" and t.start_date >= :pageStartDate";
				}
				
				if(pageEndDate!=null){
					sql = sql+" and t.start_date <= :pageEndDate";
				}
				
			sql = sql+" order by t.start_date desc,t.ticket_module_id asc";
		
			
		Query query = session.createSQLQuery(sql);

		if(pageStartDate!=null){
			query.setTimestamp("pageStartDate", CommonMethod.toStartTime(CommonMethod.string2Time1(pageStartDate)));
		}
		
		if(pageEndDate!=null){
			query.setTimestamp("pageEndDate", CommonMethod.toEndTime(CommonMethod.string2Time1(pageEndDate)));
		}
		
		List l = query.list();
		
		List<String> timeList1 = new ArrayList<String>();
		Map<String,Map> map = new HashMap<String,Map>();
		
		for(Object r : l){
			Object[] temp = (Object[])r;
			String startDate = temp[5].toString().substring(0, 10);
			Map teMap = map.get(startDate);
			
			if(teMap==null){
				teMap=new HashMap();
				timeList1.add(startDate);
			}
			
			long ticketModuleId = Long.valueOf(temp[1].toString());
			
			teMap.put("ticketModulePriceId_"+ticketModuleId, temp[0].toString());
			
			teMap.put("unitPrice_"+ticketModuleId, temp[2]);
			teMap.put("unitPriceGroup_"+ticketModuleId, temp[3]);
			teMap.put("printPrice_"+ticketModuleId, temp[4]);
			
			teMap.put("startDate", temp[5].toString().substring(0, 10));
			teMap.put("endDate", temp[6].toString().substring(0, 10));
			teMap.put("inputer", temp[8].toString());
			teMap.put("inputTime", temp[9].toString());
			
			map.put(startDate, teMap);
		}
		
		List<Map> list = new ArrayList<Map>();
		for (String key : timeList1) {
			list.add(map.get(key));
		}
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map findModulePriceListCount(final long userId) {
		Session session = sessionFactory.getCurrentSession();
		String sql = "select cast(t.start_date as timestamp) as start_date,cast(t.end_date as timestamp) as end_date,min(t.ticket_module_price_id) from TICKET_MODULE_PRICE t group by t.start_date,t.end_date order by t.start_date desc";
				
		Query query = session.createSQLQuery(sql);
		List l = query.list();
		
		List timeList = new ArrayList();
		List timeBList = new ArrayList();
		
		for(Object r : l){
			Object[] tempR = (Object[])r;
			Timestamp temp = (Timestamp)tempR[0];
			timeList.add(CommonMethod.timeToString(temp));
			Map tempBMap = new HashMap();
			tempBMap.put("startDate", tempR[0]);
			tempBMap.put("endDate", tempR[1]);
			tempBMap.put("id", tempR[2]);
			timeBList.add(tempBMap);
		}
		Map map = new HashMap();
		
		map.put("total", timeList.size());
		map.put("timeList", timeList);
		map.put("timeBList", timeBList);
		return map;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map findModulePriceTimesCount(final long userId) {
		Session session = sessionFactory.getCurrentSession();
		String sql = "select cast(t.start_date as timestamp) as start_date,cast(t.end_date as timestamp) as end_date,t.ticket_module_price_id from TICKET_MODULE_PRICE t group by t.start_date,t.end_date,t.ticket_module_price_id order by t.start_date desc";
				
		Query query = session.createSQLQuery(sql);
		List l = query.list();
		
		List timeList = new ArrayList();
		List timeBList = new ArrayList();
		
		for(Object r : l){
			Object[] tempR = (Object[])r;
			Timestamp temp = (Timestamp)tempR[0];
			timeList.add(CommonMethod.timeToString(temp));
			Map tempBMap = new HashMap();
			tempBMap.put("startDate", tempR[0]);
			tempBMap.put("endDate", tempR[1]);
			tempBMap.put("id", tempR[2]);
			timeBList.add(tempBMap);
		}
		Map map = new HashMap();
		
		map.put("total", timeList.size());
		map.put("timeList", timeList);
		map.put("timeBList", timeBList);
		return map;
	}
}