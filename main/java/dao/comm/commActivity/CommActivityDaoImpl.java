package dao.comm.commActivity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.CommActivity;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("commActivityDao")
public class CommActivityDaoImpl extends BaseDaoImpl<CommActivity> implements CommActivityDao {

}