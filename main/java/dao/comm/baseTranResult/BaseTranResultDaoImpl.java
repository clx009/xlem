package dao.comm.baseTranResult;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.BaseTranResult;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("baseTranResultDao")
public class BaseTranResultDaoImpl extends BaseDaoImpl<BaseTranResult>
		implements BaseTranResultDao {

}