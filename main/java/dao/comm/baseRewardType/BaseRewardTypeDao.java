package dao.comm.baseRewardType;
import java.util.Map;

import com.xlem.dao.BaseRewardType;

import common.dao.BaseDao;
public interface BaseRewardTypeDao extends BaseDao<BaseRewardType> {
	/**
	 * 查询奖励类型时间段
	 * @param ticketId
	 * @return
	 */
	public Map findRewardTypeTimeList(final String rewardTypeCode);
}