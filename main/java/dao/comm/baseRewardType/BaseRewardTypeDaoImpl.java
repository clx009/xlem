package dao.comm.baseRewardType;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.BaseRewardType;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("baseRewardTypeDao")
public class BaseRewardTypeDaoImpl extends BaseDaoImpl<BaseRewardType> implements BaseRewardTypeDao {
	@SuppressWarnings("unchecked")
	@Override
	public Map findRewardTypeTimeList(final String rewardTypeCode) {
		Session session = sessionFactory.getCurrentSession();
				
		String sql = "select cast(t.start_date as timestamp) as start_date,cast(t.end_date as timestamp) as end_date,min(t.reward_type_id) from BASE_REWARD_TYPE t " +
					" where t.reward_type_code = "+rewardTypeCode+
					" group by t.start_date,t.end_date order by t.start_date desc";
				
		Query query = session.createSQLQuery(sql);
		List l = query.list();
		
		List timeBList = new ArrayList();
		
		for(Object r : l){
			Object[] tempR = (Object[])r;
			Map tempBMap = new HashMap();
			tempBMap.put("startDate", tempR[0]);
			tempBMap.put("endDate", tempR[1]);
			tempBMap.put("id", tempR[2]);
			timeBList.add(tempBMap);
		}
		Map map = new HashMap();
		
		map.put("timeBList", timeBList);
		return map;
	}
	
}