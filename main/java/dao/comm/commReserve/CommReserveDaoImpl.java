package dao.comm.commReserve;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.CommReserve;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("commReserveDao")
public class CommReserveDaoImpl extends BaseDaoImpl<CommReserve> implements CommReserveDao {

}