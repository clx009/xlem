package dao.comm.baseRewardAccount;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.BaseReward;
import com.xlem.dao.BaseRewardAccount;
import com.xlem.dao.BaseRewardType;
import com.xlem.dao.Travservice;

import common.CommonMethod;
import common.action.JsonPager;
import common.dao.BaseDaoImpl;

@Transactional
@Repository("baseRewardAccountDao")
public class BaseRewardAccountDaoImpl extends BaseDaoImpl<BaseRewardAccount> implements BaseRewardAccountDao {

	@SuppressWarnings("unchecked")
	@Override
	public JsonPager<BaseReward> findReward(
			final JsonPager<BaseReward> jsonPager, final BaseRewardAccount baseRewardAccount) {
		Session session = sessionFactory.getCurrentSession();
		//1.count 总记录数
		StringBuffer querySQLCount = new StringBuffer();
		querySQLCount.append(" select count(*) from BASE_REWARD_TYPE c where c.status='11' ");
		if(baseRewardAccount.getRewardTypeCode()!=null){
			querySQLCount.append(" and c.REWARD_TYPE_CODE = '"+baseRewardAccount.getRewardTypeCode()+"' ");
		}
		Query queryCount = session.createSQLQuery(querySQLCount.toString());
		//1.1查询条件赋值
//				initCondition(queryCount,t,userId,forPage);
		jsonPager.setTotal(Integer.parseInt(queryCount.list().get(0).toString()));
		StringBuffer querySQL = new StringBuffer();
		querySQL.append(" select  c.REWARD_TYPE_ID, c.REWARD_TYPE_CODE,c.REWARD_TYPE_NAME,c.START_DATE,c.END_DATE, ");
		querySQL.append(" arc.REWARD_ACCOUNT_ID, arc.TRAVEL_AGENCY_ID,arc.REWARD_BALANCE,arc.REWARD_BALANCE_SNOW,arc.VERSION ");
		querySQL.append(" from BASE_REWARD_TYPE c ");
		querySQL.append(" left join BASE_REWARD_ACCOUNT arc ");
		querySQL.append(" on c.REWARD_TYPE_ID = arc.REWARD_TYPE_ID ");
		querySQL.append(" and arc.TRAVEL_AGENCY_ID = '"+baseRewardAccount.getBaseTravelAgency().getTravelAgencyId()+"' ");
		querySQL.append(" where c.status = '11' ");
		if(baseRewardAccount.getRewardTypeCode()!=null){
			querySQL.append(" and c.REWARD_TYPE_CODE = '"+baseRewardAccount.getRewardTypeCode()+"' ");
		}
		querySQL.append(" order by c.END_DATE desc,c.REWARD_TYPE_CODE ");
		//2.查询分页记录
		Query query = session.createSQLQuery(querySQL.toString());
		//2.1查询条件赋值
//				initCondition(query,t,userId,forPage);
		query.setFirstResult(Integer.parseInt(jsonPager.getStart()));
		query.setMaxResults(Integer.parseInt(jsonPager.getLimit()));
		query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		//3.循环填充list
		List<BaseReward> baList=new ArrayList<BaseReward>();
		List<Object> list = query.list();
		for (Object obj : list) {
			Map map = (Map)obj;
			BaseReward br = new BaseReward();
			BaseRewardType brt = new BaseRewardType();
			brt.setRewardTypeId(map.get("REWARD_TYPE_ID")==null?null:Long.parseLong(map.get("REWARD_TYPE_ID").toString()));
			brt.setRewardTypeCode(map.get("REWARD_TYPE_CODE")==null?null:map.get("REWARD_TYPE_CODE").toString());
			brt.setRewardTypeName(map.get("REWARD_TYPE_NAME")==null?null:map.get("REWARD_TYPE_NAME").toString());
			brt.setStartDate(CommonMethod.string2Time1(map.get("START_DATE").toString()));
			brt.setEndDate(CommonMethod.string2Time1(map.get("END_DATE").toString()));
			br.setBaseRewardType(brt);
			BaseRewardAccount ra = new BaseRewardAccount();
			ra.setRewardAccountId(map.get("REWARD_ACCOUNT_ID")==null?null:Long.parseLong(map.get("REWARD_ACCOUNT_ID").toString()));
			ra.setBaseTravelAgency(new Travservice(map.get("TRAVEL_AGENCY_ID")==null?baseRewardAccount.getBaseTravelAgency().getTravelAgencyId():Long.parseLong(map.get("TRAVEL_AGENCY_ID").toString()), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null));
			ra.setRewardBalance(map.get("REWARD_BALANCE")==null?null:Double.valueOf(map.get("REWARD_BALANCE").toString()));
			ra.setRewardBalanceSnow(map.get("REWARD_BALANCE_SNOW")==null?null:Double.valueOf(map.get("REWARD_BALANCE_SNOW").toString()));
			ra.setVersion(map.get("VERSION")==null?null:Long.parseLong(map.get("VERSION").toString()));
			br.setBaseRewardAccount(ra);
			br.setTravelAgencyId(map.get("TRAVEL_AGENCY_ID")==null?baseRewardAccount.getBaseTravelAgency().getTravelAgencyId():Long.parseLong(map.get("TRAVEL_AGENCY_ID").toString()));
			baList.add(br);
		}
		
		jsonPager.setRoot(baList);
		return jsonPager;
	}
	
	
	@Override
	public BaseRewardAccount findAccount(Long travelAgencyId, String rewardTypeCode,Timestamp startDate,Timestamp endDate) {
		DetachedCriteria dc = DetachedCriteria.forClass(BaseRewardAccount.class);
		dc.createAlias("baseRewardType", "baseRewardType");
		dc.add(Property.forName("baseTravelAgency.travelAgencyId").eq(travelAgencyId));
		dc.add(Property.forName("baseRewardType.rewardTypeCode").eq(rewardTypeCode));
		dc.add(Property.forName("baseRewardType.status").eq("11"));
		dc.add(Property.forName("baseRewardType.startDate").le(CommonMethod.toStartTime(startDate)));
		
		if("01".equals(rewardTypeCode)){//如果是酒店结束时间往前推一天
			endDate = CommonMethod.addDay(endDate, -1);
		}
		dc.add(Property.forName("baseRewardType.endDate").ge(CommonMethod.toEndTime(endDate)));
		
		List<BaseRewardAccount> baseRewardAccounts = findByCriteria(dc);
		if(baseRewardAccounts==null || baseRewardAccounts.isEmpty()){
			return null;
		}
		else{
			return baseRewardAccounts.get(0);
		}
	}

}