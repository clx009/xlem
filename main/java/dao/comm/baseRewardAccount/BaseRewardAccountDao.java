package dao.comm.baseRewardAccount;
import java.sql.Timestamp;

import com.xlem.dao.BaseReward;
import com.xlem.dao.BaseRewardAccount;

import common.action.JsonPager;
import common.dao.BaseDao;

public interface BaseRewardAccountDao extends BaseDao<BaseRewardAccount> {

	JsonPager<BaseReward> findReward(JsonPager<BaseReward> jp,
			BaseRewardAccount baseRewardAccount);
	
	/**
	 * 获取对应旅行社票奖励帐户
	 * @param travelAgencyId 旅行社ID
	 * @param rewardTypeCode 奖励款类型编码
	 * @return
	 */
	BaseRewardAccount findAccount(Long travelAgencyId, String rewardTypeCode,Timestamp startDate,Timestamp endDate);

}