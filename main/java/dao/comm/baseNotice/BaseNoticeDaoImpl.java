package dao.comm.baseNotice;
import org.springframework.stereotype.Repository;

import com.xlem.dao.BaseNotice;

import common.dao.BaseDaoImpl;

@Repository("baseNoticeDao")
public class BaseNoticeDaoImpl extends BaseDaoImpl<BaseNotice> implements BaseNoticeDao {

}