package dao.comm.information.baseTourGuide;
import com.xlem.dao.BaseTourGuide;

import common.dao.BaseDao;

public interface BaseTourGuideDao extends BaseDao<BaseTourGuide> {

}