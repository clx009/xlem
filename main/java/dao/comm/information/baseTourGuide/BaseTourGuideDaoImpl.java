package dao.comm.information.baseTourGuide;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.BaseTourGuide;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("baseTourGuideDao")
public class BaseTourGuideDaoImpl extends BaseDaoImpl<BaseTourGuide> implements BaseTourGuideDao {

}