package dao.comm.baseRechargeRec;
import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.TicketRechargeRec;

import common.action.JsonPager;
import common.dao.BaseDao;

public interface BaseRechargeRecDao extends BaseDao<BaseRechargeRec> {

	public JsonPager<TicketRechargeRec> findTicketRechargeRec(final JsonPager<TicketRechargeRec> jp, final BaseRechargeRec t);
	
}