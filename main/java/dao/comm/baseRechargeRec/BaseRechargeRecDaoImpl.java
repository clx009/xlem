package dao.comm.baseRechargeRec;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.TicketRechargeRec;

import common.CommonMethod;
import common.action.JsonPager;
import common.dao.BaseDaoImpl;

@Transactional
@Repository("baseRechargeRecDao")
public class BaseRechargeRecDaoImpl extends BaseDaoImpl<BaseRechargeRec> implements BaseRechargeRecDao {


	@SuppressWarnings("unchecked")
	@Override
	public JsonPager<TicketRechargeRec> findTicketRechargeRec(final JsonPager<TicketRechargeRec> jp,final BaseRechargeRec t) {
		Session session = sessionFactory.getCurrentSession();
		
		StringBuffer querySQL = new StringBuffer();
		querySQL.append(" select bta.travel_agency_name,brt.reward_type_name, ");
		querySQL.append(" cast(brr.payment_type as varchar2(2)) as payment_type,sum(nvl(brr.reward_money,0)) as reward_money," +
				"sum(nvl(brr.reward_money_snow,0)) as reward_money_snow," +
				"to_char(brr.input_time,'yyyy-mm-dd') as input_time ");
		querySQL.append(" from BASE_RECHARGE_REC brr ");
		querySQL.append(" left join BASE_REWARD_ACCOUNT bra on brr.reward_account_id = bra.reward_account_id ");
		querySQL.append(" left join BASE_REWARD_TYPE brt on bra.reward_type_id = brt.reward_type_id ");
		querySQL.append(" left join BASE_TRAVEL_AGENCY bta on bra.travel_agency_id = bta.travel_agency_id ");
		querySQL.append(" where brr.cash_money is null and brr.cash_balance is null ");
		initConditionSQL( querySQL , t);
		querySQL.append(" group by to_char(brr.input_time,'yyyy-mm-dd'),bta.travel_agency_name, ");
		querySQL.append(" brt.reward_type_name,brr.payment_type ");
		querySQL.append(" order by to_char(brr.input_time,'yyyy-mm-dd') desc,bta.travel_agency_name, ");
		querySQL.append(" brt.reward_type_name,brr.payment_type ");
				
		//1.count 总记录数
		StringBuffer querySQLCount = new StringBuffer();
		querySQLCount.append(" select count(a.input_time) as total from (");
		querySQLCount.append(querySQL);
		querySQLCount.append(" ) a");
		
		Query queryCount = session.createSQLQuery(querySQLCount.toString());
		//1.1查询条件赋值
		initCondition(queryCount,t);
		jp.setTotal(Integer.parseInt(queryCount.list().get(0).toString()));
		
		//2.查询分页记录
		Query query = session.createSQLQuery(querySQL.toString());
		//2.1查询条件赋值
		initCondition(query,t);
		query.setFirstResult(Integer.parseInt(jp.getStart()));
		query.setMaxResults(Integer.parseInt(jp.getLimit()));
		query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		//3.循环填充list
		List<TicketRechargeRec> infoList=new ArrayList<TicketRechargeRec>();
		for (Object obj : query.list()) {
			Map map = (Map)obj;
			TicketRechargeRec ti = new TicketRechargeRec();
			
				ti.setTravelAgencyName(map.get("travel_agency_name".toUpperCase())==null?null:map.get("travel_agency_name".toUpperCase()).toString());
				ti.setRewardTypeName(map.get("reward_type_name".toUpperCase())==null?null:map.get("reward_type_name".toUpperCase()).toString());
				ti.setPaymentType(map.get("payment_type".toUpperCase())==null?null:map.get("payment_type".toUpperCase()).toString());
				ti.setRewardMoney(map.get("reward_money".toUpperCase())==null?null:Double.valueOf(map.get("reward_money".toUpperCase()).toString()));
				ti.setRewardMoneySnow(map.get("reward_money_snow".toUpperCase())==null?null:Double.valueOf(map.get("reward_money_snow".toUpperCase()).toString()));
				ti.setInputTime(map.get("input_time".toUpperCase())==null?null:map.get("input_time".toUpperCase()).toString());
			infoList.add(ti);
		}
		
		jp.setRoot(infoList);
		return jp;
	}
	
	private void initConditionSQL(StringBuffer querySQL ,BaseRechargeRec t){
		
		if(t.getStartDate()!=null){
			querySQL.append(" and brr.input_Time >= :startDate ");
		}
		
		if(t.getEndDate()!=null){
			querySQL.append(" and brr.input_Time <= :endDate ");
		}
		if(t.getPaymentType()!=null && !t.getPaymentType().isEmpty()){
				querySQL.append(" and brr.payment_type like :paymentType ");
		}
		if(t.getBaseTravelAgency()!=null && t.getBaseTravelAgency().getTravelAgencyName()!=null && !t.getBaseTravelAgency().getTravelAgencyName().trim().isEmpty()){
			querySQL.append(" and bta.travel_agency_name like :travelAgencyName ");
		}
		
	}
	
	private void initCondition(Query query ,BaseRechargeRec t){
		
		if(t.getStartDate()!=null){
			query.setTimestamp("startDate", CommonMethod.toStartTime(t.getStartDate()));
		}
		
		if(t.getEndDate()!=null){
			query.setTimestamp("endDate", CommonMethod.toEndTime(Timestamp.valueOf(t.getEndDate().toString())));
		}
		if(t.getPaymentType()!=null && !t.getPaymentType().trim().isEmpty()){
			query.setString("paymentType", t.getPaymentType());
		}
		if(t.getBaseTravelAgency()!=null && t.getBaseTravelAgency().getTravelAgencyName()!=null && !t.getBaseTravelAgency().getTravelAgencyName().trim().isEmpty()){
			query.setString("travelAgencyName", "%"+t.getBaseTravelAgency().getTravelAgencyName().trim()+"%");
		}
		
	}
	
}