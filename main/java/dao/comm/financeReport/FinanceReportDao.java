package dao.comm.financeReport;
import java.util.List;

import com.xlem.dao.BaseTranRec;
import com.xlem.dao.FinanceReport;
import com.xlem.dao.TicketCheckRec;

import common.dao.BaseDao;
public interface FinanceReportDao extends BaseDao<FinanceReport> {
}