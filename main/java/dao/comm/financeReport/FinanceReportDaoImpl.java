package dao.comm.financeReport;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.BaseTranRec;
import com.xlem.dao.FinanceReport;
import com.xlem.dao.TicketCheckRec;

import common.CommonMethod;
import common.dao.BaseDaoImpl;

@Transactional
@Repository("financeReportDao")
public class FinanceReportDaoImpl extends BaseDaoImpl<FinanceReport> implements FinanceReportDao {
}