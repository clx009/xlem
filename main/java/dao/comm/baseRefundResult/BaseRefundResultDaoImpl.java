package dao.comm.baseRefundResult;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.BaseRefundResult;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("baseRefundResultDao")
public class BaseRefundResultDaoImpl extends BaseDaoImpl<BaseRefundResult> implements BaseRefundResultDao {

}