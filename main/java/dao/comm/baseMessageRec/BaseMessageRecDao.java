package dao.comm.baseMessageRec;

import com.xlem.dao.BaseMessageRec;

import common.dao.BaseDao;

public interface BaseMessageRecDao extends BaseDao<BaseMessageRec> {
	
	/**
	 * @param phoneNumber 发送号码
	 * @param content 发送内容
	 */
	void saveMess(String phoneNumber, String content);

}