package dao.comm.baseMessageRec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.BaseMessageRec;

import common.CommonMethod;
import common.dao.BaseDaoImpl;
import dao.sys.sysTablePk.SysTablePkDao;

@Transactional
@Repository("baseMessageRecDao")
public class BaseMessageRecDaoImpl extends BaseDaoImpl<BaseMessageRec> implements BaseMessageRecDao {
	@Autowired
	private SysTablePkDao sysTablePkDao;
	@Override
	public void saveMess(String phoneNumber, String content) {
		// TODO Auto-generated method stub
		BaseMessageRec mess = new BaseMessageRec(sysTablePkDao.getNextKey("BASE_MESSAGE_REC", 1));
		mess.setPhoneNumber(phoneNumber);
		mess.setMessageContent(content);
		mess.setSendTime(CommonMethod.getTimeStamp());
		save(mess);
	}

}