package dao.comm.baseCashAccount;
import com.xlem.dao.BaseCashAccount;

import common.dao.BaseDao;
public interface BaseCashAccountDao extends BaseDao<BaseCashAccount> {

}