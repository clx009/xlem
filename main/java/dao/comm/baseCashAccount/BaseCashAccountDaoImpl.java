package dao.comm.baseCashAccount;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.BaseCashAccount;
import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.BaseTranRec;
import com.xlem.dao.TicketIndent;

import common.CommonMethod;
import common.dao.BaseDaoImpl;
import common.pay.PayFactoryService;
import common.pay.PayParams;
import dao.sys.sysParam.SysParamDao;

@Transactional
@Repository("baseCashAccountDao")
public class BaseCashAccountDaoImpl extends BaseDaoImpl<BaseCashAccount> implements BaseCashAccountDao {
}