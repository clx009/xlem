package dao.comm.reg.baseTravelAgency;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.SysTablePk;
import com.xlem.dao.SysUser;
import com.xlem.dao.Travservice;

import common.dao.BaseDaoImpl;

import dao.sys.sysUser.SysUserDao;

@Transactional
@Repository("baseTravelAgencyDao")
public class BaseTravelAgencyDaoImpl extends BaseDaoImpl<Travservice> implements BaseTravelAgencyDao {

	@Autowired
	private SysUserDao sysUserDao;
	
	@Override
	public synchronized String getNextUserCode() {
		SysTablePk sysTablepk = new SysTablePk();
		sysTablepk = (SysTablePk)sessionFactory.getCurrentSession().get(SysTablePk.class, "USER_CODE");
		String baseCode = "lxszh00001";
		if(sysTablepk==null || sysTablepk.getTableName() == null){//无数据时，新增
			sysTablepk = new SysTablePk();
			sysTablepk.setTableName("USER_CODE");
			sysTablepk.setCurrentPk(baseCode);
			sessionFactory.getCurrentSession().save(sysTablepk);
			return baseCode;
		}else{
				String code = sysTablepk.getCurrentPk();
				String str = "lxszh"+String.format("%0"+5+"d", Integer.parseInt(code.substring(5, code.length()))+1);
				sysTablepk.setTableName("USER_CODE");
				sysTablepk.setCurrentPk(str);
				sessionFactory.getCurrentSession().update(sysTablepk);
				return str;
		}
	}

	@Override
	public Travservice findTravelAgencyByUser(long userId) {
		DetachedCriteria dc = DetachedCriteria.forClass(SysUser.class);
		dc.createAlias("baseTravelAgency", "baseTravelAgency");
		dc.add(Property.forName("userId").eq(userId));
		dc.add(Property.forName("baseTravelAgency.travelAgencyId").isNotNull());
		List<SysUser> list = sysUserDao.findByCriteria(dc);
		if(list.isEmpty()){
			return new Travservice();
		}else{
			SysUser sysUser = list.get(0);
			Travservice baseTravelAgency = sysUser.getBaseTravelAgency();
			baseTravelAgency.setMailbox(sysUser.getMailbox());
			baseTravelAgency.setUserCode(sysUser.getUserCode());
			return baseTravelAgency;
		}
	}
	
}