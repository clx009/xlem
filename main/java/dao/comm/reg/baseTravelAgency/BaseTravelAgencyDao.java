package dao.comm.reg.baseTravelAgency;
import com.xlem.dao.Travservice;

import common.dao.BaseDao;

public interface BaseTravelAgencyDao extends BaseDao<Travservice> {

	String getNextUserCode();

	/**
	 * 
	 * @param userId 用户id
	 * @return 旅行社
	 */
	Travservice findTravelAgencyByUser(long userId);
	
}