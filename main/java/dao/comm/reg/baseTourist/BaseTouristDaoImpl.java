package dao.comm.reg.baseTourist;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.BaseTourist;
import com.xlem.dao.SysTablePk;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("baseTouristDao")
public class BaseTouristDaoImpl extends BaseDaoImpl<BaseTourist> implements BaseTouristDao {

	@Override
	public synchronized String getNextUserCode() {
		SysTablePk sysTablepk = new SysTablePk();
		sysTablepk = (SysTablePk)sessionFactory.getCurrentSession().get(SysTablePk.class, "USER_CODE_Tourist");
		String baseCode = "PTYK00001";
		if(sysTablepk==null || sysTablepk.getTableName() == null){//无数据时，新增
			sysTablepk = new SysTablePk();
			sysTablepk.setTableName("USER_CODE_Tourist");
			sysTablepk.setCurrentPk(baseCode);
			sessionFactory.getCurrentSession().save(sysTablepk);
			return baseCode;
		}else{
				String code = sysTablepk.getCurrentPk();
				String str = "PTYK"+String.format("%0"+5+"d", Integer.parseInt(code.substring(5, code.length()))+1);
				sysTablepk.setTableName("USER_CODE_Tourist");
				sysTablepk.setCurrentPk(str);
				sessionFactory.getCurrentSession().update(sysTablepk);
				return str;
		}
	}
	
}