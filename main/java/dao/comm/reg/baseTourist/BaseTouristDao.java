package dao.comm.reg.baseTourist;
import com.xlem.dao.BaseTourist;

import common.dao.BaseDao;

public interface BaseTouristDao extends BaseDao<BaseTourist> {

	String getNextUserCode();
	
}