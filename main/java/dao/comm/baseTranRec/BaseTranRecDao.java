package dao.comm.baseTranRec;
import java.util.List;

import com.xlem.dao.BaseTranRec;
import com.xlem.dao.TicketCheckRec;

import common.dao.BaseDao;
public interface BaseTranRecDao extends BaseDao<BaseTranRec> {
	
	/**
	 * 查询网银收益数据
	 */
	public List<TicketCheckRec> findGridByAccounts(TicketCheckRec ticketCheckRec);
	
	/**
	 * 查询酒店网银收益数据
	 */
	public List<TicketCheckRec> findHotelGridByAccounts(TicketCheckRec ticketCheckRec);

}