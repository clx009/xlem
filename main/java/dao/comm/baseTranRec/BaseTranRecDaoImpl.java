package dao.comm.baseTranRec;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.BaseTranRec;
import com.xlem.dao.TicketCheckRec;

import common.CommonMethod;
import common.dao.BaseDaoImpl;

@Transactional
@Repository("baseTranRecDao")
public class BaseTranRecDaoImpl extends BaseDaoImpl<BaseTranRec> implements BaseTranRecDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<TicketCheckRec> findGridByAccounts(final TicketCheckRec ticketCheckRec) {
		Session session = sessionFactory.getCurrentSession();
		
		StringBuffer sql = new StringBuffer("(select sum(t.money) as pMoney,cast(t.payment_type as varchar2(2)) as payment_type");
		sql.append(" from base_tran_rec t");
		sql.append(" where t.pay_type = '01'");
		if(ticketCheckRec.getStartTime()!=null){
			String sDate = CommonMethod.timeToString(Timestamp.valueOf(ticketCheckRec.getStartTime().toString()));
			String eDate = CommonMethod.addDay(sDate,1);
			sql.append(" and t.payment_time >= TO_DATE('"+sDate+" 02:00:00','yyyy-mm-dd hh24:mi:ss')");
			sql.append(" and t.payment_time < TO_DATE('"+eDate+" 02:00:00','yyyy-mm-dd hh24:mi:ss')");
		}
		sql.append(" and t.terminal_no is null and t.card_number is null and t.indent_id is not null");
		sql.append(" group by t.payment_type ) ");
		sql.append(" union all ");
		sql.append(" (select sum(t.money) as  pMoney,'03' ");
		sql.append(" from base_tran_rec t");
		sql.append(" where ");
		sql.append(" t.pay_type = '01' ");
		if(ticketCheckRec.getStartTime()!=null){
			String sDate = CommonMethod.timeToString(Timestamp.valueOf(ticketCheckRec.getStartTime().toString()));
			String rDate = CommonMethod.addDay(sDate, -3);
			sql.append(" and to_char(t.payment_time, 'yyyy-MM-dd') = '"+rDate+"'");
		}
		sql.append(" and t.terminal_no is not null and t.card_number is not null )");
			
		Query query = session.createSQLQuery(sql.toString());
		@SuppressWarnings("rawtypes")
		List l = query.list();
		
		TicketCheckRec tcr = new TicketCheckRec();
		
		List tcrList = new ArrayList<TicketCheckRec>();
		
		for(Object r : l){
			
			Object[] temp = (Object[])r;
			
			//支付宝票收入
			if("01".equals(temp[1].toString())){
				if(null != temp[0]){
					tcr.setMoney(Double.valueOf(temp[0].toString()));
				}
			}
			//支付宝票退款
			if("02".equals(temp[1].toString())){
				if(null != temp[0]){
					tcr.setEntranceMoney(Double.valueOf(temp[0].toString()));
				}
			}
			//易宝购票机收入
			if("03".equals(temp[1].toString())){
				if(null != temp[0]){
					tcr.setRopewayMoney(Double.valueOf(temp[0].toString()));
				}
			}
		}
		tcrList.add(tcr);
		return tcrList;
						
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TicketCheckRec> findHotelGridByAccounts(final TicketCheckRec ticketCheckRec) {
		Session session = sessionFactory.openSession();
		StringBuffer sql = new StringBuffer("select sum(t.money) as pMoney,cast(t.payment_type as varchar2(2)) as payment_type");
		sql.append(" from base_tran_rec t");
		sql.append(" where t.pay_type = '01'");
		if(ticketCheckRec.getStartTime()!=null){
			String sDate = CommonMethod.timeToString(Timestamp.valueOf(ticketCheckRec.getStartTime().toString()));
			String eDate = CommonMethod.addDay(sDate,1);
			sql.append(" and t.payment_time >= TO_DATE('"+sDate+" 02:00:00','yyyy-mm-dd hh24:mi:ss')");
			sql.append(" and t.payment_time < TO_DATE('"+eDate+" 02:00:00','yyyy-mm-dd hh24:mi:ss')");
		}
		sql.append(" and t.terminal_no is null and t.card_number is null and t.hotel_indent_id is not null");
		sql.append(" group by t.payment_type ");
			
		Query query = session.createSQLQuery(sql.toString());
		@SuppressWarnings("rawtypes")
		List l = query.list();
		
		TicketCheckRec tcr = new TicketCheckRec();
		
		List tcrList = new ArrayList<TicketCheckRec>();
		
		for(Object r : l){
			
			Object[] temp = (Object[])r;
			
			//支付宝票收入
			if("01".equals(temp[1].toString())){
				if(null != temp[0]){
					tcr.setMoney(Double.valueOf(temp[0].toString()));
				}
			}
			//支付宝票退款
			if("02".equals(temp[1].toString())){
				if(null != temp[0]){
					tcr.setEntranceMoney(Double.valueOf(temp[0].toString()));
				}
			}
		}
		tcrList.add(tcr);
				
	
		return tcrList;
	}
}