package dao.sys.sysFunction;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import com.xlem.dao.SysFunction;
import com.xlem.dao.SysFunctionCheck;
import com.xlem.dao.SysFunctionNew;
import com.xlem.dao.SysRoleFunction;

import common.dao.BaseDao;
public interface SysFunctionDao extends BaseDao<SysFunction> {

	List<SysFunctionCheck> findFunctionTree(SysRoleFunction sysRoleFunction, String nodeId);

	List<SysFunctionNew> findNew(DetachedCriteria dc);

}