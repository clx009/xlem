package dao.sys.sysFunction;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.SysFunction;
import com.xlem.dao.SysFunctionCheck;
import com.xlem.dao.SysFunctionNew;
import com.xlem.dao.SysRoleFunction;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("sysFunctionDao")
public class SysFunctionDaoImpl extends BaseDaoImpl<SysFunction> implements SysFunctionDao {

	@Override
	public List<SysFunctionCheck> findFunctionTree(SysRoleFunction sysRoleFunction,String nodeId) {
		if(nodeId ==null || nodeId.isEmpty()){
			nodeId = "0";
		}
		DetachedCriteria dc = DetachedCriteria.forClass(SysFunctionCheck.class);
		dc.add(Property.forName("status").eq("11"));
		dc.add(Property.forName("fatherFunctionId").eq(Long.parseLong(nodeId)));
		// List<SysFunctionCheck> sfcList = getHibernateTemplate().findByCriteria(dc);
        Criteria criteria = dc.getExecutableCriteria(sessionFactory.getCurrentSession());
        List<SysFunctionCheck> sfcList = criteria.list();
		for (SysFunctionCheck sfc : sfcList) {
			DetachedCriteria dcRF= DetachedCriteria.forClass(SysRoleFunction.class);
			dcRF.add(Property.forName("sysRole").eq(sysRoleFunction.getSysRole()));
			dcRF.add(Property.forName("sysFunction").eq(new SysFunction(sfc.getFunctionId())));
			Criteria criteriaRF = dcRF.getExecutableCriteria(sessionFactory.getCurrentSession());
			if(criteriaRF.list().size()>0){
				sfc.setChecked(true);
			}
		}
		return sfcList;		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SysFunctionNew> findNew(DetachedCriteria dc) {
        Criteria criteria = dc.getExecutableCriteria(sessionFactory.getCurrentSession());
		// return getHibernateTemplate().findByCriteria(dc);
		return criteria.list();
	}

}