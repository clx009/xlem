package dao.sys.sysRoleFunction;
import com.xlem.dao.SysRoleFunction;

import common.dao.BaseDao;

public interface SysRoleFunctionDao extends BaseDao<SysRoleFunction> {

	public void findFunctionTree(String nodeId,String treeId) ;

	public void deleteByRole(SysRoleFunction sysRoleFunction);

}