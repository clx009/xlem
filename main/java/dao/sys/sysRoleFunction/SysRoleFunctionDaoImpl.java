package dao.sys.sysRoleFunction;
import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.SysRoleFunction;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("sysRoleFunctionDao")
public class SysRoleFunctionDaoImpl extends BaseDaoImpl<SysRoleFunction> implements SysRoleFunctionDao {

	@Override
	public void findFunctionTree(final String nodeId,final String treeId) {
		Session session = sessionFactory.getCurrentSession();
				String sql = "select a.function_id as id,function_name as text,function_code as code,"
				+ " father_function_id as parent,function_tp,is_log,is_valid,sort_no as sortno,memo,case is_last when 1 then 'true' else 'false' end as leaf"
				+ " ,case nvl(b.role_function_id,0) when 0 then 'false' else 'true' end as checked"
				+ " from sys_function a left join (select role_function_id,function_id from sys_role_function where "
				+ " role_id="
				+ treeId
				+ ") b on a.function_id  = b.function_id where a.is_valid=1 and father_function_id="
				+ nodeId + " order by function_code ";
				return;
	}

	@Override
	public void deleteByRole(final SysRoleFunction sysRoleFunction) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "delete SysRoleFunction s where s.sysRole.roleId = :roleId";
		session.createQuery(hql)
				.setLong("roleId", sysRoleFunction.getSysRole().getRoleId())
				.executeUpdate();
	}


}