package dao.sys.sysUser;
import java.util.List;

import com.xlem.dao.SysUser;

import common.dao.BaseDao;

public interface SysUserDao extends BaseDao<SysUser> {

	/**
	 * @param currentUserId 用户id
	 * @return true是协议旅行社，false不是协议旅行社
	 */
	boolean findIsAgreementByUseId(Long currentUserId);

	/**
	 * 获取市场部酒店订单录入人列表
	 */
	public List<SysUser> findInputerList();
}