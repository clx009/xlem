package dao.sys.sysUser;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.SysUser;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("sysUserDao")
public class SysUserDaoImpl extends BaseDaoImpl<SysUser> implements SysUserDao {

	@Override
	public boolean findIsAgreementByUseId(Long currentUserId) {
		DetachedCriteria dc = DetachedCriteria.forClass(SysUser.class);
		dc.createAlias("baseTravelAgency", "baseTravelAgency",DetachedCriteria.INNER_JOIN);
		dc.add(Property.forName("userId").eq(currentUserId));
		dc.add(Property.forName("baseTravelAgency.isAgreement").eq("11"));//11已签订协议
		if(!findByCriteria(dc).isEmpty()){
			return true;
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SysUser> findInputerList() {
		Session session = sessionFactory.getCurrentSession();
				
		String sql = "select distinct su.* from HOTEL_INDENT hi,SYS_USER su"+
				" where hi.inputer = su.user_id and su.user_type = '01'";
			
		Query query = session.createSQLQuery(sql);
		query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		List l = query.list();
		
		List<SysUser> list = new ArrayList<SysUser>();
		
		for (Object obj : l) {
			Map map = (Map)obj;
			SysUser su = new SysUser();
			
			su.setUserId(map.get("USER_ID")==null?null:Long.valueOf(map.get("USER_ID").toString()));
			su.setUserName(map.get("USER_NAME")==null?null:map.get("USER_NAME").toString());

			list.add(su);
		}
		
		return list;
	}
}