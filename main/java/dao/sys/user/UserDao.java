package dao.sys.user;

import com.xlem.dao.User;

import common.dao.BaseDao;

public interface UserDao extends BaseDao<User> {

}