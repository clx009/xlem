package dao.sys.user;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.User;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("userDao")
public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao {

} 