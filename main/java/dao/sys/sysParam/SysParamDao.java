package dao.sys.sysParam;
import com.xlem.dao.SysParam;

import common.dao.BaseDao;

public interface SysParamDao extends BaseDao<SysParam> {

	String findSysParam(String string);

	/**
	 * 查询支付宝账号
	 * @param string 仅查询账户字段："alipay_key_value","partner","seller_email"
	 * @param type  01 票账号，02 酒店账号
	 * @return
	 */
	String findSysParam(String string,String type);
}