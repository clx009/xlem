package dao.sys.sysParam;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.SysParam;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("sysParamDao")
public class SysParamDaoImpl extends BaseDaoImpl<SysParam> implements SysParamDao {

	@Override
	public String findSysParam(String paramName) {
		DetachedCriteria dc = DetachedCriteria.forClass(SysParam.class);
		dc.add(Property.forName("paramName").eq(paramName.toUpperCase()));
		dc.add(Property.forName("status").eq("11"));
		List<SysParam> list = findByCriteria(dc);
		if(list!=null && list.size()>0){
			return list.get(0).getParamValue();
		}
		else{
			return "";
		}
	}

	@Override
	public String findSysParam(String string,String type) {
		String reString = "";
		
		if("01".equals(type)){//票账户
			reString = this.findSysParam(string);
		}else if("02".equals(type)){//酒店账户
			reString = this.findSysParam(string+"_hotel");
		}else if("03".equals(type)){//分润账户
			reString = this.findSysParam(string+"_sub");
		}
		
		return reString;
	}


}