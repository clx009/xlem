package dao.sys.sysLog;
import org.springframework.stereotype.Repository;

import com.xlem.dao.SysLog;

import common.dao.BaseDaoImpl;
@Repository("sysLogDao")
public class SysLogDaoImpl extends BaseDaoImpl<SysLog> implements SysLogDao {

}