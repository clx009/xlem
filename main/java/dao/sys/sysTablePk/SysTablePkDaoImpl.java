package dao.sys.sysTablePk;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.SysTablePk;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("sysTablePkDao")
public  class SysTablePkDaoImpl extends BaseDaoImpl<SysTablePk> implements SysTablePkDao {

	/**
	 * 获取主键值
	 * 
	 * @param tablename
	 *            表名
	 * @param count
	 *            本次获取主键值个数
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Long getNextKey(String tablename, int count) {
		SysTablePk sysTablepk = new SysTablePk();
			sysTablepk = (SysTablePk)sessionFactory.getCurrentSession().get(SysTablePk.class, tablename);
		int returnstr;
		if (sysTablepk == null || sysTablepk.getTableName() == null) {
			sysTablepk = new SysTablePk();
			sysTablepk.setTableName(tablename);
			sysTablepk.setCurrentPk(String.valueOf(1000l));
			sessionFactory.getCurrentSession().save(sysTablepk);
			return 1000l;
		} else {
			long res = Integer.parseInt(sysTablepk.getCurrentPk()) + 1;
			returnstr = Integer.parseInt(sysTablepk.getCurrentPk()) + count;
			sysTablepk.setCurrentPk(String.valueOf(returnstr));
			sysTablepk.setTableName(tablename);
			sessionFactory.getCurrentSession().update(sysTablepk);
			return res;
		}
	}

	@Override
	public String getNextIndentCode(String baseCode,String prefixCode) {
//		String queryString = "select currentPk from SysTablePk where tableName = 'INDENT_CODE'";// and currentPk like '"+prefixCode+"%'
		SysTablePk sysTablepk = new SysTablePk();
		sysTablepk = (SysTablePk)sessionFactory.getCurrentSession().get(SysTablePk.class, "INDENT_CODE");
		if(sysTablepk==null || sysTablepk.getTableName() == null){//无数据时，新增
			sysTablepk = new SysTablePk();
			sysTablepk.setTableName("INDENT_CODE");
			sysTablepk.setCurrentPk(baseCode);
			sessionFactory.getCurrentSession().save(sysTablepk);
			return baseCode;
		}
		else{
			if(sysTablepk.getCurrentPk().startsWith(prefixCode)){//
				String code = sysTablepk.getCurrentPk();
				String str = baseCode.substring(0, 5)+String.format("%0"+6+"d", Integer.parseInt(code.substring(5, code.length()))+1);
				sysTablepk.setTableName("INDENT_CODE");
				sysTablepk.setCurrentPk(str);
				sessionFactory.getCurrentSession().update(sysTablepk);
				return str;
			}
			else{
				sysTablepk.setTableName("INDENT_CODE");
				sysTablepk.setCurrentPk(baseCode);
				sessionFactory.getCurrentSession().update(sysTablepk);
				return baseCode;
			}
		}
	}
	
	@Override
	public String getNextHotelIndentCode(String baseCode,String prefixCode) {
//		String queryString = "select currentPk from SysTablePk where tableName = 'INDENT_CODE'";// and currentPk like '"+prefixCode+"%'
		
		String[] letters = { "A", "B", "C", "D", "E", "F", "G",
				"H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
				"U", "V", "W", "X", "Y", "Z" };
		
		//根据参数创建当天第一个编号
		String newCode = "";
		newCode = baseCode.substring(baseCode.length()-4, baseCode.length());
		newCode = baseCode.substring(0, 5) + letters[0] + newCode;
		
		SysTablePk sysTablepk = new SysTablePk();
		sysTablepk = (SysTablePk)sessionFactory.getCurrentSession().get(SysTablePk.class, "HOTEL_INDENT_CODE");
		if(sysTablepk==null || sysTablepk.getTableName() == null){//无数据时，新增
			sysTablepk = new SysTablePk();
			sysTablepk.setTableName("HOTEL_INDENT_CODE");
			sysTablepk.setCurrentPk(newCode);
			sessionFactory.getCurrentSession().save(sysTablepk);
			return newCode;
		}
		else{
			if(sysTablepk.getCurrentPk().startsWith(prefixCode)){//
				String code = sysTablepk.getCurrentPk();
				
				int serial = Integer.parseInt(code.substring(code.length()-4, code.length()))+1;
				String serialStr = String.format("%0"+4+"d", serial);
				String middleSub = code.substring(code.length()-5, code.length()-4);
				
				if(serial==10000){
					serialStr = String.format("%0"+4+"d", 1);
					
					int lastI = -1;
					
					for(int i=0;i<letters.length;i++){
						if(lastI!=-1){
							middleSub = letters[i];
							break;
						}
						if(middleSub.equals(letters[i])){
							lastI = i;
						}
					}
					
				}
				
				String str = newCode.substring(0, 5)+middleSub+serialStr;
				sysTablepk.setTableName("HOTEL_INDENT_CODE");
				sysTablepk.setCurrentPk(str);
				//getHibernateTemplate().update(sysTablepk);
				return str;
			}
			else{
				sysTablepk.setTableName("HOTEL_INDENT_CODE");
				sysTablepk.setCurrentPk(newCode);
				//getHibernateTemplate().update(sysTablepk);
				return newCode;
			}
		}
	
	}
}
