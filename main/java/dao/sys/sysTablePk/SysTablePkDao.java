package dao.sys.sysTablePk;

import com.xlem.dao.SysTablePk;

import common.dao.BaseDao;

public  interface SysTablePkDao extends BaseDao<SysTablePk>{
	public  Long getNextKey(String tablename, int count);

	public String getNextIndentCode(String baseCode, String prefixCode);
	
	public String getNextHotelIndentCode(String baseCode, String prefixCode);
}
