package dao.sys.priv;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.Priv;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("privDao")
public class PrivDaoImpl extends BaseDaoImpl<Priv> implements PrivDao {

} 