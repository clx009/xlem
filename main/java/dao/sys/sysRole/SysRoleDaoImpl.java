package dao.sys.sysRole;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.SysRole;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("sysRoleDao")
public class SysRoleDaoImpl extends BaseDaoImpl<SysRole> implements SysRoleDao {

}