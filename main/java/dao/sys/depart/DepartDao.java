package dao.sys.depart;

import com.xlem.dao.Depart;

import common.dao.BaseDao;

public interface DepartDao extends BaseDao<Depart> {

}