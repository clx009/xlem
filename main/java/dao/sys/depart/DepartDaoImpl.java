package dao.sys.depart;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.Depart;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("departDao")
public class DepartDaoImpl extends BaseDaoImpl<Depart> implements DepartDao {

} 