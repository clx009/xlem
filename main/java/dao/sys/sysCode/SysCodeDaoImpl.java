package dao.sys.sysCode;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.SysCode;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("sysCodeDao")
public class SysCodeDaoImpl extends BaseDaoImpl<SysCode> implements SysCodeDao {

}