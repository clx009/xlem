package dao.sys.role;
import com.xlem.dao.Role;

import common.dao.BaseDao;

public interface RoleDao extends BaseDao<Role> {

}