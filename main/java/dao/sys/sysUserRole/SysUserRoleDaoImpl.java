package dao.sys.sysUserRole;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.SysUserRole;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("sysUserRoleDao")
public class SysUserRoleDaoImpl extends BaseDaoImpl<SysUserRole> implements SysUserRoleDao {

}