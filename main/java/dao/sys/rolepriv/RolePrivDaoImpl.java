package dao.sys.rolepriv;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.RolePriv;

import common.dao.BaseDaoImpl;

@Transactional
@Repository("role_privDao")
public class RolePrivDaoImpl extends BaseDaoImpl<RolePriv> implements RolePrivDao {

} 