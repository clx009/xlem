
package org.tempuri;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetVerResponse }
     * 
     */
    public GetVerResponse createGetVerResponse() {
        return new GetVerResponse();
    }

    /**
     * Create an instance of {@link GetResponse }
     * 
     */
    public GetResponse createGetResponse() {
        return new GetResponse();
    }

    /**
     * Create an instance of {@link GetVer }
     * 
     */
    public GetVer createGetVer() {
        return new GetVer();
    }

    /**
     * Create an instance of {@link ChargeUp }
     * 
     */
    public ChargeUp createChargeUp() {
        return new ChargeUp();
    }

    /**
     * Create an instance of {@link UpdPwdResponse }
     * 
     */
    public UpdPwdResponse createUpdPwdResponse() {
        return new UpdPwdResponse();
    }

    /**
     * Create an instance of {@link SendResponse }
     * 
     */
    public SendResponse createSendResponse() {
        return new SendResponse();
    }

    /**
     * Create an instance of {@link UpdPwd }
     * 
     */
    public UpdPwd createUpdPwd() {
        return new UpdPwd();
    }

    /**
     * Create an instance of {@link GetUpdateDataResponse.GetUpdateDataResult }
     * 
     */
    public GetUpdateDataResponse.GetUpdateDataResult createGetUpdateDataResponseGetUpdateDataResult() {
        return new GetUpdateDataResponse.GetUpdateDataResult();
    }

    /**
     * Create an instance of {@link RegResponse }
     * 
     */
    public RegResponse createRegResponse() {
        return new RegResponse();
    }

    /**
     * Create an instance of {@link Get }
     * 
     */
    public Get createGet() {
        return new Get();
    }

    /**
     * Create an instance of {@link Reg }
     * 
     */
    public Reg createReg() {
        return new Reg();
    }

    /**
     * Create an instance of {@link SelSumResponse }
     * 
     */
    public SelSumResponse createSelSumResponse() {
        return new SelSumResponse();
    }

    /**
     * Create an instance of {@link SelSum }
     * 
     */
    public SelSum createSelSum() {
        return new SelSum();
    }

    /**
     * Create an instance of {@link UnRegResponse }
     * 
     */
    public UnRegResponse createUnRegResponse() {
        return new UnRegResponse();
    }

    /**
     * Create an instance of {@link UnReg }
     * 
     */
    public UnReg createUnReg() {
        return new UnReg();
    }

    /**
     * Create an instance of {@link BatchSendResponse }
     * 
     */
    public BatchSendResponse createBatchSendResponse() {
        return new BatchSendResponse();
    }

    /**
     * Create an instance of {@link Send }
     * 
     */
    public Send createSend() {
        return new Send();
    }

    /**
     * Create an instance of {@link ShowMessageResponse }
     * 
     */
    public ShowMessageResponse createShowMessageResponse() {
        return new ShowMessageResponse();
    }

    /**
     * Create an instance of {@link GetUpdateData }
     * 
     */
    public GetUpdateData createGetUpdateData() {
        return new GetUpdateData();
    }

    /**
     * Create an instance of {@link GetUpdateDataResponse }
     * 
     */
    public GetUpdateDataResponse createGetUpdateDataResponse() {
        return new GetUpdateDataResponse();
    }

    /**
     * Create an instance of {@link UpdReg }
     * 
     */
    public UpdReg createUpdReg() {
        return new UpdReg();
    }

    /**
     * Create an instance of {@link UpdRegResponse }
     * 
     */
    public UpdRegResponse createUpdRegResponse() {
        return new UpdRegResponse();
    }

    /**
     * Create an instance of {@link ChargeUpResponse }
     * 
     */
    public ChargeUpResponse createChargeUpResponse() {
        return new ChargeUpResponse();
    }

    /**
     * Create an instance of {@link ShowMessage }
     * 
     */
    public ShowMessage createShowMessage() {
        return new ShowMessage();
    }

    /**
     * Create an instance of {@link BatchSend }
     * 
     */
    public BatchSend createBatchSend() {
        return new BatchSend();
    }

}
