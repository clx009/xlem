/**
 * 
 */
package org.tempuri;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * @author STKJ-12
 *
 */
public class SendSMSThread implements Runnable {

	private String Mobile = "";
	private String Content = "";
	private String send_time = "";
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		try {
			URL url = null;
			String CorpID="zxt00315";//账户名
			String Pwd="xlxsdx2012";//密码
			String send_content;
			send_content = URLEncoder.encode(Content.replaceAll("<br/>", " "), "GBK");
			url = new URL("http://sdk.cdzxkj.com/Send.aspx?CorpID="+CorpID+"&Pwd="+Pwd+"&Mobile="+Mobile+"&Content="+send_content+"&Cell=&SendTime="+send_time);
			BufferedReader in = null;
			int inputLine = 0;
			try {
				System.out.println("开始发送短信手机号码为 ："+Mobile);
				in = new BufferedReader(new InputStreamReader(url.openStream()));
				inputLine = new Integer(in.readLine()).intValue();
			} catch (Exception e) {
				System.out.println("网络异常,发送短信失败！");
				inputLine=-2;
			}
			System.out.println("结束发送短信返回值：  "+inputLine);
		}
		catch (Exception e) {
			System.out.println("发送短信出错，错误原因："+e.getMessage());
			e.printStackTrace();
		}
	}
	
	public String getMobile() {
		return Mobile;
	}
	public void setMobile(String mobile) {
		Mobile = mobile;
	}
	public String getContent() {
		return Content;
	}
	public void setContent(String content) {
		Content = content;
	}
	public String getSend_time() {
		return send_time;
	}
	public void setSend_time(String send_time) {
		this.send_time = send_time;
	}

}
