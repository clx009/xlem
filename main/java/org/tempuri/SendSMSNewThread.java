/**
 * 
 */
package org.tempuri;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import com.thoughtworks.xstream.XStream;

/**
 * @author STKJ-12
 *
 */
public class SendSMSNewThread implements Runnable {

	private String Mobile = "";
	private String Content = "";
	private String send_time = "";
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		try {
			URL url = null;
			String CorpID="105";//客户ID
			String corpCode = "scxlxs";//账号
			String Pwd="scxlxs";//密码
			String send_content;
			send_content = URLEncoder.encode(Content.replaceAll("<br/>", " "), "GBK");
			url = new URL("http://115.28.139.239:8888/smsGBK.aspx?action=send&userid="+CorpID+"&account="+corpCode+"&password="+Pwd+"&mobile="+Mobile+"&content="+send_content+"&extno=&sendTime="+send_time);
			BufferedReader in = null;
			int inputLine = 0;
			try {
				System.out.println("开始发送短信手机号码为 ："+Mobile);
				in = new BufferedReader(new InputStreamReader(url.openStream()));
				inputLine = 1;
			} catch (Exception e) {
				System.out.println("网络异常,发送短信失败！");
				inputLine=-2;
			}
			System.out.println("结束发送短信返回值：  "+inputLine);
		}
		catch (Exception e) {
			System.out.println("发送短信出错，错误原因："+e.getMessage());
			e.printStackTrace();
		}
	}
	
	public String getMobile() {
		return Mobile;
	}
	public void setMobile(String mobile) {
		Mobile = mobile;
	}
	public String getContent() {
		return Content;
	}
	public void setContent(String content) {
		Content = content;
	}
	public String getSend_time() {
		return send_time;
	}
	public void setSend_time(String send_time) {
		this.send_time = send_time;
	}

}
