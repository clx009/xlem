package com.xlem.dao;
// Generated 2016-8-5 17:11:30 by Hibernate Tools 4.0.0.Final

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

/**
 * Home object for domain model class BaseTourist.
 * @see com.xlem.dao.BaseTourist
 * @author Hibernate Tools
 */
public class BaseTouristHome {

	private static final Log log = LogFactory.getLog(BaseTouristHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(BaseTourist transientInstance) {
		log.debug("persisting BaseTourist instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(BaseTourist instance) {
		log.debug("attaching dirty BaseTourist instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(BaseTourist instance) {
		log.debug("attaching clean BaseTourist instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(BaseTourist persistentInstance) {
		log.debug("deleting BaseTourist instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public BaseTourist merge(BaseTourist detachedInstance) {
		log.debug("merging BaseTourist instance");
		try {
			BaseTourist result = (BaseTourist) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public BaseTourist findById(java.lang.Long id) {
		log.debug("getting BaseTourist instance with id: " + id);
		try {
			BaseTourist instance = (BaseTourist) sessionFactory.getCurrentSession().get("com.xlem.dao.BaseTourist", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(BaseTourist instance) {
		log.debug("finding BaseTourist instance by example");
		try {
			List results = sessionFactory.getCurrentSession().createCriteria("com.xlem.dao.BaseTourist")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
