package com.xlem.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Transient;


/**
 * SysFunction entity. @author MyEclipse Persistence Tools
 */
public class SysFunctionNew extends SysFunction implements
		java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<SysFunctionNew> children = new ArrayList<SysFunctionNew>();
	@Transient
	public List<SysFunctionNew> getChildren() {
		return children;
	}

	public void setChildren(List<SysFunctionNew> children) {
		this.children = children;
	}
	@Transient
	public String getState(){
		if(getIsLast().equals("11")){
			return "open";
		}else{
			return "closed";
		}
	}
}
