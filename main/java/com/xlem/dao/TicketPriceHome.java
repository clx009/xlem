package com.xlem.dao;
// Generated 2016-8-5 16:11:02 by Hibernate Tools 4.0.0.Final

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

/**
 * Home object for domain model class TicketPrice.
 * @see com.xlem.dao.TicketPrice
 * @author Hibernate Tools
 */
public class TicketPriceHome {

	private static final Log log = LogFactory.getLog(TicketPriceHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(TicketPrice transientInstance) {
		log.debug("persisting TicketPrice instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(TicketPrice instance) {
		log.debug("attaching dirty TicketPrice instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(TicketPrice instance) {
		log.debug("attaching clean TicketPrice instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(TicketPrice persistentInstance) {
		log.debug("deleting TicketPrice instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public TicketPrice merge(TicketPrice detachedInstance) {
		log.debug("merging TicketPrice instance");
		try {
			TicketPrice result = (TicketPrice) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public TicketPrice findById(java.lang.Long id) {
		log.debug("getting TicketPrice instance with id: " + id);
		try {
			TicketPrice instance = (TicketPrice) sessionFactory.getCurrentSession().get("com.xlem.dao.TicketPrice", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(TicketPrice instance) {
		log.debug("finding TicketPrice instance by example");
		try {
			List results = sessionFactory.getCurrentSession().createCriteria("com.xlem.dao.TicketPrice")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
