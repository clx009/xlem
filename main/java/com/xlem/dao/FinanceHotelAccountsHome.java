package com.xlem.dao;
// Generated 2016-8-3 14:45:38 by Hibernate Tools 4.0.0.Final

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

/**
 * Home object for domain model class FinanceHotelAccounts.
 * @see com.xlem.dao.FinanceHotelAccounts
 * @author Hibernate Tools
 */
public class FinanceHotelAccountsHome {

	private static final Log log = LogFactory.getLog(FinanceHotelAccountsHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(FinanceHotelAccounts transientInstance) {
		log.debug("persisting FinanceHotelAccounts instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(FinanceHotelAccounts instance) {
		log.debug("attaching dirty FinanceHotelAccounts instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(FinanceHotelAccounts instance) {
		log.debug("attaching clean FinanceHotelAccounts instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(FinanceHotelAccounts persistentInstance) {
		log.debug("deleting FinanceHotelAccounts instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public FinanceHotelAccounts merge(FinanceHotelAccounts detachedInstance) {
		log.debug("merging FinanceHotelAccounts instance");
		try {
			FinanceHotelAccounts result = (FinanceHotelAccounts) sessionFactory.getCurrentSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public FinanceHotelAccounts findById(java.lang.Integer id) {
		log.debug("getting FinanceHotelAccounts instance with id: " + id);
		try {
			FinanceHotelAccounts instance = (FinanceHotelAccounts) sessionFactory.getCurrentSession()
					.get("com.xlem.dao.FinanceHotelAccounts", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(FinanceHotelAccounts instance) {
		log.debug("finding FinanceHotelAccounts instance by example");
		try {
			List results = sessionFactory.getCurrentSession().createCriteria("com.xlem.dao.FinanceHotelAccounts")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
