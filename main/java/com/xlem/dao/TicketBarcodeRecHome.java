package com.xlem.dao;
// Generated 2016-8-5 16:11:02 by Hibernate Tools 4.0.0.Final

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

/**
 * Home object for domain model class TicketBarcodeRec.
 * @see com.xlem.dao.TicketBarcodeRec
 * @author Hibernate Tools
 */
public class TicketBarcodeRecHome {

	private static final Log log = LogFactory.getLog(TicketBarcodeRecHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(TicketBarcodeRec transientInstance) {
		log.debug("persisting TicketBarcodeRec instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(TicketBarcodeRec instance) {
		log.debug("attaching dirty TicketBarcodeRec instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(TicketBarcodeRec instance) {
		log.debug("attaching clean TicketBarcodeRec instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(TicketBarcodeRec persistentInstance) {
		log.debug("deleting TicketBarcodeRec instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public TicketBarcodeRec merge(TicketBarcodeRec detachedInstance) {
		log.debug("merging TicketBarcodeRec instance");
		try {
			TicketBarcodeRec result = (TicketBarcodeRec) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public TicketBarcodeRec findById(java.lang.String id) {
		log.debug("getting TicketBarcodeRec instance with id: " + id);
		try {
			TicketBarcodeRec instance = (TicketBarcodeRec) sessionFactory.getCurrentSession()
					.get("com.xlem.dao.TicketBarcodeRec", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(TicketBarcodeRec instance) {
		log.debug("finding TicketBarcodeRec instance by example");
		try {
			List results = sessionFactory.getCurrentSession().createCriteria("com.xlem.dao.TicketBarcodeRec")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
