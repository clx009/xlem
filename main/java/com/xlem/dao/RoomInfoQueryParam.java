package com.xlem.dao;

import java.sql.Timestamp;
import java.util.List;

public class RoomInfoQueryParam {
	
	private List<HotelRoomType> hotelRoomTypes;
	private Timestamp startDate;
	private Timestamp endDate;
	private int row;
	
	public List<HotelRoomType> getHotelRoomTypes() {
		return hotelRoomTypes;
	}
	public void setHotelRoomTypes(List<HotelRoomType> hotelRoomTypes) {
		this.hotelRoomTypes = hotelRoomTypes;
	}
	public Timestamp getStartDate() {
		return startDate;
	}
	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}
	public Timestamp getEndDate() {
		return endDate;
	}
	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}
	public int getRow() {
		return row;
	}
	public void setRow(int row) {
		this.row = row;
	}
}
