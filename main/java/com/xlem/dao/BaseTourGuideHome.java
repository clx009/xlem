package com.xlem.dao;
// Generated 2016-8-7 16:35:12 by Hibernate Tools 4.0.0.Final

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

/**
 * Home object for domain model class BaseTourGuide.
 * @see com.xlem.dao.BaseTourGuide
 * @author Hibernate Tools
 */
public class BaseTourGuideHome {

	private static final Log log = LogFactory.getLog(BaseTourGuideHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(BaseTourGuide transientInstance) {
		log.debug("persisting BaseTourGuide instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(BaseTourGuide instance) {
		log.debug("attaching dirty BaseTourGuide instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(BaseTourGuide instance) {
		log.debug("attaching clean BaseTourGuide instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(BaseTourGuide persistentInstance) {
		log.debug("deleting BaseTourGuide instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public BaseTourGuide merge(BaseTourGuide detachedInstance) {
		log.debug("merging BaseTourGuide instance");
		try {
			BaseTourGuide result = (BaseTourGuide) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public BaseTourGuide findById(java.lang.Long id) {
		log.debug("getting BaseTourGuide instance with id: " + id);
		try {
			BaseTourGuide instance = (BaseTourGuide) sessionFactory.getCurrentSession()
					.get("com.xlem.dao.BaseTourGuide", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(BaseTourGuide instance) {
		log.debug("finding BaseTourGuide instance by example");
		try {
			List results = sessionFactory.getCurrentSession().createCriteria("com.xlem.dao.BaseTourGuide")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
