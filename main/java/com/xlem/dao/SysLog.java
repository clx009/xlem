package com.xlem.dao;
// Generated 2016-10-8 17:18:38 by Hibernate Tools 3.6.0.Final

import java.util.Date;

/**
 * SysLog generated by hbm2java
 */
public class SysLog implements java.io.Serializable {

	private Long logId;
	private String opType;
	private String content;
	private Long inputer;
	private Date inputTime;

	public SysLog() {
	}

	public SysLog(String opType, String content, Long inputer, Date inputTime) {
		this.opType = opType;
		this.content = content;
		this.inputer = inputer;
		this.inputTime = inputTime;
	}

	public Long getLogId() {
		return this.logId;
	}

	public void setLogId(Long logId) {
		this.logId = logId;
	}

	public String getOpType() {
		return this.opType;
	}

	public void setOpType(String opType) {
		this.opType = opType;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getInputer() {
		return this.inputer;
	}

	public void setInputer(Long inputer) {
		this.inputer = inputer;
	}

	public Date getInputTime() {
		return this.inputTime;
	}

	public void setInputTime(Date inputTime) {
		this.inputTime = inputTime;
	}

}
