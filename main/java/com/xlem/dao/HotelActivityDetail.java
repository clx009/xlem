package com.xlem.dao;
// Generated 2016-8-4 17:23:06 by Hibernate Tools 4.0.0.Final

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Transient;

import common.CodeListener;


/**
 * HotelActivityDetail generated by hbm2java
 */
public class HotelActivityDetail implements java.io.Serializable {

	private Long hotelActivityDetailId;
    private HotelActivity hotelActivity;
    private HotelRoomType hotelRoomType;
	private String customerType;
	private Integer peopleNum;
	private Integer minDays;
	private Integer maxDays;
	private int indentMinAmount;
	private int indentMaxAmount;
	private int userMaxAmount;
	private Double roomPrice;
	private Double peoplePrice;
	private String status;
	private long inputer;
	private Date inputTime;
	private Long updater;
	private Date updateTime;

	public HotelActivityDetail() {
	}

	public HotelActivityDetail(HotelActivity hotelActivity, HotelRoomType hotelRoomType, String customerType, Integer peopleNum, Integer minDays,
			Integer maxDays, int indentMinAmount, int indentMaxAmount, int userMaxAmount, double roomPrice,
			Double peoplePrice, String status, long inputer, Date inputTime) {
		this.hotelActivity = hotelActivity;
		this.hotelRoomType = hotelRoomType;
		this.customerType = customerType;
		this.peopleNum = peopleNum;
		this.minDays = minDays;
		this.maxDays = maxDays;
		this.indentMinAmount = indentMinAmount;
		this.indentMaxAmount = indentMaxAmount;
		this.userMaxAmount = userMaxAmount;
		this.roomPrice = roomPrice;
		this.peoplePrice = peoplePrice;
		this.status = status;
		this.inputer = inputer;
		this.inputTime = inputTime;
	}

	public HotelActivityDetail(HotelActivity hotelActivity, HotelRoomType hotelRoomType, String customerType, Integer peopleNum, Integer minDays,
			Integer maxDays, int indentMinAmount, int indentMaxAmount, int userMaxAmount, double roomPrice,
			double peoplePrice, String status, long inputer, Date inputTime, Long updater, Date updateTime) {
		this.hotelActivity = hotelActivity;
		this.hotelRoomType = hotelRoomType;
		this.customerType = customerType;
		this.peopleNum = peopleNum;
		this.minDays = minDays;
		this.maxDays = maxDays;
		this.indentMinAmount = indentMinAmount;
		this.indentMaxAmount = indentMaxAmount;
		this.userMaxAmount = userMaxAmount;
		this.roomPrice = roomPrice;
		this.peoplePrice = peoplePrice;
		this.status = status;
		this.inputer = inputer;
		this.inputTime = inputTime;
		this.updater = updater;
		this.updateTime = updateTime;
	}

	public Long getHotelActivityDetailId() {
		return this.hotelActivityDetailId;
	}

	public void setHotelActivityDetailId(Long hotelActivityDetailId) {
		this.hotelActivityDetailId = hotelActivityDetailId;
	}
	
    public HotelActivity getHotelActivity() {
        return this.hotelActivity;
    }
    
    public void setHotelActivity(HotelActivity hotelActivity) {
        this.hotelActivity = hotelActivity;
    }
    
    public HotelRoomType getHotelRoomType() {
        return this.hotelRoomType;
    }
    
    public void setHotelRoomType(HotelRoomType hotelRoomType) {
        this.hotelRoomType = hotelRoomType;
    }
    
	public String getCustomerType() {
		return this.customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public Integer getPeopleNum() {
		return this.peopleNum;
	}

	public void setPeopleNum(Integer peopleNum) {
		this.peopleNum = peopleNum;
	}

	public Integer getMinDays() {
		return this.minDays;
	}

	public void setMinDays(Integer minDays) {
		this.minDays = minDays;
	}

	public Integer getMaxDays() {
		return this.maxDays;
	}

	public void setMaxDays(Integer maxDays) {
		this.maxDays = maxDays;
	}

	public int getIndentMinAmount() {
		return this.indentMinAmount;
	}

	public void setIndentMinAmount(int indentMinAmount) {
		this.indentMinAmount = indentMinAmount;
	}

	public int getIndentMaxAmount() {
		return this.indentMaxAmount;
	}

	public void setIndentMaxAmount(int indentMaxAmount) {
		this.indentMaxAmount = indentMaxAmount;
	}

	public int getUserMaxAmount() {
		return this.userMaxAmount;
	}

	public void setUserMaxAmount(int userMaxAmount) {
		this.userMaxAmount = userMaxAmount;
	}

	public Double getRoomPrice() {
		return this.roomPrice;
	}

	public void setRoomPrice(Double roomPrice) {
		this.roomPrice = roomPrice;
	}

	public Double getPeoplePrice() {
		return this.peoplePrice;
	}

	public void setPeoplePrice(Double peoplePrice) {
		this.peoplePrice = peoplePrice;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getInputer() {
		return this.inputer;
	}

	public void setInputer(long inputer) {
		this.inputer = inputer;
	}

	public Date getInputTime() {
		return this.inputTime;
	}

	public void setInputTime(Date inputTime) {
		this.inputTime = inputTime;
	}

	public Long getUpdater() {
		return this.updater;
	}

	public void setUpdater(Long updater) {
		this.updater = updater;
	}

	public Date getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
	private boolean isNew;

    @Transient
	public String getRoomTypeShowName() {
    	String reString = "";
    	if(this.getHotelRoomType()!=null){
    		reString = this.getHotelRoomType().getSelectShowName();
    	}
		return reString;
	}
    
    @Transient
	public String getCustomerTypeText() {
		return CodeListener.getNameByCode("HOTEL_INDENT", "CUSTOMER_TYPE",
				this.getCustomerType());
	}
    
	@Transient
	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}
		

}
