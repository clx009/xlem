package com.xlem.dao;
// Generated 2016-8-5 16:11:02 by Hibernate Tools 4.0.0.Final

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

/**
 * Home object for domain model class TicketThirdIndent.
 * @see com.xlem.dao.TicketThirdIndent
 * @author Hibernate Tools
 */
public class TicketThirdIndentHome {

	private static final Log log = LogFactory.getLog(TicketThirdIndentHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(TicketThirdIndent transientInstance) {
		log.debug("persisting TicketThirdIndent instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(TicketThirdIndent instance) {
		log.debug("attaching dirty TicketThirdIndent instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(TicketThirdIndent instance) {
		log.debug("attaching clean TicketThirdIndent instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(TicketThirdIndent persistentInstance) {
		log.debug("deleting TicketThirdIndent instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public TicketThirdIndent merge(TicketThirdIndent detachedInstance) {
		log.debug("merging TicketThirdIndent instance");
		try {
			TicketThirdIndent result = (TicketThirdIndent) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public TicketThirdIndent findById(java.lang.String id) {
		log.debug("getting TicketThirdIndent instance with id: " + id);
		try {
			TicketThirdIndent instance = (TicketThirdIndent) sessionFactory.getCurrentSession()
					.get("com.xlem.dao.TicketThirdIndent", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(TicketThirdIndent instance) {
		log.debug("finding TicketThirdIndent instance by example");
		try {
			List results = sessionFactory.getCurrentSession().createCriteria("com.xlem.dao.TicketThirdIndent")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
