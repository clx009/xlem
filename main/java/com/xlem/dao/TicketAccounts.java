package com.xlem.dao;

import common.CodeListener;


/**
 * 结账界面专用类
 */
public class TicketAccounts implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Constructors

	private String ticketName;
	
	private Integer amountGroup;
	private Double subtotalGroup;
	private Double payFeeGroup;
	private Double subtotalGroupE;
	private Double payFeeGroupE;
	private Double subtotalGroupR;
	private Double payFeeGroupR;
	
	private Integer amountGroupD;
	private Double subtotalGroupD;
	private Double payFeeGroupD;
	private Double subtotalGroupDE;
	private Double payFeeGroupDE;
	private Double subtotalGroupDR;
	private Double payFeeGroupDR;
	
	private Integer amount;
	private Double subtotal;
	private Double payFee;
	private Double subtotalE;
	private Double payFeeE;
	private Double subtotalR;
	private Double payFeeR;
	
	private Integer amountLocale;
	private Double subtotalLocale;
	private Double payFeeLocale;
	private Double subtotalLocaleE;
	private Double payFeeLocaleE;
	private Double subtotalLocaleR;
	private Double payFeeLocaleR;
	
	private String discount;
	private String status;
	
	private String eCompany;
	private String rCompany;
	
	/** default constructor */
	public TicketAccounts() {
	}
	
	/**
	 * 
	 * @param t
	 * @param type "E","R"
	 * @param cName
	 */
	public TicketAccounts(TicketAccounts t,String type,String cName, boolean setAmount) {
		
		this.discount = t.getDiscount();
		this.status = t.getStatus();
		
		if("E".equals(type)){
			TicketAccountsE(t,cName);
		}else{
			TicketAccountsR(t,cName);
		}
		
		if(setAmount){
			this.amount = t.getAmount();
			this.amountGroup = t.getAmountGroup();
			this.amountGroupD = t.getAmountGroupD();
			this.amountLocale = t.getAmountLocale();
		}
		
	}

	private void TicketAccountsE(TicketAccounts t,String cName) {
		
		this.ticketName = crTName(t.getTicketName(),cName,"E");
		
		this.subtotalGroup = t.getSubtotalGroupE();
		this.payFeeGroup = t.getPayFeeGroupE();
		
		this.subtotalGroupD = t.getSubtotalGroupDE();
		this.payFeeGroupD = t.getPayFeeGroupDE();
		
		this.subtotal = t.getSubtotalE();
		this.payFee = t.getPayFeeE();
		
		this.subtotalLocale = t.getSubtotalLocaleE();
		this.payFeeLocale = t.getPayFeeLocaleE();
		
	}
	
	private void TicketAccountsR(TicketAccounts t,String cName) {
		
		this.ticketName = crTName(t.getTicketName(),cName,"R");
		
		this.subtotalGroup = t.getSubtotalGroupR();
		this.payFeeGroup = t.getPayFeeGroupR();
		
		this.subtotalGroupD = t.getSubtotalGroupDR();
		this.payFeeGroupD = t.getPayFeeGroupDR();
		
		this.subtotal = t.getSubtotalR();
		this.payFee = t.getPayFeeR();
		
		this.subtotalLocale = t.getSubtotalLocaleR();
		this.payFeeLocale = t.getPayFeeLocaleR();
		
	}
	
	private String crTName(String tName,String cName,String type){
		
		String reName = "";
		
		String nameR = "";
		if(tName.indexOf("（")>-1){
			nameR = tName.substring(tName.indexOf("（"));
		}else if(tName.indexOf("[")>-1){
			nameR = tName.substring(tName.indexOf("["));
		}
		
		if(tName.indexOf("+")>-1){
			String[] tNames = tName.split("\\+");
			if("E".equals(type)){
				String eName = tNames[0];
				if(eName.indexOf("（")<0 && eName.indexOf("[")<0){
					eName = eName+nameR;
				}
				reName = eName+"("+cName+")";
			}else{
				int tL = tNames.length;
				if(tL>1){
					String eName = tNames[1];
					if(eName.indexOf("（")<0 && eName.indexOf("[")<0){
						eName = eName+nameR;
					}
					reName = eName+"("+cName+")";
				}
			}
		}else{
			reName = tName+"("+cName+")";
		}
		
		
		
		return reName;
	}
	
	public String getTicketName() {
		return ticketName;
	}

	public void setTicketName(String ticketName) {
		this.ticketName = ticketName;
	}

	public Integer getAmountGroup() {
		return amountGroup;
	}

	public void setAmountGroup(Integer amountGroup) {
		this.amountGroup = amountGroup;
	}

	public Double getSubtotalGroup() {
		return subtotalGroup;
	}

	public void setSubtotalGroup(Double subtotalGroup) {
		this.subtotalGroup = subtotalGroup;
	}

	public Double getPayFeeGroup() {
		return payFeeGroup;
	}

	public void setPayFeeGroup(Double payFeeGroup) {
		this.payFeeGroup = payFeeGroup;
	}

	public Integer getAmountGroupD() {
		return amountGroupD;
	}

	public void setAmountGroupD(Integer amountGroupD) {
		this.amountGroupD = amountGroupD;
	}

	public Double getSubtotalGroupD() {
		return subtotalGroupD;
	}

	public void setSubtotalGroupD(Double subtotalGroupD) {
		this.subtotalGroupD = subtotalGroupD;
	}

	public Double getPayFeeGroupD() {
		return payFeeGroupD;
	}

	public void setPayFeeGroupD(Double payFeeGroupD) {
		this.payFeeGroupD = payFeeGroupD;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(Double subtotal) {
		this.subtotal = subtotal;
	}

	public Double getPayFee() {
		return payFee;
	}

	public void setPayFee(Double payFee) {
		this.payFee = payFee;
	}

	public Integer getAmountLocale() {
		return amountLocale;
	}

	public void setAmountLocale(Integer amountLocale) {
		this.amountLocale = amountLocale;
	}

	public Double getSubtotalLocale() {
		return subtotalLocale;
	}

	public void setSubtotalLocale(Double subtotalLocale) {
		this.subtotalLocale = subtotalLocale;
	}

	public Double getPayFeeLocale() {
		return payFeeLocale;
	}

	public void setPayFeeLocale(Double payFeeLocale) {
		this.payFeeLocale = payFeeLocale;
	}

	public String getDiscount() {
		return this.discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getDiscountName() {
		return CodeListener.getNameByCode("TICKET_INDENT", "DISCOUNT", getDiscount());
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusName() {
		return CodeListener.getNameByCode("TICKET_BARCODE_REC", "status", getStatus());
	}

	public Double getSubtotalGroupE() {
		return subtotalGroupE;
	}

	public void setSubtotalGroupE(Double subtotalGroupE) {
		this.subtotalGroupE = subtotalGroupE;
	}

	public Double getPayFeeGroupE() {
		return payFeeGroupE;
	}

	public void setPayFeeGroupE(Double payFeeGroupE) {
		this.payFeeGroupE = payFeeGroupE;
	}

	public Double getSubtotalGroupR() {
		return subtotalGroupR;
	}

	public void setSubtotalGroupR(Double subtotalGroupR) {
		this.subtotalGroupR = subtotalGroupR;
	}

	public Double getPayFeeGroupR() {
		return payFeeGroupR;
	}

	public void setPayFeeGroupR(Double payFeeGroupR) {
		this.payFeeGroupR = payFeeGroupR;
	}

	public Double getSubtotalGroupDE() {
		return subtotalGroupDE;
	}

	public void setSubtotalGroupDE(Double subtotalGroupDE) {
		this.subtotalGroupDE = subtotalGroupDE;
	}

	public Double getPayFeeGroupDE() {
		return payFeeGroupDE;
	}

	public void setPayFeeGroupDE(Double payFeeGroupDE) {
		this.payFeeGroupDE = payFeeGroupDE;
	}

	public Double getSubtotalGroupDR() {
		return subtotalGroupDR;
	}

	public void setSubtotalGroupDR(Double subtotalGroupDR) {
		this.subtotalGroupDR = subtotalGroupDR;
	}

	public Double getPayFeeGroupDR() {
		return payFeeGroupDR;
	}

	public void setPayFeeGroupDR(Double payFeeGroupDR) {
		this.payFeeGroupDR = payFeeGroupDR;
	}

	public Double getSubtotalE() {
		return subtotalE;
	}

	public void setSubtotalE(Double subtotalE) {
		this.subtotalE = subtotalE;
	}

	public Double getPayFeeE() {
		return payFeeE;
	}

	public void setPayFeeE(Double payFeeE) {
		this.payFeeE = payFeeE;
	}

	public Double getSubtotalR() {
		return subtotalR;
	}

	public void setSubtotalR(Double subtotalR) {
		this.subtotalR = subtotalR;
	}

	public Double getPayFeeR() {
		return payFeeR;
	}

	public void setPayFeeR(Double payFeeR) {
		this.payFeeR = payFeeR;
	}

	public Double getSubtotalLocaleE() {
		return subtotalLocaleE;
	}

	public void setSubtotalLocaleE(Double subtotalLocaleE) {
		this.subtotalLocaleE = subtotalLocaleE;
	}

	public Double getPayFeeLocaleE() {
		return payFeeLocaleE;
	}

	public void setPayFeeLocaleE(Double payFeeLocaleE) {
		this.payFeeLocaleE = payFeeLocaleE;
	}

	public Double getSubtotalLocaleR() {
		return subtotalLocaleR;
	}

	public void setSubtotalLocaleR(Double subtotalLocaleR) {
		this.subtotalLocaleR = subtotalLocaleR;
	}

	public Double getPayFeeLocaleR() {
		return payFeeLocaleR;
	}

	public void setPayFeeLocaleR(Double payFeeLocaleR) {
		this.payFeeLocaleR = payFeeLocaleR;
	}

	public String geteCompany() {
		return eCompany;
	}

	public void seteCompany(String eCompany) {
		this.eCompany = eCompany;
	}

	public String getrCompany() {
		return rCompany;
	}

	public void setrCompany(String rCompany) {
		this.rCompany = rCompany;
	}
}
