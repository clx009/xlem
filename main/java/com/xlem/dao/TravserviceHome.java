package com.xlem.dao;
// Generated 2016-8-6 14:33:28 by Hibernate Tools 4.0.0.Final

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;


import common.action.JsonPager;
import common.action.PaginationSupport;
import common.dao.BaseDao;
import common.dao.BaseDaoImpl;
import dao.sys.sysUser.SysUserDao;

/**
 * Home object for domain model class Travservice.
 * @see com.xlem.dao.Travservice
 * @author Hibernate Tools
 */
public class TravserviceHome {
	@Autowired
	private SysUserDao sysUserDao;

	private static final Log log = LogFactory.getLog(TravserviceHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	public SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(Travservice transientInstance) {
		log.debug("persisting Travservice instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Travservice instance) {
		log.debug("attaching dirty Travservice instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Travservice instance) {
		log.debug("attaching clean Travservice instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Travservice persistentInstance) {
		log.debug("deleting Travservice instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Travservice merge(Travservice detachedInstance) {
		log.debug("merging Travservice instance");
		try {
			Travservice result = (Travservice) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Travservice findById(long id) {
		log.debug("getting Travservice instance with id: " + id);
		try {
			Travservice instance = (Travservice) sessionFactory.getCurrentSession().get("com.xlem.dao.Travservice", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Travservice instance) {
		log.debug("finding Travservice instance by example");
		try {
			List results = sessionFactory.getCurrentSession().createCriteria("com.xlem.dao.Travservice")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public void update(Travservice baseTravelAgency) {
		// TODO Auto-generated method stub
		log.debug("merging Travservice instance");
		try {
			sessionFactory.getCurrentSession().update(baseTravelAgency);
			log.debug("merge successful");
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}		
	}
	
	public Travservice findTravelAgencyByUser(long userId) {
		DetachedCriteria dc = DetachedCriteria.forClass(SysUser.class);
		dc.createAlias("baseTravelAgency", "baseTravelAgency");
		dc.add(Property.forName("userId").eq(userId));
		dc.add(Property.forName("baseTravelAgency.travelAgencyId").isNotNull());
		List<SysUser> list = sysUserDao.findByCriteria(dc);
		if(list.isEmpty()){
			return new Travservice();
		}else{
			SysUser sysUser = list.get(0);
			Travservice baseTravelAgency = sysUser.getBaseTravelAgency();
			// baseTravelAgency.setMailbox(sysUser.getMailbox());
			// baseTravelAgency.setUserCode(sysUser.getUserCode());
			return baseTravelAgency;
		}
	}

	public PaginationSupport<Travservice> findPageByCriteria(PaginationSupport<Travservice> ps, Order desc,
			DetachedCriteria dc) {
		// TODO Auto-generated method stub
		return null;
	}

	public void save(Travservice t) {
		// TODO Auto-generated method stub
		log.debug("persisting Travservice instance");
		try {
			sessionFactory.getCurrentSession().save(t);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}		
	}
	
	public synchronized String getNextUserCode() {
		SysTablePk sysTablepk = new SysTablePk();
		sysTablepk = (SysTablePk)sessionFactory.getCurrentSession().get(SysTablePk.class, "USER_CODE");
		String baseCode = "lxszh00001";
		if(sysTablepk==null || sysTablepk.getTableName() == null){//无数据时，新增
			sysTablepk = new SysTablePk();
			sysTablepk.setTableName("USER_CODE");
			sysTablepk.setCurrentPk(baseCode);
			sessionFactory.getCurrentSession().save(sysTablepk);
			return baseCode;
		}else{
				String code = sysTablepk.getCurrentPk();
				String str = "lxszh"+String.format("%0"+5+"d", Integer.parseInt(code.substring(5, code.length()))+1);
				sysTablepk.setTableName("USER_CODE");
				sysTablepk.setCurrentPk(str);
				sessionFactory.getCurrentSession().update(sysTablepk);
				return str;
		}
	}	
}
