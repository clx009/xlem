package com.xlem.dao;
// Generated 2016-8-7 16:17:40 by Hibernate Tools 4.0.0.Final

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

/**
 * Home object for domain model class CommReserve.
 * @see com.xlem.dao.CommReserve
 * @author Hibernate Tools
 */
public class CommReserveHome {

	private static final Log log = LogFactory.getLog(CommReserveHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(CommReserve transientInstance) {
		log.debug("persisting CommReserve instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(CommReserve instance) {
		log.debug("attaching dirty CommReserve instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(CommReserve instance) {
		log.debug("attaching clean CommReserve instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(CommReserve persistentInstance) {
		log.debug("deleting CommReserve instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public CommReserve merge(CommReserve detachedInstance) {
		log.debug("merging CommReserve instance");
		try {
			CommReserve result = (CommReserve) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public CommReserve findById(java.lang.Long id) {
		log.debug("getting CommReserve instance with id: " + id);
		try {
			CommReserve instance = (CommReserve) sessionFactory.getCurrentSession().get("com.xlem.dao.CommReserve", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(CommReserve instance) {
		log.debug("finding CommReserve instance by example");
		try {
			List results = sessionFactory.getCurrentSession().createCriteria("com.xlem.dao.CommReserve")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
