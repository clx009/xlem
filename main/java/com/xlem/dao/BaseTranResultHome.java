package com.xlem.dao;
// Generated 2016-8-6 15:51:12 by Hibernate Tools 4.0.0.Final

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

/**
 * Home object for domain model class BaseTranResult.
 * @see com.xlem.dao.BaseTranResult
 * @author Hibernate Tools
 */
public class BaseTranResultHome {

	private static final Log log = LogFactory.getLog(BaseTranResultHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(BaseTranResult transientInstance) {
		log.debug("persisting BaseTranResult instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(BaseTranResult instance) {
		log.debug("attaching dirty BaseTranResult instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(BaseTranResult instance) {
		log.debug("attaching clean BaseTranResult instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(BaseTranResult persistentInstance) {
		log.debug("deleting BaseTranResult instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public BaseTranResult merge(BaseTranResult detachedInstance) {
		log.debug("merging BaseTranResult instance");
		try {
			BaseTranResult result = (BaseTranResult) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public BaseTranResult findById(java.lang.Long id) {
		log.debug("getting BaseTranResult instance with id: " + id);
		try {
			BaseTranResult instance = (BaseTranResult) sessionFactory.getCurrentSession()
					.get("com.xlem.dao.BaseTranResult", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(BaseTranResult instance) {
		log.debug("finding BaseTranResult instance by example");
		try {
			List results = sessionFactory.getCurrentSession().createCriteria("com.xlem.dao.BaseTranResult")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
