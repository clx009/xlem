package com.xlem.dao;
// Generated 2016-8-5 16:33:49 by Hibernate Tools 4.0.0.Final

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

/**
 * Home object for domain model class SysRoleFunction.
 * @see com.xlem.dao.SysRoleFunction
 * @author Hibernate Tools
 */
public class SysRoleFunctionHome {

	private static final Log log = LogFactory.getLog(SysRoleFunctionHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(SysRoleFunction transientInstance) {
		log.debug("persisting SysRoleFunction instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(SysRoleFunction instance) {
		log.debug("attaching dirty SysRoleFunction instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(SysRoleFunction instance) {
		log.debug("attaching clean SysRoleFunction instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(SysRoleFunction persistentInstance) {
		log.debug("deleting SysRoleFunction instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public SysRoleFunction merge(SysRoleFunction detachedInstance) {
		log.debug("merging SysRoleFunction instance");
		try {
			SysRoleFunction result = (SysRoleFunction) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public SysRoleFunction findById(java.lang.Long id) {
		log.debug("getting SysRoleFunction instance with id: " + id);
		try {
			SysRoleFunction instance = (SysRoleFunction) sessionFactory.getCurrentSession()
					.get("com.xlem.dao.SysRoleFunction", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(SysRoleFunction instance) {
		log.debug("finding SysRoleFunction instance by example");
		try {
			List results = sessionFactory.getCurrentSession().createCriteria("com.xlem.dao.SysRoleFunction")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
