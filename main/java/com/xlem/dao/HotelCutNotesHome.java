package com.xlem.dao;
// Generated 2016-8-3 14:45:38 by Hibernate Tools 4.0.0.Final

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

/**
 * Home object for domain model class HotelCutNotes.
 * @see com.xlem.dao.HotelCutNotes
 * @author Hibernate Tools
 */
public class HotelCutNotesHome {

	private static final Log log = LogFactory.getLog(HotelCutNotesHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(HotelCutNotes transientInstance) {
		log.debug("persisting HotelCutNotes instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(HotelCutNotes instance) {
		log.debug("attaching dirty HotelCutNotes instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(HotelCutNotes instance) {
		log.debug("attaching clean HotelCutNotes instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(HotelCutNotes persistentInstance) {
		log.debug("deleting HotelCutNotes instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public HotelCutNotes merge(HotelCutNotes detachedInstance) {
		log.debug("merging HotelCutNotes instance");
		try {
			HotelCutNotes result = (HotelCutNotes) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public HotelCutNotes findById(long id) {
		log.debug("getting HotelCutNotes instance with id: " + id);
		try {
			HotelCutNotes instance = (HotelCutNotes) sessionFactory.getCurrentSession()
					.get("com.xlem.dao.HotelCutNotes", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(HotelCutNotes instance) {
		log.debug("finding HotelCutNotes instance by example");
		try {
			List results = sessionFactory.getCurrentSession().createCriteria("com.xlem.dao.HotelCutNotes")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
