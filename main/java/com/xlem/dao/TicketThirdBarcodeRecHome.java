package com.xlem.dao;
// Generated 2016-8-5 16:11:02 by Hibernate Tools 4.0.0.Final

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

/**
 * Home object for domain model class TicketThirdBarcodeRec.
 * @see com.xlem.dao.TicketThirdBarcodeRec
 * @author Hibernate Tools
 */
public class TicketThirdBarcodeRecHome {

	private static final Log log = LogFactory.getLog(TicketThirdBarcodeRecHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(TicketThirdBarcodeRec transientInstance) {
		log.debug("persisting TicketThirdBarcodeRec instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(TicketThirdBarcodeRec instance) {
		log.debug("attaching dirty TicketThirdBarcodeRec instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(TicketThirdBarcodeRec instance) {
		log.debug("attaching clean TicketThirdBarcodeRec instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(TicketThirdBarcodeRec persistentInstance) {
		log.debug("deleting TicketThirdBarcodeRec instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public TicketThirdBarcodeRec merge(TicketThirdBarcodeRec detachedInstance) {
		log.debug("merging TicketThirdBarcodeRec instance");
		try {
			TicketThirdBarcodeRec result = (TicketThirdBarcodeRec) sessionFactory.getCurrentSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public TicketThirdBarcodeRec findById(java.lang.String id) {
		log.debug("getting TicketThirdBarcodeRec instance with id: " + id);
		try {
			TicketThirdBarcodeRec instance = (TicketThirdBarcodeRec) sessionFactory.getCurrentSession()
					.get("com.xlem.dao.TicketThirdBarcodeRec", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(TicketThirdBarcodeRec instance) {
		log.debug("finding TicketThirdBarcodeRec instance by example");
		try {
			List results = sessionFactory.getCurrentSession().createCriteria("com.xlem.dao.TicketThirdBarcodeRec")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
