package com.xlem.dao;

import java.io.Serializable;
import java.util.List;

import common.CodeListener;

public class ConsumeTicketInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String userId;
	private String userCode;
	private String travelAgencyName;
	private String branchOffice;
	private String departments;
	private String discount;
	private String totalAmount;
	private String totalPrice;
	private Integer order;
	private String ticketTypeNames;
	private Integer ticketTotalQuantity;
	private Double ticketTotalAmount;
	private List subList;

	public List getSubList() {
		return subList;
	}

	public void setSubList(List subList) {
		this.subList = subList;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public String getTravelAgencyName() {
		return travelAgencyName;
	}

	public void setTravelAgencyName(String travelAgencyName) {
		this.travelAgencyName = travelAgencyName;
	}

	public String getTicketTypeNames() {
		return ticketTypeNames;
	}

	public void setTicketTypeNames(String ticketTypeNames) {
		this.ticketTypeNames = ticketTypeNames;
	}

	public Integer getTicketTotalQuantity() {
		return ticketTotalQuantity;
	}

	public void setTicketTotalQuantity(Integer ticketTotalQuantity) {
		this.ticketTotalQuantity = ticketTotalQuantity;
	}

	public Double getTicketTotalAmount() {
		return ticketTotalAmount;
	}

	public void setTicketTotalAmount(Double ticketTotalAmount) {
		this.ticketTotalAmount = ticketTotalAmount;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}
	public String getDiscountName() {
		return CodeListener.getNameByCode("TICKET_INDENT", "DISCOUNT", getDiscount());
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getBranchOffice() {
		return branchOffice;
	}

	public void setBranchOffice(String branchOffice) {
		this.branchOffice = branchOffice;
	}

	public String getDepartments() {
		return departments;
	}

	public void setDepartments(String departments) {
		this.departments = departments;
	}

}
