package com.xlem.dao;
// Generated 2016-8-3 14:45:38 by Hibernate Tools 4.0.0.Final

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

/**
 * Home object for domain model class HotelDetailHis.
 * @see com.xlem.dao.HotelDetailHis
 * @author Hibernate Tools
 */
public class HotelDetailHisHome {

	private static final Log log = LogFactory.getLog(HotelDetailHisHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(HotelDetailHis transientInstance) {
		log.debug("persisting HotelDetailHis instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(HotelDetailHis instance) {
		log.debug("attaching dirty HotelDetailHis instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(HotelDetailHis instance) {
		log.debug("attaching clean HotelDetailHis instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(HotelDetailHis persistentInstance) {
		log.debug("deleting HotelDetailHis instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public HotelDetailHis merge(HotelDetailHis detachedInstance) {
		log.debug("merging HotelDetailHis instance");
		try {
			HotelDetailHis result = (HotelDetailHis) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public HotelDetailHis findById(java.lang.Integer id) {
		log.debug("getting HotelDetailHis instance with id: " + id);
		try {
			HotelDetailHis instance = (HotelDetailHis) sessionFactory.getCurrentSession()
					.get("com.xlem.dao.HotelDetailHis", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(HotelDetailHis instance) {
		log.debug("finding HotelDetailHis instance by example");
		try {
			List results = sessionFactory.getCurrentSession().createCriteria("com.xlem.dao.HotelDetailHis")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List<HotelDetailHis> findByProperty(String string, Long hotelIndentId) {
		// TODO Auto-generated method stub
		log.debug("finding HotelDetailHis instance with property: "
				+ string + ", value: " + hotelIndentId);
		try {
			String queryString = "from HotelDetailHis as model where model."
					+ string + "= ?";
			Query queryObject = sessionFactory.getCurrentSession().createQuery(queryString);
			queryObject.setParameter(0, hotelIndentId);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}		
	}
}
