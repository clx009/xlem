package com.xlem.dao;

import common.CodeListener;



/**
 *  奖励款明细查询界面专用类
 */
public class TicketRechargeRec implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Constructors

	private String travelAgencyName;
	private String rewardTypeName;
	private String paymentType;
	private double rewardMoney;
	private double rewardMoneySnow;	
	private String inputTime;
	
	/** default constructor */
	public TicketRechargeRec() {
	}

	public String getTravelAgencyName() {
		return travelAgencyName;
	}

	public void setTravelAgencyName(String travelAgencyName) {
		this.travelAgencyName = travelAgencyName;
	}

	public String getRewardTypeName() {
		return rewardTypeName;
	}

	public void setRewardTypeName(String rewardTypeName) {
		this.rewardTypeName = rewardTypeName;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getPaymentTypeText() {
		return CodeListener.getNameByCode("BASE_RECHARGE_REC", "PAYMENT_TYPE",
				this.getPaymentType());
	}
	
	public String getHotelPaymentTypeText() {
		return CodeListener.getNameByCode("BASE_RECHARGE_REC", "PAYMENT_TYPE",
				this.getPaymentType());
	}
	
	public double getRewardMoney() {
		return rewardMoney;
	}

	public void setRewardMoney(double rewardMoney) {
		this.rewardMoney = rewardMoney;
	}

	public double getRewardMoneySnow() {
		return rewardMoneySnow;
	}

	public void setRewardMoneySnow(double rewardMoneySnow) {
		this.rewardMoneySnow = rewardMoneySnow;
	}

	public String getInputTime() {
		return inputTime;
	}

	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}


	
	
}
