package com.xlem.dao;
// Generated 2016-8-3 14:45:38 by Hibernate Tools 4.0.0.Final

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

/**
 * Home object for domain model class HotelCutRules.
 * @see com.xlem.dao.HotelCutRules
 * @author Hibernate Tools
 */
public class HotelCutRulesHome {

	private static final Log log = LogFactory.getLog(HotelCutRulesHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(HotelCutRules transientInstance) {
		log.debug("persisting HotelCutRules instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(HotelCutRules instance) {
		log.debug("attaching dirty HotelCutRules instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(HotelCutRules instance) {
		log.debug("attaching clean HotelCutRules instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(HotelCutRules persistentInstance) {
		log.debug("deleting HotelCutRules instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public HotelCutRules merge(HotelCutRules detachedInstance) {
		log.debug("merging HotelCutRules instance");
		try {
			HotelCutRules result = (HotelCutRules) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public HotelCutRules findById(java.lang.Integer id) {
		log.debug("getting HotelCutRules instance with id: " + id);
		try {
			HotelCutRules instance = (HotelCutRules) sessionFactory.getCurrentSession()
					.get("com.xlem.dao.HotelCutRules", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(HotelCutRules instance) {
		log.debug("finding HotelCutRules instance by example");
		try {
			List results = sessionFactory.getCurrentSession().createCriteria("com.xlem.dao.HotelCutRules")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
