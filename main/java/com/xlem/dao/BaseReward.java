package com.xlem.dao;



/**
 * 旅行社奖励款充值专用类
 */
public class BaseReward implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Constructors

	private BaseRewardAccount baseRewardAccount;
	private BaseRewardType baseRewardType;
	
	private Double ewardMoney;
	private Double ewardMoneySnow;
	private Long travelAgencyId;
	
	/** default constructor */
	public BaseReward() {
	}

	public BaseRewardAccount getBaseRewardAccount() {
		return baseRewardAccount;
	}

	public void setBaseRewardAccount(BaseRewardAccount baseRewardAccount) {
		this.baseRewardAccount = baseRewardAccount;
	}

	public BaseRewardType getBaseRewardType() {
		return baseRewardType;
	}

	public void setBaseRewardType(BaseRewardType baseRewardType) {
		this.baseRewardType = baseRewardType;
	}

	public Double getEwardMoney() {
		return ewardMoney;
	}

	public void setEwardMoney(Double ewardMoney) {
		this.ewardMoney = ewardMoney;
	}

	public Long getTravelAgencyId() {
		return travelAgencyId;
	}

	public void setTravelAgencyId(Long travelAgencyId) {
		this.travelAgencyId = travelAgencyId;
	}

	public Double getRewardBalance() {
		return this.baseRewardAccount.getRewardBalance();
	}
	
	public Double getRewardBalanceSnow() {
		return this.baseRewardAccount.getRewardBalanceSnow();
	}

	public Double getEwardMoneySnow() {
		return ewardMoneySnow;
	}

	public void setEwardMoneySnow(Double ewardMoneySnow) {
		this.ewardMoneySnow = ewardMoneySnow;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
