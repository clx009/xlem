package com.xlem.dao;
// Generated 2016-8-4 17:23:06 by Hibernate Tools 4.0.0.Final

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

/**
 * Home object for domain model class HotelActivityDetail.
 * @see com.xlem.dao.HotelActivityDetail
 * @author Hibernate Tools
 */
public class HotelActivityDetailHome {

	private static final Log log = LogFactory.getLog(HotelActivityDetailHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(HotelActivityDetail transientInstance) {
		log.debug("persisting HotelActivityDetail instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(HotelActivityDetail instance) {
		log.debug("attaching dirty HotelActivityDetail instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(HotelActivityDetail instance) {
		log.debug("attaching clean HotelActivityDetail instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(HotelActivityDetail persistentInstance) {
		log.debug("deleting HotelActivityDetail instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public HotelActivityDetail merge(HotelActivityDetail detachedInstance) {
		log.debug("merging HotelActivityDetail instance");
		try {
			HotelActivityDetail result = (HotelActivityDetail) sessionFactory.getCurrentSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public HotelActivityDetail findById(java.lang.Long id) {
		log.debug("getting HotelActivityDetail instance with id: " + id);
		try {
			HotelActivityDetail instance = (HotelActivityDetail) sessionFactory.getCurrentSession()
					.get("com.xlem.dao.HotelActivityDetail", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(HotelActivityDetail instance) {
		log.debug("finding HotelActivityDetail instance by example");
		try {
			List results = sessionFactory.getCurrentSession().createCriteria("com.xlem.dao.HotelActivityDetail")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
