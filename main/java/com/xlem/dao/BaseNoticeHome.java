package com.xlem.dao;
// Generated 2016-10-8 17:18:39 by Hibernate Tools 3.6.0.Final

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

/**
 * Home object for domain model class BaseNotice.
 * @see com.xlem.dao.BaseNotice
 * @author Hibernate Tools
 */
public class BaseNoticeHome {

	private static final Log log = LogFactory.getLog(BaseNoticeHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(BaseNotice transientInstance) {
		log.debug("persisting BaseNotice instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(BaseNotice instance) {
		log.debug("attaching dirty BaseNotice instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(BaseNotice instance) {
		log.debug("attaching clean BaseNotice instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(BaseNotice persistentInstance) {
		log.debug("deleting BaseNotice instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public BaseNotice merge(BaseNotice detachedInstance) {
		log.debug("merging BaseNotice instance");
		try {
			BaseNotice result = (BaseNotice) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public BaseNotice findById(java.lang.Long id) {
		log.debug("getting BaseNotice instance with id: " + id);
		try {
			BaseNotice instance = (BaseNotice) sessionFactory.getCurrentSession().get("com.xlem.dao.BaseNotice", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(BaseNotice instance) {
		log.debug("finding BaseNotice instance by example");
		try {
			List results = sessionFactory.getCurrentSession().createCriteria("com.xlem.dao.BaseNotice")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
