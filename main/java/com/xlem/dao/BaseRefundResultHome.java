package com.xlem.dao;
// Generated 2016-8-6 16:12:40 by Hibernate Tools 4.0.0.Final

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

/**
 * Home object for domain model class BaseRefundResult.
 * @see com.xlem.dao.BaseRefundResult
 * @author Hibernate Tools
 */
public class BaseRefundResultHome {

	private static final Log log = LogFactory.getLog(BaseRefundResultHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(BaseRefundResult transientInstance) {
		log.debug("persisting BaseRefundResult instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(BaseRefundResult instance) {
		log.debug("attaching dirty BaseRefundResult instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(BaseRefundResult instance) {
		log.debug("attaching clean BaseRefundResult instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(BaseRefundResult persistentInstance) {
		log.debug("deleting BaseRefundResult instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public BaseRefundResult merge(BaseRefundResult detachedInstance) {
		log.debug("merging BaseRefundResult instance");
		try {
			BaseRefundResult result = (BaseRefundResult) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public BaseRefundResult findById(java.lang.String id) {
		log.debug("getting BaseRefundResult instance with id: " + id);
		try {
			BaseRefundResult instance = (BaseRefundResult) sessionFactory.getCurrentSession()
					.get("com.xlem.dao.BaseRefundResult", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(BaseRefundResult instance) {
		log.debug("finding BaseRefundResult instance by example");
		try {
			List results = sessionFactory.getCurrentSession().createCriteria("com.xlem.dao.BaseRefundResult")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
