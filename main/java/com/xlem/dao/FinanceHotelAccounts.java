package com.xlem.dao;
// Generated 2016-8-3 14:45:38 by Hibernate Tools 4.0.0.Final

import java.util.Date;

/**
 * FinanceHotelAccounts generated by hbm2java
 */
public class FinanceHotelAccounts implements java.io.Serializable {

	private Long hotelAccountsId;
	private String accountsType;
	private String belongCompany;
	private String paymentType;
	private Double money;
	private Date inputTime;
	private String hotelIndentId;
	private String hotelIndentCode;
	private Long hotelIndentDetailId;
	private Long roomTypeId;
	private String typeName;
	private Long hotelRechargeRecId;
	private String tranRecId;
	private Long cutNotesId;
	private Integer amount;
	private Long backAccountsId;
	private Long returnAccountsId;
	private String remark;

	public FinanceHotelAccounts() {
	}

	public FinanceHotelAccounts(String accountsType, String belongCompany, String paymentType, Double money,
			Date inputTime, String hotelIndentId, String hotelIndentCode, Long hotelIndentDetailId,
			Long roomTypeId, String typeName, Long hotelRechargeRecId, String tranRecId, Long cutNotesId,
			Integer amount, Long backAccountsId, Long returnAccountsId, String remark) {
		this.accountsType = accountsType;
		this.belongCompany = belongCompany;
		this.paymentType = paymentType;
		this.money = money;
		this.inputTime = inputTime;
		this.hotelIndentId = hotelIndentId;
		this.hotelIndentCode = hotelIndentCode;
		this.hotelIndentDetailId = hotelIndentDetailId;
		this.roomTypeId = roomTypeId;
		this.typeName = typeName;
		this.hotelRechargeRecId = hotelRechargeRecId;
		this.tranRecId = tranRecId;
		this.cutNotesId = cutNotesId;
		this.amount = amount;
		this.backAccountsId = backAccountsId;
		this.returnAccountsId = returnAccountsId;
		this.remark = remark;
	}

	public Long getHotelAccountsId() {
		return this.hotelAccountsId;
	}

	public void setHotelAccountsId(Long hotelAccountsId) {
		this.hotelAccountsId = hotelAccountsId;
	}

	public String getAccountsType() {
		return this.accountsType;
	}

	public void setAccountsType(String accountsType) {
		this.accountsType = accountsType;
	}

	public String getBelongCompany() {
		return this.belongCompany;
	}

	public void setBelongCompany(String belongCompany) {
		this.belongCompany = belongCompany;
	}

	public String getPaymentType() {
		return this.paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public Double getMoney() {
		return this.money;
	}

	public void setMoney(Double money) {
		this.money = money;
	}

	public Date getInputTime() {
		return this.inputTime;
	}

	public void setInputTime(Date inputTime) {
		this.inputTime = inputTime;
	}

	public String getHotelIndentId() {
		return this.hotelIndentId;
	}

	public void setHotelIndentId(String hotelIndentId) {
		this.hotelIndentId = hotelIndentId;
	}

	public String getHotelIndentCode() {
		return this.hotelIndentCode;
	}

	public void setHotelIndentCode(String hotelIndentCode) {
		this.hotelIndentCode = hotelIndentCode;
	}

	public Long getHotelIndentDetailId() {
		return this.hotelIndentDetailId;
	}

	public void setHotelIndentDetailId(Long hotelIndentDetailId) {
		this.hotelIndentDetailId = hotelIndentDetailId;
	}

	public Long getRoomTypeId() {
		return this.roomTypeId;
	}

	public void setRoomTypeId(Long roomTypeId) {
		this.roomTypeId = roomTypeId;
	}

	public String getTypeName() {
		return this.typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Long getHotelRechargeRecId() {
		return this.hotelRechargeRecId;
	}

	public void setHotelRechargeRecId(Long hotelRechargeRecId) {
		this.hotelRechargeRecId = hotelRechargeRecId;
	}

	public String getTranRecId() {
		return this.tranRecId;
	}

	public void setTranRecId(String tranRecId) {
		this.tranRecId = tranRecId;
	}

	public Long getCutNotesId() {
		return this.cutNotesId;
	}

	public void setCutNotesId(Long cutNotesId) {
		this.cutNotesId = cutNotesId;
	}

	public Integer getAmount() {
		return this.amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Long getBackAccountsId() {
		return this.backAccountsId;
	}

	public void setBackAccountsId(Long backAccountsId) {
		this.backAccountsId = backAccountsId;
	}

	public Long getReturnAccountsId() {
		return this.returnAccountsId;
	}

	public void setReturnAccountsId(Long returnAccountsId) {
		this.returnAccountsId = returnAccountsId;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	/**
	 * 账目类型:01奖励款
	 */
	public static final String ACCOUNTSTYPE_01 = "01";
	/**
	 * 账目类型:02现金
	 */
	public static final String ACCOUNTSTYPE_02 = "02";
	/**
	 * 账目类型:03市场部下单
	 */
	public static final String ACCOUNTSTYPE_03 = "03";

	/**
	 * 支付类型:00订单预扣余额
	 */
	public static final String PAYMENTTYPE_00 = "00";
	/**
	 * 支付类型:01网银已支付未消费金额
	 */
	public static final String PAYMENTTYPE_01 = "01";
	/**
	 * 支付类型:02余额已支付未消费金额
	 */
	public static final String PAYMENTTYPE_02 = "02";
	/**
	 * 支付类型:03奖励款充值金额
	 */
	public static final String PAYMENTTYPE_03 = "03";
	/**
	 * 支付类型:04网银支付已消费金额
	 */
	public static final String PAYMENTTYPE_04 = "04";
	/**
	 * 支付类型:05余额支付已消费金额
	 */
	public static final String PAYMENTTYPE_05 = "05";
	/**
	 * 支付类型:06退款到卡上金额
	 */
	public static final String PAYMENTTYPE_06 = "06";
	/**
	 * 支付类型:07余额付款退款到余额
	 */
	public static final String PAYMENTTYPE_07 = "07";
	/**
	 * 支付类型:08扣除房损金额
	 */
	public static final String PAYMENTTYPE_08 = "08";
	/**
	 * 支付类型:09冲退（做订单时暂扣余额在付款时需冲退）
	 */
	public static final String PAYMENTTYPE_09 = "09";
	/**
	 * 支付类型:10抵扣（打印回传需抵扣）
	 */
	public static final String PAYMENTTYPE_10 = "10";
	
	/** minimal constructor */
	public FinanceHotelAccounts(Long hotelAccountsId) {
		this.hotelAccountsId = hotelAccountsId;
	}

}
