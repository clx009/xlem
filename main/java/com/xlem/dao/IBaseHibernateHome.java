/**
 * 
 */
package com.xlem.dao;

import org.hibernate.Session;

/**
 * @author clx
 *
 */
public interface IBaseHibernateHome {
	public Session getSession();
}
