package com.xlem.dao;
// Generated 2016-8-4 21:55:49 by Hibernate Tools 4.0.0.Final

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

/**
 * Home object for domain model class HotelRechargeRec.
 * @see com.xlem.dao.HotelRechargeRec
 * @author Hibernate Tools
 */
public class HotelRechargeRecHome {

	private static final Log log = LogFactory.getLog(HotelRechargeRecHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(HotelRechargeRec transientInstance) {
		log.debug("persisting HotelRechargeRec instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(HotelRechargeRec instance) {
		log.debug("attaching dirty HotelRechargeRec instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(HotelRechargeRec instance) {
		log.debug("attaching clean HotelRechargeRec instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(HotelRechargeRec persistentInstance) {
		log.debug("deleting HotelRechargeRec instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public HotelRechargeRec merge(HotelRechargeRec detachedInstance) {
		log.debug("merging HotelRechargeRec instance");
		try {
			HotelRechargeRec result = (HotelRechargeRec) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public HotelRechargeRec findById(java.lang.Long id) {
		log.debug("getting HotelRechargeRec instance with id: " + id);
		try {
			HotelRechargeRec instance = (HotelRechargeRec) sessionFactory.getCurrentSession()
					.get("com.xlem.dao.HotelRechargeRec", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(HotelRechargeRec instance) {
		log.debug("finding HotelRechargeRec instance by example");
		try {
			List results = sessionFactory.getCurrentSession().createCriteria("com.xlem.dao.HotelRechargeRec")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
