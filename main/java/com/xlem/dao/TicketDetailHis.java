package com.xlem.dao;
// Generated 2016-8-5 16:11:00 by Hibernate Tools 4.0.0.Final

import java.util.Date;


/**
 * TicketDetailHis generated by hbm2java
 */
public class TicketDetailHis implements java.io.Serializable {

	private Long detailHisId;
	private Integer version;
	private TicketIndent ticketIndent;
	private TicketPrice ticketPrice;
	private String indentDetailId;
	private String indentDetailCode;
	private Double unitPrice;
	private Integer amount;
	private Double perFee;
	private Double subtotal;
	private Date startDate;
	private Date endDate;
	private String remark;
	private long inputer;
	private Date inputTime;
	private Long updater;
	private Date updateTime;
	private Double entranceUnitPrice;
	private String entranceCompany;
	private Double ropewayUnitPrice;
	private String ropewayCompany;

	public TicketDetailHis() {
	}

	public TicketDetailHis(long inputer) {
		this.inputer = inputer;
	}

	public TicketDetailHis(TicketIndent ticketIndent, TicketPrice ticketPrice, String indentDetailId, String indentDetailCode,
			Double unitPrice, Integer amount, Double perFee, Double subtotal, Date startDate, Date endDate,
			String remark, long inputer, Date inputTime, Long updater, Date updateTime, Double entranceUnitPrice,
			String entranceCompany, Double ropewayUnitPrice, String ropewayCompany) {
		this.ticketIndent = ticketIndent;
		this.ticketPrice = ticketPrice;
		this.indentDetailId = indentDetailId;
		this.indentDetailCode = indentDetailCode;
		this.unitPrice = unitPrice;
		this.amount = amount;
		this.perFee = perFee;
		this.subtotal = subtotal;
		this.startDate = startDate;
		this.endDate = endDate;
		this.remark = remark;
		this.inputer = inputer;
		this.inputTime = inputTime;
		this.updater = updater;
		this.updateTime = updateTime;
		this.entranceUnitPrice = entranceUnitPrice;
		this.entranceCompany = entranceCompany;
		this.ropewayUnitPrice = ropewayUnitPrice;
		this.ropewayCompany = ropewayCompany;
	}

	public Long getDetailHisId() {
		return this.detailHisId;
	}

	public void setDetailHisId(Long detailHisId) {
		this.detailHisId = detailHisId;
	}

	public Integer getVersion() {
		return this.version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public TicketIndent getTicketIndent() {
		return this.ticketIndent;
	}

	public void setTicketIndent(TicketIndent ticketIndent) {
		this.ticketIndent = ticketIndent;
	}

	public TicketPrice getTicketPrice() {
		return this.ticketPrice;
	}

	public void setTicketPrice(TicketPrice ticketPrice) {
		this.ticketPrice = ticketPrice;
	}

	public String getIndentDetailId() {
		return this.indentDetailId;
	}

	public void setIndentDetailId(String indentDetailId) {
		this.indentDetailId = indentDetailId;
	}

	public String getIndentDetailCode() {
		return this.indentDetailCode;
	}

	public void setIndentDetailCode(String indentDetailCode) {
		this.indentDetailCode = indentDetailCode;
	}

	public Double getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Integer getAmount() {
		return this.amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Double getPerFee() {
		return this.perFee;
	}

	public void setPerFee(Double perFee) {
		this.perFee = perFee;
	}

	public Double getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(Double subtotal) {
		this.subtotal = subtotal;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public long getInputer() {
		return this.inputer;
	}

	public void setInputer(long inputer) {
		this.inputer = inputer;
	}

	public Date getInputTime() {
		return this.inputTime;
	}

	public void setInputTime(Date inputTime) {
		this.inputTime = inputTime;
	}

	public Long getUpdater() {
		return this.updater;
	}

	public void setUpdater(Long updater) {
		this.updater = updater;
	}

	public Date getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Double getEntranceUnitPrice() {
		return this.entranceUnitPrice;
	}

	public void setEntranceUnitPrice(Double entranceUnitPrice) {
		this.entranceUnitPrice = entranceUnitPrice;
	}

	public String getEntranceCompany() {
		return this.entranceCompany;
	}

	public void setEntranceCompany(String entranceCompany) {
		this.entranceCompany = entranceCompany;
	}

	public Double getRopewayUnitPrice() {
		return this.ropewayUnitPrice;
	}

	public void setRopewayUnitPrice(Double ropewayUnitPrice) {
		this.ropewayUnitPrice = ropewayUnitPrice;
	}

	public String getRopewayCompany() {
		return this.ropewayCompany;
	}

	public void setRopewayCompany(String ropewayCompany) {
		this.ropewayCompany = ropewayCompany;
	}

}
