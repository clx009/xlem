package com.xlem.dao;
// Generated 2016-8-5 16:11:00 by Hibernate Tools 4.0.0.Final

import java.util.Date;

import javax.persistence.Transient;

import common.CodeListener;

/**
 * TicketIndentCheckLog generated by hbm2java
 */
public class TicketIndentCheckLog implements java.io.Serializable {

	private Long checkLogId;
	private TicketIndent ticketIndent;
	private String discount;
	private long inputer;
	private Date inputTime;
	private String status;
	private String remark;

	public TicketIndentCheckLog() {
	}

	public TicketIndentCheckLog(long inputer) {
		this.inputer = inputer;
	}

	public TicketIndentCheckLog(TicketIndent ticketIndent, String discount, long inputer, Date inputTime, String status,
			String remark) {
		this.ticketIndent = ticketIndent;
		this.discount = discount;
		this.inputer = inputer;
		this.inputTime = inputTime;
		this.status = status;
		this.remark = remark;
	}

	public Long getCheckLogId() {
		return this.checkLogId;
	}

	public void setCheckLogId(Long checkLogId) {
		this.checkLogId = checkLogId;
	}

	public TicketIndent getTicketIndent() {
		return this.ticketIndent;
	}

	public void setTicketIndent(TicketIndent ticketIndent) {
		this.ticketIndent = ticketIndent;
	}

	public String getDiscount() {
		return this.discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public long getInputer() {
		return this.inputer;
	}

	public void setInputer(long inputer) {
		this.inputer = inputer;
	}

	public Date getInputTime() {
		return this.inputTime;
	}

	public void setInputTime(Date inputTime) {
		this.inputTime = inputTime;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	@Transient
	public String getStatusText() {
		return CodeListener.getNameByCode("TICKET_TYPE", "STATUS", getStatus());
	}

	@Transient
	public String getDiscountText() {
		return CodeListener.getNameByCode("TICKET_INDENT", "DISCOUNT",
				getDiscount());
	}

	@Transient
	public String getUserName() {
		return CodeListener.getNameByUserId(this.getInputer());
	}
}
