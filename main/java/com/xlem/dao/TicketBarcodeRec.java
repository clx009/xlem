package com.xlem.dao;
// Generated 2016-8-5 16:11:00 by Hibernate Tools 4.0.0.Final

import java.util.Date;

/**
 * TicketBarcodeRec generated by hbm2java
 */
public class TicketBarcodeRec implements java.io.Serializable {

	private String barcodeRecId;
	private TicketIndentDetail ticketIndentDetail;
	private TicketCheckRec ticketCheckRec;
	private String originalCode;
	private String encryptionCode;
	private String serialNumber;
	private String changingMachineNumber;
	private Date printTime;
	private String gate;
	private String checkingMachineNumber;
	private Date useTime;
	private String ticketBarcodeReccol;
	private String status;
	private String isCheck;
	private String backStatus;

	public TicketBarcodeRec() {
	}

	public TicketBarcodeRec(String barcodeRecId, TicketIndentDetail ticketIndentDetail) {
		this.barcodeRecId = barcodeRecId;
		this.ticketIndentDetail = ticketIndentDetail;
	}

	public TicketBarcodeRec(String barcodeRecId, TicketIndentDetail ticketIndentDetail, TicketCheckRec ticketCheckRec, String originalCode,
			String encryptionCode, String serialNumber, String changingMachineNumber, Date printTime, String gate,
			String checkingMachineNumber, Date useTime, String ticketBarcodeReccol, String status, String isCheck,
			String backStatus) {
		this.barcodeRecId = barcodeRecId;
		this.ticketIndentDetail = ticketIndentDetail;
		this.ticketCheckRec = ticketCheckRec;
		this.originalCode = originalCode;
		this.encryptionCode = encryptionCode;
		this.serialNumber = serialNumber;
		this.changingMachineNumber = changingMachineNumber;
		this.printTime = printTime;
		this.gate = gate;
		this.checkingMachineNumber = checkingMachineNumber;
		this.useTime = useTime;
		this.ticketBarcodeReccol = ticketBarcodeReccol;
		this.status = status;
		this.isCheck = isCheck;
		this.backStatus = backStatus;
	}

	public String getBarcodeRecId() {
		return this.barcodeRecId;
	}

	public void setBarcodeRecId(String barcodeRecId) {
		this.barcodeRecId = barcodeRecId;
	}

	public TicketIndentDetail getTicketIndentDetail() {
		return this.ticketIndentDetail;
	}

	public void setTicketIndentDetail(TicketIndentDetail ticketIndentDetail) {
		this.ticketIndentDetail = ticketIndentDetail;
	}

	public TicketCheckRec getTicketCheckRec() {
		return this.ticketCheckRec;
	}

	public void setTicketCheckRec(TicketCheckRec ticketCheckRec) {
		this.ticketCheckRec = ticketCheckRec;
	}
	public String getOriginalCode() {
		return this.originalCode;
	}

	public void setOriginalCode(String originalCode) {
		this.originalCode = originalCode;
	}

	public String getEncryptionCode() {
		return this.encryptionCode;
	}

	public void setEncryptionCode(String encryptionCode) {
		this.encryptionCode = encryptionCode;
	}

	public String getSerialNumber() {
		return this.serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getChangingMachineNumber() {
		return this.changingMachineNumber;
	}

	public void setChangingMachineNumber(String changingMachineNumber) {
		this.changingMachineNumber = changingMachineNumber;
	}

	public Date getPrintTime() {
		return this.printTime;
	}

	public void setPrintTime(Date printTime) {
		this.printTime = printTime;
	}

	public String getGate() {
		return this.gate;
	}

	public void setGate(String gate) {
		this.gate = gate;
	}

	public String getCheckingMachineNumber() {
		return this.checkingMachineNumber;
	}

	public void setCheckingMachineNumber(String checkingMachineNumber) {
		this.checkingMachineNumber = checkingMachineNumber;
	}

	public Date getUseTime() {
		return this.useTime;
	}

	public void setUseTime(Date useTime) {
		this.useTime = useTime;
	}

	public String getTicketBarcodeReccol() {
		return this.ticketBarcodeReccol;
	}

	public void setTicketBarcodeReccol(String ticketBarcodeReccol) {
		this.ticketBarcodeReccol = ticketBarcodeReccol;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIsCheck() {
		return this.isCheck;
	}

	public void setIsCheck(String isCheck) {
		this.isCheck = isCheck;
	}

	public String getBackStatus() {
		return this.backStatus;
	}

	public void setBackStatus(String backStatus) {
		this.backStatus = backStatus;
	}

}
