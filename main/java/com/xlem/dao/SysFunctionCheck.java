package com.xlem.dao;

import java.sql.Timestamp;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * SysFunction entity. @author MyEclipse Persistence Tools
 */
public class SysFunctionCheck extends SysFunction implements
		java.io.Serializable {
	// Constructors
	private boolean checked;
	/** default constructor */
	public SysFunctionCheck() {
	}

	/** minimal constructor */
	public SysFunctionCheck(Long functionId) {
		super(functionId);
	}

	/** full constructor */
	public SysFunctionCheck(Long functionId, Long fatherFunctionId,
			String functionCode, String functionName, String functionUrl,
			String state, Timestamp createDate, String isLast,
			Set<SysRoleFunction> sysRoleFunctions) {
		super(functionId, fatherFunctionId, functionCode, functionName,
				functionUrl, state, createDate, isLast, sysRoleFunctions);
	}
	@Transient
	public String getId() {
		return getFunctionId()==null?null:getFunctionId().toString();
	}
	@Transient
	public String getText() {
		return getFunctionName();
	}
	@Transient
	public String getCode() {
		return getFunctionCode();
	}
	@Transient
	public String getParent() {
		return getFatherFunctionId()==null?null:getFatherFunctionId().toString();
	}

	@Transient
	public boolean isLeaf() {
		if(getIsLast()!=null && getIsLast().equals("11")){
			return true;
		}
		return false;
	}

	@Transient
	public boolean isChecked(){
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}
}
