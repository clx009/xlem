package com.xlem.dao;
// Generated 2016-8-3 14:45:38 by Hibernate Tools 4.0.0.Final

import java.util.Date;

import javax.persistence.Transient;

import common.CodeListener;

/**
 * HotelPrice generated by hbm2java
 */
public class HotelPrice implements java.io.Serializable {

	private Long hotelPriceId;
	private HotelRoomType hotelRoomType;
	private Double unitPrice;
	private Double weekendFriPrice;
	private Double weekendSatPrice;
	private Date startDate;
	private Date endDate;
	private String status;
	private String remark;
	private Long inputer;
	private Date inputTime;
	private Long updater;
	private Date updateTime;
	private Double unitPriceGroup;
	private double weekendFriPriceGroup;
	private double weekendSatPriceGroup;

	public HotelPrice() {
	}

	public HotelPrice(HotelRoomType hotelRoomType, Double unitPrice, Date startDate, Date endDate, String status, Long inputer,
			Date inputTime, Double unitPriceGroup, double weekendFriPriceGroup, double weekendSatPriceGroup) {
		this.hotelRoomType = hotelRoomType;
		this.unitPrice = unitPrice;
		this.startDate = startDate;
		this.endDate = endDate;
		this.status = status;
		this.inputer = inputer;
		this.inputTime = inputTime;
		this.unitPriceGroup = unitPriceGroup;
		this.weekendFriPriceGroup = weekendFriPriceGroup;
		this.weekendSatPriceGroup = weekendSatPriceGroup;
	}

	public HotelPrice(HotelRoomType hotelRoomType, Double unitPrice, Double weekendFriPrice, Double weekendSatPrice, Date startDate,
			Date endDate, String status, String remark, Long inputer, Date inputTime, Long updater, Date updateTime,
			Double unitPriceGroup, Double weekendFriPriceGroup, Double weekendSatPriceGroup) {
		this.hotelRoomType = hotelRoomType;
		this.unitPrice = unitPrice;
		this.weekendFriPrice = weekendFriPrice;
		this.weekendSatPrice = weekendSatPrice;
		this.startDate = startDate;
		this.endDate = endDate;
		this.status = status;
		this.remark = remark;
		this.inputer = inputer;
		this.inputTime = inputTime;
		this.updater = updater;
		this.updateTime = updateTime;
		this.unitPriceGroup = unitPriceGroup;
		this.weekendFriPriceGroup = weekendFriPriceGroup;
		this.weekendSatPriceGroup = weekendSatPriceGroup;
	}

	public Long getHotelPriceId() {
		return this.hotelPriceId;
	}

	public void setHotelPriceId(Long hotelPriceId) {
		this.hotelPriceId = hotelPriceId;
	}

	public HotelRoomType getHotelRoomType() {
		return this.hotelRoomType;
	}

	public void setHotelRoomType(HotelRoomType hotelRoomType) {
		this.hotelRoomType = hotelRoomType;
	}
	
	public Double getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Double getWeekendFriPrice() {
		return this.weekendFriPrice;
	}

	public void setWeekendFriPrice(Double weekendFriPrice) {
		this.weekendFriPrice = weekendFriPrice;
	}

	public Double getWeekendSatPrice() {
		return this.weekendSatPrice;
	}

	public void setWeekendSatPrice(Double weekendSatPrice) {
		this.weekendSatPrice = weekendSatPrice;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getInputer() {
		return this.inputer;
	}

	public void setInputer(Long inputer) {
		this.inputer = inputer;
	}

	public Date getInputTime() {
		return this.inputTime;
	}

	public void setInputTime(Date inputTime) {
		this.inputTime = inputTime;
	}

	public Long getUpdater() {
		return this.updater;
	}

	public void setUpdater(Long updater) {
		this.updater = updater;
	}

	public Date getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Double getUnitPriceGroup() {
		return this.unitPriceGroup;
	}

	public void setUnitPriceGroup(Double unitPriceGroup) {
		this.unitPriceGroup = unitPriceGroup;
	}

	public Double getWeekendFriPriceGroup() {
		return this.weekendFriPriceGroup;
	}

	public void setWeekendFriPriceGroup(Double weekendFriPriceGroup) {
		this.weekendFriPriceGroup = weekendFriPriceGroup;
	}

	public Double getWeekendSatPriceGroup() {
		return this.weekendSatPriceGroup;
	}

	public void setWeekendSatPriceGroup(Double weekendSatPriceGroup) {
		this.weekendSatPriceGroup = weekendSatPriceGroup;
	}
	
	@Transient
	public String getStatusText() {
		return CodeListener.getNameByCode("HOTEL_PRICE", "STATUS", getStatus());
	}
}
