package com.xlem.dao;

import java.sql.Timestamp;

public class AvailableRoomInfo {
	
	//日期
	private Timestamp currentDate;
	//房型
	private String roomTypeCode;
	//总房数
	private Integer quantity;
	//超预定
	private Integer adjquan;
	//维修房
	private Integer lockedquan;
	//预留房
	private Integer rsvquan;
	//平台锁房
	private Integer stlockedquan;
	//平台预留房
	private Integer strsvquan;
	//可用房
	private Integer avlquan;

	public Timestamp getCurrentDate() {
		return currentDate;
	}
	public void setCurrentDate(Timestamp currentDate) {
		this.currentDate = currentDate;
	}
	public String getRoomTypeCode() {
		return roomTypeCode;
	}
	public void setRoomTypeCode(String roomTypeCode) {
		this.roomTypeCode = roomTypeCode;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getAdjquan() {
		return adjquan;
	}
	public void setAdjquan(Integer adjquan) {
		this.adjquan = adjquan;
	}
	public Integer getLockedquan() {
		return lockedquan;
	}
	public void setLockedquan(Integer lockedquan) {
		this.lockedquan = lockedquan;
	}
	public Integer getRsvquan() {
		return rsvquan;
	}
	public void setRsvquan(Integer rsvquan) {
		this.rsvquan = rsvquan;
	}
	public Integer getStlockedquan() {
		return stlockedquan;
	}
	public void setStlockedquan(Integer stlockedquan) {
		this.stlockedquan = stlockedquan;
	}
	public Integer getStrsvquan() {
		return strsvquan;
	}
	public void setStrsvquan(Integer strsvquan) {
		this.strsvquan = strsvquan;
	}
	public Integer getAvlquan() {
		return avlquan;
	}
	public void setAvlquan(Integer avlquan) {
		this.avlquan = avlquan;
	}

}
