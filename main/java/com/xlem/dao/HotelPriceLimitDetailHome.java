package com.xlem.dao;
// Generated 2016-8-3 14:45:38 by Hibernate Tools 4.0.0.Final

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Example;
import org.springframework.orm.hibernate3.HibernateTemplate;

/**
 * Home object for domain model class HotelPriceLimitDetail.
 * @see com.xlem.dao.HotelPriceLimitDetail
 * @author Hibernate Tools
 */
public class HotelPriceLimitDetailHome {

	private static final Log log = LogFactory.getLog(HotelPriceLimitDetailHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(HotelPriceLimitDetail transientInstance) {
		log.debug("persisting HotelPriceLimitDetail instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(HotelPriceLimitDetail instance) {
		log.debug("attaching dirty HotelPriceLimitDetail instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(HotelPriceLimitDetail instance) {
		log.debug("attaching clean HotelPriceLimitDetail instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(HotelPriceLimitDetail persistentInstance) {
		log.debug("deleting HotelPriceLimitDetail instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public HotelPriceLimitDetail merge(HotelPriceLimitDetail detachedInstance) {
		log.debug("merging HotelPriceLimitDetail instance");
		try {
			HotelPriceLimitDetail result = (HotelPriceLimitDetail) sessionFactory.getCurrentSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public HotelPriceLimitDetail findById(java.lang.Integer id) {
		log.debug("getting HotelPriceLimitDetail instance with id: " + id);
		try {
			HotelPriceLimitDetail instance = (HotelPriceLimitDetail) sessionFactory.getCurrentSession()
					.get("com.xlem.dao.HotelPriceLimitDetail", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(HotelPriceLimitDetail instance) {
		log.debug("finding HotelPriceLimitDetail instance by example");
		try {
			List results = sessionFactory.getCurrentSession().createCriteria("com.xlem.dao.HotelPriceLimitDetail")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
