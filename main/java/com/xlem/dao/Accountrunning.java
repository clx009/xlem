package com.xlem.dao;
// Generated 2016-8-3 14:45:38 by Hibernate Tools 4.0.0.Final

import java.util.Date;

/**
 * Accountrunning generated by hbm2java
 */
public class Accountrunning implements java.io.Serializable {

	private Integer idAccountRunning;
	private int version;
	private int idAccount;
	private int operation;
	private double amount;
	private Date runningTime;
	private int sourceAccountId;

	public Accountrunning() {
	}

	public Accountrunning(int idAccount, int operation, double amount, Date runningTime, int sourceAccountId) {
		this.idAccount = idAccount;
		this.operation = operation;
		this.amount = amount;
		this.runningTime = runningTime;
		this.sourceAccountId = sourceAccountId;
	}

	public Integer getIdAccountRunning() {
		return this.idAccountRunning;
	}

	public void setIdAccountRunning(Integer idAccountRunning) {
		this.idAccountRunning = idAccountRunning;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public int getIdAccount() {
		return this.idAccount;
	}

	public void setIdAccount(int idAccount) {
		this.idAccount = idAccount;
	}

	public int getOperation() {
		return this.operation;
	}

	public void setOperation(int operation) {
		this.operation = operation;
	}

	public double getAmount() {
		return this.amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getRunningTime() {
		return this.runningTime;
	}

	public void setRunningTime(Date runningTime) {
		this.runningTime = runningTime;
	}

	public int getSourceAccountId() {
		return this.sourceAccountId;
	}

	public void setSourceAccountId(int sourceAccountId) {
		this.sourceAccountId = sourceAccountId;
	}

}
