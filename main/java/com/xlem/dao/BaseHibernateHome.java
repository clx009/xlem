package com.xlem.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class BaseHibernateHome implements IBaseHibernateHome {

	@Resource
	private SessionFactory sessionFactory;
	
	@Override
	public Session getSession() {
		return  sessionFactory.getCurrentSession();
		//return  sessionFactory.openSession();
	}
	
	public List exeSql(String sql){
		return this.getSession().createQuery(sql).list();
	}
	public List exeSql2(String sql){
		return this.getSession().createSQLQuery(sql).list();
	}
	
}
