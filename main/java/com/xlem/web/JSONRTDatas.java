package com.xlem.web;

import java.util.List;

public class JSONRTDatas {
	private Integer num;
	private List<Integer> saleNum;
	private List<Double> saleMoney;
	
	public Integer getNum() {
		return this.num;
	}
	
	public void setNum(Integer num) {
		this.num = num;
	}
	
	public List<Integer> getSaleNum() {
		return this.saleNum;
	}
	
	public void setSaleNum(List<Integer> saleNum) {
		this.saleNum = saleNum;
	}
	
	public List<Double> getSaleMoney() {
		return this.saleMoney;
	}
	
	public void setSaleMoney(List<Double> saleMoney) {
		this.saleMoney = saleMoney;
	}
}
