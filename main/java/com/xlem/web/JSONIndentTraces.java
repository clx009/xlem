package com.xlem.web;

import java.util.List;

public class JSONIndentTraces {
	private Long num;
	private List<JSONIndentTrace> traces;
	
	public Long getNum() {
		return this.num;
	}
	
	public void setNum(Long num) {
		this.num = num;
	}
	
	public List<JSONIndentTrace> getTraces() {
		return this.traces;
	}
	
	public void setTraces(List<JSONIndentTrace> traces) {
		this.traces = traces;
	}

}
