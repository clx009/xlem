package com.xlem.web;

public class JSONWeather {
	private String date;
	private String icon;
	private String weather_d;
	private String weather_n;
	private String temp;
	private String wind;

	public String getDate() {
		return this.date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getIcon() {
		return this.icon;
	}
	
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	public String getWeather_d() {
		return this.weather_d;
	}
	
	public void setWeather_d(String weather_d) {
		this.weather_d = weather_d;
	}
	
	public String getWeather_n() {
		return this.weather_n;
	}
	
	public void setWeather_n(String weather_n) {
		this.weather_n = weather_n;
	}
	
	public String getTemp() {
		return this.temp;
	}
	
	public void setTemp(String temp) {
		this.temp = temp;
	}

	public String getWind() {
		return this.wind;
	}
	
	public void setWind(String wind) {
		this.wind = wind;
	}
}
