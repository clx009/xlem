package com.xlem.web;

import java.util.Date;

public class JSONBill {
	private Date date;
	private String name;
	private String indentId;
	private String transRecId;
	private String otherSide;
	private String indentSource;
	private Double money;
	private String moneyDetail;
	private String status;
	
	public Date getDate() {
		return this.date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getIndentId() {
		return this.indentId;
	}
	
	public void setIndentId(String indentId) {
		this.indentId = indentId;
	}
	
	public String getTransRecId() {
		return this.transRecId;
	}
	
	public void setTransRecId(String transRecId) {
		this.transRecId = transRecId;
	}
	
	public String getOtherSide() {
		return this.otherSide;
	}
	
	public void setOtherSide(String otherSide) {
		this.otherSide = otherSide;
	}
	
	public String getIndentSource() {
		return this.indentSource;
	}
	
	public void setIndentSource(String indentSource) {
		this.indentSource = indentSource;
	}
	
	public Double getMoney() {
		return this.money;
	}
	
	public void setMoney(Double money) {
		this.money = money;
	}
	
	public String getMoneyDetail() {
		return this.moneyDetail;
	}
	
	public void setMoneyDetail(String moneyDetail) {
		this.moneyDetail = moneyDetail;
	}
	
	public String getStatus() {
		return this.status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
}
