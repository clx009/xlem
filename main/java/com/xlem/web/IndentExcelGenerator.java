package com.xlem.web;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.BaseTranRec;
import com.xlem.dao.TicketIndent;
import com.xlem.dao.TicketIndentDetail;
import com.xlem.dao.Travservice;
import com.xlem.dao.User;

import service.comm.baseRechargeRec.BaseRechargeRecService;
import service.comm.baseTranRec.BaseTranRecService;
import service.comm.reg.baseTravelAgency.BaseTravelAgencyService;
import service.ticket.ticketIndent.TicketIndentService;

public class IndentExcelGenerator extends HttpServlet {
	@Autowired
	private BaseTranRecService baseTranRecService;
	@Autowired
	private BaseTravelAgencyService baseTravelAgencyService;
	@Autowired
	private BaseRechargeRecService baseRechargeRecService;
	@Autowired
	private TicketIndentService ticketIndentService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	try{
    		String startDate = request.getParameter("startDate");
    		String endDate = request.getParameter("endDate"); 
    		String type = request.getParameter("type");
    		
    		DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
    		Date start = fmt.parse(startDate);
    		Date end = fmt.parse(endDate);
    		
    		List<JSONInBill> jsonBills = new ArrayList<JSONInBill>();
    		
    		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(TicketIndent.class);
    		detachedCriteria.add(Property.forName("bookDate").ge(start));
    		detachedCriteria.add(Property.forName("bookDate").le(end));
    	
    		switch (type) {
    		case "payed":
    			detachedCriteria.add(Property.forName("status").eq("01"));
    			break;
    			
    		case "refund":
    			detachedCriteria.add(Property.forName("status").eq("08"));
    		}
    		
    		List<TicketIndent> totalIndents = ticketIndentService.findByCriteria(detachedCriteria);
    		for (TicketIndent ticketIndent : totalIndents) {
    			JSONInBill bill = new JSONInBill();
    			switch (type) {
    			case "payed":
    				bill.setPayMoney(ticketIndent.getPayFee());
    				bill.setRefundMoney(0.00);
					bill.setRemainMoney(ticketIndent.getPayFee());
    				break;
    				
    			case "refund":
    				bill.setPayMoney(ticketIndent.getPayFee());
    				bill.setRefundMoney(ticketIndent.getPayFee());
					bill.setRemainMoney(0.00);
					break;
				
    			case "all":
    				if (ticketIndent.getStatus().equals("01")) {
    					bill.setPayMoney(ticketIndent.getPayFee());
    					bill.setRefundMoney(0.00);
    					bill.setRemainMoney(ticketIndent.getPayFee());
    				}
    				
    				if (ticketIndent.getStatus().equals("08")) {
    					bill.setPayMoney(ticketIndent.getPayFee());
    					bill.setRefundMoney(ticketIndent.getPayFee());
    					bill.setRemainMoney(0.00);
    				}
    				break;
    			}
    				
				bill.setPayTime(ticketIndent.getBookDate());
				bill.setIndentId(ticketIndent.getIndentId());
				switch (ticketIndent.getCustomerType()) {
				case "01":
					bill.setIndentSource("旅行社购票");
					break;
							
				case "02":
					bill.setIndentSource("游客购票");
					break;
				}
	   			
				jsonBills.add(bill);
    		}
			
			detachedCriteria = DetachedCriteria.forClass(BaseRechargeRec.class);
			detachedCriteria.add(Property.forName("endDate").ge(start));
			detachedCriteria.add(Property.forName("endDate").le(end));
			detachedCriteria.add(Property.forName("paymentType").eq("00"));
			
			List<BaseRechargeRec> baseRechargeRecs = baseRechargeRecService.findByCriteria(detachedCriteria);
			if (baseRechargeRecs != null) {
				for (int i = 0; i < baseRechargeRecs.size(); i++) {
					BaseRechargeRec baseRechargeRec = baseRechargeRecs.get(i);
					JSONInBill bill = new JSONInBill();
					bill.setIndentId(String.valueOf(baseRechargeRec.getBackRechargeId()));
					bill.setIndentSource("旅行社充值");
					bill.setPayTime(baseRechargeRec.getEndDate());
					bill.setPayMoney(baseRechargeRec.getCashMoney());
					bill.setRefundMoney(0.00);
					bill.setRemainMoney(baseRechargeRec.getCashMoney());
						
					jsonBills.add(bill);
				}
			}	
    		
    		response.setContentType("application/vnd.ms-excel");
    		response.setHeader("Content-Disposition", "attachement; filename=outbill.xls");
    		
    		HSSFWorkbook workbook = new HSSFWorkbook();
    		HSSFSheet sheet = workbook.createSheet("new sheet");
			HSSFRow title = sheet.createRow(0);
			title.createCell(0).setCellValue("序号");
			title.createCell(1).setCellValue("订单号");
			title.createCell(2).setCellValue("订单来源");
			title.createCell(3).setCellValue("支付时间");
			title.createCell(4).setCellValue("支付金额");
			title.createCell(5).setCellValue("分账金额");
			title.createCell(6).setCellValue("分账回退金额");
			title.createCell(7).setCellValue("退款金额");
			title.createCell(8).setCellValue("订单剩余金额");    		
    		for (int i = 1; i <= jsonBills.size(); i++) {
    			HSSFRow row = sheet.createRow(i);
    			row.createCell(0).setCellValue(i);
    			row.createCell(0).setCellValue(jsonBills.get(i - 1).getIndentId());
    			row.createCell(1).setCellValue(jsonBills.get(i - 1).getIndentSource());
    			row.createCell(2).setCellValue(jsonBills.get(i - 1).getPayTime());
    			row.createCell(3).setCellValue(jsonBills.get(i - 1).getPayMoney());
    			row.createCell(3).setCellValue(jsonBills.get(i - 1).getDepartMoney());
    			row.createCell(3).setCellValue(jsonBills.get(i - 1).getDepartRefundMoney());
    			row.createCell(3).setCellValue(jsonBills.get(i - 1).getRefundMoney());
    			row.createCell(3).setCellValue(jsonBills.get(i - 1).getRemainMoney());
    		}
    		
    		workbook.write(response.getOutputStream());
    		workbook.close();
    	}
    	
    	catch(IOException e) //错误处理
    	{
    		PrintWriter toClient = response.getWriter(); //得到向客户端输出文本的对象
    		response.setContentType("text/html;charset=gb2312");
    		toClient.write("无法生成excel!");
    		toClient.close();
    	} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
