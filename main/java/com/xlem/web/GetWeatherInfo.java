package com.xlem.web;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.xlem.utils.FileHelper;
import com.xlem.utils.JsonReader;

@Component
public class GetWeatherInfo {
	@Autowired
	private HttpSession session;	
	
	@Autowired
	FileHelper fileHelper;
	
	private String weatherStatusUrl = "http://api.avatardata.cn/WeatherWord/IconList?key=405a5df1142643179cc514686ac7facd";
	private String weatherUrl = "http://api.avatardata.cn/WeatherWord/City?key=405a5df1142643179cc514686ac7facd&city=chengdu";
	
    public GetWeatherInfo() {
    }
    
    /* 初始化天气状态名称与图标 */
    private void init() throws JSONException, IOException {
    	JSONObject json = JsonReader.readJsonFromUrl(weatherStatusUrl);
    	if (json.getInt("error_code") == 0) {
    		JSONArray jsons = json.getJSONArray("result");
    		for (int i = 0; i < jsons.length(); i++) {
    			String icon = jsons.getJSONObject(i).getString("icon_url");
    			String filePath = "/images/weather/" + jsons.getJSONObject(i).getString("code") + ".png";
    			String realPath  = session.getServletContext().getRealPath(filePath);
    			File file = new File(realPath);
    			if (!file.exists()) {
    				fileHelper.getUrlContent(icon, filePath);
    			}
    			
    		}
    	}
    	else {
    		String error = json.getString("reason");
    		System.out.println(error);
    		return;
    	}
    }
    
    /* 获取天气信息 */
    public JSONArray getWeather() throws JSONException, IOException {
    	init();
    	
        String result = null;
        JSONArray results = new JSONArray();
        try {
        	JSONObject json = JsonReader.readJsonFromUrl(weatherUrl);
        	if (json.getInt("error_code") == 0) {
        		JSONObject weather = json.getJSONObject("result");
        		JSONArray weathers = weather.getJSONArray("daily_forecast");
        		for (int i = 0; i < weathers.length(); i++) {
        			if (i > 2) {
        				break;
        			}
        			
                    String daySuffix = "";
                    switch (i) {
                    case 0:
                    	daySuffix = "今天";
                    	break;
                    case 1:
                        daySuffix = "明天";
                        break;
                    case 2:
                    	daySuffix = "后天";
                    	break;
                    }
                    
                    JSONObject obj = new JSONObject();
                    obj.put("date", weathers.getJSONObject(i).getString("date") + "(" + daySuffix + ")");
                    obj.put("code_d", weathers.getJSONObject(i).getJSONObject("cond").getString("code_d"));
                    obj.put("code_n", weathers.getJSONObject(i).getJSONObject("cond").getString("code_n"));
                    obj.put("txt_d", "白天：" + weathers.getJSONObject(i).getJSONObject("cond").getString("txt_d"));
                    obj.put("txt_n", "夜间：" + weathers.getJSONObject(i).getJSONObject("cond").getString("txt_n"));
                    obj.put("temp", weathers.getJSONObject(i).getJSONObject("tmp").getString("min") + "~" 
                    				+ weathers.getJSONObject(i).getJSONObject("tmp").getString("max") + "'C");
                    obj.put("wind", weathers.getJSONObject(i).getJSONObject("wind").getString("dir") + weathers.getJSONObject(i).getJSONObject("wind").getString("sc"));
                    results.put(obj);
                }
        	}
        	else {
        		String error = json.getString("reason");
        		System.out.println(error);
        		return null;
        	}
        	
       } catch (Exception e) {
            e.printStackTrace();
        }
        return results;
    }
   
    public Document stringToElement(InputStream input) {
        try {
            DocumentBuilder db = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder();
            Document doc = db.parse(input);
            return doc;
        } catch (Exception e) {
            return null;
        }
    }
}

/*public class Test {
    public static void main(String arg[]) {
        GetWeatherInfo info = new GetWeatherInfo();
        String weather = info.getWeather("武汉");
        System.out.println(weather);
    }
} */
