package com.xlem.web;

import java.util.ArrayList;
import java.util.List;

public class JSONFinanceStat {
	private String label;
	private List<Integer> datas;
	private List<Double> amounts;
	
	public String getLabel() {
		return this.label;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}
	
	public List<Integer> getDatas() {
		return this.datas;
	}
	
	public void setDatas(List<Integer> datas) {
		this.datas = datas;
	}
	
	public List<Double> getAmounts() {
		return this.amounts;
	}
	
	public void setAmounts(List<Double> amounts) {
		this.amounts = amounts;
	}
}
