package com.xlem.web;

import java.util.List;

public class JSONNotices {
	public Integer num;
	public List<JSONNotice> deployedNotices;
	
	public Integer getNum() {
		return this.num;
	}
	
	public void setNum(Integer num) {
		this.num = num;
	}
	
	public List<JSONNotice> getDeployedNotices() {
		return this.deployedNotices;
	}
	
	public void setDeployedNotices(List<JSONNotice> deployedNotices) {
		this.deployedNotices = deployedNotices;
	}
}
