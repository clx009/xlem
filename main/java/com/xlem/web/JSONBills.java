package com.xlem.web;

import java.util.List;

public class JSONBills {
	private Integer num;
	private List<JSONBill> transRec;
	
	public Integer getNum() {
		return this.num;
	}
	
	public void setNum(Integer num) {
		this.num = num;
	}
	
	public List<JSONBill> getTransRec() {
		return this.transRec;
	}
	
	public void setTransRec(List<JSONBill> transRec) {
		this.transRec = transRec;
	}
}
