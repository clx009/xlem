package com.xlem.web;

import java.util.List;

public class JSONFinanceReports {
	private Integer num;
	private List<JSONFinanceReport> reports;
	
	public Integer getNum () {
		return this.num;
	}
	
	public void setNum (Integer num) {
		this.num = num;
	}
	
	public List<JSONFinanceReport> getReports () {
		return this.reports;
	}
	
	public void setReports (List<JSONFinanceReport> reports) {
		this.reports = reports;
	}
}
