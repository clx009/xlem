package com.xlem.web;

public class JSONFinanceReport {
	private String source;
	private String item;
	private Double prevRemain;
	private Double curPay;
	private Double curConsume;
	private Double curRemain;
	
	public String getSource () {
		return this.source;
	}
	
	public void setSource (String source) {
		this.source = source;
	}
	
	public String getItem () {
		return this.item;
	}
	
	public void setItem (String item) {
		this.item = item;
	}
	
	public Double getPrevRemain () {
		return this.prevRemain;
	}
	
	public void setPrevRemain (Double prevRemain) {
		this.prevRemain = prevRemain;
	}
	
	public Double getCurPay () {
		return this.curPay;
	}
	
	public void setCurPay (Double curPay) {
		this.curPay = curPay;
	}
	
	public Double getCurConsume () {
		return this.curConsume;
	}
	
	public void setCurConsume (Double curConsume) {
		this.curConsume = curConsume;
	}
	
	public Double getCurRemain () {
		return this.curRemain;
	}
	
	public void setCurRemain (Double curRemain) {
		this.curRemain = curRemain;
	}

}
