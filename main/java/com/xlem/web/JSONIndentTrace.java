package com.xlem.web;

import java.util.Date;

public class JSONIndentTrace {
	private String userId;
	private String indentId;
	private String orderId;
	private Date platFormTime;
	private String platFormStatus;
	private Date machineTime;
	private String machineStatus;
	private Date gateGuardTime;
	private String gateGuardStatus;
	
	public String getUserId() {
		return this.userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getIndentId() {
		return this.indentId;
	}
	
	public void setIndentId(String indentId) {
		this.indentId = indentId;
	}
	
	public String getOrderId() {
		return this.orderId;
	}
	
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
	public Date getPlatFormTime() {
		return this.platFormTime;
	}
	
	public void setPlatFormTime(Date platFormTime) {
		this.platFormTime = platFormTime;
	}
	
	public String getPlatFormStatus() {
		return this.platFormStatus;
	}
	
	public void setPlatFormStatus(String platFormStatus) {
		this.platFormStatus = platFormStatus;
	}
	
	public Date getMachineTime() {
		return this.machineTime;
	}
	
	public void setMachineTime(Date machineTime) {
		this.machineTime = machineTime;
	}
	
	public String getMachineStatus() {
		return this.machineStatus;
	}
	
	public void setMachineStatus(String machineStatus) {
		this.machineStatus = machineStatus;
	}

	public Date getGateGuardTime() {
		return this.gateGuardTime;
	}
	
	public void setGateGuardTime(Date gateGuardTime) {
		this.gateGuardTime = gateGuardTime;
	}
	
	public String getGateGuardStatus() {
		return this.gateGuardStatus;
	}
	
	public void setGateGuardStatus(String gateGuardStatus) {
		this.gateGuardStatus = gateGuardStatus;
	}
}
