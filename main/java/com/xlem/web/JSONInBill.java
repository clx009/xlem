package com.xlem.web;

import java.util.Date;

public class JSONInBill {
	private String indentId;
	private String indentSource;
	private Date payTime;
	private Double payMoney;
	private Double departMoney;
	private Double departRefundMoney;
	private Double refundMoney;
	private Double adjustMoney;
	private Double remainMoney;
	
	public String getIndentId() {
		return this.indentId;
	}
	
	public void setIndentId(String indentId) {
		this.indentId = indentId;
	}
	
	public String getIndentSource() {
		return this.indentSource;
	}
	
	public void setIndentSource(String indentSource) {
		this.indentSource = indentSource;
	}
	
	public Date getPayTime() {
		return this.payTime;
	}
	
	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}
	
	public Double getPayMoney() {
		return this.payMoney;
	}
	
	public void setPayMoney(Double payMoney) {
		this.payMoney = payMoney;
	}
	
	public Double getDepartMoney() {
		return this.departMoney;
	}
	
	public void setDepartMoney(Double departMoney) {
		this.departMoney = departMoney;
	}
	
	public Double getDepartRefundMoney() {
		return this.departRefundMoney;
	}
	
	public void setDepartRefundMoney(Double departRefundMoney) {
		this.departRefundMoney = departRefundMoney;
	}
	
	public Double getRefundMoney() {
		return this.refundMoney;
	}
	
	public void setRefundMoney(Double refundMoney) {
		this.refundMoney = refundMoney;
	}
	
	public Double getAdjustMoney() {
		return this.adjustMoney;
	}
	
	public void setAdjustMoney(Double adjustMoney) {
		this.adjustMoney = adjustMoney;
	}
	
	public Double getRemainMoney() {
		return this.remainMoney;
	}
	
	public void setRemainMoney(Double remainMoney) {
		this.remainMoney = remainMoney;
	}
}
