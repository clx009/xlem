package com.xlem.web;

import java.util.Locale;

import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;

public class MyViewResolver implements ViewResolver {

    private ViewResolver tilesResolver;
    private ViewResolver jspResolver;

    public void setJspResolver(ViewResolver jspResolver) {
        this.jspResolver = jspResolver;
    }

    public void setTilesResolver(ViewResolver tilesResolver) {
        this.tilesResolver = tilesResolver;
    }

    @Override
    public View resolveViewName(String viewName, Locale locale) throws Exception {
        if (isTilesView(viewName)) {
            return tilesResolver.resolveViewName(viewName, locale);
        } else {
            return jspResolver.resolveViewName(viewName, locale);
        }
    }

    private boolean isTilesView(String viewName) {
    	String tileViews[] = {"manage/init",
    						  "manage/employee_create",
    						  "manage/depart_list",
    						  "manage/depart_create",
    						  "manage/depart_update",
    						  "manage/depart_show",
    						  "manage/employee_list",
    						  "manage/employee_create",
    						  "manage/employee_update",
    						  "manage/employee_show",
    						  "register/init",
    						  "register/person",
    						  "register/company1",
    						  "register/company2",
    						  "register/company3",
    						  "register/company4",
    						  "register/company5",
    						  "login/guest",
    						  "login/manage",
    						  "login/market",
    						  "login/finance",
    						  "login/travservice",
    						  "login/login",
    						  "login/myaccount_login",
    						  "login/forgetpwd",
    						  "login/resetpwd",
    						  "login/depart",
    						  "account/tourist_init",
    						  "account/tourist_info",
    						  "account/tourist_passwd",
    						  "account/tourist_ticket_order_list",
    						  "account/tourist_coupon_list",
    						  "account/tourist_point",
    						  "market/init",
    						  "market/auditTravService",
    						  "market/auditedTravService",
    						  "market/viewTravService",
    						  "market/auditShowImage",
    						  "market/performTsAudit",
    						  "market/discountSetting",
    						  "market/performDiscountSetting",
    						  "market/reward",
    						  "market/performReward",
    						  "market/auditTicketIndent",
    						  "market/viewTicketIndent",
    						  "market/ticketResCtrl",
    						  "market/deployMessage",
    						  "market/deployedMsgList",
    						  "market/modifyNotice",
    						  "ts_account/ts_init",
    						  "ts_account/ts_info",
    						  "ts_account/tsShowImage",
    						  "ts_account/ts_passwd",
    						  "ts_account/ts_ticket_order_list",
    						  "ts_account/ts_rewardAccount",
    						  "ts_account/ts_reward_list",
    						  "ts_account/ts_cashAccount",
    						  "ts_account/ts_cash_list",
    						  "ts_account/ts_cashRecharge",
    						  "finance/init",
    						  "finance/eOutBill",
    						  "finance/summaryOutBill",
    						  "finance/eInBill",
    						  "finance/IndentStatistic",
    						  "finance/IndentDownload",
    						  "finance/monthFinanceReportSelect",
    						  "finance/yearFinanceReportSelect",
    						  "finance/financeReport",
    						  "finance/reportNotGen",
    						  "finance/RealTimePlot",
    						  "finance/yearStatChart",
    						  "finance/monthStatChart",
    						  "finance/dayStatChart",
    						  "finance/indentTrace"};
    	
    	for (int i = 0; i < tileViews.length; i++) {
    		if (viewName.equals(tileViews[i])) {
    			return true;
    		}
    	}
    	
    	return false;
    }
}
