package com.xlem.web;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.BaseTranRec;
import com.xlem.dao.Travservice;
import com.xlem.dao.User;

import service.comm.baseRechargeRec.BaseRechargeRecService;
import service.comm.baseTranRec.BaseTranRecService;
import service.comm.reg.baseTravelAgency.BaseTravelAgencyService;

public class OutBillExcelGenerator extends HttpServlet {
	@Autowired
	private BaseTranRecService baseTranRecService;
	@Autowired
	private BaseTravelAgencyService baseTravelAgencyService;
	@Autowired
	private BaseRechargeRecService baseRechargeRecService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	try{
    		String timeType = request.getParameter("timeType");
    		String startDate = request.getParameter("startDate");
    		String endDate = request.getParameter("endDate"); 
    		String state = request.getParameter("state");
    		String keyword = request.getParameter("keyword");
    		String keyValue	= request.getParameter("keyValue");
    		String lessMoney = request.getParameter("lessMoney");
    		String moreMoney = request.getParameter("moreMoney");
    		String cashDir = request.getParameter("cashDir");
    		String alipay = request.getParameter("alipay"); 
    		String other = request.getParameter("other");
    		String tradeClass = request.getParameter("tradeClass"); 
    		
    		int allRecords = 0;
    		JSONBills result = new JSONBills();
    		List<JSONBill> jsonBills = new ArrayList<JSONBill>();
    		
    		switch (tradeClass) {
    		case "00":
    			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(BaseTranRec.class);
    			
    			DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
    			Date start = fmt.parse(startDate);
    			Date end = fmt.parse(endDate);
    			detachedCriteria.add(Property.forName("paymentTime").ge(start));
    			detachedCriteria.add(Property.forName("paymentTime").le(end));
    			
    			if (!keyValue.equals("empty")) {
    				detachedCriteria.add(Property.forName("tranRecId").eq(keyValue));
    			}
    			
    			if (!lessMoney.equals("minus-infinite")) {
    				detachedCriteria.add(Property.forName("payFee").ge(Double.valueOf(lessMoney)));
    			}
    			
    			if (!moreMoney.equals("infinite")) {
    				detachedCriteria.add(Property.forName("payFee").le(Double.valueOf(moreMoney)));
    			}
    			
    			switch (cashDir) {
    			case "01":
    				detachedCriteria.add(Property.forName("payFee").ge(0.00));
    				break;
    				
    			case "02":
    				detachedCriteria.add(Property.forName("payFee").le(0.00));
    				break;
    			}
    			
    			List<BaseTranRec> baseTranRecs = baseTranRecService.findByCriteria(detachedCriteria);
    			if (baseTranRecs != null) {
    				allRecords = baseTranRecs.size();
    				for (int i = 0; i < allRecords; i++) {
    					BaseTranRec baseTranRec = baseTranRecs.get(i);
    					JSONBill jsonBill = new JSONBill();
    					jsonBill.setDate(baseTranRec.getPaymentTime());
    					jsonBill.setName(baseTranRec.getTicketIndentDetail().getTicketPrice().getTicketType().getTicketName());
    					jsonBill.setIndentId(baseTranRec.getTicketIndent().getIndentId());
    					jsonBill.setTransRecId(baseTranRec.getTranRecId());
    					
    					switch (baseTranRec.getTicketIndent().getCustomerType()) {
    					case "01": {
    						User user = baseTranRec.getTicketIndent().getUser();
    						DetachedCriteria criteria = DetachedCriteria.forClass(Travservice.class);
    						criteria.add(Property.forName("user").eq(user));
    						List<Travservice> travservices = baseTravelAgencyService.findByCriteria(criteria);
    						Travservice travservice = travservices.get(0);
    						jsonBill.setOtherSide(travservice.getName());
    						if (baseTranRec.getMoney() > 0.00)
    							jsonBill.setIndentSource("旅行社购票");
    						else
    							jsonBill.setIndentSource("旅行社退票");
    						break;
    					}
    						
    					case "02": {
    						User user = baseTranRec.getTicketIndent().getUser();
    						jsonBill.setOtherSide(user.getName());
    						if (baseTranRec.getMoney() > 0.00)
    							jsonBill.setIndentSource("游客购票");
    						else
    							jsonBill.setIndentSource("游客退票");
    						}
    					}
    					
    					jsonBill.setMoney(baseTranRec.getMoney());
    					jsonBill.setStatus("已完成");
    					
    					jsonBills.add(jsonBill);
    				}
    			}
    			
    			detachedCriteria = DetachedCriteria.forClass(BaseRechargeRec.class);
    			
    			start = fmt.parse(startDate);
    			end = fmt.parse(endDate);
    			detachedCriteria.add(Property.forName("endDate").ge(start));
    			detachedCriteria.add(Property.forName("endDate").le(end));
    			
    			if (!keyValue.equals("empty")) {
    				detachedCriteria.add(Property.forName("rechargeRecId").eq(keyValue));
    			}
    			
    			if (!lessMoney.equals("minus-infinite")) {
    				detachedCriteria.add(Property.forName("cashMoney").ge(lessMoney));
    			}
    			
    			if (!moreMoney.equals("infinite")) {
    				detachedCriteria.add(Property.forName("cashMoney").le(moreMoney));
    			}
    			
    			detachedCriteria.add(Property.forName("paymentType").eq("00"));
    			
    			List<BaseRechargeRec> baseRechargeRecs = baseRechargeRecService.findByCriteria(detachedCriteria);
    			if (baseRechargeRecs != null) {
    				allRecords += baseRechargeRecs.size();
    				for (int i = 0; i < baseRechargeRecs.size(); i++) {
    					BaseRechargeRec baseRechargeRec = baseRechargeRecs.get(i);
    					JSONBill jsonBill = new JSONBill();
    					jsonBill.setDate(baseRechargeRec.getEndDate());
    					jsonBill.setName("旅行社充值");
    					jsonBill.setIndentId(String.valueOf(baseRechargeRec.getRechargeRecId()));
    					jsonBill.setTransRecId(String.valueOf(baseRechargeRec.getRechargeRecId()));
    					
    					Travservice travservice = baseRechargeRec.getBaseTravelAgency();
    					jsonBill.setOtherSide(travservice.getName());
    					jsonBill.setIndentSource("旅行社充值");
    					jsonBill.setMoney(baseRechargeRec.getCashMoney());
    					jsonBill.setStatus("已完成");
    						
    					jsonBills.add(jsonBill);
    				}
    			}
    			
    			result.setNum(allRecords);
    			result.setTransRec(jsonBills);	
    			
    			break;
    		case "01":
    			detachedCriteria = DetachedCriteria.forClass(BaseTranRec.class);
    		
    			fmt = new SimpleDateFormat("yyyy-MM-dd");
    			start = fmt.parse(startDate);
    			end = fmt.parse(endDate);
    			detachedCriteria.add(Property.forName("paymentTime").ge(start));
    			detachedCriteria.add(Property.forName("paymentTime").le(end));
    			
    			if (!keyValue.equals("empty")) {
    				detachedCriteria.add(Property.forName("tranRecId").eq(keyValue));
    			}
    			
    			if (!lessMoney.equals("minus-infinite")) {
    				detachedCriteria.add(Property.forName("payFee").ge(lessMoney));
    			}
    			
    			if (!moreMoney.equals("infinite")) {
    				detachedCriteria.add(Property.forName("payFee").le(moreMoney));
    			}
    			
    			switch (cashDir) {
    			case "01":
    				detachedCriteria.add(Property.forName("payFee").ge(0.00));
    				break;
    				
    			case "02":
    				detachedCriteria.add(Property.forName("payFee").le(0.00));
    				break;
    			}
    			
    			baseTranRecs = baseTranRecService.findByCriteria(detachedCriteria);
    			if (baseTranRecs != null) {
    				allRecords = baseTranRecs.size();
    				for (int i = 0; i < allRecords; i++) {
    					BaseTranRec baseTranRec = baseTranRecs.get(i);
    					JSONBill jsonBill = new JSONBill();
    					jsonBill.setDate(baseTranRec.getPaymentTime());
    					jsonBill.setName(baseTranRec.getTicketIndentDetail().getTicketPrice().getTicketType().getTicketName());
    					jsonBill.setIndentId(baseTranRec.getTicketIndent().getIndentId());
    					jsonBill.setTransRecId(baseTranRec.getBackTranId());
    					
    					switch (baseTranRec.getTicketIndent().getCustomerType()) {
    					case "01": {
    						User user = baseTranRec.getTicketIndent().getUser();
    						DetachedCriteria criteria = DetachedCriteria.forClass(Travservice.class);
    						criteria.add(Property.forName("user").eq(user));
    						List<Travservice> travservices = baseTravelAgencyService.findByCriteria(criteria);
    						Travservice travservice = travservices.get(0);
    						jsonBill.setOtherSide(travservice.getName());
    						jsonBill.setIndentSource("旅行社购票");
    						break;
    					}
    						
    					case "02": {
    						User user = baseTranRec.getTicketIndent().getUser();
    						jsonBill.setOtherSide(user.getName());
    						jsonBill.setIndentSource("游客购票");
    					}
    					}
    					
    					jsonBill.setMoney(baseTranRec.getMoney());
    					jsonBill.setStatus("已完成");
    					
    					jsonBills.add(jsonBill);
    				}
    			}
    			
    			result.setNum(allRecords);
    			result.setTransRec(jsonBills);	
    			break;
    			
    		case "02":
    			detachedCriteria = DetachedCriteria.forClass(BaseRechargeRec.class);
    		
    			fmt = new SimpleDateFormat("yyyy-MM-dd");
    			start = fmt.parse(startDate);
    			end = fmt.parse(endDate);
    			detachedCriteria.add(Property.forName("endDate").ge(start));
    			detachedCriteria.add(Property.forName("endDate").le(end));
    			
    			if (!keyValue.equals("empty")) {
    				detachedCriteria.add(Property.forName("rechargeRecId").eq(keyValue));
    			}
    			
    			if (!lessMoney.equals("minus-infinite")) {
    				detachedCriteria.add(Property.forName("cashMoney").ge(lessMoney));
    			}
    			
    			if (!moreMoney.equals("infinite")) {
    				detachedCriteria.add(Property.forName("cashMoney").le(moreMoney));
    			}
    			
    			detachedCriteria.add(Property.forName("paymentType").eq("00"));
    			
    			baseRechargeRecs = baseRechargeRecService.findByCriteria(detachedCriteria);
    			if (baseRechargeRecs != null) {
    				allRecords += baseRechargeRecs.size();
    				for (int i = 0; i < baseRechargeRecs.size(); i++) {
    					BaseRechargeRec baseRechargeRec = baseRechargeRecs.get(i);
    					JSONBill jsonBill = new JSONBill();
    					jsonBill.setDate(baseRechargeRec.getEndDate());
    					jsonBill.setName("旅行社充值");
    					jsonBill.setIndentId(String.valueOf(baseRechargeRec.getBackRechargeId()));
    					jsonBill.setTransRecId(String.valueOf(baseRechargeRec.getBackRechargeId()));
    					
    					Travservice travservice = baseRechargeRec.getBaseTravelAgency();
    					jsonBill.setOtherSide(travservice.getName());
    					jsonBill.setIndentSource("旅行社充值");
    					jsonBill.setMoney(baseRechargeRec.getCashMoney());
    					jsonBill.setStatus("已完成");
    						
    					jsonBills.add(jsonBill);
    				}
    			}
    			
    			result.setNum(allRecords);
    			result.setTransRec(jsonBills);	
    			break;
    		}
    		
    		response.setContentType("application/vnd.ms-excel");
    		response.setHeader("Content-Disposition", "attachement; filename=outbill.xls");
    		
    		HSSFWorkbook workbook = new HSSFWorkbook();
    		HSSFSheet sheet = workbook.createSheet("new sheet");
    		for (int i = 0; i < result.getNum(); i++) {
    			HSSFRow row = sheet.createRow(i);
    			row.createCell(0).setCellValue(result.getTransRec().get(i).getDate());
    			row.createCell(1).setCellValue(result.getTransRec().get(i).getName());
    			row.createCell(2).setCellValue(result.getTransRec().get(i).getIndentId());
    			row.createCell(3).setCellValue(result.getTransRec().get(i).getTransRecId());
    			row.createCell(4).setCellValue(result.getTransRec().get(i).getOtherSide());
    			row.createCell(5).setCellValue(result.getTransRec().get(i).getIndentSource());
    			row.createCell(6).setCellValue(result.getTransRec().get(i).getMoney());
    			row.createCell(7).setCellValue(result.getTransRec().get(i).getMoneyDetail());
    			row.createCell(8).setCellValue(result.getTransRec().get(i).getStatus());
    		}
    		
    		workbook.write(response.getOutputStream());
    		workbook.close();
    	}
    	catch(IOException | ParseException e) //错误处理
    	{
    		PrintWriter toClient = response.getWriter(); //得到向客户端输出文本的对象
    		response.setContentType("text/html;charset=gb2312");
    		toClient.write("无法生成excel!");
    		toClient.close();
    	}
    }
}
