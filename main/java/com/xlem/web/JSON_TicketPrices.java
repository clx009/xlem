package com.xlem.web;

import java.util.List;

public class JSON_TicketPrices {
	private Integer num;
	private List<JSONTicketPrice> ticketPrices;
	
	public Integer getNum() {
		return this.num;
	}
	
	public void setNum(Integer num) {
		this.num = num;
	}
	
	public List<JSONTicketPrice> getTicketPrices() {
		return this.ticketPrices;
	}
	
	public void setTicketPrices(List<JSONTicketPrice> ticketPrices) {
		this.ticketPrices = ticketPrices;
	}
}
