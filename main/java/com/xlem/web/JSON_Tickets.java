package com.xlem.web;

import java.util.List;

public class JSON_Tickets {
	private Integer num;
	private List<JSONTicket> tickets;
	
	public Integer getNum() {
		return this.num;
	}
	
	public void setNum(Integer num) {
		this.num = num;
	}
	
	public List<JSONTicket> getTickets() {
		return this.tickets;
	}
	
	public void setTickets(List<JSONTicket> tickets) {
		this.tickets = tickets;
	}
	
}
