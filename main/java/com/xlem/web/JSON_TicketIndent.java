package com.xlem.web;

import java.util.Date;
import java.util.List;

public class JSON_TicketIndent {
	private String indentId;
	private Date bookDate;
	private List<JSON_TicketIndentDetail> details;
	private String status;
	private String travelAgencyName;
	
	public String getIndentId() {
		return this.indentId;
	}
	
	public void setIndentId(String indentId) {
		this.indentId = indentId;
	}
	
	public Date getBookDate() {
		return this.bookDate;
	}
	
	public void setBookDate(Date bookDate) {
		this.bookDate = bookDate;
	}
	
	public List<JSON_TicketIndentDetail> getDetails() {
		return this.details;
	}
	
	public void setDetails(List<JSON_TicketIndentDetail> details) {
		this.details = details;
	}
	
	public String getStatus() {
		return this.status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getTravelAgencyName() {
		return this.travelAgencyName;
	}

	public void setTravelAgencyName(String travelAgencyName) {
		this.travelAgencyName = travelAgencyName;
	}
}
