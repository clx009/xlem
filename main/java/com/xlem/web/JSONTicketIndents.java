package com.xlem.web;

import java.util.List;

public class JSONTicketIndents {
	private Integer num;
	private List<JSON_TicketIndent> ticketIndents;
	
	public Integer getNum() {
		return this.num;
	}
	
	public void setNum(Integer num) {
		this.num = num;
	}
	
	public List<JSON_TicketIndent> getTicketIndents() {
		return this.ticketIndents;
	}
	
	public void setTicketIndents(List<JSON_TicketIndent> ticketIndents) {
		this.ticketIndents = ticketIndents;
	}
}
