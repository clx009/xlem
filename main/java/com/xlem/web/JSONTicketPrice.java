package com.xlem.web;

public class JSONTicketPrice {
	private Long ticketPriceId;
	private String ticketName;
	private Double unitPrice;
	private Integer toplimit;
	private Integer remainToplimit;
		
	public Long getTicketPriceId() {
		return this.ticketPriceId;
	}
	
	public void setTicketPriceId(Long ticketPriceId) {
		this.ticketPriceId = ticketPriceId;
	}
	
	public String getTicketName() {
		return this.ticketName;
	}
	
	public void setTicketName(String ticketName) {
		this.ticketName = ticketName;
	}
	
	public Double getUnitPrice() {
		return this.unitPrice;
	}
	
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	public Integer getTopLimit() {
		return this.toplimit;
	}
	
	public void setTopLimit(Integer toplimit) {
		this.toplimit = toplimit;
	}
	
	public Integer getRemainToplimit() {
		return this.remainToplimit;
	}
	
	public void setRemainToplimit(Integer remainToplimit) {
		this.remainToplimit = remainToplimit;
	}
}
