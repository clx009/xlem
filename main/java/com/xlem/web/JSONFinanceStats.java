package com.xlem.web;

import java.util.List;

public class JSONFinanceStats {
	private Integer num;
	private List<JSONFinanceStat> statDatas;
	
	public Integer getNum() {
		return this.num;
	}
	
	public void setNum(Integer num) {
		this.num = num;
	}
	
	public List<JSONFinanceStat> getStatDatas() {
		return this.statDatas;
	}
	
	public void setStatDatas(List<JSONFinanceStat> statDatas) {
		this.statDatas = statDatas;
	}
}
