package com.xlem.web;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import com.xlem.utils.SystemSetting;

public class TicketShow extends HttpServlet {
	// 设置图形验证码中字符串的字体和大小
    private Font myFont = new Font("宋体", Font.PLAIN, 16);

    @Override
    public void init() throws ServletException {
        super.init();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	try{
    		String filename = request.getParameter("image");
    		String ticketName = request.getParameter("name");
    		String ticketPrice = request.getParameter("price");
    		String path = "/images/" + filename;
    		String realPath  = request.getSession().getServletContext().getRealPath(path);
    		FileInputStream hFile = new FileInputStream(realPath); // 以byte流的方式打开文件
    		int size = hFile.available(); //得到文件大小
    		byte data[] = new byte[size];
    		hFile.read(data); //读数据
    		hFile.close();
    		
            response.setHeader("Pragma", "No-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expires", 0);
            response.setContentType("image/jpeg");    		
    		
            ByteArrayInputStream in = new ByteArrayInputStream(data);    
    		BufferedImage image = ImageIO.read(in); 

    		// 在图片中绘制内容
            Graphics g = image.getGraphics();
            g.setFont(myFont);
           
            g.drawString(ticketName, 100, 266);
            g.drawString(ticketPrice, 100, 286);
            
           /* // 取得用户Session
            HttpSession session = request.getSession(true);
            // 将系统生成的图形验证码添加
            session.setAttribute("rand", sRand);
            g.dispose(); */
            
            // 输出图形验证码图片
            ImageIO.write(image, "JPEG", response.getOutputStream());
         		
    		/*response.setContentType("image/*"); //设置返回的文件类型
    		OutputStream toClient=response.getOutputStream(); //得到向客户端输出二进制数据的对象
    		toClient.write(data); //输出数据
    		toClient.close();*/
    	}
    	catch(IOException e) //错误处理
    	{
    		PrintWriter toClient = response.getWriter(); //得到向客户端输出文本的对象
    		response.setContentType("text/html;charset=utf-8");
    		toClient.write("无法打开图片!");
    		toClient.close();
    	}
    }
}
