package com.xlem.web;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ShowImage extends HttpServlet {

    @Override
    public void init() throws ServletException {
        super.init();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	try{
    		String filename = request.getParameter("filename");
    		String path = "/images/" + filename;
    		String realPath  = request.getSession().getServletContext().getRealPath(path);
    		FileInputStream hFile = new FileInputStream(realPath); // 以byte流的方式打开文件
    		int size = hFile.available(); //得到文件大小
    		byte data[] = new byte[size];
    		hFile.read(data); //读数据
    		hFile.close();
    		response.setContentType("image/*"); //设置返回的文件类型
    		OutputStream toClient=response.getOutputStream(); //得到向客户端输出二进制数据的对象
    		toClient.write(data); //输出数据
    		toClient.close();
    	}
    	catch(IOException e) //错误处理
    	{
    		PrintWriter toClient = response.getWriter(); //得到向客户端输出文本的对象
    		response.setContentType("text/html;charset=gb2312");
    		toClient.write("无法打开图片!");
    		toClient.close();
    	}
    }
}
