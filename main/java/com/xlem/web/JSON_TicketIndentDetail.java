package com.xlem.web;

import java.util.Date;

public class JSON_TicketIndentDetail {
	private String indentDetailId;
	private String ticketName;
	private JSONTicket ticket;
	private String intro;
	private Date useDate;
	private Double subtotal;
	private String status;
	
	public String getIndentDetailId() {
		return this.indentDetailId;
	}
	
	public void setIndentDetailId(String indentDetailId) {
		this.indentDetailId = indentDetailId;
	}
	
	public String getTicketName() {
		return this.ticketName;
	}
	
	public void setTicketName(String ticketName) {
		this.ticketName = ticketName;
	}
	
 	public JSONTicket getTicket() {
 		return this.ticket;
 	}
 	
 	public void setTicket(JSONTicket ticket) {
 		this.ticket = ticket;
 	}
 	
	public String getIntro() {
		return this.intro;
	}
	
	public void setIntro(String intro) {
		this.intro = intro;
	}
	
	public Date getUseDate() {
		return this.useDate;
	}
	
	public void setUseDate(Date useDate) {
		this.useDate = useDate;
	}
	
	public Double getSubtotal() {
		return this.subtotal;
	}
	
	public void setSubtotal(Double subtotal) {
		this.subtotal = subtotal;
	}
	
	public String getStatus() {
		return this.status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
}
