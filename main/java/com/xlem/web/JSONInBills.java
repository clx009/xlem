package com.xlem.web;

import java.util.List;

public class JSONInBills {
	private Integer num;
	private List<JSONInBill> transRec;
	
	public Integer getNum() {
		return this.num;
	}
	
	public void setNum(Integer num) {
		this.num = num;
	}
	
	public List<JSONInBill> getTransRec() {
		return this.transRec;
	}
	
	public void setTransRec(List<JSONInBill> transRec) {
		this.transRec = transRec;
	}
}
