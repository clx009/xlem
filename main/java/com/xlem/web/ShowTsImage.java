package com.xlem.web;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Random;
import java.util.function.Function;

import javax.imageio.ImageIO;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.xlem.dao.Travservice;

import service.comm.reg.baseTravelAgency.BaseTravelAgencyService;

public class ShowTsImage extends HttpServlet {
	@Autowired
	private BaseTravelAgencyService baseTravelAgencyService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,config.getServletContext());
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	try{
    		String Id = request.getParameter("id");
    		Long travelAgencyId = Long.parseLong(Id);
    		
    		Travservice travservice = baseTravelAgencyService.findById(travelAgencyId); 
    		
    		String functionName = request.getParameter("fname");
    		Method method = travservice.getClass().getMethod(functionName);
    		byte data[] = (byte[]) method.invoke(travservice, null);
    		
    		String scale = request.getParameter("scale");
    		if (scale == null) {
    			data = ChangeImgSize(data, 50, 50);
    		}
    		
    		response.setContentType("image/*"); //设置返回的文件类型
    		OutputStream toClient=response.getOutputStream(); //得到向客户端输出二进制数据的对象
    		toClient.write(data); //输出数据
    		toClient.close();
    	}
    	catch(IOException e) //错误处理
    	{
    		PrintWriter toClient = response.getWriter(); //得到向客户端输出文本的对象
    		response.setContentType("text/html;charset=utf-8");
    		toClient.write("无法打开图片!");
    		toClient.close();
    	} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    private byte[] ChangeImgSize(byte[] data, int nw, int nh){
        byte[] newdata = null;
        try{
             BufferedImage bis = ImageIO.read(new ByteArrayInputStream(data));
                int w = bis.getWidth();
                int h = bis.getHeight();
                double sx = (double) nw / w;
                double sy = (double) nh / h;
                AffineTransform transform = new AffineTransform();
                transform.setToScale(sx, sy);
                AffineTransformOp ato = new AffineTransformOp(transform, null);
                //原始颜色
                BufferedImage bid = new BufferedImage(nw, nh, BufferedImage.TYPE_3BYTE_BGR);
                ato.filter(bis, bid);          
                //转换成byte字节
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(bid, "jpeg", baos);
                newdata = baos.toByteArray();
        }catch(IOException e){
             e.printStackTrace();
        }
        return newdata;
        }
}
