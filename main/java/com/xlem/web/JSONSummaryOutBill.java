package com.xlem.web;

public class JSONSummaryOutBill {
	private String name;
	private Double inMoney;
	private Double outMoney;
	private Double total;
		
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Double getInMoney() {
		return this.inMoney;
	}
	
	public void setInMoney(Double inMoney) {
		this.inMoney = inMoney;
	}
	
	public Double getOutMoney() {
		return this.outMoney;
	}
	
	public void setOutMoney(Double outMoney) {
		this.outMoney = outMoney;
	}
	
	public Double getTotal() {
		return this.total;
	}
	
	public void setTotal(Double total) {
		this.total = total;
	}
}
