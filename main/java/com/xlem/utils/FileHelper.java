package com.xlem.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FileHelper {
	@Autowired
	private HttpSession session;
	
	public void getUrlContent(String url, String path) throws IOException {
		URL url2download = new URL(url);
		InputStream webIS = url2download.openStream();
		
		String realPath  = session.getServletContext().getRealPath(path);
		File file = new File(realPath);
		file.createNewFile();
		FileOutputStream fo = new FileOutputStream(file);

		int c = 0;
		do {
			c = webIS.read();
			if (c != -1) fo.write(c);
		} while (c != -1);
		
		webIS.close();
		fo.close();
	}
	
	//第二种获取文件内容方式
	public static byte[] getContent2(String filePath) throws IOException
	{
		FileInputStream in=new FileInputStream(filePath);
		
		ByteArrayOutputStream out=new ByteArrayOutputStream(1024);
		
		System.out.println("bytes available:"+in.available());
		
		byte[] temp=new byte[1024];
		
		int size=0;
		
		while((size=in.read(temp))!=-1)
		{
			out.write(temp,0,size);
		}
		
		in.close();
		
		byte[] bytes=out.toByteArray();
		System.out.println("bytes size got is:"+bytes.length);
		
		return bytes;
	}
        //将byte数组写入文件
	public static void createFile(String path, byte[] content) throws IOException {

		FileOutputStream fos = new FileOutputStream(path);

		fos.write(content);
		fos.close();
	}

}
