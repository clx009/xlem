package com.xlem.utils;

import java.util.List;

public class Page {
	private int Num;
	private List<RESTFulTicket> tickets;
	private List<String> ticketImages; 
	
	public int getNum() {
		return this.Num;
	}
	
	public void setNum(int Num) {
		this.Num = Num;
	}
	
	public List<RESTFulTicket> getTickets() {
		return this.tickets;
	}
	
	public void setTickets(List<RESTFulTicket> tickets) {
		this.tickets = tickets;
	}
	
	public List<String> getTicketImages() {
		return this.ticketImages;
	}
	
	public void setTicketImages(List<String> ticketImages) {
		this.ticketImages = ticketImages;
	}
 }
