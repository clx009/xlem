package com.xlem.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.BaseTranRec;
import com.xlem.dao.FinanceReport;
import com.xlem.dao.TicketIndent;
import com.xlem.dao.TicketIndentDetail;
import com.xlem.dao.TicketPrice;

import service.comm.baseRechargeRec.BaseRechargeRecService;
import service.comm.baseTranRec.BaseTranRecService;
import service.comm.financeReport.FinanceReportService;

@Component("financeReportBuilder")
public class FinanceReportBuilder {
	@Autowired 
	private BaseTranRecService baseTranRecService;
	@Autowired
	private FinanceReportService financeReportService;
	@Autowired
	private BaseRechargeRecService baseRechargeRecService;
	
	@Autowired
	public void init() {
		System.out.println("财务报表生成器启动");
	}	
	
	@Scheduled(cron="0 * * * * *")
	public void run() throws ParseException {
		// TODO Auto-generated method stub
		System.out.println("初始化财务报表生成");
		
		Date now = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(now);
		int year = calendar.get(Calendar.YEAR);
		
		boolean leap = false;
		if (year % 100 == 0) {
			if (year % 400 == 0) {
				leap = true;
			}
		}
		else if (year % 4 == 0) {
			leap = true;
		}
		
		int maxDay;
		int month = calendar.get(Calendar.MONTH) + 1;
		if (month == 1 || month == 3 || month == 5 || month == 7
			|| month == 8 || month == 10 || month == 12) {
			maxDay = 31;
		}
		else if (month == 2) {
			if (leap) 
				maxDay = 28;
			else
				maxDay = 29;
		}
		else {
			maxDay = 30;
		}
		
		int buildType = 0;
		int today = calendar.get(Calendar.DAY_OF_MONTH);
		
		if (today == maxDay) {
			buildType++;
		}
		
		if (month == 12 && buildType == 1) {
			buildType++;
		}
		
		// build for one year
		if (buildType == 2) {
			String start = year + "-1-1";
			String end = year + "-12-31";
			
			DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
			Date startDate = fmt.parse(start);
			Date endDate = fmt.parse(end);
			
			DetachedCriteria criteria = DetachedCriteria.forClass(FinanceReport.class);
			criteria.add(Property.forName("reportType").eq("year"));
			criteria.add(Property.forName("reportKey").eq(year - 1));
			List<FinanceReport> prevReports = financeReportService.findByCriteria(criteria);
			
			// deal with reward pay and consumed
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(BaseTranRec.class);
			detachedCriteria.add(Property.forName("paymentTime").ge(startDate));
			detachedCriteria.add(Property.forName("paymentTime").le(endDate));
			detachedCriteria.add(Property.forName("payType").eq("02")); 
			
			List<BaseTranRec> trans = baseTranRecService.findByCriteria(detachedCriteria);
			FinanceReport consumed = new FinanceReport();
			FinanceReport unconsumed = new FinanceReport();
			consumed.setReportType("year");
			consumed.setReportKey(year);
			consumed.setSource("奖励款（虚拟货币）");
			consumed.setItem("A:余额支付已消费金额（确认收入）");
			if (!prevReports.isEmpty()) {
				consumed.setPrevRemain(prevReports.get(0).getCurRemain());
			}
			else {
				consumed.setPrevRemain(0.00);
			}
			consumed.setCurPay(0.00);
			consumed.setCurConsume(0.00);
			
			unconsumed.setReportType("year");
			unconsumed.setReportKey(year);
			unconsumed.setSource("奖励款（虚拟货币）");
			unconsumed.setItem("B:余额已支付未消费金额（待确认收入）");
			if (!prevReports.isEmpty()) {
				unconsumed.setPrevRemain(prevReports.get(1).getCurRemain());
			}
			else {
				unconsumed.setPrevRemain(0.00);
			}
			unconsumed.setCurPay(0.00);
			unconsumed.setCurConsume(0.00);
			
			for (BaseTranRec baseTranRec : trans) {
				TicketIndentDetail indentDetail = baseTranRec.getTicketIndentDetail();
				if (indentDetail.getStatus().equals("08")) {
					continue;
				}
				
				if (indentDetail.getUseDate().getTime() > now.getTime()) {
					unconsumed.setCurPay(unconsumed.getCurPay() + indentDetail.getSubtotal());
				}
				else {
					consumed.setCurPay(consumed.getCurPay() + indentDetail.getSubtotal());
					consumed.setCurConsume(consumed.getCurConsume() + indentDetail.getSubtotal());
				}
			}
			consumed.setCurRemain(consumed.getPrevRemain() + consumed.getCurPay() - consumed.getCurConsume());
			unconsumed.setCurRemain(unconsumed.getPrevRemain() + unconsumed.getCurPay() - unconsumed.getCurConsume());
			financeReportService.save(consumed);
			financeReportService.save(unconsumed);

			FinanceReport recharge = new FinanceReport();
			recharge.setReportType("year");
			recharge.setReportKey(year);
			recharge.setSource("奖励款（虚拟货币）");
			recharge.setItem("C:奖励款充值金额");
			if (!prevReports.isEmpty()) {
				recharge.setPrevRemain(prevReports.get(2).getCurRemain());
			}
			else {
				recharge.setPrevRemain(0.00);
			}
			recharge.setCurPay(0.00);
			recharge.setCurConsume(0.00);
			
			// deal with reward recharge 
			detachedCriteria = DetachedCriteria.forClass(BaseRechargeRec.class);
			detachedCriteria.add(Property.forName("endDate").ge(startDate));
			detachedCriteria.add(Property.forName("endDate").le(endDate));
			detachedCriteria.add(Restrictions.or(Property.forName("paymentType").eq("02"), 
								 Property.forName("paymentType").eq("08")));
			List<BaseRechargeRec> recharges = baseRechargeRecService.findByCriteria(detachedCriteria);
			for (BaseRechargeRec baseRechargeRec : recharges) {
				if (baseRechargeRec.getRewardBalance() != null) {
					recharge.setCurPay(recharge.getCurPay() + baseRechargeRec.getRewardBalance());
				}
			}
			recharge.setCurRemain(recharge.getPrevRemain() + recharge.getCurPay() - recharge.getCurConsume());
			financeReportService.save(recharge);
			
			FinanceReport empty = new FinanceReport();
			empty.setReportType("year");
			empty.setReportKey(year);
			empty.setSource("APP、微信（第三方支付）");
			empty.setItem("A:支付宝已支付已消费金额");
			empty.setPrevRemain(0.00);
			empty.setCurPay(0.00);
			empty.setCurConsume(0.00);
			empty.setCurRemain(0.00);
			financeReportService.save(empty);
			
			empty = new FinanceReport();
			empty.setReportType("year");
			empty.setReportKey(year);
			empty.setSource("APP、微信（第三方支付）");
			empty.setItem("B:支付宝已支付未消费金额");
			empty.setPrevRemain(0.00);
			empty.setCurPay(0.00);
			empty.setCurConsume(0.00);
			empty.setCurRemain(0.00);
			financeReportService.save(empty);
			
			empty = new FinanceReport();
			empty.setReportType("year");
			empty.setReportKey(year);
			empty.setSource("APP、微信（第三方支付）");
			empty.setItem("C:支付宝退款到卡上金额");
			empty.setPrevRemain(0.00);
			empty.setCurPay(0.00);
			empty.setCurConsume(0.00);
			empty.setCurRemain(0.00);
			financeReportService.save(empty);
			
			empty = new FinanceReport();
			empty.setReportType("year");
			empty.setReportKey(year);
			empty.setSource("APP、微信（第三方支付）");
			empty.setItem("A:财付通已支付已消费金额");
			empty.setPrevRemain(0.00);
			empty.setCurPay(0.00);
			empty.setCurConsume(0.00);
			empty.setCurRemain(0.00);
			financeReportService.save(empty);
			
			empty = new FinanceReport();
			empty.setReportType("year");
			empty.setReportKey(year);
			empty.setSource("APP、微信（第三方支付）");
			empty.setItem("B:财付通已支付未消费金额");
			empty.setPrevRemain(0.00);
			empty.setCurPay(0.00);
			empty.setCurConsume(0.00);
			empty.setCurRemain(0.00);
			financeReportService.save(empty);			

			empty = new FinanceReport();
			empty.setReportType("year");
			empty.setReportKey(year);
			empty.setSource("APP、微信（第三方支付）");
			empty.setItem("C:财付通退款到卡上金额");
			empty.setPrevRemain(0.00);
			empty.setCurPay(0.00);
			empty.setCurConsume(0.00);
			empty.setCurRemain(0.00);
			financeReportService.save(empty);			
			
			// deal with prestore account pay and consumed
			detachedCriteria = DetachedCriteria.forClass(BaseTranRec.class);
			detachedCriteria.add(Property.forName("paymentTime").ge(startDate));
			detachedCriteria.add(Property.forName("paymentTime").le(endDate));
			detachedCriteria.add(Property.forName("payType").eq("01"));
			
			trans = baseTranRecService.findByCriteria(detachedCriteria);
			consumed = new FinanceReport();
			unconsumed = new FinanceReport();
			consumed.setReportType("year");
			consumed.setReportKey(year);
			consumed.setSource("预付款（转账）");
			consumed.setItem("A:已消费金额");
			if (!prevReports.isEmpty()) {
				consumed.setPrevRemain(prevReports.get(9).getCurRemain());
			}
			else {
				consumed.setPrevRemain(0.00);
			}
			consumed.setCurPay(0.00);
			consumed.setCurConsume(0.00);
			
			unconsumed.setReportType("year");
			unconsumed.setReportKey(year);
			unconsumed.setSource("预付款（转账）");
			unconsumed.setItem("B:已支付未消费金额");
			if (!prevReports.isEmpty()) {
				unconsumed.setPrevRemain(prevReports.get(10).getCurRemain());
			}
			else {
				unconsumed.setPrevRemain(0.00);
			}
			unconsumed.setCurPay(0.00);
			unconsumed.setCurConsume(0.00);
			
			for (BaseTranRec baseTranRec : trans) {
				TicketIndentDetail indentDetail = baseTranRec.getTicketIndentDetail();
				if (indentDetail.getStatus().equals("08")) {
					continue;
				}
				
				if (indentDetail.getUseDate().getTime() > now.getTime()) {
					unconsumed.setCurPay(unconsumed.getCurPay() + indentDetail.getSubtotal());
				}
				else {
					consumed.setCurPay(consumed.getCurPay() + indentDetail.getSubtotal());
					consumed.setCurConsume(consumed.getCurConsume() + indentDetail.getSubtotal());
				}
			}
			consumed.setCurRemain(consumed.getPrevRemain() + consumed.getCurPay() - consumed.getCurConsume());
			unconsumed.setCurRemain(unconsumed.getPrevRemain() + unconsumed.getCurPay() - unconsumed.getCurConsume());
			financeReportService.save(consumed);
			financeReportService.save(unconsumed);

			recharge = new FinanceReport();
			recharge.setReportType("year");
			recharge.setReportKey(year);
			recharge.setSource("预付款（转账）");
			recharge.setItem("C:充值金额");
			if (!prevReports.isEmpty()) {
				recharge.setPrevRemain(prevReports.get(11).getCurRemain());
			}
			else {
				recharge.setPrevRemain(0.00);
			}
			recharge.setCurPay(0.00);
			recharge.setCurConsume(0.00);
			
			// deal with prestore account recharge 
			detachedCriteria = DetachedCriteria.forClass(BaseRechargeRec.class);
			detachedCriteria.add(Property.forName("endDate").ge(startDate));
			detachedCriteria.add(Property.forName("endDate").le(endDate));
			detachedCriteria.add(Restrictions.or(Property.forName("paymentType").eq("00"),
												 Property.forName("paymentType").eq("08")));
			recharges = baseRechargeRecService.findByCriteria(detachedCriteria);
			for (BaseRechargeRec baseRechargeRec : recharges) {
				if (baseRechargeRec.getCashMoney() != null) {
					recharge.setCurPay(recharge.getCurPay() + baseRechargeRec.getCashMoney());
				}
			}
			recharge.setCurRemain(recharge.getPrevRemain() + recharge.getCurPay() - recharge.getCurConsume());
			financeReportService.save(recharge);	
			
			// deal with alipay pay and consumed
			detachedCriteria = DetachedCriteria.forClass(BaseTranRec.class);
			detachedCriteria.add(Property.forName("paymentTime").ge(startDate));
			detachedCriteria.add(Property.forName("paymentTime").le(endDate));
			detachedCriteria.add(Property.forName("payType").eq("00"));
			
			trans = baseTranRecService.findByCriteria(detachedCriteria);
			consumed = new FinanceReport();
			unconsumed = new FinanceReport();
			consumed.setReportType("year");
			consumed.setReportKey(year);
			consumed.setSource("电商平台（第三方支付）");
			consumed.setItem("支付宝已支付已消费金额");
			if (!prevReports.isEmpty()) {
				consumed.setPrevRemain(prevReports.get(12).getCurRemain());
			}
			else {
				consumed.setPrevRemain(0.00);
			}
			consumed.setCurPay(0.00);
			consumed.setCurConsume(0.00);
			
			unconsumed.setReportType("year");
			unconsumed.setReportKey(year);
			unconsumed.setSource("电商平台（第三方支付）");
			unconsumed.setItem("支付宝已支付未消费金额");
			if (!prevReports.isEmpty()) {
				unconsumed.setPrevRemain(prevReports.get(13).getCurRemain());
			}
			else {
				unconsumed.setPrevRemain(0.00);
			}
			unconsumed.setCurPay(0.00);
			unconsumed.setCurConsume(0.00);
			
			FinanceReport refund = new FinanceReport();
			refund.setReportType("year");
			refund.setReportKey(year);
			refund.setSource("电商平台（第三方支付）");
			refund.setItem("支付宝退款到卡上金额");
			if (!prevReports.isEmpty()) {
				refund.setPrevRemain(prevReports.get(14).getCurRemain());
			}
			else {
				refund.setPrevRemain(0.00);
			}
			refund.setCurPay(0.00);
			refund.setCurConsume(0.00);
			
			for (BaseTranRec baseTranRec : trans) {
				TicketIndentDetail indentDetail = baseTranRec.getTicketIndentDetail();
				if (indentDetail.getStatus().equals("08")) {
					if (baseTranRec.getPaymentType().equals("08")) {
						refund.setCurPay(refund.getCurPay() + indentDetail.getSubtotal());
					}
					continue;
				}
				
				if (indentDetail.getUseDate().getTime() > now.getTime()) {
					unconsumed.setCurPay(unconsumed.getCurPay() + indentDetail.getSubtotal());
				}
				else {
					consumed.setCurPay(consumed.getCurPay() + indentDetail.getSubtotal());
					consumed.setCurConsume(consumed.getCurConsume() + indentDetail.getSubtotal());
				}
			}
			consumed.setCurRemain(consumed.getPrevRemain() + consumed.getCurPay() - consumed.getCurConsume());
			unconsumed.setCurRemain(unconsumed.getPrevRemain() + unconsumed.getCurPay() - unconsumed.getCurConsume());
			refund.setCurRemain(refund.getPrevRemain() + refund.getCurPay() - refund.getCurConsume());
			financeReportService.save(consumed);
			financeReportService.save(unconsumed);
			financeReportService.save(refund);

			empty = new FinanceReport();
			empty.setReportType("year");
			empty.setReportKey(year);
			empty.setSource("电商平台（第三方支付）");
			empty.setItem("财付通已支付已消费金额");
			empty.setPrevRemain(0.00);
			empty.setCurPay(0.00);
			empty.setCurConsume(0.00);
			empty.setCurRemain(0.00);
			financeReportService.save(empty);		
			
			empty = new FinanceReport();
			empty.setReportType("year");
			empty.setReportKey(year);
			empty.setSource("电商平台（第三方支付）");
			empty.setItem("财付通已支付未消费金额");
			empty.setPrevRemain(0.00);
			empty.setCurPay(0.00);
			empty.setCurConsume(0.00);
			empty.setCurRemain(0.00);
			financeReportService.save(empty);
			
			empty = new FinanceReport();
			empty.setReportType("year");
			empty.setReportKey(year);
			empty.setSource("电商平台（第三方支付）");
			empty.setItem("财付通退款到卡上金额");
			empty.setPrevRemain(0.00);
			empty.setCurPay(0.00);
			empty.setCurConsume(0.00);
			empty.setCurRemain(0.00);
			financeReportService.save(empty);						

			empty = new FinanceReport();
			empty.setReportType("year");
			empty.setReportKey(year);
			empty.setSource("电商平台（第三方支付）");
			empty.setItem("网银已支付已消费金额");
			empty.setPrevRemain(0.00);
			empty.setCurPay(0.00);
			empty.setCurConsume(0.00);
			empty.setCurRemain(0.00);
			financeReportService.save(empty);		
			
			empty = new FinanceReport();
			empty.setReportType("year");
			empty.setReportKey(year);
			empty.setSource("电商平台（第三方支付）");
			empty.setItem("网银已支付未消费金额");
			empty.setPrevRemain(0.00);
			empty.setCurPay(0.00);
			empty.setCurConsume(0.00);
			empty.setCurRemain(0.00);
			financeReportService.save(empty);
			
			empty = new FinanceReport();
			empty.setReportType("year");
			empty.setReportKey(year);
			empty.setSource("电商平台（第三方支付）");
			empty.setItem("网银退款到卡上金额");
			empty.setPrevRemain(0.00);
			empty.setCurPay(0.00);
			empty.setCurConsume(0.00);
			empty.setCurRemain(0.00);
			financeReportService.save(empty);		
			
			buildType--;
		}
		
		// build for one month
		if (buildType == 1) {
			String start = year + "-" + month + "-1";
			String end = year + "-" + month + "-" + maxDay;
			
			DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
			Date startDate = fmt.parse(start);
			Date endDate = fmt.parse(end);
			
			DetachedCriteria criteria = DetachedCriteria.forClass(FinanceReport.class);
			criteria.add(Property.forName("reportType").eq("month"));
			criteria.add(Property.forName("reportKey").eq(month - 1));
			List<FinanceReport> prevReports = financeReportService.findByCriteria(criteria);
			
			// deal with reward pay and consumed
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(BaseTranRec.class);
			detachedCriteria.add(Property.forName("paymentTime").ge(startDate));
			detachedCriteria.add(Property.forName("paymentTime").le(endDate));
			detachedCriteria.add(Property.forName("payType").eq("02")); 
			
			List<BaseTranRec> trans = baseTranRecService.findByCriteria(detachedCriteria);
			FinanceReport consumed = new FinanceReport();
			FinanceReport unconsumed = new FinanceReport();
			consumed.setReportType("month");
			consumed.setReportKey(month);
			consumed.setSource("奖励款（虚拟货币）");
			consumed.setItem("A:余额支付已消费金额（确认收入）");
			if (!prevReports.isEmpty()) {
				consumed.setPrevRemain(prevReports.get(0).getCurRemain());
			}
			else {
				consumed.setPrevRemain(0.00);
			}
			consumed.setCurPay(0.00);
			consumed.setCurConsume(0.00);
			
			unconsumed.setReportType("month");
			unconsumed.setReportKey(month);
			unconsumed.setSource("奖励款（虚拟货币）");
			unconsumed.setItem("B:余额已支付未消费金额（待确认收入）");
			if (!prevReports.isEmpty()) {
				unconsumed.setPrevRemain(prevReports.get(1).getCurRemain());
			}
			else {
				unconsumed.setPrevRemain(0.00);
			}
			unconsumed.setCurPay(0.00);
			unconsumed.setCurConsume(0.00);
			
			for (BaseTranRec baseTranRec : trans) {
				TicketIndentDetail indentDetail = baseTranRec.getTicketIndentDetail();
				if (indentDetail.getStatus().equals("08")) {
					continue;
				}
				
				if (indentDetail.getUseDate().getTime() > now.getTime()) {
					unconsumed.setCurPay(unconsumed.getCurPay() + indentDetail.getSubtotal());
				}
				else {
					consumed.setCurPay(consumed.getCurPay() + indentDetail.getSubtotal());
					consumed.setCurConsume(consumed.getCurConsume() + indentDetail.getSubtotal());
				}
			}
			consumed.setCurRemain(consumed.getPrevRemain() + consumed.getCurPay() - consumed.getCurConsume());
			unconsumed.setCurRemain(unconsumed.getPrevRemain() + unconsumed.getCurPay() - unconsumed.getCurConsume());
			financeReportService.save(consumed);
			financeReportService.save(unconsumed);

			FinanceReport recharge = new FinanceReport();
			recharge.setReportType("month");
			recharge.setReportKey(month);
			recharge.setSource("奖励款（虚拟货币）");
			recharge.setItem("C:奖励款充值金额");
			if (!prevReports.isEmpty()) {
				recharge.setPrevRemain(prevReports.get(2).getCurRemain());
			}
			else {
				recharge.setPrevRemain(0.00);
			}
			recharge.setCurPay(0.00);
			recharge.setCurConsume(0.00);
			
			// deal with reward recharge 
			detachedCriteria = DetachedCriteria.forClass(BaseRechargeRec.class);
			detachedCriteria.add(Property.forName("endDate").ge(startDate));
			detachedCriteria.add(Property.forName("endDate").le(endDate));
			detachedCriteria.add(Restrictions.or(Property.forName("paymentType").eq("02"), 
								 Property.forName("paymentType").eq("08")));
			List<BaseRechargeRec> recharges = baseRechargeRecService.findByCriteria(detachedCriteria);
			for (BaseRechargeRec baseRechargeRec : recharges) {
				if (baseRechargeRec.getRewardBalance() != null) {
					recharge.setCurPay(recharge.getCurPay() + baseRechargeRec.getRewardBalance());
				}
			}
			recharge.setCurRemain(recharge.getPrevRemain() + recharge.getCurPay() - recharge.getCurConsume());
			financeReportService.save(recharge);
			
			FinanceReport empty = new FinanceReport();
			empty.setReportType("month");
			empty.setReportKey(month);
			empty.setSource("APP、微信（第三方支付）");
			empty.setItem("A:支付宝已支付已消费金额");
			empty.setPrevRemain(0.00);
			empty.setCurPay(0.00);
			empty.setCurConsume(0.00);
			empty.setCurRemain(0.00);
			financeReportService.save(empty);
			
			empty = new FinanceReport();
			empty.setReportType("month");
			empty.setReportKey(month);
			empty.setSource("APP、微信（第三方支付）");
			empty.setItem("B:支付宝已支付未消费金额");
			empty.setPrevRemain(0.00);
			empty.setCurPay(0.00);
			empty.setCurConsume(0.00);
			empty.setCurRemain(0.00);
			financeReportService.save(empty);
			
			empty = new FinanceReport();
			empty.setReportType("month");
			empty.setReportKey(month);
			empty.setSource("APP、微信（第三方支付）");
			empty.setItem("C:支付宝退款到卡上金额");
			empty.setPrevRemain(0.00);
			empty.setCurPay(0.00);
			empty.setCurConsume(0.00);
			empty.setCurRemain(0.00);
			financeReportService.save(empty);
			
			empty = new FinanceReport();
			empty.setReportType("month");
			empty.setReportKey(month);
			empty.setSource("APP、微信（第三方支付）");
			empty.setItem("A:财付通已支付已消费金额");
			empty.setPrevRemain(0.00);
			empty.setCurPay(0.00);
			empty.setCurConsume(0.00);
			empty.setCurRemain(0.00);
			financeReportService.save(empty);
			
			empty = new FinanceReport();
			empty.setReportType("month");
			empty.setReportKey(month);
			empty.setSource("APP、微信（第三方支付）");
			empty.setItem("B:财付通已支付未消费金额");
			empty.setPrevRemain(0.00);
			empty.setCurPay(0.00);
			empty.setCurConsume(0.00);
			empty.setCurRemain(0.00);
			financeReportService.save(empty);			

			empty = new FinanceReport();
			empty.setReportType("month");
			empty.setReportKey(month);
			empty.setSource("APP、微信（第三方支付）");
			empty.setItem("C:财付通退款到卡上金额");
			empty.setPrevRemain(0.00);
			empty.setCurPay(0.00);
			empty.setCurConsume(0.00);
			empty.setCurRemain(0.00);
			financeReportService.save(empty);			
			
			// deal with prestore account pay and consumed
			detachedCriteria = DetachedCriteria.forClass(BaseTranRec.class);
			detachedCriteria.add(Property.forName("paymentTime").ge(startDate));
			detachedCriteria.add(Property.forName("paymentTime").le(endDate));
			detachedCriteria.add(Property.forName("payType").eq("01"));
			
			trans = baseTranRecService.findByCriteria(detachedCriteria);
			consumed = new FinanceReport();
			unconsumed = new FinanceReport();
			consumed.setReportType("month");
			consumed.setReportKey(month);
			consumed.setSource("预付款（转账）");
			consumed.setItem("A:已消费金额");
			if (!prevReports.isEmpty()) {
				consumed.setPrevRemain(prevReports.get(9).getCurRemain());
			}
			else {
				consumed.setPrevRemain(0.00);
			}
			consumed.setCurPay(0.00);
			consumed.setCurConsume(0.00);
			
			unconsumed.setReportType("month");
			unconsumed.setReportKey(month);
			unconsumed.setSource("预付款（转账）");
			unconsumed.setItem("B:已支付未消费金额");
			if (!prevReports.isEmpty()) {
				unconsumed.setPrevRemain(prevReports.get(10).getCurRemain());
			}
			else {
				unconsumed.setPrevRemain(0.00);
			}
			unconsumed.setCurPay(0.00);
			unconsumed.setCurConsume(0.00);
			
			for (BaseTranRec baseTranRec : trans) {
				TicketIndentDetail indentDetail = baseTranRec.getTicketIndentDetail();
				if (indentDetail.getStatus().equals("08")) {
					continue;
				}
				
				if (indentDetail.getUseDate().getTime() > now.getTime()) {
					unconsumed.setCurPay(unconsumed.getCurPay() + indentDetail.getSubtotal());
				}
				else {
					consumed.setCurPay(consumed.getCurPay() + indentDetail.getSubtotal());
					consumed.setCurConsume(consumed.getCurConsume() + indentDetail.getSubtotal());
				}
			}
			consumed.setCurRemain(consumed.getPrevRemain() + consumed.getCurPay() - consumed.getCurConsume());
			unconsumed.setCurRemain(unconsumed.getPrevRemain() + unconsumed.getCurPay() - unconsumed.getCurConsume());
			financeReportService.save(consumed);
			financeReportService.save(unconsumed);

			recharge = new FinanceReport();
			recharge.setReportType("month");
			recharge.setReportKey(month);
			recharge.setSource("预付款（转账）");
			recharge.setItem("C:充值金额");
			if (!prevReports.isEmpty()) {
				recharge.setPrevRemain(prevReports.get(11).getCurRemain());
			}
			else {
				recharge.setPrevRemain(0.00);
			}
			recharge.setCurPay(0.00);
			recharge.setCurConsume(0.00);
			
			// deal with prestore account recharge 
			detachedCriteria = DetachedCriteria.forClass(BaseRechargeRec.class);
			detachedCriteria.add(Property.forName("endDate").ge(startDate));
			detachedCriteria.add(Property.forName("endDate").le(endDate));
			detachedCriteria.add(Restrictions.or(Property.forName("paymentType").eq("00"),
												 Property.forName("paymentType").eq("08")));
			recharges = baseRechargeRecService.findByCriteria(detachedCriteria);
			for (BaseRechargeRec baseRechargeRec : recharges) {
				if (baseRechargeRec.getCashMoney() != null) {
					recharge.setCurPay(recharge.getCurPay() + baseRechargeRec.getCashMoney());
				}
			}
			recharge.setCurRemain(recharge.getPrevRemain() + recharge.getCurPay() - recharge.getCurConsume());
			financeReportService.save(recharge);	
			
			// deal with alipay pay and consumed
			detachedCriteria = DetachedCriteria.forClass(BaseTranRec.class);
			detachedCriteria.add(Property.forName("paymentTime").ge(startDate));
			detachedCriteria.add(Property.forName("paymentTime").le(endDate));
			detachedCriteria.add(Property.forName("payType").eq("00"));
			
			trans = baseTranRecService.findByCriteria(detachedCriteria);
			consumed = new FinanceReport();
			unconsumed = new FinanceReport();
			consumed.setReportType("month");
			consumed.setReportKey(month);
			consumed.setSource("电商平台（第三方支付）");
			consumed.setItem("支付宝已支付已消费金额");
			if (!prevReports.isEmpty()) {
				consumed.setPrevRemain(prevReports.get(12).getCurRemain());
			}
			else {
				consumed.setPrevRemain(0.00);
			}
			consumed.setCurPay(0.00);
			consumed.setCurConsume(0.00);
			
			unconsumed.setReportType("month");
			unconsumed.setReportKey(month);
			unconsumed.setSource("电商平台（第三方支付）");
			unconsumed.setItem("支付宝已支付未消费金额");
			if (!prevReports.isEmpty()) {
				unconsumed.setPrevRemain(prevReports.get(13).getCurRemain());
			}
			else {
				unconsumed.setPrevRemain(0.00);
			}
			unconsumed.setCurPay(0.00);
			unconsumed.setCurConsume(0.00);
			
			FinanceReport refund = new FinanceReport();
			refund.setReportType("month");
			refund.setReportKey(month);
			refund.setSource("电商平台（第三方支付）");
			refund.setItem("支付宝退款到卡上金额");
			if (!prevReports.isEmpty()) {
				refund.setPrevRemain(prevReports.get(14).getCurRemain());
			}
			else {
				refund.setPrevRemain(0.00);
			}
			refund.setCurPay(0.00);
			refund.setCurConsume(0.00);
			
			for (BaseTranRec baseTranRec : trans) {
				TicketIndentDetail indentDetail = baseTranRec.getTicketIndentDetail();
				if (indentDetail.getStatus().equals("08")) {
					refund.setCurPay(refund.getCurPay() + indentDetail.getSubtotal());
					continue;
				}
				
				if (indentDetail.getUseDate().getTime() > now.getTime()) {
					unconsumed.setCurPay(unconsumed.getCurPay() + indentDetail.getSubtotal());
				}
				else {
					consumed.setCurPay(consumed.getCurPay() + indentDetail.getSubtotal());
					consumed.setCurConsume(consumed.getCurConsume() + indentDetail.getSubtotal());
				}
			}
			consumed.setCurRemain(consumed.getPrevRemain() + consumed.getCurPay() - consumed.getCurConsume());
			unconsumed.setCurRemain(unconsumed.getPrevRemain() + unconsumed.getCurPay() - unconsumed.getCurConsume());
			refund.setCurRemain(refund.getPrevRemain() + refund.getCurPay() - refund.getCurConsume());
			financeReportService.save(consumed);
			financeReportService.save(unconsumed);
			financeReportService.save(refund);

			empty = new FinanceReport();
			empty.setReportType("month");
			empty.setReportKey(month);
			empty.setSource("电商平台（第三方支付）");
			empty.setItem("财付通已支付已消费金额");
			empty.setPrevRemain(0.00);
			empty.setCurPay(0.00);
			empty.setCurConsume(0.00);
			empty.setCurRemain(0.00);
			financeReportService.save(empty);		
			
			empty = new FinanceReport();
			empty.setReportType("month");
			empty.setReportKey(month);
			empty.setSource("电商平台（第三方支付）");
			empty.setItem("财付通已支付未消费金额");
			empty.setPrevRemain(0.00);
			empty.setCurPay(0.00);
			empty.setCurConsume(0.00);
			empty.setCurRemain(0.00);
			financeReportService.save(empty);
			
			empty = new FinanceReport();
			empty.setReportType("month");
			empty.setReportKey(month);
			empty.setSource("电商平台（第三方支付）");
			empty.setItem("财付通退款到卡上金额");
			empty.setPrevRemain(0.00);
			empty.setCurPay(0.00);
			empty.setCurConsume(0.00);
			empty.setCurRemain(0.00);
			financeReportService.save(empty);						

			empty = new FinanceReport();
			empty.setReportType("month");
			empty.setReportKey(month);
			empty.setSource("电商平台（第三方支付）");
			empty.setItem("网银已支付已消费金额");
			empty.setPrevRemain(0.00);
			empty.setCurPay(0.00);
			empty.setCurConsume(0.00);
			empty.setCurRemain(0.00);
			financeReportService.save(empty);		
			
			empty = new FinanceReport();
			empty.setReportType("month");
			empty.setReportKey(month);
			empty.setSource("电商平台（第三方支付）");
			empty.setItem("网银已支付未消费金额");
			empty.setPrevRemain(0.00);
			empty.setCurPay(0.00);
			empty.setCurConsume(0.00);
			empty.setCurRemain(0.00);
			financeReportService.save(empty);
			
			empty = new FinanceReport();
			empty.setReportType("month");
			empty.setReportKey(month);
			empty.setSource("电商平台（第三方支付）");
			empty.setItem("网银退款到卡上金额");
			empty.setPrevRemain(0.00);
			empty.setCurPay(0.00);
			empty.setCurConsume(0.00);
			empty.setCurRemain(0.00);
			financeReportService.save(empty);		
			
			buildType--;
		}
		
		System.out.println("完成财务报表生成");
	}	
}
