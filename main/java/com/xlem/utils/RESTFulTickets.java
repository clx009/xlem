package com.xlem.utils;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="tickets")
public class RESTFulTickets {
	private List<RESTFulTicket> tickets;
	
	public RESTFulTickets() {}
	
	public RESTFulTickets(List<RESTFulTicket> tickets) {
		this.tickets = tickets;
	}
	
	public List<RESTFulTicket> getRESTFulTickets() {
		return this.tickets;
	}
	
	public void setRESTFulTickets(List<RESTFulTicket> tickets) {
		this.tickets = tickets;
	}
}

