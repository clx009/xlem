package com.xlem.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class SeriesNumber {
	private int count = 0;

	private volatile static SeriesNumber instance;

	public static String getNumber() {
		DateFormat fmt = new SimpleDateFormat("yyMMdd");
		String control = "000";

		if (instance == null) {
			synchronized (SeriesNumber.class) {
				if (instance == null) {
					instance = new SeriesNumber();
				}
			}
		}
		instance.count++;
		return fmt.format(new Date()) + control
				+ String.format("%05d", instance.count);
	}

	private SeriesNumber() {
		count = 0;
	}
	
	/** 产生一个随机的字符串*/  
	public static String RandomString(int length) {  
		String str = "0123456789";  
		Random random = new Random();  
		StringBuffer buf = new StringBuffer();  
		for (int i = 0; i < length; i++) {  
			int num = random.nextInt(10);  
			buf.append(str.charAt(num));  
		}  
		return buf.toString();  
	}  
	
}
