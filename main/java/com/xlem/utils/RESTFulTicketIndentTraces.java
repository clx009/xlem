package com.xlem.utils;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="ticketIndentTraces")
public class RESTFulTicketIndentTraces {
	private List<RESTFulTicketIndentTrace> ticketIndentTraces;
	
	public RESTFulTicketIndentTraces() {};
	
	
	public RESTFulTicketIndentTraces(List<RESTFulTicketIndentTrace> ticketIndentTraces) {
		this.ticketIndentTraces = ticketIndentTraces;
	}
	
	public List<RESTFulTicketIndentTrace> getTicketIndentTraces() {
		return this.ticketIndentTraces;
	}
	
	public void setTicketIndentTraces(List<RESTFulTicketIndentTrace> ticketIndentTraces) {
		this.ticketIndentTraces = ticketIndentTraces;
	}
}
