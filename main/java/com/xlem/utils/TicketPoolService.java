package com.xlem.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.xlem.dao.TicketIndent;
import com.xlem.dao.TicketIndentDetail;
import com.xlem.dao.TicketPrice;

import service.ticket.ticketIndent.TicketIndentService;
import service.ticket.ticketPrice.TicketPriceService;

@Component("ticketPoolService")
public class TicketPoolService {
	@Autowired
	TicketIndentService ticketIndentService;
	@Autowired
	TicketPriceService ticketPriceService;
	
	private ArrayList<String> lockedTID = new ArrayList<String>();
	private HashMap<String, Date> inPoolTime = new HashMap<String, Date>();
	
	@Autowired
	public void initPool() {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(TicketIndent.class);
		detachedCriteria.add(Property.forName("status").eq("00"));
		List<TicketIndent> ticketIndents = ticketIndentService.findByCriteria(detachedCriteria);
		for (TicketIndent ticketIndent : ticketIndents) {
			lockedTID.add(ticketIndent.getIndentId());
			inPoolTime.put(ticketIndent.getIndentId(), new Date());
		}
	}
	
	public void addTIDToPool(String TID) {
		Date now = new Date();
		lockedTID.add(TID);
		inPoolTime.put(TID, now);
	}
	
	@Scheduled(cron="0 * * * * *")
	public void run() {
		// TODO Auto-generated method stub
		System.out.println("票务订单池启动检查");
		for (String TID : lockedTID) {
			Date now = new Date();
			long diff = now.getTime() - inPoolTime.get(TID).getTime();
			long mins = diff / (1000 * 60);
			if (mins > 30) {
				TicketIndent ticketIndent = ticketIndentService.findById(TID);
				if (ticketIndent.getStatus().equals("00")) {
					for (TicketIndentDetail indentDetail : ticketIndent.getTicketIndentDetails()) {
						TicketPrice ticketPrice = indentDetail.getTicketPrice();
						ticketPrice.setRemainToplimit(ticketPrice.getRemainToplimit() + indentDetail.getAmount());
						ticketPriceService.update(ticketPrice);
					}
					
					ticketIndentService.delete(ticketIndent);
				}
			}
		}
	}
}
