package com.xlem.utils;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.web.bind.annotation.RequestParam;

@XmlRootElement (name="ticketIndentTrace")
@XmlAccessorType(XmlAccessType.FIELD)
public class RESTFulTicketIndentTrace {
	// ticket indent id
	@XmlElement	
	private String orderid;
	// ticket machine synchronize time
	@XmlElement	
	private Date machineTime;
	// ticket machine status
	@XmlElement	
	private String machineStatus;
	// ticket gate guard synchronize time
	@XmlElement	
	private Date entryGuardTime;
	// ticket gate guard status
	@XmlElement	
	private String entryGuardStatus;
	
	public String getOrderid() {
		return this.orderid;
	}
	
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	
	public Date getMachineTime() {
		return this.machineTime;
	}
	
	public void setMachineTime(Date machineTime) {
		this.machineTime = machineTime;
	}
	
	public String getMachineStatus() {
		return this.machineStatus;
	}
	
	public void setMachineStatus(String machineStatus) {
		this.machineStatus = machineStatus;
	}
	
	public Date getEntryGuardTime() {
		return this.entryGuardTime;
	}
	
	public void setEntryGuardTime(Date entryGuardTime) {
		this.entryGuardTime = entryGuardTime;
	}
	
	public String getEntryGuardStatus() {
		return this.entryGuardStatus;
	}
	
	public void setEntryGuardStatus(String entryGuardStatus) {
		this.entryGuardStatus = entryGuardStatus;
	}
}
