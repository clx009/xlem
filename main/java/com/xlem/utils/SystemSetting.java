package com.xlem.utils;

public class SystemSetting {
	private String xlpwUrl;
	private String defaultImage;
	private String ticketImageMapping;
	private String hotelImageMapping;
	
	public String getXlpwUrl() {
		return this.xlpwUrl;
	}
	
	public void setXlpwUrl(String xlpwUrl) {
		this.xlpwUrl = xlpwUrl;
	}

	public String getDefaultImage() {
		return this.defaultImage;
	}
	
	public void setDefaultImage(String image) {
		this.defaultImage = image;
	}
	
	public String getTicketImageMapping() {
		return this.ticketImageMapping;
	}
	
	public void setTicketImageMapping(String mapping) {
		this.ticketImageMapping = mapping;
	}
	
	public String getHotelImageMapping() {
		return this.hotelImageMapping;
	}
	
	public void setHotelImageMapping(String mapping) {
		this.hotelImageMapping = mapping;
	}
	
	public String getImageForTicket(String tname) {
		int pos = ticketImageMapping.indexOf(tname);
		if (pos != -1) {
			pos += tname.length();
			pos += 1;
			String rem = ticketImageMapping.substring(pos);
			int end = rem.indexOf(';');
			if (end != -1) {
				String image = rem.substring(0, end);
				return image;
			}
		}
		
		return null; 
	}
	
	public String getImageForHotel(String hname) {
		int pos = hotelImageMapping.indexOf(hname);
		if (pos != -1) {
			pos += hname.length();
			pos += 1;
			String rem = hotelImageMapping.substring(pos);
			int end = rem.indexOf(';');
			if (end != -1) {
				String image = rem.substring(0, end);
				return image;
			}
		}
		
		return null; 
	}
	
}

