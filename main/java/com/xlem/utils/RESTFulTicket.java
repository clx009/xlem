package com.xlem.utils;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name="ticket")
public class RESTFulTicket {
	@XmlElement	
	private int pid;
	@XmlElement	
	private String name;
	@XmlElement	
	private boolean isSuite;
	@XmlElement	
	private Double price;
	@XmlElement	
	private String info;
	@XmlElement	
	private Date start;
	@XmlElement	
	private Date end;
	
	public RESTFulTicket() {}
	
	public RESTFulTicket(int pid, String name, boolean isSuite, Double price, String info, Date start, Date end) {
		this.pid = pid;
		this.name = name;
		this.isSuite = isSuite;
		this.price = price;
		this.info = info;
		this.start = start;
		this.end = end;
	}
	
	public int getPid() {
		return this.pid;
	}
	
	public String getName() {
		return this.name;
	}
	
	public boolean getIsSuite() {
		return this.isSuite;
	}
	
	public Double getPrice() {
		return this.price;
	}
	
	public String getInfo() {
		return this.info;
	}
	
	public Date getStart() {
		return this.start;
	}
	
	public Date getEnd() {
		return this.end;
	}
}
