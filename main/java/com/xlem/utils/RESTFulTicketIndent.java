package com.xlem.utils;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.web.bind.annotation.RequestParam;

@XmlRootElement (name="ticketIndent")
@XmlAccessorType(XmlAccessType.FIELD)
public class RESTFulTicketIndent {
	// ticket indent id
	@XmlElement	
	private String orderid;
	// ticket user name
	@XmlElement	
	private String uname;
	// ticket user id card number
	@XmlElement	
	private String uid;
	// ticket user mobile phone
	@XmlElement	
	private String mphone;
	// ticket user sms code for indent
	@XmlElement	
	private String vcode;
	// ticket product id
	@XmlElement	
	private Integer pid;
	// ticket suite name
	@XmlElement	
	private String sname;
	// indent number
	@XmlElement	
	private Integer count;
	// discount for price
	@XmlElement
	private String discount;
	// play date
	@XmlElement	
	private Date playdate;
	// play type. normally is "alipay"
	@XmlElement	
	private String paytype;
	
	public String getOrderid() {
		return this.orderid;
	}
	
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	
	public String getUname() {
		return this.uname;
	}
	
	public void setUname(String uname) {
		this.uname = uname;
	}
	
	public String getUid() {
		return this.uid;
	}
	
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	public String getMphone() {
		return this.mphone;
	}
	
	public void setMphone(String mphone) {
		this.mphone = mphone;
	}
	
	public String getVcode() {
		return this.vcode;
	}
	
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	
	public Integer getPid() {
		return this.pid;
	}
	
	public void setPid(Integer pid) {
		this.pid = pid;
	}
	
	public String getSname() {
		return this.sname;
	}
	
	public void setSname(String sname) {
		this.sname = sname;
	}
	
	public Integer getCount() {
		return this.count;
	}
	
	public void setCount(Integer count) {
		this.count = count;
	}
	
	public String getDiscount() {
		return this.discount;
	}
	
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	
	public Date getPlaydate() {
		return this.playdate;
	}
	
	public void setPlaydate(Date playdate) {
		this.playdate = playdate;
	}
	
	public String getPaytype() {
		return this.paytype;
	}
	
	public void setPaytype(String paytype) {
		this.paytype = paytype;
	}
}
