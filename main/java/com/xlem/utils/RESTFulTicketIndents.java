package com.xlem.utils;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="ticketIndents")
public class RESTFulTicketIndents {
	private List<RESTFulTicketIndent> ticketIndents;
	
	public RESTFulTicketIndents() {};
	
	
	public RESTFulTicketIndents(List<RESTFulTicketIndent> ticketIndents) {
		this.ticketIndents = ticketIndents;
	}
	
	public List<RESTFulTicketIndent> getTicketIndents() {
		return this.ticketIndents;
	}
	
	public void setTicketIndents(List<RESTFulTicketIndent> ticketIndents) {
		this.ticketIndents = ticketIndents;
	}
}
