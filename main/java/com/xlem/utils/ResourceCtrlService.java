package com.xlem.utils;

import java.util.Date;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.xlem.dao.TicketIndent;
import com.xlem.dao.TicketIndentDetail;
import com.xlem.dao.TicketPrice;

import service.ticket.ticketPrice.TicketPriceService;

@Component("resourceCtrlService")
public class ResourceCtrlService {
	@Autowired
	private TicketPriceService ticketPriceService;
	
	@Autowired
	public void resetResource() {
		List<TicketPrice> ticketPrices = ticketPriceService.findAll();
		for (TicketPrice ticketPrice : ticketPrices) {
			if (ticketPrice.getToplimit() != null) {
				ticketPrice.setRemainToplimit(ticketPrice.getToplimit());
				ticketPriceService.update(ticketPrice);
			}
		}
	}
	
	@Scheduled(cron="0 0 0 * * *")
	public void run() {
		// TODO Auto-generated method stub
		System.out.println("资源零点复位启动");
		List<TicketPrice> ticketPrices = ticketPriceService.findAll();
		for (TicketPrice ticketPrice : ticketPrices) {
			if (ticketPrice.getToplimit() != null) {
				ticketPrice.setRemainToplimit(ticketPrice.getToplimit());
				ticketPriceService.update(ticketPrice);
			}
		}
	}	
}
