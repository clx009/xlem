package com.xlem.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import common.pay.PayParams;
import service.ticket.ticketIndent.TicketIndentService;

@RequestMapping("/pay")
@Controller
public class PayController {
	@Autowired 
	private TicketIndentService ticketIndentService;
	
	@RequestMapping(value="/bankSelect", method=RequestMethod.GET)
	public String bankSelect(Model uiModle) {
		return "/bank/bankPage";
	}
}
