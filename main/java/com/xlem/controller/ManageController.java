package com.xlem.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.xlem.dao.Depart;
import com.xlem.dao.Role;
import com.xlem.dao.User;
import com.xlem.utils.Message;
import com.xlem.utils.UrlUtil;

import service.sys.depart.DepartService;
import service.sys.role.RoleService;
import service.sys.user.UserService;

@RequestMapping("/manage")
@Controller
public class ManageController {
	final Logger logger = LoggerFactory.getLogger(ManageController.class);
	@Autowired
	private DepartService departService;
	@Autowired 
	private UserService userService;
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private HttpSession session;

	@Autowired
	MessageSource messageSource;
	
	@RequestMapping(method=RequestMethod.GET)
	public String manage_show(Model uiModle) {
		logger.info("show manage page.");
		
		Long user_id = (Long) session.getAttribute("user_id");
		if (user_id == null) {
			return "index";
		}
		
		User user = userService.findById(user_id);
		if (!user.getRole().getName().equals("ROLE_ADMIN")) {
			return "index";
		}
		
		return "manage/init";
	}
	
	@RequestMapping(value="/depart", method = RequestMethod.GET)
	public String listDeparts(Model uiModel) {
		logger.info("Listing departs");
		
		Long user_id = (Long) session.getAttribute("user_id");
		if (user_id == null) {
			return "index";
		}
		
		User user = userService.findById(user_id);
		if (!user.getRole().getName().equals("ROLE_ADMIN")) {
			return "index";
		}
		
		List<Depart> departs = departService.findAll();
		uiModel.addAttribute("departs", departs);
		logger.info("No. of departs: " + departs.size());
		return "manage/depart_list";
	}	
	
	@RequestMapping(value="/depart", params="form", method=RequestMethod.POST)
    public String createDepart(Depart depart, BindingResult bindingResult, Model uiModel, 
    		HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes, Locale locale) {
		final Logger logger = LoggerFactory.getLogger(ManageController.class);
		logger.info("Create department");
		
		Long user_id = (Long) session.getAttribute("user_id");
		if (user_id == null) {
			return "index";
		}
		
		User user = userService.findById(user_id);
		if (!user.getRole().getName().equals("ROLE_ADMIN")) {
			return "index";
		}
		
		if (bindingResult.hasErrors()) {
			uiModel.addAttribute("message", new Message("error", 
					messageSource.getMessage("depart_save_fail", new Object[]{}, locale)));
			uiModel.addAttribute("depart", depart);
			return "manage/depart_create";
		}

		uiModel.asMap().clear();
		redirectAttributes.addFlashAttribute("message", new Message("success",
				messageSource.getMessage("depart_save_success", new Object[]{}, locale)));
		
		logger.info("depart id" + depart.getIdDepart() + "depart name" + depart.getName());
		departService.save(depart);
		return "redirect:/manage/depart/" + UrlUtil.encodeUrlPathSegment(depart.getIdDepart().toString(), httpServletRequest); 
    }
	
	@RequestMapping(value = "/depart/{id}", method = RequestMethod.GET)
    public String showDepart(@PathVariable("id") Long id, Model uiModel) {
		Long user_id = (Long) session.getAttribute("user_id");
		if (user_id == null) {
			return "index";
		}
		
		User user = userService.findById(user_id);
		if (!user.getRole().getName().equals("ROLE_ADMIN")) {
			return "index";
		}

        Depart depart = departService.findById(id);
		uiModel.addAttribute("depart", depart);
        return "manage/depart_show";
    }	
    
	@RequestMapping(value="/depart", params="form", method=RequestMethod.GET)
	public String createDepartForm(Model uiModel) {
		Long user_id = (Long) session.getAttribute("user_id");
		if (user_id == null) {
			return "index";
		}
		
		User user = userService.findById(user_id);
		if (!user.getRole().getName().equals("ROLE_ADMIN")) {
			return "index";
		}

		Depart depart = new Depart();
		uiModel.addAttribute("depart", depart);
		return "manage/depart_create";
	}  
	
	@RequestMapping(value = "/depart/{id}", params = "form", method = RequestMethod.POST)
    public String updateDepart(@Valid Depart depart, BindingResult bindingResult, Model uiModel, 
    		HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes, Locale locale) {
		logger.info("Updating department");
		
		Long user_id = (Long) session.getAttribute("user_id");
		if (user_id == null) {
			return "index";
		}
		
		User user = userService.findById(user_id);
		if (!user.getRole().getName().equals("ROLE_ADMIN")) {
			return "index";
		}

		
        if (bindingResult.hasErrors()) {
        	uiModel.addAttribute("message", new Message("error", messageSource.getMessage("contact_save_fail", new Object[]{}, locale)));        	
            uiModel.addAttribute("depart", depart);
            return "manage/depart_update";
        }
        uiModel.asMap().clear();
        redirectAttributes.addFlashAttribute("message", new Message("success", messageSource.getMessage("contact_save_success", new Object[]{}, locale)));        

        departService.save(depart);
        return "redirect:/manage/depart/" + UrlUtil.encodeUrlPathSegment(depart.getIdDepart().toString(), httpServletRequest);
    }	

	@RequestMapping(value = "/depart/{id}", params = "form", method = RequestMethod.GET)
    public String updateDepartForm(@PathVariable("id") Long id, Model uiModel) {
		Long user_id = (Long) session.getAttribute("user_id");
		if (user_id == null) {
			return "index";
		}
		
		User user = userService.findById(user_id);
		if (!user.getRole().getName().equals("ROLE_ADMIN")) {
			return "index";
		}

		
        uiModel.addAttribute("depart", departService.findById(id));
        return "manage/depart_update";
	}
	
	@RequestMapping(value = "/depart/{id}", params = "delete", method = RequestMethod.POST)
    public String deleteDepart(@PathVariable("id") Long id, Model uiModel) {
		logger.info("Deleting department");
		Long user_id = (Long) session.getAttribute("user_id");
		if (user_id == null) {
			return "index";
		}
		
		User user = userService.findById(user_id);
		if (!user.getRole().getName().equals("ROLE_ADMIN")) {
			return "index";
		}

		Depart depart = departService.findById(id);
        departService.delete(depart);
        
		List<Depart> departs = departService.findAll();
		uiModel.addAttribute("departs", departs);
		logger.info("No. of departs: " + departs.size());
		return "manage/depart_list";        
    }	
	
	@RequestMapping(value="/employee", method = RequestMethod.GET)
	public String listEmployees(Model uiModel) {
		logger.info("Listing employees");
		
		Long user_id = (Long) session.getAttribute("user_id");
		if (user_id == null) {
			return "index";
		}
		
		User user = userService.findById(user_id);
		if (!user.getRole().getName().equals("ROLE_ADMIN")) {
			return "index";
		}

		List<Depart> departs = departService.findAll();
		uiModel.addAttribute("departs", departs);
		logger.info("No. of departs: " + departs.size());
		
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(User.class);
		detachedCriteria.add(Property.forName("isEmployer").eq(true));
		List<User> employees = userService.findByCriteria(detachedCriteria);
		uiModel.addAttribute("employees", employees);
		logger.info("No. of employees: " + employees.size());
		return "manage/employee_list";
	}	
	
	@RequestMapping(value="/employee", params="form", method=RequestMethod.POST)
	public String createEmployee(User user, BindingResult bindingResult, Model uiModel, 
    		HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes, 
    		Locale locale, @RequestParam(value="departId", required=true) Integer departId) {
		final Logger logger = LoggerFactory.getLogger(ManageController.class);
		logger.info("Create user as employer");
		
		Long user_id = (Long) session.getAttribute("user_id");
		if (user_id == null) {
			return "index";
		}
		
		User currentUser = userService.findById(user_id);
		if (!currentUser.getRole().getName().equals("ROLE_ADMIN")) {
			return "index";
		}

		
		if (bindingResult.hasErrors()) {
			uiModel.addAttribute("message", new Message("error", 
					messageSource.getMessage("employee_save_fail", new Object[]{}, locale)));
			uiModel.addAttribute("user", user);
			return "manage/employee_create";
		}
		
		uiModel.asMap().clear();
		redirectAttributes.addFlashAttribute("message", new Message("success",
				messageSource.getMessage("empolyee_save_success",  new Object[]{}, locale)));
		
		logger.info("employer id" + user.getUserId() + "employer name" + user.getName());
		user.setDepartId(departId);
		user.setIsEmployer(true);
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Role.class);
		detachedCriteria.add(Property.forName("name").eq("ROLE_MARKET"));
		List<Role> roles = roleService.findByCriteria(detachedCriteria);
		user.setRole(roles.get(0));
		userService.save(user);
		return "redirect:/manage/employee/" + UrlUtil.encodeUrlPathSegment(user.getUserId().toString(), 
																   httpServletRequest);
	}
		
	@RequestMapping(value="/employee", params="form", method=RequestMethod.GET)
	public String createEmployeeForm(Model uiModel) {
		Long user_id = (Long) session.getAttribute("user_id");
		if (user_id == null) {
			return "index";
		}
		
		User currentUser = userService.findById(user_id);
		if (!currentUser.getRole().getName().equals("ROLE_ADMIN")) {
			return "index";
		}
		
		User user = new User();
		uiModel.addAttribute("user", user);
		List<Depart> departs = departService.findAll();
		uiModel.addAttribute("departs", departs);
		logger.info("No. of departs: " + departs.size());
		return "manage/employee_create";
	}
	
	@RequestMapping(value = "/employee/{id}", params = "form", method = RequestMethod.POST)
    public String updateEmployee(@Valid User user, BindingResult bindingResult, Model uiModel, 
    		HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes, Locale locale,
    		@RequestParam(value="departId", required=true) Integer departId) {
		logger.info("Updating employee");
		Long user_id = (Long) session.getAttribute("user_id");
		if (user_id == null) {
			return "index";
		}
		
		User currentUser = userService.findById(user_id);
		if (!currentUser.getRole().getName().equals("ROLE_ADMIN")) {
			return "index";
		}
		
        if (bindingResult.hasErrors()) {
        	uiModel.addAttribute("message", new Message("error", messageSource.getMessage("contact_save_fail", new Object[]{}, locale)));        	
            uiModel.addAttribute("user", user);
            return "manage/employee_update";
        }
        uiModel.asMap().clear();
        redirectAttributes.addFlashAttribute("message", new Message("success", messageSource.getMessage("contact_save_success", new Object[]{}, locale)));        

        user.setDepartId(departId);
        user.setIsEmployer(true);
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Role.class);
		detachedCriteria.add(Property.forName("name").eq("ROLE_MARKET"));
		List<Role> roles = roleService.findByCriteria(detachedCriteria);
		user.setRole(roles.get(0));        
        userService.update(user);
        
		List<Depart> departs = departService.findAll();
		uiModel.addAttribute("departs", departs);
        
        return "redirect:/manage/employee/" + UrlUtil.encodeUrlPathSegment(user.getUserId().toString(), httpServletRequest);		
	}

	@RequestMapping(value = "/employee/{id}", params = "form", method = RequestMethod.GET)
    public String updateEmployeeForm(@PathVariable("id") Long id, Model uiModel) {
		Long user_id = (Long) session.getAttribute("user_id");
		if (user_id == null) {
			return "index";
		}
		
		User currentUser = userService.findById(user_id);
		if (!currentUser.getRole().getName().equals("ROLE_ADMIN")) {
			return "index";
		}
		
        uiModel.addAttribute("user", userService.findById(id));
		List<Depart> departs = departService.findAll();
		uiModel.addAttribute("departs", departs);        
        return "manage/employee_update";
	}	
	
	@RequestMapping(value = "/employee/{id}", method = RequestMethod.GET)
    public String showEmployee(@PathVariable("id") Long id, Model uiModel) {
		Long user_id = (Long) session.getAttribute("user_id");
		if (user_id == null) {
			return "index";
		}
		
		User currentUser = userService.findById(user_id);
		if (!currentUser.getRole().getName().equals("ROLE_ADMIN")) {
			return "index";
		}
		
        User user = userService.findById(id);
		uiModel.addAttribute("user", user);
		List<Depart> departs = departService.findAll();
		uiModel.addAttribute("departs", departs);
        return "manage/employee_show";
    }	
	
	@RequestMapping(value = "/employee/{id}", params = "delete", method = RequestMethod.POST)
    public String deleteEmployee(@PathVariable("id") Long id, Model uiModel) {
		logger.info("Deleting employee");
		Long user_id = (Long) session.getAttribute("user_id");
		if (user_id == null) {
			return "index";
		}
		
		User currentUser = userService.findById(user_id);
		if (!currentUser.getRole().getName().equals("ROLE_ADMIN")) {
			return "index";
		}		

		User user = userService.findById(id);
        userService.delete(user);
       
		logger.info("Listing employees");
		List<Depart> departs = departService.findAll();
		uiModel.addAttribute("departs", departs);
		logger.info("No. of departs: " + departs.size());
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(User.class);
		detachedCriteria.add(Property.forName("isEmployer").eq(true));
		List<User> employees = userService.findByCriteria(detachedCriteria);
		uiModel.addAttribute("employees", employees);
		logger.info("No. of employees: " + employees.size());
		return "redirect:/manage/employee";
    }		
}
