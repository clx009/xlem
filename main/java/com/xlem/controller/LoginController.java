package com.xlem.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.xlem.dao.Depart;
import com.xlem.dao.Role;
import com.xlem.dao.Travservice;
import com.xlem.dao.User;
import com.xlem.service.TravService;
import com.xlem.utils.Message;
import com.xlem.utils.SeriesNumber;

import common.CommonMethod;
import service.comm.reg.baseTravelAgency.BaseTravelAgencyService;
import service.sys.depart.DepartService;
import service.sys.role.RoleService;
import service.sys.user.UserService;

@RequestMapping("/login")
@Controller
public class LoginController {
	@Autowired 
	private UserService userService;
	@Autowired
	private TravService TSService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private DepartService departService;
	@Autowired
	private BaseTravelAgencyService baseTravelAgencyService;
	
	@Autowired
	MessageSource messageSource;
	
	private String smsPhone = null;
	private String smsCode = null;
	private String vCode = null;
	private User resetPwdUser = null;
	
	private String callbackUrl = null;
	
	@Autowired
	private HttpSession session;
	
	@RequestMapping(value="guest", method=RequestMethod.GET)
	public String personLoginForm(Model uiModel) {
		final Logger logger = (Logger) LoggerFactory.getLogger(RegisterController.class);
		logger.info("show login page.");
	
		uiModel.addAttribute("loginHeaderText", "游客登录");
		return "login/guest";
	}
	
	@RequestMapping(value="manage", method=RequestMethod.GET)
	public String manageLoginForm(Model uiModel) {
		final Logger logger = (Logger) LoggerFactory.getLogger(RegisterController.class);
		logger.info("show manage login page.");
		
		uiModel.addAttribute("loginHeaderText", "管理员登录");
		return "login/manage";
	
	}	
	
	@RequestMapping(value="market", method=RequestMethod.GET)
	public String marketLoginForm(Model uiModel) {
		final Logger logger = (Logger) LoggerFactory.getLogger(RegisterController.class);
		logger.info("show login page.");
		
		uiModel.addAttribute("loginHeaderText", "市场部登录");
		return "login/market";
	
	}
	
	@RequestMapping(value="depart", method=RequestMethod.GET)
	public String departLoginForm(Model uiModel) {
		final Logger logger = (Logger) LoggerFactory.getLogger(LoginController.class);
		logger.info("show login page.");
		
		uiModel.addAttribute("loginHeaderText", "部门登录");
		List<Depart> departs = departService.findAll();
		uiModel.addAttribute("departs", departs);
		return "login/depart";
	}
	
	@RequestMapping(value="finance", method=RequestMethod.GET)
	public String financeLoginForm(Model uiModel) {
		final Logger logger = (Logger) LoggerFactory.getLogger(RegisterController.class);
		logger.info("show login page.");
		
		uiModel.addAttribute("loginHeaderText", "财务部登录");
		return "login/finance";
	}	
	
	@RequestMapping(value="travservice", method=RequestMethod.GET)
	public String travserviceLoginForm(Model uiModel) {
		final Logger logger = (Logger) LoggerFactory.getLogger(RegisterController.class);
		logger.info("show login page.");
		
		uiModel.addAttribute("loginHeaderText", "旅行社登录");
		return "login/travservice";
	
	}	
	
	@RequestMapping(value="manage_login", method=RequestMethod.POST)
	public @ResponseBody String manageLogin(@RequestParam(value="loginName") String loginName,
											@RequestParam(value="passwd") String passwd) {
		
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(User.class);
		detachedCriteria.add(Property.forName("name").eq(loginName));
		detachedCriteria.add(Property.forName("isEmployer").eq(true));
		List<User> users = userService.findByCriteria(detachedCriteria);
		if (users != null && users.size() > 0) {
			User user = users.get(0);
			
			if (!user.getRole().getName().equals("ROLE_ADMIN")) {
				return "{\"msg\":\"notadmin" + "\"}"; 
			}
			
			if (user.getPwd().equals(passwd)) {
				session.setAttribute("user_id", user.getUserId());
				return "{\"url\":\"/xlem/manage" + "\"}";	
			}
			else {
				return "{\"msg\":\"notmatch" + "\"}"; 
			}
		}
		
		return "{\"msg\":\"invalid" + "\"}"; 
	}
	
	@RequestMapping(value="market_login", method=RequestMethod.POST)
	public @ResponseBody String marketLogin(@RequestParam(value="loginName") String loginName,
											@RequestParam(value="passwd") String passwd) {
		
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(User.class);
		detachedCriteria.add(Property.forName("name").eq(loginName));
		detachedCriteria.add(Property.forName("isEmployer").eq(true));
		List<User> users = userService.findByCriteria(detachedCriteria);
		if (users != null && users.size() > 0) {
			User user = users.get(0);
			Integer departId = user.getDepartId();
			if (departId == null) {
				return "{\"msg\":\"nodepart" + "\"}"; 
			}
			
			Depart depart = departService.findById(Long.valueOf(String.valueOf(departId)));
			if (!depart.getName().equals("市场部")) {
				return "{\"msg\":\"wrongdepart" + "\"}"; 
			}
			
			if (user.getPwd().equals(passwd)) {
				session.setAttribute("user_id", user.getUserId());
				return "{\"url\":\"/xlem/market/init/" + users.get(0).getUserId() + "\"}";	
			}
			else {
				return "{\"msg\":\"notmatch" + "\"}"; 
			}
		}
		
		return "{\"msg\":\"invalid" + "\"}"; 
	}
	
	@RequestMapping(value="finance_login", method=RequestMethod.POST)
	public @ResponseBody String financeLogin(@RequestParam(value="loginName") String loginName,
											@RequestParam(value="passwd") String passwd) {
		
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(User.class);
		detachedCriteria.add(Property.forName("name").eq(loginName));
		detachedCriteria.add(Property.forName("isEmployer").eq(true));
		List<User> users = userService.findByCriteria(detachedCriteria);
		if (users != null && users.size() > 0) {
			User user = users.get(0);
			Integer departId = user.getDepartId();
			if (departId == null) {
				return "{\"msg\":\"nodepart" + "\"}"; 
			}
			
			Depart depart = departService.findById(Long.valueOf(String.valueOf(departId)));
			if (!depart.getName().equals("财务部")) {
				return "{\"msg\":\"wrongdepart" + "\"}"; 
			}

			if (user.getPwd().equals(passwd)) {
				session.setAttribute("user_id", user.getUserId());
				return "{\"url\":\"/xlem/finance/init/" + users.get(0).getUserId() + "\"}";	
			}
			else {
				return "{\"msg\":\"notmatch" + "\"}"; 
			}
		}
		
		return "{\"msg\":\"invalid" + "\"}"; 
	}	

	@RequestMapping(value="travservice_login", method=RequestMethod.POST)
	public @ResponseBody String travserviceLogin(@RequestParam(value="loginName") String loginName,
												 @RequestParam(value="passwd") String passwd) {
		
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(User.class);
		detachedCriteria.add(Property.forName("name").eq(loginName));
		List<User> users = userService.findByCriteria(detachedCriteria);
		if (users != null && users.size() > 0) {
			User user = users.get(0);
			if (user.getPwd().equals(passwd)) {
				session.setAttribute("user_id", user.getUserId());
				return "{\"url\":\"/xlem/ts_account/travelAgency/" + users.get(0).getUserId() + "\"}";	
			}
			else {
				return "{\"msg\":\"notmatch" + "\"}"; 
			}
		}
		
		return "{\"msg\":\"invalid" + "\"}"; 
	}	
	
	@RequestMapping(value="/guestName", method=RequestMethod.POST)
    public @ResponseBody String personLoginByName(@RequestParam(value="login_name") String loginName,
    								@RequestParam(value="passwd") String passwd) {
		final Logger logger = (Logger) LoggerFactory.getLogger(RegisterController.class);
		logger.info("Personal login");
	
		List<User> users = userService.findByProperty("name", loginName);
		if (users.size() > 0) {
			String pwd = users.get(0).getPwd();
			if (pwd.equals(passwd)) {
				session.setAttribute("user_id", users.get(0).getUserId());
				return "{\"url\":\"/xlem/account/tourist/" + users.get(0).getUserId() + "\"}";	
			}
		}
		
		return "{\"url\":\"/xlem/login/guest" + "\"}";	
	}
	
	@RequestMapping(value="/guestSMS", method=RequestMethod.POST)
    public @ResponseBody String personLoginBySMS(@RequestParam(value="login_name") String loginName, 
    							   @RequestParam(value="sms_code") String sms_code) {
		final Logger logger = (Logger) LoggerFactory.getLogger(RegisterController.class);
		logger.info("Personal login");
		
		smsPhone = (String) session.getAttribute("sms_phone");
		smsCode = (String) session.getAttribute("sms_code");

		if (smsPhone.equals(loginName) && smsCode.equals(sms_code)) {
			List<User> users = userService.findByProperty("name", loginName);
			User user = null;
			if (users.isEmpty()) {
				user = new User();
				user.setName(loginName);
				
				DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Role.class);
				detachedCriteria.add(Property.forName("name").eq("ROLE_TOURIST"));
				List<Role> roles = roleService.findByCriteria(detachedCriteria);
				if (roles == null || roles.size() == 0) {
					RuntimeException re = new RuntimeException("ROLE_TOURIST not exist in DB.");
					throw re;
				}
				user.setRole(roles.get(0));
				user.setMobilePhone(loginName);
				user.setPwd(smsCode);
			
				userService.save(user);
			}
			else {
				user = users.get(0);
				user.setPwd(sms_code);
				userService.saveOrUpdate(user);
			}
			session.setAttribute("user_id", user.getUserId());
			return "{\"url\":\"/xlem/account/tourist/" + user.getUserId() + "\"}";	
		}
		
		return "{\"url\":\"/xlem/login/guest" + "\"}";	
	}
	
	@RequestMapping(value="general", method=RequestMethod.POST)
	public @ResponseBody String general(@RequestParam(value="callbackUrl") String callbackUrl) {
		final Logger logger = (Logger) LoggerFactory.getLogger(LoginController.class);
		logger.info("general login callback url set.");
		
		this.callbackUrl = callbackUrl;
		return "{\"url\":\"/xlem/login/generalForm" + "\"}";	
	}
	
	@RequestMapping(value="generalForm", method=RequestMethod.GET)
	public String generalLoginForm(Model uiModel) {
		final Logger logger = (Logger) LoggerFactory.getLogger(LoginController.class);
		logger.info("general login page.");
	
		uiModel.addAttribute("loginHeaderText", "用户登录");
		return "login/login";
	}
	
	@RequestMapping(value="general_login", method=RequestMethod.POST, produces="text/plain;charset=UTF-8")
	public @ResponseBody String generalLogin(@RequestParam(value="loginName") String loginName,
												 @RequestParam(value="passwd") String passwd) {
		
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(User.class);
		detachedCriteria.add(Property.forName("name").eq(loginName));
		List<User> users = userService.findByCriteria(detachedCriteria);
		if (users != null && users.size() > 0 && callbackUrl != null) {
			User user = users.get(0);
			if (user.getPwd().equals(passwd)) {
				session.setAttribute("user_id", user.getUserId());
				return "{\"url\":\"" + callbackUrl + "\"}";	
			}
			else {
				return "{\"msg\":\"notmatch" + "\"}"; 
			}
		}
		
		return "{\"msg\":\"invalid" + "\"}"; 
	}
	
	@RequestMapping(value="userLogined", method=RequestMethod.GET)
	public @ResponseBody String userLogined() {
		Long user_id = (Long) session.getAttribute("user_id");
		if (user_id != null) {
			return "{\"msg\":\"logined" + "\"}"; 
		}
		else {
			return "{\"msg\":\"unlogined" + "\"}"; 
		}
	}
	
	@RequestMapping(value="myAccountLogin", method=RequestMethod.GET)
	public String myAccountLoginForm(Model uiModel) {
		final Logger logger = (Logger) LoggerFactory.getLogger(LoginController.class);
		logger.info("login my account page.");
		
		Long user_id = (Long) session.getAttribute("user_id");
		if (user_id != null) {
			User user = userService.findById(user_id);
			if (user.getEmployerId() != null) {
				DetachedCriteria criteria = DetachedCriteria.forClass(Depart.class);
				criteria.add(Property.forName("idDepart").eq(Long.valueOf(user.getDepartId().toString())));
				List<Depart> departs = departService.findByCriteria(criteria);
				if (departs.size() > 0) {
					switch (departs.get(0).getName()) {
					case "市场部":
						return "redirect:/market/init/" + user.getUserId();
						
					case "财务部":
						return "redirect:/finance/init/" + user.getUserId();
						
					default:
						return "redirect:index";
					}
				}
				else
					return "redirect:index";
			}
			else {
				DetachedCriteria criteria = DetachedCriteria.forClass(Travservice.class);
				criteria.add(Property.forName("user").eq(user));
				List<Travservice> travelAgencies = baseTravelAgencyService.findByCriteria(criteria);
				if (travelAgencies.size() > 0) {
					return "redirect:/ts_account/travelAgency/" + user.getUserId();	
				}
				else {
					return "redirect:/account/tourist/" + user.getUserId();	
				}
			}
		}
		
		uiModel.addAttribute("loginHeaderText", "用户登录");
		return "login/myaccount_login";
	}
	
	@RequestMapping(value="myaccount_login", method=RequestMethod.POST, produces="text/html;charset=UTF-8")
	public @ResponseBody String myAccountLogin(@RequestParam(value="loginName") String loginName,
								 @RequestParam(value="passwd") String passwd) {
		
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(User.class);
		detachedCriteria.add(Property.forName("name").eq(loginName));
		List<User> users = userService.findByCriteria(detachedCriteria);
		if (users != null && users.size() > 0) {
			User user = users.get(0);
			session.setAttribute("user_id", user.getUserId());
			if (user.getPwd().equals(passwd)) {
				if (user.getEmployerId() != null) {
					DetachedCriteria criteria = DetachedCriteria.forClass(Depart.class);
					criteria.add(Property.forName("idDepart").eq(Long.valueOf(user.getDepartId().toString())));
					List<Depart> departs = departService.findByCriteria(criteria);
					if (departs.size() > 0) {
						switch (departs.get(0).getName()) {
						case "市场部":
							return "{\"url\":\"" + "/xlem/market/init/" + user.getUserId() + "\"}";
							
						case "财务部":
							return "{\"url\":\"" + "/xlem/finance/init/" + user.getUserId() + "\"}";
							
						default:
							return "redirect:index";
						}
					}
				}
				else {
					DetachedCriteria criteria = DetachedCriteria.forClass(Travservice.class);
					criteria.add(Property.forName("user").eq(user));
					List<Travservice> travelAgencies = baseTravelAgencyService.findByCriteria(criteria);
					if (travelAgencies.size() > 0) {
						return "{\"url\":\"" + "/xlem/ts_account/travelAgency/" + user.getUserId() + "\"}";	
					}
					else {
						session.setAttribute("user_id", user.getUserId());
						return "{\"url\":\"" + "/xlem/account/tourist/" + user.getUserId() + "\"}";	
					}
				}
			}
			else {
				return "{\"msg\":\"notmatch" + "\"}"; 
			}
		}
		
		return "{\"msg\":\"invalid" + "\"}"; 
	}	
	
	@RequestMapping(value="forgetpwd", method=RequestMethod.GET)
	public String forgetPwd(Model uiModel) {
		uiModel.addAttribute("loginHeaderText", "用户密码重置");
		uiModel.addAttribute("formTitle", "忘记密码");
		return "login/forgetpwd";
	}
	
	@RequestMapping(value="forgetpwd_sms", method=RequestMethod.POST)
	public @ResponseBody String forgetPwd_SMS(@RequestParam(value="mphone") String mphone) {
		vCode = SeriesNumber.RandomString(6);
		String smsContent = "您正在重置密码，验证码为：" + vCode; 
		CommonMethod.sendSms(mphone, smsContent, "true");
		
		return "{\"msg\":\"success" + "\"}"; 
	}
	
	@RequestMapping(value="forgetpwd_verify", method=RequestMethod.POST)
	public @ResponseBody String forgetpwdVerify(@RequestParam(value="user_name") String user_name,
												@RequestParam(value="mphone") String mphone,
												@RequestParam(value="vcode") String vcode) {
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(User.class);
		detachedCriteria.add(Property.forName("name").eq(user_name));
		List<User> users = userService.findByCriteria(detachedCriteria);
		if (users.size() > 0) {
			User user = users.get(0);
			if (user.getMobilePhone().equals(mphone)) {
				if (vCode != null && vCode.equals(vcode)) {
					resetPwdUser = user;
					return "{\"msg\":\"success" + "\"}"; 
				}
				else {
					return "{\"msg\":\"ngverify" + "\"}"; 
				}
			}
			else {
				return "{\"msg\":\"wronginfo" + "\"}"; 
			}
		}
		else {
			return "{\"msg\":\"notexist" + "\"}"; 
		}
	}
	
	@RequestMapping(value="resetpwd", method=RequestMethod.GET)
	public String resetPwd(Model uiModel) {
		uiModel.addAttribute("loginHeaderText", "用户密码重置");
		uiModel.addAttribute("formTitle", "重置密码");
		return "login/resetpwd";
	}
	
	@RequestMapping(value="performResetpwd", method=RequestMethod.GET)
	public @ResponseBody String performResetpwd(@RequestParam(value="new_passwd") String new_passwd) {
		if (resetPwdUser != null && vCode != null) {
			resetPwdUser.setPwd(new_passwd);
			userService.update(resetPwdUser);
			resetPwdUser = null;
			vCode = null;
			return "{\"msg\":\"success" + "\"}"; 
		}
	
		return "{\"msg\":\"invalid" + "\"}"; 
	}
	
	@RequestMapping(value="departSelect", method=RequestMethod.GET)
	public @ResponseBody String selectLoginDepart(@RequestParam(value="departId") Long departId) {
		Depart depart = departService.findById(departId);
		if (depart.getName().equals("市场部")) {
			return "{\"msg\":\"success" + "\"," + "\"url\":\"" + "/xlem/login/market" + "\"}";	
		}
		
		if (depart.getName().equals("财务部")) {
			return "{\"msg\":\"success" + "\"," + "\"url\":\"" + "/xlem/login/finance" + "\"}";	
		}
		
		return "{\"msg\":\"invalid" + "\"}"; 
	}

}
