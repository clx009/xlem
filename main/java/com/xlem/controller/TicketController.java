package com.xlem.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.lang.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.xlem.utils.SystemSetting;
import com.xlem.utils.TicketPoolService;
import com.xlem.web.JSONTicket;
import com.xlem.web.JSON_TicketIndent;
import com.xlem.web.JSON_TicketIndentDetail;
import com.xlem.web.JSON_Tickets;

import common.CommonMethod;
import common.pay.PayParams;
import dao.comm.baseRewardType.BaseRewardTypeDao;
import dao.ticket.ticketIndent.TicketIndentDao;
import dao.ticket.ticketIndentDetail.TicketIndentDetailDao;
import service.comm.baseCashAccount.BaseCashAccountService;
import service.comm.baseRewardAccount.BaseRewardAccountService;
import service.comm.baseTranRec.BaseTranRecService;
import service.comm.reg.baseTravelAgency.BaseTravelAgencyService;
import service.sys.user.UserService;
import service.ticket.ticketIndent.TicketIndentService;
import service.ticket.ticketIndentDetail.TicketIndentDetailService;
import service.ticket.ticketPrice.TicketPriceService;
import service.ticket.ticketType.TicketTypeService;

import com.xlem.dao.BaseCashAccount;
import com.xlem.dao.BaseRewardAccount;
import com.xlem.dao.BaseRewardType;
import com.xlem.dao.BaseTranRec;
import com.xlem.dao.TicketIndent;
import com.xlem.dao.TicketIndentDetail;
import com.xlem.dao.TicketPrice;
import com.xlem.dao.TicketType;
import com.xlem.dao.Travservice;
import com.xlem.dao.User;
import com.xlem.utils.Page;
import com.xlem.utils.RESTFulTicket;
import com.xlem.utils.RESTFulTicketIndent;
import com.xlem.utils.RESTFulTicketIndents;
import com.xlem.utils.RESTFulTickets;
import com.xlem.utils.SeriesNumber;

@RequestMapping("/ticket")
@Controller
public class TicketController {
	
	@Autowired
	private SystemSetting systemSetting;
	
	@Autowired
	private TicketIndentDao ticketIndentDao;
	@Autowired
	private TicketIndentDetailDao ticketIndentDetailDao;
	
	@Autowired
	private TicketIndentService ticketIndentService;
	@Autowired
	private TicketIndentDetailService ticketIndentDetailService;
	
	@Autowired
	private BaseTravelAgencyService baseTravelAgencyService;
	@Autowired
	private BaseCashAccountService baseCashAccountService;
	@Autowired
	private BaseRewardAccountService baseRewardAccountService;
	@Autowired
	private BaseRewardTypeDao baseRewardTypeDao;
	@Autowired
	private BaseTranRecService baseTranRecService;
	@Autowired
	private TicketTypeService ticketTypeService;
	@Autowired
	private TicketPriceService ticketPriceService;
	
	@Autowired
	private TicketPoolService ticketPoolService;
	
	@Autowired
	private HttpSession session;
	@Autowired
	private UserService userService;
	
	private RESTFulTickets restTickets = null;
	private List<RESTFulTicket> tickets = null;
	
	private TicketIndent ticketIndent = null;
	private List<TicketIndentDetail> ticketIndentDetails = null;
	
	private ArrayList<RESTFulTicket> shopcartTickets = null;
	private HashMap<TicketIndentDetail, RESTFulTicket> indentMap = null;
	
	final Logger logger = LoggerFactory.getLogger(TicketController.class);
	
	private int PageSize = 2;

	private void initTicketList() {
		// call xlpw web service here
		final String uri = systemSetting.getXlpwUrl() + "/rest/ticket/list";
		
	    RestTemplate restTemplate = new RestTemplate();
	     
	    restTickets = restTemplate.getForObject(uri, RESTFulTickets.class);
	    tickets = restTickets.getRESTFulTickets();
	    if (tickets == null) {
	    	return;
	    }
	    
		for (RESTFulTicket ticket : tickets) {
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(TicketType.class);
			detachedCriteria.add(Property.forName("ticketName").eq(ticket.getName()));
			List<TicketType> ticketTypes = ticketTypeService.findByCriteria(detachedCriteria);
			
			if (ticketTypes.size() > 0) {
				DetachedCriteria dc = DetachedCriteria.forClass(TicketPrice.class);
				dc.add(Property.forName("ticketType").eq(ticketTypes.get(0)));
				List<TicketPrice> ticketPrices = ticketPriceService.findByCriteria(dc);
				TicketPrice ticketPrice = ticketPrices.get(0);
				
				if (ticket.getInfo() != null) {
					if (ticketTypes.get(0).getIntro() == null 
							|| !ticketTypes.get(0).getIntro().equals(ticket.getInfo())) {
						TicketType ticketType = ticketTypes.get(0);
						ticketType.setIntro(ticket.getInfo());
						ticketTypeService.update(ticketType);
					}
				}
				
				if (!ticket.getPrice().equals(ticketPrice.getPrintPrice())
						|| !ticket.getStart().equals(ticketPrice.getStartDate())
						|| !ticket.getEnd().equals(ticketPrice.getEndDate())) {
					ticketPrice.setPrintPrice(ticket.getPrice());
					ticketPrice.setStartDate(ticket.getStart());
					ticketPrice.setEndDate(ticket.getEnd());
					ticketPriceService.update(ticketPrice);
				}			
			}
			else {
				TicketType ticketType = new TicketType(ticket.getName(), ticket.getInfo());
				ticketTypeService.save(ticketType);
				TicketPrice ticketPrice = new TicketPrice(ticketType, ticket.getPrice(), ticket.getPrice(), ticket.getStart(), ticket.getEnd());
				ticketPrice.setToplimit(5000);
				ticketPrice.setRemainToplimit(5000);
				ticketPriceService.save(ticketPrice);
			}
		}	    
	}	
	
	@RequestMapping(value="list", method=RequestMethod.GET)
	public String ticketList() {
		// call xlpw web service here
		initTicketList();
		return "ticket/list";
	}
	
	@RequestMapping(value="suite_list", method=RequestMethod.GET)
	public String suiteList() {
		// call xlpw web service here
		initTicketList();
		return "ticket/suite_list";
	}
	
	@RequestMapping(value = "/getData", method = RequestMethod.GET)  
	public @ResponseBody JSON_Tickets getData(@RequestParam(value = "offset") int offset) { 
		if (tickets == null) {
			JSON_Tickets result = new JSON_Tickets();
			result.setNum(0);
			return result;
		}
		
		int allRecords = tickets.size();
		List<JSONTicket> jsons = new ArrayList<JSONTicket>();
		
		int count = 0;
		int ticket_num = 0;
		for (int i = offset; i < allRecords; i++) {
			if (!tickets.get(i).getIsSuite()) {
				ticket_num++;
				
				if (count > 8) {
					break;
				}
				count++;
				
				JSONTicket jsonTicket = new JSONTicket();
				jsonTicket.setName(tickets.get(i).getName());
				jsonTicket.setPrice(tickets.get(i).getPrice());
				
				String image = systemSetting.getImageForTicket(tickets.get(i).getName());
				if (image == null) {
					image = systemSetting.getDefaultImage();
				}
				String obj = "/xlem/ticketShow?image=" + image + "&name=" + tickets.get(i).getName() + "&price=" + tickets.get(i).getPrice();
				jsonTicket.setImage(obj);
				
				jsons.add(jsonTicket);
			}
		}
		
		JSON_Tickets result = new JSON_Tickets();
		result.setNum(ticket_num);
		result.setTickets(jsons);
		return result;
	}
	
	@RequestMapping(value = "/getSuiteData", method = RequestMethod.GET)  
	public @ResponseBody JSON_Tickets getSuiteData(@RequestParam(value = "offset") int offset) {  
		int allRecords = tickets.size();
		List<JSONTicket> jsons = new ArrayList<JSONTicket>();
		
		int count = 0;
		int suite_num = 0;
		for (int i = offset; i < allRecords; i++) {
			if (tickets.get(i).getIsSuite()) {
				suite_num++;
				
				if (count > 8) {
					break;
				}
				count++;
				
				JSONTicket jsonTicket = new JSONTicket();
				jsonTicket.setName(tickets.get(i).getName());
				jsonTicket.setPrice(tickets.get(i).getPrice());
				
				String image = systemSetting.getImageForTicket(tickets.get(i).getName());
				if (image == null) {
					image = systemSetting.getDefaultImage();
				}
				String obj = "/xlem/ticketShow?image=" + image + "&name=" + tickets.get(i).getName() + "&price=" + tickets.get(i).getPrice();
				jsonTicket.setImage(obj);
				
				jsons.add(jsonTicket);
			}
		}
		
		JSON_Tickets result = new JSON_Tickets();
		result.setNum(suite_num);
		result.setTickets(jsons);
		return result;
	}
	
	@RequestMapping(value="showInfo", method=RequestMethod.GET)
	public String showInfo(@RequestParam(value="name") String tName, Model uiModel) {
		logger.info("show ticket information page.");
		String imageName = systemSetting.getImageForTicket(tName);
		if (imageName == null) {
			imageName = systemSetting.getDefaultImage();
		}
		
		String tInfo = null;
		Double tPrice = 0.0;
		
		for (RESTFulTicket ticket : tickets) {
			if (ticket.getName().equals(tName)) {
				tInfo = ticket.getInfo();
				tPrice = ticket.getPrice();
			}
		}

		uiModel.addAttribute("name", tName);
		uiModel.addAttribute("image", imageName);
		uiModel.addAttribute("price", tPrice);
		uiModel.addAttribute("info", tInfo);
		
		return "ticket/info";
	}
	
	@RequestMapping(value="booking", method=RequestMethod.GET)
	public String ticketBooking(@RequestParam(value="name") String tName, Model uiModel) {
		logger.info("booking ticket now");
		
		JSONTicket json = new JSONTicket();
		ticketIndent = new TicketIndent(
							ticketIndentDao.getNextCode("TicketIndent", "indentId", "00000000000000000000000000000000", null));
		
		if (tickets == null) {
			initTicketList();
		}
		
		if (indentMap == null) {
			indentMap = new HashMap<TicketIndentDetail, RESTFulTicket>();
		}
		
		Double totalPrice = 0.00;
		
		for (RESTFulTicket rest : tickets) {
			if (rest.getName().equals(tName)) {
				json.setName(tName);
				json.setPrice(rest.getPrice());
				totalPrice += rest.getPrice();
				String image = systemSetting.getImageForTicket(rest.getName());
				if (image == null) {
					image = systemSetting.getDefaultImage();
				}
				String obj = "/xlem/ticketShow?image=" + image;
				json.setImage(image);
				
				Date now = new Date();
				
				DetachedCriteria dc = DetachedCriteria.forClass(TicketType.class);
				dc.add(Property.forName("ticketName").eq(rest.getName()));
				List<TicketType> ticketTypes = ticketTypeService.findByCriteria(dc);
				TicketType ticketType = ticketTypes.get(0);
				
				dc = DetachedCriteria.forClass(TicketPrice.class);
				dc.add(Property.forName("ticketType").eq(ticketType));
				dc.add(Property.forName("printPrice").eq(rest.getPrice()));
				dc.add(Property.forName("startDate").eq(rest.getStart()));
				dc.add(Property.forName("endDate").eq(rest.getEnd()));
				List<TicketPrice> ticketPrices = ticketPriceService.findByCriteria(dc);
				TicketPrice ticketPrice = ticketPrices.get(0);
				
				DateFormat dfm = new SimpleDateFormat("yyyyMMdd");
				String date = dfm.format(new Date());
				TicketIndentDetail ticketIndentDetail = new TicketIndentDetail(
										ticketIndentDetailDao.getNextIndentCode(date + "00000000", date));
				ticketIndentDetail.setTicketIndent(ticketIndent);
				ticketIndentDetail.setTicketPrice(ticketPrice);
				ticketIndentDetail.setAmount(1);
				ticketIndentDetail.setUseDate(new Date());
				ticketIndentDetail.setSubtotal(ticketPrice.getUnitPrice());
				
				Set<TicketIndentDetail> indentDetails = new HashSet<TicketIndentDetail>();
				indentDetails.add(ticketIndentDetail);
				ticketIndentDetails = new ArrayList<TicketIndentDetail>();
				ticketIndentDetails.add(ticketIndentDetail);
				ticketIndent.setTicketIndentDetails(indentDetails);
				ticketIndent.setDetails(ticketIndentDetails);
				
				indentMap.put(ticketIndentDetail, rest);
				
				break;
			}
 		}
		ticketIndent.setTotalPrice(totalPrice);
		
		List<JSONTicket> jsons = new ArrayList<JSONTicket>();
		jsons.add(json);
		
		uiModel.addAttribute("tickets", jsons);
		uiModel.addAttribute("ticketIndent", ticketIndent);
		return "ticket/balance";
	}
	
	@RequestMapping(value="enterShopcart", method=RequestMethod.GET)
	public String enterShopCart(@RequestParam(value="name") String tName, Model uiModel) {
		logger.info("enter ticket shopcart now");
		
		if (tickets == null) {
			initTicketList();
		}
		
		if (indentMap == null) {
			indentMap = new HashMap<TicketIndentDetail, RESTFulTicket>();
		}
		
		for (RESTFulTicket rest : tickets) {
			if (rest.getName().equals(tName)) {
				if (shopcartTickets == null) {
					shopcartTickets = new ArrayList<RESTFulTicket>();
				}
				shopcartTickets.add(rest);
				uiModel.addAttribute("ticket", rest);
				return "ticket/shopcart";
			}
		}
		
		return "index";
	}
	
	@RequestMapping(value="shopcartBalance", method=RequestMethod.GET)
	public String shopcartBalance(Model uiModel) {
		logger.info("booking tickets in shopcart now");
		
		if (shopcartTickets == null) {
			return "index";
		}

		List<JSONTicket> jsons = new ArrayList<JSONTicket>();
		ticketIndent = new TicketIndent(
							ticketIndentDao.getNextCode("TicketIndent", "indentId", "00000000000000000000000000000000", null));
		
		Double totalPrice = 0.0;
		ticketIndentDetails = new ArrayList<TicketIndentDetail>();
		Set<TicketIndentDetail> indentDetails = new HashSet<TicketIndentDetail>();
		for (RESTFulTicket rest : shopcartTickets) {
			JSONTicket json = new JSONTicket();
			json.setName(rest.getName());
			json.setPrice(rest.getPrice());
			String image = systemSetting.getImageForTicket(rest.getName());
			if (image == null) {
				image = systemSetting.getDefaultImage();
			}
			String obj = "/xlem/ticketShow?image=" + image;
			json.setImage(image);
				
			Date now = new Date();
			
			DetachedCriteria dc = DetachedCriteria.forClass(TicketType.class);
			dc.add(Property.forName("ticketName").eq(rest.getName()));
			List<TicketType> ticketTypes = ticketTypeService.findByCriteria(dc);
			TicketType ticketType = ticketTypes.get(0);
			
			dc = DetachedCriteria.forClass(TicketPrice.class);
			dc.add(Property.forName("ticketType").eq(ticketType));
			dc.add(Property.forName("printPrice").eq(rest.getPrice()));
			dc.add(Property.forName("startDate").eq(rest.getStart()));
			dc.add(Property.forName("endDate").eq(rest.getEnd()));
			List<TicketPrice> ticketPrices = ticketPriceService.findByCriteria(dc);
			TicketPrice ticketPrice = ticketPrices.get(0);
			
			DateFormat dfm = new SimpleDateFormat("yyyyMMdd");
			String date = dfm.format(new Date());
			TicketIndentDetail ticketIndentDetail = new TicketIndentDetail(
										ticketIndentDetailDao.getNextIndentCode(date + "00000000", date));
			ticketIndentDetail.setTicketIndent(ticketIndent);
			ticketIndentDetail.setTicketPrice(ticketPrice);
			ticketIndentDetail.setAmount(1);
			ticketIndentDetail.setUseDate(new Date());
			ticketIndentDetail.setSubtotal(ticketPrice.getUnitPrice());
			
			totalPrice += ticketPrice.getUnitPrice();
				
			indentDetails.add(ticketIndentDetail);
			ticketIndentDetails.add(ticketIndentDetail);
			
			indentMap.put(ticketIndentDetail, rest);

			jsons.add(json);
 		}
		ticketIndent.setDetails(ticketIndentDetails);
		ticketIndent.setTicketIndentDetails(indentDetails);
		ticketIndent.setTotalPrice(totalPrice);
		
		uiModel.addAttribute("tickets", jsons);
		uiModel.addAttribute("ticketIndent", ticketIndent);
		return "ticket/balance";		
	}
	
	@RequestMapping(value="amountChange", method=RequestMethod.POST)
	public @ResponseBody String amountChange(@RequestParam(value="indentDetailId") String indentDetailId,
											 @RequestParam(value="amount") Integer amount) {
		if (ticketIndent != null) {
			for (TicketIndentDetail indentDetail : ticketIndentDetails) {
				if (indentDetail.getIndentDetailId().equals(indentDetailId)) {
					indentDetail.setAmount(amount);
					Double increase = amount * indentDetail.getTicketPrice().getUnitPrice() - indentDetail.getSubtotal();
					indentDetail.setSubtotal(amount * indentDetail.getTicketPrice().getUnitPrice());
					ticketIndent.setTotalPrice(ticketIndent.getTotalPrice() + increase);
					return "{\"msg\":\"success'" + "'\"}"; 
				}
			}
		}
		
		return "{\"msg\":\"invalid'" + "'\"}"; 
	}
	
	@RequestMapping(value="useDateChange", method=RequestMethod.POST)
	public @ResponseBody String useDateChange(@RequestParam(value="indentDetailId") String indentDetailId,
											 @RequestParam(value="useDate") Date useDate) {
		if (ticketIndent != null) {
			for (TicketIndentDetail indentDetail : ticketIndentDetails) {
				if (indentDetail.getIndentDetailId().equals(indentDetailId)) {
					indentDetail.setUseDate(useDate);;
					return "{\"msg\":\"success'" + "\"}"; 
				}
			}
		}
		
		return "{\"msg\":\"invalid'" + "\"}"; 
	}
	
	@RequestMapping(value="deleteIndent", method=RequestMethod.POST)
	public @ResponseBody JSON_TicketIndent deleteIndent(@RequestParam(value="indentDetailId") String indentDetailId) {
		JSON_TicketIndent result = new JSON_TicketIndent();
		
		Double decrease = 0.00;

		result.setBookDate(ticketIndent.getBookDate());
		result.setIndentId(ticketIndent.getIndentId());
		result.setStatus(ticketIndent.getStatus());
		result.setTravelAgencyName(ticketIndent.getTravelAgencyName());
			
		List<JSON_TicketIndentDetail> details = new ArrayList<JSON_TicketIndentDetail>();
		Set<TicketIndentDetail> indentDetails = new HashSet<TicketIndentDetail>();
		for (TicketIndentDetail indentDetail : ticketIndent.getTicketIndentDetails()) {
			if (indentDetail.getIndentDetailId().equals(indentDetailId)) {
				if (shopcartTickets != null) {
					RESTFulTicket removedTicket = indentMap.get(indentDetail);
					shopcartTickets.remove(removedTicket);
				}
				decrease = indentDetail.getSubtotal();
			}
			else {
				indentDetails.add(indentDetail);
				for (RESTFulTicket rest : tickets) {
					if (rest.getName().equals(indentDetail.getTicketPrice().getTicketType().getTicketName())) {
						JSONTicket json = new JSONTicket();
						json.setName(rest.getName());
						json.setPrice(rest.getPrice());
						String image = systemSetting.getImageForTicket(rest.getName());
						if (image == null) {
							image = systemSetting.getDefaultImage();
						}
						String obj = "/xlem/ticketShow?image=" + image;
						json.setImage(image);
					
						JSON_TicketIndentDetail detail = new JSON_TicketIndentDetail();
						detail.setTicket(json);
						detail.setIndentDetailId(indentDetail.getIndentDetailId());
						detail.setIntro(indentDetail.getTicketPrice().getTicketType().getIntro());
						detail.setSubtotal(indentDetail.getSubtotal());
						detail.setTicketName(indentDetail.getTicketPrice().getTicketType().getTicketName());
						detail.setUseDate(indentDetail.getUseDate());
					
						details.add(detail);
						break;
					}
			 	}
			}
		}
		
		ticketIndent.setTicketIndentDetails(indentDetails);
		ticketIndent.setTotalPrice(ticketIndent.getTotalPrice() - decrease);
			
		result.setDetails(details);
		return result;
	}
	
	
	@RequestMapping(value="confirmIndent", method=RequestMethod.POST)
	public @ResponseBody String confirmIndent(@RequestParam(value="payMoney") Double payMoney, 
							   @RequestParam(value="linkman") String linkman,
							   @RequestParam(value="idCardNumber") String idCardNumber, 
							   @RequestParam(value="phoneNumber") String phoneNumber, 
							   Model uiModel) {
		if (ticketIndent != null) {
			for (TicketIndentDetail indentDetail : ticketIndentDetails) {
				if (indentDetail.getTicketPrice().getRemainToplimit() < indentDetail.getAmount()) {
					return "{\"msg\":\"notenough" + "'\"}";
				}
				
				DetachedCriteria detachedCriteria = DetachedCriteria.forClass(TicketIndentDetail.class);
				detachedCriteria.add(Property.forName("status").eq("01"));
				detachedCriteria.add(Property.forName("useDate").eq(indentDetail.getUseDate()));
				List<TicketIndentDetail> effectiveDetails = ticketIndentDetailService.findByCriteria(detachedCriteria);
			
				Integer effectiveAmount = 0;
				for (TicketIndentDetail effect : effectiveDetails) {
					effectiveAmount += effect.getAmount();
				}
				
				if (effectiveAmount + indentDetail.getAmount() > indentDetail.getTicketPrice().getToplimit()) {
					return "{\"msg\":\"notenough" + "'\"}";
				}
			}
			
			for (TicketIndentDetail indentDetail : ticketIndentDetails) {
				TicketPrice ticketPrice = indentDetail.getTicketPrice();
				ticketPrice.setRemainToplimit(ticketPrice.getRemainToplimit() - indentDetail.getAmount());
				ticketPriceService.update(ticketPrice);
			}
		
			Double totalPrice = 0.00;
			Set<TicketIndentDetail> indentDetails = new HashSet<TicketIndentDetail>();
			for (TicketIndentDetail indentDetail : ticketIndentDetails) {
				// ticketIndentDetailDao.save(indentDetail);
				indentDetail.setInputer((Long) session.getAttribute("user_id"));
				indentDetail.setInputTime(new Date());
				indentDetail.setStatus("00");
				indentDetails.add(indentDetail);
				totalPrice += indentDetail.getSubtotal();
			}
			ticketIndent.setDetails(ticketIndentDetails);
			ticketIndent.setTicketIndentDetails(indentDetails);
			ticketIndent.setLinkman(linkman);
			ticketIndent.setIdCardNumber(idCardNumber);
			ticketIndent.setPhoneNumber(phoneNumber);
			ticketIndent.setBookDate(new Date());
			ticketIndent.setStatus("00");
			ticketIndent.setTotalPrice(totalPrice);
			
			DateFormat dfm = new SimpleDateFormat("yyyyMMdd");
			String date = dfm.format(new Date());
			ticketIndent.setIndentCode(ticketIndentDetailDao.getNextIndentCode(date + "00000000", date));
			// ticketIndent.setIndentCode(ticketIndent.getIndentId().substring(12));
			ticketIndent.setInputer((Long) session.getAttribute("user_id"));
			ticketIndent.setInputTime(new Date());
			
			Long userId = (Long) session.getAttribute("user_id");
			User user = userService.findById(userId);
			ticketIndent.setUser(user);
		
			String userType;
			if (user.getEmployerId() != null) {
				return "{\"msg\":\"invalid" + "\"}";
			}
			else {
				DetachedCriteria criteria = DetachedCriteria.forClass(Travservice.class);
				criteria.add(Property.forName("user").eq(user));
				List<Travservice> travelAgencies = baseTravelAgencyService.findByCriteria(criteria);
				if (travelAgencies.size() > 0) {
					Travservice travservice = travelAgencies.get(0);
					
					userType = "travelAgency";	
					
					criteria = DetachedCriteria.forClass(BaseCashAccount.class);
					criteria.add(Property.forName("baseTravelAgency").eq(travservice));
					List<BaseCashAccount> cashAccounts = baseCashAccountService.findByCriteria(criteria);
					
					Double cashBalance = cashAccounts.get(0).getCashBalance();
					
					criteria = DetachedCriteria.forClass(BaseRewardAccount.class);
					criteria.add(Property.forName("baseTravelAgency").eq(travservice));
					List<BaseRewardAccount> rewardAccounts = baseRewardAccountService.findByCriteria(criteria);
					
					Double rewardBalance = rewardAccounts.get(0).getRewardBalance();
					
					if (cashBalance >= payMoney) {
						for (TicketIndentDetail indentDetail : ticketIndent.getTicketIndentDetails()) {
							BaseTranRec baseTranRec = new BaseTranRec();
							baseTranRec.setTicketIndent(ticketIndent);
							baseTranRec.setTicketIndentDetail(indentDetail);
							baseTranRec.setTrxId(indentDetail.getIndentDetailId());
							baseTranRec.setMoney(indentDetail.getSubtotal());
							baseTranRec.setPayFee(indentDetail.getSubtotal());
							baseTranRec.setPerFee(indentDetail.getPerFee());
							baseTranRec.setCardNumber(String.valueOf(travservice.getTravelAgencyId()));
							baseTranRec.setBank("intralAccount");
							baseTranRec.setAccountName(String.valueOf(travservice.getTravelAgencyId()));
							baseTranRec.setPaymentType("00"); //支付
							Date paymentTime = new Date();
							baseTranRec.setPaymentTime(paymentTime);
							baseTranRec.setPayType("01"); //intral account payment
		    			
							baseTranRecService.save(baseTranRec);
						}
						
						BaseCashAccount newBalance = cashAccounts.get(0);
						newBalance.setCashBalance(newBalance.getCashBalance() - payMoney);
						baseCashAccountService.update(newBalance);
						
						travservice.setCashBalance(travservice.getCashBalance() - payMoney);
						baseTravelAgencyService.update(travservice);
				   		
						String smsContent = "您所预订的订单已成功预订，请等待西岭雪山市场部审核。审核通过后会以短信形式通知您。"; 
			    		CommonMethod.sendSms(travelAgencies.get(0).getContactorMp(), smsContent, "true");
						
			    		ticketIndent.setCustomerType("01");
			    		
			    		ticketIndentService.save(ticketIndent);
						ticketPoolService.addTIDToPool(ticketIndent.getIndentId());
						
			    		return "{\"msg\":\"success\"," + "\"userType\":\"" + userType + "\"," + "\"aliBank\":\"0\", \"indentId\":\"" + ticketIndent.getIndentId() + "\"}"; 
					}
					else if (rewardBalance >= payMoney) {
						for (TicketIndentDetail indentDetail : ticketIndent.getTicketIndentDetails()) {
							BaseTranRec baseTranRec = new BaseTranRec();
							baseTranRec.setTicketIndent(ticketIndent);
							baseTranRec.setTicketIndentDetail(indentDetail);
							baseTranRec.setTrxId(indentDetail.getIndentDetailId());
							baseTranRec.setMoney(indentDetail.getSubtotal());
							baseTranRec.setPayFee(indentDetail.getSubtotal());
							baseTranRec.setPerFee(indentDetail.getPerFee());
							baseTranRec.setCardNumber(String.valueOf(travservice.getTravelAgencyId()));
							baseTranRec.setBank("intralAccount");
							baseTranRec.setAccountName(String.valueOf(travservice.getTravelAgencyId()));
							baseTranRec.setPaymentType("00"); //支付
							Date paymentTime = new Date();
							baseTranRec.setPaymentTime(paymentTime);
							baseTranRec.setPayType("02"); //reward account payment
		    			
							baseTranRecService.save(baseTranRec);
						}
						
						BaseRewardAccount newRewardBalance = rewardAccounts.get(0);
						newRewardBalance.setRewardBalance(newRewardBalance.getRewardBalance() - payMoney);
						baseRewardAccountService.update(newRewardBalance);
						
						String smsContent = "您所预订的订单已成功预订，请等待西岭雪山市场部审核。审核通过后会以短信形式通知您。"; 
			    		CommonMethod.sendSms(travelAgencies.get(0).getContactorMp(), smsContent, "true");
						
			    		ticketIndent.setCustomerType("01");
			    		ticketIndentService.save(ticketIndent);
			    		ticketPoolService.addTIDToPool(ticketIndent.getIndentId());
			    		
			    		return "{\"msg\":\"success\"," + "\"userType\":\"" + userType + "\"," + "\"aliBank\":\"0\", \"indentId\":\"" + ticketIndent.getIndentId() + "\"}";
					}
					else {
						return "{\"msg\":\"noenough\"," + "\"userType\":\"" + userType + "\"," + "\"aliBank\":\"0\", \"indentId\":\"" + ticketIndent.getIndentId() + "\"}";
					}
				}
				else {
					userType = "tourist";
					ticketIndent.setCustomerType("02");
					ticketIndentService.save(ticketIndent);
					ticketPoolService.addTIDToPool(ticketIndent.getIndentId());
					
					return "{\"msg\":\"success\"," + "\"userType\":\"" + userType + "\"," + "\"aliBank\":\"0\", \"indentId\":\"" + ticketIndent.getIndentId() + "\"}"; 
				}
			}	
		}
		
		return "{\"msg\":\"invalid" + "'\"}";
	}
	
	@RequestMapping(value="getPayParam", method=RequestMethod.GET)
	public @ResponseBody List<PayParams> getPayParam(@RequestParam(value="indentId") String indentId) {
		TicketIndent ticketIndentToPay = ticketIndentService.findById(indentId);
		if (ticketIndentToPay != null) {
			try{
				PayParams payParams = ticketIndentService.savePayParamsForTourist(ticketIndentToPay);
				if(payParams.getIsUsePay()!=null && payParams.getIsUsePay()==true){
					List<PayParams> resJSON = new ArrayList<PayParams>();
					resJSON.add(payParams);
					return resJSON;
				} else {//未使用网银支付，即用奖励款或者奖金余额支付
					return null;
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			
			return null;
		}
		
		return null;
	}
	
	@RequestMapping(value="removeIndent", method=RequestMethod.POST)
	public @ResponseBody String removeIndent(@RequestParam(value="ticketIndentId") String ticketIndentId,
											 @RequestParam(value="indentDetailId") String indentDetailId) {
		TicketIndent ticketIndent = ticketIndentService.findById(ticketIndentId);
		if (ticketIndent != null) {
			for (TicketIndentDetail ticketIndentDetail : ticketIndent.getTicketIndentDetails()) {
				if (ticketIndentDetail.getIndentDetailId().equals(indentDetailId)) {
					ticketIndentDetail.setStatus("10"); // deleted
					ticketIndentDetailService.update(ticketIndentDetail);
					// ticketIndent.getTicketIndentDetails().remove(ticketIndentDetail);
				}
			}
			if (ticketIndent.getTicketIndentDetails().isEmpty()) {
				ticketIndent.setStatus("10");
				ticketIndentService.update(ticketIndent);
			}
			
			return "{\"msg\":\"success" + "\"}"; 
		}
		return "{\"msg\":\"invalid" + "\"}"; 
	}
	
	@RequestMapping(value="refundIndent", method=RequestMethod.POST) 
	public @ResponseBody String refundIndent(@RequestParam(value="ticketIndentId") String ticketIndentId,
											 @RequestParam(value="indentDetailId") String indentDetailId) {
		if (tickets == null) {
			initTicketList();
		}
		
		TicketIndent ticketIndent = ticketIndentService.findById(ticketIndentId);
		if (ticketIndent != null) {
			for (TicketIndentDetail indentDetail : ticketIndent.getTicketIndentDetails()) {
				if (indentDetail.getIndentDetailId().equals(indentDetailId)) {
					if (!ticketIndent.getStatus().equals("01")) {
						return "{\"msg\":\"notpayed" + "\"}";
					}
					
					Date now = new Date();
					Long diff = indentDetail.getUseDate().getTime() - now.getTime();
					Long hours = diff / (1000 * 60 * 60);
					if (hours < 24) {
						if (hours < 0) 
							return "{\"msg\":\"timepassed" + "\"}";
						else
							return "{\"msg\":\"timetoclose" + "\"}";
					}
					
					boolean refunded = ticketIndentDetailService.saveReturn(indentDetail);
					
					if (refunded) {
						User user = indentDetail.getTicketIndent().getUser();
						if (user.getAccumulate() != null) {
							Integer point = user.getAccumulate();
							point -= indentDetail.getSubtotal().intValue();
							user.setAccumulate(point);
						}
					}
					
					if (refunded) {
			    		List<RESTFulTicketIndent> restTicketIndents = new ArrayList<RESTFulTicketIndent>();
			    		Set<TicketIndentDetail> indentDetails = ticketIndent.getTicketIndentDetails();
			    		RESTFulTicketIndent restTicketIndent = new RESTFulTicketIndent();
			    		String orderid = ticketIndent.getIndentId();
			    		orderid = orderid.substring(16) + indentDetail.getIndentDetailId();
			    		restTicketIndent.setOrderid(orderid);
			    		restTicketIndent.setUname(ticketIndent.getLinkman());
			    		restTicketIndent.setUid(ticketIndent.getIdCardNumber());
			    		restTicketIndent.setMphone(ticketIndent.getPhoneNumber());
			    		restTicketIndent.setVcode(null);
			    		for (RESTFulTicket ticket : tickets) {
			    			if (ticket.getName().equals(indentDetail.getTicketPrice().getTicketType().getTicketName())) {
			    				restTicketIndent.setPid(ticket.getPid());
			    				if (ticket.getIsSuite()) {
			    					restTicketIndent.setSname(ticket.getName());
			    				}
			    				break;
			    			}
			    		}
			    		restTicketIndent.setCount(indentDetail.getAmount());
			    		if (ticketIndent.getDiscount() != null) {
			    			restTicketIndent.setDiscount(ticketIndent.getDiscount());
			    		}
			    		else {
			    			restTicketIndent.setDiscount("1.0");
			    		}
			    		restTicketIndent.setPlaydate(indentDetail.getUseDate());
			    		restTicketIndent.setPaytype("alipay");
			    			
			    		restTicketIndents.add(restTicketIndent);
			    		
			    		// call xlpw web service here
			    		final String uri = systemSetting.getXlpwUrl() + "/rest/ticket/cancelBook";
			    		
			    		RestTemplate restTemplate = new RestTemplate();
			    	    
			    		RESTFulTicketIndents indents = new RESTFulTicketIndents(restTicketIndents); 
			    	    RESTFulTicketIndents result = restTemplate.postForObject(uri, indents, RESTFulTicketIndents.class);
			    	
			            System.out.println(">>>>>成功完成票务订单退票：" + indentDetail.getIndentDetailId());  
						
						return "{\"msg\":\"success" + "\"}";
					}
					else {
						return "{\"msg\":\"fail" + "\"}";
					}
				}
			}
		}
		
		return "{\"msg\":\"invalid'" + "\"}"; 
		
	}
	
	@RequestMapping(value="alipayNotify", method=RequestMethod.POST)
	public String alipayNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Map<String,String> params = new HashMap<String,String>();  
        Map requestParams = request.getParameterMap();  
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {  
            String name = (String) iter.next();  
            String[] values = (String[]) requestParams.get(name);  
            String valueStr = "";  
            for (int i = 0; i < values.length; i++) {  
                valueStr = (i == values.length - 1) ? valueStr + values[i]: valueStr + values[i] + ",";  
            }  
            params.put(name, valueStr);  
        }  
        String out_trade_no = request.getParameter("out_trade_no");  
        String trade_status = request.getParameter("trade_status");
    	String notify_time = request.getParameter("notify_time");
    	String notify_type = request.getParameter("notify_type");
    	String notify_id = request.getParameter("notify_id");
    	String sign_type = request.getParameter("sign_type");
    	String sign = request.getParameter("sign");

    	String subject = request.getParameter("subject");
    	String payment_type = request.getParameter("payment_type");
    	String trade_no = request.getParameter("trade_no");

    	String gmt_create = request.getParameter("gmt_create");
    	String gmt_payment = request.getParameter("gmt_payment");
    	String gmt_close = request.getParameter("gmt_close");
    	String refund_status = request.getParameter("refund_status");
    	String gmt_refund = request.getParameter("gmt_refund");

    	String seller_email = request.getParameter("seller_email");
    	String buyer_email = request.getParameter("buyer_email");
    	String seller_id = request.getParameter("seller_id");
    	String buyer_id = request.getParameter("buyer_id");
    	String price = request.getParameter("price");

    	String total_fee = request.getParameter("total_fee");
    	String quantity = request.getParameter("quantity");
    	String body = request.getParameter("body");
    	String discount = request.getParameter("discount");
    	String is_total_fee_adjust = request.getParameter("is_total_fee_adjust");
    	String use_coupon = request.getParameter("use_coupon");

    	String extra_common_param = request.getParameter("extra_common_param");
    	String out_channel_type = request.getParameter("out_channel_type");
    	String out_channel_amount = request.getParameter("out_channel_amount");
    	String out_channel_inst = request.getParameter("out_channel_inst");
    
    	DetachedCriteria detachedCriteria = DetachedCriteria.forClass(TicketIndent.class);
    	detachedCriteria.add(Property.forName("indentCode").eq(out_trade_no));
    	List<TicketIndent> ticketIndents = ticketIndentService.findByCriteria(detachedCriteria);
    	if (ticketIndents.size() != 0 && trade_status.trim().equals("TRADE_SUCCESS")){//支付成功
    		TicketIndent ticketIndent = ticketIndents.get(0);
    		
    		if (ticketIndent.getStatus().equals("01")) {
                response.setHeader("content-type", "text/html;charset=UTF-8");
                response.getWriter().println("success");
                return "success";
    		}
    		
    		String smsCode = SeriesNumber.RandomString(6);
    		String smsContent = "您所预订的订单已成功预订，取票验证码为：" + smsCode; 
    		CommonMethod.sendSms(ticketIndent.getPhoneNumber(), smsContent, "true");
    		
    		ticketIndent.setStatus("01");
    		ticketIndent.setPayFee(Double.valueOf(total_fee));
    		ticketIndent.setUpdater(ticketIndent.getUser().getUserId());
    		ticketIndent.setUpdateTime(new Date());
    		ticketIndentService.update(ticketIndent);
    		
    		User user = ticketIndent.getUser();
    		if (user.getAccumulate() == null) {
    			user.setAccumulate(ticketIndent.getPayFee().intValue());
    			userService.update(user);
    		}
    		else {
    			Integer point = user.getAccumulate();
    			point += ticketIndent.getPayFee().intValue();
    			user.setAccumulate(point);
    			userService.update(user);
    		}
    		
    		for (TicketIndentDetail indentDetail : ticketIndent.getTicketIndentDetails()) {
    			indentDetail.setStatus("01");
    			indentDetail.setUpdater(ticketIndent.getUser().getUserId());
    			indentDetail.setUpdateTime(new Date());
    			ticketIndentDetailService.update(indentDetail);
    			
    			BaseTranRec baseTranRec = new BaseTranRec();
    			baseTranRec.setTicketIndent(ticketIndent);
    			baseTranRec.setTicketIndentDetail(indentDetail);
    			baseTranRec.setTrxId(trade_no);
    			baseTranRec.setMoney(indentDetail.getSubtotal());
    			baseTranRec.setPayFee(indentDetail.getSubtotal() * (Double.valueOf(total_fee) / ticketIndent.getTotalPrice()));
    			baseTranRec.setPerFee(indentDetail.getPerFee());
    			baseTranRec.setCardNumber(buyer_id);
    			baseTranRec.setBank("alipay");
    			baseTranRec.setAccountName(buyer_email);
    			baseTranRec.setPaymentType("00"); //支付
    			baseTranRec.setPayType("00");     //alipay
    			DateFormat format = DateFormat.getDateTimeInstance();
    			Date paymentTime = null;
				try {
					paymentTime = format.parse(gmt_payment);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    			baseTranRec.setPaymentTime(paymentTime);
    			baseTranRec.setPayType("00"); //alipay
    			
    			baseTranRecService.save(baseTranRec);
    		}
	
    		List<RESTFulTicketIndent> restTicketIndents = new ArrayList<RESTFulTicketIndent>();
    		Set<TicketIndentDetail> indentDetails = ticketIndent.getTicketIndentDetails();
    		for (TicketIndentDetail indentDetail : indentDetails) {
    			RESTFulTicketIndent restTicketIndent = new RESTFulTicketIndent();
		    	String orderid = ticketIndent.getIndentId();
    			orderid = orderid.substring(16) + indentDetail.getIndentDetailId();
    			restTicketIndent.setOrderid(orderid);
    			restTicketIndent.setUname(ticketIndent.getLinkman());
    			restTicketIndent.setUid(ticketIndent.getIdCardNumber());
    			restTicketIndent.setMphone(ticketIndent.getPhoneNumber());
    			restTicketIndent.setVcode(smsCode);
    			for (RESTFulTicket ticket : tickets) {
    				if (ticket.getName().equals(indentDetail.getTicketPrice().getTicketType().getTicketName())) {
    					restTicketIndent.setPid(ticket.getPid());
    					if (ticket.getIsSuite()) {
    						restTicketIndent.setSname(ticket.getName());
    					}
    					break;
    				}
    			}
    			restTicketIndent.setCount(indentDetail.getAmount());
    			if (ticketIndent.getDiscount() != null) {
    				restTicketIndent.setDiscount(ticketIndent.getDiscount());
    			}
    			else {
    				restTicketIndent.setDiscount("1.0");
    			}
    			restTicketIndent.setPlaydate(indentDetail.getUseDate());
    			restTicketIndent.setPaytype("alipay");
    			
    			restTicketIndents.add(restTicketIndent);
    		}
    		
    		// call xlpw web service here
    		final String uri = systemSetting.getXlpwUrl() + "/rest/ticket/book";
    		
    	    RestTemplate restTemplate = new RestTemplate();
    	    
    	    RESTFulTicketIndents indents = new RESTFulTicketIndents(restTicketIndents); 
    	    RESTFulTicketIndents result = restTemplate.postForObject(uri, indents, RESTFulTicketIndents.class);
    	
            System.out.println(">>>>>成功支付票务订单：" + out_trade_no);  
           
            response.setHeader("content-type", "text/html;charset=UTF-8");
            response.getWriter().println("success");
            return "success";
        }else{//验证失败  
            response.setHeader("content-type", "text/html;charset=UTF-8");
            response.getWriter().println("success");
            return "success";
        }  
	}
	
	@RequestMapping(value="alipayReturn", method=RequestMethod.GET)
	public String alipayReturn(HttpServletRequest request, HttpServletResponse response, Model uiModel) {
        Map<String,String> params = new HashMap<String,String>();  
        Map requestParams = request.getParameterMap();  
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {  
            String name = (String) iter.next();  
            String[] values = (String[]) requestParams.get(name);  
            String valueStr = "";  
            for (int i = 0; i < values.length; i++) {  
                valueStr = (i == values.length - 1) ? valueStr + values[i]: valueStr + values[i] + ",";  
            }  
            params.put(name, valueStr);  
        }  
        String out_trade_no = request.getParameter("out_trade_no");  
        String trade_status = request.getParameter("trade_status");
    	String notify_time = request.getParameter("notify_time");
    	String notify_type = request.getParameter("notify_type");
    	String notify_id = request.getParameter("notify_id");
    	String sign_type = request.getParameter("sign_type");
    	String sign = request.getParameter("sign");

    	String subject = request.getParameter("subject");
    	String payment_type = request.getParameter("payment_type");
    	String trade_no = request.getParameter("trade_no");

    	String gmt_create = request.getParameter("gmt_create");
    	String gmt_payment = request.getParameter("gmt_payment");
    	String gmt_close = request.getParameter("gmt_close");
    	String refund_status = request.getParameter("refund_status");
    	String gmt_refund = request.getParameter("gmt_refund");

    	String seller_email = request.getParameter("seller_email");
    	String buyer_email = request.getParameter("buyer_email");
    	String seller_id = request.getParameter("seller_id");
    	String buyer_id = request.getParameter("buyer_id");
    	String price = request.getParameter("price");

    	String total_fee = request.getParameter("total_fee");
    	String quantity = request.getParameter("quantity");
    	String body = request.getParameter("body");
    	String discount = request.getParameter("discount");
    	String is_total_fee_adjust = request.getParameter("is_total_fee_adjust");
    	String use_coupon = request.getParameter("use_coupon");

    	String extra_common_param = request.getParameter("extra_common_param");
    	String out_channel_type = request.getParameter("out_channel_type");
    	String out_channel_amount = request.getParameter("out_channel_amount");
    	String out_channel_inst = request.getParameter("out_channel_inst");
    
    	DetachedCriteria detachedCriteria = DetachedCriteria.forClass(TicketIndent.class);
    	detachedCriteria.add(Property.forName("indentCode").eq(out_trade_no));
    	List<TicketIndent> ticketIndents = ticketIndentService.findByCriteria(detachedCriteria);
    	if (ticketIndents.size() != 0 && trade_status.trim().equals("TRADE_SUCCESS")){//支付成功
    		TicketIndent ticketIndent = ticketIndents.get(0);
    		
    		uiModel.addAttribute("payResult", "订单成功支付！");
            return "ticket/alipayReturn"; 
        }else{//验证失败
        	uiModel.addAttribute("payResult", "订单支付失败，请重新支付。");
        	return "ticket/alipayReturn"; 
        }  
	}
	
	@RequestMapping(value="tsSyncToXLPW", method=RequestMethod.POST)
	public @ResponseBody String tsSyncToXLPW(@RequestParam(value="ticketIndentId") String ticketIndentId) {
		if (tickets == null) {
			initTicketList();
		}
		
    	DetachedCriteria detachedCriteria = DetachedCriteria.forClass(TicketIndent.class);
    	detachedCriteria.add(Property.forName("indentId").eq(ticketIndentId));
    	List<TicketIndent> ticketIndents = ticketIndentService.findByCriteria(detachedCriteria);
    	if (ticketIndents.size() != 0){ 
    		TicketIndent ticketIndent = ticketIndents.get(0);
			User user = ticketIndent.getUser();
			DetachedCriteria criteria = DetachedCriteria.forClass(Travservice.class);
			criteria.add(Property.forName("user").eq(user));
			List<Travservice> travservices = baseTravelAgencyService.findByCriteria(criteria);
    		
    		String smsCode = SeriesNumber.RandomString(6);
    		String smsContent = "您所预订的订单已成功预订，取票验证码为：" + smsCode; 
    		CommonMethod.sendSms(travservices.get(0).getContactorMp(), smsContent, "true");
    		
    		List<RESTFulTicketIndent> restTicketIndents = new ArrayList<RESTFulTicketIndent>();
    		Set<TicketIndentDetail> indentDetails = ticketIndent.getTicketIndentDetails();
    		for (TicketIndentDetail indentDetail : indentDetails) {
    			RESTFulTicketIndent restTicketIndent = new RESTFulTicketIndent();
		    	String orderid = ticketIndent.getIndentId();
		    	orderid = orderid.substring(16) + indentDetail.getIndentDetailId();
    			restTicketIndent.setOrderid(orderid);
    			restTicketIndent.setUname(ticketIndent.getLinkman());
    			restTicketIndent.setUid(ticketIndent.getIdCardNumber());
    			restTicketIndent.setMphone(ticketIndent.getPhoneNumber());
    			restTicketIndent.setVcode(smsCode);
    			for (RESTFulTicket ticket : tickets) {
    				if (ticket.getName().equals(indentDetail.getTicketPrice().getTicketType().getTicketName())) {
    					restTicketIndent.setPid(ticket.getPid());
    					if (ticket.getIsSuite()) {
    						restTicketIndent.setSname(ticket.getName());
    					}
    					break;
    				}
    			}
    			restTicketIndent.setCount(indentDetail.getAmount());
    			if (ticketIndent.getDiscount() != null) {
    				restTicketIndent.setDiscount(ticketIndent.getDiscount());
    			}
    			else {
    				restTicketIndent.setDiscount("1.0");
    			}
    			restTicketIndent.setPlaydate(indentDetail.getUseDate());
    			restTicketIndent.setPaytype("alipay");
    			
    			restTicketIndents.add(restTicketIndent);
    		}
    		
    		// call xlpw web service here
    		final String uri = systemSetting.getXlpwUrl() + "/rest/ticket/book";
    		
    	    RestTemplate restTemplate = new RestTemplate();
    	    
    	    RESTFulTicketIndents indents = new RESTFulTicketIndents(restTicketIndents); 
    	    RESTFulTicketIndents result = restTemplate.postForObject(uri, indents, RESTFulTicketIndents.class);
    	
            System.out.println(">>>>>成功预订旅行社票务订单：" + ticketIndentId);  
            return "{\"msg\":\"success" + "\"}"; 
    	}
    	else {
            return "{\"msg\":\"invalid" + "\"}"; 
    	}

	}
	
	@RequestMapping(value="returnToAccount", method=RequestMethod.GET)
	public @ResponseBody String returnToAccount() {
		Long user_id = (Long) session.getAttribute("user_id");
		if (user_id != null) {
			User user = userService.findById(user_id);
			if (user.getEmployerId() != null) {
				return "{\"url\":\"/xlem/market/init/" + user_id + "\"}"; 
			}
			else {
				DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Travservice.class);
				detachedCriteria.add(Property.forName("user").eq(user));
				List<Travservice> travelAgencies = baseTravelAgencyService.findByCriteria(detachedCriteria);
				if (travelAgencies.size() > 0) {
					return "{\"url\":\"/xlem/ts_account/travelAgency/" + user_id + "\"}"; 
				}
				else {
					return "{\"url\":\"/xlem/account/tourist/" + user_id + "\"}"; 
				}
			}
		}
		else {
			return "{\"url\":\"/xlem/index" + "\"}"; 
		}
	}
}
