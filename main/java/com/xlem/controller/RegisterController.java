package com.xlem.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import javax.swing.text.Document;
import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.xlem.dao.Depart;
import com.xlem.dao.Role;
import com.xlem.dao.Travservice;
import com.xlem.dao.User;
import com.xlem.service.TravService;
import com.xlem.utils.Message;
import com.xlem.utils.UrlUtil;

import service.comm.reg.baseTravelAgency.BaseTravelAgencyService;
import service.sys.role.RoleService;
import service.sys.user.UserService;

@RequestMapping("/register")
@Controller
public class RegisterController {
	final Logger logger = LoggerFactory.getLogger(RegisterController.class);
	@Resource
	private UserService userService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private BaseTravelAgencyService baseTravelAgencyService;
	
	@Autowired
	MessageSource messageSource;
	
	@RequestMapping(method=RequestMethod.GET)
	public String register_show(Model uiModel) {
		logger.info("show register page.");

		User user = new User();
		uiModel.addAttribute("user", user);
		uiModel.addAttribute("vcode", new String());
		uiModel.addAttribute("rePwd", new String());
	
		return "register/person";
	}
	
	@RequestMapping(value="/person", params="form", method = RequestMethod.GET)
	public String personRegisterForm(Model uiModel) {
		logger.info("Register personal user");
		User user = new User();
		uiModel.addAttribute("user", user);
		uiModel.addAttribute("vcode", new String());
		uiModel.addAttribute("rePwd", new String());
		return "register/person";
	}	
	
	@RequestMapping(value="/person", params="form", method=RequestMethod.POST)
    public String personRegister(User user, BindingResult bindingResult, Model uiModel, 
    		HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes, 
    		HttpSession session, @RequestParam String vcode, 
    		@RequestParam String rePwd, Locale locale) {
		final Logger logger = LoggerFactory.getLogger(RegisterController.class);
		logger.info("Register personal user");
		if (bindingResult.hasErrors()) {
			uiModel.addAttribute("message", new Message("error", 
					messageSource.getMessage("user_register_fail", new Object[]{}, locale)));
			uiModel.addAttribute("user", user);
			return "register/person";
		}
		
		String vcode2 = (String) session.getAttribute("rand");
		if (vcode.equals(vcode2)) {
			String name = user.getName();
			if (userService.findByProperty("name", name).isEmpty()) {
			  String pwd = user.getPwd();
			  if (pwd.equals(rePwd)) {
				  uiModel.asMap().clear();
				  redirectAttributes.addFlashAttribute("message", new Message("success",
						  messageSource.getMessage("user_register_success", new Object[]{}, locale)));
				
				  logger.info("user id" + user.getUserId() + "user name" + user.getName());
				  DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Role.class);
				  detachedCriteria.add(Property.forName("name").eq("ROLE_TOURIST"));
				  List<Role> roles = roleService.findByCriteria(detachedCriteria);
				  user.setRole(roles.get(0));
				  userService.save(user);
				  return "redirect:/account/tourist/" 
				  		 + UrlUtil.encodeUrlPathSegment(user.getUserId().toString(), httpServletRequest);
			  }
			  else {
				  uiModel.addAttribute("message", new Message("error", 
					 	  messageSource.getMessage("user_pwd_notsame", new Object[]{}, locale)));
				  uiModel.addAttribute("user", user);
				  return "register/person";
			  }
			}
			else {
			  uiModel.addAttribute("message", new Message("error", 
				 	  messageSource.getMessage("user_name_same", new Object[]{}, locale)));
			  uiModel.addAttribute("user", user);
			  return "register/person";
			}
		}
		else {
			uiModel.addAttribute("message", new Message("error", 
					messageSource.getMessage("user_vcode_error", new Object[]{}, locale)));
			uiModel.addAttribute("user", user);
			return "register/person";
			
		}
	}
	
	@RequestMapping(value="/company/first", params="form", method = RequestMethod.GET)
	public String companyRegisterForm1(Model uiModel) {
		final Logger logger = LoggerFactory.getLogger(RegisterController.class);
		logger.info("Register company user step 1");
		uiModel.addAttribute("isNew", true);
		return "register/company1";
	}	

	@RequestMapping(value="/company/first", params="form", method = RequestMethod.POST)
	public String companyRegister1(Model uiModel, HttpServletRequest httpServletRequest, 
			RedirectAttributes redirectAttributes, HttpSession session, Locale locale) {
		final Logger logger = LoggerFactory.getLogger(RegisterController.class);
		logger.info("Register company user step 1");
	
		return "redirect:/register/company/second?form";
	}	

	@RequestMapping(value="/company/second", params="form", method = RequestMethod.GET)
	public String companyRegisterForm2(Model uiModel) {
		final Logger logger = LoggerFactory.getLogger(RegisterController.class);
		logger.info("Register company user step 2");

		Travservice travs = new Travservice();
		uiModel.addAttribute("travs", travs);
		
		return "register/company2";
	}

	
	@RequestMapping(value="/company/second", params="form", method = RequestMethod.POST)
	public String companyRegister2(Travservice travs, BindingResult bindingResult, Model uiModel, 
    		HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes, 
    		HttpSession session, Locale locale) {
		final Logger logger = LoggerFactory.getLogger(RegisterController.class);
		logger.info("Register company user step 2");

		if (bindingResult.hasErrors()) {
			uiModel.addAttribute("message", new Message("error", 
					messageSource.getMessage("ts_register_fail", new Object[]{}, locale)));
			uiModel.addAttribute("travs", travs);
			return "register/company2";
		}
		
		if (travs.getName().equals("")) {
			uiModel.addAttribute("travs", travs);
			return "register/company2";
		}
		
		if (travs.getSubTs().equals("")) {
			uiModel.addAttribute("travs", travs);
			return "register/company2";
		}
		
		if (travs.getDepartTs().equals("")) {
			uiModel.addAttribute("travs", travs);
			return "register/company2";
		}
		
		if (travs.getTelephone().equals("")) {
			uiModel.addAttribute("travs", travs);
			return "register/company2";
		}
		
		if (travs.getContactor().equals("")) {
			uiModel.addAttribute("travs", travs);
			return "register/company2";
		}
		
		if (travs.getContactorId().equals("")) {
			uiModel.addAttribute("travs", travs);
			return "register/company2";
		}
		
		if (travs.getContactorMp().equals("")) {
			uiModel.addAttribute("travs", travs);
			return "register/company2";
		}
		
		if (travs.getEmail().equals("")) {
			uiModel.addAttribute("travs", travs);
			return "register/company2";
		}
		
		if (travs.getCity().equals("全部")) {
			uiModel.addAttribute("travs", travs);
			return "register/company2";
		}
		
		if (travs.getDistrict() == null || travs.getDistrict().equals("全部")) {
			uiModel.addAttribute("travs", travs);
			return "register/company2";
		}
		
		if (travs.getAddress().equals("")) {
			uiModel.addAttribute("travs", travs);
			return "register/company2";
		}
	
        redirectAttributes.addFlashAttribute("travs", travs);        
		return "redirect:/register/company/three/?form";
	}		

	@RequestMapping(value="/company/three", params="form", method = RequestMethod.GET)
	public String companyRegisterForm3(Model uiModel) {
		logger.info("Register company user step 3");
		
		// Travservice travs = (Travservice) uiModel.asMap().get("travs");
		// uiModel.addAttribute("travs", travs);
		return "register/company3";
	}			
	
	@RequestMapping(value="/company/three", params="form", method = RequestMethod.POST)
	public String companyRegisterForm3(Travservice travs, BindingResult bindingResult, Model uiModel, 
    		HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes, 
    		HttpSession session, Locale locale, 
    		@RequestParam(value="file1", required=true) Part accountLicense,
    		@RequestParam(value="file2", required=true) Part tmRegLicense,
    		@RequestParam(value="file3", required=true) Part tmAuthorization) {
		final Logger logger = LoggerFactory.getLogger(RegisterController.class);
		logger.info("Register company user step 3");

		if (bindingResult.hasErrors()) {
			uiModel.addAttribute("message", new Message("error", 
					messageSource.getMessage("ts_register_fail", new Object[]{}, locale)));
			uiModel.addAttribute("travs", travs);
			return "register/company1";
		}
		
		if (travs.getBank().equals("")) {
			uiModel.addAttribute("travs", travs);
			return "register/company2";
		}
		
		if (travs.getAccount().equals("")) {
			uiModel.addAttribute("travs", travs);
			return "register/company2";
		}
		
		if (accountLicense == null || tmRegLicense == null || tmAuthorization == null) {
			uiModel.addAttribute("travs", travs);
			return "register/company2";
		}
		
		// process upload file
		if (accountLicense != null) {
			logger.info("File name: "+ accountLicense.getName());
			logger.info("File size: " + accountLicense.getSize());
			logger.info("File content type: " + accountLicense.getContentType());
			byte[] fileContent = null;
			try {
				InputStream inputStream = accountLicense.getInputStream();
				if (inputStream == null) logger.info("File inputstream is null");
				fileContent = IOUtils.toByteArray(inputStream);
				travs.setAccountLicense(fileContent);
			} catch (IOException ex) {
				logger.error("Error when saving uploaded file");
			}
			travs.setAccountLicense(fileContent);
		}

		// process upload file
		if (tmRegLicense != null) {
			logger.info("File name: "+ tmRegLicense.getName());
			logger.info("File size: " + tmRegLicense.getSize());
			logger.info("File content type: " + tmRegLicense.getContentType());
			byte[] fileContent = null;
			try {
				InputStream inputStream = tmRegLicense.getInputStream();
				if (inputStream == null) logger.info("File inputstream is null");
				fileContent = IOUtils.toByteArray(inputStream);
				travs.setTmRegLicense(fileContent);
			} catch (IOException ex) {
				logger.error("Error when saving uploaded file");
			}
			travs.setTmRegLicense(fileContent);
		}

		// process upload file
		if (tmAuthorization!= null) {
			logger.info("File name: "+ tmAuthorization.getName());
			logger.info("File size: " + tmAuthorization.getSize());
			logger.info("File content type: " + tmAuthorization.getContentType());
			byte[] fileContent = null;
			try {
				InputStream inputStream = tmAuthorization.getInputStream();
				if (inputStream == null) logger.info("File inputstream is null");
				fileContent = IOUtils.toByteArray(inputStream);
				travs.setTmAuthorization(fileContent);
			} catch (IOException ex) {
				logger.error("Error when saving uploaded file");
			}
			travs.setTmAuthorization(fileContent);
		}

		travs.setStatus("00");
		baseTravelAgencyService.save(travs);
		return "redirect:/register/company/four?form";
	}
	
	@RequestMapping(value="/company/four", params="form", method = RequestMethod.GET)
	public String companyRegisterFinalForm(Model uiModel) {
		logger.info("Register company user accomplished");
		uiModel.addAttribute("isNew", true);
		return "register/company4";
	}

	@RequestMapping(value="/company/four", params="form", method = RequestMethod.POST)
	public String companyRegisterFinal(Model uiModel) {
		logger.info("Register comany user accomplished");
		return "index";
	}
		
}
