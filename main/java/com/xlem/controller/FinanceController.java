package com.xlem.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.BaseTranRec;
import com.xlem.dao.FinanceReport;
import com.xlem.dao.TicketIndent;
import com.xlem.dao.TicketIndentDetail;
import com.xlem.dao.TicketType;
import com.xlem.dao.Travservice;
import com.xlem.dao.User;
import com.xlem.utils.RESTFulTicketIndentTrace;
import com.xlem.utils.RESTFulTicketIndentTraces;
import com.xlem.utils.RESTFulTickets;
import com.xlem.utils.SystemSetting;
import com.xlem.web.JSONBill;
import com.xlem.web.JSONBills;
import com.xlem.web.JSONCashDirection;
import com.xlem.web.JSONFinanceStat;
import com.xlem.web.JSONFinanceStats;
import com.xlem.web.JSONInBill;
import com.xlem.web.JSONInBills;
import com.xlem.web.JSONIndentState;
import com.xlem.web.JSONIndentTrace;
import com.xlem.web.JSONIndentTraces;
import com.xlem.web.JSONKeyword;
import com.xlem.web.JSONQueryTimeType;
import com.xlem.web.JSONRTDatas;
import com.xlem.web.JSONSummaryOutBill;
import com.xlem.web.JSONTicketIndents;
import com.xlem.web.JSONTradeClass;
import com.xlem.web.JSON_TicketIndent;
import com.xlem.web.JSON_TicketIndentDetail;

import service.comm.baseRechargeRec.BaseRechargeRecService;
import service.comm.baseTranRec.BaseTranRecService;
import service.comm.financeReport.FinanceReportService;
import service.comm.reg.baseTravelAgency.BaseTravelAgencyService;
import service.sys.user.UserService;
import service.ticket.ticketIndent.TicketIndentService;
import service.ticket.ticketType.TicketTypeService;

@Controller
@RequestMapping("/finance")
public class FinanceController {
	@Autowired
	private SystemSetting systemSetting;
	@Autowired
	private UserService userService;
	@Autowired
	private BaseTranRecService baseTranRecService;
	@Autowired
	private BaseTravelAgencyService baseTravelAgencyService;
	@Autowired
	private BaseRechargeRecService baseRechargeRecService;
	@Autowired
	private TicketIndentService ticketIndentService;
	@Autowired
	private FinanceReportService financeReportService;
	@Autowired
	private TicketTypeService ticketTypeService;
	
	private User currentUser = null;
	
	private ArrayList<JSONIndentState> jsonIndentStates = new ArrayList<JSONIndentState>();
	private ArrayList<JSONKeyword> jsonKeywords = new ArrayList<JSONKeyword>();
	private ArrayList<JSONQueryTimeType> jsonQueryTimeTypes = new ArrayList<JSONQueryTimeType>();
	private ArrayList<JSONCashDirection> jsonCashDirections = new ArrayList<JSONCashDirection>();
	private ArrayList<JSONTradeClass> jsonTradeClasses = new ArrayList<JSONTradeClass>();
	
	private List<JSONInBill> jsonInBills = null;
	
	private ArrayList<Integer> saleRTIndent = new ArrayList<Integer>();
	private ArrayList<Double> saleRTMoney = new ArrayList<Double>();

	@RequestMapping(value="/init/{id}", method=RequestMethod.GET)
	public String marketInit(@PathVariable(value="id") Long id, Model uiModel) {
		currentUser = userService.findById(id);
		uiModel.addAttribute("user", currentUser);

		return "finance/init";
	}
	
	@RequestMapping(value="/eOutBill", method=RequestMethod.GET)
	public String eOutBillForm(Model uiModel) {
		if (currentUser == null) {
			return "index";
		}
		
		if (currentUser.getRole().getName().equals("ROLE_FINANCE")) {
			return "index";
		}
		
		uiModel.addAttribute("user", currentUser);
		
		jsonIndentStates.clear();
		JSONIndentState jsonIndentState = new JSONIndentState();
		jsonIndentState.setCode("00");
		jsonIndentState.setName("所有状态");
		jsonIndentStates.add(jsonIndentState);
		
		jsonIndentState = new JSONIndentState();
		jsonIndentState.setCode("01");
		jsonIndentState.setName("已支付");
		jsonIndentStates.add(jsonIndentState);

		jsonIndentState = new JSONIndentState();
		jsonIndentState.setCode("02");
		jsonIndentState.setName("已失效");
		jsonIndentStates.add(jsonIndentState);

		jsonIndentState = new JSONIndentState();
		jsonIndentState.setCode("03");
		jsonIndentState.setName("已退款");
		jsonIndentStates.add(jsonIndentState);
		
		uiModel.addAttribute("states", jsonIndentStates);
		
		jsonKeywords.clear();
		JSONKeyword jsonKeyword = new JSONKeyword();
		jsonKeyword.setCode("00");
		jsonKeyword.setName("交易号");
		jsonKeywords.add(jsonKeyword);
		
		jsonKeyword = new JSONKeyword();
		jsonKeyword.setCode("01");
		jsonKeyword.setName("流水号");
		jsonKeywords.add(jsonKeyword);
		
		uiModel.addAttribute("keywords", jsonKeywords);
		
		jsonQueryTimeTypes.clear();
		JSONQueryTimeType jsonQueryTimeType = new JSONQueryTimeType();
		jsonQueryTimeType.setCode("00");
		jsonQueryTimeType.setName("创建时间");
		jsonQueryTimeTypes.add(jsonQueryTimeType);
		
		uiModel.addAttribute("timeTypes", jsonQueryTimeTypes);
		
		jsonCashDirections.clear();
		JSONCashDirection jsonCashDirection = new JSONCashDirection();
		jsonCashDirection.setCode("00");
		jsonCashDirection.setName("全部");
		jsonCashDirections.add(jsonCashDirection);
		
		jsonCashDirection = new JSONCashDirection();
		jsonCashDirection.setCode("01");
		jsonCashDirection.setName("入金");
		jsonCashDirections.add(jsonCashDirection);
		
		jsonCashDirection = new JSONCashDirection();
		jsonCashDirection.setCode("02");
		jsonCashDirection.setName("出金");
		jsonCashDirections.add(jsonCashDirection);
		
		uiModel.addAttribute("cash_dirs", jsonCashDirections);
		
		jsonTradeClasses.clear();
		JSONTradeClass jsonTradeClass = new JSONTradeClass();
		jsonTradeClass.setCode("00");
		jsonTradeClass.setName("所有交易");
		jsonTradeClasses.add(jsonTradeClass);
		
		jsonTradeClass = new JSONTradeClass();
		jsonTradeClass.setCode("01");
		jsonTradeClass.setName("票务订单");
		jsonTradeClasses.add(jsonTradeClass);

		jsonTradeClass = new JSONTradeClass();
		jsonTradeClass.setCode("02");
		jsonTradeClass.setName("充值业务");
		jsonTradeClasses.add(jsonTradeClass);
		
		uiModel.addAttribute("tradeClasses", jsonTradeClasses);

		return "finance/eOutBill";		
	}
	
	@RequestMapping(value="/getQueryTimeType", method=RequestMethod.GET, produces="text/json;charset=UTF-8")
	public @ResponseBody String getQueryTimeType(@RequestParam(value="timeType") String timeType) {
		for (JSONQueryTimeType tt : jsonQueryTimeTypes) {
			if (tt.getCode().equals(timeType)) {
				return "{\"msg\":\"success" + "\"," + "\"timeTypeName\":\"" + tt.getName() + "\"}";	
			}
		}
		
		return "{\"msg\":\"invalid" + "\"}"; 
	}
	
	@RequestMapping(value = "/getTransRecData", method = RequestMethod.GET)  
	public @ResponseBody JSONBills getFinancialBills(@RequestParam(value = "offset") int offset,
													 @RequestParam(value = "timeType") String timeType,
													 @RequestParam(value = "startDate") String startDate,
													 @RequestParam(value = "endDate") String endDate,
													 @RequestParam(value = "state") String state,
													 @RequestParam(value = "keyword") String keyword,
													 @RequestParam(value = "keyValue") String keyValue,
													 @RequestParam(value = "lessMoney") String lessMoney,
													 @RequestParam(value = "moreMoney") String moreMoney,
													 @RequestParam(value = "cashDir") String cashDir,
													 @RequestParam(value = "alipay") Boolean alipay,
													 @RequestParam(value = "other") Boolean other,
													 @RequestParam(value = "tradeClass") String tradeClass) throws ParseException {
		int allRecords = 0;
		int count = 0;
		JSONBills result = new JSONBills();
		List<JSONBill> jsonBills = new ArrayList<JSONBill>();
		
		switch (tradeClass) {
		case "00":
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(BaseTranRec.class);
			
			DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
			Date start = fmt.parse(startDate);
			Date end = fmt.parse(endDate);
			detachedCriteria.add(Property.forName("paymentTime").ge(start));
			detachedCriteria.add(Property.forName("paymentTime").le(end));
			
			if (!keyValue.equals("empty")) {
				detachedCriteria.add(Property.forName("tranRecId").eq(keyValue));
			}
			
			if (!lessMoney.equals("minus-infinite")) {
				detachedCriteria.add(Property.forName("payFee").ge(Double.valueOf(lessMoney)));
			}
			
			if (!moreMoney.equals("infinite")) {
				detachedCriteria.add(Property.forName("payFee").le(Double.valueOf(moreMoney)));
			}
			
			switch (cashDir) {
			case "01":
				detachedCriteria.add(Property.forName("payFee").ge(0.00));
				break;
				
			case "02":
				detachedCriteria.add(Property.forName("payFee").le(0.00));
				break;
			}
			
			List<BaseTranRec> baseTranRecs = baseTranRecService.findByCriteria(detachedCriteria);
			if (baseTranRecs != null) {
				allRecords = baseTranRecs.size();
				for (int i = offset; i < allRecords; i++) {
					if (count > 9) {
						break;
					}
					count++;
					
					BaseTranRec baseTranRec = baseTranRecs.get(i);
					JSONBill jsonBill = new JSONBill();
					jsonBill.setDate(baseTranRec.getPaymentTime());
					jsonBill.setName(baseTranRec.getTicketIndentDetail().getTicketPrice().getTicketType().getTicketName());
					jsonBill.setIndentId(baseTranRec.getTicketIndent().getIndentId());
					jsonBill.setTransRecId(baseTranRec.getTranRecId());
					
					switch (baseTranRec.getTicketIndent().getCustomerType()) {
					case "01": {
						User user = baseTranRec.getTicketIndent().getUser();
						DetachedCriteria criteria = DetachedCriteria.forClass(Travservice.class);
						criteria.add(Property.forName("user").eq(user));
						List<Travservice> travservices = baseTravelAgencyService.findByCriteria(criteria);
						Travservice travservice = travservices.get(0);
						jsonBill.setOtherSide(travservice.getName());
						if (baseTranRec.getMoney() > 0.00)
							jsonBill.setIndentSource("旅行社购票");
						else
							jsonBill.setIndentSource("旅行社退票");
						break;
					}
						
					case "02": {
						User user = baseTranRec.getTicketIndent().getUser();
						jsonBill.setOtherSide(user.getName());
						if (baseTranRec.getMoney() > 0.00)
							jsonBill.setIndentSource("游客购票");
						else
							jsonBill.setIndentSource("游客退票");
						}
					}
					
					jsonBill.setMoney(baseTranRec.getMoney());
					jsonBill.setStatus("已完成");
					
					jsonBills.add(jsonBill);
				}
			}
			
			detachedCriteria = DetachedCriteria.forClass(BaseRechargeRec.class);
			
			start = fmt.parse(startDate);
			end = fmt.parse(endDate);
			detachedCriteria.add(Property.forName("endDate").ge(start));
			detachedCriteria.add(Property.forName("endDate").le(end));
			
			if (!keyValue.equals("empty")) {
				detachedCriteria.add(Property.forName("rechargeRecId").eq(keyValue));
			}
			
			if (!lessMoney.equals("minus-infinite")) {
				detachedCriteria.add(Property.forName("cashMoney").ge(lessMoney));
			}
			
			if (!moreMoney.equals("infinite")) {
				detachedCriteria.add(Property.forName("cashMoney").le(moreMoney));
			}
			
			detachedCriteria.add(Property.forName("paymentType").eq("00"));
			
			List<BaseRechargeRec> baseRechargeRecs = baseRechargeRecService.findByCriteria(detachedCriteria);
			if (baseRechargeRecs != null) {
				allRecords += baseRechargeRecs.size();
				for (int i = count; i < baseRechargeRecs.size(); i++) {
					if (count > 9) {
						break;
					}
					count++;
						
					BaseRechargeRec baseRechargeRec = baseRechargeRecs.get(i);
					JSONBill jsonBill = new JSONBill();
					jsonBill.setDate(baseRechargeRec.getEndDate());
					jsonBill.setName("旅行社充值");
					jsonBill.setIndentId(String.valueOf(baseRechargeRec.getRechargeRecId()));
					jsonBill.setTransRecId(String.valueOf(baseRechargeRec.getRechargeRecId()));
					
					Travservice travservice = baseRechargeRec.getBaseTravelAgency();
					jsonBill.setOtherSide(travservice.getName());
					jsonBill.setIndentSource("旅行社充值");
					jsonBill.setMoney(baseRechargeRec.getCashMoney());
					jsonBill.setStatus("已完成");
						
					jsonBills.add(jsonBill);
				}
			}
			
			result.setNum(allRecords);
			result.setTransRec(jsonBills);	
			
			break;
		case "01":
			detachedCriteria = DetachedCriteria.forClass(BaseTranRec.class);
		
			fmt = new SimpleDateFormat("yyyy-MM-dd");
			start = fmt.parse(startDate);
			end = fmt.parse(endDate);
			detachedCriteria.add(Property.forName("paymentTime").ge(start));
			detachedCriteria.add(Property.forName("paymentTime").le(end));
			
			if (!keyValue.equals("empty")) {
				detachedCriteria.add(Property.forName("tranRecId").eq(keyValue));
			}
			
			if (!lessMoney.equals("minus-infinite")) {
				detachedCriteria.add(Property.forName("payFee").ge(lessMoney));
			}
			
			if (!moreMoney.equals("infinite")) {
				detachedCriteria.add(Property.forName("payFee").le(moreMoney));
			}
			
			switch (cashDir) {
			case "01":
				detachedCriteria.add(Property.forName("payFee").ge(0.00));
				break;
				
			case "02":
				detachedCriteria.add(Property.forName("payFee").le(0.00));
				break;
			}
			
			baseTranRecs = baseTranRecService.findByCriteria(detachedCriteria);
			if (baseTranRecs != null) {
				allRecords = baseTranRecs.size();
				for (int i = offset; i < allRecords; i++) {
					if (count > 9) {
						break;
					}
					count++;
					
					BaseTranRec baseTranRec = baseTranRecs.get(i);
					JSONBill jsonBill = new JSONBill();
					jsonBill.setDate(baseTranRec.getPaymentTime());
					jsonBill.setName(baseTranRec.getTicketIndentDetail().getTicketPrice().getTicketType().getTicketName());
					jsonBill.setIndentId(baseTranRec.getTicketIndent().getIndentId());
					jsonBill.setTransRecId(baseTranRec.getBackTranId());
					
					switch (baseTranRec.getTicketIndent().getCustomerType()) {
					case "01": {
						User user = baseTranRec.getTicketIndent().getUser();
						DetachedCriteria criteria = DetachedCriteria.forClass(Travservice.class);
						criteria.add(Property.forName("user").eq(user));
						List<Travservice> travservices = baseTravelAgencyService.findByCriteria(criteria);
						Travservice travservice = travservices.get(0);
						jsonBill.setOtherSide(travservice.getName());
						jsonBill.setIndentSource("旅行社购票");
						break;
					}
						
					case "02": {
						User user = baseTranRec.getTicketIndent().getUser();
						jsonBill.setOtherSide(user.getName());
						jsonBill.setIndentSource("游客购票");
					}
					}
					
					jsonBill.setMoney(baseTranRec.getMoney());
					jsonBill.setStatus("已完成");
					
					jsonBills.add(jsonBill);
				}
			}
			
			result.setNum(allRecords);
			result.setTransRec(jsonBills);	
			break;
			
		case "02":
			detachedCriteria = DetachedCriteria.forClass(BaseRechargeRec.class);
		
			fmt = new SimpleDateFormat("yyyy-MM-dd");
			start = fmt.parse(startDate);
			end = fmt.parse(endDate);
			detachedCriteria.add(Property.forName("endDate").ge(start));
			detachedCriteria.add(Property.forName("endDate").le(end));
			
			if (!keyValue.equals("empty")) {
				detachedCriteria.add(Property.forName("rechargeRecId").eq(keyValue));
			}
			
			if (!lessMoney.equals("minus-infinite")) {
				detachedCriteria.add(Property.forName("cashMoney").ge(lessMoney));
			}
			
			if (!moreMoney.equals("infinite")) {
				detachedCriteria.add(Property.forName("cashMoney").le(moreMoney));
			}
			
			detachedCriteria.add(Property.forName("paymentType").eq("00"));
			
			baseRechargeRecs = baseRechargeRecService.findByCriteria(detachedCriteria);
			if (baseRechargeRecs != null) {
				allRecords += baseRechargeRecs.size();
				for (int i = count; i < baseRechargeRecs.size(); i++) {
					if (count > 9) {
						break;
					}
					count++;
						
					BaseRechargeRec baseRechargeRec = baseRechargeRecs.get(i);
					JSONBill jsonBill = new JSONBill();
					jsonBill.setDate(baseRechargeRec.getEndDate());
					jsonBill.setName("旅行社充值");
					jsonBill.setIndentId(String.valueOf(baseRechargeRec.getBackRechargeId()));
					jsonBill.setTransRecId(String.valueOf(baseRechargeRec.getBackRechargeId()));
					
					Travservice travservice = baseRechargeRec.getBaseTravelAgency();
					jsonBill.setOtherSide(travservice.getName());
					jsonBill.setIndentSource("旅行社充值");
					jsonBill.setMoney(baseRechargeRec.getCashMoney());
					jsonBill.setStatus("已完成");
						
					jsonBills.add(jsonBill);
				}
			}
			
			result.setNum(allRecords);
			result.setTransRec(jsonBills);	
			break;
		}
		
		return result;
	}
	
	@RequestMapping(value = "/summaryOutBill", method = RequestMethod.GET)  
	public String summaryOutBills(Model uiModel,
								  @RequestParam(value = "timeType") String timeType,
								  @RequestParam(value = "startDate") String startDate,
								  @RequestParam(value = "endDate") String endDate,
								  @RequestParam(value = "state") String state,
								  @RequestParam(value = "keyword") String keyword,
								  @RequestParam(value = "keyValue") String keyValue,
								  @RequestParam(value = "lessMoney") String lessMoney,
								  @RequestParam(value = "moreMoney") String moreMoney,
								  @RequestParam(value = "cashDir") String cashDir,
								  @RequestParam(value = "alipay") Boolean alipay,
								  @RequestParam(value = "other") Boolean other,
								  @RequestParam(value = "tradeClass") String tradeClass) throws ParseException {
		HashMap<String, JSONSummaryOutBill> tsSummaryMap = new HashMap<String, JSONSummaryOutBill>();
		HashMap<String, JSONSummaryOutBill> tuSummaryMap = new HashMap<String, JSONSummaryOutBill>();
		HashMap<String, JSONSummaryOutBill> rechargeSummaryMap = new HashMap<String, JSONSummaryOutBill>();
		int allRecords = 0;
		
		if (currentUser == null) {
			return "index";
		}
		
		if (currentUser.getRole().getName().equals("ROLE_FINANCE")) {
			return "index";
		}
		
		uiModel.addAttribute("user", currentUser);
		
		switch (tradeClass) {
		case "00":
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(BaseTranRec.class);
			
			DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
			Date start = fmt.parse(startDate);
			Date end = fmt.parse(endDate);
			detachedCriteria.add(Property.forName("paymentTime").ge(start));
			detachedCriteria.add(Property.forName("paymentTime").le(end));
			
			if (!keyValue.equals("empty")) {
				detachedCriteria.add(Property.forName("tranRecId").eq(keyValue));
			}
			
			if (!lessMoney.equals("minus-infinite")) {
				detachedCriteria.add(Property.forName("payFee").ge(Double.valueOf(lessMoney)));
			}
			
			if (!moreMoney.equals("infinite")) {
				detachedCriteria.add(Property.forName("payFee").le(Double.valueOf(moreMoney)));
			}
			
			switch (cashDir) {
			case "01":
				detachedCriteria.add(Property.forName("payFee").ge(0.00));
				break;
				
			case "02":
				detachedCriteria.add(Property.forName("payFee").le(0.00));
				break;
			}
			
			List<BaseTranRec> baseTranRecs = baseTranRecService.findByCriteria(detachedCriteria);
			if (baseTranRecs != null) {
				allRecords = baseTranRecs.size();
				for (int i = 0; i < allRecords; i++) {
					BaseTranRec baseTranRec = baseTranRecs.get(i);
					switch (baseTranRec.getTicketIndent().getCustomerType()) {
					case "01": {
						if (tsSummaryMap.get(baseTranRec.getTicketIndentDetail().getTicketPrice().getTicketType().getTicketName()) != null) {
							JSONSummaryOutBill outBill = tsSummaryMap.get(baseTranRec.getTicketIndentDetail().getTicketPrice().getTicketType().getTicketName());
							if (baseTranRec.getMoney() > 0.00) {
								outBill.setInMoney(baseTranRec.getMoney() + outBill.getInMoney());
							}
							else {
								outBill.setOutMoney(baseTranRec.getMoney() + outBill.getOutMoney());
							}
							outBill.setTotal(outBill.getInMoney() + outBill.getOutMoney());
							tsSummaryMap.put(baseTranRec.getTicketIndentDetail().getTicketPrice().getTicketType().getTicketName(), outBill);
						}
						else {
							JSONSummaryOutBill outBill = new JSONSummaryOutBill();
							outBill.setName(baseTranRec.getTicketIndentDetail().getTicketPrice().getTicketType().getTicketName());
							if (baseTranRec.getMoney() > 0.00) {
								outBill.setInMoney(baseTranRec.getMoney());
								outBill.setOutMoney(0.00);
							}
							else {
								outBill.setOutMoney(baseTranRec.getMoney());
								outBill.setInMoney(0.00);
							}
							outBill.setTotal(outBill.getInMoney() + outBill.getOutMoney());
							tsSummaryMap.put(baseTranRec.getTicketIndentDetail().getTicketPrice().getTicketType().getTicketName(), outBill);
						}
					}
						
					case "02": {
						if (tuSummaryMap.get(baseTranRec.getTicketIndentDetail().getTicketPrice().getTicketType().getTicketName()) != null) {
							JSONSummaryOutBill outBill = tuSummaryMap.get(baseTranRec.getTicketIndentDetail().getTicketPrice().getTicketType().getTicketName());
							if (baseTranRec.getMoney() > 0.00) {
								outBill.setInMoney(baseTranRec.getMoney() + outBill.getInMoney());
							}
							else {
								outBill.setOutMoney(baseTranRec.getMoney() + outBill.getOutMoney());
							}
							outBill.setTotal(outBill.getInMoney() + outBill.getOutMoney());
							tuSummaryMap.put(baseTranRec.getTicketIndentDetail().getTicketPrice().getTicketType().getTicketName(), outBill);
						}
						else {
							JSONSummaryOutBill outBill = new JSONSummaryOutBill();
							outBill.setName(baseTranRec.getTicketIndentDetail().getTicketPrice().getTicketType().getTicketName());
							if (baseTranRec.getMoney() > 0.00) {
								outBill.setInMoney(baseTranRec.getMoney());
								outBill.setOutMoney(0.00);
							}
							else {
								outBill.setOutMoney(baseTranRec.getMoney());
								outBill.setInMoney(0.00);
							}
							outBill.setTotal(outBill.getInMoney() + outBill.getOutMoney());
							tuSummaryMap.put(baseTranRec.getTicketIndentDetail().getTicketPrice().getTicketType().getTicketName(), outBill);
						}
					}
				}
			}
			
			detachedCriteria = DetachedCriteria.forClass(BaseRechargeRec.class);
			
			start = fmt.parse(startDate);
			end = fmt.parse(endDate);
			detachedCriteria.add(Property.forName("endDate").ge(start));
			detachedCriteria.add(Property.forName("endDate").le(end));
			
			if (!keyValue.equals("empty")) {
				detachedCriteria.add(Property.forName("rechargeRecId").eq(keyValue));
			}
			
			if (!lessMoney.equals("minus-infinite")) {
				detachedCriteria.add(Property.forName("cashMoney").ge(lessMoney));
			}
			
			if (!moreMoney.equals("infinite")) {
				detachedCriteria.add(Property.forName("cashMoney").le(moreMoney));
			}
			
			detachedCriteria.add(Property.forName("paymentType").eq("00"));
			
			List<BaseRechargeRec> baseRechargeRecs = baseRechargeRecService.findByCriteria(detachedCriteria);
			if (baseRechargeRecs != null) {
				for (int i = 0; i < baseRechargeRecs.size(); i++) {
					BaseRechargeRec baseRechargeRec = baseRechargeRecs.get(i);
					if (rechargeSummaryMap.get("旅行社充值") != null) {
						JSONSummaryOutBill outBill = rechargeSummaryMap.get("旅行社充值");
						outBill.setInMoney(baseRechargeRec.getCashMoney() + outBill.getInMoney());
						outBill.setTotal(outBill.getInMoney() + outBill.getOutMoney());
						rechargeSummaryMap.put("旅行社充值", outBill);
					}
					else {
						JSONSummaryOutBill outBill = new JSONSummaryOutBill();
						outBill.setName("旅行社充值");
						outBill.setInMoney(baseRechargeRec.getCashMoney());
						outBill.setOutMoney(0.00);
						outBill.setTotal(outBill.getInMoney() + outBill.getOutMoney());
						rechargeSummaryMap.put("旅行社充值", outBill);
					}					
				}
			}
			}
			
			break;
			
		case "01": 
			detachedCriteria = DetachedCriteria.forClass(BaseTranRec.class);
		
			fmt = new SimpleDateFormat("yyyy-MM-dd");
			start = fmt.parse(startDate);
			end = fmt.parse(endDate);
			detachedCriteria.add(Property.forName("paymentTime").ge(start));
			detachedCriteria.add(Property.forName("paymentTime").le(end));
			
			if (!keyValue.equals("empty")) {
				detachedCriteria.add(Property.forName("tranRecId").eq(keyValue));
			}
			
			if (!lessMoney.equals("minus-infinite")) {
				detachedCriteria.add(Property.forName("payFee").ge(lessMoney));
			}
			
			if (!moreMoney.equals("infinite")) {
				detachedCriteria.add(Property.forName("payFee").le(moreMoney));
			}
			
			switch (cashDir) {
			case "01":
				detachedCriteria.add(Property.forName("payFee").ge(0.00));
				break;
				
			case "02":
				detachedCriteria.add(Property.forName("payFee").le(0.00));
				break;
			}
			
			baseTranRecs = baseTranRecService.findByCriteria(detachedCriteria);
			if (baseTranRecs != null) {
				allRecords = baseTranRecs.size();
				for (int i = 0; i < allRecords; i++) {
					BaseTranRec baseTranRec = baseTranRecs.get(i);
					switch (baseTranRec.getTicketIndent().getCustomerType()) {
					case "01": {
						if (tsSummaryMap.get(baseTranRec.getTicketIndentDetail().getTicketPrice().getTicketType().getTicketName()) != null) {
							JSONSummaryOutBill outBill = tsSummaryMap.get(baseTranRec.getTicketIndentDetail().getTicketPrice().getTicketType().getTicketName());
							if (baseTranRec.getMoney() > 0.00) {
								outBill.setInMoney(baseTranRec.getMoney() + outBill.getInMoney());
							}
							else {
								outBill.setOutMoney(baseTranRec.getMoney() + outBill.getOutMoney());
							}
							outBill.setTotal(outBill.getInMoney() + outBill.getOutMoney());
							tsSummaryMap.put(baseTranRec.getTicketIndentDetail().getTicketPrice().getTicketType().getTicketName(), outBill);
						}
						else {
							JSONSummaryOutBill outBill = new JSONSummaryOutBill();
							outBill.setName(baseTranRec.getTicketIndentDetail().getTicketPrice().getTicketType().getTicketName());
							if (baseTranRec.getMoney() > 0.00) {
								outBill.setInMoney(baseTranRec.getMoney());
								outBill.setOutMoney(0.00);
							}
							else {
								outBill.setOutMoney(baseTranRec.getMoney());
								outBill.setInMoney(0.00);
							}
							outBill.setTotal(outBill.getInMoney() + outBill.getOutMoney());
							tsSummaryMap.put(baseTranRec.getTicketIndentDetail().getTicketPrice().getTicketType().getTicketName(), outBill);
						}
					}
						
					case "02": {
						if (tuSummaryMap.get(baseTranRec.getTicketIndentDetail().getTicketPrice().getTicketType().getTicketName()) != null) {
							JSONSummaryOutBill outBill = tuSummaryMap.get(baseTranRec.getTicketIndentDetail().getTicketPrice().getTicketType().getTicketName());
							if (baseTranRec.getMoney() > 0.00) {
								outBill.setInMoney(baseTranRec.getMoney() + outBill.getInMoney());
							}
							else {
								outBill.setOutMoney(baseTranRec.getMoney() + outBill.getOutMoney());
							}
							outBill.setTotal(outBill.getInMoney() + outBill.getOutMoney());
							tuSummaryMap.put(baseTranRec.getTicketIndentDetail().getTicketPrice().getTicketType().getTicketName(), outBill);
						}
						else {
							JSONSummaryOutBill outBill = new JSONSummaryOutBill();
							outBill.setName(baseTranRec.getTicketIndentDetail().getTicketPrice().getTicketType().getTicketName());
							if (baseTranRec.getMoney() > 0.00) {
								outBill.setInMoney(baseTranRec.getMoney());
								outBill.setOutMoney(0.00);
							}
							else {
								outBill.setOutMoney(baseTranRec.getMoney());
								outBill.setInMoney(0.00);
							}
							outBill.setTotal(outBill.getInMoney() + outBill.getOutMoney());
							tuSummaryMap.put(baseTranRec.getTicketIndentDetail().getTicketPrice().getTicketType().getTicketName(), outBill);
						}
					}
				}
			}
			}
			break;
			
		case "02":
			detachedCriteria = DetachedCriteria.forClass(BaseRechargeRec.class);
		
			fmt = new SimpleDateFormat("yyyy-MM-dd");
			start = fmt.parse(startDate);
			end = fmt.parse(endDate);
			detachedCriteria.add(Property.forName("endDate").ge(start));
			detachedCriteria.add(Property.forName("endDate").le(end));
			
			if (!keyValue.equals("empty")) {
				detachedCriteria.add(Property.forName("rechargeRecId").eq(keyValue));
			}
			
			if (!lessMoney.equals("minus-infinite")) {
				detachedCriteria.add(Property.forName("cashMoney").ge(lessMoney));
			}
			
			if (!moreMoney.equals("infinite")) {
				detachedCriteria.add(Property.forName("cashMoney").le(moreMoney));
			}
			
			detachedCriteria.add(Property.forName("paymentType").eq("00"));
			
			List<BaseRechargeRec> baseRechargeRecs = baseRechargeRecService.findByCriteria(detachedCriteria);
			if (baseRechargeRecs != null) {
				for (int i = 0; i < baseRechargeRecs.size(); i++) {
					BaseRechargeRec baseRechargeRec = baseRechargeRecs.get(i);
					if (rechargeSummaryMap.get("旅行社充值") != null) {
						JSONSummaryOutBill outBill = rechargeSummaryMap.get("旅行社充值");
						outBill.setInMoney(baseRechargeRec.getCashMoney() + outBill.getInMoney());
						outBill.setTotal(outBill.getInMoney() + outBill.getOutMoney());
						rechargeSummaryMap.put("旅行社充值", outBill);
					}
					else {
						JSONSummaryOutBill outBill = new JSONSummaryOutBill();
						outBill.setName("旅行社充值");
						outBill.setInMoney(baseRechargeRec.getCashMoney());
						outBill.setOutMoney(0.00);
						outBill.setTotal(outBill.getInMoney() + outBill.getOutMoney());
						rechargeSummaryMap.put("旅行社充值", outBill);
					}					
				}
			}
			break;
		}
	
		uiModel.addAttribute("tsSummarys", tsSummaryMap.values());
		uiModel.addAttribute("tuSummarys", tuSummaryMap.values());
		uiModel.addAttribute("rechargeSummarys", rechargeSummaryMap.values());
		return "finance/summaryOutBill";
	}
	
	@RequestMapping(value="/eInBill", method=RequestMethod.GET)
	public String eInBillForm(Model uiModel) {
		if (currentUser == null) {
			return "index";
		}
		
		if (currentUser.getRole().getName().equals("ROLE_FINANCE")) {
			return "index";
		}
		
		uiModel.addAttribute("user", currentUser);
		return "finance/eInBill";
	}
	
	@RequestMapping(value = "/getInBillData", method = RequestMethod.GET)  
	public @ResponseBody JSONInBills getFinancialBills(@RequestParam(value = "offset") int offset,
													 @RequestParam(value = "queryMode") String queryMode,
													 @RequestParam(value = "keyValue") String keyValue,
													 @RequestParam(value = "startDate") String startDate,
													 @RequestParam(value = "endDate") String endDate,
													 @RequestParam(value = "onlyAdjust") Boolean onlyAdjust) throws ParseException {
		int allRecords = 0;
		int count = 0;
		HashMap<TicketIndent, JSONInBill> itMap = new HashMap<TicketIndent, JSONInBill>();
		JSONInBills result = new JSONInBills();
		jsonInBills = new ArrayList<JSONInBill>();
		
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(BaseTranRec.class);
		switch (queryMode) {
		case "time":
			DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
			Date start = fmt.parse(startDate);
			Date end = fmt.parse(endDate);
			
			detachedCriteria.add(Property.forName("paymentTime").ge(start));
			detachedCriteria.add(Property.forName("paymentTime").le(end));
			List<BaseTranRec> baseTranRecs = baseTranRecService.findByCriteria(detachedCriteria);
			for (BaseTranRec baseTranRec : baseTranRecs) {
				if (itMap.containsKey(baseTranRec.getTicketIndent())) {
					JSONInBill bill = itMap.get(baseTranRec.getTicketIndent());
					if (baseTranRec.getTrxId().equals("11111111111111111111111111111111")) {
						bill.setRefundMoney(bill.getRefundMoney() - baseTranRec.getMoney());
						bill.setRemainMoney(bill.getRemainMoney() + baseTranRec.getMoney());
					}
					else {
						bill.setPayMoney(bill.getPayMoney() + baseTranRec.getMoney());
						bill.setPayTime(baseTranRec.getPaymentTime());
						bill.setRemainMoney(bill.getRemainMoney() + baseTranRec.getMoney());
					}
					
					itMap.put(baseTranRec.getTicketIndent(), bill);
				}
				else {
					JSONInBill bill = new JSONInBill();
					if (baseTranRec.getTrxId().equals("11111111111111111111111111111111")) {
						bill.setPayMoney(0.00);
						bill.setRefundMoney(baseTranRec.getMoney());
						bill.setRemainMoney(baseTranRec.getMoney());
					}
					else {
						bill.setPayMoney(baseTranRec.getMoney());
						bill.setRefundMoney(0.00);
						bill.setPayTime(baseTranRec.getPaymentTime());
						bill.setRemainMoney(baseTranRec.getMoney());
						
						bill.setIndentId(baseTranRec.getTicketIndent().getIndentId());
						switch (baseTranRec.getTicketIndent().getCustomerType()) {
						case "01":
							bill.setIndentSource("旅行社购票");
							break;
							
						case "02":
							bill.setIndentSource("游客购票");
							break;
						}
					}
					
					count++;
					allRecords++;
					itMap.put(baseTranRec.getTicketIndent(), bill);				
				}
			}
			
			jsonInBills.addAll(itMap.values());
			
			detachedCriteria = DetachedCriteria.forClass(BaseRechargeRec.class);
			detachedCriteria.add(Property.forName("endDate").ge(start));
			detachedCriteria.add(Property.forName("endDate").le(end));
			detachedCriteria.add(Property.forName("paymentType").eq("00"));
			
			List<BaseRechargeRec> baseRechargeRecs = baseRechargeRecService.findByCriteria(detachedCriteria);
			if (baseRechargeRecs != null) {
				allRecords += baseRechargeRecs.size();
				for (int i = 0; i < baseRechargeRecs.size(); i++) {
					count++;
						
					BaseRechargeRec baseRechargeRec = baseRechargeRecs.get(i);
					JSONInBill bill = new JSONInBill();
					bill.setIndentId(String.valueOf(baseRechargeRec.getBackRechargeId()));
					bill.setIndentSource("旅行社充值");
					bill.setPayTime(baseRechargeRec.getEndDate());
					bill.setPayMoney(baseRechargeRec.getCashMoney());
					bill.setRefundMoney(0.00);
					bill.setRemainMoney(baseRechargeRec.getCashMoney());
					
					jsonInBills.add(bill);
				}
			}
			
			break;
			
		case "id":
			TicketIndent ticketIndent = ticketIndentService.findById(keyValue);
			detachedCriteria = DetachedCriteria.forClass(BaseTranRec.class);
			detachedCriteria.add(Property.forName("ticketIndent").eq(ticketIndent));
			baseTranRecs = baseTranRecService.findByCriteria(detachedCriteria);
			for (BaseTranRec baseTranRec : baseTranRecs) {
				if (count > 9) {
					break;
				}
				
				if (itMap.containsKey(baseTranRec.getTicketIndent())) {
					JSONInBill bill = itMap.get(baseTranRec.getTicketIndent());
					if (baseTranRec.getTrxId().equals("11111111111111111111111111111111")) {
						bill.setRefundMoney(bill.getRefundMoney() - baseTranRec.getMoney());
						bill.setRemainMoney(bill.getRemainMoney() + baseTranRec.getMoney());
					}
					else {
						bill.setPayMoney(bill.getPayMoney() + baseTranRec.getMoney());
						bill.setPayTime(baseTranRec.getPaymentTime());
						bill.setRemainMoney(bill.getRemainMoney() + baseTranRec.getMoney());
					}
					
					itMap.put(baseTranRec.getTicketIndent(), bill);
				}
				else {
					JSONInBill bill = new JSONInBill();
					if (baseTranRec.getTrxId().equals("11111111111111111111111111111111")) {
						bill.setPayMoney(0.00);
						bill.setRefundMoney(baseTranRec.getMoney());
						bill.setRemainMoney(baseTranRec.getMoney());
					}
					else {
						bill.setPayMoney(baseTranRec.getMoney());
						bill.setRefundMoney(0.00);
						bill.setPayTime(baseTranRec.getPaymentTime());
						bill.setRemainMoney(baseTranRec.getMoney());
						count++;
					}
					
					itMap.put(baseTranRec.getTicketIndent(), bill);				
				}
			}
			jsonInBills.addAll(itMap.values());
			break;
		}
			
		result.setNum(allRecords);
		List<JSONInBill> jsonBills = new ArrayList<JSONInBill>();
		for (int i = 0; i < Math.min(10, jsonInBills.size()); i++) {
			jsonBills.add(jsonInBills.get(i));
		}
		result.setTransRec(jsonBills);	

		return result;
	}
	
	@RequestMapping(value = "/getInBillPage", method = RequestMethod.GET)  
	public @ResponseBody JSONInBills getFinancialBillPage(@RequestParam(value = "offset") int offset) {
		JSONInBills result = new JSONInBills();
		if (jsonInBills != null) {
			result.setNum(jsonInBills.size());
			List<JSONInBill> jsonBills = new ArrayList<JSONInBill>();
			for (int i = offset; i < Math.min(10 + offset, jsonInBills.size()); i++) {
				jsonBills.add(jsonInBills.get(i));
			}
			result.setTransRec(jsonBills);	
		}
		else {
			result.setNum(0);
			result.setTransRec(null);
		}
		
		return result;
	}
	
	@RequestMapping(value="/indentStatistic", method=RequestMethod.GET)
	public String indentStatistic(Model uiModel) throws ParseException {
		if (currentUser == null) {
			return "index";
		}
		
		if (currentUser.getRole().getName().equals("ROLE_FINANCE")) {
			return "index";
		}
		
		uiModel.addAttribute("user", currentUser);
		
		DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		Date start = fmt.parse("2016-1-1");
		Date end = new Date();
		
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(TicketIndent.class);
		detachedCriteria.add(Property.forName("bookDate").ge(start));
		detachedCriteria.add(Property.forName("bookDate").le(end));
		List<TicketIndent> totalIndents = ticketIndentService.findByCriteria(detachedCriteria);
		
		Integer totalOrder = totalIndents.size();
		Double totalMoney = 0.00;
		for (TicketIndent ticketIndent : totalIndents) {
			totalMoney += ticketIndent.getTotalPrice();
		}
		
		detachedCriteria.add(Restrictions.or(Property.forName("status").eq("01"), Property.forName("status").eq("08")));
		List<TicketIndent> successIndents = ticketIndentService.findByCriteria(detachedCriteria);
	
		Integer successOrder = successIndents.size();
		Double successMoney = 0.00;
		for (TicketIndent ticketIndent : successIndents) {
			for (TicketIndentDetail indentDetail : ticketIndent.getTicketIndentDetails()) {
				if (indentDetail.getStatus().equals("01")) {
					successMoney += indentDetail.getSubtotal();
				}
				if (indentDetail.getStatus().equals("08")) {
					successMoney += indentDetail.getSubtotal();
				}
			}
		}
		
		uiModel.addAttribute("totalOrder", totalOrder);
		uiModel.addAttribute("totalMoney", totalMoney);
		uiModel.addAttribute("successOrder", successOrder);
		uiModel.addAttribute("successMoney", successMoney);
		
		return "finance/IndentStatistic";
	}
	
	@RequestMapping(value="getIndentStatistic", method=RequestMethod.GET)
	public @ResponseBody String getIndentStatistic(@RequestParam(value="startDate") String startDate,
												   @RequestParam(value="endDate") String endDate) throws ParseException {
		DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		Date start = fmt.parse(startDate);
		Date end = fmt.parse(endDate);
		
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(TicketIndent.class);
		detachedCriteria.add(Property.forName("bookDate").ge(start));
		detachedCriteria.add(Property.forName("bookDate").le(end));
		List<TicketIndent> totalIndents = ticketIndentService.findByCriteria(detachedCriteria);
		
		Integer totalOrder = totalIndents.size();
		Double totalMoney = 0.00;
		for (TicketIndent ticketIndent : totalIndents) {
			totalMoney += ticketIndent.getTotalPrice();
		}

		detachedCriteria.add(Restrictions.or(Property.forName("status").eq("01"), Property.forName("status").eq("08")));
		List<TicketIndent> successIndents = ticketIndentService.findByCriteria(detachedCriteria);
	
		Integer successOrder = successIndents.size();
		Double successMoney = 0.00;
		for (TicketIndent ticketIndent : successIndents) {
			for (TicketIndentDetail indentDetail : ticketIndent.getTicketIndentDetails()) {
				if (indentDetail.getStatus().equals("01")) {
					successMoney += indentDetail.getSubtotal();
				}
				if (indentDetail.getStatus().equals("08")) {
					successMoney += indentDetail.getSubtotal();
				}
			}
		}
		
		return "{\"totalOrder\":\"" + totalOrder + "\"," + "\"totalMoney\":\"" + totalMoney + "\"," 
				 + "\"successOrder\":\"" + successOrder + "\", \"successMoney\":\"" + successMoney + "\"}"; 
	}
	
	@RequestMapping(value="/indentDownload", method=RequestMethod.GET)
	public String indentDownload(Model uiModel) {
		if (currentUser == null) {
			return "index";
		}
		
		if (currentUser.getRole().getName().equals("ROLE_FINANCE")) {
			return "index";
		}
		
		uiModel.addAttribute("user", currentUser);
		
		return "finance/IndentDownload";
	}
	
	@RequestMapping(value="/monthReportSelect", method=RequestMethod.GET)
	public String monthReportSelect(Model uiModel) {
		if (currentUser == null) {
			return "index";
		}
		
		if (currentUser.getRole().getName().equals("ROLE_FINANCE")) {
			return "index";
		}
		
		uiModel.addAttribute("user", currentUser);
		
		ArrayList<String> monthes = new ArrayList<String>();
		monthes.add("一月");
		monthes.add("二月");
		monthes.add("三月");
		monthes.add("四月");
		monthes.add("五月");
		monthes.add("六月");
		monthes.add("七月");
		monthes.add("八月");
		monthes.add("九月");
		monthes.add("十月");
		monthes.add("十一月");
		monthes.add("十二月");
		
		uiModel.addAttribute("monthes", monthes);
		return "finance/monthFinanceReportSelect";
	}
	
	@RequestMapping(value="/yearReportSelect", method=RequestMethod.GET)
	public String yearReportSelect(Model uiModel) {
		if (currentUser == null) {
			return "index";
		}
		
		if (currentUser.getRole().getName().equals("ROLE_FINANCE")) {
			return "index";
		}
		
		uiModel.addAttribute("user", currentUser);
		
		ArrayList<String> years = new ArrayList<String>();
		years.add("2016年");
		years.add("2017年");
		years.add("2018年");
		years.add("2019年");
		years.add("2020年");
		years.add("2021年");
		years.add("2022年");
		years.add("2023年");
		years.add("2024年");
		years.add("2025年");
		years.add("2026年");
		years.add("2027年");
		years.add("2028年");
		years.add("2029年");
		years.add("2030年");
		
		uiModel.addAttribute("years", years);
		return "finance/yearFinanceReportSelect";
	}	
	
	@RequestMapping(value="/monthReport", method=RequestMethod.GET)
	public String monthReport(@RequestParam(value="month") Integer month, Model uiModel) {
		if (currentUser == null) {
			return "index";
		}
		
		if (currentUser.getRole().getName().equals("ROLE_FINANCE")) {
			return "index";
		}
		
		uiModel.addAttribute("user", currentUser);
		
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(FinanceReport.class);
		detachedCriteria.add(Property.forName("reportType").eq("month"));
		detachedCriteria.add(Property.forName("reportKey").eq(month));
		List<FinanceReport> reports = financeReportService.findByCriteria(detachedCriteria);
		
		if (reports.isEmpty()) {
			uiModel.addAttribute("result", "未到月报生成时间，未找到相关报表数据");
			return "finance/reportNotGen";
		}
		else {
			uiModel.addAttribute("financeReports", reports);
			return "finance/financeReport";
		}
	}
	
	@RequestMapping(value="/yearReport", method=RequestMethod.GET)
	public String yearReport(@RequestParam(value="year") Integer year, Model uiModel) {
		if (currentUser == null) {
			return "index";
		}
		
		if (currentUser.getRole().getName().equals("ROLE_FINANCE")) {
			return "index";
		}
		
		uiModel.addAttribute("user", currentUser);
		
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(FinanceReport.class);
		detachedCriteria.add(Property.forName("reportType").eq("year"));
		detachedCriteria.add(Property.forName("reportKey").eq(year));
		List<FinanceReport> reports = financeReportService.findByCriteria(detachedCriteria);
		
		if (reports.isEmpty()) {
			uiModel.addAttribute("result", "未到年报生成时间，未找到相关报表数据");
			return "finance/reportNotGen";
		}
		else {
			uiModel.addAttribute("financeReports", reports);
			return "finance/financeReport";
		}
	}
	
	@RequestMapping(value="/RTPlot", method=RequestMethod.GET)
	public String RTPlot(Model uiModel) {
		if (currentUser == null) {
			return "index";
		}
		
		if (currentUser.getRole().getName().equals("ROLE_FINANCE")) {
			return "index";
		}
		
		uiModel.addAttribute("user", currentUser);
		
		return "finance/RealTimePlot";
	}
	
	@RequestMapping(value="/getSaleRTData", method=RequestMethod.GET)
	public @ResponseBody JSONRTDatas getSaleRTData(@RequestParam(value="cutTime") Long cutTime) {
		Long prevCutTime = cutTime - 30000;
		Date start = new Date(prevCutTime);
		Date end = new Date(cutTime);
		
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(TicketIndent.class);
		detachedCriteria.add(Property.forName("bookDate").ge(start));
		detachedCriteria.add(Property.forName("bookDate").le(end));
		List<TicketIndent> ticketIndents = ticketIndentService.findByCriteria(detachedCriteria);
		
		Integer saleNum = 0;
		Double saleMoney = 0.00;
		for (TicketIndent ticketIndent : ticketIndents) {
			for (TicketIndentDetail indentDetail : ticketIndent.getTicketIndentDetails()) {
				saleNum += indentDetail.getAmount();
				saleMoney += indentDetail.getSubtotal();
			}
		}
		
		saleRTIndent.add(saleNum);
		saleRTMoney.add(saleMoney);
		
		for (int i = 1; i < 10; i++) {
			saleRTIndent.add(i);
			saleRTMoney.add(Double.valueOf(i * 3));
		}
		
		JSONRTDatas result = new JSONRTDatas();
		result.setNum(saleRTIndent.size());
		result.setSaleNum(saleRTIndent);
		result.setSaleMoney(saleRTMoney);
		
		return result;
	}
	
	@RequestMapping(value="/yearStatChart", method=RequestMethod.GET)
	public String yearStatisticChart(Model uiModel) {
		if (currentUser == null) {
			return "index";
		}
		
		if (currentUser.getRole().getName().equals("ROLE_FINANCE")) {
			return "index";
		}
		
		uiModel.addAttribute("user", currentUser);
		
		ArrayList<String> years = new ArrayList<String>();
		years.add("2016年");
		years.add("2017年");
		years.add("2018年");
		years.add("2019年");
		years.add("2020年");
		years.add("2021年");
		years.add("2022年");
		years.add("2023年");
		years.add("2024年");
		years.add("2025年");
		years.add("2026年");
		years.add("2027年");
		years.add("2028年");
		years.add("2029年");
		years.add("2030年");
		
		uiModel.addAttribute("years", years);
		
		return "finance/yearStatChart";		
	}
	
	@RequestMapping(value="/getYearSaleData", method=RequestMethod.GET)
	public @ResponseBody JSONFinanceStats getYearSaleData(@RequestParam(value="year") Integer year) throws ParseException {
		JSONFinanceStats result = new JSONFinanceStats();
		List<JSONFinanceStat> stats = new ArrayList<JSONFinanceStat>();
	
		Calendar now = Calendar.getInstance();
		now.setTime(new Date());
		Integer currentYear = now.get(Calendar.YEAR);
		Integer maxMonth = 12;
		if (currentYear.equals(year)) {
			maxMonth = now.get(Calendar.MONTH) + 1;
		}
		else if (currentYear < year) {
			result.setNum(0);
			result.setStatDatas(null);
			return result;
		}
		
		boolean leap = false;
		if (year % 100 == 0) {
			if (year % 400 == 0) {
				leap = true;
			}
		}
		else {
			if (year % 4 == 0) {
				leap = true;
			}
		}
		
		HashMap<String, JSONFinanceStat> typeMap = new HashMap<String, JSONFinanceStat>();
		List<TicketType> ticketTypes = ticketTypeService.findAll();
		for (TicketType ticketType : ticketTypes) {
			JSONFinanceStat stat = new JSONFinanceStat();
			stat.setLabel(ticketType.getTicketName());
			List<Integer> datas = new ArrayList<Integer>();
			List<Double> amounts = new ArrayList<Double>();
			for (int k = 1; k <= maxMonth; k++) {
				datas.add(1);
				amounts.add(1.0);
			}
			stat.setDatas(datas);
			stat.setAmounts(amounts);
			stats.add(stat);
			typeMap.put(ticketType.getTicketName(), stat);
		}

		for (int i = 1; i <= maxMonth; i++) {
			DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
			String start = year + "-" + i + "-1";
			String end;
			if (i == 2) {
				if (leap) {
					end = year + "-" + i + "-28";
				}
				else {
					end = year + "-" + i + "-29";
				}
			}
			else {
				if (i == 1 || i == 3 || i == 5 || i == 7
						|| i == 8 || i == 10 || i == 12) {
					end = year + "-" + i + "-31";
				}
				else {
					end = year + "-" + i + "-30";
				}
			}
			
			Date startDate = fmt.parse(start);
			Date endDate = fmt.parse(end);
			
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(TicketIndent.class);
			detachedCriteria.add(Property.forName("bookDate").ge(startDate));
			detachedCriteria.add(Property.forName("bookDate").le(endDate));
			
			List<TicketIndent> ticketIndents = ticketIndentService.findByCriteria(detachedCriteria);
			for (TicketIndent ticketIndent : ticketIndents) {
				for (TicketIndentDetail indentDetail : ticketIndent.getTicketIndentDetails()) {
					if (indentDetail.getStatus().equals("01")) {
						JSONFinanceStat stat = typeMap.get(indentDetail.getTicketPrice().getTicketType().getTicketName());
						if (stat != null) {
							List<Integer> datas = stat.getDatas();
							List<Double> amounts = stat.getAmounts();
							datas.set(i, datas.get(i) + indentDetail.getAmount());
							amounts.set(i, amounts.get(i) + indentDetail.getSubtotal());
							stat.setDatas(datas);
							stat.setAmounts(amounts);
						}
					}
				}
			}
		}
		
		result.setNum(stats.size());
		result.setStatDatas(stats);
		
		return result;
	}
	
	@RequestMapping(value="/monthStatChart", method=RequestMethod.GET)
	public String monthStatisticChart(Model uiModel) {
		if (currentUser == null) {
			return "index";
		}
		
		if (currentUser.getRole().getName().equals("ROLE_FINANCE")) {
			return "index";
		}
		
		uiModel.addAttribute("user", currentUser);
		
		ArrayList<String> years = new ArrayList<String>();
		years.add("2016年");
		years.add("2017年");
		years.add("2018年");
		years.add("2019年");
		years.add("2020年");
		years.add("2021年");
		years.add("2022年");
		years.add("2023年");
		years.add("2024年");
		years.add("2025年");
		years.add("2026年");
		years.add("2027年");
		years.add("2028年");
		years.add("2029年");
		years.add("2030年");
		uiModel.addAttribute("years", years);
		
		ArrayList<String> monthes = new ArrayList<String>();
		monthes.add("一月");
		monthes.add("二月");
		monthes.add("三月");
		monthes.add("四月");
		monthes.add("五月");
		monthes.add("六月");
		monthes.add("七月");
		monthes.add("八月");
		monthes.add("九月");
		monthes.add("十月");
		monthes.add("十一月");
		monthes.add("十二月");
		uiModel.addAttribute("monthes", monthes);
		
		return "finance/monthStatChart";		
	}	
	
	@RequestMapping(value="/getMonthSaleData", method=RequestMethod.GET)
	public @ResponseBody JSONFinanceStats getMonthSaleData(@RequestParam(value="year") Integer year,
														   @RequestParam(value="month") Integer month) throws ParseException {
		JSONFinanceStats result = new JSONFinanceStats();
		List<JSONFinanceStat> stats = new ArrayList<JSONFinanceStat>();
		
		Integer maxMonth = month + 1;
		boolean leap = false;
		if (year % 100 == 0) {
			if (year % 400 == 0) {
				leap = true;
			}
		}
		else {
			if (year % 4 == 0) {
				leap = true;
			}
		}
		
		Integer maxDay;
		if (maxMonth == 1 || maxMonth == 3 || maxMonth == 5 || maxMonth == 7
			|| maxMonth == 8 || maxMonth == 10 || maxMonth == 12) {
			maxDay = 31;
		}
		else if (maxMonth == 2) {
			if (leap) 
				maxDay = 28;
			else
				maxDay = 29;
		}
		else {
			maxDay = 30;
		}
	
		Calendar now = Calendar.getInstance();
		now.setTime(new Date());
		Integer currentYear = now.get(Calendar.YEAR);
		if (currentYear.equals(year)) {
			Integer curMonth = now.get(Calendar.MONTH) + 1;
			if (maxMonth.equals(curMonth)) {
				maxDay = now.get(Calendar.DAY_OF_MONTH);
			}
			else if (maxMonth > curMonth) {
				result.setNum(0);
				result.setStatDatas(null);
				return result;
			}
		}
		else if (currentYear < year) {
			result.setNum(0);
			result.setStatDatas(null);
			return result;
		}
		
		HashMap<String, JSONFinanceStat> typeMap = new HashMap<String, JSONFinanceStat>();
		List<TicketType> ticketTypes = ticketTypeService.findAll();
		for (TicketType ticketType : ticketTypes) {
			JSONFinanceStat stat = new JSONFinanceStat();
			stat.setLabel(ticketType.getTicketName());
			List<Integer> datas = new ArrayList<Integer>();
			List<Double> amounts = new ArrayList<Double>();
			for (int k = 1; k <= maxDay; k++) {
				datas.add(1);
				amounts.add(1.0);
			}
			stat.setDatas(datas);
			stat.setAmounts(amounts);
			stats.add(stat);
			typeMap.put(ticketType.getTicketName(), stat);
		}

		for (int i = 1; i <= maxDay; i++) {
			DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
			String end = year + "-" + maxMonth + "-" + i;
			
			Date endDate = fmt.parse(end);
			
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(TicketIndent.class);
			detachedCriteria.add(Property.forName("bookDate").eq(endDate));
			
			List<TicketIndent> ticketIndents = ticketIndentService.findByCriteria(detachedCriteria);
			for (TicketIndent ticketIndent : ticketIndents) {
				for (TicketIndentDetail indentDetail : ticketIndent.getTicketIndentDetails()) {
					if (indentDetail.getStatus().equals("01")) {
						JSONFinanceStat stat = typeMap.get(indentDetail.getTicketPrice().getTicketType().getTicketName());
						if (stat != null) {
							List<Integer> datas = stat.getDatas();
							List<Double> amounts = stat.getAmounts();
							datas.set(i, datas.get(i) + indentDetail.getAmount());
							amounts.set(i, amounts.get(i) + indentDetail.getSubtotal());
							stat.setDatas(datas);
							stat.setAmounts(amounts);
						}
					}
				}
			}
		}
		
		result.setNum(stats.size());
		result.setStatDatas(stats);
		
		return result;
	}	
	
	@RequestMapping(value="/dayStatChart", method=RequestMethod.GET)
	public String dayStatisticChart(Model uiModel) {
		if (currentUser == null) {
			return "index";
		}
		
		if (currentUser.getRole().getName().equals("ROLE_FINANCE")) {
			return "index";
		}
		
		uiModel.addAttribute("user", currentUser);
		
		return "finance/dayStatChart";		
	}	
	
	@RequestMapping(value="/getDaySaleData", method=RequestMethod.GET)
	public @ResponseBody JSONFinanceStats getDaySaleData(@RequestParam(value="day") String day) throws ParseException {
		JSONFinanceStats result = new JSONFinanceStats();
		List<JSONFinanceStat> stats = new ArrayList<JSONFinanceStat>();

		DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		Date date = fmt.parse(day);
		Date now = new Date();
		if (now.getTime() < date.getTime()) {
			result.setNum(0);
			result.setStatDatas(null);
			return result;
		}
		
		HashMap<String, JSONFinanceStat> typeMap = new HashMap<String, JSONFinanceStat>();
		List<TicketType> ticketTypes = ticketTypeService.findAll();
		for (TicketType ticketType : ticketTypes) {
			JSONFinanceStat stat = new JSONFinanceStat();
			stat.setLabel(ticketType.getTicketName());
			List<Integer> datas = new ArrayList<Integer>();
			List<Double> amounts = new ArrayList<Double>();
			datas.add(1);
			amounts.add(1.0);
			stat.setDatas(datas);
			stat.setAmounts(amounts);
			stats.add(stat);
			typeMap.put(ticketType.getTicketName(), stat);
		}

		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(TicketIndent.class);
		detachedCriteria.add(Property.forName("bookDate").eq(date));
			
		List<TicketIndent> ticketIndents = ticketIndentService.findByCriteria(detachedCriteria);
		for (TicketIndent ticketIndent : ticketIndents) {
			for (TicketIndentDetail indentDetail : ticketIndent.getTicketIndentDetails()) {
				if (indentDetail.getStatus().equals("01")) {
					JSONFinanceStat stat = typeMap.get(indentDetail.getTicketPrice().getTicketType().getTicketName());
					if (stat != null) {
						List<Integer> datas = stat.getDatas();
						List<Double> amounts = stat.getAmounts();
						datas.set(0, datas.get(0) + indentDetail.getAmount());
						amounts.set(0, amounts.get(0) + indentDetail.getSubtotal());
						stat.setDatas(datas);
						stat.setAmounts(amounts);
					}
				}
			}
		}
		
		result.setNum(stats.size());
		result.setStatDatas(stats);
		
		return result;
	}		
	
	@RequestMapping(value="/indentTrace", method=RequestMethod.GET)
	public String traceTicketIndent(Model uiModel) {
		if (currentUser == null) {
			return "index";
		}
		
		if (currentUser.getRole().getName().equals("ROLE_FINANCE")) {
			return "index";
		}
		
		uiModel.addAttribute("user", currentUser);
		
		return "finance/indentTrace";		
	}
	
	@RequestMapping(value = "/getTicketIndentTraceData", method = RequestMethod.GET)  
	public @ResponseBody JSONIndentTraces getTicketIndentTraceData(@RequestParam(value="userName") String userName,
																   @RequestParam(value="indentId") String indentId,
																   @RequestParam(value = "offset") int offset) {
		int allRecords = 0;
		JSONIndentTraces result = new JSONIndentTraces();
		List<JSONIndentTrace> jsonTraces = new ArrayList<JSONIndentTrace>();
		
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(TicketIndent.class);
		if (!userName.equals("all")) {
			DetachedCriteria criteria = DetachedCriteria.forClass(User.class);
			criteria.add(Property.forName("name").eq(userName));
			List<User> users = userService.findByCriteria(criteria);
			if (!users.isEmpty()) {
				Criterion cond = Property.forName("user").eq(users.get(0));
				for (int i = 1; i < users.size(); i++) {
					cond = Restrictions.or(cond, Property.forName("user").eq(users.get(i)));
				}
				detachedCriteria.add(cond);
			}
			else {
				result.setNum(Long.valueOf(0));
				return result;
			}
		}
		
		if (!indentId.equals("all")) {
			detachedCriteria.add(Property.forName("indentId").eq(indentId));
		}
		
		List<TicketIndent> ticketIndents = ticketIndentService.findByCriteria(detachedCriteria);
		if (ticketIndents != null) {
			allRecords = ticketIndents.size();
			int count = 0;
			for (int i = offset; i < allRecords; i++) {
				if (count > 9) {
					break;
				}
				count++;
				
				TicketIndent ticketIndent = ticketIndents.get(i);
				for (TicketIndentDetail ticketIndentDetail : ticketIndent.getTicketIndentDetails()) {
					JSONIndentTrace jsonIndentTrace = new JSONIndentTrace();
					jsonIndentTrace.setIndentId(ticketIndent.getIndentId());
				
			    	String orderid = ticketIndent.getIndentId();
			    	orderid = orderid.substring(12) + ticketIndentDetail.getIndentDetailId();
			    	jsonIndentTrace.setOrderId(orderid);
					
			    	DetachedCriteria criteria = DetachedCriteria.forClass(Travservice.class);
					criteria.add(Property.forName("user").eq(ticketIndent.getUser()));
					List<Travservice> travelAgencies = baseTravelAgencyService.findByCriteria(criteria);
					if (travelAgencies.size() > 0) {	
						jsonIndentTrace.setUserId(travelAgencies.get(0).getName());
					}
					else {
						jsonIndentTrace.setUserId(ticketIndent.getUser().getName());
					}
			    	
					switch (ticketIndentDetail.getStatus()) {
					case "00":
						jsonIndentTrace.setPlatFormStatus("00");
						jsonIndentTrace.setPlatFormTime(ticketIndentDetail.getInputTime());
						break;
						
					default:
						jsonIndentTrace.setPlatFormStatus(ticketIndentDetail.getStatus());
						jsonIndentTrace.setPlatFormTime(ticketIndentDetail.getUpdateTime());
						break;
					}
					
					jsonTraces.add(jsonIndentTrace);
				}
			}
			
			RESTFulTicketIndentTraces restTraces = new RESTFulTicketIndentTraces();
			List<RESTFulTicketIndentTrace> traces = new ArrayList<RESTFulTicketIndentTrace>();
			for (JSONIndentTrace trace : jsonTraces) {
				RESTFulTicketIndentTrace rest = new RESTFulTicketIndentTrace();
				rest.setOrderid(trace.getOrderId());
				traces.add(rest);
			}
			restTraces.setTicketIndentTraces(traces);
			
			// call xlpw web service here
			final String uri = systemSetting.getXlpwUrl() + "/rest/ticket/traceIndents";
		    RestTemplate restTemplate = new RestTemplate();
		    RESTFulTicketIndentTraces traceResult = restTemplate.postForObject(uri, restTraces, RESTFulTicketIndentTraces.class);
		  
		    if (traceResult == null) {
		    	result.setNum(Long.valueOf(0));
		    	return result;
		    }
		    
		    for (JSONIndentTrace trace : jsonTraces) {
				for (RESTFulTicketIndentTrace rest : traceResult.getTicketIndentTraces()) {
					if (trace.getOrderId().equals(rest.getOrderid())) {
						trace.setMachineStatus(rest.getMachineStatus());
						trace.setMachineTime(rest.getMachineTime());
						trace.setGateGuardTime(rest.getEntryGuardTime());
						trace.setGateGuardStatus(rest.getEntryGuardStatus());
						break;
					}
				}
		    }
			
			result.setNum(Long.valueOf(allRecords));
			result.setTraces(jsonTraces);
		}
		else {
			result.setNum(Long.valueOf(0));
		}
		
		return result;
	}		
}
