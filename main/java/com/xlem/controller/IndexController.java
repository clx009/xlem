package com.xlem.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.xlem.dao.BaseNotice;
import com.xlem.dao.Hotel;
import com.xlem.utils.RESTFulTicket;
import com.xlem.utils.RESTFulTickets;
import com.xlem.utils.SystemSetting;
import com.xlem.web.GetWeatherInfo;
import com.xlem.web.JSONNotice;
import com.xlem.web.JSONNotices;
import com.xlem.web.JSONWeather;

import service.comm.baseNotice.BaseNoticeService;
import service.hotel.hotel.HotelService;

@RequestMapping("/index")
@Controller
public class IndexController {

	final Logger logger = LoggerFactory.getLogger(IndexController.class);
	private int homeImageNum = 6;

	@Autowired
	private GetWeatherInfo weather;
	
	@Autowired
	private BaseNoticeService baseNoticeService;
	
	@Autowired
	private SystemSetting systemSetting;
	
	private RESTFulTickets restTickets = null;
	private List<RESTFulTicket> tickets = null;
	private List<Hotel> hotels = null;
	private int ticketShowGroup = 0;
	private int suiteShowGroup = 0;
	
	private Integer noticeShow = 0;
	
	@Autowired
	private HotelService hotelService;
	
	private void initTicketList() {
		// call xlpw web service here
		final String uri = systemSetting.getXlpwUrl() + "/rest/ticket/list";
		
	    RestTemplate restTemplate = new RestTemplate();
	     
	    restTickets = restTemplate.getForObject(uri, RESTFulTickets.class);
	    tickets = restTickets.getRESTFulTickets();
	}

	
	@RequestMapping(method=RequestMethod.GET)
	public String Index_show(Model uiModel) {
		logger.info("show index page.");
	
		initTicketList();
		hotels = hotelService.findAll();
		// uiModel.addAttribute("Hotels", hotels);
		return "index";
	}
	
	@RequestMapping(value="/nextImg/{curId}", method=RequestMethod.GET)
	public String getNextImage(Model uiModel, @PathVariable("curId") int curId) {
		int nextId = curId;
		if (nextId > homeImageNum) {
			nextId = 0;
		}
		else
			nextId++;
		
		return "/xlem/showImg?filename=sjyz" + nextId + ".jpeg";
	}
	
	
	@RequestMapping(value="/weatherForcast", method=RequestMethod.GET, produces="application/json")
	public @ResponseBody List<JSONWeather> weatherForcast() throws JSONException, IOException {
		// return "{\"msg\":\"you say:'" + "abc" + "'\"}"; 
		JSONArray forcast = weather.getWeather();
		
		List<JSONWeather> results = new ArrayList<JSONWeather>();

		JSONWeather weather = new JSONWeather();
		weather.setDate(forcast.getJSONObject(0).getString("date"));
		weather.setIcon("/xlem/showImg?filename=weather/" + forcast.getJSONObject(0).getString("code_d") + ".png");
		weather.setWeather_d(forcast.getJSONObject(0).getString("txt_d"));
		weather.setWeather_n(forcast.getJSONObject(0).getString("txt_n"));
		weather.setTemp(forcast.getJSONObject(0).getString("temp"));
		weather.setWind(forcast.getJSONObject(0).getString("wind"));
		results.add(weather);
		
		weather = new JSONWeather();
		weather.setDate(forcast.getJSONObject(1).getString("date"));
		weather.setIcon("/xlem/showImg?filename=weather/" + forcast.getJSONObject(1).getString("code_d") + ".png");
		weather.setWeather_d(forcast.getJSONObject(1).getString("txt_d"));
		weather.setWeather_n(forcast.getJSONObject(1).getString("txt_n"));
		weather.setTemp(forcast.getJSONObject(1).getString("temp"));
		weather.setWind(forcast.getJSONObject(1).getString("wind"));
		results.add(weather);

		weather = new JSONWeather();
		weather.setDate(forcast.getJSONObject(2).getString("date"));
		weather.setIcon("/xlem/showImg?filename=weather/" + forcast.getJSONObject(2).getString("code_d") + ".png");
		weather.setWeather_d(forcast.getJSONObject(2).getString("txt_d"));
		weather.setWeather_n(forcast.getJSONObject(2).getString("txt_n"));
		weather.setTemp(forcast.getJSONObject(2).getString("temp"));
		weather.setWind(forcast.getJSONObject(2).getString("wind"));
		results.add(weather);
		
		return results;
	}
	
	@RequestMapping(value="/showTickets", method=RequestMethod.GET)
	public @ResponseBody List<String> showTickets() {
		List<String> ticketImages = new ArrayList<String>();
		
		int i = 0;
		int singleTicket = 0;
		for (RESTFulTicket ticket : tickets) {
			if (!ticket.getIsSuite()) {
				singleTicket++;
				if (i < ticketShowGroup)
					continue;
				String image = systemSetting.getImageForTicket(ticket.getName());
				if (image == null) {
					image = systemSetting.getDefaultImage();
				}
				
				String obj = "/xlem/ticketShow?image=" + image + "&name=" + ticket.getName() + "&price=" + ticket.getPrice();
				ticketImages.add(obj);
				
				i++;
				if (i == ticketShowGroup + 3) {
					ticketShowGroup += 3;
					break;
				}
			}
		}
		
		if (ticketShowGroup + 3 > singleTicket ) {
			ticketShowGroup = 0;
		}

		return ticketImages;
	}
	
	@RequestMapping(value="/showHotels", method=RequestMethod.GET)
	public @ResponseBody List<String> showHotels() {
		List<String> hotelImages = new ArrayList<String>();
		
		for (Hotel hotel : hotels) {
			String image = systemSetting.getImageForHotel(hotel.getHotelName());
			if (image == null) {
				image = systemSetting.getDefaultImage();
			}
			String obj = "/xlem/hotelShow?image=" + image + "&name=" + hotel.getHotelName() + "&price=300起";
			hotelImages.add(obj);
				
		}
		return hotelImages;
	}

	@RequestMapping(value="/showSuites", method=RequestMethod.GET)
	public @ResponseBody List<String> showSuites() {
		List<String> suiteImages = new ArrayList<String>();
		
		int i = 0;
		int Suite = 0;
		for (RESTFulTicket ticket : tickets) {
			if (ticket.getIsSuite()) {
				Suite++;
				if (i < suiteShowGroup)
					continue;
			
				String image = systemSetting.getImageForTicket(ticket.getName());
				if (image == null) {
					image = systemSetting.getDefaultImage();
				}
				String obj = "/xlem/ticketShow?image=" + image + "&name=" + ticket.getName() + "&price=" + ticket.getPrice();
				suiteImages.add(obj);
				
				i++;
				if (i == suiteShowGroup + 3) {
					suiteShowGroup += 3;
					break;
				}
			}
		}
		
		if (suiteShowGroup + 3 > Suite ) {
			suiteShowGroup = 0;
		}

		return suiteImages;
	}
	
	@RequestMapping(value="showNotices", method=RequestMethod.GET)
	public @ResponseBody JSONNotices showNotices() {
		Date now = new Date();
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(BaseNotice.class);
		detachedCriteria.add(Property.forName("startDate").le(now));
		detachedCriteria.add(Property.forName("endDate").ge(now));
		List<BaseNotice> baseNotices = baseNoticeService.findByCriteria(detachedCriteria);
		
		List<JSONNotice> notices = new ArrayList<JSONNotice>();
		JSONNotices result = new JSONNotices();
		result.setNum(0);
		int count = 0;
		for (int i = noticeShow; i < baseNotices.size(); i++) {
			JSONNotice notice = new JSONNotice();
			notice.setNoticeId(baseNotices.get(i).getNoticeId());
			notice.setNoticeTitle(baseNotices.get(i).getNoticeTitle());
			notice.setRemark(baseNotices.get(i).getRemark());
			notice.setStartDate(baseNotices.get(i).getStartDate());
			notice.setEndDate(baseNotices.get(i).getEndDate());
			notices.add(notice);
			count++;
		}
		
		result.setDeployedNotices(notices);
		result.setNum(count);
		if (noticeShow + count >= baseNotices.size()) {
			noticeShow = 0;
		}
		else {
			noticeShow++;
		}
		
		return result;
	}
	
	@RequestMapping(value="showNoticeDetail/{noticeId}", method=RequestMethod.GET)
	public String showNoticeDetail(@PathVariable("noticeId") Long noticeId, Model uiModel) {
		BaseNotice baseNotice = baseNoticeService.findById(noticeId);
		
		uiModel.addAttribute("baseNotice", baseNotice);
		return "showNotice";
	}
}
