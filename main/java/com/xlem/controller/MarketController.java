package com.xlem.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.xlem.dao.Travservice;
import com.xlem.dao.User;
import com.xlem.utils.Message;
import com.xlem.utils.RESTFulTicket;
import com.xlem.utils.RESTFulTickets;
import com.xlem.utils.SystemSetting;
import com.xlem.utils.UrlUtil;
import com.xlem.web.JSONNotice;
import com.xlem.web.JSONNotices;
import com.xlem.web.JSONTicketIndents;
import com.xlem.web.JSONTicketPrice;
import com.xlem.web.JSON_TicketIndent;
import com.xlem.web.JSON_TicketIndentDetail;
import com.xlem.web.JSON_TicketPrices;
import com.xlem.dao.BaseCashAccount;
import com.xlem.dao.BaseNotice;
import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.BaseRewardAccount;
import com.xlem.dao.BaseRewardType;
import com.xlem.dao.Depart;
import com.xlem.dao.Role;
import com.xlem.dao.TicketIndent;
import com.xlem.dao.TicketIndentDetail;
import com.xlem.dao.TicketPrice;
import com.xlem.dao.TicketType;

import common.CommonMethod;
import dao.comm.baseRewardType.BaseRewardTypeDao;
import service.comm.baseCashAccount.BaseCashAccountService;
import service.comm.baseNotice.BaseNoticeService;
import service.comm.baseRechargeRec.BaseRechargeRecService;
import service.comm.baseRewardAccount.BaseRewardAccountService;
import service.comm.reg.baseTravelAgency.BaseTravelAgencyService;
import service.sys.role.RoleService;
import service.sys.user.UserService;
import service.ticket.ticketIndent.TicketIndentService;
import service.ticket.ticketIndentDetail.TicketIndentDetailService;
import service.ticket.ticketPrice.TicketPriceService;
import service.ticket.ticketType.TicketTypeService;

@Controller
@RequestMapping("/market")
public class MarketController {
	@Autowired
	private SystemSetting systemSetting;
	
	@Autowired
	MessageSource messageSource;
	
	@Autowired
	private UserService userService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private BaseTravelAgencyService baseTravelAgencyService;
	@Autowired
	private BaseRewardTypeDao baseRewardTypeDao;
	@Autowired
	private BaseRewardAccountService baseRewardAccountService;
	@Autowired
	private BaseRechargeRecService baseRechargeRecService;
	@Autowired
	private TicketIndentService ticketIndentService;
	@Autowired
	private TicketIndentDetailService ticketIndentDetailService;
	@Autowired
	private BaseCashAccountService baseCashAccountService;
	@Autowired
	private TicketTypeService ticketTypeService;
	@Autowired
	private TicketPriceService ticketPriceService;
	@Autowired
	private BaseNoticeService baseNoticeService;
	
	private User currentUser = null;
	
	@Autowired
	private HttpSession session;
	
	private List<TicketIndent> ticketIndents;
	
	private List<BaseNotice> baseNotices = null;
	
	private RESTFulTickets restTickets = null;
	private List<RESTFulTicket> tickets = null;
	
	private BaseNotice currentMsg = null;
	
	private void initTicketList() {
		// call xlpw web service here
		final String uri = systemSetting.getXlpwUrl() + "/rest/ticket/list";
		
	    RestTemplate restTemplate = new RestTemplate();
	     
	    restTickets = restTemplate.getForObject(uri, RESTFulTickets.class);
	    tickets = restTickets.getRESTFulTickets();
	}		
	
	@RequestMapping(value="/init/{id}", method=RequestMethod.GET)
	public String marketInit(@PathVariable(value="id") Long id, Model uiModel) {
		currentUser = userService.findById(id);
		uiModel.addAttribute("user", currentUser);
		
		return "market/init";
	}
	
	@RequestMapping(value="auditTravService", method=RequestMethod.GET)
	public String marketAuditTravService(Model uiModel) {
		if (currentUser != null) {
			uiModel.addAttribute("user", currentUser);
			
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Travservice.class);
			detachedCriteria.add(Property.forName("status").eq("00"));
			List<Travservice> travservices = baseTravelAgencyService.findByCriteria(detachedCriteria);
			uiModel.addAttribute("travservices", travservices);
			
			return "market/auditTravService";
		}
		else {
			return "login/market";
		}
			
	}
	
	@RequestMapping(value="auditedTravService", method=RequestMethod.GET)
	public String marketAuditedTravService(Model uiModel) {
		if (currentUser != null) {
			uiModel.addAttribute("user", currentUser);
			
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Travservice.class);
			detachedCriteria.add(Property.forName("status").eq("01"));
			List<Travservice> travservices = baseTravelAgencyService.findByCriteria(detachedCriteria);
			uiModel.addAttribute("travservices", travservices);
			
			return "market/auditedTravService";
		}
		else {
			return "login/market";
		}
			
	}	
	
	@RequestMapping(value="queryTravservice", method=RequestMethod.GET)
	public @ResponseBody List<Travservice> queryTravservice(@RequestParam(value="tsName") String tsName,
														    @RequestParam(value="linkMan") String linkMan,
														    @RequestParam(value="mobilePhone") String mobilePhone) {
		if (tsName.equals("all") && linkMan.equals("all") && mobilePhone.equals("all")) {
			List<Travservice> result = baseTravelAgencyService.findAll();
			return result;
		}
		
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Travservice.class);
		if (tsName != null) {
			detachedCriteria.add(Property.forName("name").eq(tsName));
		}
		if (linkMan != null) {
			detachedCriteria.add(Property.forName("contactor").eq(linkMan));
		}
		if (mobilePhone != null) {
			detachedCriteria.add(Property.forName("contactorMp").eq(mobilePhone));
		}
		
		List<Travservice> travservices = baseTravelAgencyService.findByCriteria(detachedCriteria);
		return travservices;
	}
	
	@RequestMapping(value="performAudit", method=RequestMethod.GET)
	public String performAudit(@RequestParam(value="travelAgencyId") Long travelAgencyId, Model uiModel) {
		if (currentUser != null) {
			uiModel.addAttribute("user", currentUser);
			Travservice ts = baseTravelAgencyService.findById(travelAgencyId);
		
			uiModel.addAttribute("travservice", ts);
			return "market/performTsAudit";
		}
		else {
			return "login/market";
		}
	}
	
	@RequestMapping(value="showImage", method=RequestMethod.GET)
	public String showImage(@RequestParam(value="travelAgencyId") Long travelAgencyId, 
							@RequestParam(value="fname") String fname, 
							Model uiModel) {
		if (currentUser != null) {
			uiModel.addAttribute("user", currentUser);	
			Travservice ts = baseTravelAgencyService.findById(travelAgencyId);
			uiModel.addAttribute("travservice", ts);
			uiModel.addAttribute("fname", fname);
			return "market/auditShowImage";
		}
		else {
			return "login/market";
		}
	}	
	
	@RequestMapping(value="auditPass", method=RequestMethod.POST)
	public @ResponseBody String auditPass(@RequestParam(value="travelAgencyId") Long travelAgencyId) {
		Travservice travservice = baseTravelAgencyService.findById(travelAgencyId);
		
		if (travservice != null) {
			travservice.setStatus("01");
			
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Role.class);
			detachedCriteria.add(Property.forName("name").eq("ROLE_TRAVELAGENCY"));
			List<Role> roles = roleService.findByCriteria(detachedCriteria);
			
			String name = "TA" + String.format("%06d", travelAgencyId);
			
			User tsUser = new User(roles.get(0), name, travservice.getContactorId(), false,
								   null, travservice.getContactorMp(), travservice.getEmail(),
								   null, "000000", null);
			userService.save(tsUser);
			travservice.setUser(tsUser);
			
			BaseCashAccount baseCashAccount = new BaseCashAccount();
			baseCashAccount.setBaseTravelAgency(travservice);
			baseCashAccount.setCashBalance(0.00);
			baseCashAccountService.save(baseCashAccount);
			
			baseTravelAgencyService.update(travservice);
			
			String content = "尊敬的" + travservice.getName() + "旅行社用户，\n" + "您的注册信息已通过审核。登录用户名为："
							 + name + "\n口令为：000000\n 请尽快登录修改密码";
			CommonMethod.sendSms(travservice.getContactorMp(), content, "true");
			return "{\"msg\":\"success" + "\"}"; 
		}
		else {
			return "{\"msg\":\"failed" + "\"}"; 
		}
	}
	
	@RequestMapping(value="auditFail", method=RequestMethod.POST)
	public @ResponseBody String auditFail(@RequestParam(value="travelAgencyId") Long travelAgencyId,
										  @RequestParam(value="failReason") String failReason) {
		Travservice travservice = baseTravelAgencyService.findById(travelAgencyId);
		
		if (travservice != null) {
			String content = "尊敬的" + travservice.getName() + "旅行社用户，\n" + "您的注册信息未通过审核。失败原因为："
							 + failReason + "\n请重新注册提交审核";
			CommonMethod.sendSms(travservice.getContactorMp(), content, "true");
			baseTravelAgencyService.delete(travservice);

			return "{\"msg\":\"success" + "\"}"; 
		}
		else {
			return "{\"msg\":\"failed" + "\"}"; 
		}
	}
	
	@RequestMapping(value="viewTravService/{travelAgencyId}", method=RequestMethod.GET)
	public String viewTravService(@PathVariable(value="travelAgencyId") Long travelAgencyId, Model uiModel) {
		if (currentUser != null) {
			uiModel.addAttribute("user", currentUser);	
		
			Travservice ts = baseTravelAgencyService.findById(travelAgencyId);
			uiModel.addAttribute("travservice", ts);
			return "market/viewTravService";
		}
		else {
			return "login/market";
		}
	}
	
	@RequestMapping(value="removeTravService", method=RequestMethod.GET)
	public @ResponseBody String removeTravService(@RequestParam(value="travelAgencyId") Long travelAgencyId) {
		Travservice ts = baseTravelAgencyService.findById(travelAgencyId);
		if (ts != null) {
			baseTravelAgencyService.delete(ts);
			return "{\"msg\":\"success" + "\"}"; 
		}
		
		return "{\"msg\":\"failed" + "\"}"; 
	}
	
	@RequestMapping(value="discountSetting", method=RequestMethod.GET)
	public String marketDiscountSetting(Model uiModel) {
		if (currentUser != null) {
			uiModel.addAttribute("user", currentUser);	
			
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Travservice.class);
			detachedCriteria.add(Property.forName("status").eq("01"));
			List<Travservice> travservices = baseTravelAgencyService.findByCriteria(detachedCriteria);
			uiModel.addAttribute("travservices", travservices);
			
			return "market/discountSetting";
		}
		else {
			return "login/market";
		}
	}
	
	@RequestMapping(value="discountSetting/{travelAgencyId}", method=RequestMethod.GET)
	public String discountSetting4TravService(@PathVariable(value="travelAgencyId") Long travelAgencyId, Model uiModel) {
		if (currentUser != null) {
			uiModel.addAttribute("user", currentUser);	
		
			Travservice ts = baseTravelAgencyService.findById(travelAgencyId);
			uiModel.addAttribute("travservice", ts);
			return "market/performDiscountSetting";
		}
		else {
			return "login/market";
		}
	}
	
	@RequestMapping(value="discountSure", method=RequestMethod.POST)
	public @ResponseBody String discountSetting(@RequestParam(value="travelAgencyId") Long travelAgencyId,
												@RequestParam(value="discount") String discount) {
		Travservice ts = baseTravelAgencyService.findById(travelAgencyId);
		if (ts != null) {
			ts.setDiscount(discount);
			baseTravelAgencyService.update(ts);
			return "{\"msg\":\"success" + "\"}"; 
		}
		
		return "{\"msg\":\"failed" + "\"}"; 
	}
	
	@RequestMapping(value="reward", method=RequestMethod.GET)
	public String reward(Model uiModel) {
		if (currentUser != null) {
			uiModel.addAttribute("user", currentUser);	
			
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Travservice.class);
			detachedCriteria.add(Property.forName("status").eq("01"));
			List<Travservice> travservices = baseTravelAgencyService.findByCriteria(detachedCriteria);
			uiModel.addAttribute("travservices", travservices);
			
			return "market/reward";
		}
		else {
			return "login/market";
		}
	}	
	
	@RequestMapping(value="reward/{travelAgencyId}", method=RequestMethod.GET)
	public String reward4TravService(@PathVariable(value="travelAgencyId") Long travelAgencyId, Model uiModel) {
		if (currentUser != null) {
			uiModel.addAttribute("user", currentUser);	
		
			Travservice ts = baseTravelAgencyService.findById(travelAgencyId);
			uiModel.addAttribute("travservice", ts);
			
			List<BaseRewardType> rewardTypes = baseRewardTypeDao.findAll();
			uiModel.addAttribute("rewardTypes", rewardTypes);
			
			return "market/performReward";
		}
		else {
			return "login/market";
		}
	}	
	
	@RequestMapping(value="rewardSure", method=RequestMethod.POST)
	public @ResponseBody String reward4Ts(@RequestParam(value="travelAgencyId") Long travelAgencyId,
										  @RequestParam(value="rewardType") String rewardType,
										  @RequestParam(value="reward") String reward) {
		Travservice ts = baseTravelAgencyService.findById(travelAgencyId);
		if (ts != null) {
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(BaseRewardType.class);
			detachedCriteria.add(Property.forName("rewardTypeName").eq(rewardType));
			List<BaseRewardType> rewardTypes = baseRewardTypeDao.findByCriteria(detachedCriteria);
			
			detachedCriteria = DetachedCriteria.forClass(BaseRewardAccount.class);
			detachedCriteria.add(Property.forName("baseTravelAgency").eq(ts));
			List<BaseRewardAccount> rewardAccounts = baseRewardAccountService.findByCriteria(detachedCriteria);
			
			Double rewardBalance;
			
			BaseRewardAccount rewardAccount;
			if (rewardAccounts.size() > 0) {
				rewardAccount = rewardAccounts.get(0);
				rewardAccount.setBaseRewardType(rewardTypes.get(0));
				rewardBalance = rewardAccount.getRewardBalance() + Double.parseDouble(reward);
				rewardAccount.setRewardBalance(rewardBalance);
				baseRewardAccountService.update(rewardAccount);
			}
			else {
				rewardAccount = new BaseRewardAccount(rewardTypes.get(0), ts, Double.parseDouble(reward), "00", 0.0); 
				baseRewardAccountService.save(rewardAccount);
				rewardBalance = Double.parseDouble(reward);
			}
		
			BaseRechargeRec rechargeRec = new BaseRechargeRec(ts, null, rewardAccount, "奖励充值", new Date(), null, null, 
												Double.parseDouble(reward), rewardBalance, "02", null, currentUser.getUserId(), 
												new Date(), null, null, null, null, null, null, null, "02");
			baseRechargeRecService.save(rechargeRec);		
			
			ts.setRewardBalance(rewardBalance);
			baseTravelAgencyService.update(ts);
			return "{\"msg\":\"success" + "\"}"; 
		}
		
		return "{\"msg\":\"failed" + "\"}"; 
	}	
	
	@RequestMapping(value="auditTicketIndent", method=RequestMethod.GET)
	public String auditTicketIndent(Model uiModel) {
		if (currentUser != null) {
			uiModel.addAttribute("user", currentUser);	
			
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Travservice.class);
			detachedCriteria.add(Property.forName("status").eq("01"));
			List<Travservice> travservices = baseTravelAgencyService.findByCriteria(detachedCriteria);
			uiModel.addAttribute("travservices", travservices);
			
			ticketIndents = new ArrayList<TicketIndent>();
		
			for (Travservice travservice : travservices) {
				detachedCriteria = DetachedCriteria.forClass(TicketIndent.class);
				detachedCriteria.add(Property.forName("user").eq(travservice.getUser()));
				List<TicketIndent> tis = ticketIndentService.findByCriteria(detachedCriteria);
				for (TicketIndent ticketIndent : tis) {
					List<TicketIndentDetail> details = new ArrayList<TicketIndentDetail>();
					for (TicketIndentDetail ticketIndentDetail : ticketIndent.getTicketIndentDetails()) {
						details.add(ticketIndentDetail);
					}
					ticketIndent.setDetails(details);
				}
				
				ticketIndents.addAll(tis);
			}
			uiModel.addAttribute("ticketIndents", ticketIndents);
			
			return "market/auditTicketIndent";
		}
		else {
			return "login/market";
		}		
	}
	
	@RequestMapping(value = "/getTicketIndentData", method = RequestMethod.GET)  
	public @ResponseBody JSONTicketIndents getTicketIndentData(@RequestParam(value = "offset") int offset) {
		int allRecords = 0;
		JSONTicketIndents result = new JSONTicketIndents();
		List<JSON_TicketIndent> jsonIndents = new ArrayList<JSON_TicketIndent>();
		if (ticketIndents != null) {
			allRecords = ticketIndents.size();
			int count = 0;
			for (int i = offset; i < allRecords; i++) {
				if (count > 9) {
					break;
				}
				count++;
				
				TicketIndent ticketIndent = ticketIndents.get(i);
				JSON_TicketIndent jsonTicketIndent = new JSON_TicketIndent();
				jsonTicketIndent.setIndentId(ticketIndent.getIndentId());
				jsonTicketIndent.setBookDate(ticketIndent.getBookDate());
				jsonTicketIndent.setStatus(ticketIndent.getStatus());
				
				DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Travservice.class);
				detachedCriteria.add(Property.forName("user").eq(ticketIndent.getUser()));
				List<Travservice> tss = baseTravelAgencyService.findByCriteria(detachedCriteria);
				jsonTicketIndent.setTravelAgencyName(tss.get(0).getName());
			
				List<JSON_TicketIndentDetail> jsonDetails = new ArrayList<JSON_TicketIndentDetail>();
				for (TicketIndentDetail ticketIndentDetail : ticketIndent.getTicketIndentDetails()) {
					JSON_TicketIndentDetail jsonDetail = new JSON_TicketIndentDetail();
					jsonDetail.setIndentDetailId(ticketIndentDetail.getIndentDetailId());
					if (ticketIndentDetail.getTicketPrice().getTicketType().getIntro() != null) {
						jsonDetail.setIntro(ticketIndentDetail.getTicketPrice().getTicketType().getIntro());
					}
					else {
						jsonDetail.setIntro("");
					}
					jsonDetail.setTicketName(ticketIndentDetail.getTicketPrice().getTicketType().getTicketName());
					jsonDetail.setUseDate(ticketIndentDetail.getUseDate());
					if (ticketIndentDetail.getSubtotal() != null) {
						jsonDetail.setSubtotal(ticketIndentDetail.getSubtotal());
					}
					else {
						jsonDetail.setSubtotal(0.00);
					}
					jsonDetail.setStatus(ticketIndentDetail.getStatus());
				
					jsonDetails.add(jsonDetail);
				}
			
				jsonTicketIndent.setDetails(jsonDetails);
				jsonIndents.add(jsonTicketIndent);
				
			}
			
			result.setNum(allRecords);
			result.setTicketIndents(jsonIndents);
		}
		else {
			result.setNum(0);
		}
		
		return result;
	}	
	
	@RequestMapping(value="passTicketIndent", method=RequestMethod.POST)
	public @ResponseBody String passTicketIndent(@RequestParam(value="ticketIndentId") String ticketIndentId,
												 @RequestParam(value="indentDetailId") String indentDetailId) {
		TicketIndent ticketIndent = ticketIndentService.findById(ticketIndentId);
		if (currentUser != null) {
			if (ticketIndent != null) {
				ticketIndent.setStatus("01");
				ticketIndent.setUpdater(currentUser.getUserId());
				ticketIndent.setUpdateTime(new Date());
				ticketIndentService.update(ticketIndent);
				
				for (TicketIndentDetail indentDetail : ticketIndent.getTicketIndentDetails()) {
					indentDetail.setStatus("01");
					indentDetail.setUpdater(currentUser.getUserId());
					indentDetail.setUpdateTime(new Date());
					ticketIndentDetailService.update(indentDetail);
				}
				
				DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Travservice.class);
				detachedCriteria.add(Property.forName("user").eq(ticketIndent.getUser()));
				List<Travservice> travelAgencies = baseTravelAgencyService.findByCriteria(detachedCriteria);
				String smsContent = "您所预订的订单:" + ticketIndentId + "已成功通过审核，可以正常使用。"; 
				CommonMethod.sendSms(travelAgencies.get(0).getContactorMp(), smsContent, "true");
				
				return "{\"msg\":\"success" + "\"}"; 
			}
			else {
				return "{\"msg\":\"invalid" + "\"}";
			}
		}
		else {
			return "{\"msg\":\"unlogined" + "\"}";
		}
	}
	
	@RequestMapping(value="ngTicketIndent", method=RequestMethod.POST)
	public @ResponseBody String ngTicketIndent(@RequestParam(value="ticketIndentId") String ticketIndentId,
											   @RequestParam(value="indentDetailId") String indentDetailId,
											   @RequestParam(value="failReason") String failReason) {
		TicketIndent ticketIndent = ticketIndentService.findById(ticketIndentId);
		if (currentUser != null) {
			if (ticketIndent != null) {
				DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Travservice.class);
				detachedCriteria.add(Property.forName("user").eq(ticketIndent.getUser()));
				List<Travservice> travelAgencies = baseTravelAgencyService.findByCriteria(detachedCriteria);
				String smsContent = "您所预订的订单:" + ticketIndentId + "未通过审核，失败原因: " + failReason + "。请重新预订。"; 
				CommonMethod.sendSms(travelAgencies.get(0).getContactorMp(), smsContent, "true");
				
				Double refund = 0.0;
				for (TicketIndentDetail indentDetail : ticketIndent.getTicketIndentDetails()) {
					refund += indentDetail.getSubtotal();
				}
				
				detachedCriteria = DetachedCriteria.forClass(BaseRewardType.class);
				detachedCriteria.add(Property.forName("rewardTypeName").eq("奖励退款"));
				List<BaseRewardType> baseRewardTypes = baseRewardTypeDao.findByCriteria(detachedCriteria);
				
				BaseRewardAccount baseRewardAccount = new BaseRewardAccount();
				baseRewardAccount.setBaseRewardType(baseRewardTypes.get(0));
				baseRewardAccount.setBaseTravelAgency(travelAgencies.get(0));
				baseRewardAccount.setBelongCompany("00");
				baseRewardAccount.setRewardBalance(-refund);
				baseRewardAccountService.save(baseRewardAccount);
				
				ticketIndent.setStatus("04");
				ticketIndent.setUpdater(currentUser.getUserId());
				ticketIndent.setUpdateTime(new Date());
				ticketIndentService.update(ticketIndent);
				
				for (TicketIndentDetail indentDetail : ticketIndent.getTicketIndentDetails()) {
					indentDetail.setStatus("04");
					indentDetail.setUpdater(currentUser.getUserId());
					indentDetail.setUpdateTime(new Date());
					ticketIndentDetailService.update(indentDetail);
				}				
				
				return "{\"msg\":\"success" + "\"}"; 
			}
			else {
				return "{\"msg\":\"invalid" + "\"}";
			}
		}
		else {
			return "{\"msg\":\"unlogined" + "\"}";
		}		
	}
	
	@RequestMapping(value="viewTicketIndent", method=RequestMethod.GET)
	public String viewTicketIndent(@RequestParam(value="ticketIndentId") String ticketIndentId,
								   @RequestParam(value="indentDetailId") String indentDetailId,
								   Model uiModel) {
		uiModel.addAttribute("user", currentUser);
		
		TicketIndent ticketIndent = ticketIndentService.findById(ticketIndentId);
		uiModel.addAttribute("ticketIndent", ticketIndent);
		
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Travservice.class);
		detachedCriteria.add(Property.forName("user").eq(ticketIndent.getUser()));
		List<Travservice> travservices = baseTravelAgencyService.findByCriteria(detachedCriteria);
		uiModel.addAttribute("travservice", travservices.get(0));
		
		for (TicketIndentDetail indentDetail : ticketIndent.getTicketIndentDetails()) {
			if (indentDetail.getIndentDetailId().equals(indentDetailId)) {
				uiModel.addAttribute("indentDetail", indentDetail);
				break;
			}
		}
		
		return "market/viewTicketIndent";
	}
	
	@RequestMapping(value="queryTicketIndent", method=RequestMethod.GET)
	public @ResponseBody String queryTicketIndent(@RequestParam(value="tsNameOfTI") String tsName,
												 @RequestParam(value="linkManOfTI") String linkMan,
												 @RequestParam(value="mobilePhoneOfTI") String mobilePhone) {
		List<Travservice> travservices = null;
		if (tsName.equals("all") && linkMan.equals("all") && mobilePhone.equals("all")) {
			travservices = baseTravelAgencyService.findAll();
		}
		
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Travservice.class);
		if (!tsName.equals("all")) {
			detachedCriteria.add(Property.forName("name").eq(tsName));
		}
		
		if (!linkMan.equals("all")) {
			detachedCriteria.add(Property.forName("contactor").eq(linkMan));
		}
		
		if (!mobilePhone.equals("all")) {
			detachedCriteria.add(Property.forName("contactorMp").eq(mobilePhone));
		}
		
		travservices = baseTravelAgencyService.findByCriteria(detachedCriteria);
		
		ticketIndents = new ArrayList<TicketIndent>();
		
		for (Travservice travservice : travservices) {
			detachedCriteria = DetachedCriteria.forClass(TicketIndent.class);
			detachedCriteria.add(Property.forName("user").eq(travservice.getUser()));
			List<TicketIndent> tis = ticketIndentService.findByCriteria(detachedCriteria);
			for (TicketIndent ticketIndent : tis) {
				List<TicketIndentDetail> details = new ArrayList<TicketIndentDetail>();
				for (TicketIndentDetail ticketIndentDetail : ticketIndent.getTicketIndentDetails()) {
					details.add(ticketIndentDetail);
				}
				ticketIndent.setDetails(details);
			}
			
			ticketIndents.addAll(tis);
		}
		
		return "{\"msg\":\"success" + "\"}"; 
	}
	
	@RequestMapping(value="ticketResourceControl", method=RequestMethod.GET)
	public String ticetResourceControl(Model uiModel) {
		if (currentUser != null) {
			if (tickets == null) {
				initTicketList();
			}
			
			for (RESTFulTicket ticket : tickets) {
				DetachedCriteria detachedCriteria = DetachedCriteria.forClass(TicketType.class);
				detachedCriteria.add(Property.forName("ticketName").eq(ticket.getName()));
				List<TicketType> ticketTypes = ticketTypeService.findByCriteria(detachedCriteria);
				
				if (ticketTypes.size() > 0) {
					DetachedCriteria dc = DetachedCriteria.forClass(TicketPrice.class);
					dc.add(Property.forName("ticketType").eq(ticketTypes.get(0)));
					List<TicketPrice> ticketPrices = ticketPriceService.findByCriteria(dc);
					TicketPrice ticketPrice = ticketPrices.get(0);
					
					if (ticket.getInfo() != null) {
						if (ticketTypes.get(0).getIntro() == null 
								|| !ticketTypes.get(0).getIntro().equals(ticket.getInfo())) {
							TicketType ticketType = ticketTypes.get(0);
							ticketType.setIntro(ticket.getInfo());
							ticketTypeService.update(ticketType);
						}
					}
					
					if (!ticket.getPrice().equals(ticketPrice.getPrintPrice())
							|| !ticket.getStart().equals(ticketPrice.getStartDate())
							|| !ticket.getEnd().equals(ticketPrice.getEndDate())) {
						ticketPrice.setPrintPrice(ticket.getPrice());
						ticketPrice.setStartDate(ticket.getStart());
						ticketPrice.setEndDate(ticket.getEnd());
						ticketPriceService.update(ticketPrice);
					}
				}
				else {
					TicketType ticketType = new TicketType(ticket.getName(), ticket.getInfo());
					ticketTypeService.save(ticketType);
					TicketPrice ticketPrice = new TicketPrice(ticketType, ticket.getPrice(), ticket.getPrice(), ticket.getStart(), ticket.getEnd());
					ticketPrice.setToplimit(5000);
					ticketPrice.setRemainToplimit(5000);
					ticketPriceService.save(ticketPrice);
				}
			}
			
			List<TicketPrice> results = ticketPriceService.findAll();
			uiModel.addAttribute("ticketPrices", results);
			uiModel.addAttribute("user", currentUser);
		
			return "market/ticketResCtrl";
			
		}
		else {
			return "login/market";
		}
	}
	
	@RequestMapping(value="changeTicketTopLimit", method=RequestMethod.POST)
	public @ResponseBody JSON_TicketPrices changeTicketTopLimit(@RequestParam(value="ticketPriceId") Long ticketPriceId,
																@RequestParam(value="topLimit") Integer topLimit) {
		TicketPrice ticketPrice = ticketPriceService.findById(ticketPriceId);
		if (ticketPrice != null) {
			Integer increase = topLimit - ticketPrice.getToplimit();
			ticketPrice.setToplimit(topLimit);
			ticketPrice.setRemainToplimit(ticketPrice.getRemainToplimit() + increase);
			ticketPriceService.update(ticketPrice);
		}
		
		List<TicketPrice> results = ticketPriceService.findAll();
		
		List<JSONTicketPrice> ticketPrices = new ArrayList<JSONTicketPrice>();
		for (TicketPrice tp : results) {
			JSONTicketPrice json = new JSONTicketPrice();
			json.setTicketPriceId(tp.getTicketPriceId());
			json.setTicketName(tp.getTicketType().getTicketName());
			json.setUnitPrice(tp.getUnitPrice());
			json.setTopLimit(tp.getToplimit());
			json.setRemainToplimit(tp.getRemainToplimit());
			
			ticketPrices.add(json);
		}
		
		JSON_TicketPrices resJSON = new JSON_TicketPrices();
		resJSON.setNum(ticketPrices.size());
		resJSON.setTicketPrices(ticketPrices);
		
		return resJSON;
	}
	
	@RequestMapping(value="deployMessage", method=RequestMethod.GET)
	public String deployMessgeForm(Model uiModel) {
		if (currentUser != null) {
			uiModel.addAttribute("user", currentUser);	

			currentMsg = new BaseNotice();
			return "market/deployMessage";
		}
		
		return "login/market_login";
	}
	
	@RequestMapping(value="msgStartTimeChange", method=RequestMethod.POST)
	public @ResponseBody String changeMsgStartTime(@RequestParam(value="startTime") String startTime) {
		if (currentMsg != null) {
			DateFormat fmt = new SimpleDateFormat("yyyy-mm-dd");
			try {
				currentMsg.setStartDate(fmt.parse(startTime));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "{\"msg\":\"success" + "\"}"; 
			
		}
		else {
			return "{\"msg\":\"invalid" + "\"}"; 
		}
	}
	
	@RequestMapping(value="msgEndTimeChange", method=RequestMethod.POST)
	public @ResponseBody String changeMsgEndTime(@RequestParam(value="endTime") String endTime) {
		if (currentMsg != null) {
			DateFormat fmt = new SimpleDateFormat("yyyy-mm-dd");
			try {
				currentMsg.setEndDate(fmt.parse(endTime));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "{\"msg\":\"success" + "\"}"; 
			
		}
		else {
			return "{\"msg\":\"invalid" + "\"}"; 
		}
	}
	
	@RequestMapping(value="performMsgDeploy", method=RequestMethod.POST)
	public @ResponseBody String performMsgDeploy(@RequestParam(value="title") String title,
												 @RequestParam(value="content") String content,
												 @RequestParam(value="startTime") Date startTime,
												 @RequestParam(value="endTime") Date endTime) throws ParseException {
		if (currentMsg != null) {
			Long userId = (Long) session.getAttribute("user_id");
			if (userId == null) {
				return "{\"msg\":\"notlogin" + "\"}"; 
			}
			
			currentMsg.setNoticeTitle(title);
			currentMsg.setRemark(content);
			currentMsg.setStartDate(startTime);
			currentMsg.setEndDate(endTime);
			currentMsg.setInputer(userId);
			currentMsg.setInputTime(new Date());
			
			baseNoticeService.save(currentMsg);
			return "{\"msg\":\"success" + "\"}"; 
		}
		else {
			return "{\"msg\":\"invalid" + "\"}"; 
		}
	}
	
	@RequestMapping(value="deployedMsgList", method=RequestMethod.GET)
	public String deployedMsgList(Model uiModel) {
		if (currentUser != null) {
			uiModel.addAttribute("user", currentUser);	

			baseNotices = baseNoticeService.findAll();
			return "market/deployedMsgList";
		}
		
		return "login/market_login";
	}
	
	@RequestMapping(value = "/getMsgData", method = RequestMethod.GET)  
	public @ResponseBody JSONNotices getMsgData(@RequestParam(value = "offset") int offset) {
		int allRecords = 0;
		JSONNotices result = new JSONNotices();
		List<JSONNotice> jsonNotices = new ArrayList<JSONNotice>();
		if (baseNotices != null) {
			allRecords = baseNotices.size();
			int count = 0;
			for (int i = offset; i < allRecords; i++) {
				if (count > 9) {
					break;
				}
				count++;
				
				BaseNotice baseNotice = baseNotices.get(i);
				JSONNotice jsonNotice = new JSONNotice();
				jsonNotice.setNoticeId(baseNotice.getNoticeId());
				jsonNotice.setNoticeTitle(baseNotice.getNoticeTitle());
				jsonNotice.setRemark(baseNotice.getRemark());
				jsonNotice.setStartDate(baseNotice.getStartDate());
				jsonNotice.setEndDate(baseNotice.getEndDate());
				
				jsonNotices.add(jsonNotice);
			}
			
			result.setNum(allRecords);
			result.setDeployedNotices(jsonNotices);
		}
		else {
			result.setNum(0);
		}
		
		return result;
	}	
	
	@RequestMapping(value="updateNotice/{noticeId}", params = "form", method=RequestMethod.GET)
	public String updateNoticeForm(@PathVariable("noticeId") Long noticeId, Model uiModel) {
		if (currentUser != null) {
			BaseNotice baseNotice = baseNoticeService.findById(noticeId);
			
			uiModel.addAttribute("user", currentUser);
			uiModel.addAttribute("baseNotice", baseNotice);
			
			return "market/modifyNotice";
		}
		
		return "login/market_login";
	}
	
	@RequestMapping(value="updateNotice/{noticeId}", params="form", method = RequestMethod.POST)
    public String updateNotice(@Valid BaseNotice baseNotice, BindingResult bindingResult, Model uiModel, 
    		HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes, Locale locale) {
        if (bindingResult.hasErrors()) {
        	uiModel.addAttribute("message", new Message("error", messageSource.getMessage("msg_update_fail", new Object[]{}, locale)));        	
            uiModel.addAttribute("baseNotice", baseNotice);
            return "market/modifyNotice";
        }
        uiModel.asMap().clear();
        redirectAttributes.addFlashAttribute("message", new Message("success", messageSource.getMessage("msg_update_success", new Object[]{}, locale)));        

        baseNotice.setUpdater((Long) session.getAttribute("user_id")); 
        baseNoticeService.update(baseNotice);
        return "redirect:/market/deployedMsgList";
    }
	
	@RequestMapping(value="removeNotice", method=RequestMethod.POST)
	public @ResponseBody String removeNotice(@RequestParam(value="noticeId") Long noticeId) {
		if (currentUser != null) {
			BaseNotice baseNotice = baseNoticeService.findById(noticeId);
			baseNoticeService.delete(baseNotice);
			baseNotices = baseNoticeService.findAll();
			
			return "{\"msg\":\"success" + "\"}"; 
		}
		else {
			return "{\"msg\":\"notlogin" + "\"}"; 
		}
	}
}
