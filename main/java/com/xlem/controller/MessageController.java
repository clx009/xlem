package com.xlem.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xlem.utils.SeriesNumber;
import common.CommonMethod;

@RequestMapping("/message")
@Controller
public class MessageController {

	private static HashMap<String, Long> smsInterval = new HashMap<String, Long>();
	private Long sendInterval = 60 * 1000L;
	private Long removeInterval = 5 * 60 * 1000L; 
	
	@Autowired
	private HttpSession session;
	
	@RequestMapping(value="randomCode", method=RequestMethod.GET)
	public @ResponseBody String getRandomCode() {
		String code = SeriesNumber.RandomString(6);
		session.setAttribute("sms_code", code);
		return "{\"code\":\"" + code + "\"}";
	}
	
	@RequestMapping(value="sendSMS", method=RequestMethod.POST)
	public @ResponseBody String sendSMS(@RequestParam(value="phone") String phone,
										@RequestParam(value="content") String content) {
		if (smsInterval.containsKey(phone)) {
			Date now = new Date();
			if (now.getTime() - smsInterval.get(phone) > sendInterval) {
				smsInterval.put(phone, now.getTime());
				CommonMethod.sendSms(phone, content, "true");
				return "{\"msg\":\"success" + "\"}"; 
			}
			else {
				return "{\"msg\":\"frequent" + "\"}"; 
			}
		}
		
		Date now = new Date();
		smsInterval.put(phone, now.getTime());
		CommonMethod.sendSms(phone, content, "true");
		
		session.setAttribute("sms_phone", phone);
		
		return "{\"msg\":\"success" + "\"}"; 
	}
	
	@Scheduled(cron="30 * * * * *")
	public void run() {
		// TODO Auto-generated method stub
		System.out.println("短信间隔控制定时器启动");
		Date now = new Date();
		Iterator iter = smsInterval.entrySet().iterator();
		while (iter.hasNext()) {
			Entry entry = (Entry) iter.next();
			String phone = (String) entry.getKey();
			Long anchor = (Long) entry.getValue();
			
			if (now.getTime() - anchor > removeInterval) {
				iter.remove();
			}
		}
	}
 }
