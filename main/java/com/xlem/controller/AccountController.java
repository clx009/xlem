package com.xlem.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.xlem.dao.Coupon;
import com.xlem.dao.TicketIndent;
import com.xlem.dao.TicketIndentDetail;
import com.xlem.dao.User;
import com.xlem.utils.Message;
import com.xlem.utils.UrlUtil;
import com.xlem.web.JSONTicket;
import com.xlem.web.JSONTicketIndents;
import com.xlem.web.JSON_TicketIndent;
import com.xlem.web.JSON_TicketIndentDetail;
import com.xlem.web.JSON_Tickets;

import service.sys.user.UserService;
import service.ticket.ticketIndent.TicketIndentService;

@Controller
@RequestMapping("/account")
public class AccountController {
	@Autowired 
	private UserService userService;
	@Autowired
	private TicketIndentService ticketIndentService;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private HttpSession session;
	
	private User currentUser = null;
	
	private List<TicketIndent> ticketIndents = null;
	
	@RequestMapping(value="/employee", params="form", method=RequestMethod.POST)
	public String createEmployee(User user, BindingResult bindingResult, Model uiModel, 
    		HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes, Locale locale) {
		final Logger logger = LoggerFactory.getLogger(AccountController.class);
		logger.info("Create user as employer");
		if (bindingResult.hasErrors()) {
			uiModel.addAttribute("message", new Message("error", 
					messageSource.getMessage("employee_save_fail", new Object[]{}, locale)));
			uiModel.addAttribute("user", user);
			return "account/employee_create";
		}
		
		uiModel.asMap().clear();
		redirectAttributes.addFlashAttribute("message", new Message("success",
				messageSource.getMessage("empolyee_save_success",  new Object[]{}, locale)));
		
		logger.info("employer id" + user.getUserId() + "employer name" + user.getName());
		userService.save(user);
		return "redirect:/account/" + UrlUtil.encodeUrlPathSegment(user.getUserId().toString(), 
																   httpServletRequest);
	}
		
	@RequestMapping(value="/employee", params="form", method=RequestMethod.GET)
	public String createEmployeeForm(Model uiModel) {
		User user = new User();
		uiModel.addAttribute("user", user);
		return "account/employee_create";
	}
	
	@RequestMapping(value="/tourist/{id}", method=RequestMethod.GET)
	public String initTourist(@PathVariable("id") Long id, Model uiModel) {
		Long user_id = (Long) session.getAttribute("user_id");
		if (user_id == null || user_id != id) {
			return "index";
		}		
		
		User user = userService.findById(id);
		uiModel.addAttribute("user", user);
		currentUser = user;
		return "account/tourist_init";
	}
	
	@RequestMapping(value="/tourist_info/{id}", method=RequestMethod.GET)
	public String showTouristInfo(@PathVariable("id") Long id, Model uiModel) {
		if (currentUser == null || currentUser.getUserId() != id) {
			currentUser = null;
			return "index";
		}
		
		User user = userService.findById(id);
		
		uiModel.addAttribute("user", user);
		currentUser = user;
		
		return "account/tourist_info";
	}
	
	@RequestMapping(value="/tourist_passwd/{id}", method=RequestMethod.GET)
	public String showTouristPasswd(@PathVariable("id") Long id, Model uiModel) {
		if (currentUser == null && currentUser.getUserId() != id) {
			currentUser = null;
			return "index";
		}
		
		User user = userService.findById(id);
		uiModel.addAttribute("user", user);
		currentUser = user;
		
		return "account/tourist_passwd";
	}
	
	@RequestMapping(value="/tourist_coupon/{id}", method=RequestMethod.GET)
	public String showTouristCoupon(@PathVariable("id") Long id, Model uiModel) {
		if (currentUser == null && currentUser.getUserId() != id) {
			currentUser = null;
			return "index";
		}
		
		User user = userService.findById(id);
		ArrayList<Coupon> coupons = new ArrayList<Coupon>();
		coupons.addAll(user.getCoupons());
		
		uiModel.addAttribute("user", user);
		uiModel.addAttribute("coupons", coupons);
		
		return "account/tourist_coupon_list";
	}
	
	@RequestMapping(value="/tourist_point/{id}", method=RequestMethod.GET)
	public String showTouristPoint(@PathVariable("id") Long id, Model uiModel) {
		if (currentUser == null && currentUser.getUserId() != id) {
			currentUser = null;
			return "index";
		}
		
		User user = userService.findById(id);
		
		uiModel.addAttribute("user", user);
		if (user.getAccumulate() != null)
			uiModel.addAttribute("point", user.getAccumulate());
		else
			uiModel.addAttribute("point", 0);
		
		return "account/tourist_point";
	}
	
	@RequestMapping(value="/tourist_ticket_order_list/{id}", method=RequestMethod.GET)
	public String ticketOrderList(@PathVariable("id") Long id, Model uiModel) {
		if (currentUser == null || currentUser.getUserId() != id) {
			currentUser = null;
			return "index";
		}
		
		User user = userService.findById(id);
		uiModel.addAttribute("user", user);
		currentUser = user;
	
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(TicketIndent.class);
		detachedCriteria.add(Property.forName("user").eq(user));
		detachedCriteria.add(Property.forName("status").ne("10"));
		ticketIndents = ticketIndentService.findByCriteria(detachedCriteria);
		for (TicketIndent ticketIndent : ticketIndents) {
			List<TicketIndentDetail> details = new ArrayList<TicketIndentDetail>();
			for (TicketIndentDetail ticketIndentDetail : ticketIndent.getTicketIndentDetails()) {
				if (ticketIndentDetail.getStatus().equals("10")) {
					continue;
				}
				
				details.add(ticketIndentDetail);
			}
			ticketIndent.setDetails(details);
		}
		uiModel.addAttribute("ticketIndents", ticketIndents);
		
		return "account/tourist_ticket_order_list";
	}
	
	@RequestMapping(value = "/getTicketIndentData", method = RequestMethod.GET)  
	public @ResponseBody JSONTicketIndents getTicketIndentData(@RequestParam(value = "offset") int offset) {
		int allRecords = 0;
		JSONTicketIndents result = new JSONTicketIndents();
		List<JSON_TicketIndent> jsonIndents = new ArrayList<JSON_TicketIndent>();
		
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(TicketIndent.class);
		detachedCriteria.add(Property.forName("user").eq(currentUser));
		detachedCriteria.add(Property.forName("status").ne("10"));
		ticketIndents = ticketIndentService.findByCriteria(detachedCriteria);
		
		if (ticketIndents != null) {
			allRecords = ticketIndents.size();
			int count = 0;
			for (int i = offset; i < allRecords; i++) {
				if (count > 9) {
					break;
				}
				count++;
				
				TicketIndent ticketIndent = ticketIndents.get(i);
				JSON_TicketIndent jsonTicketIndent = new JSON_TicketIndent();
				jsonTicketIndent.setIndentId(ticketIndent.getIndentId());
				jsonTicketIndent.setBookDate(ticketIndent.getBookDate());
				jsonTicketIndent.setStatus(ticketIndent.getStatus());
			
				List<JSON_TicketIndentDetail> jsonDetails = new ArrayList<JSON_TicketIndentDetail>();
				for (TicketIndentDetail ticketIndentDetail : ticketIndent.getTicketIndentDetails()) {
					if (ticketIndentDetail.getStatus().equals("10")) {
						continue;
					}
					
					JSON_TicketIndentDetail jsonDetail = new JSON_TicketIndentDetail();
					jsonDetail.setIndentDetailId(ticketIndentDetail.getIndentDetailId());
					if (ticketIndentDetail.getTicketPrice().getTicketType().getIntro() != null) {
						jsonDetail.setIntro(ticketIndentDetail.getTicketPrice().getTicketType().getIntro());
					}
					else {
						jsonDetail.setIntro("");
					}
					jsonDetail.setTicketName(ticketIndentDetail.getTicketPrice().getTicketType().getTicketName());
					jsonDetail.setUseDate(ticketIndentDetail.getUseDate());
					if (ticketIndentDetail.getSubtotal() != null) {
						jsonDetail.setSubtotal(ticketIndentDetail.getSubtotal());
					}
					else {
						jsonDetail.setSubtotal(0.00);
					}
					jsonDetail.setStatus(ticketIndentDetail.getStatus());
				
					jsonDetails.add(jsonDetail);
				}
			
				jsonTicketIndent.setDetails(jsonDetails);
				jsonIndents.add(jsonTicketIndent);
				
			}
			
			result.setNum(allRecords);
			result.setTicketIndents(jsonIndents);
		}
		else {
			result.setNum(0);
		}
		
		return result;
	}	
	
	@RequestMapping(value="refreshTicketIndent", method=RequestMethod.POST)
	public @ResponseBody String refreshTicketIndent() {
		if (currentUser != null) {
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(TicketIndent.class);
			detachedCriteria.add(Property.forName("user").eq(currentUser));
			detachedCriteria.add(Property.forName("status").ne("10"));
			ticketIndents = ticketIndentService.findByCriteria(detachedCriteria);
			return "{\"msg\":\"success" + "\"}"; 
		}
		else {
			return "{\"msg\":\"unlogined" + "\"}"; 
		}
	}
	
	@RequestMapping(value="updateTouristInfo", method=RequestMethod.POST) 
	public @ResponseBody String updateTouristInfo(@RequestParam(value="name") String name,
												  @RequestParam(value="mobilePhone") String mobilePhone,
												  @RequestParam(value="email") String email) {
		if (currentUser != null) {
			currentUser.setName(name);
			currentUser.setMobilePhone(mobilePhone);
			currentUser.setEmail(email);
			
			userService.update(currentUser);
			return "{\"msg\":\"success" + "\"}"; 
		}
		else {
			return "{\"msg\":\"invalid" + "\"}"; 
		}
	}
	
	@RequestMapping(value="updateTouristPasswd", method=RequestMethod.POST)
	public @ResponseBody String updateTouristPasswd(@RequestParam(value="originPasswd") String originPasswd,
													@RequestParam(value="newPasswd") String newPasswd) {
		if (currentUser != null) {
			String oldPasswd = currentUser.getPwd();
			if (oldPasswd.equals(originPasswd)) {
				currentUser.setPwd(newPasswd);
				userService.update(currentUser);
				return "{\"msg\":\"success" + "\"}"; 
			}
			else {
				return "{\"msg\":\"wrong" + "\"}"; 
			}
		}
		else {
			return "{\"msg\":\"invalid" + "\"}"; 
		}
	}
}
