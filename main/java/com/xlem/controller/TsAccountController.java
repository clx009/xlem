package com.xlem.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.xlem.dao.BaseCashAccount;
import com.xlem.dao.BaseRechargeRec;
import com.xlem.dao.BaseRewardAccount;
import com.xlem.dao.BaseTranRec;
import com.xlem.dao.Coupon;
import com.xlem.dao.TicketIndent;
import com.xlem.dao.TicketIndentDetail;
import com.xlem.dao.Travservice;
import com.xlem.dao.User;
import com.xlem.utils.Message;
import com.xlem.utils.RESTFulTicket;
import com.xlem.utils.RESTFulTicketIndent;
import com.xlem.utils.RESTFulTicketIndents;
import com.xlem.utils.SeriesNumber;
import com.xlem.utils.UrlUtil;
import com.xlem.web.JSONTicketIndents;
import com.xlem.web.JSON_TicketIndent;
import com.xlem.web.JSON_TicketIndentDetail;

import common.CommonMethod;
import common.pay.PayParams;
import dao.comm.baseTranRec.BaseTranRecDao;
import service.comm.baseCashAccount.BaseCashAccountService;
import service.comm.baseRechargeRec.BaseRechargeRecService;
import service.comm.baseRewardAccount.BaseRewardAccountService;
import service.comm.reg.baseTravelAgency.BaseTravelAgencyService;
import service.sys.user.UserService;
import service.ticket.ticketIndent.TicketIndentService;

@Controller
@RequestMapping("/ts_account")
public class TsAccountController {
	@Autowired 
	private UserService userService;
	@Autowired
	private BaseTravelAgencyService baseTravelAgencyService;
	@Autowired
	private BaseRewardAccountService baseRewardAccountService;
	@Autowired
	private BaseCashAccountService baseCashAccountService;
	@Autowired
	private BaseTranRecDao baseTranRecDao;
	@Autowired
	private BaseRechargeRecService baseRechargeRecService;
	@Autowired
	private TicketIndentService ticketIndentService;
	@Autowired
	private HttpSession session;
	@Autowired
	private MessageSource messageSource;
	
	private User currentUser = null;
	private Travservice currentTs = null;
	
	private List<TicketIndent> ticketIndents = null;
	private ArrayList<String> rechargeTradeNos = new ArrayList<String>();
	
	@RequestMapping(value="travelAgency/{id}", method=RequestMethod.GET)
	public String initTravelAgency(@PathVariable("id") Long id, Model uiModel) {
		User user = userService.findById(id);
		uiModel.addAttribute("user", user);
		
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Travservice.class);
		detachedCriteria.add(Property.forName("user").eq(user));
		List<Travservice> travservices = baseTravelAgencyService.findByCriteria(detachedCriteria);
		uiModel.addAttribute("travservice", travservices.get(0));
		
		currentUser = user;
		currentTs = travservices.get(0);
		return "ts_account/ts_init";
	}
	
	@RequestMapping(value="/ts_info/{id}", method=RequestMethod.GET)
	public String showTouristInfo(@PathVariable("id") Long id, Model uiModel) {
		Travservice travservice = baseTravelAgencyService.findById(id);
		uiModel.addAttribute("travservice", travservice);
		currentTs = travservice;
		
		User user = travservice.getUser();
		uiModel.addAttribute("user", user);
		currentUser = user;
		
		return "ts_account/ts_info";
	}
	
	@RequestMapping(value="showImage", method=RequestMethod.GET)
	public String showImage(@RequestParam(value="travelAgencyId") Long travelAgencyId, 
							@RequestParam(value="fname") String fname, 
							Model uiModel) {
		if (currentUser != null) {
			uiModel.addAttribute("user", currentUser);	
			Travservice ts = baseTravelAgencyService.findById(travelAgencyId);
			uiModel.addAttribute("travservice", ts);
			uiModel.addAttribute("fname", fname);
			return "ts_account/tsShowImage";
		}
		else {
			return "account/travservice";
		}
	}		
	
	@RequestMapping(value="/ts_passwd/{id}", method=RequestMethod.GET)
	public String showTouristPasswd(@PathVariable("id") Long id, Model uiModel) {
		User user = userService.findById(id);
		uiModel.addAttribute("user", user);
		
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Travservice.class);
		detachedCriteria.add(Property.forName("user").eq(user));
		List<Travservice> travservices = baseTravelAgencyService.findByCriteria(detachedCriteria);
		uiModel.addAttribute("travservice", travservices.get(0));
		
		currentUser = user;
		currentTs = travservices.get(0);
		
		return "ts_account/ts_passwd";
	}
	
	@RequestMapping(value="/ts_ticket_order_list/{id}", method=RequestMethod.GET)
	public String ticketOrderList(@PathVariable("id") Long id, Model uiModel) {
		User user = userService.findById(id);
		uiModel.addAttribute("user", user);
		currentUser = user;
		
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Travservice.class);
		detachedCriteria.add(Property.forName("user").eq(user));
		List<Travservice> travservices = baseTravelAgencyService.findByCriteria(detachedCriteria);
		if (travservices.size() > 0) {
			Travservice travservice = travservices.get(0);
			uiModel.addAttribute("travservice", travservice);
			
			Set<TicketIndent> TicketIndents = user.getTicketIndents();
			ticketIndents = new ArrayList<TicketIndent>();
			for (TicketIndent ticketIndent : TicketIndents) {
				if (ticketIndent.getStatus().equals("10")) {
					continue;
				}
				
				ArrayList<TicketIndentDetail> ticketIndentDetails = new ArrayList<TicketIndentDetail>();
				ticketIndentDetails.addAll(ticketIndent.getTicketIndentDetails());
				for (TicketIndentDetail detail : ticketIndentDetails) {
					if (detail.getStatus().equals("10")) {
						ticketIndentDetails.remove(detail);
					}
				}
				ticketIndent.setDetails(ticketIndentDetails);
				ticketIndents.add(ticketIndent);
			}
			uiModel.addAttribute("ticketIndents", ticketIndents);
		
			return "ts_account/ts_ticket_order_list";
		}
		else {
			return "/login/travservice";
		}
	}
	
	@RequestMapping(value = "/getTicketIndentData", method = RequestMethod.GET)  
	public @ResponseBody JSONTicketIndents getTicketIndentData(@RequestParam(value = "offset") int offset) {
		int allRecords = 0;
		JSONTicketIndents result = new JSONTicketIndents();
		List<JSON_TicketIndent> jsonIndents = new ArrayList<JSON_TicketIndent>();
		if (ticketIndents != null) {
			allRecords = ticketIndents.size();
			int count = 0;
			for (int i = offset; i < allRecords; i++) {
				if (count > 9) {
					break;
				}
				count++;
				
				TicketIndent ticketIndent = ticketIndents.get(i);
				JSON_TicketIndent jsonTicketIndent = new JSON_TicketIndent();
				jsonTicketIndent.setIndentId(ticketIndent.getIndentId());
				jsonTicketIndent.setBookDate(ticketIndent.getBookDate());
				jsonTicketIndent.setStatus(ticketIndent.getStatus());
			
				List<JSON_TicketIndentDetail> jsonDetails = new ArrayList<JSON_TicketIndentDetail>();
				for (TicketIndentDetail ticketIndentDetail : ticketIndent.getTicketIndentDetails()) {
					if (ticketIndentDetail.getStatus().equals("10")) {
						continue;
					}
					
					JSON_TicketIndentDetail jsonDetail = new JSON_TicketIndentDetail();
					jsonDetail.setIndentDetailId(ticketIndentDetail.getIndentDetailId());
					if (ticketIndentDetail.getTicketPrice().getTicketType().getIntro() != null) {
						jsonDetail.setIntro(ticketIndentDetail.getTicketPrice().getTicketType().getIntro());
					}
					else {
						jsonDetail.setIntro("");
					}
					jsonDetail.setTicketName(ticketIndentDetail.getTicketPrice().getTicketType().getTicketName());
					jsonDetail.setUseDate(ticketIndentDetail.getUseDate());
					if (ticketIndentDetail.getSubtotal() != null) {
						jsonDetail.setSubtotal(ticketIndentDetail.getSubtotal());
					}
					else {
						jsonDetail.setSubtotal(0.00);
					}
					jsonDetail.setStatus(ticketIndentDetail.getStatus());
				
					jsonDetails.add(jsonDetail);
				}
			
				jsonTicketIndent.setDetails(jsonDetails);
				jsonIndents.add(jsonTicketIndent);
				
			}
			
			result.setNum(allRecords);
			result.setTicketIndents(jsonIndents);
		}
		else {
			result.setNum(0);
		}
		
		return result;
	}	
	
	@RequestMapping(value="refreshTicketIndent", method=RequestMethod.POST)
	public @ResponseBody String refreshTicketIndent() {
		if (currentUser != null) {
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(TicketIndent.class);
			detachedCriteria.add(Property.forName("user").eq(currentUser));
			detachedCriteria.add(Property.forName("status").ne("10"));
			ticketIndents = ticketIndentService.findByCriteria(detachedCriteria);
			return "{\"msg\":\"success" + "\"}"; 
		}
		else {
			return "{\"msg\":\"unlogined" + "\"}"; 
		}
	}	
	
	@RequestMapping(value="updateTsInfo", method=RequestMethod.POST) 
	public @ResponseBody String updateTsInfo(@RequestParam(value="name") String name,
												  @RequestParam(value="mobilePhone") String mobilePhone,
												  @RequestParam(value="email") String email) {
		if (currentUser != null) {
			currentUser.setName(name);
			currentUser.setMobilePhone(mobilePhone);
			currentUser.setEmail(email);
			
			userService.update(currentUser);
			return "{\"msg\":\"success" + "\"}"; 
		}
		else {
			return "{\"msg\":\"invalid" + "\"}"; 
		}
	}
	
	@RequestMapping(value="updateTsPasswd", method=RequestMethod.POST)
	public @ResponseBody String updateTouristPasswd(@RequestParam(value="originPasswd") String originPasswd,
													@RequestParam(value="newPasswd") String newPasswd) {
		if (currentUser != null) {
			String oldPasswd = currentUser.getPwd();
			if (oldPasswd.equals(originPasswd)) {
				currentUser.setPwd(newPasswd);
				userService.update(currentUser);
				return "{\"msg\":\"success" + "\"}"; 
			}
			else {
				return "{\"msg\":\"wrong" + "\"}"; 
			}
		}
		else {
			return "{\"msg\":\"invalid" + "\"}"; 
		}
	}
	
	@RequestMapping(value="rewardAccount/{travelAgencyId}", method=RequestMethod.GET)
	public String rewardAccount(@PathVariable(value="travelAgencyId") Long travelAgencyId, Model uiModel) {
		if (currentUser != null) {
			uiModel.addAttribute("user", currentUser);
			
			Travservice travservice = baseTravelAgencyService.findById(travelAgencyId);
			uiModel.addAttribute("travservice", travservice);
			
			return "ts_account/ts_rewardAccount";
			
		}
		
		return "ts_account/ts_init";
	}
	
	@RequestMapping(value="rewardRecord/{travelAgencyId}", method=RequestMethod.GET)
	public String rewardList(@PathVariable(value="travelAgencyId") Long travelAgencyId, Model uiModel) {
		if (currentUser != null) {
			uiModel.addAttribute("user", currentUser);
			
			Travservice travservice = baseTravelAgencyService.findById(travelAgencyId);
			uiModel.addAttribute("travservice", travservice);
			
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(BaseRechargeRec.class);
			detachedCriteria.add((Property.forName("baseTravelAgency").eq(travservice)));
			detachedCriteria.add((Property.forName("fundType").eq("02")));  // 02 奖励
			List<BaseRechargeRec> rechargeRecs = baseRechargeRecService.findByCriteria(detachedCriteria);
			uiModel.addAttribute("rechargeRecs", rechargeRecs);
			
			return "ts_account/ts_reward_list";
			
		}
		
		return "ts_account/ts_init";
	}	
	
	@RequestMapping(value="cashAccount/{travelAgencyId}", method=RequestMethod.GET)
	public String cashAccount(@PathVariable(value="travelAgencyId") Long travelAgencyId, Model uiModel) {
		if (currentUser != null) {
			uiModel.addAttribute("user", currentUser);
			
			Travservice travservice = baseTravelAgencyService.findById(travelAgencyId);
			uiModel.addAttribute("travservice", travservice);
			
			return "ts_account/ts_cashAccount";
			
		}
		
		return "ts_account/ts_init";
	}
	
	@RequestMapping(value="cashRecord/{travelAgencyId}", method=RequestMethod.GET)
	public String cashList(@PathVariable(value="travelAgencyId") Long travelAgencyId, Model uiModel) {
		if (currentUser != null) {
			uiModel.addAttribute("user", currentUser);
			
			Travservice travservice = baseTravelAgencyService.findById(travelAgencyId);
			uiModel.addAttribute("travservice", travservice);
			
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(BaseRechargeRec.class);
			detachedCriteria.add((Property.forName("baseTravelAgency").eq(travservice)));
			detachedCriteria.add((Property.forName("fundType").eq("00"))); // 00 支付宝充值
			List<BaseRechargeRec> rechargeRecs = baseRechargeRecService.findByCriteria(detachedCriteria);
			uiModel.addAttribute("rechargeRecs", rechargeRecs);
			
			return "ts_account/ts_cash_list";
			
		}
		
		return "ts_account/ts_init";
	}
	
	@RequestMapping(value="entry", method=RequestMethod.GET)
	public String tsEntry(Model uiModel) {
		Long user_id = (Long) session.getAttribute("user_id");
		if (user_id != null) {
			User user = userService.findById(user_id);
			uiModel.addAttribute("user", user);
		
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Travservice.class);
			detachedCriteria.add(Property.forName("user").eq(user));
			List<Travservice> travservices = baseTravelAgencyService.findByCriteria(detachedCriteria);
			uiModel.addAttribute("travservice", travservices.get(0));
		
			currentUser = user;
			currentTs = travservices.get(0);
			return "ts_account/ts_init";
		}
		else {
			return "/login/travservice";
		}
	}
	
	@RequestMapping(value="alipayRecharge/{travelAgencyId}", method=RequestMethod.GET)
	public String alipayRecharge(@PathVariable(value="travelAgencyId") Long travelAgencyId,
								 Model uiModel) {
		if (currentUser != null) {
			uiModel.addAttribute("user", currentUser);
			Travservice travservice = baseTravelAgencyService.findById(travelAgencyId);
			uiModel.addAttribute("travservice", travservice);
			
			return "ts_account/ts_cashRecharge";
			
		}
		
		return "ts_account/ts_init";
	}
	
	@RequestMapping(value="getRechargeParam", method=RequestMethod.GET)
	public @ResponseBody List<PayParams> getPayParam(@RequestParam(value="recharge") String recharge) {
		Double money = Double.valueOf(recharge);
		if (currentUser != null) {
			try{
				DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Travservice.class);
				detachedCriteria.add(Property.forName("user").eq(currentUser));
				List<Travservice> travservices = baseTravelAgencyService.findByCriteria(detachedCriteria);
				
				detachedCriteria = DetachedCriteria.forClass(BaseCashAccount.class);
				detachedCriteria.add(Property.forName("baseTravelAgency").eq(travservices.get(0)));
				List<BaseCashAccount> baseCashAccounts = baseCashAccountService.findByCriteria(detachedCriteria);
				
				BaseRechargeRec baseRechargeRec = new BaseRechargeRec();
				Long baseRachargeRecId = baseRechargeRecService.getNextKey("Base_Recharge_Rec".toUpperCase(), 1);
				baseRechargeRec.setRechargeRecId(baseRachargeRecId);
				baseRechargeRec.setBaseTravelAgency(travservices.get(0));
				baseRechargeRec.setBaseCashAccount(baseCashAccounts.get(0));
				baseRechargeRec.setFundType("00");    // 现金
				baseRechargeRec.setCashMoney(0.00);   
				baseRechargeRec.setPaymentType("00"); // alipay
				baseRechargeRecService.save(baseRechargeRec);
				
				Long rechargeKey = baseRechargeRec.getRechargeRecId();
				String key = String.format("%018d" , rechargeKey);
				PayParams payParams = baseCashAccountService.savePayParamsForRecharge(key, money);
				if(payParams.getIsUsePay()!=null && payParams.getIsUsePay()==true){
					List<PayParams> resJSON = new ArrayList<PayParams>();
					resJSON.add(payParams);
					return resJSON;
				} else {//未使用网银支付，即用奖励款或者奖金余额支付
					return null;
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			
			return null;
		}
		
		return null;
	}	
	
	@RequestMapping(value="alipayNotify", method=RequestMethod.POST)
	public @ResponseBody String alipayNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Map<String,String> params = new HashMap<String,String>();  
        Map requestParams = request.getParameterMap();  
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {  
            String name = (String) iter.next();  
            String[] values = (String[]) requestParams.get(name);  
            String valueStr = "";  
            for (int i = 0; i < values.length; i++) {  
                valueStr = (i == values.length - 1) ? valueStr + values[i]: valueStr + values[i] + ",";  
            }  
            params.put(name, valueStr);  
        }  
        String out_trade_no = request.getParameter("out_trade_no");  
        String trade_status = request.getParameter("trade_status");
    	String notify_time = request.getParameter("notify_time");
    	String notify_type = request.getParameter("notify_type");
    	String notify_id = request.getParameter("notify_id");
    	String sign_type = request.getParameter("sign_type");
    	String sign = request.getParameter("sign");

    	String subject = request.getParameter("subject");
    	String payment_type = request.getParameter("payment_type");
    	String trade_no = request.getParameter("trade_no");

    	String gmt_create = request.getParameter("gmt_create");
    	String gmt_payment = request.getParameter("gmt_payment");
    	String gmt_close = request.getParameter("gmt_close");
    	String refund_status = request.getParameter("refund_status");
    	String gmt_refund = request.getParameter("gmt_refund");

    	String seller_email = request.getParameter("seller_email");
    	String buyer_email = request.getParameter("buyer_email");
    	String seller_id = request.getParameter("seller_id");
    	String buyer_id = request.getParameter("buyer_id");
    	String price = request.getParameter("price");

    	String total_fee = request.getParameter("total_fee");
    	String quantity = request.getParameter("quantity");
    	String body = request.getParameter("body");
    	String discount = request.getParameter("discount");
    	String is_total_fee_adjust = request.getParameter("is_total_fee_adjust");
    	String use_coupon = request.getParameter("use_coupon");

    	String extra_common_param = request.getParameter("extra_common_param");
    	String out_channel_type = request.getParameter("out_channel_type");
    	String out_channel_amount = request.getParameter("out_channel_amount");
    	String out_channel_inst = request.getParameter("out_channel_inst");
    
    	DetachedCriteria detachedCriteria = DetachedCriteria.forClass(BaseRechargeRec.class);
    	detachedCriteria.add(Property.forName("rechargeRecId").eq(Long.valueOf(out_trade_no)));
    	List<BaseRechargeRec> baseRechargeRecs = baseRechargeRecService.findByCriteria(detachedCriteria);
    	if (baseRechargeRecs.size() != 0 && trade_status.trim().equals("TRADE_SUCCESS")){//支付成功
			BaseRechargeRec baseRechargeRec = baseRechargeRecs.get(0);
			baseRechargeRec.setCashMoney(Double.valueOf(total_fee));
			baseRechargeRec.setEndDate(new Date());
			baseRechargeRecService.update(baseRechargeRec);
    		
    		String smsContent = "您已成功充值，充值金额：" + total_fee; 
    		CommonMethod.sendSms(baseRechargeRec.getBaseTravelAgency().getContactorMp(), smsContent, "true");
    		
    		BaseCashAccount baseCashAccount = baseRechargeRec.getBaseCashAccount();
    		baseCashAccount.setBaseTravelAgency(baseRechargeRec.getBaseTravelAgency());
    		baseCashAccount.setCashBalance(baseCashAccount.getCashBalance() + Double.valueOf(total_fee));
    		baseCashAccountService.update(baseCashAccount);
    		
    		Travservice travservice = baseRechargeRec.getBaseTravelAgency();
    		travservice.setCashBalance(travservice.getCashBalance() + Double.valueOf(total_fee));
    		baseTravelAgencyService.update(travservice);
    		
            System.out.println(">>>>>旅行社成功充值：" + out_trade_no);  
            response.getWriter().write("success");
            return "success"; 
        }else{//验证失败  
        	return "fail"; 
        }  
	}	
	
	@RequestMapping(value="alipayReturn", method=RequestMethod.GET)
	public String alipayReturn(HttpServletRequest request, HttpServletResponse response, Model uiModel) {
        Map<String,String> params = new HashMap<String,String>();  
        Map requestParams = request.getParameterMap();  
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {  
            String name = (String) iter.next();  
            String[] values = (String[]) requestParams.get(name);  
            String valueStr = "";  
            for (int i = 0; i < values.length; i++) {  
                valueStr = (i == values.length - 1) ? valueStr + values[i]: valueStr + values[i] + ",";  
            }  
            params.put(name, valueStr);  
        }  
        String out_trade_no = request.getParameter("out_trade_no");  
        String trade_status = request.getParameter("trade_status");
    	String notify_time = request.getParameter("notify_time");
    	String notify_type = request.getParameter("notify_type");
    	String notify_id = request.getParameter("notify_id");
    	String sign_type = request.getParameter("sign_type");
    	String sign = request.getParameter("sign");

    	String subject = request.getParameter("subject");
    	String payment_type = request.getParameter("payment_type");
    	String trade_no = request.getParameter("trade_no");

    	String gmt_create = request.getParameter("gmt_create");
    	String gmt_payment = request.getParameter("gmt_payment");
    	String gmt_close = request.getParameter("gmt_close");
    	String refund_status = request.getParameter("refund_status");
    	String gmt_refund = request.getParameter("gmt_refund");

    	String seller_email = request.getParameter("seller_email");
    	String buyer_email = request.getParameter("buyer_email");
    	String seller_id = request.getParameter("seller_id");
    	String buyer_id = request.getParameter("buyer_id");
    	String price = request.getParameter("price");

    	String total_fee = request.getParameter("total_fee");
    	String quantity = request.getParameter("quantity");
    	String body = request.getParameter("body");
    	String discount = request.getParameter("discount");
    	String is_total_fee_adjust = request.getParameter("is_total_fee_adjust");
    	String use_coupon = request.getParameter("use_coupon");

    	String extra_common_param = request.getParameter("extra_common_param");
    	String out_channel_type = request.getParameter("out_channel_type");
    	String out_channel_amount = request.getParameter("out_channel_amount");
    	String out_channel_inst = request.getParameter("out_channel_inst");
    
    	DetachedCriteria detachedCriteria = DetachedCriteria.forClass(BaseRechargeRec.class);
    	detachedCriteria.add(Property.forName("rechargeRecId").eq(Long.valueOf(out_trade_no)));
    	List<BaseRechargeRec> baseRechargeRecs = baseRechargeRecService.findByCriteria(detachedCriteria);
    	if (baseRechargeRecs.size() != 0 && trade_status.trim().equals("TRADE_SUCCESS")){//支付成功
    		BaseRechargeRec baseRechargeRec = baseRechargeRecs.get(0);
    		
            System.out.println(">>>>>旅行社成功充值：" + out_trade_no); 
        
            uiModel.addAttribute("payResult", "您的充值已经成功支付。");
            return "ts_account/alipayReturn"; 
        }else{//验证失败  
        	uiModel.addAttribute("payResult", "充值支付发生了错误。");
        	return "ts_account/alipayReturn"; 
        }  
	}		
}
