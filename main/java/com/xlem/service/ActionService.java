package com.xlem.service;

import java.util.ArrayList;
import java.util.List;

import com.xlem.dao.Priv;

public interface ActionService {
	ArrayList<Priv> findByRoleID(Long roleId);
}
