package com.xlem.service;
import java.util.List;

import com.xlem.dao.Depart;
import com.xlem.dao.User;

public interface UserService {
	public List<User> findAllUser();
	public User findUserById(Long id);
	public User findUserByName(String name);
	public User saveUser(User user);
	public void deleteUser(User user);
	
	public List<User> findAllEmployee();
	
	public List<Depart> findAllDepart();
	public Depart saveDepart(Depart depart);
	public Depart findDepartById(Long id);
	public void deleteDepart(Depart depart);
}
