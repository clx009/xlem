package com.xlem.service;

import java.util.ArrayList;

import com.xlem.dao.Role;

public interface RoleService {
	ArrayList<Role> getAllRoles();
	Role getRoleById(Long roleId);
	Long getRoleIdByName(String roleName);
}
