package com.xlem.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.Priv;
import com.xlem.dao.PrivHome;
import com.xlem.dao.Role;
import com.xlem.dao.RoleHome;
import com.xlem.dao.RolePriv;
import com.xlem.dao.RolePrivHome;
import com.xlem.service.ActionService;

import dao.sys.priv.PrivDao;
import dao.sys.role.RoleDao;
import dao.sys.rolepriv.RolePrivDao;

@Transactional
@Service
public class ActionServiceImpl implements ActionService {
	@Autowired
	private RoleDao roleDao;
	@Autowired
	private PrivDao privDao;
	@Autowired
	private RolePrivDao role_privDao;
	
	@Override
	public ArrayList<Priv> findByRoleID(Long roleId) {
		// TODO Auto-generated method stub
		ArrayList<Priv> result = new ArrayList<Priv>();
		Role role = roleDao.findById(roleId);
		List<RolePriv> role_privs = role_privDao.findAll();
		for (RolePriv rp : role_privs) {
			if (rp.getRoleId() == role.getRoleId()) {
				Priv action = privDao.findById(rp.getPrivId());
				result.add(action);
			}
		}
		
		return result;
	}

}
