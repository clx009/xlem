package com.xlem.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.Depart;
import com.xlem.dao.DepartHome;
import com.xlem.dao.User;
import com.xlem.service.UserService;
import com.xlem.dao.UserHome;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	private UserHome UserDao;
	private DepartHome DepartDao;
	
	@Override
	public List<User> findAllUser() {
		// TODO Auto-generated method stub
		return UserDao.findAll();
	}

	@Override
	public User findUserById(Long id) {
		// TODO Auto-generated method stub
		return UserDao.findById(id);
	}
	
	@Override
	public User findUserByName(String name) {
		// TODO Auto-generated method stub
		return UserDao.findByName(name);
	}	

	@Override
	public User saveUser(User user) {
		// TODO Auto-generated method stub
		UserDao.save(user);
		return user;
	}
	
	@Override
	public void deleteUser(User user) {
		// TODO Auto-generated method stub
		UserDao.delete(user);
	}
	
	@Override
	public List<User> findAllEmployee() {
		// TODO Auto-generated method stub
		return UserDao.findAllEmployee();
	}

	@Override
	public List<Depart> findAllDepart() {
		// TODO Auto-generated method stub
		return DepartDao.findAll();
	}
	
	@Override
	public Depart saveDepart(Depart depart) {	
		DepartDao.save(depart);
		return depart;
	}

	@Override
	public Depart findDepartById(Long id) {
		// TODO Auto-generated method stub
		return DepartDao.findById(id);
	}
	
	@Override
	public void deleteDepart(Depart depart) {
		DepartDao.delete(depart);
	}


}
