package com.xlem.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.Travservice;
import com.xlem.dao.TravserviceHome;
import com.xlem.service.TravService;

@Service
@Transactional
public class TravServiceImpl implements TravService {
	private TravserviceHome TravserviceDao;
	
	@Override
	public void saveTravService(Travservice ts) {
		TravserviceDao.save(ts);
	}
}
