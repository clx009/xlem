package com.xlem.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xlem.dao.Role;
import com.xlem.service.RoleService;

import dao.sys.role.RoleDao;


@Service
@Transactional
public class RoleServiceImpl implements RoleService {
	@Autowired
	RoleDao roleDao;
	
	@Override
	public ArrayList<Role> getAllRoles() {
		// TODO Auto-generated method stub
		return (ArrayList<Role>) roleDao.findAll();
	}
	
	@Override
	public Role getRoleById(Long roleId) {
		return roleDao.findById(roleId);
	}
	
	@Override
	public Long getRoleIdByName(String roleName) {
		List<Role> roles = roleDao.findAll();
		
		for (Role role : roles) {
			if (roleName.equals(role.getName())) {
				return role.getRoleId();
			}
		}
		
		return (long) -1;
	}

}
