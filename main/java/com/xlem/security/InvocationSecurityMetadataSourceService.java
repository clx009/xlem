package com.xlem.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.util.RequestMatcher;
import org.springframework.security.web.util.AntPathRequestMatcher;
import org.springframework.stereotype.Service;

import com.xlem.dao.Priv;
import com.xlem.dao.Role;
import com.xlem.service.ActionService;
import com.xlem.service.RoleService;
/*
 * 
 * 最核心的地方，就是提供某个资源对应的权限定义，即getAttributes方法返回的结果。
 * 注意，我例子中使用的是AntUrlPathMatcher这个path matcher来检查URL是否与资源定义匹配，
 * 事实上你还要用正则的方式来匹配，或者自己实现一个matcher。
 * 
 * 此类在初始化时，应该取到所有资源及其对应角色的定义
 * 
 * 说明：对于方法的spring注入，只能在方法和成员变量里注入，
 * 如果一个类要进行实例化的时候，不能注入对象和操作对象，
 * 所以在构造函数里不能进行操作注入的数据。
 */

@Service("InvocationSecurityMetadataSourceService")
public class InvocationSecurityMetadataSourceService implements FilterInvocationSecurityMetadataSource {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(InvocationSecurityMetadataSourceService.class);

	@Autowired
	private RoleService roleService;
	@Autowired
	private ActionService actionService; 
	
	private static Map<String, Collection<ConfigAttribute>> resourceMap = null;
	public  void loadResourceDefine() throws Exception  {
		resourceMap = new HashMap<String, Collection<ConfigAttribute>>();
		
		//通过数据库中的信息设置，resouce和role
		for (Role item : this.roleService.getAllRoles()){
			Collection<ConfigAttribute> atts = new ArrayList<ConfigAttribute>();
			ConfigAttribute ca = new SecurityConfig(item.getName());
			atts.add(ca);
			List<Priv> tActionList = actionService.findByRoleID(item.getRoleId());
			//把资源放入到spring security的resouceMap中
			for(Priv tAction : tActionList){
				logger.debug("获得角色：["+item.getName()+"]拥有的acton有："+tAction.getPrivFunc());
				resourceMap.put(tAction.getPrivFunc(), atts);
			}
		}
		
		/*//通过硬编码设置，resouce和role
		resourceMap = new HashMap<String, Collection<ConfigAttribute>>();
		Collection<ConfigAttribute> atts = new ArrayList<ConfigAttribute>();
		ConfigAttribute ca = new SecurityConfig("admin"); 
		atts.add(ca); 
		resourceMap.put("/jsp/admin.jsp", atts); 
		resourceMap.put("/swf/zara.html", atts);*/ 
		
	}
	
	// According to a URL, Find out permission configuration of this URL.
	public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
		if (logger.isDebugEnabled()) {
			logger.debug("getAttributes(Object) - start"); //$NON-NLS-1$
		}
		// guess object is a URL.
	String url = ((FilterInvocation) object).getRequestUrl();
		Iterator<String> ite = resourceMap.keySet().iterator();
		while (ite.hasNext()) {
			String resURL = ite.next();
			// RequestMatcher urlMatcher = new AntPathRequestMatcher(resURL);
			if (url.equals(resURL)) {
				Collection<ConfigAttribute> returnCollection = resourceMap.get(resURL);
				if (logger.isDebugEnabled()) {
					logger.debug("getAttributes(Object) - end"); //$NON-NLS-1$
				}
				return returnCollection;
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("getAttributes(Object) - end"); //$NON-NLS-1$
		}
		return null;
	}
	public boolean supports(Class<?> clazz) {
		return true;
	}
	public Collection<ConfigAttribute> getAllConfigAttributes() {
		return null;
	}
	public RoleService getRoleService() {
		return roleService;
	}
	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}
	public ActionService getActionService() {
		return actionService;
	}
	public void setActionService(ActionService actionService) {
		this.actionService = actionService;
	}
}