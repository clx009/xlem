package com.xlem.security;

import java.util.ArrayList;
import java.util.Collection;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.xlem.dao.Role;
import com.xlem.service.RoleService;
import com.xlem.service.UserService;

@Service("UserDetailService")
public class UserDetailService implements UserDetailsService  {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(UserDetailService.class);
	@Autowired
	UserService userService;
	@Autowired
	RoleService roleService;
	 	
	public UserDetails loadUserByUsername(String username)throws UsernameNotFoundException, DataAccessException {
		if (logger.isDebugEnabled()) {
			logger.debug("loadUserByUsername(String) - start"); //$NON-NLS-1$
		}
			
		Collection<GrantedAuthority> auths=new ArrayList<GrantedAuthority>();
			
		String password=null;
		//取得用户的密码
		com.xlem.dao.User tUser = userService.findUserByName(username);
		if (tUser ==null){
			String message = "用户"+username+"不存在";
			logger.error(message);
			throw new UsernameNotFoundException(message);
		}
		password=tUser.getPwd();
			
		//获得用户的角色
		Role tRole = tUser.getRole();
		SimpleGrantedAuthority grantedAuthority = new SimpleGrantedAuthority(tRole.getName());
		if (logger.isDebugEnabled()){
			logger.debug("用户：["+tUser.getName()+"]拥有角色：["+tRole.getName()+"],即spring security中的access");
		}
		auths.add(grantedAuthority);
			
	    User user = new User(username,password, true, true, true, true, auths);
	        
		if (logger.isDebugEnabled()) {
			logger.debug("loadUserByUsername(String) - end"); //$NON-NLS-1$
		}
	    
		return user;
	}
}
