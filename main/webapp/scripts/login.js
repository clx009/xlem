	 jQuery(document).ready(function($) {
		var id = document.getElementsByName("login_mode");
	    id[0].checked = true;
	 });
	 
	jQuery(document).on('change', "#login_mode", function() {
		var id = document.getElementsByName("login_mode");
	    for(var i=0;i<id.length; i++){
	        var radio=id[i];
	        if (radio.checked) {
	        	switch (i) {
	        	case 0:
	        		$("#login_name_tag").html("登录名");
	        		$("#forget_passwd").toggle();
	        		$("#dynamic_login").toggle();
	        		
	        		break;
	
	        	case 1:
	        		$("#login_name_tag").html("手机号");
	        		$("#forget_passwd").toggle();
	        		$("#dynamic_login").toggle();
	        		
	        		break;
	        	
	        	default:
	        		alert("error.");
	        		break;
	        	}
	        }
	    }
	});
	
	$(document).on('click', "#dynamic", function() {
        $.ajax({
        	url: "/xlem/message/randomCode",
        	data: 'limit=100',
        	type: 'get',
        	dataType: 'json',
        	async: false,
        }).done(function(data, status, xhr) {
        	var code = data.code;
        	var message = '西岭雪山电商平台注册码：';
        	message += code;
        	
        	var phone = document.getElementById("login_name").value;
            var datas = "phone=" + phone;  
            datas += "&&content=" + message;  	
			$.ajax({  
				url: "/xlem/message/sendSMS",
				data:'limit=5000',
				type:'post',
				dataType:'json',
	            async : false,  
	            data : datas,
			}).done(function(data, status, xhr) {
				if (data.msg == "frequent") {
					alert("短信发送过于频繁，请稍候再试");
					return;
				}
			}).fail(function(xhr, status, error) {  
	        	alert("请求失败！");
			});
		}).fail(function(xhr, status, error) {  
	        	alert("请求失败！");
		});
	});

	$(document).on('click', "#btnGuestLogin", function() {
		var mode = 0;
		var modes = document.getElementsByName("login_mode");
		if (modes[0].checked) {
			mode = 0;
		}
		else if (modes[1].checked) {
			mode = 1;
		} 
			
		if (mode == 0) {
			var loginName = document.getElementById("login_name").value;
			var passwd = document.getElementById("login_passwd").value;
			var datas = "login_name=" + loginName;
			datas += "&&passwd=" + passwd;
			$.ajax({  
				url: "/xlem/login/guestName",
				data:'limit=5000',
				type:'post',
				dataType:'json',
				data: datas,
	            async : false,  
			}).done(function(data, status, xhr) {
				window.location = data.url;
			}).fail(function(xhr, status, error) {  
	        	alert("请求失败！");
			});
		}
		else {
			var phone = document.getElementById("login_name").value;
			var sms = document.getElementById("login_passwd").value;
			var datas = 'login_name=' + phone;
			datas += "&&sms_code=" + sms;
			$.ajax({  
				url: "/xlem/login/guestSMS",
				data:'limit=5000',
				type:'post',
				dataType:'json',
				data:datas,
	            async : false,  
			}).done(function(data, status, xhr) {
				window.location = convSpecialChar(data.url);
			}).fail(function(xhr, status, error) {  
	        	alert("请求失败！");
			});		
		}
	});
	
	$(document).on('click', "#btnManageLogin", function() {
		var loginName = document.getElementById("manage_login_name").value;
		var passwd = document.getElementById("manage_login_passwd").value;
		var datas = "loginName=" + loginName;
		datas += "&&passwd=" + passwd;

		$.ajax({  
			url: "/xlem/login/manage_login",
			data:'limit=5000',
			type:'post',
			dataType:'json',
	        async:false,
	        data:datas,
		}).done(function(data, status, xhr) {
			if (data.msg == "invalid") {
				alert("用户不存在");
				return;
			}
			
			if (data.msg == "notmatch") {
				alert("密码不正确");
				return;
			}
			
			if (data.msg == "notadmin") {
				alert("登录用户非管理员");
				return;
			}
			
			window.location = convSpecialChar(data.url);
		}).fail(function(xhr, status, error) {  
	        alert("请求失败！");
		});
	});		
	
	$(document).on('click', "#btnMarketLogin", function() {
		var loginName = document.getElementById("market_login_name").value;
		var passwd = document.getElementById("market_login_passwd").value;
		var datas = "loginName=" + loginName;
		datas += "&&passwd=" + passwd;

		$.ajax({  
			url: "/xlem/login/market_login",
			data:'limit=5000',
			type:'post',
			dataType:'json',
	        async:false,
	        data:datas,
		}).done(function(data, status, xhr) {
			if (data.msg == "invalid") {
				alert("用户不存在");
				return;
			}
			
			if (data.msg == "notmatch") {
				alert("密码不正确");
				return;
			}
			
			if (data.msg == "wrongdepart") {
				alert("用户非市场部工作人员");
				return;
			}
			
			window.location = convSpecialChar(data.url);
		}).fail(function(xhr, status, error) {  
	        alert("请求失败！");
		});
	});	
	
	$(document).on('click', "#btnFinanceLogin", function() {
		var loginName = document.getElementById("finance_login_name").value;
		var passwd = document.getElementById("finance_login_passwd").value;
		var datas = "loginName=" + loginName;
		datas += "&&passwd=" + passwd;

		$.ajax({  
			url: "/xlem/login/finance_login",
			data:'limit=5000',
			type:'post',
			dataType:'json',
	        async:false,
	        data:datas,
		}).done(function(data, status, xhr) {
			if (data.msg == "invalid") {
				alert("用户不存在");
				return;
			}
			
			if (data.msg == "notmatch") {
				alert("密码不正确");
				return;
			}
			
			window.location = convSpecialChar(data.url);
		}).fail(function(xhr, status, error) {  
	        alert("请求失败！");
		});
	});		
	
	$(document).on('click', "#btnTsLogin", function() {
		var loginName = document.getElementById("ts_login_name").value;
		var passwd = document.getElementById("ts_login_passwd").value;
		var datas = "loginName=" + loginName;
		datas += "&&passwd=" + passwd;

		$.ajax({  
			url: "/xlem/login/travservice_login",
			data:'limit=5000',
			type:'post',
			dataType:'json',
	        async:false,
	        data:datas,
		}).done(function(data, status, xhr) {
			if (data.msg == "invalid") {
				alert("用户不存在");
				return;
			}
			
			if (data.msg == "notmatch") {
				alert("密码不正确");
				return;
			}
			
			window.location = convSpecialChar(data.url);
		}).fail(function(xhr, status, error) {  
	        alert("请求失败！");
		});
	});	
	
	$(document).on('click', "#btnLogin", function() {
		var loginName = document.getElementById("login_name").value;
		var passwd = document.getElementById("login_passwd").value;
		var datas = "loginName=" + loginName;
		datas += "&&passwd=" + passwd;

		$.ajax({  
			url: "/xlem/login/general_login",
			data:'limit=5000',
			type:'post',
			dataType:'json',
	        async:false,
	        data:datas,
		}).done(function(data, status, xhr) {
			if (data.msg == "invalid") {
				alert("该用户不存在");
				return;
			}
			if (data.msg == "notmatch") {
				alert("密码错误");
				return;
			}
			window.location = convSpecialChar(data.url);
		}).fail(function(xhr, status, error) {  
	        alert("请求失败！");
		});
	});	
	
	$(document).on('click', "#btnLoginMyAccount", function() {
		var loginName = document.getElementById("login_name").value;
		var passwd = document.getElementById("login_passwd").value;
		var datas = "loginName=" + loginName;
		datas += "&&passwd=" + passwd;

		$.ajax({  
			url: "/xlem/login/myaccount_login",
			data:'limit=5000',
			type:'post',
			dataType:'json',
	        async:false,
	        data:datas,
		}).done(function(data, status, xhr) {
			if (data.msg == "invalid") {
				alert("该用户不存在");
				return;
			}
			if (data.msg == "notmatch") {
				alert("密码错误");
				return;
			}
			window.location = convSpecialChar(data.url);
		}).fail(function(xhr, status, error) {  
	        alert("请求失败！");
		});
	});	
	
	function convSpecialChar(url) {
		var pos = url.search("name=");
		if (pos == -1) {
			return url;
		}
		
		pos += 5;
		var name = url.slice(pos);
		name = name.replace(/\+/g, '%2B').replace(/\"/g,'%22').replace(/\'/g, '%27').replace(/\//g,'%2F');
		
		return url.slice(0, pos) + name;
	}
	
	$(document).on('click', '#btnSendVCode', function() {
		var mphone = document.getElementById("user_mphone").value;
		var datas = "mphone=" + mphone;

		$.ajax({  
			url: "/xlem/login/forgetpwd_sms",
			data:'limit=5000',
			type:'post',
			dataType:'json',
	        async:false,
	        data:datas,
		}).done(function(data, status, xhr) {
			if (data.msg == "success") {
				alert("验证码已发送");
			}
		}).fail(function(xhr, status, error) {  
	        alert("请求失败！");
		});
	})
	
	$(document).on('click', '#btnResetPwd', function() {
		var user_name = document.getElementById("login_name").value;
		var mphone = document.getElementById("user_mphone").value;
		var vcode = document.getElementById("vcode").value;
		
		var datas = "user_name=" + user_name;
		datas += "&&mphone=" + mphone;
		datas += "&&vcode=" + vcode;
		
		$.ajax({  
			url: "/xlem/login/forgetpwd_verify",
			data:'limit=5000',
			type:'post',
			dataType:'json',
	        async:false,
	        data:datas,
		}).done(function(data, status, xhr) {
			if (data.msg == "success") {
				window.location = "/xlem/login/resetpwd";
			}
			
			if (data.msg == "ngverify") {
				alert("验证码错误");
			}
			
			if (data.msg == "wronginfo") {
				alert("错误的手机号");
			}
			
			if (data.msg == "notexist") {
				alert("该用户不存在");
			}
		}).fail(function(xhr, status, error) {  
	        alert("请求失败！");
		});		
	})
	
	$(document).on('click', '#btnNewPwd', function () {
		var new_passwd = document.getElementById("new_passwd").value;
		var renew_passwd = document.getElementById("renew_passwd").value;
		
		if (new_passwd === renew_passwd) {
			var datas = "new_passwd=" + new_passwd;
			$.ajax({  
				url: "/xlem/login/performResetpwd",
				data:'limit=5000',
				type:'get',
				dataType:'json',
		        async:false,
		        data:datas,
			}).done(function(data, status, xhr) {
				if (data.msg == "success") {
					window.location = "/xlem/login/generalForm";
				}

				if (data.msg == "invalid") {
					alert("未知用户");
				}
			}).fail(function(xhr, status, error) {  
		        alert("请求失败！");
			});					
		}
	})
	
	$(document).on('change', "#depart-dropdown", function () {
		$('input[name="departId"]').val(this.value);
	});
	
	$(document).on('click', "#btnDepartLogin", function () {
		var departId = document.getElementById("departId").value;
		var datas = "departId=" + departId;
		$.ajax({  
			url: "/xlem/login/departSelect",
			data:'limit=5000',
			type:'get',
			dataType:'json',
		    async:false,
		    data:datas,
		}).done(function(data, status, xhr) {
			if (data.msg == "success") {
				window.location = data.url;
			}

			if (data.msg == "invalid") {
				alert("未知部门");
				return;
			}
		}).fail(function(xhr, status, error) {  
			alert("请求失败！");
		});					
	})