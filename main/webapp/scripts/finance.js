	$(document).ready(function() {
		if (document.getElementById("transRecDiv") != null) {
			$('input[name="indentState"]').val(document.getElementById("indentStateList").value);
			$('input[name="keyword"]').val(document.getElementById("keywordList").value);
			$('input[name="timeType"]').val(document.getElementById("timeTypeList").value);
			$('input[name="cashDir"]').val(document.getElementById("cashDirList").value);
			$('input[name="tradeClass"]').val(document.getElementById("tradeTypeList").value);
			initTransRecData(0, "/xlem/finance/getTransRecData");
			initTransRecPager("/xlem/finance/getTransRecData");
		}
		
		$('.datepicker').each(function(){
		    var datepicker = $(this).datepicker();
		    if (this.value == null) {
		    	datepicker.datepicker('option', {dateFormat: 'yy-m-d'});
		    	if (datepicker.context.id == "startDate") {
		    		datepicker.datepicker('setDate', new Date("2016-1-1"));
		    	}
		    	if (datepicker.context.id == "endDate") {
		    		datepicker.datepicker('setDate', new Date(this.value));
		    	}
		    	if (datepicker.context.id == "daySelected") {
		    		datepicker.datepicker('setDate', new Date());
		    	}
		    }
		    else {
		    	datepicker.datepicker('option', {dateFormat: 'yy-m-d'});
		    	if (datepicker.context.id == "startDate") {
		    		datepicker.datepicker('setDate', new Date("2016-1-1"));
		    	}
		    	if (datepicker.context.id == "endDate") {
		    		datepicker.datepicker('setDate', new Date(this.value));
		    	}
		    	if (datepicker.context.id == "daySelected") {
		    		datepicker.datepicker('setDate', new Date(this.value));
		    	}		    	
		    }
		});
	})
		    
	function initTransRecData(pageindx, url) {  
		var tbody = "";  
		var pageCount = "";  
		var datas = "offset=" + (pageindx);
		
		var timeType = document.getElementById("timeType").value;
		var timeData = "timeType=" + timeType;
		
		$.ajax({  
			url: "/xlem/finance/getQueryTimeType",
			data:'limit=5000',
			type:'get',
			contentType:'application/json;charset=UTF-8',
			dataType:'json',
		    async : false,  
		    data : timeData,
		}).done(function(data, status, xhr) {
			if (data.msg == "invalid") {
				alert("服务器发生错误");
				return;
			}
			
			var timeTypeName = data.timeTypeName;
			
			datas += "&&timeType=" + document.getElementById("timeType").value;
			
			if (document.getElementById("startDate").value != "") {
				datas += "&&startDate=" + document.getElementById("startDate").value;
			}
			else {
				datas += "&&startDate=2016-1-1";
			}
			
			if (document.getElementById("endDate").value != "") {
				datas += "&&endDate=" + document.getElementById("endDate").value;
			}
			else {
				var now = new Date();
				datas += "&&endDate=" + now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate();
			}
			
			datas += "&&state=" + document.getElementById("indentState").value; 
			datas += "&&keyword=" + document.getElementById("keyword").value;
			
			if (document.getElementById("keyValue").value != "") {
				datas += "&&keyValue=" + document.getElementById("keyValue").value;
			}
			else {
				datas += "&&keyValue=empty";
			}
			
			if (document.getElementById("lessMoney").value != "") {
				datas += "&&lessMoney=" + document.getElementById("lessMoney").value;
			}
			else {
				datas += "&&lessMoney=minus-infinite"; 
			}
			
			if (document.getElementById("moreMoney").value != "") {
				datas += "&&moreMoney=" + document.getElementById("moreMoney").value;
			}
			else {
				datas += "&&moreMoney=infinite";
			}
			
			datas += "&&cashDir=" + document.getElementById("cashDir").value;
			
			if (document.getElementById("alipay").checked) {
				datas += "&&alipay=true";
			}
			else {
				datas += "&&alipay=false";
			}
			
			if (document.getElementById("other").checked) {
				datas += "&&other=true";
			}
			else {
				datas += "&&other=false";
			}
			
			datas += "&&tradeClass=" + document.getElementById("tradeClass").value;
		
			$.ajax({  
				url: url,
				data:'limit=5000',
				type:'get',
				dataType:'json',
				async : false,  
				data : datas,
			}).done(function(data, status, xhr) {
				resHtml = "";
				var entryCount = 0;
				resHtml = '<table id="transRecTable" style="width:100%">';
				resHtml += '<tr>';
				resHtml += '<th>';
				resHtml += timeTypeName;
				resHtml += '</th>';
				resHtml += '<th>名称</th>';
				resHtml += '<th>订单号</th>';
				resHtml += '<th>交易号</th>';
				resHtml += '<th>对方</th>';
				resHtml += '<th>订单来源</th>';
				resHtml += '<th>金额</th>';
				resHtml += '<th>明细</th>';
				resHtml += '<th>状态</th>';
				resHtml += '</tr>';
					
				for (var i = 0; i < data.transRec.length; i++) {
					if (entryCount < 10) {
						resHtml += '<tr>';
						resHtml += '<td>';
						var date = new Date(data.transRec[i].date);
						resHtml += date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
						resHtml += '</td>';
						resHtml += '<td style="width:10%">';
						resHtml += data.transRec[i].name;
						resHtml += '</td>';
						resHtml += '<td style="width:10%">';
						resHtml += data.transRec[i].indentId;
						resHtml += '</td>';
						resHtml += '<td style="width:10%">';
						resHtml += data.transRec[i].transRecId;
						resHtml += '</td>';
						resHtml += '<td>';
						resHtml += data.transRec[i].otherSide;
						resHtml += '</td>';
						resHtml += '<td>';
						resHtml += data.transRec[i].indentSource;
						resHtml += '</td>';
						resHtml += '<td>';
						resHtml += data.transRec[i].money;
						resHtml += '</td>';
						resHtml += '<td>';
						resHtml += data.transRec[i].moneyDetail;
						resHtml += '</td>';
						resHtml += '<td name="transRecStatus" style="width:8%">';
						resHtml += data.transRec[i].status;
						resHtml += '</td>';
						resHtml += '</tr>';
								
						entryCount++;
					}
				}
				resHtml += '</table>';
					
				resHtml += '<div id="Pager" style="text-align:left">';
				resHtml += '</div>';

				document.getElementById("transRecDiv").innerHTML = resHtml;
				document.getElementById("total").value = data.num;
				document.getElementById("offset").value = pageindx;
		            
				initTransRecPager("/xlem/finance/getTransRecData");
				// ticketIndentStatus();
			}).fail(function(xhr, status, error) {  
				alert("请求失败！");
			});
		}).fail(function(xhr, status, error) {  
			alert("请求失败！");
		})
	}
		    
	function updateTransRecPager(offset, total) {
		if (offset < 0) {
			alert("已经是第一页");
			return;
		}
		    	
		if (offset >= total) {
			alert("已经是最后一页");
		    return;
		}
		    	
		initTransRecData(offset, "/xlem/finance/getTransRecData");
	}
		    
	function initTransRecPager(url) {
		var total = document.getElementById("total").value;
		var offset = document.getElementById("offset").value;
		    	
		var pagerHtml = "";
		pagerHtml += '<nav aria-label="Page navigation">';
		pagerHtml += '<ul class="pagination">';
		pagerHtml += '<li>';
		pagerHtml += '<a href="javascript:updateTransRecPager(';
		pagerHtml += offset - 10;
		pagerHtml += ',';
		pagerHtml += total;
		pagerHtml += ');" ';
		pagerHtml += 'aria-label="Previous">';
		pagerHtml += '<span aria-hidden="true">&#171;</span>';
		pagerHtml += '</a>';
		pagerHtml += '</li>';
		        
		for (i = 0; i < Math.ceil(total / 10); i++) {
			pagerHtml += '<li><a href="javascript:updateTransRecPager(';
		    pagerHtml += i * 10;
		    pagerHtml += ',';
		    pagerHtml += total;
		    pagerHtml += ');" >';
		    pagerHtml += i + 1;
		    pagerHtml += '</a></li>';
		}
		        
		pagerHtml += '<li>';
		pagerHtml += '<a href="javascript:updateTransRecPager(';
		pagerHtml += parseInt(offset) + 10;
		pagerHtml += ',';
		pagerHtml += total;
		pagerHtml += ');" ';
		pagerHtml += 'aria-label="Next">';
		pagerHtml += '<span aria-hidden="true">&#187;</span>';
		pagerHtml += '</a>';
		pagerHtml += '</li>';
		pagerHtml += '</ul>';
		pagerHtml += '</nav>';
		        
		document.getElementById("Pager").innerHTML = pagerHtml;
	}
	
    $(document).on('change', "#indentStateList", function () {
    	$('input[name="indentState"]').val(this.value);
    });	
    
    function summaryBills() {
		var datas = "timeType=" + document.getElementById("timeType").value;
		
		if (document.getElementById("startDate").value != "") {
			datas += "&startDate=" + document.getElementById("startDate").value;
		}
		else {
			datas += "&startDate=2016-1-1";
		}
		
		if (document.getElementById("endDate").value != "") {
			datas += "&endDate=" + document.getElementById("endDate").value;
		}
		else {
			var now = new Date();
			datas += "&endDate=" + now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate();
		}
		
		datas += "&state=" + document.getElementById("indentState").value; 
		datas += "&keyword=" + document.getElementById("keyword").value;
		
		if (document.getElementById("keyValue").value != "") {
			datas += "&keyValue=" + document.getElementById("keyValue").value;
		}
		else {
			datas += "&keyValue=empty";
		}
		
		if (document.getElementById("lessMoney").value != "") {
			datas += "&lessMoney=" + document.getElementById("lessMoney").value;
		}
		else {
			datas += "&lessMoney=minus-infinite"; 
		}
		
		if (document.getElementById("moreMoney").value != "") {
			datas += "&moreMoney=" + document.getElementById("moreMoney").value;
		}
		else {
			datas += "&moreMoney=infinite";
		}
		
		datas += "&cashDir=" + document.getElementById("cashDir").value;
		
		if (document.getElementById("alipay").checked) {
			datas += "&alipay=true";
		}
		else {
			datas += "&alipay=false";
		}
		
		if (document.getElementById("other").checked) {
			datas += "&other=true";
		}
		else {
			datas += "&other=false";
		}
		
		datas += "&tradeClass=" + document.getElementById("tradeClass").value;

		var url = "/xlem/finance/summaryOutBill?" + datas;
		window.location = url;
    }
    
    function downloadBills() {
		var datas = "timeType=" + document.getElementById("timeType").value;
		
		if (document.getElementById("startDate").value != "") {
			datas += "&startDate=" + document.getElementById("startDate").value;
		}
		else {
			datas += "&startDate=2016-1-1";
		}
		
		if (document.getElementById("endDate").value != "") {
			datas += "&endDate=" + document.getElementById("endDate").value;
		}
		else {
			var now = new Date();
			datas += "&endDate=" + now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate();
		}
		
		datas += "&state=" + document.getElementById("indentState").value; 
		datas += "&keyword=" + document.getElementById("keyword").value;
		
		if (document.getElementById("keyValue").value != "") {
			datas += "&keyValue=" + document.getElementById("keyValue").value;
		}
		else {
			datas += "&keyValue=empty";
		}
		
		if (document.getElementById("lessMoney").value != "") {
			datas += "&lessMoney=" + document.getElementById("lessMoney").value;
		}
		else {
			datas += "&lessMoney=minus-infinite"; 
		}
		
		if (document.getElementById("moreMoney").value != "") {
			datas += "&moreMoney=" + document.getElementById("moreMoney").value;
		}
		else {
			datas += "&moreMoney=infinite";
		}
		
		datas += "&cashDir=" + document.getElementById("cashDir").value;
		
		if (document.getElementById("alipay").checked) {
			datas += "&alipay=true";
		}
		else {
			datas += "&alipay=false";
		}
		
		if (document.getElementById("other").checked) {
			datas += "&other=true";
		}
		else {
			datas += "&other=false";
		}
		
		datas += "&tradeClass=" + document.getElementById("tradeClass").value;

		var url = "/xlem/outBillExcelGen?" + datas;
		window.location = url;    	
    }
    
	$(document).ready(function() {
		if (document.getElementById("inBillDiv") != null) {
			var id = document.getElementsByName("query_mode");
			id[0].checked = true;
			$("#indentQueryDiv").toggle();
			initInBillData(0, "/xlem/finance/getInBillData", "yes");
			initInBillPager("/xlem/finance/getInBillData");
		}
		
		$('.datepicker').each(function(){
		    var datepicker = $(this).datepicker();
		    if (this.value == null) {
		    	datepicker.datepicker('option', {dateFormat: 'yy-m-d'});
		    	if (datepicker.context.id == "startDate") {
		    		datepicker.datepicker('setDate', new Date("2016-1-1"));
		    	}
		    	if (datepicker.context.id == "endDate") {
		    		datepicker.datepicker('setDate', new Date(this.value));
		    	}
		    }
		    else {
		    	datepicker.datepicker('option', {dateFormat: 'yy-m-d'});
		    	if (datepicker.context.id == "startDate") {
		    		datepicker.datepicker('setDate', new Date("2016-1-1"));
		    	}
		    	if (datepicker.context.id == "endDate") {
		    		datepicker.datepicker('setDate', new Date(this.value));
		    	}
		    }
		});
	})
		    
	function initInBillData(pageindx, url, init) {  
		var tbody = "";  
		var pageCount = "";  
		var datas = "offset=" + (pageindx);
		
		if (init == "yes") {
			var id = document.getElementsByName("query_mode");
		    for(var i=0;i<id.length; i++){
		        var radio=id[i];
		        if (radio.checked) {
		        	switch (i) {
		        	case 0:
		        		datas += "&&queryMode=time";
		        		datas += "&&keyValue=empty";
		        		break;
		
		        	case 1:
		        		datas += "&&queryMode=id";
		        		datas += "&&keyValue=" + document.getElementById("indentId");
		        		break;
		        	
		        	default:
		        		alert("error.");
		        		break;
		        	}
		        }
		    }		
			
			if (document.getElementById("startDate").value != "") {
				datas += "&&startDate=" + document.getElementById("startDate").value;
			}
			else {
				datas += "&&startDate=2016-1-1";
			}
				
			if (document.getElementById("endDate").value != "") {
				datas += "&&endDate=" + document.getElementById("endDate").value;
			}
			else {
				var now = new Date();
				datas += "&&endDate=" + now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate();
			}
				
			if (document.getElementById("onlyAdjust").checked) {
				datas += "&&onlyAdjust=true";
			}
			else {
				datas += "&&onlyAdjust=false";
			}
		}
			
		$.ajax({  
			url: url,
			data:'limit=5000',
			type:'get',
			dataType:'json',
			async : false,  
			data : datas,
		}).done(function(data, status, xhr) {
			resHtml = "";
			var entryCount = 0;
			resHtml = '<table id="inBillTable" style="width:1350px">';
			resHtml += '<tr>';
			resHtml += '<th>序号</th>';
			resHtml += '<th>订单号</th>';
			resHtml += '<th>订单来源</th>';
			resHtml += '<th>支付时间</th>';
			resHtml += '<th>支付金额</th>';
			resHtml += '<th>分账金额</th>';
			resHtml += '<th>分账回退金额</th>';
			resHtml += '<th>退款金额</th>';
			resHtml += '<th>调账金额</th>';
			resHtml += '<th>订单剩余金额</th>';
			resHtml += '<th>操作</th>';
			resHtml += '</tr>';
					
			for (var i = 0; i < data.transRec.length; i++) {
				if (entryCount < 10) {
					resHtml += '<tr>';
					resHtml += '<td style="width:50px">';
					resHtml += i + 1;
					resHtml += '</td>';
					resHtml += '<td style="width:225px">';
					resHtml += data.transRec[i].indentId;
					resHtml += '</td>';
					resHtml += '<td style="width:90px">';
					resHtml += data.transRec[i].indentSource;
					resHtml += '</td>';
					resHtml += '<td style="width:90px">';
					var date = new Date(data.transRec[i].payTime);
					resHtml += date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
					resHtml += '</td>';
					resHtml += '<td style="width:90px">';
					resHtml += data.transRec[i].payMoney;
					resHtml += '</td>';
					resHtml += '<td style="width:90px">';
					resHtml += data.transRec[i].departMoney;
					resHtml += '</td>';
					resHtml += '<td style="width:90px">';
					resHtml += data.transRec[i].departRefundMoney;
					resHtml += '</td>';
					resHtml += '<td style="width:90px">';
					resHtml += data.transRec[i].refundMoney;
					resHtml += '</td>';
					resHtml += '<td style="width:90px">';
					resHtml += data.transRec[i].adjustMoney;
					resHtml += '</td>';
					resHtml += '<td style="width:90px">';
					resHtml += data.transRec[i].remainMoney;
					resHtml += '</td>';
					resHtml += '<td>';
					resHtml += '</td>';
					resHtml += '</tr>';
								
					entryCount++;
				}
			}
			
			resHtml += '</table>';
					
			resHtml += '<div id="Pager" style="text-align:left">';
			resHtml += '</div>';

			document.getElementById("inBillDiv").innerHTML = resHtml;
			document.getElementById("total").value = data.num;
			document.getElementById("offset").value = pageindx;
			
			var detailTable = document.getElementById("inBillTable");
			var trs = detailTable.getElementsByTagName("tr");
			
			// Iterate over the links
			for (var i = 1, len = trs.length, tr; i < len; ++i) {
			    tr = trs[i];
			    var tds = tr.getElementsByTagName("td");
			    var seq = parseInt(tds[0].innerText);
			    seq += pageindx;
			    tds[0].innerText = seq;
			}
		            
			initInBillPager("/xlem/finance/getInBillData");
			// ticketIndentStatus();
		}).fail(function(xhr, status, error) {  
			alert("请求失败！");
		})
	}
		    
	function updateInBillPager(offset, total) {
		if (offset < 0) {
			alert("已经是第一页");
			return;
		}
		    	
		if (offset >= total) {
			alert("已经是最后一页");
		    return;
		}
		    	
		initInBillData(offset, "/xlem/finance/getInBillPage", "no");
	}
		    
	function initInBillPager(url) {
		var total = document.getElementById("total").value;
		var offset = document.getElementById("offset").value;
		    	
		var pagerHtml = "";
		pagerHtml += '<nav aria-label="Page navigation">';
		pagerHtml += '<ul class="pagination">';
		pagerHtml += '<li>';
		pagerHtml += '<a href="javascript:updateInBillPager(';
		pagerHtml += offset - 10;
		pagerHtml += ',';
		pagerHtml += total;
		pagerHtml += ');" ';
		pagerHtml += 'aria-label="Previous">';
		pagerHtml += '<span aria-hidden="true">&#171;</span>';
		pagerHtml += '</a>';
		pagerHtml += '</li>';
		        
		for (i = 0; i < Math.ceil(total / 10); i++) {
			pagerHtml += '<li><a href="javascript:updateInBillPager(';
		    pagerHtml += i * 10;
		    pagerHtml += ',';
		    pagerHtml += total;
		    pagerHtml += ');" >';
		    pagerHtml += i + 1;
		    pagerHtml += '</a></li>';
		}
		        
		pagerHtml += '<li>';
		pagerHtml += '<a href="javascript:updateInBillPager(';
		pagerHtml += parseInt(offset) + 10;
		pagerHtml += ',';
		pagerHtml += total;
		pagerHtml += ');" ';
		pagerHtml += 'aria-label="Next">';
		pagerHtml += '<span aria-hidden="true">&#187;</span>';
		pagerHtml += '</a>';
		pagerHtml += '</li>';
		pagerHtml += '</ul>';
		pagerHtml += '</nav>';
		        
		document.getElementById("Pager").innerHTML = pagerHtml;
	}
	
	jQuery(document).on('change', ".query_mode", function() {
		$("#indentQueryDiv").toggle();
	    $("#timeQueryDiv").toggle();
	})
	
	$(document).ready(function() {
		if (document.getElementById("indentStatistic") != null) {
			var datepicker = document.getElementById("startDate");
		    datepicker.datepicker('option', {dateFormat: 'yy-m-d'});
		    datepicker.datepicker('setDate', new Date("2016-1-1"));
		    datepicker = document.getElementById("endDate");
		    datepicker.datepicker('option', {dateFormat: 'yy-m-d'});
		    datepicker.datepicker('setDate', new Date(this.value));
		}
	})
	
	jQuery(document).on('click', "#statistic", function() {
		var startDate = document.getElementById("startDate").value;
		var endDate = document.getElementById("endDate").value;
		
		var datas = "startDate=" + startDate;
		datas += "&&endDate=" + endDate;
		
		$.ajax({  
			url: "/xlem/finance/getIndentStatistic",
			data:'limit=5000',
			type:'get',
			dataType:'json',
			async : false,  
			data : datas,
		}).done(function(data, status, xhr) {
			var statTable = document.getElementById("indentStatTable");
			var trs = statTable.getElementsByTagName("tr");
			var tr = trs[0];
			var tds = tr.getElementsByTagName("td");
			tds[1].innerHtml = data.totalOrder;
			tds[3].innerHtml = data.totalMoney;
			tr = trs[1];
			tds = tr.getElementsByTagName("td");
			tds[1].innerHtml = data.successOrder;
			tds[3].innerHtml = data.successMoney;
		}).fail(function(xhr, status, error) {  
			alert("请求失败！");
			var statTable = document.getElementyById("indentStatTable");
			var trs = indentTable.getElementsByTagName("tr");
			var tr = trs[0];
			var tds = tr.getElementsByTagName("td");
			tds[1].innerHtml = "0";
			tds[3].innerHtml = "0.00";
			tr = trs[1];
			tds = tr.getElementByTagName("td");
			tds[1].innerHtml = "0";
			tds[1].innerHtml = "0.00";
		})
	})
	
	$(document).ready(function() {
		var now = new Date();
		var year = now.getFullYear();
		var leap = false;
		if (year % 100 == 0) {
			if (year % 400 == 0) {
				leap = true;
			}
		}
		else {
			if (year % 4 == 0) {
				leap = true;
			}
		}
		
		var maxDay;
		var month = now.getMonth() + 1;
		if (month == 1 || month == 3 || month == 5 || month == 7
			|| month == 8 || month == 10 || month == 12) {
			maxDay = 31;
		}
		else if (month == 2) {
			if (leap) 
				maxDay = 28;
			else
				maxDay = 29;
		}
		else {
			maxDay = 30;
		}
	
		var start = year + "-" + month + "-1";
		var end = year + "-" + month + "-" + maxDay;
		
	    if (document.getElementById("indentMatrix") != null) {
			var resHtml = "<table>";
			resHtml += "<tr>";
			resHtml += '<th>时间段（开始-结束）</th>';
			resHtml += '<th>周一</th>';
			resHtml += '<th>周二</th>';
			resHtml += '<th>周三</th>';
			resHtml += '<th>周四</th>';
			resHtml += '<th>周五</th>';
			resHtml += '<th>周六</th>';
			resHtml += '<th>周日</th>';
			resHtml += '<th>每周账单下载</th>';
			resHtml += '</tr>';
			
			var start = year + "-" + month + "-1";
			var end = year + "-" + month + "-" + maxDay;
			var startDate = new Date(start);
			var endDate = new Date(end);
			originDay = startDate.getDay();
			
			var count = 1;
			for (var i = 0; i < 5; i++) {
				resHtml += '<tr>';
				for (var j = 0; j < 9; j++) {
					switch (j) {
					case 0:
						var timeScale;
						switch (i) {
						case 0:
							timeScale = month + '月1日';
							timeScale += ' - ' + month + '月' + (7 - originDay + 1) + '日';
							resHtml += '<td>';
							resHtml += timeScale;
							resHtml += '</td>';
							break;
							
						case 4:
							timeScale = month + '月' + (21 + (7 - originDay + 1) + 1) + '日';
							timeScale += ' - ' + month + '月' + maxDay + '日';
							resHtml += '<td>';
							resHtml += timeScale;
							resHtml += '</td>';
							break;
							
						default:
							timeScale = month + '月' + ((i - 1) * 7 + (7 - originDay + 1) + 1) + '日';
							timeScale += ' - ' + month + '月' + (i * 7 + (7 - originDay + 1)) + '日';
							resHtml += '<td>';
							resHtml += timeScale;
							resHtml += '</td>';
							break;
						}
						break;
						
					case 8:
						var date0, date1;
						switch (i) {
						case 0:
							date0 = year + '-' + month + '-1';
							date1 = year + '-' + month + '-' + (7 - originDay + 1);
							resHtml += '<td>';
							resHtml += '<a href="javascript:weekIndentDownload(\'';
							resHtml += date0;
							resHtml += ',\'';
							resHtml += date1;
							resHtml += '\',\'all\');">打包下载</a>';
							resHtml += '</td>';
							break;
							
						case 4:
							date0 = year + '-' + month + '-' + (21 + (7 - originDay + 1) + 1);
							date1 = year + '-' + month + '-' + maxDay;
							resHtml += '<td>';
							resHtml += '<a href="javascript:weekIndentDownload(\'';
							resHtml += date0;
							resHtml += ',\'';
							resHtml += date1;
							resHtml += '\',\'all\');">打包下载</a>';
							resHtml += '</td>';
							break;
							
						default:
							date0 = year + '-' + month + '-' + ((i - 1) * 7 + (7 - originDay) + 1);
							date1 = year + '-' + month + '-' + (i * 7 + (7 - originDay));
							resHtml += '<td>';
							resHtml += '<a href="javascript:weekIndentDownload(\'';
							resHtml += date0;
							resHtml += '\',\'';
							resHtml += date1;
							resHtml += '\',\'all\');">打包下载</a>';
							resHtml += '</td>';
							break;
						}
						break;
					
					default:
						if (i * 7 + j >= originDay && count <= maxDay) {
							resHtml += '<td>';
							resHtml += '<a href="javascript:dayIndentDownload(\'';
							resHtml += year + '-' + month + '-' + count;
							resHtml += '\',\'all\');">'
							resHtml += month + '月' + count + '日';
							resHtml += '</a></td>';
							count++;
						}
						else {
							resHtml += '<td></td>';
						}
					}
				}
				resHtml += '</tr>';
			}
			resHtml += '</table>';
			resHtml += '<input type="hidden" id="indentType" value="all"></input>';
			resHtml += '<span>自定义日期：</span>';
			resHtml += '<input  class="datepicker" id="startDate" type="text" />';
			resHtml += '<span>&#160;&#160;&#160;&#160;</span>'
			resHtml += '<span>至：</span>';
			resHtml += '<input  class="datepicker" id="endDate" type="text" />';
			resHtml += '<button id="packDownload" type="button">打包下载</button>';
				
			document.getElementById("indentMatrix").innerHTML = resHtml;
			
			$('.datepicker').each(function(){
			    var datepicker = $(this).datepicker();
			    if (this.value == null) {
			    	datepicker.datepicker('option', {dateFormat: 'yy-m-d'});
			    	if (datepicker.context.id == "startDate") {
			    		datepicker.datepicker('setDate', new Date(start));
			    	}
			    	if (datepicker.context.id == "endDate") {
			    		datepicker.datepicker('setDate', new Date(end));
			    	}
			    }
			    else {
			    	datepicker.datepicker('option', {dateFormat: 'yy-m-d'});
			    	if (datepicker.context.id == "startDate") {
			    		datepicker.datepicker('setDate', new Date(start));
			    	}
			    	if (datepicker.context.id == "endDate") {
			    		datepicker.datepicker('setDate', new Date(end));
			    	}
			    }
			});	
		}
	})
	
	function getAllIndent() {
		var now = new Date();
		var year = now.getFullYear();
		var leap = false;
		if (year % 100 == 0) {
			if (year % 400 == 0) {
				leap = true;
			}
		}
		else {
			if (year % 4 == 0) {
				leap = true;
			}
		}
		
		var maxDay;
		var month = now.getMonth() + 1;
		if (month == 1 || month == 3 || month == 5 || month == 7
			|| month == 8 || month == 10 || month == 12) {
			maxDay = 31;
		}
		else if (month == 2) {
			if (leap) 
				maxDay = 28;
			else
				maxDay = 29;
		}
		else {
			maxDay = 30;
		}
	
		var start = year + "-" + month + "-1";
		var end = year + "-" + month + "-" + maxDay;
		
	    if (document.getElementById("indentMatrix") != null) {
			var resHtml = "<table>";
			resHtml += "<tr>";
			resHtml += '<th>时间段（开始-结束）</th>';
			resHtml += '<th>周一</th>';
			resHtml += '<th>周二</th>';
			resHtml += '<th>周三</th>';
			resHtml += '<th>周四</th>';
			resHtml += '<th>周五</th>';
			resHtml += '<th>周六</th>';
			resHtml += '<th>周日</th>';
			resHtml += '<th>每周账单下载</th>';
			resHtml += '</tr>';
			
			var start = year + "-" + month + "-1";
			var end = year + "-" + month + "-" + maxDay;
			var startDate = new Date(start);
			var endDate = new Date(end);
			originDay = startDate.getDay();
			
			var count = 1;
			for (var i = 0; i < 5; i++) {
				resHtml += '<tr>';
				for (var j = 0; j < 9; j++) {
					switch (j) {
					case 0:
						var timeScale;
						switch (i) {
						case 0:
							timeScale = month + '月1日';
							timeScale += ' - ' + month + '月' + (7 - originDay + 1) + '日';
							resHtml += '<td>';
							resHtml += timeScale;
							resHtml += '</td>';
							break;
							
						case 4:
							timeScale = month + '月' + (21 + (7 - originDay + 1) + 1) + '日';
							timeScale += ' - ' + month + '月' + maxDay + '日';
							resHtml += '<td>';
							resHtml += timeScale;
							resHtml += '</td>';
							break;
							
						default:
							timeScale = month + '月' + ((i - 1) * 7 + (7 - originDay + 1) + 1) + '日';
							timeScale += ' - ' + month + '月' + (i * 7 + (7 - originDay + 1)) + '日';
							resHtml += '<td>';
							resHtml += timeScale;
							resHtml += '</td>';
							break;
						}
						break;
						
					case 8:
						var date0, date1;
						switch (i) {
						case 0:
							date0 = year + '-' + month + '-1';
							date1 = year + '-' + month + '-' + (7 - originDay + 1);
							resHtml += '<td>';
							resHtml += '<a href="javascript:weekIndentDownload("';
							resHtml += date0;
							resHtml += '","';
							resHtml += date1;
							resHtml += '","all");">打包下载</a>';
							resHtml += '</td>';
							break;
							
						case 4:
							date0 = year + '-' + month + '-' + (21 + (7 - originDay + 1) + 1);
							date1 = year + '-' + month + '-' + maxDay;
							resHtml += '<td>';
							resHtml += '<a href="javascript:weekIndentDownload("';
							resHtml += date0;
							resHtml += '","';
							resHtml += date1;
							resHtml += '","all");">打包下载</a>';
							resHtml += '</td>';
							break;
							
						default:
							date0 = year + '-' + month + '-' + ((i - 1) * 7 + (7 - originDay) + 1);
							date1 = year + '-' + month + '-' + (i * 7 + (7 - originDay));
							resHtml += '<td>';
							resHtml += '<a href="javascript:weekIndentDownload("';
							resHtml += date0;
							resHtml += '","';
							resHtml += date1;
							resHtml += '","all");">打包下载</a>';
							resHtml += '</td>';
							break;
						}
						break;
					
					default:
						if (i * 7 + j >= originDay && count <= maxDay) {
							resHtml += '<td>';
							resHtml += '<a href="javascript:dayIndentDownload("';
							resHtml += year + '-' + month + '-' + count;
							resHtml += '","all");">'
							resHtml += month + '月' + count + '日';
							resHtml += '</a></td>';
							count++;
						}
						else {
							resHtml += '<td></td>';
						}
					}
				}
				resHtml += '</tr>';
			}
			resHtml += '</table>';
			resHtml += '<input type="hidden" id="indentType" value="all"></input>';
			resHtml += '<span>自定义日期：</span>';
			resHtml += '<input  class="datepicker" id="startDate" type="text" />';
			resHtml += '<span>&#160;&#160;&#160;&#160;</span>'
			resHtml += '<span>至：</span>';
			resHtml += '<input  class="datepicker" id="endDate" type="text" />';
			resHtml += '<button id="packDownload" type="button">打包下载</button>';
				
			document.getElementById("indentMatrix").innerHTML = resHtml;
			
			$('.datepicker').each(function(){
			    var datepicker = $(this).datepicker();
			    if (this.value == null) {
			    	datepicker.datepicker('option', {dateFormat: 'yy-m-d'});
			    	if (datepicker.context.id == "startDate") {
			    		datepicker.datepicker('setDate', new Date(start));
			    	}
			    	if (datepicker.context.id == "endDate") {
			    		datepicker.datepicker('setDate', new Date(end));
			    	}
			    }
			    else {
			    	datepicker.datepicker('option', {dateFormat: 'yy-m-d'});
			    	if (datepicker.context.id == "startDate") {
			    		datepicker.datepicker('setDate', new Date(start));
			    	}
			    	if (datepicker.context.id == "endDate") {
			    		datepicker.datepicker('setDate', new Date(end));
			    	}
			    }
			});	
		}
	}
	
	function getPayedIndent() {
		var now = new Date();
		var year = now.getFullYear();
		var leap = false;
		if (year % 100 == 0) {
			if (year % 400 == 0) {
				leap = true;
			}
		}
		else {
			if (year % 4 == 0) {
				leap = true;
			}
		}
		
		var maxDay;
		var month = now.getMonth() + 1;
		if (month == 1 || month == 3 || month == 5 || month == 7
			|| month == 8 || month == 10 || month == 12) {
			maxDay = 31;
		}
		else if (month == 2) {
			if (leap) 
				maxDay = 28;
			else
				maxDay = 29;
		}
		else {
			maxDay = 30;
		}
	
		var start = year + "-" + month + "-1";
		var end = year + "-" + month + "-" + maxDay;
		
	    if (document.getElementById("indentMatrix") != null) {
			var resHtml = "<table>";
			resHtml += "<tr>";
			resHtml += '<th>时间段（开始-结束）</th>';
			resHtml += '<th>周一</th>';
			resHtml += '<th>周二</th>';
			resHtml += '<th>周三</th>';
			resHtml += '<th>周四</th>';
			resHtml += '<th>周五</th>';
			resHtml += '<th>周六</th>';
			resHtml += '<th>周日</th>';
			resHtml += '<th>每周账单下载</th>';
			resHtml += '</tr>';
			
			var start = year + "-" + month + "-1";
			var end = year + "-" + month + "-" + maxDay;
			var startDate = new Date(start);
			var endDate = new Date(end);
			originDay = startDate.getDay();
			
			var count = 1;
			for (var i = 0; i < 5; i++) {
				resHtml += '<tr>';
				for (var j = 0; j < 9; j++) {
					switch (j) {
					case 0:
						var timeScale;
						switch (i) {
						case 0:
							timeScale = month + '月1日';
							timeScale += ' - ' + month + '月' + (7 - originDay + 1) + '日';
							resHtml += '<td>';
							resHtml += timeScale;
							resHtml += '</td>';
							break;
							
						case 4:
							timeScale = month + '月' + (21 + (7 - originDay + 1) + 1) + '日';
							timeScale += ' - ' + month + '月' + maxDay + '日';
							resHtml += '<td>';
							resHtml += timeScale;
							resHtml += '</td>';
							break;
							
						default:
							timeScale = month + '月' + ((i - 1) * 7 + (7 - originDay + 1) + 1) + '日';
							timeScale += ' - ' + month + '月' + (i * 7 + (7 - originDay + 1)) + '日';
							resHtml += '<td>';
							resHtml += timeScale;
							resHtml += '</td>';
							break;
						}
						break;
						
					case 8:
						var date0, date1;
						switch (i) {
						case 0:
							date0 = year + '-' + month + '-1';
							date1 = year + '-' + month + '-' + (7 - originDay + 1);
							resHtml += '<td>';
							resHtml += '<a href="javascript:weekIndentDownload("';
							resHtml += date0;
							resHtml += '","';
							resHtml += date1;
							resHtml += '","payed");">打包下载</a>';
							resHtml += '</td>';
							break;
							
						case 4:
							date0 = year + '-' + month + '-' + (21 + (7 - originDay + 1) + 1);
							date1 = year + '-' + month + '-' + maxDay;
							resHtml += '<td>';
							resHtml += '<a href="javascript:weekIndentDownload("';
							resHtml += date0;
							resHtml += '","';
							resHtml += date1;
							resHtml += '","payed");">打包下载</a>';
							resHtml += '</td>';
							break;
							
						default:
							date0 = year + '-' + month + '-' + ((i - 1) * 7 + (7 - originDay) + 1);
							date1 = year + '-' + month + '-' + (i * 7 + (7 - originDay));
							resHtml += '<td>';
							resHtml += '<a href="javascript:weekIndentDownload("';
							resHtml += date0;
							resHtml += '","';
							resHtml += date1;
							resHtml += '","payed");">打包下载</a>';
							resHtml += '</td>';
							break;
						}
						break;
					
					default:
						if (i * 7 + j >= originDay && count <= maxDay) {
							resHtml += '<td>';
							resHtml += '<a href="javascript:dayIndentDownload("';
							resHtml += year + '-' + month + '-' + count;
							resHtml += '","payed");">'
							resHtml += month + '月' + count + '日';
							resHtml += '</a></td>';
							count++;
						}
						else {
							resHtml += '<td></td>';
						}
					}
				}
				resHtml += '</tr>';
			}
			resHtml += '</table>';
			resHtml += '<input type="hidden" id="indentType" value="payed"></input>';
			resHtml += '<span>自定义日期：</span>';
			resHtml += '<input  class="datepicker" id="startDate" type="text" />';
			resHtml += '<span>&#160;&#160;&#160;&#160;</span>'
			resHtml += '<span>至：</span>';
			resHtml += '<input  class="datepicker" id="endDate" type="text" />';
			resHtml += '<button id="packDownload" type="button">打包下载</button>';
				
			document.getElementById("indentMatrix").innerHTML = resHtml;
			
			$('.datepicker').each(function(){
			    var datepicker = $(this).datepicker();
			    if (this.value == null) {
			    	datepicker.datepicker('option', {dateFormat: 'yy-m-d'});
			    	if (datepicker.context.id == "startDate") {
			    		datepicker.datepicker('setDate', new Date(start));
			    	}
			    	if (datepicker.context.id == "endDate") {
			    		datepicker.datepicker('setDate', new Date(end));
			    	}
			    }
			    else {
			    	datepicker.datepicker('option', {dateFormat: 'yy-m-d'});
			    	if (datepicker.context.id == "startDate") {
			    		datepicker.datepicker('setDate', new Date(start));
			    	}
			    	if (datepicker.context.id == "endDate") {
			    		datepicker.datepicker('setDate', new Date(end));
			    	}
			    }
			});	
		}	
	}
	
	function getRefundIndent() {
		var now = new Date();
		var year = now.getFullYear();
		var leap = false;
		if (year % 100 == 0) {
			if (year % 400 == 0) {
				leap = true;
			}
		}
		else {
			if (year % 4 == 0) {
				leap = true;
			}
		}
		
		var maxDay;
		var month = now.getMonth() + 1;
		if (month == 1 || month == 3 || month == 5 || month == 7
			|| month == 8 || month == 10 || month == 12) {
			maxDay = 31;
		}
		else if (month == 2) {
			if (leap) 
				maxDay = 28;
			else
				maxDay = 29;
		}
		else {
			maxDay = 30;
		}
	
		var start = year + "-" + month + "-1";
		var end = year + "-" + month + "-" + maxDay;
		
	    if (document.getElementById("indentMatrix") != null) {
			var resHtml = "<table>";
			resHtml += "<tr>";
			resHtml += '<th>时间段（开始-结束）</th>';
			resHtml += '<th>周一</th>';
			resHtml += '<th>周二</th>';
			resHtml += '<th>周三</th>';
			resHtml += '<th>周四</th>';
			resHtml += '<th>周五</th>';
			resHtml += '<th>周六</th>';
			resHtml += '<th>周日</th>';
			resHtml += '<th>每周账单下载</th>';
			resHtml += '</tr>';
			
			var start = year + "-" + month + "-1";
			var end = year + "-" + month + "-" + maxDay;
			var startDate = new Date(start);
			var endDate = new Date(end);
			originDay = startDate.getDay();
			
			var count = 1;
			for (var i = 0; i < 5; i++) {
				resHtml += '<tr>';
				for (var j = 0; j < 9; j++) {
					switch (j) {
					case 0:
						var timeScale;
						switch (i) {
						case 0:
							timeScale = month + '月1日';
							timeScale += ' - ' + month + '月' + (7 - originDay + 1) + '日';
							resHtml += '<td>';
							resHtml += timeScale;
							resHtml += '</td>';
							break;
							
						case 4:
							timeScale = month + '月' + (21 + (7 - originDay + 1) + 1) + '日';
							timeScale += ' - ' + month + '月' + maxDay + '日';
							resHtml += '<td>';
							resHtml += timeScale;
							resHtml += '</td>';
							break;
							
						default:
							timeScale = month + '月' + ((i - 1) * 7 + (7 - originDay + 1) + 1) + '日';
							timeScale += ' - ' + month + '月' + (i * 7 + (7 - originDay + 1)) + '日';
							resHtml += '<td>';
							resHtml += timeScale;
							resHtml += '</td>';
							break;
						}
						break;
						
					case 8:
						var date0, date1;
						switch (i) {
						case 0:
							date0 = year + '-' + month + '-1';
							date1 = year + '-' + month + '-' + (7 - originDay + 1);
							resHtml += '<td>';
							resHtml += '<a href="javascript:weekIndentDownload("';
							resHtml += date0;
							resHtml += '","';
							resHtml += date1;
							resHtml += '","refund");">打包下载</a>';
							resHtml += '</td>';
							break;
							
						case 4:
							date0 = year + '-' + month + '-' + (21 + (7 - originDay + 1) + 1);
							date1 = year + '-' + month + '-' + maxDay;
							resHtml += '<td>';
							resHtml += '<a href="javascript:weekIndentDownload("';
							resHtml += date0;
							resHtml += '","';
							resHtml += date1;
							resHtml += '","refund");">打包下载</a>';
							resHtml += '</td>';
							break;
							
						default:
							date0 = year + '-' + month + '-' + ((i - 1) * 7 + (7 - originDay) + 1);
							date1 = year + '-' + month + '-' + (i * 7 + (7 - originDay));
							resHtml += '<td>';
							resHtml += '<a href="javascript:weekIndentDownload("';
							resHtml += date0;
							resHtml += '","';
							resHtml += date1;
							resHtml += '","refund");">打包下载</a>';
							resHtml += '</td>';
							break;
						}
						break;
					
					default:
						if (i * 7 + j >= originDay && count <= maxDay) {
							resHtml += '<td>';
							resHtml += '<a href="javascript:dayIndentDownload("';
							resHtml += year + '-' + month + '-' + count;
							resHtml += '","refund");">'
							resHtml += month + '月' + count + '日';
							resHtml += '</a></td>';
							count++;
						}
						else {
							resHtml += '<td></td>';
						}
					}
				}
				resHtml += '</tr>';
			}
			resHtml += '</table>';
			resHtml += '<input type="hidden" id="indentType" value="refund"></input>';
			resHtml += '<span>自定义日期：</span>';
			resHtml += '<input  class="datepicker" id="startDate" type="text" />';
			resHtml += '<span>&#160;&#160;&#160;&#160;</span>'
			resHtml += '<span>至：</span>';
			resHtml += '<input  class="datepicker" id="endDate" type="text" />';
			resHtml += '<button id="packDownload" type="button">打包下载</button>';
				
			document.getElementById("indentMatrix").innerHTML = resHtml;
			
			$('.datepicker').each(function(){
			    var datepicker = $(this).datepicker();
			    if (this.value == null) {
			    	datepicker.datepicker('option', {dateFormat: 'yy-m-d'});
			    	if (datepicker.context.id == "startDate") {
			    		datepicker.datepicker('setDate', new Date(start));
			    	}
			    	if (datepicker.context.id == "endDate") {
			    		datepicker.datepicker('setDate', new Date(end));
			    	}
			    }
			    else {
			    	datepicker.datepicker('option', {dateFormat: 'yy-m-d'});
			    	if (datepicker.context.id == "startDate") {
			    		datepicker.datepicker('setDate', new Date(start));
			    	}
			    	if (datepicker.context.id == "endDate") {
			    		datepicker.datepicker('setDate', new Date(end));
			    	}
			    }
			});	
		}			
	}
	
	$(document).on('click', "#packDownload", function () {
		var start = document.getElementById("startDate").value;
		var end = document.getElementById("endDate").value;
		var type = document.getElementById("indentType").value;
		
		var datas = "?startDate=" + start;
		datas += "&endDate=" + end;
		datas += "&type=" + type;
		
		window.location = "/xlem/indentExcelGen" + datas;
	})
	
	function weekIndentDownload(startDate, endDate, type) {
		var datas = "?startDate=" + startDate;
		datas += "&endDate=" + endDate;
		datas += "&type=" + type;
		
		window.location = "/xlem/indentExcelGen" + datas;		
	}
	
	function dayIndentDownload(startDate, type) {
		var datas = "?startDate=" + startDate;
		datas += "&endDate=" + startDate;
		datas += "&type=" + type;
		
		window.location = "/xlem/indentExcelGen" + datas;		
	}
	
	$(document).ready(function() {
		if (document.getElementById("monthReportSelect") != null) {
			$('#monthReportSelect').modal({ backdrop: 'static', keyboard: false })
        	.one('click', '#monthMakeSure', function() {
        		$('#monthReportSelect').modal('hide');	
        		var month = document.getElementById("monthSelected").value;
        		var datas = "?month=" + month;
        		window.location = "/xlem/finance/monthReport" + datas;
        	});
		}
	})
	
	$(document).ready(function() {
		if (document.getElementById("yearReportSelect") != null) {
			$('#yearReportSelect').modal({ backdrop: 'static', keyboard: false })
        	.one('click', '#yearMakeSure', function() {
        		$('#yearReportSelect').modal('hide');	
        		var year = document.getElementById("yearSelected").value;
        		var datas = "?year=" + year;
        		window.location = "/xlem/finance/yearReport" + datas;
        	});
		}
	})
	
	$(document).on('change', "#month-dropdown", function () {
		var month = parseInt(this.value);
		month += 1;
		document.getElementById("monthSelected").value = month;
	})
	
	$(document).on('change', "#year-dropdown", function () {
		var year = parseInt(this.value);
		year += 2016;
		document.getElementById("yearSelected").value = year;
	})
	
	$(document).on('click', "#returnFinance", function () {
		history.go(-2);
	})
	
	$(document).ready(function () {
		if (document.getElementById("financeReportTab") != null) {
			var reportTable = document.getElementById("financeReportTab");
			var trs = reportTable.getElementsByTagName("tr");
			var tr0 = trs[2];
			var tr1 = trs[3];
			var tr2 = trs[4];
			var tr3 = trs[5];
			var tds0 = tr0.getElementsByTagName("td");
			var tds1 = tr1.getElementsByTagName("td");
			var tds2 = tr2.getElementsByTagName("td");
			var tds3 = tr3.getElementsByTagName("td");
			tds3[1].innerText = (parseFloat(tds2[1].innerText) - parseFloat(tds0[3].innerText)).toFixed(2);
			tds3[3].innerText = (parseFloat(tds2[3].innerText) - parseFloat(tds0[5].innerText)).toFixed(2);
			tds3[4].innerText = (parseFloat(tds2[4].innerText) - parseFloat(tds0[6].innerText)).toFixed(2);
			tds3[6].innerText = (parseFloat(tds2[6].innerText) - parseFloat(tds0[8].innerText)).toFixed(2);
			tds3[7].innerText = (parseFloat(tds2[7].innerText) - parseFloat(tds0[9].innerText)).toFixed(2);
			tds3[9].innerText = (parseFloat(tds2[9].innerText) - parseFloat(tds0[11].innerText)).toFixed(2);
			tds3[10].innerText = (parseFloat(tds2[10].innerText) - parseFloat(tds0[12].innerText)).toFixed(2);
			tds3[12].innerText = (parseFloat(tds2[12].innerText) - parseFloat(tds0[14].innerText)).toFixed(2);
			tr0 = trs[6];
			tr1 = trs[7];
			tr2 = trs[8];
			tr3 = trs[9];
			tds0 = tr0.getElementsByTagName("td");
			tds1 = tr1.getElementsByTagName("td");
			tds2 = tr2.getElementsByTagName("td");
			tds3 = tr3.getElementsByTagName("td");
			tds3[1].innerText = (parseFloat(tds0[3].innerText) + parseFloat(tds1[1].innerText) + parseFloat(tds2[1].innerText)).toFixed(2);
			tds3[3].innerText = (parseFloat(tds0[5].innerText) + parseFloat(tds1[3].innerText) + parseFloat(tds2[3].innerText)).toFixed(2);
			tds3[4].innerText = (parseFloat(tds0[6].innerText) + parseFloat(tds1[4].innerText) + parseFloat(tds2[4].innerText)).toFixed(2);
			tds3[6].innerText = (parseFloat(tds0[8].innerText) + parseFloat(tds1[6].innerText) + parseFloat(tds2[6].innerText)).toFixed(2);
			tds3[7].innerText = (parseFloat(tds0[9].innerText) + parseFloat(tds1[7].innerText) + parseFloat(tds2[7].innerText)).toFixed(2);
			tds3[9].innerText = (parseFloat(tds0[11].innerText) + parseFloat(tds1[9].innerText) + parseFloat(tds2[9].innerText)).toFixed(2);	
			tds3[10].innerText = (parseFloat(tds0[12].innerText) + parseFloat(tds1[10].innerText) + parseFloat(tds2[10].innerText)).toFixed(2);
			tds3[12].innerText = (parseFloat(tds0[14].innerText) + parseFloat(tds1[12].innerText) + parseFloat(tds2[12].innerText)).toFixed(2);	
			tr0 = trs[10];
			tr1 = trs[11];
			tr2 = trs[12];
			tr3 = trs[13];
			tds0 = tr0.getElementsByTagName("td");
			tds1 = tr1.getElementsByTagName("td");
			tds2 = tr2.getElementsByTagName("td");
			tds3 = tr3.getElementsByTagName("td");
			tds3[1].innerText = (parseFloat(tds0[1].innerText) + parseFloat(tds1[1].innerText) + parseFloat(tds2[1].innerText)).toFixed(2);
			tds3[3].innerText = (parseFloat(tds0[3].innerText) + parseFloat(tds1[3].innerText) + parseFloat(tds2[3].innerText)).toFixed(2);
			tds3[4].innerText = (parseFloat(tds0[4].innerText) + parseFloat(tds1[4].innerText) + parseFloat(tds2[4].innerText)).toFixed(2);
			tds3[6].innerText = (parseFloat(tds0[6].innerText) + parseFloat(tds1[6].innerText) + parseFloat(tds2[6].innerText)).toFixed(2);
			tds3[7].innerText = (parseFloat(tds0[7].innerText) + parseFloat(tds1[7].innerText) + parseFloat(tds2[7].innerText)).toFixed(2);
			tds3[9].innerText = (parseFloat(tds0[9].innerText) + parseFloat(tds1[9].innerText) + parseFloat(tds2[9].innerText)).toFixed(2);	
			tds3[10].innerText = (parseFloat(tds0[10].innerText) + parseFloat(tds1[10].innerText) + parseFloat(tds2[10].innerText)).toFixed(2);
			tds3[12].innerText = (parseFloat(tds0[12].innerText) + parseFloat(tds1[12].innerText) + parseFloat(tds2[12].innerText)).toFixed(2);	
			var tr0 = trs[15];
			var tr1 = trs[16];
			var tr2 = trs[17];
			var tr3 = trs[18];
			var tds0 = tr0.getElementsByTagName("td");
			var tds1 = tr1.getElementsByTagName("td");
			var tds2 = tr2.getElementsByTagName("td");
			var tds3 = tr3.getElementsByTagName("td");
			tds3[1].innerText = (parseFloat(tds2[1].innerText) - parseFloat(tds0[3].innerText)).toFixed(2);
			tds3[3].innerText = (parseFloat(tds2[3].innerText) - parseFloat(tds0[5].innerText)).toFixed(2);
			tds3[4].innerText = (parseFloat(tds2[4].innerText) - parseFloat(tds0[6].innerText)).toFixed(2);
			tds3[6].innerText = (parseFloat(tds2[6].innerText) - parseFloat(tds0[8].innerText)).toFixed(2);
			tds3[7].innerText = (parseFloat(tds2[7].innerText) - parseFloat(tds0[9].innerText)).toFixed(2);
			tds3[9].innerText = (parseFloat(tds2[9].innerText) - parseFloat(tds0[11].innerText)).toFixed(2);
			tds3[10].innerText = (parseFloat(tds2[10].innerText) - parseFloat(tds0[12].innerText)).toFixed(2);
			tds3[12].innerText = (parseFloat(tds2[12].innerText) - parseFloat(tds0[14].innerText)).toFixed(2);
			tr0 = trs[19];
			tr1 = trs[20];
			tr2 = trs[21];
			tr3 = trs[22];
			tds0 = tr0.getElementsByTagName("td");
			tds1 = tr1.getElementsByTagName("td");
			tds2 = tr2.getElementsByTagName("td");
			tds3 = tr3.getElementsByTagName("td");
			tds3[1].innerText = (parseFloat(tds0[3].innerText) + parseFloat(tds1[1].innerText) + parseFloat(tds2[1].innerText)).toFixed(2);
			tds3[3].innerText = (parseFloat(tds0[5].innerText) + parseFloat(tds1[3].innerText) + parseFloat(tds2[3].innerText)).toFixed(2);
			tds3[4].innerText = (parseFloat(tds0[6].innerText) + parseFloat(tds1[4].innerText) + parseFloat(tds2[4].innerText)).toFixed(2);
			tds3[6].innerText = (parseFloat(tds0[8].innerText) + parseFloat(tds1[6].innerText) + parseFloat(tds2[6].innerText)).toFixed(2);
			tds3[7].innerText = (parseFloat(tds0[9].innerText) + parseFloat(tds1[7].innerText) + parseFloat(tds2[7].innerText)).toFixed(2);
			tds3[9].innerText = (parseFloat(tds0[11].innerText) + parseFloat(tds1[9].innerText) + parseFloat(tds2[9].innerText)).toFixed(2);
			tds3[10].innerText = (parseFloat(tds0[12].innerText) + parseFloat(tds1[10].innerText) + parseFloat(tds2[10].innerText)).toFixed(2);
			tds3[12].innerText = (parseFloat(tds0[14].innerText) + parseFloat(tds1[12].innerText) + parseFloat(tds2[12].innerText)).toFixed(2);
			tr0 = trs[23];
			tr1 = trs[24];
			tr2 = trs[25];
			tr3 = trs[26];
			tds0 = tr0.getElementsByTagName("td");
			tds1 = tr1.getElementsByTagName("td");
			tds2 = tr2.getElementsByTagName("td");
			tds3 = tr3.getElementsByTagName("td");
			tds3[1].innerText = (parseFloat(tds0[1].innerText) + parseFloat(tds1[1].innerText) + parseFloat(tds2[1].innerText)).toFixed(2);
			tds3[3].innerText = (parseFloat(tds0[3].innerText) + parseFloat(tds1[3].innerText) + parseFloat(tds2[3].innerText)).toFixed(2);
			tds3[4].innerText = (parseFloat(tds0[4].innerText) + parseFloat(tds1[4].innerText) + parseFloat(tds2[4].innerText)).toFixed(2);
			tds3[6].innerText = (parseFloat(tds0[6].innerText) + parseFloat(tds1[6].innerText) + parseFloat(tds2[6].innerText)).toFixed(2);
			tds3[7].innerText = (parseFloat(tds0[7].innerText) + parseFloat(tds1[7].innerText) + parseFloat(tds2[7].innerText)).toFixed(2);
			tds3[9].innerText = (parseFloat(tds0[9].innerText) + parseFloat(tds1[9].innerText) + parseFloat(tds2[9].innerText)).toFixed(2);				
			tds3[10].innerText = (parseFloat(tds0[10].innerText) + parseFloat(tds1[10].innerText) + parseFloat(tds2[10].innerText)).toFixed(2);
			tds3[12].innerText = (parseFloat(tds0[12].innerText) + parseFloat(tds1[12].innerText) + parseFloat(tds2[12].innerText)).toFixed(2);				
			tr0 = trs[27];
			tr1 = trs[28];
			tr2 = trs[29];
			tr3 = trs[30];
			tds0 = tr0.getElementsByTagName("td");
			tds1 = tr1.getElementsByTagName("td");
			tds2 = tr2.getElementsByTagName("td");
			tds3 = tr3.getElementsByTagName("td");
			tds3[1].innerText = (parseFloat(tds0[1].innerText) + parseFloat(tds1[1].innerText) + parseFloat(tds2[1].innerText)).toFixed(2);
			tds3[3].innerText = (parseFloat(tds0[3].innerText) + parseFloat(tds1[3].innerText) + parseFloat(tds2[3].innerText)).toFixed(2);
			tds3[4].innerText = (parseFloat(tds0[4].innerText) + parseFloat(tds1[4].innerText) + parseFloat(tds2[4].innerText)).toFixed(2);
			tds3[6].innerText = (parseFloat(tds0[6].innerText) + parseFloat(tds1[6].innerText) + parseFloat(tds2[6].innerText)).toFixed(2);
			tds3[7].innerText = (parseFloat(tds0[7].innerText) + parseFloat(tds1[7].innerText) + parseFloat(tds2[7].innerText)).toFixed(2);
			tds3[9].innerText = (parseFloat(tds0[9].innerText) + parseFloat(tds1[9].innerText) + parseFloat(tds2[9].innerText)).toFixed(2);
			tds3[10].innerText = (parseFloat(tds0[10].innerText) + parseFloat(tds1[10].innerText) + parseFloat(tds2[10].innerText)).toFixed(2);
			tds3[12].innerText = (parseFloat(tds0[12].innerText) + parseFloat(tds1[12].innerText) + parseFloat(tds2[12].innerText)).toFixed(2);
			
			// deal with total summary
			tr0 = trs[5];
			tr1 = trs[9];
			tr2 = trs[13];
			tr3 = trs[14];
			tds0 = tr0.getElementsByTagName("td");
			tds1 = tr1.getElementsByTagName("td");
			tds2 = tr2.getElementsByTagName("td");
			tds3 = tr3.getElementsByTagName("td");
			tds3[1].innerText = (parseFloat(tds0[1].innerText) + parseFloat(tds1[1].innerText) + parseFloat(tds2[1].innerText)).toFixed(2);
			tds3[3].innerText = (parseFloat(tds0[3].innerText) + parseFloat(tds1[3].innerText) + parseFloat(tds2[3].innerText)).toFixed(2);
			tds3[4].innerText = (parseFloat(tds0[4].innerText) + parseFloat(tds1[4].innerText) + parseFloat(tds2[4].innerText)).toFixed(2);
			tds3[6].innerText = (parseFloat(tds0[6].innerText) + parseFloat(tds1[6].innerText) + parseFloat(tds2[6].innerText)).toFixed(2);
			tds3[7].innerText = (parseFloat(tds0[7].innerText) + parseFloat(tds1[7].innerText) + parseFloat(tds2[7].innerText)).toFixed(2);
			tds3[9].innerText = (parseFloat(tds0[9].innerText) + parseFloat(tds1[9].innerText) + parseFloat(tds2[9].innerText)).toFixed(2);
			tds3[10].innerText = (parseFloat(tds0[10].innerText) + parseFloat(tds1[10].innerText) + parseFloat(tds2[10].innerText)).toFixed(2);
			tds3[12].innerText = (parseFloat(tds0[12].innerText) + parseFloat(tds1[12].innerText) + parseFloat(tds2[12].innerText)).toFixed(2);

			// deal with total summary
			tr0 = trs[18];
			tr1 = trs[22];
			tr2 = trs[26];
			tr3 = trs[30];
			var tr4 = trs[31];
			tds0 = tr0.getElementsByTagName("td");
			tds1 = tr1.getElementsByTagName("td");
			tds2 = tr2.getElementsByTagName("td");
			tds3 = tr3.getElementsByTagName("td");
			var tds4 = tr4.getElementsByTagName("td");
			tds4[1].innerText = (parseFloat(tds0[1].innerText) + parseFloat(tds1[1].innerText) + parseFloat(tds2[1].innerText) + parseFloat(tds3[1].innerText)).toFixed(2);
			tds4[3].innerText = (parseFloat(tds0[3].innerText) + parseFloat(tds1[3].innerText) + parseFloat(tds2[3].innerText) + parseFloat(tds3[3].innerText)).toFixed(2);
			tds4[4].innerText = (parseFloat(tds0[4].innerText) + parseFloat(tds1[4].innerText) + parseFloat(tds2[4].innerText) + parseFloat(tds3[4].innerText)).toFixed(2);
			tds4[6].innerText = (parseFloat(tds0[6].innerText) + parseFloat(tds1[6].innerText) + parseFloat(tds2[6].innerText) + parseFloat(tds3[6].innerText)).toFixed(2);
			tds4[7].innerText = (parseFloat(tds0[7].innerText) + parseFloat(tds1[7].innerText) + parseFloat(tds2[7].innerText) + parseFloat(tds3[7].innerText)).toFixed(2);
			tds4[9].innerText = (parseFloat(tds0[9].innerText) + parseFloat(tds1[9].innerText) + parseFloat(tds2[9].innerText) + parseFloat(tds3[9].innerText)).toFixed(2);			
			tds4[10].innerText = (parseFloat(tds0[10].innerText) + parseFloat(tds1[10].innerText) + parseFloat(tds2[10].innerText) + parseFloat(tds3[10].innerText)).toFixed(2);
			tds4[12].innerText = (parseFloat(tds0[12].innerText) + parseFloat(tds1[12].innerText) + parseFloat(tds2[12].innerText) + parseFloat(tds3[12].innerText)).toFixed(2);			
		}
	})
	
	function updatePlot() {
		var now = new Date();
		var cutTime = now.getTime();
		var datas = "cutTime=" + cutTime;
		
		$.ajax({  
			url: "/xlem/finance/getSaleRTData",
			data:'limit=5000',
			type:'get',
			dataType:'json',
			async : false,  
			data: datas
		}).done(function(data, status, xhr) {
			var saleNum = [];
			var saleMoney = [];
			for (var i = 0; i < data.num; i++) {
				saleNum.push([i, data.saleNum[i]]);
				saleMoney.push([i, data.saleMoney[i]]);
			}
		
			var options = {
				legend: {
					noColumn: 0,
					backgroundColor: "transparent",
					position: "se"
				}
			};
			
			$.plot("#placeholder", [
			  { label: "实时票务订单量", 
				data: saleNum,
				lines: { show: true },
				points: { show: true }
			  }, 
			  { label: "实时票务销售额", 
				data: saleMoney,
				lines: { show: true },
				points: { show: true }
			}], options);
			$("#placeholder div.legend div").css("left", "30px");
			$("#placeholder div.legend table").width(150);
		}).fail(function(xhr, status, error) {  
			alert("请求失败！");
		})				
	}

	$(document).ready(function () {
		if (document.getElementById("placeholder")) {
			updatePlot();
			// setInterval(updatePlot, 5000);
		}
	})
	
	$(document).ready(function () {
		if (document.getElementById("yearBarChart")) {
			if (document.getElementById("yearStatSelect") != null) {
				$('#yearStatSelect').modal({ backdrop: 'static', keyboard: false })
        	.one('click', '#yearMakeSure', function() {
    			$('#yearStatSelect').modal('hide');	
    			
    			var useMoney = false;
    			var id = document.getElementsByName("data_mode");
    		    for(var i=0;i<id.length; i++){
    		        var radio=id[i];
    		        if (radio.checked) {
    		        	switch (i) {
    		        	case 0:
    		        		useMoney = false;
    		        		break;
    		
    		        	case 1:
    		        		useMoney = true;
    		        		break;
    		        	
    		        	default:
    		        		alert("error.");
    		        		break;
    		        	}
    		        }
    		    }
    		    
    			var year = document.getElementById("yearSelected").value;
    			var datas = "year=" + year;
   			
        		$.ajax({  
        			url: "/xlem/finance/getYearSaleData",
        			data:'limit=5000',
        			type:'get',
        			dataType:'json',
        			async : false,  
        			data: datas
        		}).done(function(data, status, xhr) {
        			var datas = [];
        			for (var i = 0; i < data.num; i++) {
        				var tmpData = [];
        				for (var j = 0; j < data.statDatas[i].datas.length; j++ ) {
        					var anchor = new Date(year, j, 15).getTime();
        					if (!useMoney) 
        						tmpData.push([anchor, data.statDatas[i].datas[j]]);
        					else
        						tmpData.push([anchor, data.statDatas[i].amounts[j]]);
        					
        				}
        				
        				datas.push({label: data.statDatas[i].label, data: tmpData, 
        							bars: {show: true,
        								   barWidth: 12 * 24 * 60 * 60 * 300,
        								   fill: true,
        								   lineWidth: 1,
        								   order: i + 1 }});
        			}
        					
        			var options = {
        				xaxis: {
        					min: (new Date(year, 0, 1)).getTime(),
        					max: (new Date(year, 11, 30)).getTime(),	
        					mode: "time",
        					timeformat: "%b",
        					tickSize: [1, "month"],
        					monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        					tickLength: 0,
        					axisLabel: 'Month',
        		            axisLabelUseCanvas: true,
        		            axisLabelFontSizePixels: 12,
        		            axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
        		            axisLabelPadding: 5
        		        },
        		        yaxis: {
        		            axisLabel: 'Value',
        		            axisLabelUseCanvas: true,
        		            axisLabelFontSizePixels: 12,
        		            axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
        		            axisLabelPadding: 5
        		        },
        		        grid: {
        		            hoverable: true,
        		            clickable: false,
        		            borderWidth: 1
        		        },
        		        legend: {
        		            labelBoxBorderColor: "none",
        					backgroundColor: "transparent",
        					position: "right"
        		        },
        		        series: {
        		            shadowSize: 1
        		        } };
        			
	        			$.plot("#yearBarChart", datas, options);
	        			
	        			var pies = [];
	        			for (var i = 0; i < data.num; i++) {
	        				var tmpData = 0;
	        				for (var j = 0; j < data.statDatas[i].datas.length; j++ ) {
	        					if (!useMoney)
	        						tmpData += data.statDatas[i].datas[j];
	        					else 
	        						tmpData += data.statDatas[i].amounts[j];
	        				}
	        				
	        				pies.push({label: data.statDatas[i].label, data: tmpData});
	        			}
	        			
	        		    $.plot("#yearPieChart", pies, {
	        		         series: {
	        		            pie: {
	        		                show: true
	        		            }
	        		         },
	        		         legend: {
	        		            labelBoxBorderColor: "none"
	        		         }
	        		    });
	        			
	        			
	        		}).fail(function(xhr, status, error) {  
	        			alert("请求失败！");
	        		});				        		
        		});
			}
		}
	})
	
	$(document).ready(function () {
		if (document.getElementById("monthBarChart")) {
			if (document.getElementById("monthStatSelect") != null) {
				$('#monthStatSelect').modal({ backdrop: 'static', keyboard: false })
        	.one('click', '#monthMakeSure', function() {
    			$('#monthStatSelect').modal('hide');	
    			
    			var useMoney = false;
    			var id = document.getElementsByName("data_mode");
    		    for(var i=0;i<id.length; i++){
    		        var radio=id[i];
    		        if (radio.checked) {
    		        	switch (i) {
    		        	case 0:
    		        		useMoney = false;
    		        		break;
    		
    		        	case 1:
    		        		useMoney = true;
    		        		break;
    		        	
    		        	default:
    		        		alert("error.");
    		        		break;
    		        	}
    		        }
    		    }
    		    
    			var year = document.getElementById("yearSelected").value;
    			var month = document.getElementById("monthSelected").value;
    			var datas = "year=" + year;
    			datas += "&&month=" + month;
   			
        		$.ajax({  
        			url: "/xlem/finance/getMonthSaleData",
        			data:'limit=5000',
        			type:'get',
        			dataType:'json',
        			async : false,  
        			data: datas
        		}).done(function(data, status, xhr) {
        			var datas = [];
        			for (var i = 0; i < data.num; i++) {
        				var tmpData = [];
        				for (var j = 0; j < data.statDatas[i].datas.length; j++ ) {
        					var anchor = new Date(year, month, j + 1).getTime();
        					if (!useMoney) 
        						tmpData.push([anchor, data.statDatas[i].datas[j]]);
        					else
        						tmpData.push([anchor, data.statDatas[i].amounts[j]]);
        					
        				}
        				
        				datas.push({label: data.statDatas[i].label, data: tmpData, 
        							bars: {show: true,
        								   barWidth: 24 * 60 * 60 * 600 / data.num,
        								   fill: true,
        								   lineWidth: 1,
        								   order: i + 1 }});
        			}
        			
        			var leap = false;
        			if (year % 100 == 0) {
        				if (year % 400 == 0) {
        					leap = true;
        				}
        			}
        			else {
        				if (year % 4 == 0) {
        					leap = true;
        				}
        			}
        			
        			var maxDay;
        			if (month + 1 == 1 || month + 1 == 3 || month + 1 == 5 || month + 1 == 7
        				|| month + 1 == 8 || month + 1 == 10 || month + 1 == 12) {
        				maxDay = 31;
        			}
        			else if (month + 1 == 2) {
        				if (leap) 
        					maxDay = 28;
        				else
        					maxDay = 29;
        			}
        			else {
        				maxDay = 30;
        			}        			
        					
        			var options = {
        				xaxis: {
        					min: (new Date(year, month, 1)).getTime(),
        					max: (new Date(year, month, maxDay)).getTime(),	
        					mode: "time",
        					timeformat: "%d",
        					tickSize: [3, "day"],
        					tickLength: 0,
        					axisLabel: 'Day',
        		            axisLabelUseCanvas: true,
        		            axisLabelFontSizePixels: 12,
        		            axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
        		            axisLabelPadding: 5
        		        },
        		        yaxis: {
        		            axisLabel: 'Value',
        		            axisLabelUseCanvas: true,
        		            axisLabelFontSizePixels: 12,
        		            axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
        		            axisLabelPadding: 5
        		        },
        		        grid: {
        		            hoverable: true,
        		            clickable: false,
        		            borderWidth: 1
        		        },
        		        legend: {
        		            labelBoxBorderColor: "none",
        					backgroundColor: "transparent",
        					position: "right"
        		        },
        		        series: {
        		            shadowSize: 1
        		        } };
        			
	        			$.plot("#monthBarChart", datas, options);
	        			
	        			var pies = [];
	        			for (var i = 0; i < data.num; i++) {
	        				var tmpData = 0;
	        				for (var j = 0; j < data.statDatas[i].datas.length; j++ ) {
	        					if (!useMoney)
	        						tmpData += data.statDatas[i].datas[j];
	        					else 
	        						tmpData += data.statDatas[i].amounts[j];
	        				}
	        				
	        				pies.push({label: data.statDatas[i].label, data: tmpData});
	        			}
	        			
	        		    $.plot("#monthPieChart", pies, {
	        		         series: {
	        		            pie: {
	        		                show: true
	        		            }
	        		         },
	        		         legend: {
	        		            labelBoxBorderColor: "none"
	        		         }
	        		    });
	        			
	        			
	        		}).fail(function(xhr, status, error) {  
	        			alert("请求失败！");
	        		});				        		
        		});
			}
		}
	})	
	
	$(document).ready(function () {
		if (document.getElementById("dayBarChart")) {
			if (document.getElementById("dayStatSelect") != null) {
				$('#dayStatSelect').modal({ backdrop: 'static', keyboard: false })
        	.one('click', '#dayMakeSure', function() {
    			$('#dayStatSelect').modal('hide');	
    			
    			var useMoney = false;
    			var id = document.getElementsByName("data_mode");
    		    for(var i=0;i<id.length; i++){
    		        var radio=id[i];
    		        if (radio.checked) {
    		        	switch (i) {
    		        	case 0:
    		        		useMoney = false;
    		        		break;
    		
    		        	case 1:
    		        		useMoney = true;
    		        		break;
    		        	
    		        	default:
    		        		alert("error.");
    		        		break;
    		        	}
    		        }
    		    }
    		    
    			var day = document.getElementById("daySelected").value;
    			var datas = "day=" + day;
    			var queryDate = new Date(day);
    			var year = queryDate.getFullYear();
    			var month = queryDate.getMonth() + 1;
    			var date = queryDate.getDate();
   			
        		$.ajax({  
        			url: "/xlem/finance/getDaySaleData",
        			data:'limit=5000',
        			type:'get',
        			dataType:'json',
        			async : false,  
        			data: datas
        		}).done(function(data, status, xhr) {
        			var datas = [];
        			for (var i = 0; i < data.num; i++) {
        				var tmpData = [];
        				var anchor = new Date(year, month, date).getTime();
        				if (!useMoney) 
        					tmpData.push([anchor, data.statDatas[i].datas[0]]);
        				else
        					tmpData.push([anchor, data.statDatas[i].amounts[0]]);
        					
        				datas.push({label: data.statDatas[i].label, data: tmpData, 
        							bars: {show: true,
        								   barWidth: 24 * 60 * 60 * 600 / data.num,
        								   fill: true,
        								   lineWidth: 1,
        								   order: i + 1 }});
        			}
        			
        			var options = {
        				xaxis: {
        					min: (new Date(year, month, date)).getTime() - 24 * 60 * 60 * 1000,
        					max: (new Date(year, month, date)).getTime() + 24 * 60 * 60 * 1000,	
        					mode: "time",
        					timeformat: "%b",
        					monthNames: ["", "Feb", "Mar"],
        					tickSize: [1, "month"],
        					tickLength: 0,
        					axisLabel: year + "年" + month + "月" + date + "日",
        		            axisLabelUseCanvas: true,
        		            axisLabelFontSizePixels: 12,
        		            axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
        		            axisLabelPadding: 5
        		        },
        		        yaxis: {
        		            axisLabel: 'Value',
        		            axisLabelUseCanvas: true,
        		            axisLabelFontSizePixels: 12,
        		            axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
        		            axisLabelPadding: 5
        		        },
        		        grid: {
        		            hoverable: true,
        		            clickable: false,
        		            borderWidth: 1
        		        },
        		        legend: {
        		            labelBoxBorderColor: "none",
        					backgroundColor: "transparent",
        					position: "right"
        		        },
        		        series: {
        		            shadowSize: 1
        		        } };
        			
	        			$.plot("#dayBarChart", datas, options);
	        			
	        			var pies = [];
	        			for (var i = 0; i < data.num; i++) {
	        				var tmpData = 0;
	        				if (!useMoney)
	        					tmpData += data.statDatas[i].datas[0];
	        				else 
	        					tmpData += data.statDatas[i].amounts[0];
	        				
	        				pies.push({label: data.statDatas[i].label, data: tmpData});
	        			}
	        			
	        		    $.plot("#dayPieChart", pies, {
	        		         series: {
	        		            pie: {
	        		                show: true
	        		            }
	        		         },
	        		         legend: {
	        		            labelBoxBorderColor: "none"
	        		         }
	        		    });
	        			
	        			
	        		}).fail(function(xhr, status, error) {  
	        			alert("请求失败！");
	        		});				        		
        		});
			}
		}
	})	
	
    function initData(pageindx, url) {  
        var tbody = "";  
        var pageCount = "";
        
        var userName = document.getElementById("userId").value;
        if (userName == "") {
        	userName = "all";
        }
        
        var indentId = document.getElementById("indentId").value;
        if (indentId == "") {
        	indentId = "all";
        }
        
        var datas = "userName=" + userName;
        datas += "&&indentId=" + indentId;
        datas += "&&offset=" + (pageindx);  
        
        $.ajax({  
			url: url,
			data:'limit=5000',
			type:'get',
			dataType:'json',
            async : false,  
            data : datas,
		}).done(function(data, status, xhr) {
			resHtml = "";
			var needEndDiv;
			var entryCount = 0;
			resHtml = '<table id="indentTraceTable" style="width:100%">';
			resHtml += '<tr>';
			resHtml += '<th colspan="4" style="text-align: center">电商平台</th>';
			resHtml += '<th colspan="2" style="text-align: center">自动取票机</th>';
			resHtml += '<th colspan="2" style="text-align: center">门禁系统</th>';
			resHtml += '</tr>';
			resHtml += '<tr>';
			resHtml += '<th style="text-align: center">游客ID</th>'
			resHtml += '<th style="text-align: center">订单号</th>';
			resHtml += '<th style="text-align: center">录入系统时间</th>';
			resHtml += '<th style="text-align: center">订单状态</th>';
			resHtml += '<th style="text-align: center">数据同步时间</th>';
			resHtml += '<th style="text-align: center">订单状态</th>';
			resHtml += '<th style="text-align: center">数据同步时间</th>';
			resHtml += '<th style="text-align: center">订单状态</th>';
			resHtml += '</tr>';
			
			for (var i = 0; i < data.num; i++) {
				if (entryCount < 10) {
					resHtml += '<tr>';
					resHtml += '<td>';
					resHtml += data.traces[i].userId;
					resHtml += '</td>';
					resHtml += '<td>';
					resHtml += data.traces[i].indentId;
					resHtml += '</td>';
					resHtml += '<td>';
					var bookDate = new Date(data.traces[i].platFormTime);
					var datestring = bookDate.getFullYear() + "-" + (bookDate.getMonth() + 1) + "-" + bookDate.getDate();
					resHtml += datestring;
					resHtml += '</td>';
					resHtml += '<td name="platformStatus">';
					resHtml += data.traces[i].platFormStatus;
					resHtml += '</td>';
					resHtml += '<td>';
					var machineDate = new Date(data.traces[i].machineTime);
					if (data.traces[i].machineTime != null) {
						datestring = machineDate.getFullYear() + "-" + (machineDate.getMonth() + 1) + "-" + machineDate.getDate();
					}
					else {
						datestring = "";
					}
					resHtml += datestring;
					resHtml += '</td>';
					resHtml += '<td name="machineStatus">';
					if (data.traces[i].machineStatus != null) {
						resHtml += data.traces[i].machineStatus;
					}
					else {
						resHtml += "";
					}
					resHtml += '</td>';
					resHtml += '<td>';
					var entryGuardDate = new Date(data.traces[i].gateGuardTime);
					if (data.traces[i].gateGuardTime != null) {
						datestring = entryGuardDate.getFullYear() + "-" + (entryGuardDate.getMonth() + 1) + "-" + entryGuardDate.getDate();
					}
					else {
						datestring = "";
					}
					resHtml += datestring;
					resHtml += '</td>';
					resHtml += '<td name="entryGuardStatus">';
					if (data.traces[i].gateGuardStatus != null) {
						resHtml += data.traces[i].gateGuardStatus;
					}
					else {
						resHtml += "";
					}
					resHtml += '</td>';
					resHtml += '</tr>';
					
					entryCount++;
				}
			}
			resHtml += '</table>';
			
			resHtml += '<div id="Pager" style="text-align:left">';
		    resHtml += '</div>';

			// $("indentDiv").html(resHtml);  
			document.getElementById("indentTraceDiv").innerHTML = resHtml;
            document.getElementById("total").value = data.num;
            document.getElementById("offset").value = pageindx;
            
            initPager("/xlem/finance/getTicketIndentTraceData");
            ticketIndentTraceStatus();
		}).fail(function(xhr, status, error) {  
        	alert("请求失败！");
		})	
    }
    
    function updatePager(offset, total) {
    	if (offset < 0) {
    		alert("已经是第一页");
    		return;
    	}
    	
    	if (offset >= total) {
    		alert("已经是最后一页");
    		return;
    	}
    	
    	initData(offset, "/xlem/finance/getTicketIndentTraceData");
    }
    
    $(document).ready(function() { 
    	if (document.getElementById("indentTraceDiv") != null) {
	    	initData(0, "/xlem/finance/getTicketIndentTraceData");
	    	initPager("/xlem/finance/getTicketIndentTraceData");
			ticketIndentTraceStatus();
    	}
    })
    
    function initPager(url) {
    	var total = document.getElementById("total").value;
    	var offset = document.getElementById("offset").value;
    	
        var pagerHtml = "";
        pagerHtml += '<nav aria-label="Page navigation">';
        pagerHtml += '<ul class="pagination">';
        pagerHtml += '<li>';
        pagerHtml += '<a href="javascript:updatePager(';
        pagerHtml += offset - 10;
        pagerHtml += ',';
        pagerHtml += total;
        pagerHtml += ');" ';
        pagerHtml += 'aria-label="Previous">';
        pagerHtml += '<span aria-hidden="true">&#171;</span>';
        pagerHtml += '</a>';
        pagerHtml += '</li>';
        
        for (i = 0; i < Math.ceil(total / 10); i++) {
        	pagerHtml += '<li><a href="javascript:updatePager(';
        	pagerHtml += i * 10;
        	pagerHtml += ',';
        	pagerHtml += total;
        	pagerHtml += ');" >';
        	pagerHtml += i + 1;
        	pagerHtml += '</a></li>';
        }
        
        pagerHtml += '<li>';
        pagerHtml += '<a href="javascript:updatePager(';
        pagerHtml += parseInt(offset) + 10;
        pagerHtml += ',';
        pagerHtml += total;
        pagerHtml += ');" ';
        pagerHtml += 'aria-label="Next">';
        pagerHtml += '<span aria-hidden="true">&#187;</span>';
        pagerHtml += '</a>';
        pagerHtml += '</li>';
        pagerHtml += '</ul>';
        pagerHtml += '</nav>';
        
		document.getElementById("Pager").innerHTML = pagerHtml;
    }    
    
	function ticketIndentTraceStatus() {
		var indentTraceTable = document.getElementById("indentTraceTable");
		var trs = indentTraceTable.getElementsByTagName("tr");
		
		// Iterate over the links
		for (var i = 2, len = trs.length, tr; i < len; ++i) {
		    tr = trs[i];
		    var tds = tr.getElementsByTagName("td");
		    var status = tds[3].innerText;
		    var text = status;
		    switch (status) {
		    case "00":
		    	text = "待支付／待审核";
		    	break;
		    
		    case "01":
		    	text = "已支付／已审核";
		    	break;
		    
		    case "02":
		    	text = "已使用";
		    	break;
		    	
		    case "08":
		    	text = "已退票";
		    	break;
		    
		    case "04":
		    	text = "已删除";
		    	break;
		    		
		    }
		    tds[3].innerText = text;
		    
		    status = tds[5].innerText;
		    var text = status;
		    switch (status) {
		    case "00":
		    	text = "未取票";
		    	break;
		    
		    case "01":
		    	text = "已取票";
		    	break;
		    
		    case "02":
		    	text = "取票失败";
		    	break;
		    }
		    tds[5].innerText = text;
		    
		    status = tds[7].innerText;
		    var text = status;
		    switch (status) {
		    case "100":
		    	text = "已使用";
		    	break;
		    
		    case "000":
		    	text = "未使用";
		    	break;
		    }
		    tds[7].innerText = text;		    
		}
	}
	
	$(document).on('click', "#traceIndent", function () {
    	initData(0, "/xlem/finance/getTicketIndentTraceData");
    	initPager("/xlem/finance/getTicketIndentTraceData");
		ticketIndentTraceStatus();
	})
		    