$(function() {
	//loadGrid('hotelGrid','');
	
	$.ajax({
		url:'roomTypeOrveAction!findGrid.action',
		data:'limit=100',
		dataType:'json',
		type:'post',
		success: function(result){
			
			var hotelId = 0;
			
			var hotelsHtml = "";
			var roomTypeHtml = {};
			
			$.each(result.root, function(i, n){
				var hotId = n.hotel.hotelId;
				if(hotelId==hotId){
					roomTypeHtml[hotId]=roomTypeHtml[hotId]+'、'+n.typeName;
				}else{
					hotelId=hotId;
					roomTypeHtml[hotId]=n.typeName;
				}
				
			});
			hotelId = 0;
			$.each(result.root, function(i, n){
				var hotId = n.hotel.hotelId;
				if(hotelId!=hotId){
					hotelId=hotId;
					
					hotelsHtml+='<tr>';
					hotelsHtml+='<td rowspan="3" style="padding: 10px;" >';
					hotelsHtml+='<span class="hotImg">';
					hotelsHtml+='<img src="img/jd'+hotelId+'.gif" width="200" class="butImg" height="150" onclick="book('+hotelId+');" title="预订酒店"/>';
					hotelsHtml+='</span>';
					hotelsHtml+='</td>';
					hotelsHtml+='<td colspan="2" class="hotNam">';
					hotelsHtml+=n.hotel.hotelName+'<img src="img/jdBut.gif" class="butImg" onclick="book('+hotelId+');" title="预订酒店"/>';
					hotelsHtml+='<a href="javaScript:" onclick="showImg(\''+hotelId+'\',\''+n.hotel.hotelName+'\');" style="margin-left: 10px;color: #AE096F;">酒店图片</a>';
					hotelsHtml+='</td>';
					hotelsHtml+='</tr>';
					hotelsHtml+='<tr>';
					hotelsHtml+='<td width="60px" class="tit">';
					hotelsHtml+='房型：';
					hotelsHtml+='</td>';
					hotelsHtml+='<td class="con">';
					hotelsHtml+=roomTypeHtml[hotId];
					hotelsHtml+='</td>';
					hotelsHtml+='</tr>';
					hotelsHtml+='<tr>';
					hotelsHtml+='<td class="tit">';
					hotelsHtml+='简介：';
					hotelsHtml+='</td>';
					hotelsHtml+='<td class="con">';
					hotelsHtml+=n.hotel.hotelDesc;
					hotelsHtml+='</td>';
					hotelsHtml+='</tr>';
					hotelsHtml+='<tr>';
					hotelsHtml+='<td colspan="3" class="splitLing">';
					hotelsHtml+='&nbsp;';
					hotelsHtml+='</td>';
					hotelsHtml+='</tr>';
					
				}
				
			});
			$("#hotTable").html(hotelsHtml);
			
			calcHeigt('',50);
		},
		error:commerror
	});
	
	$('#showImg').window({
		title:'酒店图片展示',
		iconCls:'icon-tip',
		minimizable:false,
		maximizable:false,
		collapsible:false,
		resizable:false,
		//href:'jsp/home/hotelImgShow.html',
		draggable:true,
		closed:true,
		onClose:function(){
			$('#showImgTable').html("");
			hotId="";
			hotName = "酒店";
		}
	});
});

var hotId;
var hotName;

function showImg(hoId,hoName){
	hotId = hoId;
	hotName = hoName;
	window.open('jumpOverAction!hotelImgPage.action?hotId='+hotId+'&hotName='+encodeURIComponent(hotName),target='imgP');
	 
}

var  roomReservationHotelId = "";

/**
 * 进入对应酒店房型选择页面
 * @param hotelId
 */
function book(hotelId){
	roomReservationHotelId = hotelId;
	if(hotelId!=""){
		parent.loadhref("hotelAction!queryRoomType.action",'',{hotelId:hotelId});
	}
	else{
		alert("酒店选择有误，请重新选择!");
	}
}
