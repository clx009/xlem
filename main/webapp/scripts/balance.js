	function initData() {
		var totalPrice = 0.0;
		var detailTable = document.getElementById("detailTable");
		var trs = detailTable.getElementsByTagName("tr");
		
		// Iterate over the links
		for (var i = 1, len = trs.length, tr; i < len; ++i) {
		    tr = trs[i];
		    var tds = tr.getElementsByTagName("td");
		    var price = parseFloat(tds[3].innerText);
		    var num = parseInt(tds[5].childNodes[0].childNodes[0].value);
		    totalPrice += price * num;
		}
		
		document.getElementById("total").value = totalPrice;
		document.getElementById("sumPrice").value = totalPrice;
	}

	jQuery(document).ready(function ($) {
		$('.datepicker').each(function(){
		    var datepicker = $(this).datepicker();
		    datepicker.datepicker('setDate', new Date());
		});
		
		$('.spinner').each(function(){
		    var spinner = $(this).spinner();
		    spinner.spinner("value", 1);
		});

		initData();
	});
	
	$(document).on('spinstop', ".spinner", function () {
        var datas = "indentDetailId=" + this.id;  
        datas += "&&amount=" + this.value;  
  
        $.ajax({  
			url: "/xlem/ticket/amountChange",
			data:'limit=5000',
			type:'post',
			dataType:'json',
            async : false,  
            data : datas,
		}).done(function(data, status, xhr) {
			initData();
		}).fail(function(xhr, status, error) {  
        	alert("请求失败！");
		})			
	});

	$(document).on('change', ".spinner", function () {
        var datas = "indentDetailId=" + this.id;  
        datas += "&&amount=" + this.value;  
  
        $.ajax({  
			url: "/xlem/ticket/amountChange",
			data:'limit=5000',
			type:'post',
			dataType:'json',
            async : false,  
            data : datas,
		}).done(function(data, status, xhr) {
			initData();
		}).fail(function(xhr, status, error) {  
        	alert("请求失败！");
		})			
	});

	$(document).on('change', ".datepicker", function () {
		var td = this.parentNode;
		var tr = td.parentNode;
		
        var datas = "indentDetailId=" + tr.id;  
        datas += "&&useDate=" + this.value;  
  
        $.ajax({  
			url: "/xlem/ticket/useDateChange",
			data:'limit=5000',
			type:'post',
			dataType:'json',
            async : false,  
            data : datas,
		}).done(function(data, status, xhr) {
			initData();
		}).fail(function(xhr, status, error) {  
        	alert("请求失败！");
		})	
	})
	
	$(document).on('click', "#Pay", function() {
        $.ajax({  
			url: "/xlem/login/userLogined",
			data:'limit=5000',
			type:'get',
			dataType:'json',
            async : false,  
		}).done(function(data, status, xhr) {
			if (data.msg == "unlogined") {
				var datas = "callbackUrl=" + document.location.href;
		        $.ajax({  
					url: "/xlem/login/general",
					data:'limit=100',
					type:'post',
					dataType:'json',
		            async : false,
		            data: datas,
				}).done(function(data, status, xhr) {
					window.location.replace(data.url);
				}).fail(function(xhr, status, error) {  
		        	alert("请求失败！");
		        	return;
				});
			}
			
			if (data.msg == 'logined') {
				var payMoney = document.getElementById("sumPrice").value;
				var linkman = document.getElementById("ticketMan").value;
				if (linkman == "") {
					alert("请填写取票人");
					return;
				}
				var idCardNumber = document.getElementById("idCard").value;
				if (idCardNumber == "") {
					alert("请填写取票人身份证号");
					return;
				}
				var phoneNumber = document.getElementById("phoneNumber").value;
				if (phoneNumber == "") {
					alert("请填写取票人手机号");
					return;
				}
				
				var datas = "payMoney=" + payMoney;
				datas += "&&linkman=" + linkman;
				datas += "&&idCardNumber=" + idCardNumber;
				datas += "&&phoneNumber=" + phoneNumber;
			
		        $.ajax({  
					url: "/xlem/ticket/confirmIndent",
					data:'limit=5000',
					type:'post',
					dataType:'json',
		            async : false,  
		            data : datas,
				}).done(function(data, status, xhr) {
					if (data.msg == "invalid") {
						alert("仅游客和旅行社可以购票");
						return;
					}
					
					if (data.userType == "tourist") {
						ajaxUrlVal = '/xlem/ticket/getPayParam';
						ajaxDataVal='indentId=' + data.indentId + '&defaultbank=';
						if(data.aliBank=="1"){
							popup('bankPage','/xlem/pay/bankSelect','选择网上银行',800,600,
							{
								ajaxUrlVal:ajaxUrlVal,
								ajaxDataVal:ajaxDataVal,
								ajaxReturnType:'ticket'
							},
							function(){});
						}else{
							$.ajax({
								url:ajaxUrlVal,
								data:ajaxDataVal,
								dataType:'json',
								type:'get',
								async:false,
							}).done(function(data, status, xhr) {
								ticketReturn(data[0],'pay');
							}).fail(function(xhr, status, error) {  
								alert("请求失败！");
							})
						}
					}
					
					if (data.userType == "travelAgency") {
						if (data.noenough == "noenough") {
							alert("专用账户余额不足，请充值");
							window.location = "/xlem/ts_account/travelAgency"
						}
						alert("已完成预订，请等待审核");
						window.location = "/xlem/index";
					}
				}).fail(function(xhr, status, error) {  
		        	alert("请求失败！");
				});
			}
		}).fail(function(xhr, status, error) {  
        	alert("请求失败！");
        	return;
		});
	})
	
	function ticketReturn(result,p){
		if(p=='pay'){//提交支付
			$("#aliBankpay").form('load',result);
			var payKeys = Object.keys(result);
			var inputElements = document.getElementsByTagName('input');
			for (i = 0; i < payKeys.length; i++) {
				for (j = 0; j < inputElements.length; j++) {
					if (inputElements[j].name == payKeys[i]) {
						inputElements[j].value = result[payKeys[i]];
						break;
					}
				}
			}
  		
			var pay_test = result.pay_test;
		
			if("true"==pay_test){
				$("#aliBankpay").attr("action",'http://192.168.5.211:8080/simulationPay/simulatePayAction!gateway.action');
			}else{
				$("#aliBankpay").attr("action",'https://mapi.alipay.com/gateway.do');
			}
		
			$("#aliBankpay").submit();
			
			openDialog("confirmPay", "支付确认", 270, 100, "请确认支付已完成", function(){
			},{
			buttons:[{
				text: "支付完成",
				click: function() {
					$(this).dialog("close");
					// test code for alipay
					/*var out_trade_no = document.getElementsByName("out_trade_no")[0].value;
					var trade_status = "TRADE_SUCCESS";
					
					var datas = "out_trade_no=" + out_trade_no;
					datas += "&&trade_status=" + trade_status;
					datas += "&&notify_time=test";
					datas += "&&notify_type=test";
					datas += "&&notify_id=test";
					datas += "&&sign_type=test";
					datas += "&&sign=test";
					datas += "&&subject=test";
					datas += "&&payment_type=test";
					datas += "&&trade_no=test";
					datas += "&&gmt_create=test";
					datas += "&&gmt_payment=test";
					datas += "&&gmt_close=test";
					datas += "&&refund_status=test";
					datas += "&&gmt_refund=test";
					datas += "&&seller_email=test";
					datas += "&&buyer_email=test";
					datas += "&&seller_id=test";
					datas += "&&buyer_id=test";
					datas += "&&price=test";
					datas += "&&total_fee=test";
					datas += "&&quantity=test";
					datas += "&&body=test";
					datas += "&&discount=test";
					datas += "&&is_total_fee_adjust=test";
					datas += "&&use_coupon=test";
					datas += "&&extra_common_param=test";
					datas += "&&out_channel_type=test";
					datas += "&&out_channel_amount=test";
					datas += "&&out_channel_inst=test";
					
					$.ajax({
						url: "/xlem/ticket/alipayNotify",
						data: datas,
						dataType:'json',
						type:'post',
						async:false,
					}).done(function(data, status, xhr) {
						alert(data.msg);
					}).fail(function(xhr, status, error) {  
			        	alert("请求失败！");
					});	*/	
					
					$.ajax({
						url: "/xlem/ticket/returnToAccount",
						data:'limit=5000',
						dataType:'json',
						type:'get',
						async:false,
					}).done(function(data, status, xhr) {
						window.location = data.url;
					}).fail(function(xhr, status, error) {  
						alert("请求失败！");
						window.location = "/xlem/index";
					})	
			}
			},{
				text: "支付遇到问题",
				click: function(){
					$(this).dialog("close");
					alert("请稍后在订单查询中完成对该订单的支付或关闭定单!");
					$.ajax({
						url: "/xlem/ticket/returnToAccount",
						data:'limit=5000',
						dataType:'json',
						type:'get',
						async:false,
					}).done(function(data, status, xhr) {
						window.location = data.url;
					}).fail(function(xhr, status, error) {  
						alert("请求失败！");
						window.location = "/xlem/index";
					})						
				}
			}]
			});
		}else{
			window.location = "/xlem/ticket/returnToAccount";
		}
	}
	
	function removeIndentDetail(ticketIndentDetailId) {
		var datas = "indentDetailId=" + ticketIndentDetailId;  
  
        $.ajax({  
			url: "/xlem/ticket/deleteIndent",
			data:'limit=5000',
			type:'post',
			dataType:'json',
            async : false,  
            data : datas,
		}).done(function(data, status, xhr) {
			resHtml = '<table>';
			resHtml += '<th>门票</th>';
			resHtml += '<th>名称</th>';
			resHtml += '<th>原价</th>';
			resHtml += '<th>平台价</th>';
			resHtml += '<th>日期</th>';
			resHtml += '<th>数量</th>';
			resHtml += '<th>操作</th>';
			
			for (i = 0; i < data.details.length; i++) {
				resHtml += '<tr id="'
				resHtml += data.details[i].indentDetailId;
				resHtml += '">';
				resHtml += '<td></td>';
				resHtml += '<td>';
				resHtml += '<img alt="" src="';
				resHtml += data.details[i].ticket.image;
				resHtml += '" id="ticketImg" align="absmiddle">';
				resHtml += data.details[i].ticket.name;
				resHtml += '</img>';
				resHtml += '</td>';
				resHtml += '<td>';
				resHtml += data.details[i].ticket.price;
				resHtml += '</td>';
				resHtml += '<td>';
				resHtml += data.details[i].ticket.price;
				resHtml += '</td>';
				resHtml += '<td><input class="datepicker" id="" type="text"/></td>';
				resHtml += '<td><input class="spinner" id="spinner" type="text"/></td>';
				resHtml += '<td><a href="javascript:removeIndentDetail(';
				resHtml += parseInt(data.details[i].indentDetailId);
				resHtml += ');">删除</a></td>';
				resHtml += '</tr>';
			}
			resHtml += '</table>';
			document.getElementById("detailTable").innerHTML = resHtml;
			
			$('.datepicker').each(function(){
			    var datepicker = $(this).datepicker();
			    datepicker.datepicker('setDate', new Date());
			});
			
			$('.spinner').each(function(){
			    var spinner = $(this).spinner();
			    spinner.spinner("value", 1);
			});
			initData();
			
		}).fail(function(xhr, status, error) {  
        	alert("请求失败！");
		});
	}
