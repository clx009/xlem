
var ajaxUrlVal,ajaxDataVal,ajaxReturnType,page,showMsg;


$(function() {
	
	ajaxUrlVal=winParam.ajaxUrlVal;
	ajaxDataVal=winParam.ajaxDataVal;
	ajaxReturnType=winParam.ajaxReturnType;
	page=winParam.page;
	
	creatImgBut("saveOrder","img-new/saveOrderBut1.gif","img-new/saveOrderBut.gif");
	$("#saveOrder").unbind('click').click(function(){
		$.ajax({
			url:ajaxUrlVal,
			data:ajaxDataVal,
			dataType:'json',
			type:'post',
			async:false,
			success: function(result){
				if(ajaxReturnType=='hotel'){
					hotelReturn(result,'noPay');
				}else{
					ticketReturn(result,'noPay');
				}
			},
			error:commerror
		});
	});
	
	creatImgBut("bcgPay","img-new/payBut1.gif","img-new/payBut.gif");
	
	$("#bcgPay").unbind('click').click(function(){
		$.ajax({
			url:ajaxUrlVal,
			data:ajaxDataVal+$('input[name="defaultbank"]:checked').val(),
			dataType:'json',
			type:'post',
			async:false,
			success: function(result){
				if(ajaxReturnType=='hotel'){
					hotelReturn(result,'pay');
				}else{
					ticketReturn(result,'pay');
				}
			},
			error:commerror
		});
	});
	
});

function ticketReturn(result,p){
	if(result.exception){//有误时提示,并刷新页面
		initBcfData();
		alert(result.exception);
		return;
	}
	else if(result.success){//购买免费票无需支付时，直接转到订单查询
		toSearchTicketIndent();
		alert(result.success);
		return;
	}
	else if(p=='pay'){//提交支付
		$("#aliBankpay").form('load',result);
		
		var pay_test = result.pay_test;
		
		if("true"==pay_test){
			$("#aliBankpay").attr("action",'http://192.168.5.211:8080/simulationPay/simulatePayAction!gateway.action');
		}else{
			$("#aliBankpay").attr("action",'https://mapi.alipay.com/gateway.do');
		}
		
		$("#aliBankpay").submit();
		
		openDialog('confirmIsPay','支付确认',270,100,'',function(){
		},{
			buttons:[{
				text:'支付完成',
				iconCls:'icon-ok',
				handler:function(){
					toSearchTicketIndent();
				}
			},{
				text:'支付遇到问题',
				handler:function(){
					toSearchTicketIndent();
					alert("请稍后在订单查询中完成对该订单的支付或关闭定单!");
				}
			}]
		});
	}else{
		toSearchTicketIndent();
	}
}

function hotelReturn(result,p){
	if(result.refund){
		$.messager.alert('提示',result.refund,'',function(){
			searchHotel();
		});
	}
	else if(result.success){
		alert(result.success);
	}
	else if(result.exception){
		alert(result.exception);
	}
	else if(result.payType==2 && (typeof(result.p0_Cmd) == "undefined" || result.p0_Cmd=='')){//市场部订单无需到易宝支付
		searchHotel();
		alert("预订成功!");
	}
	else if(p=='pay'){//提交支付
		$("#aliBankpay").form('load',result);
		var pay_test = result.pay_test;
		
		if("true"==pay_test){
			$("#aliBankpay").attr("action",'http://192.168.5.211:8080/simulationPay/simulatePayAction!gateway.action');
		}else{
			$("#aliBankpay").attr("action",'https://mapi.alipay.com/gateway.do');
		}
		var it_b_pay = result.it_b_pay;
		if(parseInt(it_b_pay, 0)<1){
			alert("抱歉，您不能进行本次支付，订单已超时！");
		}else{
			$("#aliBankpay").submit();
		}
		
		openDialog('confirmIsPayDlg','支付确认',270,100,'',function(){
		},{
			buttons:[{
				text:'支付完成',
				iconCls:'icon-ok',
				handler:function(){
					searchHotel();
				}
			},{
				text:'支付遇到问题',
				handler:function(){
					searchHotel();
					alert("请稍后在订单查询中完成对该订单的支付或关闭定单!");
				}
			}]
		});
	}else{
		$.messager.alert('提示','半小时之内未付款将自动取消订单，请及时付款！','',function(){
			searchHotel();
		});
	}
}

function searchHotel(){
	if('confirm'==page){
		toSearchHotelIndent();
	}else{
		window.frames["center"].window.frames["loginInCenter"].toSearchHotelIndent();
	}
	
}