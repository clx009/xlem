$(function() {
	//loadGrid('hotelGrid','');
	
	$.ajax({
		url:'/xlem/hotel/hotelList/{"${hotel.hotelId}"}',
		data:'limit=100',
		dataType:'json',
		type:'get',
		success: function(result){
			
			var roomId = 0;
			
			var roomsHtml = "";
			var roomTypeHtml = {};
			
			$.each(result, function(i, n){
				var rmId = n.roomTypeId;
				if(roomId != rmId){
					roomId = rmId;
					
					roomsHtml += '<table>';
					roomsHtml += '<tr>';	
					roomsHtml += '<th>房型</th>';
					roomsHtml += '<th>名称</th>';
					roomsHtml += '<th>早餐</th>';
					roomsHtml += '<th>宽带</th>';	
					roomsHtml += '<th>政策</th>';
					roomsHtml += '<th>平台价</th>';
					roomsHtml += '<th>购买数量</th>';
					roomsHtml += '<th>操作</th>';
					
					$.ajax({
						url:'/xlem/hotel/roomPrices/{"${rmId}"}',
						data:'limit=100',
						dataType:'json',
						type:'get',
						success: function(prices) {
							var minPrice = 0.0;
							if (prcies.length > 0) {
								minPrice = prices[0].price;
							}
							
							// 此处放置房间活动查询
							
							roomsHtml += '<tr>';
							roomsHtml += '<td>';
							roomsHtml += '<span class="roomImg">';
							roomsHtml += '<img src="images/jd' + hotel.hotelId + '/room' + rmId + '.gif" width="30" class="roomImg" height="30" onclick="book('+rmId+');" title="预订"/>';
							roomsHtml += '</span>';
							roomsHtml += '<span class="roomDesc">';
							roomsHtml += n.typeName;
							roomsHtml += '</span>';
							roomsHtml += '</td>';
							roomsHtml += '<td>';
							roomsHtml += n.typeName;
							roomsHtml += '</td>';
							roomsHtml += '<td>';
							roomsHtml += '</td>';
							roomsHtml += '<td>';
							roomsHtml += '</td>';
							roomsHtml += '<td>';
							roomsHtml += '</td>';
							roomsHtml += '<td>';
							roomsHtml += minPrice;
							roomsHtml += '</td>';
							roomsHtml += '<td>';
							roomsHtml += '<input class="dropdown" type="dropdown" id="buyNum">';
							roomsHtml += '</td>';
							roomsHtml += '<td>';
							roomsHtml += '<input class="book" type = "button" id="book">';
							roomsHtml += '</td>';
							roomsHtml += '</tr>';
						
							// price table
							roomsHtml += '<table>';
							roomsHtml += '<tr>';
							roomsHtml += '<c:forEach items="' + prices + '" var="price" varStatus="status"';
							roomsHtml += 'c:if test="${status.current == 7}"';
							roomsHtml += '<tr>';
							roomsHtml += '</c:if>';
							roomsHtml += '<td>';
							roomsHtml += '<span>' + '${price}' + '</span>';
							roomsHtml += '</td>';
							roomsHtml += 'c:if test="${status.current == 7}"';
							roomsHtml += '</tr>';
							roomsHtml += '</c:if>';
							roomsHtml += '</tr>';
							roomsHtml += '</table>';
						}});
					}
				
			});
			$("#hotTable").html(hotelsHtml);
		},
		error:commerror
	});
	
	$('#showImg').window({
		title:'酒店图片展示',
		iconCls:'icon-tip',
		minimizable:false,
		maximizable:false,
		collapsible:false,
		resizable:false,
		//href:'jsp/home/hotelImgShow.html',
		draggable:true,
		closed:true,
		onClose:function(){
			$('#showImgTable').html("");
			hotId="";
			hotName = "酒店";
		}
	});
});

var hotId;
var hotName;

function showImg(hoId,hoName){
	hotId = hoId;
	hotName = hoName;
	window.open('jumpOverAction!hotelImgPage.action?hotId='+hotId+'&hotName='+encodeURIComponent(hotName),target='imgP');
	 
}

var  roomReservationHotelId = "";

/**
 * 进入对应酒店房型选择页面
 * @param hotelId
 */
function book(hotelId){
	roomReservationHotelId = hotelId;
	if(hotelId!=""){
		parent.loadhref("hotelAction!queryRoomType.action",'',{hotelId:hotelId});
	}
	else{
		alert("酒店选择有误，请重新选择!");
	}
}
