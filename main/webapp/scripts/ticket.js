    var orderby = "order by id";  
    $(document).ready(function() { 
	    if (document.getElementById("pageeee") != null) {
	    	initData(0, "/xlem/ticket/getData");
	    	initPager("/xlem/ticket/getData");
	    }
	    
    	if (document.getElementById("suite_pageeee") != null) {
	    	initSuiteData(0, "/xlem/ticket/getSuiteData");
	    	initSuitePager("/xlem/ticket/getSuiteData");
	    }
    })
    
    function initData(pageindx, url) {  
        var tbody = "";  
        var pageCount = "";  
        var arg = $("#fn").val();  
        var datas = "offset=" + (pageindx);  
        if (arg != "") {  
            datas += "&&conditions=" + (arg);  
        }  
        $.ajax({  
			url: url,
			data:'limit=5000',
			type:'get',
			dataType:'json',
            async : false,  
            data : datas,
		}).done(function(data, status, xhr) {
			resHtml = "";
			var needEndDiv;
			for (var i = 0; i < data.tickets.length; i++) {
				if (i % 3 == 0) {
					resHtml += '<div class="row" id="tickets">';
					needEndDiv = 1;
				}
				resHtml += '<div class="col-md-4 content">';
				resHtml += '<img class="image" src="';
				resHtml += data.tickets[i].image;
				resHtml += '" ';
				resHtml += 'alt="图片未显示"></img>';
				resHtml += '<a class="booking" href=';
				resHtml += '"/xlem/ticket/booking?name=';
				resHtml += data.tickets[i].name;
				resHtml += '">现在预订</a>';
				resHtml += '<a class="shopcart" href=';
				resHtml += '"/xlem/ticket/enterShopcart?name=';
				resHtml += data.tickets[i].name;
				resHtml +='">加入购物车</a>';
				resHtml += '</div>';
				
				if ((i + 1) % 3 == 0) {
					resHtml += '</div>';
					needEndDiv = 0;
				}
			}
			
			if (needEndDiv == 1) {
				resHtml += '</div>';
			}

			resHtml += '<div id="Pager" style="text-align:left">';
		    resHtml += '</div>';
			
			// $("pageeee").html(resHtml);
			document.getElementById("pageeee").innerHTML = resHtml;
            document.getElementById("total").value = data.num;
            document.getElementById("offset").value = pageindx;
            
            initPager("/xlem/ticket/getData");
            
		}).fail(function(xhr, status, error) {  
        	alert("请求失败！");
		})	
  
 
    }
    
    function updatePager(offset, total) {
    	if (offset < 0) {
    		alert("已经是第一页");
    		return;
    	}
    	
    	if (offset >= total) {
    		alert("已经是最后一页");
    		return;
    	}
    	
    	initData(offset, "/xlem/ticket/getData");
    }
    
    function initPager(url) {
    	var total = document.getElementById("total").value;
    	var offset = document.getElementById("offset").value;
    	
        var pagerHtml = "";
        pagerHtml += '<nav aria-label="Page navigation">';
        pagerHtml += '<ul class="pagination">';
        pagerHtml += '<li>';
        pagerHtml += '<a href="javascript:updatePager(';
        pagerHtml += offset - 9;
        pagerHtml += ',';
        pagerHtml += total;
        pagerHtml += ');" ';
        pagerHtml += 'aria-label="Previous">';
        pagerHtml += '<span aria-hidden="true">&#171;</span>';
        pagerHtml += '</a>';
        pagerHtml += '</li>';
        
        for (i = 0; i < Math.ceil(total / 9); i++) {
        	pagerHtml += '<li><a href="javascript:updatePager(';
        	pagerHtml += i * 9;
        	pagerHtml += ',';
        	pagerHtml += total;
        	pagerHtml += ');" >';
        	pagerHtml += i + 1;
        	pagerHtml += '</a></li>';
        }
        
        pagerHtml += '<li>';
        pagerHtml += '<a href="javascript:updatePager(';
        pagerHtml += parseInt(offset) + 9;
        pagerHtml += ',';
        pagerHtml += total;
        pagerHtml += ');" ';
        pagerHtml += 'aria-label="Next">';
        pagerHtml += '<span aria-hidden="true">&#187;</span>';
        pagerHtml += '</a>';
        pagerHtml += '</li>';
        pagerHtml += '</ul>';
        pagerHtml += '</nav>';
        
		document.getElementById("Pager").innerHTML = pagerHtml;

    }
    
    function initSuiteData(pageindx, url) {  
        var tbody = "";  
        var pageCount = "";  
        var datas = "offset=" + (pageindx);  
        
        $.ajax({  
			url: url,
			data:'limit=5000',
			type:'get',
			dataType:'json',
            async : false,  
            data : datas,
		}).done(function(data, status, xhr) {
			resHtml = "";
			var needEndDiv;
			for (var i = 0; i < data.tickets.length; i++) {
				if (i % 3 == 0) {
					resHtml += '<div class="row" id="tickets">';
					needEndDiv = 1;
				}
				resHtml += '<div class="col-md-4 content">';
				resHtml += '<img class="image" src="';
				resHtml += data.tickets[i].image;
				resHtml += '" ';
				resHtml += 'alt="图片未显示"></img>';
				resHtml += '<a class="booking" href=';
				resHtml += '"/xlem/ticket/booking?name=';
				var relay = data.tickets[i].name.replace(/\+/g, "%2B").replace(/\"/g,'%22').replace(/\'/g, '%27').replace(/\//g,'%2F');
				resHtml += relay;
				resHtml += '">现在预订</a>';
				resHtml += '<a class="shopcart" href=';
				resHtml += '"/xlem/ticket/enterShopcart?name=';
				resHtml += relay;
				resHtml +='">加入购物车</a>';
				resHtml += '</div>';
				
				if ((i + 1) % 3 == 0) {
					resHtml += '</div>';
					needEndDiv = 0;
				}
			}
			
			if (needEndDiv == 1) {
				resHtml += '</div>';
			}

			resHtml += '<div id="Suite_Pager" style="text-align:left">';
		    resHtml += '</div>';
			
			// $("pageeee").html(resHtml);
			document.getElementById("suite_pageeee").innerHTML = resHtml;
            document.getElementById("suite_total").value = data.num;
            document.getElementById("suite_offset").value = pageindx;
            
            initSuitePager("/xlem/ticket/getSuiteData");
            
		}).fail(function(xhr, status, error) {  
        	alert("请求失败！");
		})	
  
 
    }
    
    function updateSuitePager(offset, total) {
    	if (offset < 0) {
    		alert("已经是第一页");
    		return;
    	}
    	
    	if (offset >= total) {
    		alert("已经是最后一页");
    		return;
    	}
    	
    	initSuiteData(offset, "/xlem/ticket/getSuiteData");
    }
    
    function initSuitePager(url) {
    	var total = document.getElementById("suite_total").value;
    	var offset = document.getElementById("suite_offset").value;
    	
        var pagerHtml = "";
        pagerHtml += '<nav aria-label="Page navigation">';
        pagerHtml += '<ul class="pagination">';
        pagerHtml += '<li>';
        pagerHtml += '<a href="javascript:updateSuitePager(';
        pagerHtml += offset - 9;
        pagerHtml += ',';
        pagerHtml += total;
        pagerHtml += ');" ';
        pagerHtml += 'aria-label="Previous">';
        pagerHtml += '<span aria-hidden="true">&#171;</span>';
        pagerHtml += '</a>';
        pagerHtml += '</li>';
        
        for (i = 0; i < Math.ceil(total / 9); i++) {
        	pagerHtml += '<li><a href="javascript:updateSuitePager(';
        	pagerHtml += i * 9;
        	pagerHtml += ',';
        	pagerHtml += total;
        	pagerHtml += ');" >';
        	pagerHtml += i + 1;
        	pagerHtml += '</a></li>';
        }
        
        pagerHtml += '<li>';
        pagerHtml += '<a href="javascript:updateSuitePager(';
        pagerHtml += parseInt(offset) + 9;
        pagerHtml += ',';
        pagerHtml += total;
        pagerHtml += ');" ';
        pagerHtml += 'aria-label="Next">';
        pagerHtml += '<span aria-hidden="true">&#187;</span>';
        pagerHtml += '</a>';
        pagerHtml += '</li>';
        pagerHtml += '</ul>';
        pagerHtml += '</nav>';
        
		document.getElementById("Suite_Pager").innerHTML = pagerHtml;

    }
    
    function showTicket(name) {
    	window.location.href="/xlem/ticket/showInfo";
    }
    
    function bookTicket(name) {
    	window.location.href="/xlem/ticket/book";
    }
    
    $(document).ready(function() {
    	// 1. 注册事件
    	$(".spinner").change(function(){
    		//2. 验证数据的有效性
    		var number = this.value; //也可以使用$(this).val();
    		//isNaN(number)表示若number不是数字就返回真
    		if(!isNaN(number) && parseInt(number)==number && number>0){ 
    			//如果合法，同步更新的数
    			$(this).attr("lang", number);
    			//找到当前标签中第一个是tr的父节点，然后拿到属性为lang的值，也就是商品的id
    			var pid = $(this).parents("tr:first").attr("lang");
    			//发送Ajax请求，传输当前的数量与商品的id，返回修改数量后的总价格
    			$.post("/ticket/updatePrice", 
    					{number:number, 'product.id':pid},
    					function(total) { 
    						var totalID = "total";
    						totalID.concat(pid);
    						document.getElementById(totalID).html(total); //所有商品小计
    						updateSumPrice();
    					}, "text");
    		} else {
    			//如果非法，还原为刚刚合法的数
    			this.value = $(this).attr("lang");
    		}
    	})
    });     
    
    function updateSumPrice() {
    	var sum = 0.00;
    	var ticketOrders = '${tickets}';
    	for (var i = 0; i < ticketOrders.size(); i++) {
    		var pid = getOrderId(ticketOrders[i].idTicketOrder);
			var totalID = "total";
			totalID.concat(pid);
			sum += document.getElementById(totalID).value(); //所有商品小计
    	}

    	var hotelOrders = '${hotels}';
    	for (var i = 0; i < hotelOrders.size(); i++) {
    		var pid = getOrderId(hotelOrders[i].idHotelOrder);
			var totalID = "total";
			totalID.concat(pid);
			sum += document.getElementById(totalID).value(); //所有商品小计
    	}
     	
    	$("#sumPrice").html(sum.toFixed(2));
    }
    
    function getOrderId(thid) {
     	var ticketOrders = '${tickets}';
     	var orders = '${orders}';
    	for (var i = 0; i < ticketOrders.size(); i++) {
    		var tid = ticketOrders[i].idTicketOrder;
    		for (var j = 0; j < orders.size(); j++) {
    			if (orders[j].detailId == tid) {
    				return orders[j].idOrder;
    			}
    		}
    	}

    	var hotelOrders = '${hotels}';
    	for (var i = 0; i < hotelOrders.size(); i++) {
    		var hid = getOrderId(hotelOrders[i].idHotelOrder);
    		for (var j = 0; j < orders.size(); j++) {
    			if (orders[j].detailId == hid) {
    				return orders[j].idOrder;
    			}
    		}
    	}
    	
    	return "-1";
    	
    }
    
	$(document).on('click', "#toBalance", function () {
		var url ="/xlem/ticket/shopcartBalance";
		window.location = url;
	});
	
	$(document).on('click', "#shopcartReturn", function () {
		history.go(-1);
	});
	
	$(document).on('click', '#returnTickets', function () {
		var url = "/xlem/ticket/list";
		window.location = url;
	})
	
	$(document).on('click', '#returnIndex', function () {
		var url = "/xlem/index";
		window.location = url;
	})