	function viewPicture(travelAgencyId, fname) {
		var url = "/xlem/ts_account/showImage/?travelAgencyId=" + travelAgencyId;
		url += "&fname=" + fname;
		window.location = url;
	}
	
	$(document).on('click', "#tsInfoReturn", function () {
		history.go(-1);
	});
	
	$(document).on('click', "#tsImageReturn", function () {
		history.go(-1);
	});

	$(document).on('click', "#tsPasswdUpdate", function() {
	    var originPasswd = document.getElementById("originPasswd").value;
	    var newPasswd = document.getElementById("newPasswd").value;
	    var rePasswd = document.getElementById("rePasswd").value;
	    
	    if (newPasswd != rePasswd) {
	    	alert("前后输入的密码不匹配");
	    	return;
	    }
	    
		var datas = "originPasswd=" + originPasswd;
		datas += "&&newPasswd=" + newPasswd;
  
        $.ajax({  
			url: "/xlem/ts_account/updateTsPasswd",
			data:'limit=5000',
			type:'post',
			dataType:'json',
            async : false,  
            data : datas,
		}).done(function(data, status, xhr) {
			if (data.msg == "wrong") {
				alert("原始密码错误");
			}
			if (data.msg == "success") {
				alert("密码修改成功");
			}
		}).fail(function(xhr, status, error) {  
        	alert("请求失败！");
		})			
	})
	
	$(document).on('click', "#rewardReturn", function () {
		history.go(-1);
	});
	
	$(document).on('click', "#cashReturn", function () {
		history.go(-1);
	});	
	
	function ticketIndentStatus() {
		var indentTable = document.getElementById("indentTable");
		var trs = indentTable.getElementsByTagName("tr");
		
		// Iterate over the links
		for (var i = 1, len = trs.length, tr; i < len; ++i) {
		    tr = trs[i];
		    var tds = tr.getElementsByTagName("td");
		    var status = tds[6].innerText;
		    var text = status;
		    switch (status) {
		    case "00":
		    	text = "待审核";
		    	break;
		    
		    case "01":
		    	text = "已审核";
		    	break;
		    
		    case "02":
		    	text = "已使用";
		    	break;
		    	
		    case "08":
		    	text = "已退票";
		    	break;
		    
		    case "04":
		    	text = "已删除";
		    	break;
		    		
		    }
		    tds[6].innerText = text;
		}
	}
	
    function initData(pageindx, url) {  
        var tbody = "";  
        var pageCount = "";  
        var datas = "offset=" + (pageindx);  
        
        $.ajax({  
			url: url,
			data:'limit=5000',
			type:'get',
			dataType:'json',
            async : false,  
            data : datas,
		}).done(function(data, status, xhr) {
			resHtml = "";
			var needEndDiv;
			var entryCount = 0;
			resHtml = '<table id="indentTable" style="width:100%">';
			resHtml += '<tr>';
			resHtml += '<th>订单编号</th>';
			resHtml += '<th>名称</th>';
			resHtml += '<th>预订日期</th>';
			resHtml += '<th>产品内容</th>';
			resHtml += '<th>行程有效日期</th>';
			resHtml += '<th>订单金额</th>';
			resHtml += '<th>订单状态</th>';
			resHtml += '<th>操作</th>';
			resHtml += '</tr>';
			
			for (var i = 0; i < data.ticketIndents.length; i++) {
				for (var j = 0; j < data.ticketIndents[i].details.length; j++) {
					if (entryCount < 10) {
						resHtml += '<tr>';
						resHtml += '<td style="width:10%">';
						resHtml += data.ticketIndents[i].indentId;
						resHtml += '</td>';
						resHtml += '<td style="width:10%">';
						resHtml += data.ticketIndents[i].details[j].ticketName;
						resHtml += '</td>';
						resHtml += '<td style="width:5%">';
						var bookDate = new Date(data.ticketIndents[i].bookDate);
						var datestring = bookDate.getFullYear() + "-" + (bookDate.getMonth() + 1) + "-" + bookDate.getDate();
						resHtml += datestring;
						resHtml += '</td>';
						resHtml += '<td>';
						resHtml += data.ticketIndents[i].details[j].intro;
						resHtml += '</td>';
						resHtml += '<td style="width:5%">';
						var useDate = new Date(data.ticketIndents[i].details[j].useDate)
						datestring = useDate.getFullYear() + "-" + (useDate.getMonth() + 1) + "-" + useDate.getDate();
						resHtml += datestring;
						resHtml += '</td>';
						resHtml += '<td style="width:10%">';
						resHtml += data.ticketIndents[i].details[j].subtotal;
						resHtml += '</td>';
						resHtml += '<td name="ticketIndentStatus" style="width:8%">';
						resHtml += data.ticketIndents[i].details[j].status;
						resHtml += '</td>';
						resHtml += '<td style="width:15%">';
						resHtml += '<a href="javascript:removeTicketIndent(';
						resHtml += parseInt(data.ticketIndents[i].indentId);
						resHtml += ',';
						resHtml += parseInt(data.ticketIndents[i].details[j].indentDetailId);
						resHtml += ');">删除</a>';
						resHtml += ' | ';
						resHtml += '<a href="javascript:refundTicketIndent(';
						resHtml += parseInt(data.ticketIndents[i].indentId);
						resHtml += ','
						resHtml += parseInt(data.ticketIndents[i].details[j].indentDetailId);
						resHtml += ');">退票</a>';
						resHtml += '</td>';
						resHtml += '</tr>';
						
						entryCount++;
					}
				}
			}
			resHtml += '</table>';
			
			resHtml += '<div id="Pager" style="text-align:left">';
		    resHtml += '</div>';

			// $("indentDiv").html(resHtml);  
			document.getElementById("indentDiv").innerHTML = resHtml;
            document.getElementById("total").value = data.num;
            document.getElementById("offset").value = pageindx;
            
            initPager("/xlem/ts_account/getTicketIndentData");
            ticketIndentStatus();
		}).fail(function(xhr, status, error) {  
        	alert("请求失败！");
		})	
    }
    
    function updatePager(offset, total) {
    	if (offset < 0) {
    		alert("已经是第一页");
    		return;
    	}
    	
    	if (offset >= total) {
    		alert("已经是最后一页");
    		return;
    	}
    	
    	initData(offset, "/xlem/ts_account/getTicketIndentData");
    }
    
    $(document).ready(function() {  
    	initData(0, "/xlem/ts_account/getTicketIndentData");
    	initPager("/xlem/ts_account/getTicketIndentData");
		ticketIndentStatus();
    })
    
    function initPager(url) {
    	var total = document.getElementById("total").value;
    	var offset = document.getElementById("offset").value;
    	
        var pagerHtml = "";
        pagerHtml += '<nav aria-label="Page navigation">';
        pagerHtml += '<ul class="pagination">';
        pagerHtml += '<li>';
        pagerHtml += '<a href="javascript:updatePager(';
        pagerHtml += offset - 10;
        pagerHtml += ',';
        pagerHtml += total;
        pagerHtml += ');" ';
        pagerHtml += 'aria-label="Previous">';
        pagerHtml += '<span aria-hidden="true">&#171;</span>';
        pagerHtml += '</a>';
        pagerHtml += '</li>';
        
        for (i = 0; i < Math.ceil(total / 10); i++) {
        	pagerHtml += '<li><a href="javascript:updatePager(';
        	pagerHtml += i * 10;
        	pagerHtml += ',';
        	pagerHtml += total;
        	pagerHtml += ');" >';
        	pagerHtml += i + 1;
        	pagerHtml += '</a></li>';
        }
        
        pagerHtml += '<li>';
        pagerHtml += '<a href="javascript:updatePager(';
        pagerHtml += parseInt(offset) + 10;
        pagerHtml += ',';
        pagerHtml += total;
        pagerHtml += ');" ';
        pagerHtml += 'aria-label="Next">';
        pagerHtml += '<span aria-hidden="true">&#187;</span>';
        pagerHtml += '</a>';
        pagerHtml += '</li>';
        pagerHtml += '</ul>';
        pagerHtml += '</nav>';
        
		document.getElementById("Pager").innerHTML = pagerHtml;

    }
    
	function removeTicketIndent (ticketIndentId, indentDetailId) {
		openDialog("confirmDelete", "确认删除", 270, 100, "请确认要删除订单", function(){
		},{
		buttons:[{
			text: "删除",
			click: function() {
				$(this).dialog("close");
		       
				var relay = ticketIndentId.toString();
				var strTicketIndentId = "";
				for (var i = 0; i < 32 - relay.length; i++) {
					strTicketIndentId += "0";
				}
				strTicketIndentId += relay;
				relay = indentDetailId.toString();
				var strIndentDetailId = "";
				for (var i = 0; i < 11 - relay.length; i++) {
					strIndentDetailId += "0";
				}
				strIndentDetailId += relay;
				
				var datas = "ticketIndentId=" + strTicketIndentId;
				datas += "&&indentDetailId=" + strIndentDetailId;
		        
		        $.ajax({  
					url: "/xlem/ticket/removeIndent",
					data:'limit=5000',
					type:'post',
					dataType:'json',
		            async : false,  
		            data : datas,
				}).done(function(data, status, xhr) {
					if (data.msg == "success") {
						$.ajax({
							url: "/xlem/ts_account/refreshTicketIndent",
							data: 'limit=5000',
							dataType: 'json',
							type: 'post',
							async: false,
						}).done(function(data, status, xhr) {
							if (data.msg == "success") {
								var offset = document.getElementById("offset").value;
						    	initData(offset, "/xlem/ts_account/getTicketIndentData");
						    	initPager("/xlem/ts_account/getTicketIndentData");
								ticketIndentStatus();
								couponStatus();
							}
							
							if (data.msg == "unlogined") {
								window.location = "/xlem/index";
							}
						}).fail(function(xhr, status, error) {  
				        	alert("请求失败！");
						});
					}
					if (data.msg == "invalid") {
						alert("订单不存在");
						return;
					}
				}).fail(function(xhr, status, error) {  
		        	alert("请求失败！");
				})			
			}
		},{
			text: "放弃",
			click: function(){
				$(this).dialog("close");
				return;
			}
		}]
		});
	}
	
	function refundTicketIndent (ticketIndentId, indentDetailId) {
		openDialog("confirmRefund", "确认退票", 270, 100, "请确认要退订该订单", function(){
		},{
		buttons:[{
			text: "退票",
			click: function() {
				$(this).dialog("close");
		        
				var relay = ticketIndentId.toString();
				var strTicketIndentId = "";
				for (var i = 0; i < 32 - relay.length; i++) {
					strTicketIndentId += "0";
				}
				strTicketIndentId += relay;
				relay = indentDetailId.toString();
				var strIndentDetailId = "";
				for (var i = 0; i < 11 - relay.length; i++) {
					strIndentDetailId += "0";
				}
				strIndentDetailId += relay;
				
				var datas = "ticketIndentId=" + strTicketIndentId;
				datas += "&&indentDetailId=" + strIndentDetailId;
		        
		        $.ajax({  
					url: "/xlem/ticket/refundIndent",
					data:'limit=5000',
					type:'post',
					dataType:'json',
		            async : false,  
		            data : datas,
				}).done(function(data, status, xhr) {
					if (data.msg == "success") {
						var offset = document.getElementById("offset").value;
				    	initData(offset, "/xlem/ts_account/getTicketIndentData");
				    	initPager("/xlem/ts_account/getTicketIndentData");
						ticketIndentStatus();
						couponStatus();
					}
					if (data.msg == "invalid") {
						alert("订单不存在");
						return;
					}
				}).fail(function(xhr, status, error) {  
		        	alert("请求失败！");
				})			
			}
		},{
			text: "放弃",
			click: function(){
				$(this).dialog("close");
				return;
			}
		}]
		});
	}
	
	$(document).on('click', "#rechargeReturn", function () {
		history.go(-1);
	});
	
	$(document).on('click', "#rechargeConfirm", function () {
		var money = document.getElementById("rechargeVal").value;
		if (!isNaN(money)) {
			ajaxUrlVal = '/xlem/ts_account/getRechargeParam';
			ajaxDataVal='recharge=' + money + '&defaultbank=';
			var aliBank = "0";
			if(aliBank=="1"){
				popup('bankPage','/xlem/pay/bankSelect','选择网上银行',800,600,
				{
					ajaxUrlVal:ajaxUrlVal,
					ajaxDataVal:ajaxDataVal,
					ajaxReturnType:'recharge'
				},
				function(){});
			}else{
				$.ajax({
					url:ajaxUrlVal,
					data:ajaxDataVal,
					dataType:'json',
					type:'get',
					async:false,
				}).done(function(data, status, xhr) {
					rechargeReturn(data[0], 'pay');
				}).fail(function(xhr, status, error) {  
					alert("请求失败！");
				})
			}			
		}
		else {
			alert("请正确输入充值金额");
			return;
		}
	});
	
	function rechargeReturn(result, p){
		if(p=='pay'){//提交支付
			$("#aliBankpay").form('load', result);
			var payKeys = Object.keys(result);
			var inputElements = document.getElementsByTagName('input');
			for (i = 0; i < payKeys.length; i++) {
				for (j = 0; j < inputElements.length; j++) {
					if (inputElements[j].name == payKeys[i]) {
						inputElements[j].value = result[payKeys[i]];
						break;
					}
				}
			}
  		
			var pay_test = result.pay_test;
		
			if("true"==pay_test){
				$("#aliBankpay").attr("action",'http://192.168.5.211:8080/simulationPay/simulatePayAction!gateway.action');
			}else{
				$("#aliBankpay").attr("action",'https://mapi.alipay.com/gateway.do');
			}
		
			$("#aliBankpay").submit();
			
			openDialog("confirmPay", "支付确认", 270, 100, "请确认支付已完成", function(){
			},{
			buttons:[{
				text: "支付完成",
				click: function() {
					$(this).dialog("close");
					// test code for alipay
					/*var out_trade_no = document.getElementsByName("out_trade_no")[0].value;
					var trade_status = "TRADE_SUCCESS";
					
					var datas = "out_trade_no=" + out_trade_no;
					datas += "&&trade_status=" + trade_status;
					datas += "&&notify_time=test";
					datas += "&&notify_type=test";
					datas += "&&notify_id=test";
					datas += "&&sign_type=test";
					datas += "&&sign=test";
					datas += "&&subject=test";
					datas += "&&payment_type=test";
					datas += "&&trade_no=test";
					datas += "&&gmt_create=test";
					datas += "&&gmt_payment=test";
					datas += "&&gmt_close=test";
					datas += "&&refund_status=test";
					datas += "&&gmt_refund=test";
					datas += "&&seller_email=test";
					datas += "&&buyer_email=test";
					datas += "&&seller_id=test";
					datas += "&&buyer_id=test";
					datas += "&&price=test";
					datas += "&&total_fee=test";
					datas += "&&quantity=test";
					datas += "&&body=test";
					datas += "&&discount=test";
					datas += "&&is_total_fee_adjust=test";
					datas += "&&use_coupon=test";
					datas += "&&extra_common_param=test";
					datas += "&&out_channel_type=test";
					datas += "&&out_channel_amount=test";
					datas += "&&out_channel_inst=test";
					
					$.ajax({
						url: "/xlem/ticket/alipayNotify",
						data: datas,
						dataType:'json',
						type:'post',
						async:false,
					}).done(function(data, status, xhr) {
						alert(data.msg);
					}).fail(function(xhr, status, error) {  
			        	alert("请求失败！");
					});	*/	
					
					return;
			}
			},{
				text: "支付遇到问题",
				click: function(){
					$(this).dialog("close");
					alert("请稍后继续充值!");
					return;
				}
			}]
			});
		}else{
			return;
		}
	}
	
	$(document).on('click', '#returnAccount', function () {
		var url = "/xlem/ts_account/entry";
		window.location = url;
	})
	
	$(document).on('click', '#returnIndex', function () {
		var url = "/xlem/index";
		window.location = url;
	})