	function ticketIndentStatus() {
		var indentTable = document.getElementById("indentTable");
		var trs = indentTable.getElementsByTagName("tr");
		
		// Iterate over the links
		for (var i = 1, len = trs.length, tr; i < len; ++i) {
		    tr = trs[i];
		    var tds = tr.getElementsByTagName("td");
		    var status = tds[6].innerText;
		    var text = status;
		    switch (status) {
		    case "00":
		    	text = "未支付";
		    	break;
		    
		    case "01":
		    	text = "已支付";
		    	break;
		    
		    case "02":
		    	text = "已使用";
		    	break;
		    	
		    case "08":
		    	text = "已退票";
		    	break;
		    
		    case "04":
		    	text = "已删除";
		    	break;
		    		
		    }
		    tds[6].innerText = text;
		}
	}

	function removeTicketIndent (ticketIndentId, indentDetailId) {
		openDialog("confirmDelete", "确认删除", 270, 100, "请确认要删除订单", function(){
		},{
		buttons:[{
			text: "删除",
			click: function() {
				$(this).dialog("close");
		       
				var relay = ticketIndentId.toString();
				var strTicketIndentId = "";
				for (var i = 0; i < 32 - relay.length; i++) {
					strTicketIndentId += "0";
				}
				strTicketIndentId += relay;
				relay = indentDetailId.toString();
				var strIndentDetailId = "";
				for (var i = 0; i < 11 - relay.length; i++) {
					strIndentDetailId += "0";
				}
				strIndentDetailId += relay;
				
				var datas = "ticketIndentId=" + strTicketIndentId;
				datas += "&&indentDetailId=" + strIndentDetailId;
		        
		        $.ajax({  
					url: "/xlem/ticket/removeIndent",
					data:'limit=5000',
					type:'post',
					dataType:'json',
		            async : false,  
		            data : datas,
				}).done(function(data, status, xhr) {
					if (data.msg == "success") {
						$.ajax({
							url: "/xlem/account/refreshTicketIndent",
							data: 'limit=5000',
							dataType: 'json',
							type: 'post',
							async: false,
						}).done(function(data, status, xhr) {
							if (data.msg == "success") {
								var offset = document.getElementById("offset").value;
						    	initData(offset, "/xlem/account/getTicketIndentData");
						    	initPager("/xlem/account/getTicketIndentData");
								ticketIndentStatus();
								couponStatus();
							}
							
							if (data.msg == "unlogined") {
								window.location = "/xlem/index";
							}
						}).fail(function(xhr, status, error) {  
				        	alert("请求失败！");
						});
					}
					if (data.msg == "invalid") {
						alert("订单不存在");
						return;
					}
				}).fail(function(xhr, status, error) {  
		        	alert("请求失败！");
				})			
			}
		},{
			text: "放弃",
			click: function(){
				$(this).dialog("close");
				return;
			}
		}]
		});
	}
	
	function refundTicketIndent (ticketIndentId, indentDetailId) {
		openDialog("confirmRefund", "确认退票", 270, 100, "请确认要退订该订单", function(){
		},{
		buttons:[{
			text: "退票",
			click: function() {
				$(this).dialog("close");
		        
				var relay = ticketIndentId.toString();
				var strTicketIndentId = "";
				for (var i = 0; i < 32 - relay.length; i++) {
					strTicketIndentId += "0";
				}
				strTicketIndentId += relay;
				relay = indentDetailId.toString();
				
				var strIndentDetailId = indentDetailId;
				
				var datas = "ticketIndentId=" + strTicketIndentId;
				datas += "&&indentDetailId=" + strIndentDetailId;
		        
		        $.ajax({  
					url: "/xlem/ticket/refundIndent",
					data:'limit=5000',
					type:'post',
					dataType:'json',
		            async : false,  
		            data : datas,
				}).done(function(data, status, xhr) {
					if (data.msg == "notpayed") {
						alert("订单未支付");
						return;
					}
					
					if (data.msg == "timetoclose") {
						alert("距游玩时间24小时内不可退票");
						return;
					}
					
					if (data.msg == "timepassed") {
						alert("已过期订单不可退票");
						return;
					}
					
					if (data.msg == "success") {
						$.ajax({
							url: "/xlem/account/refreshTicketIndent",
							data: 'limit=5000',
							dataType: 'json',
							type: 'post',
							async: false,
						}).done(function(data, status, xhr) {
							if (data.msg == "success") {
								var offset = document.getElementById("offset").value;
						    	initData(offset, "/xlem/account/getTicketIndentData");
						    	initPager("/xlem/account/getTicketIndentData");
								ticketIndentStatus();
								couponStatus();
							}
							
							if (data.msg == "unlogined") {
								window.location = "/xlem/index";
							}
						}).fail(function(xhr, status, error) {  
				        	alert("请求失败！");
						});
					}
					
					if (data.msg == "fail") {
						alert("退票失败，请稍后再试");
						return;
					}
					
					if (data.msg == "invalid") {
						alert("订单不存在");
						return;
					}
				}).fail(function(xhr, status, error) {  
		        	alert("请求失败！");
				})			
			}
		},{
			text: "放弃",
			click: function(){
				$(this).dialog("close");
				return;
			}
		}]
		});
	}
	
	$(document).on('click', "#touristInfoUpdate", function() {
	    var name = document.getElementById("name").value;
	    var mobilePhone = document.getElementById("mobilePhone").value;
	    var email = document.getElementById("email").value;
	    
		var datas = "name=" + name;
		datas += "&&mobilePhone=" + mobilePhone;
		datas += "&&email=" + email;
  
        $.ajax({  
			url: "/xlem/account/updateTouristInfo",
			data:'limit=5000',
			type:'post',
			dataType:'json',
            async : false,  
            data : datas,
		}).done(function(data, status, xhr) {
			if (data.msg == "success") {
				alert("更新成功");
			}
		}).fail(function(xhr, status, error) {  
        	alert("请求失败！");
		})			
	})
	
	$(document).on('click', "#touristPasswdUpdate", function() {
	    var originPasswd = document.getElementById("originPasswd").value;
	    var newPasswd = document.getElementById("newPasswd").value;
	    var rePasswd = document.getElementById("rePasswd").value;
	   
	    if (newPasswd == "") {
	    	alert(" 新密码不可为空!");
	    	return;
	    }
	    
	    if (newPasswd != rePasswd) {
	    	alert("前后输入的密码不匹配");
	    	return;
	    }
	    
		var datas = "originPasswd=" + originPasswd;
		datas += "&&newPasswd=" + newPasswd;
  
        $.ajax({  
			url: "/xlem/account/updateTouristPasswd",
			data:'limit=5000',
			type:'post',
			dataType:'json',
            async : false,  
            data : datas,
		}).done(function(data, status, xhr) {
			if (data.msg == "wrong") {
				alert("原始密码错误");
			}
			if (data.msg == "success") {
				alert("密码修改成功");
			}
		}).fail(function(xhr, status, error) {  
        	alert("请求失败！");
		})			
	})
	
	function couponStatus() {
		var couponTable = document.getElementById("couponTable");
		if (couponTable == null) {
			return;
		}
		var trs = couponTable.getElementsByTagName("tr");
		
		// Iterate over the links
		for (var i = 1, len = trs.length, tr; i < len; ++i) {
		    tr = trs[i];
		    var tds = tr.getElementsByTagName("td");
		    var applyScale = tds[2].innerText;
		    var text = "";
		    switch (applayScale) {
		    case "00":
		    	text = "门票及项目票";
		    	break;
		    	
		    case "01":
		    	text = "酒店";
		    	break;
		    }
		    tds[2].innerText = text;
		    
		    var effectDate = tds[4].innerText;
		    if (effectDate == "") {
		    	var today = new Date();
		    	var expireDate = new Date(effectDate);
	
		    	if (expireDate < today) {
		    		text = "已过期";
		    	}
		    	else {
		    		text = "未使用";
		    	}
		    }
		    else {
		    	text = "已使用";
		    }
		    
		    tds[4].innerText = text;
		}
	}

    $(document).ready(function() {  
    	initData(0, "/xlem/account/getTicketIndentData");
    	initPager("/xlem/account/getTicketIndentData");
		ticketIndentStatus();
		couponStatus();
    })
    
    function initData(pageindx, url) {  
        var tbody = "";  
        var pageCount = "";  
        var datas = "offset=" + (pageindx);  
        
        $.ajax({  
			url: url,
			data:'limit=5000',
			type:'get',
			dataType:'json',
            async : false,  
            data : datas,
		}).done(function(data, status, xhr) {
			resHtml = "";
			var needEndDiv;
			var entryCount = 0;
			resHtml = '<table id="indentTable" style="width:100%">';
			resHtml += '<tr>';
			resHtml += '<th>订单编号</th>';
			resHtml += '<th>名称</th>';
			resHtml += '<th>预订日期</th>';
			resHtml += '<th>产品内容</th>';
			resHtml += '<th>行程有效日期</th>';
			resHtml += '<th>订单金额</th>';
			resHtml += '<th>订单状态</th>';
			resHtml += '<th>操作</th>';
			resHtml += '</tr>';
			
			for (var i = 0; i < data.ticketIndents.length; i++) {
				for (var j = 0; j < data.ticketIndents[i].details.length; j++) {
					if (entryCount < 10) {
						resHtml += '<tr>';
						resHtml += '<td style="width:10%">';
						resHtml += data.ticketIndents[i].indentId;
						resHtml += '</td>';
						resHtml += '<td style="width:10%">';
						resHtml += data.ticketIndents[i].details[j].ticketName;
						resHtml += '</td>';
						resHtml += '<td style="width:5%">';
						var bookDate = new Date(data.ticketIndents[i].bookDate);
						var datestring = bookDate.getFullYear() + "-" + (bookDate.getMonth() + 1) + "-" + bookDate.getDate();
						resHtml += datestring;
						resHtml += '</td>';
						resHtml += '<td>';
						resHtml += data.ticketIndents[i].details[j].intro;
						resHtml += '</td>';
						resHtml += '<td style="width:5%">';
						var useDate = new Date(data.ticketIndents[i].details[j].useDate)
						datestring = useDate.getFullYear() + "-" + (useDate.getMonth() + 1) + "-" + useDate.getDate();
						resHtml += datestring;
						resHtml += '</td>';
						resHtml += '<td style="width:10%">';
						resHtml += data.ticketIndents[i].details[j].subtotal;
						resHtml += '</td>';
						resHtml += '<td name="ticketIndentStatus" style="width:8%">';
						resHtml += data.ticketIndents[i].details[j].status;
						resHtml += '</td>';
						resHtml += '<td style="width:15%">';
						resHtml += '<a href="javascript:removeTicketIndent(';
						resHtml += parseInt(data.ticketIndents[i].indentId);
						resHtml += ',';
						resHtml += parseInt(data.ticketIndents[i].details[j].indentDetailId);
						resHtml += ');">删除</a>';
						if (data.ticketIndents[i].details[j].status != "08") { 
							resHtml += ' | ';
							resHtml += '<a href="javascript:refundTicketIndent(';
							resHtml += parseInt(data.ticketIndents[i].indentId);
							resHtml += ','
							resHtml += parseInt(data.ticketIndents[i].details[j].indentDetailId);
							resHtml += ');">退票</a>';
						}
						resHtml += '</td>';
						resHtml += '</tr>';
						
						entryCount++;
					}
				}
			}
			resHtml += '</table>';
			
			resHtml += '<div id="Pager" style="text-align:left">';
		    resHtml += '</div>';

			// $("indentDiv").html(resHtml);  
			document.getElementById("indentDiv").innerHTML = resHtml;
            document.getElementById("total").value = data.num;
            document.getElementById("offset").value = pageindx;
            
            initPager("/xlem/account/getTicketIndentData");
            ticketIndentStatus();
            couponStatus();
		}).fail(function(xhr, status, error) {  
        	alert("请求失败！");
		})	
    }
    
    function updatePager(offset, total) {
    	if (offset < 0) {
    		alert("已经是第一页");
    		return;
    	}
    	
    	if (offset >= total) {
    		alert("已经是最后一页");
    		return;
    	}
    	
    	initData(offset, "/xlem/account/getTicketIndentData");
    }
    
    function initPager(url) {
    	var total = document.getElementById("total").value;
    	var offset = document.getElementById("offset").value;
    	
        var pagerHtml = "";
        pagerHtml += '<nav aria-label="Page navigation">';
        pagerHtml += '<ul class="pagination">';
        pagerHtml += '<li>';
        pagerHtml += '<a href="javascript:updatePager(';
        pagerHtml += offset - 10;
        pagerHtml += ',';
        pagerHtml += total;
        pagerHtml += ');" ';
        pagerHtml += 'aria-label="Previous">';
        pagerHtml += '<span aria-hidden="true">&#171;</span>';
        pagerHtml += '</a>';
        pagerHtml += '</li>';
        
        for (i = 0; i < Math.ceil(total / 10); i++) {
        	pagerHtml += '<li><a href="javascript:updatePager(';
        	pagerHtml += i * 10;
        	pagerHtml += ',';
        	pagerHtml += total;
        	pagerHtml += ');" >';
        	pagerHtml += i + 1;
        	pagerHtml += '</a></li>';
        }
        
        pagerHtml += '<li>';
        pagerHtml += '<a href="javascript:updatePager(';
        pagerHtml += parseInt(offset) + 10;
        pagerHtml += ',';
        pagerHtml += total;
        pagerHtml += ');" ';
        pagerHtml += 'aria-label="Next">';
        pagerHtml += '<span aria-hidden="true">&#187;</span>';
        pagerHtml += '</a>';
        pagerHtml += '</li>';
        pagerHtml += '</ul>';
        pagerHtml += '</nav>';
        
		document.getElementById("Pager").innerHTML = pagerHtml;

    }
    
    $(document).on('click', "#returnToAccount", function () {
    	history.go(-1);
    })

