	$(document).on('click', "#searchTs", function () {
		var tsName = document.getElementById("tsName").value;
		var linkMan = document.getElementById("linkMan").value;
		var mobilePhone = document.getElementById("mobilePhone").value;
		
		var datas = "";
		
		if (tsName != "") {
			datas += "tsName=" + tsName;
		}
		else {
			datas += "tsName=all";
		}
		
		if (linkMan != "") {
			datas += "&&linkMan=" + linkMan;
		}
		else {
			datas += "&&linkMan=all";
		}
		
		if (mobilePhone != "") {
			datas += "&&mobilePhone=" + mobilePhone;
		}
		else {
			datas += "&&mobilePhone=all";
		}
	
	    $.ajax({  
	    	url: "/xlem/market/queryTravservice",
			data:'limit=5000',
			type:'get',
			dataType:'json',
	        async : false,  
	        data : datas,
		}).done(function(data, status, xhr) {
			var resHtml = "";
			resHtml += '<tr>';
			resHtml += '<th>旅行社名称</th>';
			resHtml += '<th>所属分社</th>';
			resHtml += '<th>所属部门</th>';
			resHtml += '<th>联系人</th>';
			resHtml += '<th>联系人身份证</th>';
			resHtml += '<th>联系人手机</th>';
			resHtml += '<th>电子邮箱</th>';
			resHtml += '<th>操作</th>';
			resHtml += '</tr>';
			
			for (var i = 0; i < data.length; i++) {
				resHtml += '<tr id="';
				resHtml += data[i].travelAgencyId;
				resHtml += '">';
				resHtml += '<td>';
				resHtml += data[i].name;
				resHtml += '</td>';
				resHtml += '<td>';
				resHtml += data[i].subTs;
				resHtml += '</td>' 
				resHtml += '<td>';
				resHtml += data[i].departTs;
				resHtml += '</td>' 
				resHtml += '<td>';
				resHtml += data[i].contactor;
				resHtml += '</td>' 
				resHtml += '<td>';
				resHtml += data[i].contactorId;
				resHtml += '</td>' 
				resHtml += '<td>';
				resHtml += data[i].contactorMp;
				resHtml += '</td>' 
				resHtml += '<td>';
				resHtml += data[i].email;
				resHtml += '</td>' 
				resHtml += '<td>';
				resHtml += '<a href="javascript:viewTs(';
				resHtml += data[i].travelAgencyId;
				resHtml += ');">查看详情</a> | ';
				resHtml += '<a href="javascript:AuditTs(';
				resHtml += data[i].travelAgencyId;
				resHtml += ');">审核</a> | ';
				resHtml += '<a href="javascript:RemoveTs(';
				resHtml += data[i].travelAgencyId;
				resHtml += ');">删除</a>';
				resHtml += '</td>';
				resHtml += '</tr>';
			}
			
			document.getElementById("travServiceTable").innerHTML = resHtml;
		}).fail(function(xhr, status, error) {  
        	alert("请求失败！");
		});
	});
	
	function auditTs(travelAgencyId) {
		var url = "/xlem/market/performAudit/?travelAgencyId=" + travelAgencyId;
		window.location = url;
	}
	
	function viewPicture(travelAgencyId, fname) {
		var url = "/xlem/market/showImage/?travelAgencyId=" + travelAgencyId;
		url += "&fname=" + fname;
		window.location = url;
	}
	
	$(document).on('click', "#auditImageReturn", function () {
		history.go(-1);
	});

	$(document).on('click', "#tsAuditReturn", function () {
		history.go(-1);
	});
	
	$(document).on('click', "#makeTsAuditPass", function () {
		var travelAgencyId = document.getElementById("travelAgencyId").value;
		var datas = "travelAgencyId=" + travelAgencyId;
	    $.ajax({  
	    	url: "/xlem/market/auditPass",
			data:'limit=5000',
			type:'post',
			dataType:'json',
	        async : false,  
	        data : datas,
		}).done(function(data, status, xhr) {
			alert(data.msg);
			history.go(-1);
		}).fail(function(xhr, status, error) {  
        	alert("请求失败！");
		});				
	});	
	
	$(document).on('click', "#makeTsAuditFail", function () {
		var travelAgencyId = document.getElementById("travelAgencyId").value;
		var datas = "travelAgencyId=" + travelAgencyId;
		
		$('#auditFailReason').modal({ backdrop: 'static', keyboard: false })
		        .one('click', '#makeSure', function() {
		    $('#auditFailReason').modal('hide');
		    var failReason = document.getElementById("failReason").value;
		    datas += "&&failReason=" + failReason;
		    $.ajax({  
		    	url: "/xlem/market/auditFail",
		    	data:'limit=5000',
		    	type:'post',
		    	dataType:'json',
		    	async : false,  
		    	data : datas,
		    }).done(function(data, status, xhr) {
		    	alert(data.msg);
				var url = "/xlem/market/auditTravService";
				window.location = url;
		    	// history.go(-1);
		    }).fail(function(xhr, status, error) {  
		    	alert("请求失败！");
		    });				
		});
	});
	
	function viewTs(travelAgencyId) {
		var url = "/xlem/market/viewTravService/" + travelAgencyId;
		window.location = url;
	}
		
	function removeTs(travelAgencyId) {
		var datas = "travelAgencyId=" + travelAgencyId;
		$.ajax({  
			url: "/xlem/market/removeTravService",
			data:'limit=5000',
			type:'get',
			dataType:'json',
		    async : false,  
		    data : datas,
		}).done(function(data, status, xhr) {
			alert(data.msg);
			history.go(-1);
		}).fail(function(xhr, status, error) {  
			alert("请求失败！");
		});
	}
		
	function discountSetting4Ts(travelAgencyId) {
		var url = "/xlem/market/discountSetting/" + travelAgencyId;
		window.location = url;
	}
		
	function discountSetting(travelAgencyId) {
		var datas = "travelAgencyId=" + travelAgencyId;
		var discount = document.getElementById("discountVal").value;
		datas += "&&discount=" + discount;
		$.ajax({  
			url: "/xlem/market/discountSure",
			data:'limit=5000',
			type:'post',
			dataType:'json',
		    async : false,  
		    data : datas,
		}).done(function(data, status, xhr) {
			alert(data.msg);
		}).fail(function(xhr, status, error) {  
			alert("请求失败！");
		});			
	}
		
	$(document).on('click', "#discountSettingReturn", function () {
		history.go(-1);
		window.location = location.href;
	});
		
	function reward4Ts(travelAgencyId) {
		var url = "/xlem/market/reward/" + travelAgencyId;
		window.location = url;
	}
		
	function reward(travelAgencyId) {
		var datas = "travelAgencyId=" + travelAgencyId;
		var rewardType = document.getElementById("RewardType").value;
		datas += "&&rewardType=" + rewardType;
		var reward = document.getElementById("rewardVal").value;
		datas += "&&reward=" + reward;
			
		$.ajax({  
			url: "/xlem/market/rewardSure",
			data:'limit=5000',
			type:'post',
			dataType:'json',
		    async : false,  
		    data : datas,
		}).done(function(data, status, xhr) {
			alert(data.msg);
		}).fail(function(xhr, status, error) {  
			alert("请求失败！");
		});						
	}
		
	$(document).on('click', "#rewardReturn", function () {
		history.go(-1);
		window.location = location.href;
	});
		
	$(document).ready(function() {
		if (document.getElementById("travServiceTIDiv") != null) {
			initData(0, "/xlem/market/getTicketIndentData");
			initPager("/xlem/market/getTicketIndentData");
		}
		
		$('.datepicker').each(function(){
		    var datepicker = $(this).datepicker();
		    if (this.value == null) {
		    	datepicker.datepicker('setDate', new Date());
		    }
		    else {
		    	datepicker.datepicker('setDate', new Date(this.value));
		    }
		});
		
		if (document.getElementById("deployedMsgDiv") != null) {
			initMsgData(0, "/xlem/market/getMsgData");
			initMsgPager("/xlem/market/getMsgData");
		}
		
	})
		    
	function initData(pageindx, url) {  
		var tbody = "";  
		var pageCount = "";  
		var datas = "offset=" + (pageindx);  
		        
		$.ajax({  
			url: url,
			data:'limit=5000',
			type:'get',
			dataType:'json',
		    async : false,  
		    data : datas,
		}).done(function(data, status, xhr) {
			resHtml = "";
			var entryCount = 0;
			resHtml = '<table id="ticketIndentOfTsTable" style="width:100%">';
			resHtml += '<tr>';
			resHtml += '<th>所属旅行社</th>';
			resHtml += '<th>订单编号</th>';
			resHtml += '<th>名称</th>';
			resHtml += '<th>订单金额</th>';
			resHtml += '<th>订单状态</th>';
			resHtml += '<th>操作</th>';
			resHtml += '</tr>';
					
			for (var i = 0; i < data.ticketIndents.length; i++) {
				for (var j = 0; j < data.ticketIndents[i].details.length; j++) {
					if (entryCount < 10) {
						resHtml += '<tr>';
						resHtml += '<td>';
						resHtml += data.ticketIndents[i].travelAgencyName;
						resHtml += '</td>';
						resHtml += '<td style="width:10%">';
						resHtml += data.ticketIndents[i].indentId;
						resHtml += '</td>';
						resHtml += '<td style="width:10%">';
						resHtml += data.ticketIndents[i].details[j].ticketName;
						resHtml += '</td>';
						resHtml += '<td style="width:10%">';
						resHtml += data.ticketIndents[i].details[j].subtotal;
						resHtml += '</td>';
						resHtml += '<td name="ticketIndentStatus" style="width:8%">';
						resHtml += data.ticketIndents[i].details[j].status;
						resHtml += '</td>';
						resHtml += '<td style="width:15%">';
						resHtml += '<a href="javascript:viewTicketIndent(';
						resHtml += parseInt(data.ticketIndents[i].indentId);
						resHtml += ',';
						resHtml += parseInt(data.ticketIndents[i].details[j].indentDetailId);
						resHtml += ');">详情</a>';
						resHtml += ' | ';
						resHtml += '<a href="javascript:passTicketIndent(';
						resHtml += parseInt(data.ticketIndents[i].indentId);
						resHtml += ',';
						resHtml += parseInt(data.ticketIndents[i].details[j].indentDetailId);
						resHtml += ');">审核</a>';
						resHtml += '</td>';
						resHtml += '</tr>';
								
						entryCount++;
					}
				}
			}
			resHtml += '</table>';
					
			resHtml += '<div id="Pager" style="text-align:left">';
			resHtml += '</div>';

			// $("indentDiv").html(resHtml);  
			document.getElementById("travServiceTIDiv").innerHTML = resHtml;
		    document.getElementById("total").value = data.num;
		    document.getElementById("offset").value = pageindx;
		            
		    initPager("/xlem/market/getTicketIndentData");
		    ticketIndentStatus();
		}).fail(function(xhr, status, error) {  
			alert("请求失败！");
		})	
	}
		    
	function updatePager(offset, total) {
		if (offset < 0) {
			alert("已经是第一页");
			return;
		}
		    	
		if (offset >= total) {
			alert("已经是最后一页");
		    return;
		}
		    	
		initData(offset, "/xlem/market/getTicketIndentData");
	}
		    
	function initPager(url) {
		var total = document.getElementById("total").value;
		var offset = document.getElementById("offset").value;
		    	
		var pagerHtml = "";
		pagerHtml += '<nav aria-label="Page navigation">';
		pagerHtml += '<ul class="pagination">';
		pagerHtml += '<li>';
		pagerHtml += '<a href="javascript:updatePager(';
		pagerHtml += offset - 10;
		pagerHtml += ',';
		pagerHtml += total;
		pagerHtml += ');" ';
		pagerHtml += 'aria-label="Previous">';
		pagerHtml += '<span aria-hidden="true">&#171;</span>';
		pagerHtml += '</a>';
		pagerHtml += '</li>';
		        
		for (i = 0; i < Math.ceil(total / 10); i++) {
			pagerHtml += '<li><a href="javascript:updatePager(';
		    pagerHtml += i * 10;
		    pagerHtml += ',';
		    pagerHtml += total;
		    pagerHtml += ');" >';
		    pagerHtml += i + 1;
		    pagerHtml += '</a></li>';
		}
		        
		pagerHtml += '<li>';
		pagerHtml += '<a href="javascript:updatePager(';
		pagerHtml += parseInt(offset) + 10;
		pagerHtml += ',';
		pagerHtml += total;
		pagerHtml += ');" ';
		pagerHtml += 'aria-label="Next">';
		pagerHtml += '<span aria-hidden="true">&#187;</span>';
		pagerHtml += '</a>';
		pagerHtml += '</li>';
		pagerHtml += '</ul>';
		pagerHtml += '</nav>';
		        
		document.getElementById("Pager").innerHTML = pagerHtml;
	}
		    
	function passTicketIndent (ticketIndentId, indentDetailId) {
		openDialog("confirmPass", "审核确认", 270, 100, "请对该订单进行审核审核", function(){
		},{
		buttons:[{
			text: "通过",
			click: function() {
				$(this).dialog("close");
		       
				var relay = ticketIndentId.toString();
				var strTicketIndentId = "";
				for (var i = 0; i < 32 - relay.length; i++) {
					strTicketIndentId += "0";
				}
				strTicketIndentId += relay;
				relay = indentDetailId.toString();
				var strIndentDetailId = "";
				for (var i = 0; i < 11 - relay.length; i++) {
					strIndentDetailId += "0";
				}
				strIndentDetailId += relay;
				
				var datas = "ticketIndentId=" + strTicketIndentId;
				datas += "&&indentDetailId=" + strIndentDetailId;
		        
		        $.ajax({  
					url: "/xlem/market/passTicketIndent",
					data:'limit=5000',
					type:'post',
					dataType:'json',
		            async : false,  
		            data : datas,
				}).done(function(data, status, xhr) {
					if (data.msg == "success") {
						datas = "ticketIndentId=" + strTicketIndentId;
				        $.ajax({  
							url: "/xlem/ticket/tsSyncToXLPW",
							data:'limit=5000',
							type:'post',
							dataType:'json',
				            async : false,  
				            data : datas,
						}).done(function(data, status, xhr) {
							alert("订单审核完成");
			            	var offset = document.getElementById("offset").value;
			        		initData(offset, "/xlem/market/getTicketIndentData");
			        		initPager("/xlem/market/getTicketIndentData");
						}).fail(function(xhr, status, error) {  
				        	alert("请求失败！");
						})
					}
					if (data.msg == "invalid") {
						alert("订单不存在");
		            	var offset = document.getElementById("offset").value;
		        		initData(offset, "/xlem/market/getTicketIndentData");
		        		initPager("/xlem/market/getTicketIndentData");
					}
					if (data.msg == "unlogined") {
						alert("登录失效，请重新登录");
						window.location = "/login/travservice";
					}
				}).fail(function(xhr, status, error) {  
		        	alert("请求失败！");
				})			
			}
		},{
			text: "不通过",
			click: function(){
				$(this).dialog("close");

				var relay = ticketIndentId.toString();
				var strTicketIndentId = "";
				for (var i = 0; i < 32 - relay.length; i++) {
					strTicketIndentId += "0";
				}
				strTicketIndentId += relay;
				relay = indentDetailId.toString();
				var strIndentDetailId = "";
				for (var i = 0; i < 11 - relay.length; i++) {
					strIndentDetailId += "0";
				}
				strIndentDetailId += relay;
				
				document.getElementById("ticketIndentId").value = strTicketIndentId;
				document.getElementById("indentDetailId").value = strIndentDetailId;
				
				$('#tiAuditFailReason').modal('show');
			}
		}]
		});
	}
	
	$(document).on('click', "#tiMakeSure", function () {
		$('#tiAuditFailReason').modal('hide');
		
		var ticketIndentId = document.getElementById("ticketIndentId").value;
		var indentDetailId = document.getElementById("indentDetailId").value;
		var failReason = document.getElementById("tiFailReason").value;
		
		var datas = "ticketIndentId=" + ticketIndentId;
		datas += "&&indentDetailId=" + indentDetailId;
        datas += "&&failReason=" + failReason;
		
        $.ajax({  
        	url: "/xlem/market/ngTicketIndent",
            data:'limit=5000',
            type:'post',
            dataType:'json',
            async : false,  
            data : datas,
        }).done(function(data, status, xhr) {
			if (data.msg == "success") {
				alert("订单审核完成");
	            var offset = document.getElementById("offset").value;
	        	initData(offset, "/xlem/market/getTicketIndentData");
	        	initPager("/xlem/market/getTicketIndentData");
			}
			if (data.msg == "invalid") {
				alert("订单不存在");
	            var offset = document.getElementById("offset").value;
	        	initData(offset, "/xlem/market/getTicketIndentData");
	        	initPager("/xlem/market/getTicketIndentData");
			}
			if (data.msg == "unlogined") {
				alert("登录失效，请重新登录");
				window.location = "/login/travservice";
			}
        }).fail(function(xhr, status, error) {  
        	alert("请求失败！");
        });				
	})
	
	function viewTicketIndent (ticketIndentId, indentDetailId) {
		var relay = ticketIndentId.toString();
		var strTicketIndentId = "";
		for (var i = 0; i < 32 - relay.length; i++) {
			strTicketIndentId += "0";
		}
		strTicketIndentId += relay;
		relay = indentDetailId.toString();
		var strIndentDetailId = "";
		for (var i = 0; i < 11 - relay.length; i++) {
			strIndentDetailId += "0";
		}
		strIndentDetailId += relay;
		
		var url = "/xlem/market/viewTicketIndent?ticketIndentId=" + strTicketIndentId;
		url += "&indentDetailId=" + strIndentDetailId;
		window.location = url;
	}
	
	$(document).on('click', "#ticketIndentReturn", function () {
		history.go(-1);
	});

	function ticketIndentStatus() {
		var indentTable = document.getElementById("ticketIndentOfTsTable");
		var trs = indentTable.getElementsByTagName("tr");
		
		// Iterate over the links
		for (var i = 1, len = trs.length, tr; i < len; ++i) {
		    tr = trs[i];
		    var tds = tr.getElementsByTagName("td");
		    var status = tds[4].innerText;
		    var text = status;
		    switch (status) {
		    case "00":
		    	text = "待审核";
		    	break;
		    
		    case "01":
		    	text = "已审核";
		    	break;
		    
		    case "02":
		    	text = "已使用";
		    	break;
		    	
		    case "08":
		    	text = "已退票";
		    	break;
		    
		    case "04":
		    	text = "已删除";
		    	break;
		    	
		    case "05":
		    	text = "审核未通过";
		    	break;
		    		
		    }
		    tds[4].innerText = text;
		}
	}
	
	$(document).on('click', "#searchTsOfTI", function () {
		var tsNameOfTI = document.getElementById("tsNameOfTI").value;
		var linkManOfTI = document.getElementById("linkManOfTI").value;
		var mobilePhoneOfTI = document.getElementById("mobilePhoneOfTI").value;
		
		var datas = "";
		
		if (tsNameOfTI != "") {
			datas += "tsNameOfTI=" + tsNameOfTI;
		}
		else {
			datas += "tsNameOfTI=all";
		}
		
		if (linkManOfTI != "") {
			datas += "&&linkManOfTI=" + linkManOfTI;
		}
		else {
			datas += "&&linkManOfTI=all";
		}
		
		if (mobilePhoneOfTI != "") {
			datas += "&&mobilePhoneOfTI=" + mobilePhoneOfTI;
		}
		else {
			datas += "&&mobilePhoneOfTI=all";
		}

		$.ajax({  
	    	url: "/xlem/market/queryTicketIndent",
			data:'limit=5000',
			type:'get',
			dataType:'json',
	        async : false,  
	        data : datas,
		}).done(function(data, status, xhr) {
			initData(0, "/xlem/market/getTicketIndentData");
			initPager("/xlem/market/getTicketIndentData");
			
		}).fail(function(xhr, status, error) {  
        	alert("请求失败！");
		});
	});
	
	function modifyTicketTopLimit(ticketPriceId) {
		$('#ticketResTopLimit').modal({ backdrop: 'static', keyboard: false })
        	.one('click', '#confirmTicketTopLimit', function() {
        	$('#ticketResTopLimit').modal('hide');	
        	var topLimit = document.getElementById("topLimit").value;
        	if (Math.floor(topLimit).toString() == topLimit) {
        	    var datas = "ticketPriceId=" + ticketPriceId;
        	    datas += "&&topLimit=" + topLimit;
        	    $.ajax({  
        	    	url: "/xlem/market/changeTicketTopLimit",
        	    	data:'limit=5000',
        	    	type:'post',
        	    	dataType:'json',
        	    	async : false,  
        	    	data : datas,
        	    }).done(function(data, status, xhr) {
        	    	var resHtml = "";
        	    	resHtml += '<tr>';
        	    	resHtml += '<th>票名</th>';
        	    	resHtml += '<th>价格</th>';
        	    	resHtml += '<th>上限票额</th>';
        	    	resHtml += '<th>剩余票额</th>';
        	    	resHtml += '<th>操作</th>';
        	    	resHtml += '</tr>';
        	    	
        	    	for (var i = 0; i < data.num; i++) {
        	    		resHtml += '<tr id=';
        	    		resHtml += data.ticketPrices[i].ticketPriceId;
        	    		resHtml += '>';
        	    		resHtml += '<td>';
        	    		resHtml += data.ticketPrices[i].ticketName;
        	    		resHtml += '</td>';
        	    		resHtml += '<td>';
        	    		resHtml += data.ticketPrices[i].unitPrice;
        	    		resHtml += '</td>'
        	    		resHtml += '<td>';
        	    		resHtml += data.ticketPrices[i].topLimit;
        	    		resHtml += '</td>';
        	    		resHtml += '<td>';
        	    		resHtml += data.ticketPrices[i].remainToplimit;
        	    		resHtml += '</td>';
        	    		resHtml += '<td>';
        	    		resHtml += '<a href="javascript:modifyTicketTopLimit(';
        	    		resHtml += data.ticketPrices[i].ticketPriceId;
        	    		resHtml += ');">修改额度</a>';
        	    		resHtml += '</td>';
        	    		resHtml += '</tr>';
        	    	}
        	    	
        	    	document.getElementById("ticketResTable").innerHTML = resHtml;
        	    }).fail(function(xhr, status, error) {  
        	    	alert("请求失败！");
        	    });				
        	}
        	else {
        		alert("请正确输入");
        		return;
        	}
        });
	}
	
	$(document).on('click', '#msgDeploy', function() {
		var msgTitle = document.getElementById("msgTitle").value;
		if (msgTitle == "") {
			alert("请输入消息标题");
			return;
		}
		
		var msgContent = document.getElementById("msgContent").value;
		if (msgContent == "") {
			alert("请输入消息正文");
			return;
		}
		
		var startTime = document.getElementById("msgStartTime").value;
		var endTime = document.getElementById("msgEndTime").value;
		
		var sDate = new Date(startTime);
		var eDate = new Date(endTime);
		if (sDate > eDate) {
			alert("结束日期不能小于开始日期");
			return;
		}
		
		var datas = "title=" + msgTitle;
		datas += "&&content=" + msgContent;
		datas += "&&startTime=" + startTime;
		datas += "&&endTime=" + endTime;
		
        $.ajax({  
			url: "/xlem/market/performMsgDeploy",
			data:'limit=5000',
			type:'post',
			dataType:'json',
            async : false,  
            data : datas,
		}).done(function(data, status, xhr) {
			alert("成功发布消息");
			window.location = "/xlem/market/deployedMsgList";
		}).fail(function(xhr, status, error) {  
        	alert("请求失败！");
		})			
	})
	
	function initMsgData(pageindx, url) {  
		var tbody = "";  
		var pageCount = "";  
		var datas = "offset=" + (pageindx);  
		        
		$.ajax({  
			url: url,
			data:'limit=5000',
			type:'get',
			dataType:'json',
		    async : false,  
		    data : datas,
		}).done(function(data, status, xhr) {
			resHtml = "";
			var entryCount = 0;
			resHtml = '<table id="deployedNoticeTable" style="width:100%">';
			resHtml += '<tr>';
			resHtml += '<th>消息标题</th>';
			resHtml += '<th>起始时间</th>';
			resHtml += '<th>结束时间</th>';
			resHtml += '<th>消息内容</th>';
			resHtml += '<th>操作</th>';
			resHtml += '</tr>';
					
			for (var i = 0; i < data.deployedNotices.length; i++) {
				if (entryCount < 10) {
					resHtml += '<tr>';
					resHtml += '<td>';
					resHtml += data.deployedNotices[i].noticeTitle;
					resHtml += '</td>';
					resHtml += '<td>';
					var startDate = new Date(data.deployedNotices[i].startDate);
					resHtml += startDate.getFullYear() + '-' + (startDate.getMonth() + 1) + '-' + startDate.getDate();
					resHtml += '</td>';
					resHtml += '<td>';
					var endDate = new Date(data.deployedNotices[i].endDate);
					resHtml += endDate.getFullYear() + '-' + (endDate.getMonth() + 1) + '-' + endDate.getDate();
					resHtml += '</td>';
					resHtml += '<td>';
					resHtml += data.deployedNotices[i].remark;
					resHtml += '</td>';
					resHtml += '<td>';
					resHtml += '<a href="javascript:updateNotice(';
					resHtml += parseInt(data.deployedNotices[i].noticeId);
					resHtml += ');">修改</a>';
					resHtml += ' | ';
					resHtml += '<a href="javascript:removeNotice(';
					resHtml += parseInt(data.deployedNotices[i].noticeId);
					resHtml += ');">删除</a>';
					resHtml += '</td>';
					resHtml += '</tr>';
								
					entryCount++;
				}
			}
			resHtml += '</table>';
					
			resHtml += '<div id="Pager" style="text-align:left">';
			resHtml += '</div>';

			// $("indentDiv").html(resHtml);  
			document.getElementById("deployedMsgDiv").innerHTML = resHtml;
		    document.getElementById("msg_total").value = data.num;
		    document.getElementById("msg_offset").value = pageindx;
		            
		    initMsgPager("/xlem/market/getMsgData");
		}).fail(function(xhr, status, error) {  
			alert("请求失败！");
		})	
	}
		    
	function updateMsgPager(offset, total) {
		if (offset < 0) {
			alert("已经是第一页");
			return;
		}
		    	
		if (offset >= total) {
			alert("已经是最后一页");
		    return;
		}
		    	
		initData(offset, "/xlem/market/getMsgData");
	}
		    
	function initMsgPager(url) {
		var total = document.getElementById("msg_total").value;
		var offset = document.getElementById("msg_offset").value;
		    	
		var pagerHtml = "";
		pagerHtml += '<nav aria-label="Page navigation">';
		pagerHtml += '<ul class="pagination">';
		pagerHtml += '<li>';
		pagerHtml += '<a href="javascript:updateMsgPager(';
		pagerHtml += offset - 10;
		pagerHtml += ',';
		pagerHtml += total;
		pagerHtml += ');" ';
		pagerHtml += 'aria-label="Previous">';
		pagerHtml += '<span aria-hidden="true">&#171;</span>';
		pagerHtml += '</a>';
		pagerHtml += '</li>';
		        
		for (i = 0; i < Math.ceil(total / 10); i++) {
			pagerHtml += '<li><a href="javascript:updateMsgPager(';
		    pagerHtml += i * 10;
		    pagerHtml += ',';
		    pagerHtml += total;
		    pagerHtml += ');" >';
		    pagerHtml += i + 1;
		    pagerHtml += '</a></li>';
		}
		        
		pagerHtml += '<li>';
		pagerHtml += '<a href="javascript:updateMsgPager(';
		pagerHtml += parseInt(offset) + 10;
		pagerHtml += ',';
		pagerHtml += total;
		pagerHtml += ');" ';
		pagerHtml += 'aria-label="Next">';
		pagerHtml += '<span aria-hidden="true">&#187;</span>';
		pagerHtml += '</a>';
		pagerHtml += '</li>';
		pagerHtml += '</ul>';
		pagerHtml += '</nav>';
		        
		document.getElementById("Pager").innerHTML = pagerHtml;
	}
	
	function updateNotice(noticeId) {
		window.location = "/xlem/market/updateNotice/" + noticeId + '?form';
	}
	
	function removeNotice(noticeId) {
		var datas = "noticeId=" + noticeId;  
		        
		$.ajax({  
			url: '/xlem/market/removeNotice',
			data:'limit=5000',
			type:'post',
			dataType:'json',
		    async : false,  
		    data : datas,
		}).done(function(data, status, xhr) {
			var offset = document.getElementById("msg_offset").value; 
			initMsgData(offset, "/xlem/market/getMsgData");
			initMsgPager("/xlem/market/getMsgData");
		}).fail(function(xhr, status, error) {  
			alert("请求失败！");
		})			
	}

	$(document).on('click', '#msgUpdateSubmit', function () {
		$("#noticeUpdateForm").submit();
	})
	
	$(document).on('click', '#msgUpdateCancel', function () {
		history.go(-1);
	})