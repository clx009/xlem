    $(document).ready(function() {  
    	$('checkinDate').datepicker({
    		dataFormat: 'yy-mm-dd',
    		changeYear: true
    	});
    	
    	$('checkoutDate').datepicker({
    		dataFormat: 'yy-mm-dd',
    		changeYear: true
    	});    	
    })
