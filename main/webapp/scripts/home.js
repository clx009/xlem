	function changeImg() { 
		var obj=document.getElementById("sjyzImg"); 
		var curId = obj.alt;
		$.ajax({
			url:'/xlem/index/nextImage?{$curId}',
			data:'limit=100',
			dataType:'json',
			type:'get',
			success: function(result){	
				obj.src = result;
			}
		});
	}
	
	$(document).ready(function() {  
		var curIndex=0; 
		//时间间隔 单位毫秒 
		var timeInterval=1000; 
		setInterval(changeImg,timeInterval);
	});

	function weatherForcast() {
		$.ajax({
			url:'/xlem/index/weatherForcast',
			  headers: {          
				  Accept: "application/json; charset=utf-8",         
				   "Content-Type": "application/json; charset=utf-8"   
			 },    
			data:'limit=100',
			dataType:'json',
			type:'get'
        	}).done(function(data, status, xhr) { 
        		document.getElementById("today").innerHTML = data[0].date;
        		document.getElementById("tomorrow").innerHTML = data[1].date;
        		document.getElementById("thirdday").innerHTML = data[2].date;
				
        		document.getElementById("todayIcon").src = data[0].icon;
				document.getElementById("tomorrowIcon").src = data[1].icon;
				document.getElementById("thirddayIcon").src = data[2].icon;
				
				document.getElementById("todayWeather_d").innerHTML = data[0].weather_d;
				document.getElementById("tomorrowWeather_d").innerHTML = data[1].weather_d;
				document.getElementById("thirddayWeather_d").innerHTML = data[2].weather_d;

				document.getElementById("todayWeather_n").innerHTML = data[0].weather_n;
				document.getElementById("tomorrowWeather_n").innerHTML = data[1].weather_n;
				document.getElementById("thirddayWeather_n").innerHTML = data[2].weather_n;

				document.getElementById("todayTemp").innerHTML = data[0].temp;
				document.getElementById("tomorrowTemp").innerHTML = data[1].temp;
				document.getElementById("thirddayTemp").innerHTML = data[2].temp;

				document.getElementById("todayWind").innerHTML = data[0].wind;
				document.getElementById("tomorrowWind").innerHTML = data[1].wind;
				document.getElementById("thirddayWind").innerHTML = data[2].wind;
        	}).fail(function(xhr, status, error) {  
        		alter("请求失败！");
        	
        })
        	
		/* var divToday = document.getElementById("today");
		var divTomorrow = document.getElementById("tomorrow");
		var divThirdday = document.getElementById("thirdday");
		
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();

		if(dd<10) {
		    dd='0'+dd
		} 

		if(mm<10) {
		    mm='0'+mm
		} 
		
		divToday.innerHTML = mm + '月' + dd + '日';
		
		var tomorrow = new Date();
		tomorrow.setDate(today.getDate() + 1);
		dd = tomorrow.getDate();
		mm = tomorrow.getMonth()+1; //January is 0!
		divTomorrow.innerHTML = mm + '月' + dd + '日';
		
		var thirdday = new Date();
		thirdday.setDate(tomorrow.getDate() + 1);
		dd = thirdday.getDate();
		mm = thirdday.getMonth()+1; //January is 0!
		divThirdday.innerHTML = mm + '月' + dd + '日'; */
		

	}
		
	$(document).ready(function() {  
		weatherForcast();
	});
	
	function showTickets() {
		$.ajax({
			url:'/xlem/index/showTickets',
			headers: {          
				Accept: "application/json; charset=utf-8",         
				"Content-Type": "application/json; charset=utf-8"   
			},    			
			data:'limit=500',
			dataType:'json',
			type:'get'
		}).done(function(data, status, xhr) {  
			document.getElementById("ticketShow0").src = data[0];
			var start = data[0].indexOf("name=");
			var end = data[0].slice(start).indexOf("&");
			var name = data[0].slice(start).slice(5, end);
			document.getElementById("ticketBooking0").href = 'ticket/booking?name=' + name;
			document.getElementById("ticketShopcart0").href = 'ticket/enterShopcart?name=' + name;
			document.getElementById("ticketShow1").src = data[1];
			start = data[1].indexOf("name=");
			end = data[1].slice(start).indexOf("&");
			name = data[1].slice(start).slice(5, end);
			document.getElementById("ticketBooking1").href = 'ticket/booking?name=' + name;
			document.getElementById("ticketShopcart1").href = 'ticket/enterShopcart?name=' + name;
			document.getElementById("ticketShow2").src = data[2];       
			start = data[2].indexOf("name=");
			end = data[2].slice(start).indexOf("&");
			name = data[2].slice(start).slice(5, end);
			document.getElementById("ticketBooking2").href = 'ticket/booking?name=' + name;
			document.getElementById("ticketShopcart2").href = 'ticket/enterShopcart?name=' + name;
		}).fail(function(xhr, status, error) {  
        	alter("请求失败！");
		})	
	}
	
	$(document).ready(function() {
		showTickets();
	})
	
	function showHotels() {
		$.ajax({
			url:'/xlem/index/showHotels',
			headers: {          
				Accept: "application/json; charset=utf-8",         
				"Content-Type": "application/json; charset=utf-8"   
			},    			
			data:'limit=500',
			dataType:'json',
			type:'get'
		}).done(function(data, status, xhr) {  
			document.getElementById("hotelShow0").src = data[0];
			document.getElementById("hotelShow1").src = data[1];
			document.getElementById("hotelShow2").src = data[2];       
			document.getElementById("hotelShow3").src = data[3];       
		}).fail(function(xhr, status, error) {  
        	alter("请求失败！");
		})	
	}
	
	$(document).ready(function() {
		showHotels();
	})	
	
	function showSuites() {
		$.ajax({
			url:'/xlem/index/showSuites',
			headers: {          
				Accept: "application/json; charset=utf-8",         
				"Content-Type": "application/json; charset=utf-8"   
			},    			
			data:'limit=500',
			dataType:'json',
			type:'get'
		}).done(function(data, status, xhr) {
			for (var i = 0; i < data.length; i++) {
				var id = "suiteShow" + i;
				document.getElementById(id).src = data[i];
				var start = data[i].indexOf("name=");
				var end = data[i].slice(start).indexOf("&");
				var name = data[i].slice(start).slice(5, end);
				var relay = name.replace(/\+/g, "%2B").replace(/\"/g,'%22').replace(/\'/g, '%27').replace(/\//g,'%2F');
				id = "suiteBooking" + i;
				document.getElementById(id).href = 'ticket/booking?name=' + relay;
				id = "suiteShopCart" + i;
				document.getElementById(id).href = 'ticket/enterShopcart?name=' + relay;
			}
			// document.getElementById("suiteShow2").src = data[2];
		}).fail(function(xhr, status, error) {  
        	alter("请求失败！");
			for (j = 0; j < 3; j++) {
				var aId = "suiteBooking" + j;
				document.getElementById(aId).innerHTML = "";
				aId = "suiteShopCart" + j;
				document.getElementById(aId).innerHTML = "";
			}
		})	
	}
	
	$(document).ready(function() {
		showSuites();
	})
	
    function bookTicket(ticketId) {
    	window.location.href="/xlem/ticket/book?"+ticketId;
    }
	
	$(document).ready(function () {
		//时间间隔 单位毫秒 
		var timeInterval = 5000; 
		setInterval(showNotices, timeInterval);
	})
	
	function showNotices() {
		$.ajax({
			url:'/xlem/index/showNotices',
			headers: {          
			  Accept: "application/json; charset=utf-8",         
			   "Content-Type": "application/json; charset=utf-8"   
			},    
			data:'limit=100',
			dataType:'json',
			type:'get'
    	}).done(function(data, status, xhr) { 
			resHtml = "";
			var entryCount = 0;
			resHtml = '<table id="displayNoticeTable" style="width:100%">';
			resHtml += '</tr>';
					
			for (var i = 0; i < data.deployedNotices.length; i++) {
				if (entryCount < 5) {
					resHtml += '<tr>';
					resHtml += '<td>';
					resHtml += '<a href="/xlem/index/showNoticeDetail/';
					resHtml += parseInt(data.deployedNotices[i].noticeId);
					resHtml += '"/>';
					resHtml += data.deployedNotices[i].noticeTitle;
					resHtml += '</a></td>';
					resHtml += '</tr>';
								
					entryCount++;
				}
			}
			
			resHtml += '</table>';    		
    		
    		document.getElementById("notice").innerHTML = resHtml;
    	}).fail(function(xhr, status, error) {  
    		alter("请求失败！");
    	})
	}
	

    